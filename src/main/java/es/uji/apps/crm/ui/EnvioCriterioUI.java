package es.uji.apps.crm.ui;

import java.util.List;

public class EnvioCriterioUI {
    private Long envio;
    private Long campanya;
    private List<Long> campanyaEstados;
    private List<Long> criteriosCirculo;
    private List<Long> criteriosEstudioUJI;
    private List<Long> criteriosEstudioNoUJI;
    private List<Long> criteriosEstudioUJIBeca;
    private Long mesNacimiento;
    private Long combinadoCampanyas;
    private Long tipoSeleccionCampanya;
    private Long anyoNacimientoDesde;
    private Long anyoNacimientoHasta;
    private Long anyoFinalizacionEstudioUJIInicio;
    private Long anyoFinalizacionEstudioUJIFin;
    private Long anyoFinalizacionEstudioNoUJIInicio;
    private Long anyoFinalizacionEstudioNoUJIFin;

    public EnvioCriterioUI() {
    }

    public Long getEnvio() {
        return envio;
    }

    public void setEnvio(Long envio) {
        this.envio = envio;
    }

    public Long getCampanya() {
        return campanya;
    }

    public void setCampanya(Long campanya) {
        this.campanya = campanya;
    }

    public List<Long> getCampanyaEstados() {
        return campanyaEstados;
    }

    public void setCampanyaEstados(List<Long> campanyaEstados) {
        this.campanyaEstados = campanyaEstados;
    }

    public List<Long> getCriteriosCirculo() {
        return criteriosCirculo;
    }

    public void setCriteriosCirculo(List<Long> criteriosCirculo) {
        this.criteriosCirculo = criteriosCirculo;
    }

    public Long getMesNacimiento() {
        return mesNacimiento;
    }

    public void setMesNacimiento(Long mesNacimiento) {
        this.mesNacimiento = mesNacimiento;
    }

    public Long getCombinadoCampanyas() {
        return combinadoCampanyas;
    }

    public void setCombinadoCampanyas(Long combinadoCampanyas) {
        this.combinadoCampanyas = combinadoCampanyas;
    }

    public Long getTipoSeleccionCampanya() {
        return tipoSeleccionCampanya;
    }

    public void setTipoSeleccionCampanya(Long tipoSeleccionCampanya) {
        this.tipoSeleccionCampanya = tipoSeleccionCampanya;
    }

    public Long getAnyoNacimientoDesde() {
        return anyoNacimientoDesde;
    }

    public void setAnyoNacimientoDesde(Long anyoNacimientoDesde) {
        this.anyoNacimientoDesde = anyoNacimientoDesde;
    }

    public Long getAnyoNacimientoHasta() {
        return anyoNacimientoHasta;
    }

    public void setAnyoNacimientoHasta(Long anyoNacimientoHasta) {
        this.anyoNacimientoHasta = anyoNacimientoHasta;
    }

    public List<Long> getCriteriosEstudioUJI() {
        return criteriosEstudioUJI;
    }

    public void setCriteriosEstudioUJI(List<Long> criteriosEstudioUJI) {
        this.criteriosEstudioUJI = criteriosEstudioUJI;
    }

    public Long getAnyoFinalizacionEstudioUJIInicio() {
        return anyoFinalizacionEstudioUJIInicio;
    }

    public void setAnyoFinalizacionEstudioUJIInicio(Long anyoFinalizacionEstudioUJIInicio) {
        this.anyoFinalizacionEstudioUJIInicio = anyoFinalizacionEstudioUJIInicio;
    }

    public Long getAnyoFinalizacionEstudioUJIFin() {
        return anyoFinalizacionEstudioUJIFin;
    }

    public void setAnyoFinalizacionEstudioUJIFin(Long anyoFinalizacionEstudioUJIFin) {
        this.anyoFinalizacionEstudioUJIFin = anyoFinalizacionEstudioUJIFin;
    }

    public List<Long> getCriteriosEstudioNoUJI() {
        return criteriosEstudioNoUJI;
    }

    public void setCriteriosEstudioNoUJI(List<Long> criteriosEstudioNoUJI) {
        this.criteriosEstudioNoUJI = criteriosEstudioNoUJI;
    }

    public Long getAnyoFinalizacionEstudioNoUJIInicio() {
        return anyoFinalizacionEstudioNoUJIInicio;
    }

    public void setAnyoFinalizacionEstudioNoUJIInicio(Long anyoFinalizacionEstudioNoUJIInicio) {
        this.anyoFinalizacionEstudioNoUJIInicio = anyoFinalizacionEstudioNoUJIInicio;
    }

    public Long getAnyoFinalizacionEstudioNoUJIFin() {
        return anyoFinalizacionEstudioNoUJIFin;
    }

    public void setAnyoFinalizacionEstudioNoUJIFin(Long anyoFinalizacionEstudioNoUJIFin) {
        this.anyoFinalizacionEstudioNoUJIFin = anyoFinalizacionEstudioNoUJIFin;
    }

    public List<Long> getCriteriosEstudioUJIBeca() {
        return criteriosEstudioUJIBeca;
    }

    public void setCriteriosEstudioUJIBeca(List<Long> criteriosEstudioUJIBeca) {
        this.criteriosEstudioUJIBeca = criteriosEstudioUJIBeca;
    }
}
