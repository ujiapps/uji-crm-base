package es.uji.apps.crm.ui;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.uji.commons.rest.ParamUtils;

public class FormularioUI {
    private Long id;
    private String titulo;
    private String accion;
    private String metodo;
    private String encType;
    private String textoLOPD;
    private Boolean autenticado;
    private Date fechaInicioFormulario;
    private Long campanya;
    private Date fechaFinFormulario;
    private String plantilla;
    private String cabecera;
    private List<CampoUI> campos;

    public FormularioUI() {
        this.metodo = "post";
        this.encType = "multipart/form-data";
        this.autenticado = false;
    }

    public FormularioUI(String accion, String metodo, String encType) {
        this.accion = accion;
        this.metodo = metodo;
        this.encType = encType;
        this.autenticado = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getMetodo() {
        return metodo;
    }

    public void setMetodo(String metodo) {
        this.metodo = metodo;
    }

    public List<CampoUI> getCampos() {
        return campos;
    }

    public void setCampos(List<CampoUI> campos) {
        this.campos = campos;
    }

    public String getEncType() {
        return encType;
    }

    public void setEncType(String encType) {
        this.encType = encType;
    }

    public String getTextoLOPD() {
        return textoLOPD;
    }

    public void setTextoLOPD(String textoLOPD) {
        this.textoLOPD = textoLOPD;
    }

    public CampoUI getCampoByNombre(String nombre) {
        for (CampoUI campo : campos)
        {
            if (campo.getNombre().equals(nombre))
            {
                return campo;
            }
        }
        return null;
    }

    public Long getCampanya() {
        return campanya;
    }

    public void setCampanya(Long campanya) {
        this.campanya = campanya;
    }

    public Boolean getAutenticado() {
        return autenticado;
    }

    public void setAutenticado(Boolean autenticado) {
        this.autenticado = autenticado;
    }

    public Date getFechaInicioFormulario() {
        return fechaInicioFormulario;
    }

    public void setFechaInicioFormulario(Date fechaInicioFormulario) {

        Calendar inicio = Calendar.getInstance();

        if (ParamUtils.isNotNull(fechaInicioFormulario))
        {

            inicio.setTime(fechaInicioFormulario);

        }
        else
        {
            inicio.setTime(new Date());
        }
        inicio.set(Calendar.SECOND, 0);
        inicio.set(Calendar.MILLISECOND, 0);
        inicio.set(Calendar.MINUTE, 0);
        inicio.set(Calendar.HOUR_OF_DAY, 0);

        this.fechaInicioFormulario = inicio.getTime();
    }

    public Date getFechaFinFormulario() {
        return fechaFinFormulario;
    }

    public void setFechaFinFormulario(Date fechaFinFormulario) {

        Calendar fin = Calendar.getInstance();

        if (ParamUtils.isNotNull(fechaFinFormulario))
        {
            fin.setTime(fechaFinFormulario);
        }
        else
        {
            fin.setTime(new Date());
        }

        fin.set(Calendar.SECOND, 59);
        fin.set(Calendar.MILLISECOND, 0);
        fin.set(Calendar.MINUTE, 59);
        fin.set(Calendar.HOUR_OF_DAY, 23);

        this.fechaFinFormulario = fin.getTime();
    }

    public String getPlantilla() {
        return plantilla;
    }

    public void setPlantilla(String plantilla) {
        this.plantilla = plantilla;
    }

    public String getCabecera() {
        return cabecera;
    }

    public void setCabecera(String cabecera) {
        this.cabecera = cabecera;
    }
}