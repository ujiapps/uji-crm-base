package es.uji.apps.crm.ui;

import es.uji.apps.crm.model.CartaImagen;
import es.uji.apps.crm.model.EnvioImagen;

public class RecursoUI {

    private String id;
    private String url;
    private String urlThumbnail;
    private String urlRedirect;
    private String nombre;
    private String typeMime;

    public RecursoUI(EnvioImagen envioImagen, String urlAde) {
        this.id = envioImagen.getId().toString();
        this.url = urlAde;
        this.urlThumbnail = url + envioImagen.getReferencia() + "?t=SCALE,h=100";
        this.urlRedirect = urlAde.concat(envioImagen.getReferencia());
        this.nombre = envioImagen.getNombre();
        this.typeMime = "";
    }

    public RecursoUI(CartaImagen cartaImagen, String urlAde) {
        this.id = cartaImagen.getId().toString();
        this.url = urlAde;
        this.urlThumbnail = url + cartaImagen.getReferencia() + "?t=SCALE,h=100";
        this.urlRedirect = urlAde.concat(cartaImagen.getReferencia());
        this.nombre = cartaImagen.getNombre();
        this.typeMime = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlThumbnail() {
        return urlThumbnail;
    }

    public void setUrlThumbnail(String urlThumbnail) {
        this.urlThumbnail = urlThumbnail;
    }

    public String getUrlRedirect() {
        return urlRedirect;
    }

    public void setUrlRedirect(String urlRedirect) {
        this.urlRedirect = urlRedirect;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTypeMime() {
        return typeMime;
    }

    public void setTypeMime(String typeMime) {
        this.typeMime = typeMime;
    }
}
