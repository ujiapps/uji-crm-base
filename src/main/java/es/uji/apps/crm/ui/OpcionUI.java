package es.uji.apps.crm.ui;

import com.mysema.query.annotations.QueryProjection;

import java.util.List;

public class OpcionUI {
    private String valor;
    private String texto;
    private String ayuda;
    private List<Long> mostrar;
    private List<Long> ocultar;

    @QueryProjection
    public OpcionUI(String valor, String texto) {
        this.valor = valor;
        this.texto = texto;
    }

    public OpcionUI(String valor, String texto, String ayuda) {
        this.valor = valor;
        this.texto = texto;
        this.ayuda = ayuda;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getAyuda() {
        return ayuda;
    }

    public void setAyuda(String ayuda) {
        this.ayuda = ayuda;
    }

    public boolean hasAyuda() {
        return (ayuda != null && !ayuda.isEmpty());
    }

    public List<Long> getMostrar() {
        return mostrar;
    }

    public void setMostrar(List<Long> mostrar) {
        this.mostrar = mostrar;
    }

    public List<Long> getOcultar() {
        return ocultar;
    }

    public void setOcultar(List<Long> ocultar) {
        this.ocultar = ocultar;
    }
}
