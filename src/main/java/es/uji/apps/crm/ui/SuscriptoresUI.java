package es.uji.apps.crm.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.domains.TipoItem;
import es.uji.commons.rest.ParamUtils;

import javax.persistence.criteria.CriteriaBuilder;

public class SuscriptoresUI {
    private Long id;
    private String nombre;
    private String descripcion;
    private Boolean disableAplicacionMovil;
    private Boolean readonlyAplicacionMovil;
    private Boolean disableCorreo;
    private Boolean readonlyCorreo;
    private Boolean disableCorreoPostal;
    private Boolean readonlyCorreoPostal;
    private String grupo;
    private Integer ordenSuscriptor;
    private Integer ordenGrupo;

    public SuscriptoresUI(Item i, String idioma) {
        this.id = i.getId();
        this.ordenSuscriptor = i.getOrden();
        this.ordenGrupo = i.getGrupo().getOrden();
        switch (idioma)
        {
            case "es":
                this.nombre = i.getNombreEs();
                this.grupo = (i.getGrupo() != null) ? i.getGrupo().getNombreEs() + (ParamUtils.isNotNull(i.getGrupo().getDescripcionEs()) ? "<div class='field-hint' style='font-weight: normal !important; font-style: italic !important;'>" + i.getGrupo().getDescripcionEs() + "</div>" : "") : "";
                this.descripcion = i.getDescripcionEs();

                break;
            case "en":
                this.nombre = i.getNombreUk();
                this.grupo = (i.getGrupo() != null) ? i.getGrupo().getNombreUk() + (ParamUtils.isNotNull(i.getGrupo().getDescripcionUk()) ? "<div class='field-hint' style='font-weight: normal !important; font-style: italic !important;'>" + i.getGrupo().getDescripcionUk() + "</div>" : "") : "";
                this.descripcion = i.getDescripcionUk();

                break;
            default:
                this.nombre = i.getNombreCa();
                this.grupo = (i.getGrupo() != null) ? i.getGrupo().getNombreCa() + (ParamUtils.isNotNull(i.getGrupo().getDescripcionCa()) ? "<div class='field-hint' style='font-weight: normal !important; font-style: italic !important;'>" + i.getGrupo().getDescripcionCa() + "</div>" : "") : "";
                this.descripcion = i.getDescripcionCa();

                break;
        }

        if (i.getAplicacionMovil().getId().equals(TipoItem.NO_MODIFICABLE.getId())
                || i.getAplicacionMovil().getId().equals(TipoItem.NO_MODIFICABLE_DEFECTO.getId()))
        {
            this.readonlyAplicacionMovil = Boolean.TRUE;
            this.disableAplicacionMovil = Boolean.FALSE;
        }
        else if (i.getAplicacionMovil().getId().equals(TipoItem.NO_ACTIU.getId()))
        {
            this.readonlyAplicacionMovil = Boolean.FALSE;
            this.disableAplicacionMovil = Boolean.TRUE;
        }
        else
        {
            this.readonlyAplicacionMovil = Boolean.FALSE;
            this.disableAplicacionMovil = Boolean.FALSE;
        }

        if (i.getCorreo().getId().equals(TipoItem.NO_MODIFICABLE.getId())
                || i.getCorreo().getId().equals(TipoItem.NO_MODIFICABLE_DEFECTO.getId()))
        {
            this.readonlyCorreo = Boolean.TRUE;
            this.disableCorreo = Boolean.FALSE;
        }
        else if (i.getCorreo().getId().equals(TipoItem.NO_ACTIU.getId()))
        {
            this.disableCorreo = Boolean.TRUE;
            this.readonlyCorreo = Boolean.FALSE;
        }
        else
        {
            this.readonlyCorreo = Boolean.FALSE;
            this.disableCorreo = Boolean.FALSE;
        }

        if (i.getCorreoPostal().getId().equals(TipoItem.NO_MODIFICABLE.getId())
                || i.getCorreoPostal().getId().equals(TipoItem.NO_MODIFICABLE_DEFECTO.getId()))
        {
            this.readonlyCorreoPostal = Boolean.TRUE;
            this.disableCorreoPostal = Boolean.FALSE;
        }
        else if (i.getCorreoPostal().getId().equals(TipoItem.NO_ACTIU.getId()))
        {
            this.readonlyCorreoPostal = Boolean.FALSE;
            this.disableCorreoPostal = Boolean.TRUE;
        }
        else
        {
            this.readonlyCorreoPostal = Boolean.FALSE;
            this.disableCorreoPostal = Boolean.FALSE;
        }
    }

    public static List<ItemUI> toUI(List<Item> l, String idioma) {
        return l.stream()
                .map(a -> new ItemUI(a, idioma))
                .collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getDisableAplicacionMovil() {
        return disableAplicacionMovil;
    }

    public void setDisableAplicacionMovil(Boolean disableAplicacionMovil) {
        this.disableAplicacionMovil = disableAplicacionMovil;
    }

    public Boolean getReadonlyAplicacionMovil() {
        return readonlyAplicacionMovil;
    }

    public void setReadonlyAplicacionMovil(Boolean readonlyAplicacionMovil) {
        this.readonlyAplicacionMovil = readonlyAplicacionMovil;
    }

    public Boolean getDisableCorreo() {
        return disableCorreo;
    }
    public Boolean isDisableCorreo() {
        return disableCorreo;
    }

    public void setDisableCorreo(Boolean disableCorreo) {
        this.disableCorreo = disableCorreo;
    }

    public Boolean getReadonlyCorreo() {
        return readonlyCorreo;
    }

    public void setReadonlyCorreo(Boolean readonlyCorreo) {
        this.readonlyCorreo = readonlyCorreo;
    }

    public Boolean getDisableCorreoPostal() {
        return disableCorreoPostal;
    }

    public void setDisableCorreoPostal(Boolean disableCorreoPostal) {
        this.disableCorreoPostal = disableCorreoPostal;
    }

    public Boolean getReadonlyCorreoPostal() {
        return readonlyCorreoPostal;
    }

    public void setReadonlyCorreoPostal(Boolean readonlyCorreoPostal) {
        this.readonlyCorreoPostal = readonlyCorreoPostal;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getOrdenSuscriptor() {
        return ordenSuscriptor;
    }

    public void setOrdenSuscriptor(Integer ordenSuscriptor) {
        this.ordenSuscriptor = ordenSuscriptor;
    }

    public Integer getOrdenGrupo() {
        return ordenGrupo;
    }

    public void setOrdenGrupo(Integer ordenGrupo) {
        this.ordenGrupo = ordenGrupo;
    }
}