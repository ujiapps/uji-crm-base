package es.uji.apps.crm.ui;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.uji.apps.crm.model.ClienteEstudio;
import es.uji.commons.rest.ParamUtils;

public class ClienteEstudioUI {
    private String tipo;
    private String nombre;

    public ClienteEstudioUI(ClienteEstudio i, String idioma) {
        if (ParamUtils.isNotNull(i))
        {
            this.tipo = i.getTipo();
            switch (idioma)
            {
                case "es":
                    this.nombre = i.getEstudio();
                    break;
                case "en":
                    this.nombre = i.getEstudioUk();
                    break;
                default:
                    this.nombre = i.getEstudioCa();
                    break;
            }
        }
        else
        {
            this.tipo = null;
            this.nombre = null;
        }
    }

    public static Map<String, List<ClienteEstudioUI>> toUI(List<ClienteEstudio> l, String idioma) {
        if (ParamUtils.isNotNull(l))
        {
            return l.stream()
                    .map(a -> new ClienteEstudioUI(a, idioma))
                    .collect(Collectors.groupingBy(ClienteEstudioUI::getTipo));
        }
        else
        {
            return null;
        }
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}