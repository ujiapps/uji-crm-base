package es.uji.apps.crm.ui;

import com.mysema.query.annotations.QueryProjection;

import java.io.Serializable;

public class ClienteDatoOpcionUI implements Serializable {

    private Long id;
    private String etiqueta;
    private Long valor;
    private String referencia;

    public ClienteDatoOpcionUI() {
    }

    @QueryProjection
    public ClienteDatoOpcionUI(Long id, String etiqueta, Long valor, String referencia) {
        this.id = id;
        this.etiqueta = etiqueta;
        this.valor = valor;
        this.referencia = referencia;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
}
