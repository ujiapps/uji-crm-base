package es.uji.apps.crm.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.crm.model.Item;
import es.uji.commons.rest.ParamUtils;

public class SuscriptoresBajaMensajeriaUI {
    private Long id;
    private String nombre;
    private String descripcion;
    private String grupo;
    private Boolean checked;

    public SuscriptoresBajaMensajeriaUI(Item i, String idioma) {
        this.id = i.getId();
        switch (idioma)
        {
            case "es":
                this.nombre = i.getNombreEs();
                this.grupo = (i.getGrupo() != null) ? i.getGrupo().getNombreEs() + (ParamUtils.isNotNull(i.getGrupo().getDescripcionEs()) ? "<div class='field-hint'>" + i.getGrupo().getDescripcionEs() + "</div  >" : "") : "";
                this.descripcion = i.getDescripcionEs();

                break;
            case "en":
                this.nombre = i.getNombreUk();
                this.grupo = (i.getGrupo() != null) ? i.getGrupo().getNombreUk() + (ParamUtils.isNotNull(i.getGrupo().getDescripcionUk()) ? "<div class='field-hint'>" + i.getGrupo().getDescripcionUk() + "</div>" : "") : "";
                this.descripcion = i.getDescripcionUk();

                break;
            default:
                this.nombre = i.getNombreCa();
                this.grupo = (i.getGrupo() != null) ? i.getGrupo().getNombreCa() + (ParamUtils.isNotNull(i.getGrupo().getDescripcionCa()) ? "<div class='field-hint'>" + i.getGrupo().getDescripcionCa() + "</div>" : "") : "";
                this.descripcion = i.getDescripcionCa();

                break;
        }

    }

    public static List<ItemUI> toUI(List<Item> l, String idioma) {
        return l.stream()
                .map(a -> new ItemUI(a, idioma))
                .collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }
}