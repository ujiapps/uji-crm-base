package es.uji.apps.crm.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.crm.model.TipoEstudio;

public class TipoEstudioUI {
    private Long id;
    private String nombre;

    public TipoEstudioUI(TipoEstudio i, String idioma) {
        this.id = i.getId();
        switch (idioma)
        {
            case "es":
                this.nombre = i.getNombreEs();
                break;
            case "en":
                this.nombre = i.getNombreUk();
                break;
            default:
                this.nombre = i.getNombreCa();
                break;
        }
    }

    public static List<TipoEstudioUI> toUI(List<TipoEstudio> l, String idioma) {
        return l.stream()
                .map(a -> new TipoEstudioUI(a, idioma))
                .collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}