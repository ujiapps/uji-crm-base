package es.uji.apps.crm.ui;

import java.text.ParseException;
import java.util.Date;

import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.crm.model.CampanyaSeguimiento;
import es.uji.apps.crm.model.Envio;
import es.uji.apps.crm.model.PersonaPasPdi;
import es.uji.apps.crm.model.Programa;
import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.model.domains.TipoCorreoEnvio;
import es.uji.apps.crm.model.domains.TipoEnvio;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

public class EnvioUI {

    private Envio envio;

    public EnvioUI(Envio envioParam, MultivaluedMap<String, String> params, Long enviar)
            throws ParseException {

        this.setEnvio(envioParam);

        String format = "dd/MM/yyyyHH:mm";

        envio.setNombre(params.getFirst("nombre"));
        envio.setCuerpo(params.getFirst("cuerpo"));
        envio.setAsunto(params.getFirst("asunto"));
        envio.setDesde(params.getFirst("desde"));
        envio.setResponder(params.getFirst("responder"));
        envio.setTipoCorreoEnvio(ParamUtils.parseLong(params.getFirst("tipoCorreoEnvio")));
        if (ParamUtils.isNotNull(params.getFirst("programaId"))) {
            Programa programa = new Programa();
            programa.setId(ParamUtils.parseLong(params.getFirst("programaId")));
            envio.setPrograma(programa);
        } else {
            envio.setPrograma(null);
        }

        if (ParamUtils.isNotNull(params.getFirst("fechaEnvioDestinatario"))) {
            String fecha;
            if (ParamUtils.isNotNull(params.getFirst("horaEnvioDestinatario"))) {
                fecha = params.getFirst("fechaEnvioDestinatario").concat(
                        params.getFirst("horaEnvioDestinatario"));
            } else {
                fecha = params.getFirst("fechaEnvioDestinatario").concat("00:00");
            }
            envio.setFechaEnvioDestinatario(UtilsService.fechaParse(fecha, format));
        } else if (enviar == 1L) {
            envio.setFechaEnvioDestinatario(new Date());
        } else {
            envio.setFechaEnvioDestinatario(null);
        }

        if (ParamUtils.isNotNull(params.getFirst("fechaFinVigencia"))) {
            String fechaFinVigencia;
            if (ParamUtils.isNotNull(params.getFirst("horaFinVigencia"))) {
                fechaFinVigencia = params.getFirst("fechaFinVigencia").concat(
                        params.getFirst("horaFinVigencia"));
            } else {
                fechaFinVigencia = params.getFirst("fechaFinVigencia").concat("00:00");
            }
            envio.setFechaFinVigencia(UtilsService.fechaParse(fechaFinVigencia, format));
        } else {
            envio.setFechaFinVigencia(null);
        }
    }

    public EnvioUI(Long connectedUserId, FormDataMultiPart multiPart) {

        this.setEnvio(new Envio());

        Tipo tipo = new Tipo();
        tipo.setId(TipoEnvio.EMAIL.getId());
        envio.setEnvioTipo(tipo);

        PersonaPasPdi persona = new PersonaPasPdi();
        persona.setId(connectedUserId);
        envio.setPersona(persona);

        envio.setFecha(new Date());

        envio.setNombre("Envio Manual");
        envio.setCuerpo(multiPart.getField("cuerpoForm").getValue());
        envio.setAsunto(multiPart.getField("asunto").getValue());
        envio.setDesde(multiPart.getField("desde").getValue());
        envio.setResponder(multiPart.getField("responder").getValue());
        envio.setTipoCorreoEnvio(TipoCorreoEnvio.PERSONAL.getId());

        envio.setFechaEnvioDestinatario(new Date());
        envio.setFechaFinVigencia(null);
        envio.setFechaEnvioPlataforma(new Date());
    }

    public EnvioUI(Long connectedUserId, UIEntity entity) throws ParseException {

        this.setEnvio(entity.toModel(Envio.class));

        if (ParamUtils.isNotNull(entity.get("envioId")) && !entity.get("envioId").equals("")) {
            envio.setId(ParamUtils.parseLong(entity.get("envioId")));
        }
        PersonaPasPdi user = new PersonaPasPdi();
        user.setId(connectedUserId);
        envio.setPersona(user);

        String stringFecha = entity.get("fecha");
        if (stringFecha != null && !stringFecha.isEmpty()) {
            Date fecha = UtilsService.fechaParse(stringFecha, "dd/MM/yyyy HH:mm:ss");
            envio.setFecha(fecha);
        } else {
            envio.setFecha(new Date());
        }

        if (ParamUtils.isNotNull(entity.get("envioTipoId")) && !entity.get("envioTipoId").equals("")) {
            Tipo tipo = new Tipo();
            tipo.setId(ParamUtils.parseLong(entity.get("envioTipoId")));
            envio.setEnvioTipo(tipo);
        }

        if (ParamUtils.isNotNull(envio.getSeguimiento())) {
            CampanyaSeguimiento campanyaSeguimiento = new CampanyaSeguimiento();
            campanyaSeguimiento.setId(envio.getSeguimiento().getId());
            envio.setSeguimiento(campanyaSeguimiento);
        }

        String stringFechaEnvioDestinatario = entity.get("fechaEnvioDestinatario");
        if (stringFechaEnvioDestinatario != null && !stringFechaEnvioDestinatario.isEmpty()) {
            Date fecha = UtilsService.fechaParse(stringFechaEnvioDestinatario,
                    "dd/MM/yyyy HH:mm:ss");
            envio.setFechaEnvioDestinatario(fecha);
        }

        String stringFechaFinVigencia = entity.get("fechaFinVigencia");
        if (stringFechaFinVigencia != null && !stringFechaFinVigencia.isEmpty()) {
            Date fecha = UtilsService.fechaParse(stringFechaFinVigencia, "dd/MM/yyyy HH:mm:ss");
            envio.setFechaFinVigencia(fecha);
        }

        String stringFechaEnvioPlataforma = entity.get("fechaEnvioPlataforma");
        if (stringFechaEnvioPlataforma != null && !stringFechaEnvioPlataforma.isEmpty()) {
            Date fecha = UtilsService.fechaParse(stringFechaEnvioPlataforma, "dd/MM/yyyy HH:mm:ss");
            envio.setFechaEnvioPlataforma(fecha);
        } else {
            envio.setFechaEnvioPlataforma(null);
        }
    }

    public Envio getEnvio() {
        return envio;
    }

    public void setEnvio(Envio envio) {
        this.envio = envio;
    }
}