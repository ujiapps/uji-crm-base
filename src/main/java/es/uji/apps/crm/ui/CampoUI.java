package es.uji.apps.crm.ui;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.crm.model.domains.TipoDato;
import es.uji.commons.rest.ParamUtils;

public class CampoUI {
    private Long tipoDato;
    private String etiqueta;
    private String nombre;
    private String valor;
    private String placeHolder;
    private Boolean requerido;
    private Boolean readOnly;
    private Boolean vinculadoCampanya;
    private Boolean visible;
    private List<OpcionUI> opciones;
    private String ayuda;
    private Long campanyaDatoId;
    private String model;
    private Long clienteDatoId;
    private List<CampoUI> camposDependientes;
    private String tipoValidacion;


    public CampoUI(Long tipoDato, String nombre, String valor) {
        this.tipoDato = tipoDato;
        this.nombre = nombre;
        this.requerido = false;
        this.valor = valor;
        this.placeHolder = null;
        this.readOnly = false;
        this.vinculadoCampanya = false;
        this.visible = true;
        this.camposDependientes = new ArrayList<>();
    }

    public CampoUI(String etiqueta, Long tipoDato, String nombre) {
        this.etiqueta = etiqueta;
        this.tipoDato = tipoDato;
        this.nombre = nombre;
        this.requerido = false;
        this.valor = null;
        this.placeHolder = null;
        this.readOnly = false;
        this.vinculadoCampanya = false;
        this.visible = true;
        this.camposDependientes = new ArrayList<>();
    }

    public CampoUI(String etiqueta, Long tipoDato, String nombre, String valor, Boolean requerido) {
        this.etiqueta = etiqueta;
        this.tipoDato = tipoDato;
        this.nombre = nombre;
        this.valor = valor;
        this.placeHolder = null;
        this.requerido = requerido;
        this.vinculadoCampanya = false;
        this.visible = true;
        this.camposDependientes = new ArrayList<>();
    }

    public CampoUI(String etiqueta, Long tipoDato, String nombre, Boolean requerido, Boolean readOnly, String model, String valor) {
        this.etiqueta = etiqueta;
        this.tipoDato = tipoDato;
        this.nombre = nombre;
        this.valor = valor;
        this.placeHolder = null;
        this.requerido = requerido;
        this.vinculadoCampanya = false;
        this.visible = true;
        this.readOnly = readOnly;
        this.model = model;
        this.camposDependientes = new ArrayList<>();
    }
    public CampoUI(String etiqueta, Long tipoDato, String nombre, List<OpcionUI> opcionesDato, Boolean requerido, Boolean readOnly, String model, String valor) {
        this.etiqueta = etiqueta;
        this.tipoDato = tipoDato;
        this.nombre = nombre;
        this.valor = valor;
        this.placeHolder = null;
        this.requerido = requerido;
        this.vinculadoCampanya = false;
        this.visible = true;
        this.readOnly = readOnly;
        this.opciones = opcionesDato;
        this.model = model;
        this.camposDependientes = new ArrayList<>();
    }

    public CampoUI(String etiqueta, Long tipoDato, String nombre, List<OpcionUI> opcionesDato, Boolean requerido, Boolean readOnly, String model, String valor, Long campanyaDatoId) {
        this.etiqueta = etiqueta;
        this.tipoDato = tipoDato;
        this.nombre = nombre;
        this.valor = valor;
        this.placeHolder = null;
        this.requerido = requerido;
        this.vinculadoCampanya = false;
        this.visible = true;
        this.readOnly = readOnly;
        this.opciones = opcionesDato;
        this.model = model;
        this.campanyaDatoId = campanyaDatoId;
        this.camposDependientes = new ArrayList<>();
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getPlaceHolder() {
        return placeHolder;
    }

    public void setPlaceHolder(String placeHolder) {
        this.placeHolder = placeHolder;
    }

    public List<OpcionUI> getOpciones() {
        return opciones;
    }

    public void setOpciones(List<OpcionUI> opciones) {
        this.opciones = opciones;
    }

    public Boolean getRequerido() {
        return requerido;
    }

    public void setRequerido(Boolean requerido) {
        this.requerido = requerido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    public String getAyuda() {
        return ayuda;
    }

    public void setAyuda(String ayuda) {
        this.ayuda = ayuda;
    }

    public boolean hasAyuda() {
        return (ayuda != null && !ayuda.isEmpty());
    }

    public Long getCampanyaDatoId() {
        return campanyaDatoId;
    }

    public void setCampanyaDatoId(Long campanyaDatoId) {
        this.campanyaDatoId = campanyaDatoId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Boolean getVinculadoCampanya() {
        return vinculadoCampanya;
    }

    public void setVinculadoCampanya(Boolean vinculadoCampanya) {
        this.vinculadoCampanya = vinculadoCampanya;
    }

    public Long getTipoDato() {
        return tipoDato;
    }

    public void setTipoDato(Long tipoDato) {
        this.tipoDato = tipoDato;
    }

    public Long getClienteDatoId() {
        return clienteDatoId;
    }

    public void setClienteDatoId(Long clienteDatoId) {
        this.clienteDatoId = clienteDatoId;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String getTipoValidacion() {
        return tipoValidacion;
    }

    public void setTipoValidacion(String tipoValidacion) {
        this.tipoValidacion = tipoValidacion;
    }

    public String getTemplate() {
        if (tipoDato.equals(TipoDato.NUMERO.getId()))
        {
            if (ParamUtils.isNotNull(tipoValidacion) && tipoValidacion.equals("Año"))
            {
                return "crm/form/anyo";
            }
            return "crm/form/numero";
        }
        if (tipoDato.equals(TipoDato.TELEFONO.getId()))
        {
            return "crm/form/telefono";
        }
        if (tipoDato.equals(TipoDato.EMAIL.getId()))
        {
            return "crm/form/email";
        }
        if (tipoDato.equals(TipoDato.SELECCION.getId()) || tipoDato.equals(TipoDato.ITEMS_SELECCION_UNICA_DESPL.getId()))
        {
            return "crm/form/seleccion";
        }
        if (tipoDato.equals(TipoDato.FECHA.getId()))
        {
            return "crm/form/fecha";
        }
        if (tipoDato.equals(TipoDato.TEXTO_LARGO.getId()))
        {
            return "crm/form/textolargo";
        }
        if (tipoDato.equals(TipoDato.CABECERA.getId()))
        {
            return "crm/form/cabecera";
        }
        if (tipoDato.equals(TipoDato.ENUNCIADO.getId()))
        {
            return "crm/form/enunciado";
        }
        if (tipoDato.equals(TipoDato.BOOLEANO.getId()))
        {
            return "crm/form/booleano";
        }
        if (tipoDato.equals(TipoDato.TEXTO_LOPD.getId()))
        {
            return "crm/form/textoLOPD";
        }
        if (tipoDato.equals(TipoDato.ITEMS_SELECCION_MULTIPLE.getId()) && camposDependientes.size() == 0)
        {
            return "crm/form/seleccionmultiple";
        }
        if (tipoDato.equals(TipoDato.ITEMS_SELECCION_MULTIPLE.getId()) && camposDependientes.size() > 0)
        {
            return "crm/form/seleccionmultipledependientes";
        }
        if (tipoDato.equals(TipoDato.ITEMS_SELECCION_MULTIPLE_CHECKS.getId()))
        {
            return "crm/form/checks";
        }
        if (tipoDato.equals(TipoDato.ITEMS_SELECCION_UNICA_CHECKS.getId()))
        {
            return "crm/form/radio";
        }
        if (tipoDato.equals(TipoDato.BINARIO.getId()))
        {
            return "crm/form/fichero";
        }
        if (ParamUtils.isNotNull(tipoValidacion))
        {
            if (tipoDato.equals(TipoDato.TEXTO.getId()) && tipoValidacion.equals("IBAN"))
            {
                return "crm/form/iban";
            }
            if (tipoDato.equals(TipoDato.TEXTO.getId()) && (tipoValidacion.equals("IDENTIFICACION")))
            {
                return "crm/form/identificacion";
            }
            if (tipoDato.equals(TipoDato.TEXTO.getId()) && tipoValidacion.equals("CIF"))
            {
                return "crm/form/cif";
            }
            if (tipoDato.equals(TipoDato.TEXTO.getId()) && tipoValidacion.equals("PREFIJO"))
            {
                return "crm/form/prefijo";
            }
        }

        return "crm/form/texto";
    }

    public List<CampoUI> getCamposDependientes() {
        return camposDependientes;
    }

    public void setCamposDependientes(List<CampoUI> camposDependientes) {
        this.camposDependientes = camposDependientes;
    }

    public void addCampoDependiente(CampoUI campoUI) {
        this.camposDependientes.add(campoUI);
    }
}