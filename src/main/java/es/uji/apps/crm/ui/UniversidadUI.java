package es.uji.apps.crm.ui;

import java.util.List;
import java.util.stream.Collectors;

import com.mysema.query.annotations.QueryProjection;
import es.uji.apps.crm.model.Universidad;

public class UniversidadUI {
    private Long id;
    private String nombre;

    @QueryProjection
    public UniversidadUI(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

//    public UniversidadUI(Universidad i, String idioma) {
//        this.id = i.getId();
//        switch (idioma)
//        {
//            case "es":
//                this.nombre = i.getNombreEs();
//                break;
//            case "en":
//                this.nombre = i.getNombreUk();
//                break;
//            default:
//                this.nombre = i.getNombreCa();
//                break;
//        }
//    }
//
//    public static List<UniversidadUI> toUI(List<Universidad> l, String idioma) {
//        return l.stream()
//                .map(a -> new UniversidadUI(a, idioma))
//                .collect(Collectors.toList());
//    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}