package es.uji.apps.crm.ui;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonaUI implements Serializable {
    private Long id;

    private String identificacion;

    private String nombre;

    private String apellidos;

    private String apellidosNombre;
    private String correo;
    private String correoOficial;


    private String movil;

    private String postal;

    private String cuenta;

    private String busqueda;

    public PersonaUI() {
    }

    public PersonaUI(Long id, String identificacion, String nombre, String apellidos, String apellidosNombre, String correo, String correoOficial, String movil, String postal, String cuenta, String busqueda) {
        this.id = id;
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.apellidosNombre = apellidosNombre;
        this.correo = correo;
        this.correoOficial = correoOficial;
        this.movil = movil;
        this.postal = postal;
        this.cuenta = cuenta;
        this.busqueda = busqueda;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCorreoOficial() {
        return correoOficial;
    }

    public void setCorreoOficial(String correoOficial) {
        this.correoOficial = correoOficial;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public String getApellidosNombre() {
        return apellidosNombre;
    }

    public void setApellidosNombre(String apellidosNombre) {
        this.apellidosNombre = apellidosNombre;
    }
}