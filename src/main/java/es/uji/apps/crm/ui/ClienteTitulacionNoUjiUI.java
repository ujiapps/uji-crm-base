package es.uji.apps.crm.ui;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.mysema.query.annotations.QueryProjection;
import es.uji.apps.crm.model.ClasificacionEstudio;
import es.uji.apps.crm.model.ClienteTitulacionesNoUJI;
import es.uji.apps.crm.model.TipoEstudio;
import es.uji.apps.crm.model.Universidad;
import es.uji.apps.crm.model.domains.TipoEstudioNoUJI;

public class ClienteTitulacionNoUjiUI {
    private Long id;
    private String tipo;
    private String nombre;
    private String campoLibre;
    private String ambito;
    private String anyoFinalizacion;
    private String universidad;

    public ClienteTitulacionNoUjiUI(ClienteTitulacionesNoUJI i, String idioma) {
        this.id = i.getId();
        this.campoLibre = i.getNombre();
        this.anyoFinalizacion = i.getAnyoFinalizacion();

        Long tipoEstudioId = i.getTipoEstudio();

        if (tipoEstudioId.equals(TipoEstudioNoUJI.GRADO.getId())) {
            this.tipo = TipoEstudioNoUJI.GRADO.getNombre();
        } else if (tipoEstudioId.equals(TipoEstudioNoUJI.POSTGRADO.getId())) {
            this.tipo = TipoEstudioNoUJI.POSTGRADO.getNombre();
        } else if (tipoEstudioId.equals(TipoEstudioNoUJI.DOCTORADO.getId())) {
            this.tipo = TipoEstudioNoUJI.DOCTORADO.getNombre();
        } else if (tipoEstudioId.equals(TipoEstudioNoUJI.ELEMENTAL.getId())) {
            this.tipo = TipoEstudioNoUJI.ELEMENTAL.getNombre();
            return;
        } else if (tipoEstudioId.equals(TipoEstudioNoUJI.MEDIOS.getId())) {
            this.tipo = TipoEstudioNoUJI.MEDIOS.getNombre();
        } else if (tipoEstudioId.equals(TipoEstudioNoUJI.SUPERIORESNOUNI.getId())) {
            this.tipo = TipoEstudioNoUJI.SUPERIORESNOUNI.getNombre();
        }

        if (i.getClasificacion() != null) {
            switch (idioma) {
                case "es":
                    this.ambito = i.getClasificacion().getNombreEs();
                    break;
                case "en":
                    this.ambito = i.getClasificacion().getNombreUk();
                    break;
                default:
                    this.ambito = i.getClasificacion().getNombreCa();
                    break;
            }
        }

        if (i.getUniversidad() != null) {
            switch (idioma) {
                case "es":
                    this.universidad = i.getUniversidad().getNombreEs();
                    break;
                case "en":
                    this.universidad = i.getUniversidad().getNombreUk();
                    break;
                default:
                    this.universidad = i.getUniversidad().getNombreCa();
                    break;
            }
        }
    }

    public static Map<String, List<ClienteTitulacionNoUjiUI>> toUI(List<ClienteTitulacionesNoUJI> l, String idioma) {
        return l.stream()
                .map(a -> new ClienteTitulacionNoUjiUI(a, idioma))
                .collect(Collectors.groupingBy(ClienteTitulacionNoUjiUI::getTipo));
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAmbito() {
        return ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }

    public String getAnyoFinalizacion() {
        return anyoFinalizacion;
    }

    public void setAnyoFinalizacion(String anyoFinalizacion) {
        this.anyoFinalizacion = anyoFinalizacion;
    }

    public String getUniversidad() {
        return universidad;
    }

    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    public String getCampoLibre() {
        return campoLibre;
    }

    public void setCampoLibre(String campoLibre) {
        this.campoLibre = campoLibre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}