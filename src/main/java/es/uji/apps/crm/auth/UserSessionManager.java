package es.uji.apps.crm.auth;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteGeneral;
import es.uji.apps.crm.services.ClienteGeneralService;
import es.uji.apps.crm.services.ClienteService;
import es.uji.apps.crm.services.SesionService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.sso.AccessManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserSessionManager {


    private final SesionService sesionService;
    private final ClienteGeneralService clienteGeneralService;
    private final ClienteService clienteService;

    private Long connectedUserId = -1L;
    private String hash;
    private ClienteGeneral clienteGeneral = null;
    private Cliente cliente = null;

    @Autowired
    public UserSessionManager(SesionService sesionService, ClienteGeneralService clienteGeneralService, ClienteService clienteService) {
        this.sesionService = sesionService;
        this.clienteGeneralService = clienteGeneralService;
        this.clienteService = clienteService;
    }

    public void init(HttpServletRequest request, String hash, Boolean zonaPrivada) {
        if (hash != null && sesionService.isValidSession(hash)) {
            setHash(hash);

            Cliente cliente = sesionService.getUserIdActiveSesion(hash, zonaPrivada);
            if (ParamUtils.isNotNull(cliente)) {
                this.setCliente(cliente);
                this.setClienteGeneral(this.getCliente().getClienteGeneral());
                if (ParamUtils.isNotNull(this.getCliente().getClienteGeneral().getPersona())) {
                    this.setConnectedUserId(this.getCliente().getClienteGeneral().getPersona().getId());
                } else {
                    this.setConnectedUserId(-1L);
                }
            }
        } else if (ParamUtils.isNotNull(AccessManager.getConnectedUserId(request))) {
            setHash(null);
            this.setConnectedUserId(AccessManager.getConnectedUserId(request));
            ClienteGeneral clienteGeneral = clienteGeneralService.getClienteGeneralByPerId(this.getConnectedUserId());
            if (ParamUtils.isNotNull(clienteGeneral)) {
                this.setClienteGeneral(clienteGeneral);
                this.setCliente(clienteService.getClienteByClienteGeneralId(this.getClienteGeneral().getId(), zonaPrivada));
            } else {
                //Mosrar error usuario logeado pero sin cuenta CRM
                this.setClienteGeneral(null);
                this.setCliente(null);
            }
        }
    }

    public Long getConnectedUserId() {
        return connectedUserId;
    }

    public void setConnectedUserId(Long connectedUserId) {
        this.connectedUserId = connectedUserId;
    }

    public ClienteGeneral getClienteGeneral() {
        return clienteGeneral;
    }

    public void setClienteGeneral(ClienteGeneral clienteGeneral) {
        this.clienteGeneral = clienteGeneral;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getHash() {
        return hash;
    }
}
