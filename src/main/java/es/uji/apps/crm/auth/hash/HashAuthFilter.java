package es.uji.apps.crm.auth.hash;

import es.uji.apps.crm.dao.SesionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@Component
public class HashAuthFilter extends OncePerRequestFilter {
    public static Logger log = LoggerFactory.getLogger(HashAuthFilter.class);

    @Autowired
    private SesionDAO sesionDAO;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String cookie = getCookieValue(httpServletRequest.getCookies(), "p_hash");

        if (!cookie.isEmpty()) {
            if (isValidSession(cookie)) {
                filterChain.doFilter(httpServletRequest, httpServletResponse);
                return;
            }
        }
        log.info("Bad credentials for hash authentication");
        httpServletResponse.sendRedirect("/crm/rest/alumni/inici");// setStatus(HttpServletResponse.SC_FORBIDDEN);
    }

    public String getCookieValue(Cookie[] cookies, String key) {
        return Arrays.stream(cookies)
                .filter(c -> key.equals(c.getName()))
                .map(Cookie::getValue)
                .findFirst().orElse("");
    }

    public Boolean isValidSession(String hash) {
        return sesionDAO.isValidSession(hash);
    }

}
