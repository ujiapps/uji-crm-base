package es.uji.apps.crm.utils;

/**
 * Created with IntelliJ IDEA.
 * User: veriton1
 * Date: 21/05/13
 * Time: 15:11
 * To change this template use File | Settings | File Templates.
 */

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "row")
public class TreeRow {
    @XmlAttribute
    protected String id;
    @XmlAttribute
    protected String title;
    @XmlAttribute
    protected String text;
    @XmlAttribute(required = false)
    protected String leaf;
    @XmlAttribute(required = false)
    protected Boolean checked;
    @XmlElement(name = "row", required = false)
    protected List<TreeRow> hijos;

    public TreeRow() {
        hijos = new ArrayList<TreeRow>();
    }

    public String getId() {
        return id;
    }

    public void setId(String value) {
        this.id = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String value) {
        this.title = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String value) {
        this.text = value;
    }

    public String getLeaf() {
        return leaf;
    }

    public void setLeaf(String value) {
        this.leaf = value;
    }

    public List<TreeRow> getHijos() {
        return hijos;
    }

    public void setHijos(List<TreeRow> hijos) {
        this.hijos = hijos;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }
}