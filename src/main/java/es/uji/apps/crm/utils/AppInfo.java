package es.uji.apps.crm.utils;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.Menu;
import es.uji.commons.web.template.model.Pagina;

@Component
public class AppInfo {
    private static String urlBase;
    private static String host;

    public static String getHost() {
        return host;
    }

    @Value("${uji.crm.host}")
    public void setHost(String host) {
        this.host = host;
    }

    @Value("${uji.webapp.urlBase}")
    public void setUrlBase(String urlBase) {
        this.urlBase = urlBase;
    }

    /**
     * Metodo que de vuelve un template generico.
     *
     * @param plantilla
     * @param idioma
     * @return
     * @throws ParseException
     */
    public static Template buildPagina(String plantilla, String idioma) throws ParseException {

        Template template = new HTMLTemplate(plantilla, new Locale(idioma), "crm");
        Pagina pagina = new Pagina(urlBase, urlBase, idioma.toLowerCase(), "CRM");
        pagina.setTitulo("CRM");
        pagina.setMenu(new Menu());

        template.put("urlBase", urlBase);
        template.put("host", host);
        template.put("server", host);
        template.put("page_title", "CRM");
        template.put("idioma", idioma.toLowerCase());
        template.put("pagina", pagina);

        return template;
    }
}