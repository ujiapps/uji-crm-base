package es.uji.apps.crm.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

public class HashUtils {

    public static String stringToHash(String input) {
        String sha1 = null;
        String cad = input.concat("4780125");
        try
        {
            MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
            msdDigest.update(cad.getBytes("UTF-8"), 0, cad.length());
            sha1 = DatatypeConverter.printHexBinary(msdDigest.digest());
        }
        catch (UnsupportedEncodingException | NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return sha1.toLowerCase();
    }

    public static String stringToHashLogin() {
        String sha1 = null;
        String date = new Date().toString();
        String cad = date.concat("sdkskljw");
        try
        {
            MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
            msdDigest.update(cad.getBytes("UTF-8"), 0, cad.length());
            sha1 = DatatypeConverter.printHexBinary(msdDigest.digest()).toLowerCase();
        }
        catch (UnsupportedEncodingException | NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return sha1;
    }
}