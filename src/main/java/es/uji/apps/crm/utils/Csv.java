package es.uji.apps.crm.utils;

import au.com.bytecode.opencsv.CSVWriter;
import org.springframework.stereotype.Service;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Service
public class Csv {

    public String listToCSV(ArrayList<String> cabeceras, List<List<String>> lista) {

        final char DELIMITER = ';';

        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = null;

        try {
            csvWriter = new CSVWriter(writer, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");

            List<String[]> records = new ArrayList<String[]>();
            List<String> record = cabeceras;

            String[] recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);

            for (List<String> elem : lista) {
                String[] recordArrayMov = new String[elem.size()];
                elem.toArray(recordArrayMov);
                records.add(recordArrayMov);
            }
            csvWriter.writeAll(records);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return writer.toString();
    }
}
