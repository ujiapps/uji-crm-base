package es.uji.apps.crm.utils;

/**
 * Created with IntelliJ IDEA.
 * User: veriton1
 * Date: 21/05/13
 * Time: 15:12
 * To change this template use File | Settings | File Templates.
 */

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "rowset")
public class TreeRowset {

    @XmlElement(name = "row")
    protected List<TreeRow> row;

    public List<TreeRow> getRow() {
        if (row == null)
        {
            row = new ArrayList<TreeRow>();
        }
        return this.row;
    }

}