package es.uji.apps.crm.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class SoloSeAdmitenFicherosExcelException extends CoreDataBaseException
{
    public SoloSeAdmitenFicherosExcelException()
    {
        super("Només s'admeten fitxers Excel amb extensions .xls o .xlsx");
    }

    public SoloSeAdmitenFicherosExcelException(String message)
    {
        super(message);
    }
}
