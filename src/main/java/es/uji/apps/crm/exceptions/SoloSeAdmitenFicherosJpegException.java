package es.uji.apps.crm.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class SoloSeAdmitenFicherosJpegException extends CoreDataBaseException
{
    public SoloSeAdmitenFicherosJpegException()
    {
        super("Només s'admeten fitxers JPEG amb extensions .jpg o .jpeg");
    }

    public SoloSeAdmitenFicherosJpegException(String message)
    {
        super(message);
    }
}
