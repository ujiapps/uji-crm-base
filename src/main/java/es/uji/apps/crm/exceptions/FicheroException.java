package es.uji.apps.crm.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class FicheroException extends CoreDataBaseException {
    public FicheroException() {
        super("No s'ha especificat la recerca");
    }

    public FicheroException(String message) {
        super(message);
    }
}