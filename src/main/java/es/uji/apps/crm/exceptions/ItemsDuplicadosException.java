package es.uji.apps.crm.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class ItemsDuplicadosException extends CoreDataBaseException {
    public ItemsDuplicadosException() {
        super("No es pot afegir el item al client perque ya disposa de la informació");
    }

    public ItemsDuplicadosException(String message) {
        super(message);
    }
}