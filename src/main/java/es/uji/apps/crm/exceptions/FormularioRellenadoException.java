package es.uji.apps.crm.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class FormularioRellenadoException extends CoreDataBaseException {
    public FormularioRellenadoException() {
        super("Ya has rellenado el formulario");
    }

    public FormularioRellenadoException(String message) {
        super(message);
    }
}