package es.uji.apps.crm.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class ErrorEnBorradoDeRegistroException extends CoreDataBaseException {
    public ErrorEnBorradoDeRegistroException() {
        super("No s'ha pogut esborrar el registre.");
    }

    public ErrorEnBorradoDeRegistroException(String message) {
        super(message);
    }
}
