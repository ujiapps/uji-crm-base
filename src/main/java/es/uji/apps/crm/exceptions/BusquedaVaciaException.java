package es.uji.apps.crm.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class BusquedaVaciaException extends CoreDataBaseException {
    public BusquedaVaciaException() {
        super("No s'ha especificat la recerca");
    }
    public BusquedaVaciaException(String message) {
        super(message);
    }
}
