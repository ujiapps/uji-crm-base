package es.uji.apps.crm.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class ErrorSubiendoDocumentoException extends CoreDataBaseException {
    public ErrorSubiendoDocumentoException() {
        super("No s'ha pogut pujar el document");
    }

    public ErrorSubiendoDocumentoException(String message) {
        super(message);
    }
}