package es.uji.apps.crm.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.crm.model.Banner;
import es.uji.apps.crm.model.QBanner;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class BannerDAO extends BaseDAODatabaseImpl {
    @Autowired
    private BannerDAO bannerDAO;

    QBanner qBanner = QBanner.banner;

    public List<Banner> getBanners() {
        JPAQuery query = new JPAQuery(entityManager).from(qBanner);
        return query.list(qBanner);
    }

    @Transactional
    public void updateBanner(Banner banner) {
        try
        {
            JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qBanner);

            updateClause.where(qBanner.id.eq(banner.getId()))
                    .set(qBanner.imgAlt, banner.getImgAlt())
                    .set(qBanner.imgUrl, banner.getImgUrl())
                    .set(qBanner.referencia, banner.getReferencia())
                    .set(qBanner.titulo, banner.getTitulo())
                    .set(qBanner.subtitulo, banner.getSubtitulo())
                    .execute();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Transactional
    public void deleteBanner(Long bannerId) {
        JPADeleteClause jpaDeleteClause = new JPADeleteClause(entityManager, qBanner).where(qBanner.id.eq(bannerId));
        jpaDeleteClause.execute();
    }
}
