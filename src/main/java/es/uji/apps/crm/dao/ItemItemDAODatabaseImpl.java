package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.ItemItem;
import es.uji.apps.crm.model.QItemItem;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ItemItemDAODatabaseImpl extends BaseDAODatabaseImpl implements ItemItemDAO {

    @Override
    public List<ItemItem> getItemsItemsByItemId(Item item) {
        QItemItem itemItem = QItemItem.itemItem;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(itemItem).where(itemItem.itemOrigen.id.eq(item.getId()));

        return query.list(itemItem);

    }

    @Override
    public List<ItemItem> getItemsItemsOrigenByItemId(Item item) {
        QItemItem itemItem = QItemItem.itemItem;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(itemItem).where(itemItem.itemDestino.id.eq(item.getId()));

        return query.list(itemItem);
    }
}