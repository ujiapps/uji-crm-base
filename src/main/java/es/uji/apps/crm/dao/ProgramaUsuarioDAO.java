package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.ProgramaUsuario;
import es.uji.commons.db.BaseDAO;

public interface ProgramaUsuarioDAO extends BaseDAO {

//    List<PersonaPasPdi> getPersonas();
//
//    List<ServicioUsuario> getProgramaUsuario();

    List<ProgramaUsuario> getProgramaUsuariosByProgramaId(Long programaId);

    List<ProgramaUsuario> getProgramasUsuariosByPersonaId(Long personaId);

    List<ProgramaUsuario> getProgramasUsuariosByPersonaIdAndTipoAcceso(Long personaId, String tipoAcceso);

    ProgramaUsuario getProgramasUsuarioById(Long programaUsuarioId);

    List<Long> getProgramasIdByPersonaIdAndTipoAcceso(Long personaId, String tipoAcceso);

    List<Long> getProgramasIdByPersonaId(Long personaId);

}