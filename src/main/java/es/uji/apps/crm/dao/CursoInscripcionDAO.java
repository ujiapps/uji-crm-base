package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.CursoInscripcion;
import es.uji.commons.db.BaseDAO;

public interface CursoInscripcionDAO extends BaseDAO {

    List<CursoInscripcion> getCursosInscripcionesByPersonaId(Long connectedUserId, Long personaId);
}