package es.uji.apps.crm.dao;

import java.util.Date;
import java.util.List;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.Movimiento;
import es.uji.commons.db.BaseDAO;

public interface MovimientoDAO extends BaseDAO {
    Movimiento getMovimientoByClienteIdAndCampanyaIdAndTipoId(Long clienteId, Long campanyaId, Long campanyaEnvioTipoId);

    List<Movimiento> getMovimientosByClienteId(Long clienteId);

    List<Movimiento> getMovimientoByClienteIdAndCampanyaId(Long campanyaId, Long clienteId);

    void deleteMovimientosByCampanyaCliente(Campanya campanya, Cliente cliente);

    Date getFechaAlta(Long id, Long campanyaId);

    Date getFechaBaja(Long id, Long campanyaId);
}