package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QTipoEstadoCampanya;
import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoEstadoCampanyaDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoEstadoCampanyaDAO {


    @Override
    public List<TipoEstadoCampanya> getTiposEstadoCampanyaByCampanyaId(Long campanya) {

        QTipoEstadoCampanya tipoEstadoCampanya = QTipoEstadoCampanya.tipoEstadoCampanya;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(tipoEstadoCampanya)
                .where(tipoEstadoCampanya.campanya.id.eq(campanya));

        return query.list(tipoEstadoCampanya);
    }

    @Override
    public TipoEstadoCampanya getTipoEstadoCampanyaById(Long tipoEstadoCampanyaId) {

        QTipoEstadoCampanya tipoEstadoCampanya = QTipoEstadoCampanya.tipoEstadoCampanya;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(tipoEstadoCampanya)
                .where(tipoEstadoCampanya.id.eq(tipoEstadoCampanyaId));

        List<TipoEstadoCampanya> lista = query.list(tipoEstadoCampanya);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

    @Override
    public TipoEstadoCampanya getTipoEstadoCampanyaByNombre(Long campanyaId, String nombreEstado) {

        QTipoEstadoCampanya tipoEstadoCampanya = QTipoEstadoCampanya.tipoEstadoCampanya;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(tipoEstadoCampanya)
                .where(tipoEstadoCampanya.campanya.id.eq(campanyaId)
                        .and(tipoEstadoCampanya.nombre.containsIgnoreCase(nombreEstado)));

        List<TipoEstadoCampanya> lista = query.list(tipoEstadoCampanya);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return new TipoEstadoCampanya();
    }

    @Override
    public TipoEstadoCampanya getTipoEstadoCampanyaByAccion(Long campanyaId, String accion) {

        QTipoEstadoCampanya tipoEstadoCampanya = QTipoEstadoCampanya.tipoEstadoCampanya;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(tipoEstadoCampanya)
                .where(tipoEstadoCampanya.campanya.id.eq(campanyaId)
                        .and(tipoEstadoCampanya.accion.like(accion)));

        List<TipoEstadoCampanya> lista = query.list(tipoEstadoCampanya);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }
}