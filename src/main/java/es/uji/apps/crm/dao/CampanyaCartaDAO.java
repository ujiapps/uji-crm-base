package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.CampanyaCarta;
import es.uji.commons.db.BaseDAO;

public interface CampanyaCartaDAO extends BaseDAO {
    List<CampanyaCarta> getCampanyaCartasByCampanyaId(Long campanyaId);

    CampanyaCarta getCampanyaCartaByCampanyaIdAndTipoId(Long campanyaId, Long tipoId);

    List<CampanyaCarta> getCampanyaCartasByCampanyaIdAndTipoId(Long campanyaId, Long estadoId);
}