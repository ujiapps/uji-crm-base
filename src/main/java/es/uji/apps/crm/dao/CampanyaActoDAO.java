package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaActo;
import es.uji.commons.db.BaseDAO;

public interface CampanyaActoDAO extends BaseDAO {
    List<CampanyaActo> getCampanyasActoByCampanya(Campanya campanya);

    List<CampanyaActo> getCampanyasActoRss();

    CampanyaActo getCampanyaActoById(Long campanyaActoId);

    void activarRssEvento(Long actoId);
}