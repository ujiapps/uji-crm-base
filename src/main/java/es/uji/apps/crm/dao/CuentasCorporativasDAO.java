package es.uji.apps.crm.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.CuentaCorportativa;
import es.uji.apps.crm.model.QCuentaCorportativa;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CuentasCorporativasDAO extends BaseDAODatabaseImpl {
    QCuentaCorportativa qCuentaCorportativa = QCuentaCorportativa.cuentaCorportativa;

    public List<CuentaCorportativa> getCuentasCorporativasByUserId(Long userId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qCuentaCorportativa).where(qCuentaCorportativa.persona.id.eq(userId)).list(qCuentaCorportativa);
    }

    public List<CuentaCorportativa> getCuentasCorporativasActivasByUserId(Long userId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qCuentaCorportativa)
                .where(qCuentaCorportativa.persona.id.eq(userId)
                        .and((qCuentaCorportativa.fin.isNull()
                                .or(qCuentaCorportativa.fin.goe(new Date()))))).list(qCuentaCorportativa);
    }
}