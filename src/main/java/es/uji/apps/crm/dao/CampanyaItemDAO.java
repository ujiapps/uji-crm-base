package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.CampanyaItem;
import es.uji.commons.db.BaseDAO;

public interface CampanyaItemDAO extends BaseDAO {
    List<CampanyaItem> getCampanyaItemsByCampanyaId(Long campanyaId);

    CampanyaItem getCampanyaItemById(Long campanyaItemId);
}