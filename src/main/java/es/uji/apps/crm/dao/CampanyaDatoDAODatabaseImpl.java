package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.CampanyaDato;
import es.uji.apps.crm.model.QCampanyaDato;
import es.uji.apps.crm.model.QCampanyaFormulario;
import es.uji.apps.crm.model.QClienteDatoTipo;
import es.uji.apps.crm.model.QGrupo;
import es.uji.apps.crm.model.QItem;
import es.uji.apps.crm.model.QTipoValidacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CampanyaDatoDAODatabaseImpl extends BaseDAODatabaseImpl implements CampanyaDatoDAO {

    @Override
    public CampanyaDato getCampanyaDatoById(Long campanyaDatoId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaDato qCampanyaDato = QCampanyaDato.campanyaDato1;
        QClienteDatoTipo clienteDatoTipo = QClienteDatoTipo.clienteDatoTipo;
        QCampanyaFormulario campanyaFormulario = QCampanyaFormulario.campanyaFormulario;
        QItem qItem = QItem.item;
        QGrupo qGrupo = QGrupo.grupo;

        query.from(qCampanyaDato)
                .join(qCampanyaDato.clienteDatoTipo, clienteDatoTipo).fetch()
                .leftJoin(qCampanyaDato.item, qItem).fetch()
                .leftJoin(qCampanyaDato.campanyaFormulario, campanyaFormulario).fetch()
                .leftJoin(qCampanyaDato.grupo, qGrupo)
                .fetch()
                .where(qCampanyaDato.id.eq(campanyaDatoId));

        List<CampanyaDato> lista = query.list(qCampanyaDato);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

    @Override
    public List<CampanyaDato> getCampanyaDatosPrincipalesByFormularioId(Long formularioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaDato qCampanyaDato = QCampanyaDato.campanyaDato1;
        QCampanyaDato qCampanyaDatosDependientes = new QCampanyaDato("campanyaDatosDependientes");
        QCampanyaDato qCampanyaDatoPadre = new QCampanyaDato("campanyaDatoPadre");
        QClienteDatoTipo qClienteDatoTipo = QClienteDatoTipo.clienteDatoTipo;
        QItem qItem = QItem.item;
        QGrupo qGrupo = QGrupo.grupo;

        query.from(qCampanyaDato)
                .join(qCampanyaDato.clienteDatoTipo, qClienteDatoTipo).fetch()
                .leftJoin(qCampanyaDato.item, qItem).fetch().leftJoin(qCampanyaDato.grupo, qGrupo)
                .leftJoin(qCampanyaDato.campanyaDatos, qCampanyaDatosDependientes).fetch()
                .leftJoin(qCampanyaDato.campanyaDato, qCampanyaDatoPadre).fetch().fetch()
                .where(qCampanyaDato.campanyaFormulario.id.eq(formularioId))
                .distinct()
                .orderBy(qCampanyaDato.orden.asc());

        return query.list(qCampanyaDato);

    }

    @Override
    public List<CampanyaDato> getCampanyaDatosByFormularioId(Long formularioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaDato qCampanyaDato = QCampanyaDato.campanyaDato1;
        QCampanyaDato qCampanyaDatosDependientes = new QCampanyaDato("campanyaDatosDependientes");
        QCampanyaDato qCampanyaDatoPadre = new QCampanyaDato("campanyaDatoPadre");
        QClienteDatoTipo qClienteDatoTipo = QClienteDatoTipo.clienteDatoTipo;
        QItem qItem = QItem.item;
        QGrupo qGrupo = QGrupo.grupo;
        QTipoValidacion tipoValidacion = QTipoValidacion.tipoValidacion;

        query.from(qCampanyaDato)
                .join(qCampanyaDato.clienteDatoTipo, qClienteDatoTipo).fetch()
                .leftJoin(qCampanyaDato.item, qItem).fetch().leftJoin(qCampanyaDato.grupo, qGrupo)
                .leftJoin(qCampanyaDato.campanyaDatos, qCampanyaDatosDependientes).fetch()
                .leftJoin(qCampanyaDato.campanyaDato, qCampanyaDatoPadre).fetch()
                .leftJoin(qCampanyaDato.tipoValidacion, tipoValidacion).fetch()
                .where(qCampanyaDato.campanyaFormulario.id.eq(formularioId))
                .distinct()
                .orderBy(qCampanyaDato.orden.asc()).orderBy(qCampanyaDatosDependientes.orden.asc());

        return query.list(qCampanyaDato);
    }

    @Override
    public List<CampanyaDato> getCampanyaDatoRelacionados(Long campanyaDatoId) {
        QCampanyaDato qCampanyaDato = QCampanyaDato.campanyaDato1;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qCampanyaDato).where(qCampanyaDato.preguntaRelacionada.id.eq(campanyaDatoId)).orderBy(qCampanyaDato.orden.asc());

        return query.list(qCampanyaDato);
    }

    @Override
    public List<CampanyaDato> getCampanyaDatosSinDatoAsociadoByFormularioId(Long campanyaFormularioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaDato qCampanyaDato = QCampanyaDato.campanyaDato1;
        QCampanyaDato qCampanyaDatosDependientes = new QCampanyaDato("campanyaDatosDependientes");
        QCampanyaDato qCampanyaDatoPadre = new QCampanyaDato("campanyaDatoPadre");
        QClienteDatoTipo qClienteDatoTipo = QClienteDatoTipo.clienteDatoTipo;
        QItem qItem = QItem.item;
        QGrupo qGrupo = QGrupo.grupo;
        QTipoValidacion tipoValidacion = QTipoValidacion.tipoValidacion;

        query.from(qCampanyaDato)
                .join(qCampanyaDato.clienteDatoTipo, qClienteDatoTipo).fetch()
                .leftJoin(qCampanyaDato.item, qItem).fetch().leftJoin(qCampanyaDato.grupo, qGrupo)
                .leftJoin(qCampanyaDato.campanyaDatos, qCampanyaDatosDependientes).fetch()
                .leftJoin(qCampanyaDato.campanyaDato, qCampanyaDatoPadre).fetch()
                .leftJoin(qCampanyaDato.tipoValidacion, tipoValidacion).fetch()
                .where(qCampanyaDato.campanyaFormulario.id.eq(campanyaFormularioId)
                        .and(qCampanyaDato.campanyaDato.isNull()))
                .distinct()
                .orderBy(qCampanyaDato.orden.asc()).orderBy(qCampanyaDatosDependientes.orden.asc());

        return query.list(qCampanyaDato);
    }

}