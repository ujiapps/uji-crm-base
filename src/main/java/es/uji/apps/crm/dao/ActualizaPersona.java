package es.uji.apps.crm.dao;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

@Component
public class ActualizaPersona {

    private static DataSource dataSource;
    private GuardaFotoCarnet foto;
    private CreaCuentaCorporativa cuenta;
    private GuardaMovilSuscriptor movilSuscriptor;
    private GuardaCorreoSuscriptor correoSuscriptor;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        ActualizaPersona.dataSource = dataSource;
    }

    @PostConstruct
    public void init() {
        this.foto = new GuardaFotoCarnet(dataSource);
        this.cuenta = new CreaCuentaCorporativa(dataSource);
        this.movilSuscriptor = new GuardaMovilSuscriptor(dataSource);
        this.correoSuscriptor = new GuardaCorreoSuscriptor(dataSource);
    }

    public void guardaFotoCarnet(Long clienteId) {

        foto.guardaFotoCarnet(clienteId);
    }

    public String creaCuentaCorporativa(Long personaId) {
        return cuenta.creaCuentaCorporativa(personaId);
    }

    public void guardaMovilSuscriptor(Long personaId, String movil) {
        movilSuscriptor.guardaMovilSuscriptor(personaId, movil);
    }

    public void guardaCorreoSuscriptor(Long personaId, String correo) {
        correoSuscriptor.guardaCorreoSuscriptor(personaId, correo);
    }

    private class GuardaFotoCarnet extends StoredProcedure {
        private static final String SQL = "uji_crm.guarda_foto_carnet";
        private static final String PCLIENTE = "p_cliente";

        public GuardaFotoCarnet(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(PCLIENTE, Types.BIGINT));
            compile();
        }

        public void guardaFotoCarnet(Long clienteId) {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PCLIENTE, clienteId);

            Map<String, Object> results = execute(inParams);
        }
    }

    private class CreaCuentaCorporativa extends StoredProcedure {
        private static final String SQL = "uji_crm.crea_cuenta";
        private static final String PPERSONA = "p_persona_id";
        private static final String PUSUARIO = "p_usuario";
        private static final String PCLAVE = "p_clave";

        public CreaCuentaCorporativa(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(PPERSONA, Types.BIGINT));
            declareParameter(new SqlOutParameter(PUSUARIO, Types.VARCHAR));
            declareParameter(new SqlOutParameter(PCLAVE, Types.VARCHAR));

            compile();
        }

        public String creaCuentaCorporativa(Long personaId) {

            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PPERSONA, personaId);
            inParams.put(PUSUARIO, "");
            inParams.put(PCLAVE, "");

            Map<String, Object> results = execute(inParams);
            return (String) results.get(PUSUARIO);
        }
    }

    private class GuardaMovilSuscriptor extends StoredProcedure {
        private static final String SQL = "uji_crm.alta_movil_suscriptor";
        private static final String PPERSONA = "p_persona_id";
        private static final String PMOVIL = "p_movil";

        public GuardaMovilSuscriptor(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(PPERSONA, Types.BIGINT));
            declareParameter(new SqlParameter(PMOVIL, Types.VARCHAR));
            compile();
        }

        public void guardaMovilSuscriptor(Long personaId, String movil) {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PPERSONA, personaId);
            inParams.put(PMOVIL, movil);

            Map<String, Object> results = execute(inParams);
        }
    }

    private class GuardaCorreoSuscriptor extends StoredProcedure {
        private static final String SQL = "uji_crm.alta_correo_suscriptor";
        private static final String PPERSONA = "p_persona_id";
        private static final String PCORREO = "p_correo";

        public GuardaCorreoSuscriptor(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(PPERSONA, Types.BIGINT));
            declareParameter(new SqlParameter(PCORREO, Types.VARCHAR));
            compile();
        }

        public void guardaCorreoSuscriptor(Long personaId, String correo) {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PPERSONA, personaId);
            inParams.put(PCORREO, correo);

            Map<String, Object> results = execute(inParams);
        }
    }

}