package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.FiltroCorreoNoValido;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.model.QCorreoNoValidoControl;
import es.uji.apps.crm.model.QVwCorreoNoValido;
import es.uji.apps.crm.model.VwCorreoNoValido;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CorreoNoValidoDAO extends BaseDAODatabaseImpl {

    QVwCorreoNoValido qCorreoNoValido = QVwCorreoNoValido.vwCorreoNoValido;
    QCorreoNoValidoControl correoNoValidoControl = QCorreoNoValidoControl.correoNoValidoControl;
    @Autowired
    private UtilsDAO utilsDAO;

//    Long TODOS = 0L;
//
//    Long Y = 1L;
//    Long O = 2L;
//    Long YNO = 3L;
//    Long ONO = 4L;

//    public void creaQueryFechas(BooleanBuilder build, FiltroCorreoNoValido filtroCorreoNoValido, DateTimePath<Date> fechaPrimerIntento) {

//        for (FiltroFechas filtroFechas : filtroCorreoNoValido.getListaFiltroFechas()) {
//            utilsDAO.creaQueryFecha(build, filtroFechas, fechaPrimerIntento);
//        }
//    }

//    public void creaQueryFiltroModificado(BooleanBuilder build, FiltroCorreoNoValido filtroCorreoNoValido) {
//
//        Long modificado = filtroCorreoNoValido.getModificado();
//        Long operador = filtroCorreoNoValido.getOperador();
//
//        if (!modificado.equals(TODOS)) {
//            if (operador.equals(Y))
//                build.and(correoNoValidoControl.modificado.isTrue());
//
//            if (operador.equals(YNO))
//                build.and(correoNoValidoControl.id.isNull().or(correoNoValidoControl.modificado.isFalse()));
//
//            if (operador.equals(O))
//                build.or(correoNoValidoControl.modificado.isTrue());
//
//            if (operador.equals(ONO))
//                build.or(correoNoValidoControl.id.isNull().or(correoNoValidoControl.modificado.isFalse()));
//        }
//    }

    public List<VwCorreoNoValido> getCorreosNoValidos(FiltroCorreoNoValido filtroCorreoNoValido, Paginacion paginacion, String sort, String dir) {

        JPAQuery query = new JPAQuery(entityManager);
//        BooleanBuilder build = new BooleanBuilder();

        query.from(qCorreoNoValido)
                .leftJoin(qCorreoNoValido.correoNoValidoControl, correoNoValidoControl);


//        creaQueryFechas(build, filtroCorreoNoValido, qCorreoNoValido.fechaPrimerIntento);
//        creaQueryFiltroModificado(build, filtroCorreoNoValido);
        creaQueryFiltroModificado(query, filtroCorreoNoValido);
//        query.where(build);

        if (sort.equals("fechaPrimerIntento"))
        {
            if (dir.equals("DESC"))
            {
                query.orderBy(qCorreoNoValido.fechaPrimerIntento.desc());
            }
            else
            {
                query.orderBy(qCorreoNoValido.fechaPrimerIntento.asc());
            }
        }

        if (sort.equals("fechaEnvio"))
        {
            if (dir.equals("DESC"))
            {
                query.orderBy(qCorreoNoValido.fechaEnvio.desc());
            }
            else
            {
                query.orderBy(qCorreoNoValido.fechaEnvio.asc());
            }
        }

        if (sort.equals("nombre"))
        {
            if (dir.equals("DESC"))
            {
                query.orderBy(qCorreoNoValido.nombre.desc());
            }
            else
            {
                query.orderBy(qCorreoNoValido.nombre.asc());
            }
        }

        if (paginacion != null)
        {
            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        return query.list(qCorreoNoValido);

    }

    public void creaQueryFiltroModificado(JPAQuery query, FiltroCorreoNoValido filtroCorreoNoValido) {

        query.where(qCorreoNoValido.nombre.upper().containsIgnoreCase(filtroCorreoNoValido.getCorreo().toUpperCase()));
//        Long modificado = filtroCorreoNoValido.getModificado();
//        Long operador = filtroCorreoNoValido.getOperador();

//        if (!modificado.equals(TODOS)) {
//            if (operador.equals(Y))
//                build.and(correoNoValidoControl.modificado.isTrue());
//
//            if (operador.equals(YNO))
//                build.and(correoNoValidoControl.id.isNull().or(correoNoValidoControl.modificado.isFalse()));
//
//            if (operador.equals(O))
//                build.or(correoNoValidoControl.modificado.isTrue());
//
//            if (operador.equals(ONO))
//                build.or(correoNoValidoControl.id.isNull().or(correoNoValidoControl.modificado.isFalse()));
//        }
    }

    public VwCorreoNoValido getCorreoNoValido(VwCorreoNoValido correoNoValido) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qCorreoNoValido)
                .leftJoin(qCorreoNoValido.correoNoValidoControl, correoNoValidoControl)
                .where(qCorreoNoValido.eq(correoNoValido));

        List<VwCorreoNoValido> lista = query.list(qCorreoNoValido);

        if (lista.isEmpty())
        {
            return null;
        }
        else
        {
            return lista.get(0);
        }


    }
}