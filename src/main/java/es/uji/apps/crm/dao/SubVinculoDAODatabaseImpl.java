package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QPrograma;
import es.uji.apps.crm.model.QSubVinculo;
import es.uji.apps.crm.model.SubVinculo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class SubVinculoDAODatabaseImpl extends BaseDAODatabaseImpl implements SubVinculoDAO {
    @Override
    public List<SubVinculo> getSubVinculos() {
        JPAQuery query = new JPAQuery(entityManager);
        QSubVinculo subVinculo = QSubVinculo.subVinculo;

        return query.from(subVinculo).list(subVinculo);
    }

    @Override
    public List<SubVinculo> getSubVinculosByPrograma(Long programaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QSubVinculo subVinculo = QSubVinculo.subVinculo;
        QPrograma programa = QPrograma.programa;

        return query.from(subVinculo, programa)
                .where(subVinculo.vinculoId.eq(programa.vinculoId)
                        .and(programa.id.eq(programaId)))
                .list(subVinculo);
    }

    @Override
    public String getSubVinculoNombreById(Long subVinculoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QSubVinculo subVinculo = QSubVinculo.subVinculo;

        query.from(subVinculo).where(subVinculo.id.eq(subVinculoId));
        return query.list(subVinculo.subVinculoNombre).get(0);

    }
}