package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QTipoEstadoCampanyaMotivo;
import es.uji.apps.crm.model.TipoEstadoCampanyaMotivo;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Repository
public class TipoEstadoCampanyaMotivoDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoEstadoCampanyaMotivoDAO {


    @Override
    public List<TipoEstadoCampanyaMotivo> getTiposEstadoCampanyaMotivosByEstadoCampanyaId(Long estadoCampanya) {

        if (ParamUtils.isNotNull(estadoCampanya))
        {

            QTipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivo = QTipoEstadoCampanyaMotivo.tipoEstadoCampanyaMotivo;

            JPAQuery query = new JPAQuery(entityManager);

            query.from(tipoEstadoCampanyaMotivo)
                    .where(tipoEstadoCampanyaMotivo.tipoEstadoCampanya.id.eq(estadoCampanya));

            return query.list(tipoEstadoCampanyaMotivo);
        }
        return null;
    }

    @Override
    public TipoEstadoCampanyaMotivo getTipoEstadoCampanyaById(Long campanyaEstadoMotivoId) {
        QTipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivo = QTipoEstadoCampanyaMotivo.tipoEstadoCampanyaMotivo;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(tipoEstadoCampanyaMotivo)
                .where(tipoEstadoCampanyaMotivo.id.eq(campanyaEstadoMotivoId));

        List<TipoEstadoCampanyaMotivo> tipoEstadoCampanyaMotivos = query.list(tipoEstadoCampanyaMotivo);

        if (tipoEstadoCampanyaMotivos.size() > 0)
        {
            return tipoEstadoCampanyaMotivos.get(0);
        }
        else
        {
            return null;
        }

    }
}