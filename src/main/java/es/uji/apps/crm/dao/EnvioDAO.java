package es.uji.apps.crm.dao;

import java.util.*;

import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Envio;
import es.uji.apps.crm.model.EnvioAdjunto;
import es.uji.apps.crm.model.FiltroEnvio;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.model.QEnvio;
import es.uji.apps.crm.model.QEnvioAdjunto;
import es.uji.apps.crm.model.QEnvioCliente;
import es.uji.apps.crm.model.QSesion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Repository
public class EnvioDAO extends BaseDAODatabaseImpl {

    QEnvio qEnvio = QEnvio.envio;
    QEnvioAdjunto qEnvioAdjunto = QEnvioAdjunto.envioAdjunto;
    QEnvioCliente qEnvioCliente = QEnvioCliente.envioCliente;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public EnvioDAO() {
        relations.put("id", qEnvio.id);
        relations.put("fechaEnvioDestinatario", qEnvio.fechaEnvioDestinatario);
        relations.put("fecha", qEnvio.fecha);
        relations.put("asunto", qEnvio.asunto);
        relations.put("nombre", qEnvio.nombre);
        relations.put("envioTipoId", qEnvio.envioTipo.nombre);
    }


    @Autowired
    private UtilsDAO utilsDAO;

    public List<Envio> getEnvios() {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEnvio)
                .orderBy(qEnvio.nombre.asc());

        return query.list(qEnvio);
    }

    public Envio getEnvioById(Long envioId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEnvio)
                .where(qEnvio.id.eq(envioId));

        List<Envio> lista = query.list(qEnvio);
        if (lista.size() > 0) {
            return lista.get(0);
        }
        return null;
    }

    public List<EnvioAdjunto> getAdjuntosByEnvioId(Long envioId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEnvioAdjunto)
                .where(qEnvioAdjunto.envio.id.eq(envioId))
                .orderBy(qEnvioAdjunto.nombreFichero.asc());

        return query.list(qEnvioAdjunto);
    }

    public List<Envio> getEnviosGenerico(Long connectedUserId, FiltroEnvio filtroEnvio,
                                         Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEnvio)
                .where(qEnvio.seguimiento.isNull()
                        .and(qEnvio.nombre.notLike("NOU")));

        incluir_filtro_busqueda(query, filtroEnvio.getBusqueda());
        incluir_filtro_fecha_desde(query, filtroEnvio.getFechaDesde());
        incluir_filtro_fecha_hasta(query, filtroEnvio.getFechaHasta());
        incluir_filtro_tipo_envio(query, filtroEnvio.getTipoEnvio());
        incluir_filtro_persona(query, filtroEnvio.getPersona(), connectedUserId);
        incluir_filtro_email(query, filtroEnvio.getEmail());
        incluir_filtro_estado(query, filtroEnvio.getEstado());

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

//        query.orderBy(envio.fechaEnvioDestinatario.desc());
//
//        if (ParamUtils.isNotNull(paginacion)) {
//            paginacion.setTotalCount(query.count());
//            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
//        }
//
        List<Envio> todos = query.list(qEnvio);
        List<Envio> otros = getEnvioNuevoAll(connectedUserId);
        todos.addAll(otros);
        return todos;
    }

    private void incluir_filtro_busqueda(JPAQuery query, String busqueda) {
        if (ParamUtils.isNotNull(busqueda)) {
            query.where(utilsDAO.limpiaAcentos(qEnvio.busqueda).containsIgnoreCase(utilsDAO.limpiaAcentos(busqueda)));
        }
    }

    private void incluir_filtro_fecha_desde(JPAQuery query, Date fecha_desde) {
        if (fecha_desde != null) {
            query.where(qEnvio.fechaEnvioDestinatario.gt(fecha_desde)
                    .or(qEnvio.fechaEnvioDestinatario.eq(fecha_desde)));
        }
    }

    private void incluir_filtro_fecha_hasta(JPAQuery query, Date fecha_hasta) {
        if (fecha_hasta != null) {
            Date hasta = addDays(fecha_hasta, 1);
            query.where(qEnvio.fechaEnvioDestinatario.lt(hasta)
                    .or(qEnvio.fechaEnvioDestinatario.eq(hasta)));
        }
    }

    public Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); // minus number would decrement the days
        return cal.getTime();
    }

    private void incluir_filtro_tipo_envio(JPAQuery query, Long tipoEnvio) {
        if (tipoEnvio != null) {
            query.where(qEnvio.envioTipo.id.eq(tipoEnvio));
        }
    }

    private void incluir_filtro_persona(JPAQuery query, Long persona, Long connectedUserId) {
        if (ParamUtils.isNotNull(persona)) {
            if (persona.equals(-1L)) {
                query.where(qEnvio.persona.id.eq(connectedUserId));
            }
            if (persona.equals(-2L)) {
                query.where(qEnvio.persona.id.ne(40000L));
            }

            if (persona.equals(-3L)) {
                query.where(qEnvio.persona.id.eq(40000L));
            }
        }
    }

    private void incluir_filtro_email(JPAQuery query, String email) {
        if (ParamUtils.isNotNull(email)) {

            JPAQuery subquery = new JPAQuery(entityManager);
            subquery.from(qEnvioCliente)
                    .where(utilsDAO.limpiaAcentos(qEnvioCliente.para).contains(utilsDAO.limpiaAcentos(email)));

            query.where(qEnvio.id.in(subquery.distinct()
                    .list(qEnvioCliente.envio.id)));
        }
    }

    private void incluir_filtro_estado(JPAQuery query, Long estado) {
        if (ParamUtils.isNotNull(estado)) {
            if (estado.equals(1L)) {
                query.where(qEnvio.fechaEnvioPlataforma.isNotNull());
            }

            if (estado.equals(2L)) {

                query.where(qEnvio.fechaEnvioDestinatario.isNull());
            }

            if (estado.equals(3L)) {
                query.where(qEnvio.fechaEnvioDestinatario.gt(new Date()));
            }
        }
    }

    public List<Envio> getEnvioNuevoAll(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEnvio).where(
                qEnvio.nombre.eq("NOU").and(
                        qEnvio.seguimiento.id.isNull().and(qEnvio.persona.id.eq(connectedUserId))));

        return query.list(qEnvio);
    }

    public List<Envio> getEnviosSeguimiento(Long seguimientoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEnvio).where(qEnvio.seguimiento.id.eq(seguimientoId));

        return query.list(qEnvio);
    }

    public List<Envio> getEnviosByClienteId(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEnvio).join(qEnvio.envioClientes, qEnvioCliente)
                .where(qEnvioCliente.cliente.id.eq(clienteId));

        return query.list(qEnvio);
    }

    public Envio getEnvioNuevoConSeguimiento(Long connectedUserId, Long tipoId, Long seguimientoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEnvio).where(
                qEnvio.nombre.eq("NOU").and(
                        qEnvio.envioTipo.id.eq(tipoId).and(
                                qEnvio.seguimiento.id.eq(seguimientoId).and(
                                        qEnvio.persona.id.eq(connectedUserId)))));

        if (query.list(qEnvio).size() > 0) {
            return query.list(qEnvio).get(0);
        }
        return null;
    }

    public Envio getEnvioNuevo(Long connectedUserId, Long tipoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEnvio).where(
                qEnvio.nombre.eq("NOU").and(
                        qEnvio.envioTipo.id.eq(tipoId).and(
                                qEnvio.seguimiento.id.isNull().and(
                                        qEnvio.persona.id.eq(connectedUserId)))));

        if (query.list(qEnvio).size() > 0) {
            return query.list(qEnvio).get(0);
        }
        return null;
    }

    public Envio getEnvioByHash(String hash) {

        QSesion qSesion = QSesion.sesion;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEnvio).join(qEnvio.envioSesiones, qSesion).where(qSesion.id.eq(hash));

        List<Envio> res = query.list(qEnvio);
        if (res.size() > 0) {
            return res.get(0);
        } else {
            return null;
        }
    }
}