package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.ProgramaPerfil;
import es.uji.commons.db.BaseDAO;

public interface ProgramaPerfilDAO extends BaseDAO {

    List<ProgramaPerfil> getProgramaPerfilesByProgramaId(Long programaId);

    ProgramaPerfil getProgramaPerfilById(Long programaPerfilId);

}