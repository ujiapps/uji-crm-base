package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QSeguimientoTipo;
import es.uji.apps.crm.model.SeguimientoTipo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class SeguimientoTipoDAODatabaseImpl extends BaseDAODatabaseImpl implements
        SeguimientoTipoDAO {

    @Override
    public List<SeguimientoTipo> getSeguimientoTipos() {
        JPAQuery query = new JPAQuery(entityManager);

        QSeguimientoTipo qSeguimientoTipo = QSeguimientoTipo.seguimientoTipo;

        query.from(qSeguimientoTipo).orderBy(qSeguimientoTipo.nombre.desc());

        return query.list(qSeguimientoTipo);
    }

}