package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Emisora;
import es.uji.commons.db.BaseDAO;

public interface EmisoraDAO extends BaseDAO {

    List<Emisora> getEmisoras();

}