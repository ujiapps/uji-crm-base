package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.ClienteEtiqueta;
import es.uji.commons.db.BaseDAO;

public interface ClienteEtiquetaDAO extends BaseDAO {
    List<ClienteEtiqueta> getClienteEtiquetasByClienteId(Long clienteId);

    ClienteEtiqueta getClienteEtiquetaById(Long clienteEtiquetaId);
}