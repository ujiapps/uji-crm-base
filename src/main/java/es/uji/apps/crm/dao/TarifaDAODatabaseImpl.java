package es.uji.apps.crm.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QTarifa;
import es.uji.apps.crm.model.Tarifa;
import es.uji.apps.crm.model.TipoTarifa;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TarifaDAODatabaseImpl extends BaseDAODatabaseImpl implements TarifaDAO {

    @Override
    public List<Tarifa> getTarifasByTipoTarifa(TipoTarifa tipoTarifa) {
        JPAQuery query = new JPAQuery(entityManager);
        QTarifa qTarifa = QTarifa.tarifa;
        query.from(qTarifa).where(qTarifa.tipoTarifa.eq(tipoTarifa));

        return query.list(qTarifa);
    }

    @Override
    public Tarifa getTarifaVigente(TipoTarifa tipoTarifa) {
        JPAQuery query = new JPAQuery(entityManager);
        QTarifa qTarifa = QTarifa.tarifa;
        Date hoy = new Date();


        query.from(qTarifa).where(qTarifa.tipoTarifa.eq(tipoTarifa).and(qTarifa.fechaInicio.before(hoy)).and(qTarifa.fechaFin.after(hoy)));

        return query.uniqueResult(qTarifa);
    }
}
