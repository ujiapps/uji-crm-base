package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QServicio;
import es.uji.apps.crm.model.QServicioUsuario;
import es.uji.apps.crm.model.Servicio;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ServicioDAODatabaseImpl extends BaseDAODatabaseImpl implements ServicioDAO {

    @Override
    public List<Servicio> getServicios() {
        JPAQuery query = new JPAQuery(entityManager);

        QServicio servicio = QServicio.servicio;

        query.from(servicio).orderBy(servicio.nombre.asc());

        return query.list(servicio);
    }

    @Override
    public Servicio getServicioById(Long servicioId) {
        JPAQuery query = new JPAQuery(entityManager);
        QServicio servicio = QServicio.servicio;

        query.from(servicio).where(servicio.id.eq(servicioId));

        List<Servicio> listaServicios = query.list(servicio);
        if (listaServicios.size() > 0)
        {
            return listaServicios.get(0);
        }
        return null;
    }

    @Override
    public List<Servicio> getServiciosByUsuario(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        QServicio servicio = QServicio.servicio;
        QServicioUsuario servicioUsuario = QServicioUsuario.servicioUsuario;

        query.from(servicio)
                .join(servicio.serviciosUsuario, servicioUsuario)
                .where(servicioUsuario.personaPasPdi.id.eq(connectedUserId))
                .orderBy(servicio.nombre.asc());
        return query.list(servicio);
    }
}