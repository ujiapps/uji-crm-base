package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.CampanyaEnvioAuto;
import es.uji.apps.crm.model.CampanyaEnvioProgramado;
import es.uji.commons.db.BaseDAO;

public interface CampanyaEnvioProgramadoDAO extends BaseDAO {
    List<CampanyaEnvioProgramado> getEnvioProgramadoByEnvio(CampanyaEnvioAuto envio);

//    List<Tarifa> getTarifasByCampanya(Campanya tipoTarifa);

}