package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.ui.OpcionUI;
import es.uji.apps.crm.ui.QOpcionUI;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.QTuple;

import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.QCampanyaDato;
import es.uji.apps.crm.model.QCampanyaItem;
import es.uji.apps.crm.model.QClienteItem;
import es.uji.apps.crm.model.QEnvioItem;
import es.uji.apps.crm.model.QGrupo;
import es.uji.apps.crm.model.QItem;
import es.uji.apps.crm.model.QPersonaPrograma;
import es.uji.apps.crm.model.QPrograma;
import es.uji.apps.crm.model.QTipoAcceso;
import es.uji.apps.crm.model.domains.TipoAccesoProgramaUsuario;
import es.uji.commons.db.BaseDAODatabaseImpl;

import static java.util.stream.Collectors.toList;

@Repository
public class ItemDAODatabaseImpl extends BaseDAODatabaseImpl implements ItemDAO {
    QItem item = QItem.item;
    QEnvioItem envioItem = QEnvioItem.envioItem;
    QCampanyaItem campanyaItem = QCampanyaItem.campanyaItem;
    QGrupo grupo = QGrupo.grupo;
    QTipoAcceso tipoAcceso = QTipoAcceso.tipoAcceso;
    QPrograma programa = QPrograma.programa;
    QPersonaPrograma personaPrograma = QPersonaPrograma.personaPrograma;
    QCampanyaDato campanyaDato = QCampanyaDato.campanyaDato1;


    @Override
    public List<Item> getItems(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(item)
                .join(item.grupo, grupo).fetch()
                .join(grupo.programa, programa).fetch()
                .join(grupo.tipoAcceso, tipoAcceso).fetch()
                .join(programa.personaProgramas, personaPrograma)
                .where(personaPrograma.persona.id.eq(connectedUserId)
                        .and(personaPrograma.tipoAcceso.eq(TipoAccesoProgramaUsuario.ADMIN.getNombre())));

        return query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc()).list(item);
    }

    @Override
    public List<Item> getItemsVisibles(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(item)
                .join(item.grupo, grupo).fetch()
                .join(grupo.programa, programa).fetch()
                .join(grupo.tipoAcceso, tipoAcceso).fetch()
                .join(programa.personaProgramas, personaPrograma)
                .where(personaPrograma.persona.id.eq(connectedUserId)
                        .and(personaPrograma.tipoAcceso.eq(TipoAccesoProgramaUsuario.ADMIN.getNombre())).and(item.visible.eq(true)));

        return query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc()).list(item);
    }

    @Override
    public List<Item> getItemsByGrupoId(Long connectedUserId, Long grupoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(item)
                .join(item.grupo, grupo).fetch()
                .join(grupo.programa, programa).fetch()
                .join(grupo.tipoAcceso, tipoAcceso).fetch()
                .join(programa.personaProgramas, personaPrograma)
                .where(personaPrograma.persona.id.eq(connectedUserId)
                        .and(personaPrograma.tipoAcceso.eq(TipoAccesoProgramaUsuario.ADMIN.getNombre()))
                        .and(item.grupo.id.eq(grupoId)));

        return query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc()).list(item);
    }

    @Override
    public List<Item> getItemsVisiblesByGrupoId(Long connectedUserId, Long grupoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(item)
                .join(item.grupo, grupo).fetch()
                .join(grupo.programa, programa).fetch()
                .join(grupo.tipoAcceso, tipoAcceso).fetch()
                .join(programa.personaProgramas, personaPrograma)
                .where(personaPrograma.persona.id.eq(connectedUserId)
                        .and(personaPrograma.tipoAcceso.eq(TipoAccesoProgramaUsuario.ADMIN.getNombre()))
                        .and(item.grupo.id.eq(grupoId))
                        .and(item.visible.eq(true)));

        return query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc()).list(item);
    }

    @Override
    public List<Item> getItemsByGrupoId(Long grupoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(item)
                .where(item.grupo.id.eq(grupoId))
                .orderBy(item.orden.asc());

        return query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc()).list(item);
    }

    @Override
    public Item getItemById(Long itemId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(item).where(item.id.eq(itemId));

        List<Item> listaItems = query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc()).list(item);
        if (!listaItems.isEmpty()) {
            return listaItems.get(0);
        }
        return null;
    }

    @Override
    public List<Item> getItemsAsociadosByCampanyaId(Long campanyaId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(item)
                .join(item.campanyaItems, campanyaItem)
                .where(campanyaItem.campanya.id.eq(campanyaId));

        return query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc()).list(item);
    }

    @Override
    public List<Item> getItemsByItemIdYCampanyaId(Long item_id, Long campanya_id) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(item)
                .join(item.campanyaItems, campanyaItem)
                .where(campanyaItem.campanya.id.eq(campanya_id)
                        .and(item.id.eq(item_id)));

        return query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc()).list(item);
    }

    @Override
    public List<Item> getItemsByEnvioId(Long envioId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(item)
                .join(item.enviosItems, envioItem)
                .where(envioItem.envio.id.eq(envioId));

        return query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc()).list(item);
    }

    @Override
    public List<Item> getItemsByItemIdYEnvioId(Long item_id, Long envioId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(item)
                .join(item.enviosItems, envioItem)
                .where(envioItem.envio.id.eq(envioId)
                        .and(item.id.eq(item_id)));

        return query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc()).list(item);
    }

    @Override
    public List<Item> getItemsByClienteIdFaltan(Long connectedUserId, Long clienteId, Long grupoId) {
        JPAQuery query = new JPAQuery(entityManager);

        List<Item> items = getItemsByClienteId(clienteId);

        if (items.isEmpty()) {
            return getItemsByGrupoId(connectedUserId, grupoId);
        } else {
            query.from(item)
                    .join(item.grupo, grupo).fetch()
                    .join(grupo.programa, programa).fetch()
                    .join(grupo.tipoAcceso, tipoAcceso).fetch()
                    .join(programa.personaProgramas, personaPrograma)
                    .where(personaPrograma.persona.id.eq(connectedUserId)
                            .and(personaPrograma.tipoAcceso.eq(TipoAccesoProgramaUsuario.ADMIN.getNombre()))
                            .and(item.grupo.id.eq(grupoId))
                            .and(item.notIn(items)));
        }
        return query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc()).list(item);
    }

    public List<Item> getItemsByClienteId(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteItem clienteItem = QClienteItem.clienteItem;

        query.from(item)
                .join(item.clientesItems, clienteItem)
                .where(clienteItem.cliente.id.eq(clienteId));

        return query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc()).list(item);
    }

    public List<Item> getItemsByGrupoIdAndClienteId(Long grupoId, Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteItem clienteItem = QClienteItem.clienteItem;

        query.from(item)
                .join(item.clientesItems, clienteItem)
                .where(item.grupo.id.eq(grupoId)
                        .and(clienteItem.cliente.id.eq(clienteId)));

        return query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc()).list(item);
    }

    @Override
    public List<Item> getItemsByDatoCampanya(Long datoCampanya) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(item)
                .join(item.grupo, grupo).fetch()
                .join(grupo.campanyaDatos, campanyaDato).fetch()
                .where(campanyaDato.id.eq(datoCampanya));

        return query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc()).list(item);

    }

    public List<Item> getItemsByGrupId(Long grupoId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(item)
                .where(item.grupo.id.eq(grupoId));
        return query.orderBy(item.orden.asc()).orderBy(item.nombreCa.asc())
                .list(new QTuple(item.id, item.nombreCa, item.nombreEs, item.nombreUk))
                .stream().map(tuple -> {
                    Item i = new Item();
                    i.setId(tuple.get(item.id));
                    i.setNombreCa(tuple.get(item.nombreCa));
                    i.setNombreEs(tuple.get(item.nombreEs));
                    i.setNombreUk(tuple.get(item.nombreUk));
                    return i;
                }).collect(toList());

    }

    public List<OpcionUI> getItemsByGrupoOrderByIdioma(Long grupoId, String idioma) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(item)
                .where(item.grupo.id.eq(grupoId)).orderBy(item.orden.asc());
        if (idioma.equalsIgnoreCase("en") || idioma.equalsIgnoreCase("uk")) {
            query.orderBy(item.nombreUk.asc());
            return query.list(new QOpcionUI(item.id.stringValue(), item.nombreUk));
        } else {
            if (idioma.equalsIgnoreCase("es")) {
                query.orderBy(item.nombreEs.asc());
                return query.list(new QOpcionUI(item.id.stringValue(), item.nombreEs));
            } else {
                query.orderBy(item.nombreCa.asc());
                return query.list(new QOpcionUI(item.id.stringValue(), item.nombreCa));
            }
        }
    }

    @Override
    public List<Item> getItemsVisiblesByProgramaId(Long programaId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(item).join(item.grupo, grupo).fetch()
                .where(grupo.programa.id.eq(programaId)
                        .and(grupo.visible.isTrue())
                        .and(item.visible.isTrue()));

        return query.list(item);

    }

    @Override
    public List<Item> getItemsVisiblesByGrupId(Long grupoId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(item).join(item.grupo, grupo).fetch()
                .where(grupo.id.eq(grupoId)
                        .and(grupo.visible.isTrue())
                        .and(item.visible.isTrue()));

        return query.list(item);
    }

}