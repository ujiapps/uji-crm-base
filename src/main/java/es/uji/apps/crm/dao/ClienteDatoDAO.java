package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.CampanyaDato;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteDato;
import es.uji.apps.crm.model.Item;
import es.uji.commons.db.BaseDAO;

public interface ClienteDatoDAO extends BaseDAO {

    ClienteDato getClienteDatoById(Long clienteDatoId);

    ClienteDato getClienteDatoByClienteAndTipoId(Cliente cliente, Long tipoId);

    ClienteDato getClienteDatoByClienteAndTipoIdAndItem(Cliente cliente, Long tipoId, Item item);

    ClienteDato getClienteDatoByClienteAndTipoIdAndClienteDato(Cliente cliente, Long tipoId, Long padreDatoId);

    List<ClienteDato> getClienteDatoByClienteDatoId(Long datoId);

    List<ClienteDato> getClienteDatoByItemId(Long itemId);

    List<ClienteDato> getClienteDatoByClienteAndItemId(Cliente cliente, Long itemId);

    List<ClienteDato> getDatosCampanyaByCliente(List<CampanyaDato> campanyaDatos, Cliente cliente);

    List<ClienteDato> getDatosAsociadosACampanya(Long campanyaId, Long clienteId);

    ClienteDato getClienteDatoByClienteAndTipoIdVinculadoCampanya(Cliente cliente, Long tipoDatoId, Long campanyaId);

    List<ClienteDato> getClienteDatosByCampanyaClienteId(Long campanyaClienteId);

    void addClienteDatoExtraEntradas(Long acto, Long entradas);

    ClienteDato getClienteDatoByClienteAndTipoIdAndClienteDatoAndVinculadoCampanya(Cliente cliente, Long tipoId, Long campanyaId, Long tipoVinculadoId);

    List<ClienteDato> getDatosByClienteId(Long clienteId);

    List<ClienteDato> getAllDatosByClienteId(Long clienteId);


//    List<ClienteDato> getClientesDatoEntradasByActoId(Long actoId);
}