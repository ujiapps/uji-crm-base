package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Fase;
import es.uji.apps.crm.model.QFase;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class FaseDAODatabaseImpl extends BaseDAODatabaseImpl implements FaseDAO {

    @Override
    public List<Fase> getFases() {
        JPAQuery query = new JPAQuery(entityManager);

        QFase fase = QFase.fase;

        query.from(fase).orderBy(fase.nombre.asc());

        return query.list(fase);
    }

    @Override
    public Fase getFaseById(Long faseId) {
        JPAQuery query = new JPAQuery(entityManager);

        QFase fase = QFase.fase;

        query.from(fase).where(fase.id.eq(faseId));

        List<Fase> lista = query.list(fase);

        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

}