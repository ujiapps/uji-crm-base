package es.uji.apps.crm.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteDato;
import es.uji.apps.crm.model.ClienteDatoTipo;
import es.uji.apps.crm.model.ClienteGrid;
import es.uji.apps.crm.model.ClienteView;
import es.uji.apps.crm.model.FiltroCliente;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.model.QCampanyaCliente;
import es.uji.apps.crm.model.QClasificacionEstudio;
import es.uji.apps.crm.model.QCliente;
import es.uji.apps.crm.model.QClienteDato;
import es.uji.apps.crm.model.QClienteDatoOpcion;
import es.uji.apps.crm.model.QClienteDatoTipo;
import es.uji.apps.crm.model.QClienteEstudio;
import es.uji.apps.crm.model.QClienteEtiqueta;
import es.uji.apps.crm.model.QClienteGeneral;
import es.uji.apps.crm.model.QClienteGrid;
import es.uji.apps.crm.model.QClienteItem;
import es.uji.apps.crm.model.QClienteTitulacionesNoUJI;
import es.uji.apps.crm.model.QClienteView;
import es.uji.apps.crm.model.QEnvioCliente;
import es.uji.apps.crm.model.QEtiqueta;
import es.uji.apps.crm.model.QPersona;
import es.uji.apps.crm.model.QServicio;
import es.uji.apps.crm.model.QServicioUsuario;
import es.uji.apps.crm.model.QVwItem;
import es.uji.apps.crm.model.domains.TipoComparacion;
import es.uji.apps.crm.model.domains.TipoFecha;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;

@Repository
public class ClienteDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    public static final Long MAX_RESULTADOS = 1000L;
    QCliente cliente = QCliente.cliente;
    QClienteItem clienteItem = QClienteItem.clienteItem;
    QEnvioCliente envioCliente = QEnvioCliente.envioCliente;
    QClienteGrid clienteGrid = QClienteGrid.clienteGrid;
    QClienteGeneral clienteGeneral = QClienteGeneral.clienteGeneral;
    @Autowired
    private UtilsDAO utilsDAO;

    public Cliente getClienteById(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteGeneral clienteGeneral = QClienteGeneral.clienteGeneral;
        QPersona qPersona = QPersona.persona;

        query.from(cliente)
                .leftJoin(cliente.clienteGeneral, clienteGeneral).fetch()
                .leftJoin(clienteGeneral.persona, qPersona).fetch()
                .where(cliente.id.eq(clienteId));

        List<Cliente> lista = query.list(cliente);
        if (!lista.isEmpty()) {
            return lista.get(0);
        }
        return null;
    }

    public List<Cliente> getClientesByEnvioId(Long envioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QClienteGeneral clienteGeneral = QClienteGeneral.clienteGeneral;

        query.from(cliente)
                .join(cliente.envioClientes, envioCliente).fetch()
                .join(cliente.clienteGeneral, clienteGeneral).fetch()
                .where(envioCliente.envio.id.eq(envioId)).orderBy(cliente.id.asc());

        return query.list(cliente);
    }

    public List<ClienteGrid> getClienteByBusqueda(Long connectedUserId, FiltroCliente filtro, Paginacion paginacion)
            throws ParseException {
        JPAQuery query = new JPAQuery(entityManager);
        QServicio servicio = QServicio.servicio;
        QServicioUsuario servicioUsuario = QServicioUsuario.servicioUsuario;

        query.from(clienteGrid, cliente)
                .join(cliente.servicio, servicio)
                .join(servicio.serviciosUsuario, servicioUsuario)
                .where(servicioUsuario.personaPasPdi.id.eq(connectedUserId).and(clienteGrid.clienteId.eq(cliente.id))).distinct();


        creaQueryBusqueda(query, filtro);

        if (paginacion != null) {
            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        } else {
            query.limit(MAX_RESULTADOS);
        }

        return query.list(clienteGrid);
    }

    private void creaQueryBusqueda(JPAQuery query, FiltroCliente filtro) {

        anyade_filtro_busqueda(query, filtro.getBusquedaGenerica());
        anyade_filtro_etiqueta(query, filtro.getEtiqueta());
        anyade_filtro_tipo(query, filtro.getTipoCliente());
        anyade_filtro_campanya(query, filtro.getCampanyaId(), filtro.getCampanyaTipoEstado());
        anyade_filtro_dato_extra(query, filtro);
        anyade_filtro_correo(query, filtro.getCorreo());
        anyade_filtro_postal(query, filtro.getPostal());
        anyade_filtro_movil(query, filtro.getMovil());
        anyade_filtro_nacionalidad(query, filtro.getNacionalidad());
        anyade_filtro_codigoPostal(query, filtro.getCodigoPostal());
        anyade_filtro_paisDomicilio(query, filtro.getPaisDomicilio());
        anyade_filtro_provinciaDomicilio(query, filtro.getProvinciaDomicilio());
        anyade_filtro_poblacionDomicilio(query, filtro.getPoblacionDomiclio());
        anyade_filtro_programa(query, filtro.getPrograma(), filtro.getGrupo(), filtro.getItem());

        anyade_filtro_estudioUJI(query, filtro.getEstudioUJI(), filtro.getEstudioUJIBeca());
        anyade_filtro_estudioNoUJI(query, filtro.getEstudioNoUJI());
    }

    private void anyade_filtro_correo(JPAQuery query, String correo) {
        if (ParamUtils.isNotNull(correo)) {

            String correoFiltrado = getCadenaFiltrada(correo);

            if (correoFiltrado.equals("-1")) {
                query.where(cliente.correo.isNull());
            } else if (correoFiltrado.equals("-2")) {
                query.where(cliente.correo.isNotNull());
            } else {
                query.where(utilsDAO.limpiaAcentos(cliente.correo).containsIgnoreCase(correoFiltrado)
                        .or(utilsDAO.limpiaAcentos(clienteGrid.correoOficial).containsIgnoreCase(correoFiltrado)));
            }

        }
    }

    private void anyade_filtro_nacionalidad(JPAQuery query, String nacionalidad) {
        if (ParamUtils.isNotNull(nacionalidad)) {
            query.where(cliente.nacionalidad.id.eq(nacionalidad));
        }
    }

    private void anyade_filtro_codigoPostal(JPAQuery query, String codigoPostal) {
        if (ParamUtils.isNotNull(codigoPostal)) {

            String codigoPostalFiltrado = getCadenaFiltrada(codigoPostal);

            if (codigoPostalFiltrado.equals("-1")) {
                query.where(cliente.codigoPostal.isNull());
            } else if (codigoPostalFiltrado.equals("-2")) {
                query.where(cliente.codigoPostal.isNotNull());
            } else {
                query.where(cliente.codigoPostal.eq(codigoPostalFiltrado));
            }

        }
    }

    private void anyade_filtro_paisDomicilio(JPAQuery query, String paisDomicilio) {

        if (ParamUtils.isNotNull(paisDomicilio)) {

            if (paisDomicilio.equals("-1")) {
                query.where(cliente.paisPostal.isNull());
            } else if (paisDomicilio.equals("-2")) {
                query.where(cliente.paisPostal.isNotNull());
            } else {
                query.where(cliente.paisPostal.id.eq(paisDomicilio));
            }

        }
    }

    private void anyade_filtro_provinciaDomicilio(JPAQuery query, Long provinciaDomicilio) {

        if (ParamUtils.isNotNull(provinciaDomicilio)) {

            if (provinciaDomicilio.equals(-1L)) {
                query.where(cliente.provinciaPostal.isNull());
            } else if (provinciaDomicilio.equals(-2L)) {
                query.where(cliente.provinciaPostal.isNotNull());
            } else {
                query.where(cliente.provinciaPostal.id.eq(provinciaDomicilio));
            }

        }
    }

    private void anyade_filtro_poblacionDomicilio(JPAQuery query, Long poblacionDomicilio) {

        if (ParamUtils.isNotNull(poblacionDomicilio)) {

            if (poblacionDomicilio.equals(-1L)) {
                query.where(cliente.poblacionPostal.isNull());
            } else if (poblacionDomicilio.equals(-2L)) {
                query.where(cliente.poblacionPostal.isNotNull());
            } else {
                query.where(cliente.poblacionPostal.id.eq(poblacionDomicilio));
            }

        }
    }

    private void anyade_filtro_estudioUJI(JPAQuery query, Long estudioUJI, List<Long> estudioUJIBeca) {
        QClienteEstudio qClienteEstudio = QClienteEstudio.clienteEstudio;

        if (ParamUtils.isNotNull(estudioUJI) || (ParamUtils.isNotNull(estudioUJIBeca) && !estudioUJIBeca.isEmpty())) {
            query.from(qClienteEstudio).where(cliente.clienteGeneral.persona.id.eq(qClienteEstudio.persona));
        }
        if (ParamUtils.isNotNull(estudioUJI)) {

            QClasificacionEstudio qClasificacionEstudio = QClasificacionEstudio.clasificacionEstudio;
            query.from(qClasificacionEstudio)
                    .where(qClienteEstudio.estudioId.eq(qClasificacionEstudio.estudioId).and(qClienteEstudio.tipoEstudio.eq(qClasificacionEstudio.tipo)));

            query.where(qClasificacionEstudio.id.eq(estudioUJI));

        }

        if (ParamUtils.isNotNull(estudioUJIBeca) && !estudioUJIBeca.isEmpty()) {
            if (estudioUJIBeca.size() == 1) {
                if (estudioUJIBeca.get(0).equals(-1L)) {
                    query.where(qClienteEstudio.tipoBecaId.isNull());
                } else {
                    if (estudioUJIBeca.get(0).equals(-2L)) {
                        query.where(qClienteEstudio.tipoBecaId.isNotNull());
                    } else {
                        query.where(qClienteEstudio.tipoBecaId.eq(estudioUJIBeca.get(0)));
                    }
                }
            } else {
                query.where(qClienteEstudio.tipoBecaId.in(estudioUJIBeca));
            }
        }
    }

    private void anyade_filtro_estudioNoUJI(JPAQuery query, Long estudioNoUJI) {

        if (ParamUtils.isNotNull(estudioNoUJI)) {

            QClienteTitulacionesNoUJI qClienteTitulacionesNoUJI = QClienteTitulacionesNoUJI.clienteTitulacionesNoUJI;

            query.from(qClienteTitulacionesNoUJI).join(cliente.titulacionesNoUJI, qClienteTitulacionesNoUJI)
                    .where(qClienteTitulacionesNoUJI.clasificacion.id.eq(estudioNoUJI));

        }
    }

    private void anyade_filtro_postal(JPAQuery query, String postal) {
        if (ParamUtils.isNotNull(postal)) {

            if (utilsDAO.limpiaAcentos(postal).equals("-1")) {
                query.where(cliente.postal.isNull());
            } else if (utilsDAO.limpiaAcentos(postal).equals("-2")) {
                query.where(cliente.postal.isNotNull());
            } else {
                query.where(utilsDAO.limpiaAcentos(cliente.postal).containsIgnoreCase(utilsDAO.limpiaAcentos(postal)));
            }

        }
    }

    private void anyade_filtro_movil(JPAQuery query, String movil) {
        if (movil != null && !movil.isEmpty()) {

            String movilFiltrado = getCadenaFiltrada(movil);

            if (movilFiltrado.equals("-1")) {
                query.where(cliente.movil.isNull());
            } else if (movilFiltrado.equals("-2")) {
                query.where(cliente.movil.isNotNull());
            } else {
                query.where(cliente.movil.contains(movilFiltrado));
            }
        }
    }

    private void anyade_filtro_programa(JPAQuery query, Long programa, Long grupo, Long item) {

        if (programa != null) {
            QClienteItem qClienteItem = QClienteItem.clienteItem;
            QVwItem qVwItem = QVwItem.vwItem;

            query.from(qVwItem).join(cliente.clientesItems, qClienteItem)
                    .where(qClienteItem.item.id.eq(qVwItem.id));
            if (grupo != null) {
                if (item != null) {
                    query.where(qVwItem.id.eq(item));
                } else {
                    query.where(qVwItem.grupo.id.eq(grupo));
                }
            } else {
                query.where(qVwItem.programaId.eq(programa));
            }
        }

    }

    private void anyade_filtro_dato_extra(JPAQuery query, FiltroCliente filtro) {

        ClienteDatoTipo tipoDato = filtro.getTipoDato();
        String valorDato = filtro.getValorDato();
        Long tipoComparacion = filtro.getTipoComparacion();
        Date fecha = filtro.getFecha();
        Date fechaMax = filtro.getFechaMax();
        Long tipoFecha = filtro.getTipoFecha();
        Long mes = filtro.getMes();
        Long mesMax = filtro.getMesMax();
        Long anyo = filtro.getAnyo();
        Long anyoMax = filtro.getAnyoMax();

        if (tipoDato != null) {
            QClienteDato qClienteDato = QClienteDato.clienteDato;
            query.leftJoin(cliente.clientesDatos, qClienteDato);

            if (valorDato.equals("-1")) {
                QClienteDato qClienteDatoExtra = QClienteDato.clienteDato;

                if (tipoDato.getClienteDatoTipoTipo().getNombre().charAt(0) == 'N'
                        || tipoDato.getClienteDatoTipoTipo().getNombre().charAt(0) == 'S') {
                    query.where((qClienteDato.clienteDatoTipo.id.eq(tipoDato.getId())
                            .and(qClienteDato.valorNumero.isNull())).or(new JPASubQuery()
                            .from(qClienteDatoExtra)
                            .where(qClienteDatoExtra.clienteDatoTipo.id.eq(tipoDato.getId()).and(
                                    qClienteDatoExtra.cliente.id.eq(cliente.id)))
                            .list(qClienteDatoExtra).notExists()));
                }

                if (tipoDato.getClienteDatoTipoTipo().getNombre().charAt(0) == 'T') {
                    query.where((qClienteDato.clienteDatoTipo.id.eq(tipoDato.getId())
                            .and(qClienteDato.valorTexto.isNull())).or(new JPASubQuery()
                            .from(qClienteDatoExtra)
                            .where(qClienteDatoExtra.clienteDatoTipo.id.eq(tipoDato.getId()).and(
                                    qClienteDatoExtra.cliente.id.eq(cliente.id)))
                            .list(qClienteDatoExtra).notExists()));
                }

                if (tipoDato.getClienteDatoTipoTipo().getNombre().charAt(0) == 'F') {
                    query.where((qClienteDato.clienteDatoTipo.id.eq(tipoDato.getId())
                            .and(qClienteDato.valorFecha.isNull())).or(new JPASubQuery()
                            .from(qClienteDatoExtra)
                            .where(qClienteDatoExtra.clienteDatoTipo.id.eq(tipoDato.getId()).and(
                                    qClienteDatoExtra.cliente.id.eq(cliente.id)))
                            .list(qClienteDatoExtra).notExists()));
                }
            } else if (valorDato.equals("-2")) {
                if (tipoDato.getClienteDatoTipoTipo().getNombre().charAt(0) == 'N'
                        || tipoDato.getClienteDatoTipoTipo().getNombre().charAt(0) == 'S') {
                    query.where(qClienteDato.clienteDatoTipo.id.eq(tipoDato.getId()).and(
                            qClienteDato.valorNumero.isNotNull()));
                }

                if (tipoDato.getClienteDatoTipoTipo().getNombre().charAt(0) == 'T') {
                    query.where(qClienteDato.clienteDatoTipo.id.eq(tipoDato.getId()).and(
                            qClienteDato.valorTexto.isNotNull()));
                }

                if (tipoDato.getClienteDatoTipoTipo().getNombre().charAt(0) == 'F') {
                    query.where(qClienteDato.clienteDatoTipo.id.eq(tipoDato.getId()).and(
                            qClienteDato.valorFecha.isNotNull()));
                }
            } else {
                String busquedaDato = getCadenaFiltrada(valorDato);

                if (tipoDato.getClienteDatoTipoTipo().getNombre().charAt(0) == 'N'
                        || tipoDato.getClienteDatoTipoTipo().getNombre().charAt(0) == 'S') {
                    query.where(qClienteDato.clienteDatoTipo.id.eq(tipoDato.getId()).and(
                            qClienteDato.valorNumero.eq(Long.valueOf(valorDato))));
                }

                if (tipoDato.getClienteDatoTipoTipo().getNombre().charAt(0) == 'T') {
                    query.where(qClienteDato.clienteDatoTipo.id.eq(tipoDato.getId()).and(
                            utilsDAO.limpiaAcentos(qClienteDato.valorTexto).containsIgnoreCase(busquedaDato)));
                }

                if (tipoDato.getClienteDatoTipoTipo().getNombre().charAt(0) == 'F') {
                    query.where(qClienteDato.clienteDatoTipo.id.eq(tipoDato.getId()));

                    if (tipoFecha.equals(TipoFecha.COMPLETA.getId())) {

                        if (tipoComparacion.equals(TipoComparacion.MAYORQUE.getId())) {
                            query.where(qClienteDato.valorFecha.gt(fecha));
                        }
                        if (tipoComparacion.equals(TipoComparacion.MENORQUE.getId())) {
                            query.where(qClienteDato.valorFecha.lt(fecha));
                        }
                        if (tipoComparacion.equals(TipoComparacion.IGUAL.getId())) {
                            query.where(qClienteDato.valorFecha.eq(fecha));
                        }
                        if (tipoComparacion.equals(TipoComparacion.MAYOROIGUAL.getId())) {
                            query.where(qClienteDato.valorFecha.eq(fecha).or(
                                    qClienteDato.valorFecha.gt(fecha)));
                        }
                        if (tipoComparacion.equals(TipoComparacion.MENOROIGUAL.getId())) {
                            query.where(qClienteDato.valorFecha.eq(fecha).or(
                                    qClienteDato.valorFecha.lt(fecha)));
                        }
                        if (tipoComparacion.equals(TipoComparacion.ENTRE.getId())) {
                            query.where(qClienteDato.valorFecha.between(fecha, fechaMax));
                        }
                    }

                    if (tipoFecha.equals(TipoFecha.ANUAL.getId())) {
                        if (tipoComparacion.equals(TipoComparacion.MAYORQUE.getId())) {
                            query.where(qClienteDato.valorFecha.year().gt(anyo));
                        }
                        if (tipoComparacion.equals(TipoComparacion.MENORQUE.getId())) {
                            query.where(qClienteDato.valorFecha.year().lt(anyo));
                        }
                        if (tipoComparacion.equals(TipoComparacion.IGUAL.getId())) {
                            query.where(qClienteDato.valorFecha.year().eq(anyo.intValue()));
                        }
                        if (tipoComparacion.equals(TipoComparacion.MAYOROIGUAL.getId())) {
                            query.where(qClienteDato.valorFecha.year().eq(anyo.intValue())
                                    .or(qClienteDato.valorFecha.year().gt(anyo)));
                        }
                        if (tipoComparacion.equals(TipoComparacion.MENOROIGUAL.getId())) {
                            query.where(qClienteDato.valorFecha.year().eq(anyo.intValue())
                                    .or(qClienteDato.valorFecha.year().lt(anyo)));
                        }
                        if (tipoComparacion.equals(TipoComparacion.ENTRE.getId())) {
                            query.where(qClienteDato.valorFecha.year().between(anyo, anyoMax));
                        }
                    }

                    if (tipoFecha.equals(TipoFecha.MENSUAL.getId())) {
                        if (tipoComparacion.equals(TipoComparacion.MAYORQUE.getId())) {
                            query.where(qClienteDato.valorFecha.month().gt(mes));
                        }
                        if (tipoComparacion.equals(TipoComparacion.MENORQUE.getId())) {
                            query.where(qClienteDato.valorFecha.month().lt(mes));
                        }
                        if (tipoComparacion.equals(TipoComparacion.IGUAL.getId())) {
                            query.where(qClienteDato.valorFecha.month().eq(mes.intValue()));
                        }
                        if (tipoComparacion.equals(TipoComparacion.MAYOROIGUAL.getId())) {
                            query.where(qClienteDato.valorFecha.month().eq(mes.intValue())
                                    .or(qClienteDato.valorFecha.month().gt(mes)));
                        }
                        if (tipoComparacion.equals(TipoComparacion.MENOROIGUAL.getId())) {
                            query.where(qClienteDato.valorFecha.month().eq(mes.intValue())
                                    .or(qClienteDato.valorFecha.month().lt(mes)));
                        }
                        if (tipoComparacion.equals(TipoComparacion.ENTRE.getId())) {
                            query.where(qClienteDato.valorFecha.month().between(mes, mesMax));
                        }
                    }
                }
            }

        }
    }

    private void anyade_filtro_campanya(JPAQuery query, Long campanya, Long campanyaTipoEstado) {
        if (campanya != null) {
            QCampanyaCliente qCampanyaCliente = QCampanyaCliente.campanyaCliente;

            query.leftJoin(cliente.campanyasClientes, qCampanyaCliente);

            query.where(qCampanyaCliente.campanya.id.eq(campanya));
            if (campanyaTipoEstado != null) {
                query.where(qCampanyaCliente.campanyaClienteTipo.id.eq(campanyaTipoEstado));
            }
        }
    }

    private void anyade_filtro_tipo(JPAQuery query, String tipo) {
        if (tipo != null && !tipo.isEmpty()) {
            query.where(cliente.tipo.eq(tipo));
        }
    }

    private void anyade_filtro_etiqueta(JPAQuery query, String etiqueta) {

        if (etiqueta != null && !etiqueta.isEmpty()) {
            QEtiqueta qEtiqueta = QEtiqueta.etiqueta1;
            QClienteEtiqueta qClienteEtiqueta = QClienteEtiqueta.clienteEtiqueta;

            query.leftJoin(cliente.clientesEtiquetas, qClienteEtiqueta).leftJoin(
                    qClienteEtiqueta.etiqueta, qEtiqueta);

            String cadenaEtiqueta = getCadenaFiltrada(etiqueta);
            query.where(utilsDAO.limpiaAcentos(qEtiqueta.busqueda).containsIgnoreCase(cadenaEtiqueta));
        }
    }

    private void anyade_filtro_busqueda(JPAQuery query, String busqueda) {
        if (ParamUtils.isNotNull(busqueda)) {
            String busquedaFiltrada = getCadenaFiltrada(busqueda);

            query.where(utilsDAO.limpiaAcentos(clienteGrid.busqueda).containsIgnoreCase(busquedaFiltrada));
        }
    }

    private String getCadenaFiltrada(String cadena) {
        return StringUtils.limpiaAcentos(cadena).trim().toUpperCase();
    }

    public List<Long> addGridClientesACampanya(FiltroCliente filtro) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteGrid, cliente).where(clienteGrid.clienteId.eq(cliente.id)).distinct();

        creaQueryBusqueda(query, filtro);
        return query.list(cliente.id);
    }

    public List<ClienteGrid> getClientesGridByClienteGeneralId(Long connectedUserId, Long clienteGeneralId) {

        JPAQuery query = new JPAQuery(entityManager);
        QServicio servicio = QServicio.servicio;
        QServicioUsuario servicioUsuario = QServicioUsuario.servicioUsuario;

        query.from(clienteGrid, cliente).join(cliente.servicio, servicio).join(servicio.serviciosUsuario, servicioUsuario)
                .where(clienteGrid.clienteId.eq(cliente.id)
                        .and(cliente.clienteGeneral.id.eq(clienteGeneralId)).and(servicioUsuario.personaPasPdi.id.eq(connectedUserId)));
        return query.list(clienteGrid);
    }

    public List<ClienteView> getClientesByClienteGeneralId(Long conectedUserId, Long clienteGeneralId) {
        JPAQuery query = new JPAQuery(entityManager);

        QClienteView qClienteView = QClienteView.clienteView;
        QServicio qServicio = QServicio.servicio;
        QServicioUsuario qServicioUsuario = QServicioUsuario.servicioUsuario;

        query.from(qClienteView, qServicio, qServicioUsuario)
                .where(qClienteView.servicioId.eq(qServicio.id)
                        .and(qServicioUsuario.servicio.id.eq(qServicio.id)
                                .and(qServicioUsuario.personaPasPdi.id.eq(conectedUserId)
                                        .and(qClienteView.clienteGeneral.eq(clienteGeneralId)))));
        return query.list(qClienteView);
    }

    public Cliente getClienteByClienteGeneralId(Long clienteGeneralId, Boolean zonaPrivada) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(cliente)
                .where(cliente.clienteGeneral.id.eq(clienteGeneralId));

        if (zonaPrivada) {
            query.where(cliente.fichaZonaPrivada.isTrue()
                    .and(cliente.servicio.id.eq(366L)));
        }

        return query.singleResult(cliente);
    }

    public List<ClienteDato> getClientesDatos(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDato clienteDato = QClienteDato.clienteDato;
        QClienteDatoTipo clienteDatoTipo = QClienteDatoTipo.clienteDatoTipo;
        QClienteDatoOpcion clienteDatoOpcion = QClienteDatoOpcion.clienteDatoOpcion;
        query.from(clienteDato).join(clienteDato.clienteDatoTipo, clienteDatoTipo).fetch()
                .leftJoin(clienteDatoTipo.clientesDatosOpciones, clienteDatoOpcion).fetch()
                .where(clienteDato.cliente.id.eq(clienteId)).distinct()
                .orderBy(clienteDatoTipo.orden.asc());

        return query.list(clienteDato);
    }

    public List<Cliente> getClientesByItemId(Long itemId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(cliente).join(cliente.clientesItems, clienteItem)
                .where(clienteItem.item.id.eq(itemId));
        return query.list(cliente);
    }

    public List<LookupItem> search(String cadena) {
        JPAQuery query = new JPAQuery(entityManager);
        QCliente qCliente = QCliente.cliente;

        String cadenaFiltrada = getCadenaFiltrada(cadena);

        List<LookupItem> result = new ArrayList<>();
        if (ParamUtils.isNotNull(cadenaFiltrada)) {
            query.from(qCliente).where(qCliente.busqueda.containsIgnoreCase(cadenaFiltrada));

            List<Cliente> listaClientes = query.list(qCliente);

            for (Cliente cliente : listaClientes) {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(cliente.getId().toString());
                lookupItem.setNombre(cliente.getNombreApellidos());
                lookupItem.addExtraParam("Servicio", cliente.getServicio().getNombre());
                result.add(lookupItem);
            }
        }
        return result;
    }

    public Cliente getClienteByPersonaIdAndServicio(Long personaId, Long servicioId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(cliente)
                .leftJoin(cliente.clienteGeneral, clienteGeneral).fetch()
                .where(clienteGeneral.persona.id.eq(personaId)
                        .and(cliente.servicio.id.eq(servicioId)));

        List<Cliente> clientes = query.list(cliente);
        if (!clientes.isEmpty()) {
            return clientes.get(0);
        }

        return null;
    }

    public Cliente getClienteByCorreoAndServicio(String correo, Long servicioId) {
        JPAQuery query = new JPAQuery(entityManager);

        List<Cliente> lista = query.from(cliente)
                .where(cliente.correo.eq(correo)
                        .and(cliente.servicio.id.eq(servicioId))
                        .and(cliente.fichaZonaPrivada.isTrue())).list(cliente);

        if (!lista.isEmpty()) return lista.get(0);
        else return null;
    }

    public ClienteGrid getClienteParaFormularioByCorreoAndServicio(String correo, Long servicioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QClienteGrid qClienteVw = QClienteGrid.clienteGrid;
        List<ClienteGrid> lista = query.from(qClienteVw)
                .where((qClienteVw.correo.eq(correo).or(qClienteVw.correoOficial.eq(correo)))
                        .and(qClienteVw.servicio.eq(servicioId).and(qClienteVw.definitivo.isTrue()))
                ).list(qClienteVw);

        if (!lista.isEmpty()) return lista.get(0);
        else return null;
    }

    public List<ClienteGrid> getClientesParaFormularioByCorreoAndServicio(String correo, Long servicioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QClienteGrid qClienteVw = QClienteGrid.clienteGrid;
        return query.from(qClienteVw)
                .where((qClienteVw.correo.eq(correo).or(qClienteVw.correoOficial.eq(correo)))
                        .and(qClienteVw.servicio.eq(servicioId).and(qClienteVw.definitivo.isTrue()))
                ).list(qClienteVw);
    }

    public List<ClienteGrid> getClientesGridByIdentificacion(Long connectedUserId, String identificacion) {
        JPAQuery query = new JPAQuery(entityManager);
        QServicio servicio = QServicio.servicio;
        QServicioUsuario servicioUsuario = QServicioUsuario.servicioUsuario;

        query.from(clienteGrid, cliente)
                .join(cliente.servicio, servicio)
                .join(servicio.serviciosUsuario, servicioUsuario)
                .where(servicioUsuario.personaPasPdi.id.eq(connectedUserId).and(clienteGrid.clienteId.eq(cliente.id)).and(clienteGrid.identificacion.eq(identificacion))).distinct();

        return query.list(clienteGrid);
    }

    public Cliente getClienteByIdentificacionAndServicio(String identificacion, Long servicio) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(cliente)
                .where(cliente.clienteGeneral.identificacion.eq(identificacion)
                        .and(cliente.servicio.id.eq(servicio)
                                .and(cliente.fichaZonaPrivada.isTrue())));
        return query.singleResult(cliente);
    }

    public Cliente getClienteByCorreoAndIdentificacionAndServicio(String correo, String identificacion, Long servicioId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(cliente)
                .join(cliente.clienteGeneral, clienteGeneral)
                .where(cliente.correo.eq(correo)
                        .and(cliente.servicio.id.eq(servicioId))
                        .and(clienteGeneral.identificacion.eq(identificacion))
                        .and(cliente.fichaZonaPrivada.isTrue()));
        List<Cliente> clientes = query.list(cliente);
        if (!clientes.isEmpty()) return clientes.get(0);
        else return null;

    }
}