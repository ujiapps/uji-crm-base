package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.commons.db.BaseDAO;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ClienteItem;
import es.uji.apps.crm.model.QCliente;
import es.uji.apps.crm.model.QClienteItem;
import es.uji.apps.crm.model.QGrupo;
import es.uji.apps.crm.model.QItem;
import es.uji.apps.crm.model.domains.TipoEnvio;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ClienteItemDAO extends BaseDAODatabaseImpl{

    QClienteItem clienteItem = QClienteItem.clienteItem;
    QItem qItem = QItem.item;
    QGrupo qGrupo = QGrupo.grupo;
    QCliente qCliente = QCliente.cliente;

    public ClienteItem getItemsClientes(Long itemId, Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteItem)
                .join(clienteItem.item, qItem).fetch()
                .join(clienteItem.cliente, qCliente).fetch()
                .join(qItem.grupo, qGrupo).fetch()
                .where(clienteItem.cliente.id.eq(clienteId)
                        .and(clienteItem.item.id.eq(itemId)));

        List<ClienteItem> listaItems = query.list(clienteItem);

        if (listaItems.size() > 0)
        {
            return listaItems.get(0);
        }
        return null;
    }

    public ClienteItem getItemsClientesById(Long clienteItemId) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteItem)
                .join(clienteItem.item, qItem).fetch()
                .join(clienteItem.cliente, qCliente).fetch()
                .join(qItem.grupo, qGrupo).fetch()
                .where(clienteItem.id.eq(clienteItemId));

        List<ClienteItem> listaItems = query.list(clienteItem);

        if (listaItems.size() > 0)
        {
            return listaItems.get(0);
        }
        return null;
    }

    public List<ClienteItem> getItemsByGrupoIdAndClienteId(Long grupoId, Long clienteId) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteItem)
                .join(clienteItem.item, qItem).fetch()
                .join(clienteItem.cliente, qCliente).fetch()
                .join(qItem.grupo, qGrupo).fetch()
                .where(qItem.grupo.id.eq(grupoId)
                        .and(clienteItem.cliente.id.eq(clienteId))
                        .and(clienteItem.campanya.isNull())
                        .and(clienteItem.clienteDato.isNull())
                        .and(clienteItem.clienteItemRelacionado.isNull()));

        return query.list(clienteItem);
    }

    public List<ClienteItem> getItemsByClienteId(Long clienteId) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteItem)
                .join(clienteItem.item, qItem).fetch()
                .join(clienteItem.cliente, qCliente).fetch()
                .join(qItem.grupo, qGrupo).fetch()
                .where(clienteItem.cliente.id.eq(clienteId)
                        .and(clienteItem.campanya.isNull())
                        .and(clienteItem.clienteDato.isNull())
                        .and(clienteItem.clienteItemRelacionado.isNull()));

        return query.list(clienteItem);
    }

    public List<ClienteItem> getAllItemsByClienteId(Long clienteId) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteItem)
                .join(clienteItem.item, qItem).fetch()
                .join(clienteItem.cliente, qCliente).fetch()
                .join(qItem.grupo, qGrupo).fetch()
                .where(clienteItem.cliente.id.eq(clienteId));

        return query.list(clienteItem);
    }

    public Long getClientesItemByItemId(Long itemId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteItem)
                .where(clienteItem.item.id.eq(itemId));

        return query.count();
    }

    public Long getCountClientesItem(Long tipoEnvioId, Long itemId) {
        JPAQuery query = new JPAQuery(entityManager);

        if (tipoEnvioId.equals(TipoEnvio.EMAIL.getId()))
        {
            query.from(clienteItem)
                    .where(clienteItem.correo.isTrue()
                            .and(clienteItem.item.id.eq(itemId)))
                    .groupBy(clienteItem.item);
        }

        if (tipoEnvioId.equals(TipoEnvio.POSTAL.getId()))
        {
            query.from(clienteItem)
                    .where(clienteItem.postal.isTrue()
                            .and(clienteItem.item.id.eq(itemId)))
                    .groupBy(clienteItem.item);
        }

        List<Long> listaItems = query.list(clienteItem.count());

        if (listaItems.size() > 0)
        {
            return listaItems.get(0);
        }
        return 0L;
    }

    public List<ClienteItem> getItemRelacionadosByClienteIdAndItemId(Long clienteId, Long itemId) {

        JPAQuery query = new JPAQuery(entityManager);
        QClienteItem clienteItem = QClienteItem.clienteItem;

        query.from(clienteItem)
                .join(clienteItem.item, qItem).fetch()
                .join(clienteItem.cliente, qCliente).fetch()
                .join(qItem.grupo, qGrupo).fetch()
                .where(clienteItem.cliente.id.eq(clienteId)
                        .and(clienteItem.clienteItemRelacionado.eq(itemId)));

        return query.list(clienteItem);
    }

    public ClienteItem getClienteItemCliente(Long grupoId, Long clienteId) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteItem)
                .join(clienteItem.item, qItem)
                .join(clienteItem.cliente, qCliente)
                .join(qItem.grupo, qGrupo)
                .where(qItem.grupo.id.eq(grupoId)
                        .and(clienteItem.cliente.id.eq(clienteId))
                        .and(clienteItem.campanya.isNull())
                        .and(clienteItem.clienteItemRelacionado.isNull()));

        List<ClienteItem> lista = query.list(clienteItem);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        else
        {
            return null;
        }
    }

    public List<ClienteItem> getItemsVisiblesByClienteIdAndProgramaId(Long clienteId, Long programaId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteItem)
                .join(clienteItem.item, qItem).fetch()
                .join(qItem.grupo, qGrupo)
                .join(clienteItem.cliente, qCliente)
                .where(qGrupo.programa.id.eq(programaId)
                        .and(qGrupo.visible.isTrue())
                        .and(qItem.visible.isTrue())
                        .and(qCliente.id.eq(clienteId))
                );

        return query.list(clienteItem);

    }

    public List<ClienteItem> getItemsByGrupoIdAndClienteIdAndCampanyaId(Long grupoId, Long clienteId, Long campanyaId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteItem)
                .join(clienteItem.item, qItem).fetch()
                .join(clienteItem.cliente, qCliente).fetch()
                .join(qItem.grupo, qGrupo).fetch()
                .where(qItem.grupo.id.eq(grupoId)
                        .and(clienteItem.cliente.id.eq(clienteId))
                        .and(clienteItem.campanya.id.eq(campanyaId))
                        );

        return query.list(clienteItem);
    }

//    @Override
//    public ClienteItem getClienteItemByItemAndCliente(Long clienteId, Long itemId) {
//        QClienteItem clienteItem = QClienteItem.clienteItem;
//
//        JPAQuery query = new JPAQuery(entityManager);
//
//        query.from(clienteItem)
//                .where(clienteItem.item.id.eq(itemId).and(clienteItem.cliente.id.eq(clienteId)));
//
//        List<ClienteItem> res = query.list(clienteItem);
//        if (res.size() > 0) {
//            return res.get(0);
//        }
//        return null;
//    }
}