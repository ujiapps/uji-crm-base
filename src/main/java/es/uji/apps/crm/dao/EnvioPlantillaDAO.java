package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.EnvioPlantilla;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.model.QEnvioPlantilla;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EnvioPlantillaDAO extends BaseDAODatabaseImpl {

    @Autowired
    private UtilsDAO utilsDAO;

    public List<EnvioPlantilla> getEnvioPlantillas(Paginacion paginacion) {

        QEnvioPlantilla qEnvioPlantilla = QEnvioPlantilla.envioPlantilla;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEnvioPlantilla);
        paginacion.setTotalCount(query.count());
        query.offset(paginacion.getStart()).limit(paginacion.getLimit()).orderBy(qEnvioPlantilla.nombre.asc());
        return query.list(qEnvioPlantilla);

    }

    public EnvioPlantilla getEnvioPlantillaById(Long envioCabeceraId) {

        QEnvioPlantilla qEnvioPlantilla = QEnvioPlantilla.envioPlantilla;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEnvioPlantilla).where(qEnvioPlantilla.id.eq(envioCabeceraId));

        return query.singleResult(qEnvioPlantilla);
    }
}