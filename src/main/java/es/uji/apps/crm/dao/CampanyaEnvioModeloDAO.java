package es.uji.apps.crm.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.CampanyaEnvioModelo;
import es.uji.apps.crm.model.QCampanyaEnvioModelo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CampanyaEnvioModeloDAO extends BaseDAODatabaseImpl {
    public CampanyaEnvioModelo getCampanyaEnvioModelo(String tipoModelo) {

        JPAQuery query = new JPAQuery(entityManager);
        QCampanyaEnvioModelo campanyaEnvioModelo = QCampanyaEnvioModelo.campanyaEnvioModelo;

        query.from(campanyaEnvioModelo).where(campanyaEnvioModelo.tipoModelo.eq(tipoModelo));

        return query.uniqueResult(campanyaEnvioModelo);

    }
}