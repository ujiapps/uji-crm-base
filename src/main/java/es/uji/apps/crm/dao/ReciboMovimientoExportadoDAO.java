package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.ReciboMovimientoExportado;
import es.uji.commons.db.BaseDAO;

public interface ReciboMovimientoExportadoDAO extends BaseDAO {

    List<ReciboMovimientoExportado> getReciboMovimientoExportado(Long movimientoId);

    ReciboMovimientoExportado getReciboMovimientoExportadoById(Long reciboMovimientoExportadoId);
}