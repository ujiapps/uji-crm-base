package es.uji.apps.crm.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.crm.ui.OpcionUI;
import es.uji.apps.crm.ui.QOpcionUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Poblacion;
import es.uji.apps.crm.model.QPoblacion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.json.lookup.LookupItem;


@Repository
public class PoblacionDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    @Autowired
    private UtilsDAO utilsDAO;

    public List<Poblacion> getPoblacionesByProvinciaId(Long provinciaId, String idioma) {
        JPAQuery query = new JPAQuery(entityManager);
        QPoblacion poblacion = QPoblacion.poblacion;

        query.from(poblacion).where(poblacion.provincia.id.eq(provinciaId));

        if (idioma.equalsIgnoreCase("en") || idioma.equalsIgnoreCase("uk"))
        {
            query.orderBy(utilsDAO.limpiaAcentos(poblacion.nombreEN).asc());
        }
        else
        {
            if (idioma.equalsIgnoreCase("es"))
            {
                query.orderBy(utilsDAO.limpiaAcentos(poblacion.nombreES).asc());
            }
            else
            {
                query.orderBy(utilsDAO.limpiaAcentos(poblacion.nombreCA).asc());
            }
        }
        return query.list(poblacion);
    }

    public List<Poblacion> getPoblaciones(String idioma) {
        JPAQuery query = new JPAQuery(entityManager);
        QPoblacion poblacion = QPoblacion.poblacion;

        query.from(poblacion);

        if (idioma.equalsIgnoreCase("en") || idioma.equalsIgnoreCase("uk"))
        {

            query.orderBy(utilsDAO.limpiaAcentos(poblacion.nombreEN).asc());
        }
        else
        {
            if (idioma.equalsIgnoreCase("es"))
            {
                query.orderBy(utilsDAO.limpiaAcentos(poblacion.nombreES).asc());
            }
            else
            {
                query.orderBy(utilsDAO.limpiaAcentos(poblacion.nombreCA).asc());
            }
        }
        return query.list(poblacion);
    }

    public List<LookupItem> search(String cadena) {
        JPAQuery query = new JPAQuery(entityManager);
        QPoblacion qPoblacion = QPoblacion.poblacion;

        String cadenaFiltrada = utilsDAO.limpiaAcentos(cadena);

        List<LookupItem> result = new ArrayList<>();
        if (ParamUtils.isNotNull(cadenaFiltrada))
        {
            query.from(qPoblacion);
            query.where(utilsDAO.limpiaAcentos(qPoblacion.nombreES).containsIgnoreCase(cadenaFiltrada)
                    .or(utilsDAO.limpiaAcentos(qPoblacion.nombreCA).containsIgnoreCase(cadenaFiltrada))
                    .or(utilsDAO.limpiaAcentos(qPoblacion.nombreEN).containsIgnoreCase(cadenaFiltrada)));

            List<Poblacion> listaPoblaciones = query.list(qPoblacion);

            for (Poblacion poblacion : listaPoblaciones)
            {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(poblacion.getId().toString());
                lookupItem.setNombre(poblacion.getNombreCA());
                lookupItem.addExtraParam("Provincia", poblacion.getProvincia().getNombreCA());
                result.add(lookupItem);
            }
        }
        return result;
    }

    public List<OpcionUI> getOpcionPoblacionesByProvinciaId(Long provinciaId, String idioma) {
        JPAQuery query = new JPAQuery(entityManager);
        QPoblacion poblacion = QPoblacion.poblacion;

        query.from(poblacion).where(poblacion.provincia.id.eq(provinciaId));

        if (idioma.equalsIgnoreCase("en") || idioma.equalsIgnoreCase("uk"))
        {
            query.orderBy(poblacion.nombreENLimpio.asc());
            return query.list(new QOpcionUI(poblacion.id.stringValue(), poblacion.nombreEN));
        }
        else
        {
            if (idioma.equalsIgnoreCase("es"))
            {
                query.orderBy(poblacion.nombreESLimpio.asc());
                return query.list(new QOpcionUI(poblacion.id.stringValue(), poblacion.nombreES));
            }
            else
            {
                query.orderBy(poblacion.nombreCALimpio.asc());
                return query.list(new QOpcionUI(poblacion.id.stringValue(), poblacion.nombreCA));
            }
        }
    }
}