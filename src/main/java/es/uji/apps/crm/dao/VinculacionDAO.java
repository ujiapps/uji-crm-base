package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Vinculacion;
import es.uji.commons.db.BaseDAO;

public interface VinculacionDAO extends BaseDAO {
    List<Vinculacion> getVinculacionesByCliente1Id(Long clienteId);

    List<Vinculacion> getVinculacionesInversasByClienteId(Long clienteId);
}