package es.uji.apps.crm.dao;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.EstudioUJI;
import es.uji.apps.crm.model.QEstudioUJI;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EstudioUJIDAO extends BaseDAODatabaseImpl {
    public static final Long MAX_RESULTADOS = 500L;
    @Autowired
    private UtilsDAO utilsDAO;

    public List<EstudioUJI> getEstudiosUJI(Long clasificacionId) {
        QEstudioUJI estudioUJI = QEstudioUJI.estudioUJI;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(estudioUJI)
                .where(estudioUJI.clasificacion.eq(clasificacionId));

        return query.list(estudioUJI);
    }
}