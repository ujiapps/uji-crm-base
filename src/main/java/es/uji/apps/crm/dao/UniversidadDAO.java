package es.uji.apps.crm.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.crm.model.QUniversidad;
import es.uji.apps.crm.ui.QUniversidadUI;
import es.uji.apps.crm.ui.UniversidadUI;
import org.springframework.stereotype.Repository;

import es.uji.commons.db.BaseDAODatabaseImpl;

import java.util.List;

@Repository
public class UniversidadDAO extends BaseDAODatabaseImpl {
    public List<UniversidadUI> getUniversidades(String idioma) {
        JPAQuery query = new JPAQuery(entityManager);
        QUniversidad universidad = QUniversidad.universidad;

        query.from(universidad);

        return query.list(new QUniversidadUI(universidad.id,
                idioma.equals("es") ? universidad.nombreEs : idioma.equals("en") ? universidad.nombreUk : idioma.equals("uk") ? universidad.nombreUk :universidad.nombreCa));
    }
}