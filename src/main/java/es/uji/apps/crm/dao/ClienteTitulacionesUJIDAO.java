package es.uji.apps.crm.dao;

import java.util.List;

import com.mysema.query.Tuple;
import es.uji.apps.crm.model.QClienteDatoOpcion;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ClienteTitulacionesUJI;
import es.uji.apps.crm.model.QClienteTitulacionesUJI;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ClienteTitulacionesUJIDAO extends BaseDAODatabaseImpl {
    public List<ClienteTitulacionesUJI> getClienteEstudiosUJIByCliente(Long clienteId) {
        QClienteTitulacionesUJI clienteTitulacionesUJI = QClienteTitulacionesUJI.clienteTitulacionesUJI;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteTitulacionesUJI).where(clienteTitulacionesUJI.cliente.id.eq(clienteId));

        return query.list(clienteTitulacionesUJI);
    }

    public List<Tuple> getClienteEstudiosUJIByClienteJoinClasificacion(Long clienteId) {
        QClienteTitulacionesUJI clienteTitulacionesUJI = QClienteTitulacionesUJI.clienteTitulacionesUJI;
        QClienteDatoOpcion qClienteDatoOpcion = QClienteDatoOpcion.clienteDatoOpcion;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteTitulacionesUJI)
                .join(qClienteDatoOpcion)
                .on(clienteTitulacionesUJI.tipoEstudio.eq(qClienteDatoOpcion.referencia)
                        .and(qClienteDatoOpcion.clienteDatoTipo.id.eq(8740721L)))
                .where(clienteTitulacionesUJI.cliente.id.eq(clienteId));

        return query.list(clienteTitulacionesUJI, qClienteDatoOpcion);
    }
}