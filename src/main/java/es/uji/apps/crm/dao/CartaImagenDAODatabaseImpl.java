package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.CartaImagen;
import es.uji.apps.crm.model.QCartaImagen;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CartaImagenDAODatabaseImpl extends BaseDAODatabaseImpl implements CartaImagenDAO {

    @Override
    public List<CartaImagen> getCartaImagenes() {
        JPAQuery query = new JPAQuery(entityManager);

        QCartaImagen qCartaImagen = QCartaImagen.cartaImagen;

        query.from(qCartaImagen);
        return query.list(qCartaImagen);
    }

    @Override
    public List<CartaImagen> getCartaImagenesByCampanyaCartaId(Long campanyaCartaId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCartaImagen qCartaImagen = QCartaImagen.cartaImagen;

        query.from(qCartaImagen).where(qCartaImagen.campanyaCarta.id.eq(campanyaCartaId));
        return query.list(qCartaImagen);
    }

    @Override
    public CartaImagen getCartaImagenById(Long cartaImagenId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCartaImagen qCartaImagen = QCartaImagen.cartaImagen;

        query.from(qCartaImagen).where(qCartaImagen.id.eq(cartaImagenId));
        List<CartaImagen> cartaImagenes = query.list(qCartaImagen);
        if (cartaImagenes.isEmpty())
        {
            return null;
        }
        else
        {
            return cartaImagenes.get(0);
        }
    }
}