package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.CampanyaFase;
import es.uji.apps.crm.model.QCampanyaFase;
import es.uji.apps.crm.model.QFase;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CampanyaFaseDAODatabaseImpl extends BaseDAODatabaseImpl implements CampanyaFaseDAO {

    @Override
    public List<CampanyaFase> getCampanyaFases() {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaFase campanyaFase = QCampanyaFase.campanyaFase;
        QFase fase = QFase.fase;

        query.from(campanyaFase)
                .join(campanyaFase.fase, fase).fetch();

        return query.list(campanyaFase);
    }

    @Override
    public List<CampanyaFase> getCampanyaFasesByCampanyaId(Long campanyaId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaFase campanyaFase = QCampanyaFase.campanyaFase;
        QFase fase = QFase.fase;

        query.from(campanyaFase)
                .join(campanyaFase.fase, fase).fetch()
                .where(campanyaFase.campanya.id.eq(campanyaId));

        return query.list(campanyaFase);
    }

    @Override
    public CampanyaFase getCampanyaFasesById(Long campanyaFaseId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaFase campanyaFase = QCampanyaFase.campanyaFase;

        query.from(campanyaFase)
                .where(campanyaFase.id.eq(campanyaFaseId));

        List<CampanyaFase> lista = query.list(campanyaFase);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

}