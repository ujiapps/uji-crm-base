package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.SeguimientoTipo;
import es.uji.commons.db.BaseDAO;

public interface SeguimientoTipoDAO extends BaseDAO {
    List<SeguimientoTipo> getSeguimientoTipos();
}