package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.TipoValidacion;
import es.uji.commons.db.BaseDAO;

public interface TipoValidacionDAO extends BaseDAO {
    List<TipoValidacion> getTiposValidacion();
}