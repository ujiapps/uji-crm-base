package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QTipoTarifaEnvioProgramado;
import es.uji.apps.crm.model.TipoTarifaEnvio;
import es.uji.apps.crm.model.TipoTarifaEnvioProgramado;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoTarifaEnvioProgramadoDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoTarifaEnvioProgramadoDAO {
    @Override
    public List<TipoTarifaEnvioProgramado> getTipoTarifaEnvioProgramadoByTipoTarifaEnvio(TipoTarifaEnvio tipoTarifaEnvio) {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoTarifaEnvioProgramado qTipoTarifaEnvioProgramado = QTipoTarifaEnvioProgramado.tipoTarifaEnvioProgramado;

        query.from(qTipoTarifaEnvioProgramado).where(qTipoTarifaEnvioProgramado.tarifaTipoEnvio.eq(tipoTarifaEnvio));

        return query.list(qTipoTarifaEnvioProgramado);
    }

    @Transactional
    @Override
    public void deleteEnvioProgramadoByTipoTarifaEnvio(Long tarifaEnvioId) {
        QTipoTarifaEnvioProgramado qTipoTarifaEnvioProgramado = QTipoTarifaEnvioProgramado.tipoTarifaEnvioProgramado;

        JPADeleteClause deleteProgramados = new JPADeleteClause(entityManager, qTipoTarifaEnvioProgramado);

        deleteProgramados.where(qTipoTarifaEnvioProgramado.tarifaTipoEnvio.id.eq(tarifaEnvioId)).execute();

    }

}