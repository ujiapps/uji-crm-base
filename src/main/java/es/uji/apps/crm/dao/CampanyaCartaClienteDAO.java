package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.CampanyaCartaCliente;
import es.uji.commons.db.BaseDAO;

public interface CampanyaCartaClienteDAO extends BaseDAO {
    List<CampanyaCartaCliente> getClienteCartasByClienteId(Long clienteId, Long campanyaId);

    CampanyaCartaCliente getClienteCartaById(Long clienteCartaId);
}