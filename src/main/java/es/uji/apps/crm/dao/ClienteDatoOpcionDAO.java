package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.QClienteDatoTipo;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ClienteDatoOpcion;
import es.uji.apps.crm.model.QClienteDatoOpcion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ClienteDatoOpcionDAO extends BaseDAODatabaseImpl  {

    public ClienteDatoOpcion getClienteDatoOpcionById(Long clienteDatoOpcionId) {
        if (clienteDatoOpcionId == null)
        {
            return null;
        }

        JPAQuery query = new JPAQuery(entityManager);
        QClienteDatoOpcion clienteDatoOpcion = QClienteDatoOpcion.clienteDatoOpcion;

        query.from(clienteDatoOpcion)
                .where(clienteDatoOpcion.id.eq(clienteDatoOpcionId));

        List<ClienteDatoOpcion> lista = query.list(clienteDatoOpcion);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

    public List<ClienteDatoOpcion> getClienteDatoOpcionesByTipoId(Long clienteDatoTipoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDatoOpcion clienteDatoOpcion = QClienteDatoOpcion.clienteDatoOpcion;

        query.from(clienteDatoOpcion)
                .where(clienteDatoOpcion.clienteDatoTipo.id.eq(clienteDatoTipoId));

        return query.list(clienteDatoOpcion);
    }
}