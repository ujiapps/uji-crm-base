package es.uji.apps.crm.dao;

import java.util.Date;
import java.util.List;

import com.mysema.query.jpa.impl.JPAUpdateClause;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaActo;
import es.uji.apps.crm.model.QCampanyaActo;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class CampanyaActoDAODatabaseImpl extends BaseDAODatabaseImpl implements CampanyaActoDAO {
    @Override
    public List<CampanyaActo> getCampanyasActoByCampanya(Campanya campanya) {

        QCampanyaActo campanyaActo = QCampanyaActo.campanyaActo;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(campanyaActo).where(campanyaActo.campanya.eq(campanya));

        return query.list(campanyaActo);
    }

    @Override
    public List<CampanyaActo> getCampanyasActoRss() {
        QCampanyaActo campanyaActo = QCampanyaActo.campanyaActo;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(campanyaActo).where(campanyaActo.rssActivo.isTrue().and(campanyaActo.fecha.after(new Date())));
        return query.list(campanyaActo);
    }

    @Override
    public CampanyaActo getCampanyaActoById(Long campanyaActoId) {

        QCampanyaActo campanyaActo = QCampanyaActo.campanyaActo;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(campanyaActo).where(campanyaActo.id.eq(campanyaActoId));

        List<CampanyaActo> lista = query.list(campanyaActo);

        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

    @Override
    @Transactional
    public void activarRssEvento(Long actoId) {
        QCampanyaActo campanyaActo = QCampanyaActo.campanyaActo;
        new JPAUpdateClause(entityManager, campanyaActo).where(campanyaActo.id.eq(actoId)).set(campanyaActo.rssActivo, true).execute();
    }
}