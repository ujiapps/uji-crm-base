package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.CampanyaFase;
import es.uji.commons.db.BaseDAO;

public interface CampanyaFaseDAO extends BaseDAO {
    List<CampanyaFase> getCampanyaFases();

    List<CampanyaFase> getCampanyaFasesByCampanyaId(Long campanyaId);

    CampanyaFase getCampanyaFasesById(Long campanyaFaseId);

}