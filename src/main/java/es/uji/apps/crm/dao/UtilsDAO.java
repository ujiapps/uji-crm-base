package es.uji.apps.crm.dao;

import java.util.Date;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.expr.StringExpression;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.StringPath;

import es.uji.apps.crm.model.FiltroFechas;
import es.uji.commons.db.BaseDAO;

public interface UtilsDAO extends BaseDAO {
    void creaQueryFecha(BooleanBuilder build, FiltroFechas filtro, DateTimePath<Date> fechaColumna);

    StringExpression limpiaAcentos(StringPath nombre);

    String limpiaAcentos(String cadena);

}