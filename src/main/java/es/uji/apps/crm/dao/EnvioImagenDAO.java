package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.EnvioImagen;
import es.uji.commons.db.BaseDAO;

public interface EnvioImagenDAO extends BaseDAO {
    List<EnvioImagen> getEnvioImagenes();

    List<EnvioImagen> getEnvioImagenesByEnvioId(Long envioId);

    EnvioImagen getEnvioImagenById(Long envioImagenId);

    List<EnvioImagen> getEnvioImagenesByTarifaEnvioId(Long tarifaEnvioId);

    List<EnvioImagen> getEnvioImagenesGenerico(String search);

}