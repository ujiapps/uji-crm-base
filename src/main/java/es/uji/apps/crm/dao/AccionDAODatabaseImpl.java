package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Accion;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.QAccion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AccionDAODatabaseImpl extends BaseDAODatabaseImpl implements AccionDAO {

    @Override
    public Accion getAccionByReferencia(Cliente cliente, Campanya campanya) {
        JPAQuery query = new JPAQuery(entityManager);
        QAccion qAccion = QAccion.accion1;

        query.from(qAccion).where(
                qAccion.referencia.eq("ALTA#" + cliente.getId() + "#" + campanya.getId()));

        List<Accion> result = query.list(qAccion);
        if (result.size() > 0)
        {
            return result.get(0);
        }
        return null;
    }
}