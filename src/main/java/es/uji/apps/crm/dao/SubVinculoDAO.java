package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.SubVinculo;
import es.uji.commons.db.BaseDAO;

public interface SubVinculoDAO extends BaseDAO {

    List<SubVinculo> getSubVinculos();

    List<SubVinculo> getSubVinculosByPrograma(Long programaId);

    String getSubVinculoNombreById(Long subVinculo);

}