package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Servicio;
import es.uji.commons.db.BaseDAO;

public interface ServicioDAO extends BaseDAO {
    List<Servicio> getServicios();

    Servicio getServicioById(Long servicioId);

    List<Servicio> getServiciosByUsuario(Long connectedUserId);

//    void updateServicio(Servicio servicio);

}