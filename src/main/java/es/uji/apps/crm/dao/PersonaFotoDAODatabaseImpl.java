package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.PersonaFoto;
import es.uji.apps.crm.model.QPersonaFoto;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PersonaFotoDAODatabaseImpl extends BaseDAODatabaseImpl implements PersonaFotoDAO {

    @Override
    public PersonaFoto getPersonaFotoByClienteId(Long clienteId) {
        QPersonaFoto personaFoto = QPersonaFoto.personaFoto;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(personaFoto).where(personaFoto.clienteId.eq(clienteId));
        List<PersonaFoto> list = query.list(personaFoto);
        if (list.size() > 0)
        {
            return list.get(0);
        }
        else
        {
            return null;
        }

    }
}