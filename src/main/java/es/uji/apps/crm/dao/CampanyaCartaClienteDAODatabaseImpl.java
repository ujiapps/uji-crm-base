package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.CampanyaCartaCliente;
import es.uji.apps.crm.model.QCampanyaCartaCliente;
import es.uji.apps.crm.model.QCliente;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CampanyaCartaClienteDAODatabaseImpl extends BaseDAODatabaseImpl implements CampanyaCartaClienteDAO {
    @Override
    public List<CampanyaCartaCliente> getClienteCartasByClienteId(Long clienteId, Long campanyaId) {

        QCampanyaCartaCliente campanyaCartaCliente = QCampanyaCartaCliente.campanyaCartaCliente;
        QCliente cliente = QCliente.cliente;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(campanyaCartaCliente)
                .join(campanyaCartaCliente.cliente, cliente).fetch()
                .where(cliente.id.eq(clienteId).and(campanyaCartaCliente.campanya.id.eq(campanyaId)));

        return query.list(campanyaCartaCliente);
    }

    @Override
    public CampanyaCartaCliente getClienteCartaById(Long clienteCartaId) {

        QCampanyaCartaCliente campanyaCartaCliente = QCampanyaCartaCliente.campanyaCartaCliente;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(campanyaCartaCliente)
                .where(campanyaCartaCliente.id.eq(clienteCartaId));

        List<CampanyaCartaCliente> campanyaClienteCartas = query.list(campanyaCartaCliente);
        if (campanyaClienteCartas.size() > 0)
        {
            return campanyaClienteCartas.get(0);
        }
        return null;
    }
}