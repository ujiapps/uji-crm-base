package es.uji.apps.crm.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteTarifa;
import es.uji.apps.crm.model.QCampanya;
import es.uji.apps.crm.model.QCliente;
import es.uji.apps.crm.model.QClienteTarifa;
import es.uji.apps.crm.model.QTipoTarifa;
import es.uji.apps.crm.model.TipoTarifa;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ClienteTarifaDAODatabaseImpl extends BaseDAODatabaseImpl implements ClienteTarifaDAO {
    @Override
    public List<ClienteTarifa> getClienteTarifaByClienteIdAndCampanyaId(Long clienteId, Long campanyaId) {

        JPAQuery query = new JPAQuery(entityManager);
        QClienteTarifa clienteTarifa = QClienteTarifa.clienteTarifa;
        QTipoTarifa tipoTarifa = QTipoTarifa.tipoTarifa;
        QCampanya campanya = QCampanya.campanya1;
        QCliente cliente = QCliente.cliente;

        query.from(clienteTarifa)
                .join(clienteTarifa.tipoTarifa, tipoTarifa).fetch()
                .join(clienteTarifa.cliente, cliente).fetch()
                .join(tipoTarifa.campanya, campanya).fetch()
                .where(clienteTarifa.cliente.id.eq(clienteId)
                        .and(tipoTarifa.campanya.id.eq(campanyaId)));

        return query.list(clienteTarifa);
    }

    @Override
    public ClienteTarifa dameFechaMaxCaducaClienteTarifa(Cliente cliente) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteTarifa clienteTarifa = QClienteTarifa.clienteTarifa;

        List<ClienteTarifa> lista = query.from(clienteTarifa)
                .where(clienteTarifa.cliente.eq(cliente)
                        .and(clienteTarifa.fechaCaducidad.isNotNull())
                        .and(clienteTarifa.fechaCaducidad.after(new Date())))
                .orderBy(clienteTarifa.fechaCaducidad.desc())
                .list(clienteTarifa);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        else
        {
            return null;
        }
    }

    @Override
    public ClienteTarifa getClienteTarifaById(Long clienteTarifaId) {

        JPAQuery query = new JPAQuery(entityManager);
        QClienteTarifa clienteTarifa = QClienteTarifa.clienteTarifa;
        QCliente cliente = QCliente.cliente;
        QTipoTarifa tipoTarifa = QTipoTarifa.tipoTarifa;

        query.from(clienteTarifa)
                .join(clienteTarifa.cliente, cliente).fetch()
                .join(clienteTarifa.tipoTarifa, tipoTarifa).fetch()
                .where(clienteTarifa.id.eq(clienteTarifaId));

        List<ClienteTarifa> res = query.list(clienteTarifa);
        if (res.size() > 0)
        {
            return res.get(0);
        }
        return null;
    }

    @Override
    public List<ClienteTarifa> getClienteTarifasByClienteAndTipoTarifa(Cliente cliente, TipoTarifa tipoTarifa) {

        JPAQuery query = new JPAQuery(entityManager);
        QClienteTarifa clienteTarifa = QClienteTarifa.clienteTarifa;

        query.from(clienteTarifa)
                .where(clienteTarifa.cliente.id.eq(cliente.getId())
                        .and(clienteTarifa.tipoTarifa.id.eq(tipoTarifa.getId())));

        return query.list(clienteTarifa);
    }
}