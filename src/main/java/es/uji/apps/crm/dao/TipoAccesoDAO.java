package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.TipoAcceso;
import es.uji.commons.db.BaseDAO;

public interface TipoAccesoDAO extends BaseDAO {
    TipoAcceso getTipoAccesoById(Long tipoAccesoId);

    List<TipoAcceso> getTiposAcceso();
}