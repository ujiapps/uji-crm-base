package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.PersonaPasPdi;
import es.uji.apps.crm.model.ServicioUsuario;
import es.uji.commons.db.BaseDAO;

public interface ServicioUsuarioDAO extends BaseDAO {

    List<PersonaPasPdi> getPersonas();

    List<ServicioUsuario> getServicioUSuario();

    List<ServicioUsuario> getServiciosUsuarioById(Long servicioId);

    List<ServicioUsuario> getServiciosUsuariosByPersonaId(Long personaId);

    List<Long> getServiciosIdByPersonaId(Long usuarioId);

}