package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.TipoTarifa;
import es.uji.apps.crm.model.TipoTarifaEnvio;
import es.uji.commons.db.BaseDAO;

public interface TipoTarifaEnvioDAO extends BaseDAO {
    List<TipoTarifaEnvio> getTipoTarifaEnvioByTipoTarifa(TipoTarifa tipoTarifa);

    TipoTarifaEnvio getTipoTarifaEnvioById(Long tipoTarifaEnvioId);

//    List<Tarifa> getTarifasByTipoTarifa(TipoTarifa tipoTarifa);

}