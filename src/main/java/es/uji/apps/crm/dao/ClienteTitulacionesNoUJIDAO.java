package es.uji.apps.crm.dao;

import java.util.List;

import com.mysema.query.types.Expression;
import es.uji.apps.crm.model.*;
import es.uji.apps.crm.ui.ClienteTitulacionNoUjiUI;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ClienteTitulacionesNoUJIDAO extends BaseDAODatabaseImpl {

    public List<ClienteTitulacionesNoUJI> getClienteEstudiosNoUJIByPersona(Long clienteId) {

        JPAQuery query = new JPAQuery(entityManager);
        QClienteTitulacionesNoUJI qClienteTitulacionesNoUJI = QClienteTitulacionesNoUJI.clienteTitulacionesNoUJI;

        query.from(qClienteTitulacionesNoUJI).where(qClienteTitulacionesNoUJI.cliente.id.eq(clienteId))
                .orderBy(qClienteTitulacionesNoUJI.tipoEstudio.asc())
                .orderBy(qClienteTitulacionesNoUJI.anyoFinalizacion.desc())
                .orderBy(qClienteTitulacionesNoUJI.nombre.asc());

        return query.list(qClienteTitulacionesNoUJI);
    }

    public ClienteTitulacionesNoUJI getClienteEstudiosNoUJIByClienteDato(Long clienteDatoNivelEstudioId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteTitulacionesNoUJI qClienteTitulacionesNoUJI = QClienteTitulacionesNoUJI.clienteTitulacionesNoUJI;

        query.from(qClienteTitulacionesNoUJI).where(qClienteTitulacionesNoUJI.clienteDatoEstudio.id.eq(clienteDatoNivelEstudioId));

        List<ClienteTitulacionesNoUJI> clienteTitulacionesNoUJILista = query.list(qClienteTitulacionesNoUJI);
        return clienteTitulacionesNoUJILista.isEmpty() ? new ClienteTitulacionesNoUJI() : clienteTitulacionesNoUJILista.get(0);
    }
}