package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.PersonaPasPdi;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.db.LookupDAO;


public interface PersonaPasPdiDAO extends BaseDAO, LookupDAO {

    List<PersonaPasPdi> getPersonasByServicioId(Long servicioId);

    PersonaPasPdi getPersonaById(Long personaId);

    List<PersonaPasPdi> getPersonasByProgramaId(Long programaId);

//    public List<PersonaPasPdi> getPersonasPasPdi();


}