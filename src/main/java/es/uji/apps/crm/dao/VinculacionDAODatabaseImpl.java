package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QCliente;
import es.uji.apps.crm.model.QTipo;
import es.uji.apps.crm.model.QVinculacion;
import es.uji.apps.crm.model.Vinculacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class VinculacionDAODatabaseImpl extends BaseDAODatabaseImpl implements VinculacionDAO {

    @Override
    public List<Vinculacion> getVinculacionesByCliente1Id(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        QVinculacion vinculacion = QVinculacion.vinculacion;
        QTipo tipoVinculacion = QTipo.tipo;
        QCliente cliente1 = QCliente.cliente;
        QCliente cliente2 = new QCliente("cliente2");

        query.from(vinculacion).join(vinculacion.tipo, tipoVinculacion).fetch()
                .join(vinculacion.cliente1, cliente1).fetch()
                .join(vinculacion.cliente2, cliente2).fetch()
                .where(vinculacion.cliente1.id.eq(clienteId));

        return query.list(vinculacion);
    }

    @Override
    public List<Vinculacion> getVinculacionesInversasByClienteId(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        QVinculacion vinculacion = QVinculacion.vinculacion;
        QTipo tipoVinculacion = QTipo.tipo;
        QCliente cliente1 = QCliente.cliente;
        QCliente cliente2 = new QCliente("cliente2");

        query.from(vinculacion).join(vinculacion.tipo, tipoVinculacion).fetch()
                .join(vinculacion.cliente1, cliente1).fetch()
                .join(vinculacion.cliente2, cliente2).fetch()
                .where(vinculacion.cliente2.id.eq(clienteId));

        return query.list(vinculacion);
    }
}