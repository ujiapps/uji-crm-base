package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Perfil;
import es.uji.apps.crm.model.QPerfil;
import es.uji.apps.crm.model.QProgramaPerfil;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PerfilDAODatabaseImpl extends BaseDAODatabaseImpl implements PerfilDAO {
    QPerfil perfil = QPerfil.perfil;

    @Override
    public List<Perfil> getPerfiles() {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(perfil).orderBy(perfil.nombre.asc());
        return query.list(perfil);
    }

    @Override
    public List<Perfil> getPerfilesByProgramaId(Long programaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QProgramaPerfil programaPerfil = QProgramaPerfil.programaPerfil;

        query.from(perfil).join(perfil.programasPerfiles, programaPerfil)
                .where(programaPerfil.programa.id.eq(programaId));

        return query.list(perfil);
    }

    @Override
    public List<Perfil> getPerfilesFaltanByProgramaId(Long programaId) {
        JPAQuery query = new JPAQuery(entityManager);

        List<Perfil> lista = getPerfilesByProgramaId(programaId);

        if (lista.isEmpty())
        {
            return getPerfiles();
        }
        else
        {
            query.from(perfil).where(perfil.notIn(lista)).orderBy(perfil.nombre.asc());
        }
        return query.list(perfil);

    }

}