package es.uji.apps.crm.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ProgramaUsuario;
import es.uji.apps.crm.model.QPersonaPasPdi;
import es.uji.apps.crm.model.QProgramaUsuario;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ProgramaUsuarioDAODatabaseImpl extends BaseDAODatabaseImpl implements
        ProgramaUsuarioDAO {
    @Override
    public List<ProgramaUsuario> getProgramaUsuariosByProgramaId(Long programaId) {
        JPAQuery query = new JPAQuery(entityManager);

        QProgramaUsuario programaUsuario = QProgramaUsuario.programaUsuario;
        QPersonaPasPdi personaPasPdi = QPersonaPasPdi.personaPasPdi;

        query.from(programaUsuario)
                .leftJoin(programaUsuario.personaPasPdi, personaPasPdi).fetch()
                .where(programaUsuario.programa.id.eq(programaId));

        return query.list(programaUsuario);
    }

    @Override
    public List<ProgramaUsuario> getProgramasUsuariosByPersonaId(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);

        QProgramaUsuario programaUsuario = QProgramaUsuario.programaUsuario;

        query.from(programaUsuario)
                .where(programaUsuario.personaPasPdi.id.eq(personaId));

        return query.list(programaUsuario);
    }

    @Override
    public List<ProgramaUsuario> getProgramasUsuariosByPersonaIdAndTipoAcceso(Long personaId, String tipoAcceso) {
        JPAQuery query = new JPAQuery(entityManager);

        QProgramaUsuario programaUsuario = QProgramaUsuario.programaUsuario;

        query.from(programaUsuario)
                .where(programaUsuario.personaPasPdi.id.eq(personaId)
                        .and(programaUsuario.tipo.eq(tipoAcceso)));

        return query.list(programaUsuario);
    }

    @Override
    public ProgramaUsuario getProgramasUsuarioById(Long programaUsuarioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QProgramaUsuario programaUsuario = QProgramaUsuario.programaUsuario;

        query.from(programaUsuario)
                .where(programaUsuario.id.eq(programaUsuarioId));

        List<ProgramaUsuario> listaProgramaUsuario = query.list(programaUsuario);
        if (listaProgramaUsuario.size() > 0)
        {
            return listaProgramaUsuario.get(0);
        }
        return null;

    }

    @Override
    public List<Long> getProgramasIdByPersonaIdAndTipoAcceso(Long personaId, String tipoAcceso) {
        List<ProgramaUsuario> programasUsuarios = getProgramasUsuariosByPersonaIdAndTipoAcceso(
                personaId, tipoAcceso);
        List<Long> programasPersonaConectadaIds = new ArrayList<Long>();
        for (ProgramaUsuario programaUsuario : programasUsuarios)
        {
            programasPersonaConectadaIds.add(programaUsuario.getPrograma().getId());
        }
        return programasPersonaConectadaIds;
    }

    @Override
    public List<Long> getProgramasIdByPersonaId(Long personaId) {
        List<ProgramaUsuario> programasUsuarios = getProgramasUsuariosByPersonaId(personaId);
        List<Long> programasPersonaConectadaIds = new ArrayList<Long>();
        for (ProgramaUsuario programaUsuario : programasUsuarios)
        {
            programasPersonaConectadaIds.add(programaUsuario.getPrograma().getId());
        }
        return programasPersonaConectadaIds;
    }

}