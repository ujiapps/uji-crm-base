package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaCampanya;
import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.commons.db.BaseDAO;

public interface CampanyaCampanyaDAO extends BaseDAO {
    List<CampanyaCampanya> getCampanyasCampanyasDestinoByCampanya(Campanya Campanya);

    List<CampanyaCampanya> getCampanyasCampanyasOrigenByCampanyaId(Campanya Campanya);

    CampanyaCampanya getCampanyaVinculada(Campanya campanya, TipoEstadoCampanya estado);
}