package es.uji.apps.crm.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QCliente;
import es.uji.apps.crm.model.QClienteCirculo;
import es.uji.apps.crm.model.QGrupo;
import es.uji.apps.crm.model.QItem;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.UIEntity;

@Repository
public class ClienteCirculoDAO extends BaseDAODatabaseImpl {
    public List<UIEntity> getItemsClientesSuscripcionesByCliente(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteCirculo clienteCirculo = QClienteCirculo.clienteCirculo;
        QItem item = QItem.item;
        QGrupo grupo = QGrupo.grupo;
        QCliente cliente = QCliente.cliente;

        query.from(clienteCirculo)
                .join(clienteCirculo.cliente, cliente).fetch()
                .join(clienteCirculo.item, item).fetch()
                .join(item.grupo, grupo).fetch()
                .where(clienteCirculo.cliente.id.eq(clienteId));
        return query.list(clienteCirculo).stream().map(campanyaClienteItem -> {
            UIEntity entity = UIEntity.toUI(campanyaClienteItem);
            entity.put("nombre", campanyaClienteItem.getItem().getNombreCa());
            entity.put("tipo", "AUTO");

            if (campanyaClienteItem.getCliente() != null)
            {
                entity.put("clienteId", campanyaClienteItem.getCliente().getId());
            }
            if (campanyaClienteItem.getItem() != null && campanyaClienteItem.getItem().getGrupo() != null)
            {
                entity.put("grupoId", campanyaClienteItem.getItem().getGrupo().getId());
                entity.put("grupoNombre", campanyaClienteItem.getItem().getGrupo().getNombreCa());
            }
            return entity;
        }).collect(Collectors.toList());
    }
}