package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.SeguimientoFichero;
import es.uji.commons.db.BaseDAO;

public interface SeguimientoFicheroDAO extends BaseDAO {

    List<SeguimientoFichero> getSeguimientoFicherosByCampanyaSeguimientoId(
            Long campanyaSeguimientoId);

    SeguimientoFichero getSeguimientoFicheroById(Long seguimientoFicheroId);

}