package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.QCampanya;
import es.uji.apps.crm.model.QCampanyaActo;
import es.uji.apps.crm.model.QCampanyaCliente;
import es.uji.apps.crm.model.QCampanyaTexto;
import es.uji.apps.crm.model.QPersonaPrograma;
import es.uji.apps.crm.model.QPrograma;
import es.uji.apps.crm.model.QProgramaUsuario;
import es.uji.apps.crm.model.QTipoEstadoCampanya;
import es.uji.apps.crm.model.QTipoTarifa;
import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.apps.crm.model.domains.TipoAccesoProgramaUsuario;
import es.uji.apps.crm.model.domains.TipoAfiliacionUsuarioCampanya;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CampanyaDAO extends BaseDAODatabaseImpl {

    public List<Campanya> getCampanyas() {

        JPAQuery query = new JPAQuery(entityManager);

        QCampanya campanya = new QCampanya("campanya");
        QCampanya campanyaRelacionada = new QCampanya("campanyaRelacionada");
        QPrograma programa = QPrograma.programa;

        query.from(campanya).join(campanya.programa, programa).fetch()
                .leftJoin(campanya.campanya, campanyaRelacionada).fetch()
                .orderBy(campanya.nombre.asc());

        return query.list(campanya);
    }

    public List<Campanya> getCampanyasAdmin(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanya campanya = new QCampanya("campanya");
        QCampanya campanyaRelacionada = new QCampanya("campanyaRelacionada");
        QPrograma programa = QPrograma.programa;
        QPersonaPrograma personaPrograma = QPersonaPrograma.personaPrograma;

        return query
                .from(campanya)
                .join(campanya.programa, programa)
                .fetch()
                .leftJoin(campanya.campanya, campanyaRelacionada)
                .fetch()
                .join(programa.personaProgramas, personaPrograma)
                .where(personaPrograma.tipoAcceso.eq(TipoAccesoProgramaUsuario.ADMIN.getNombre())
                        .and(personaPrograma.persona.id.eq(connectedUserId)))
                .orderBy(campanya.nombre.asc()).list(campanya);
    }

    public List<Campanya> getCampanyasUser(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanya campanya = new QCampanya("campanya");
        QCampanya campanyaRelacionada = new QCampanya("campanyaRelacionada");
        QPrograma programa = QPrograma.programa;
        QPersonaPrograma personaPrograma = QPersonaPrograma.personaPrograma;

        return query.from(campanya).join(campanya.programa, programa).fetch()
                .leftJoin(campanya.campanya, campanyaRelacionada).fetch()
                .join(programa.personaProgramas, personaPrograma)
                .where(personaPrograma.persona.id.eq(connectedUserId))
                .orderBy(campanya.nombre.asc()).list(campanya);
    }

    public List<Campanya> getCampanyasByServicio(Long connectedUserId, Long servicioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanya qCampanya = QCampanya.campanya1;
        QPrograma qPrograma = QPrograma.programa;

        return query.from(qCampanya).join(qCampanya.programa, qPrograma).fetch()
                .where(qPrograma.servicio.id.eq(servicioId))
                .orderBy(qCampanya.nombre.asc()).list(qCampanya);
    }

    public Campanya getCampanyaById(Long campanyaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QCampanya campanya = QCampanya.campanya1;
        QCampanya campanyaRelacionada = new QCampanya("campanyaRelacionada");
        QCampanyaTexto campanyaTexto = QCampanyaTexto.campanyaTexto;
        QPrograma programa = QPrograma.programa;

        query.from(campanya).join(campanya.programa, programa).fetch()
                .leftJoin(campanya.campanya, campanyaRelacionada).fetch()
                .leftJoin(campanya.campanyaTexto, campanyaTexto).fetch()
                .where(campanya.id.eq(campanyaId))
                .orderBy(campanya.nombre.asc());

        List<Campanya> lista = query.list(campanya);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

    public List<Campanya> getCampanyasByClienteId(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaCliente campanyaCliente = QCampanyaCliente.campanyaCliente;
        QCampanya campanya = QCampanya.campanya1;

        query.from(campanyaCliente).join(campanyaCliente.campanya, campanya)
                .where(campanyaCliente.cliente.id.eq(clienteId))
                .orderBy(campanya.nombre.asc());

        return query.list(campanya);
    }

    public List<Campanya> getCampanyasAdminTree(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanya campanya = new QCampanya("campanya");
        QPrograma programa = QPrograma.programa;
        QPersonaPrograma personaPrograma = QPersonaPrograma.personaPrograma;

        return query
                .from(campanya)
                .join(campanya.programa, programa)
                .join(programa.personaProgramas, personaPrograma)
                .where(personaPrograma.tipoAcceso.eq(TipoAccesoProgramaUsuario.ADMIN.getNombre())
                        .and(personaPrograma.persona.id.eq(connectedUserId).and(
                                campanya.campanya.isNull()))).orderBy(campanya.nombre.asc()).distinct()
                .list(campanya);
    }

    public List<Campanya> getCampanyasHijas(Long connectedUserId, Campanya campanya) {

        JPAQuery query = new JPAQuery(entityManager);

        QCampanya qCampanya = new QCampanya("campanya");
        QPrograma programa = QPrograma.programa;

        QPersonaPrograma personaPrograma = QPersonaPrograma.personaPrograma;

        return query
                .from(qCampanya)
                .join(qCampanya.programa, programa)
                .fetch()
                .join(programa.personaProgramas, personaPrograma)
                .where(personaPrograma.persona.id.eq(connectedUserId)
                        .and(qCampanya.campanya.id.eq(campanya.getId()))).distinct()
                .orderBy(qCampanya.nombre.asc()).list(qCampanya);

    }

    public Campanya getCampanyaByActoId(Long actoId) {

        JPAQuery query = new JPAQuery(entityManager);
        QCampanya campanya = QCampanya.campanya1;
        QCampanyaActo campanyaActo = QCampanyaActo.campanyaActo;

        query.from(campanya).join(campanya.campanyaActos, campanyaActo)
                .where(campanyaActo.id.eq(actoId));

        List<Campanya> lista = query.list(campanya);

        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;

    }

    public List<Campanya> getCampanyasUserConTarifa(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanya campanya = new QCampanya("campanya");
        QPrograma programa = QPrograma.programa;
        QPersonaPrograma personaPrograma = QPersonaPrograma.personaPrograma;
        QTipoTarifa tipoTarifa = QTipoTarifa.tipoTarifa;

        query.from(campanya).join(campanya.programa, programa).fetch()
                .join(programa.personaProgramas, personaPrograma)
                .join(campanya.campanyasTarifas, tipoTarifa)
                .where(personaPrograma.persona.id.eq(connectedUserId))
                .orderBy(campanya.nombre.asc()).distinct();

        return query.list(campanya);
    }

    public TipoEstadoCampanya getEstadoCampanyaByCampanya(Campanya campanya, TipoAfiliacionUsuarioCampanya tipo) {

        QTipoEstadoCampanya tipoEstadoCampanya = QTipoEstadoCampanya.tipoEstadoCampanya;
        QCampanya qCampanya = QCampanya.campanya1;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(tipoEstadoCampanya).join(tipoEstadoCampanya.campanya, qCampanya)
                .where(tipoEstadoCampanya.accion.eq(tipo.getTipo())
                        .and(qCampanya.id.eq(campanya.getId())));

        List<TipoEstadoCampanya> lista = query.list(tipoEstadoCampanya);

        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        else
        {
            return null;
        }
    }

    public List<Campanya> getCampanyasByClienteIdAndAccion(Long clienteId, String accion) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaCliente campanyaCliente = QCampanyaCliente.campanyaCliente;
        QCampanya campanya = QCampanya.campanya1;
        QTipoEstadoCampanya estadoCampanya = QTipoEstadoCampanya.tipoEstadoCampanya;

        query.from(campanyaCliente).join(campanyaCliente.campanya, campanya).join(campanyaCliente.campanyaClienteTipo, estadoCampanya)
                .where(campanyaCliente.cliente.id.eq(clienteId).and(estadoCampanya.accion.eq(accion)))
                .orderBy(campanya.nombre.asc());
        return query.list(campanya);
    }

    public List<Campanya> getCampanyasByClienteIdAndCampanyaPadreId(Long clienteId, Long campanyaPadreId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanya campanya = QCampanya.campanya1;
        QCampanyaCliente campanyaCliente = QCampanyaCliente.campanyaCliente;

        query.from(campanya).join(campanya.campanyasClientes, campanyaCliente).fetch()
                .where(campanya.campanya.id.eq(campanyaPadreId)
                        .and(campanyaCliente.cliente.id.eq(clienteId)));
        return query.list(campanya);
    }

    public boolean tieneHijos(Long campanyaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QCampanya campanya = QCampanya.campanya1;
        query.from(campanya).where(campanya.campanya.id.eq(campanyaId));
        if (query.list(campanya).size() > 0)
        {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public List<Campanya> getCampanyasAdminRoot(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanya campanya = QCampanya.campanya1;
        QPrograma programa = QPrograma.programa;
        QProgramaUsuario programaUsuario = QProgramaUsuario.programaUsuario;

        return query
                .from(campanya)
                .join(campanya.programa, programa)
                .join(programa.programasUsuarios, programaUsuario)
                .where((programaUsuario.tipo.eq(TipoAccesoProgramaUsuario.ADMIN.getNombre()).or(programaUsuario.tipo.eq(TipoAccesoProgramaUsuario.ACTOS.getNombre())))
                        .and(programaUsuario.personaPasPdi.id.eq(connectedUserId)).and(campanya.campanya.id.isNull())).distinct()
                .orderBy(campanya.nombre.asc()).list(campanya);
    }

    public List<Campanya> getCampanyasRoot(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanya campanya = new QCampanya("campanya");
        QPrograma programa = QPrograma.programa;
        QPersonaPrograma personaPrograma = QPersonaPrograma.personaPrograma;

        return query
                .from(campanya)
                .join(campanya.programa, programa)
                .join(programa.personaProgramas, personaPrograma)
                .where(personaPrograma.persona.id.eq(connectedUserId).and(campanya.campanya.id.isNull())).distinct()
                .orderBy(campanya.nombre.asc()).list(campanya);
    }

    public List<Campanya> getCampanyasAdminHijas(Long connectedUserId, Campanya campanya) {

        JPAQuery query = new JPAQuery(entityManager);

        QCampanya qCampanya = new QCampanya("campanya");
        QPrograma programa = QPrograma.programa;

        QPersonaPrograma personaPrograma = QPersonaPrograma.personaPrograma;

        return query
                .from(qCampanya)
                .join(qCampanya.programa, programa)
                .fetch()
                .join(programa.personaProgramas, personaPrograma)
                .where((personaPrograma.tipoAcceso.eq(TipoAccesoProgramaUsuario.ADMIN.getNombre()).or(personaPrograma.tipoAcceso.eq(TipoAccesoProgramaUsuario.ACTOS.getNombre())))
                        .and(personaPrograma.persona.id.eq(connectedUserId))
                        .and(qCampanya.campanya.id.eq(campanya.getId()))).distinct()
                .orderBy(qCampanya.nombre.asc()).list(qCampanya);

    }

}