package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.ItemItem;
import es.uji.commons.db.BaseDAO;

public interface ItemItemDAO extends BaseDAO {

    List<ItemItem> getItemsItemsByItemId(Item item);

    List<ItemItem> getItemsItemsOrigenByItemId(Item item);

}