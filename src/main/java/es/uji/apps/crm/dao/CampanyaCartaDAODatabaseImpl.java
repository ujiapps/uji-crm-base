package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.CampanyaCarta;
import es.uji.apps.crm.model.QCampanyaCarta;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CampanyaCartaDAODatabaseImpl extends BaseDAODatabaseImpl implements CampanyaCartaDAO {

    QCampanyaCarta campanyaCarta = QCampanyaCarta.campanyaCarta;

    @Override
    public List<CampanyaCarta> getCampanyaCartasByCampanyaId(Long campanyaId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(campanyaCarta).where(campanyaCarta.campanya.id.eq(campanyaId));

        return query.list(campanyaCarta);
    }

    @Override
    public CampanyaCarta getCampanyaCartaByCampanyaIdAndTipoId(Long campanyaId, Long tipoId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(campanyaCarta).where(
                campanyaCarta.campanya.id.eq(campanyaId).and(
                        campanyaCarta.campanyaCartaTipo.id.eq(tipoId)));

        List<CampanyaCarta> result = query.list(campanyaCarta);

        if (result.size() > 0)
        {
            return result.get(0);
        }
        return null;

    }

    @Override
    public List<CampanyaCarta> getCampanyaCartasByCampanyaIdAndTipoId(Long campanyaId, Long estadoId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(campanyaCarta).where(campanyaCarta.campanya.id.eq(campanyaId).and(campanyaCarta.campanyaCartaTipo.id.eq(estadoId)));

        return query.list(campanyaCarta);
    }
}