package es.uji.apps.crm.dao;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

@Component
public class TratamientosRGPD {
    private static DataSource dataSource;
    private VisualizaTratamientoPLSQL tablas;
    private RegistraTratamientoPLSQL registro;
    private EstaRegistradoTratamientoPLSQL estaRegistrado;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        TratamientosRGPD.dataSource = dataSource;
    }

    @PostConstruct
    public void init() {
        this.tablas = new VisualizaTratamientoPLSQL(dataSource);
        this.registro = new RegistraTratamientoPLSQL(dataSource);
        this.estaRegistrado = new EstaRegistradoTratamientoPLSQL(dataSource);
    }

    public String visualizaTratamientoPLSQL(String codigo, String tipo, String idioma) {

        return tablas.visualizaTratamientoPLSQL(codigo, tipo, idioma);
    }

    public String registraTratamientoPLSQL(String codigo, Long persona) {

        return registro.registraTratamientoPLSQL(codigo, persona);
    }

    public String estaRegistradoTratamientoPLSQL(String codigo, Long persona) {
        return estaRegistrado.estaRegistradoTratamientoPLSQL(codigo, persona);
    }

    private class VisualizaTratamientoPLSQL extends StoredProcedure {
        private static final String SQL = "gri_per.genera_tratamiento_rgpd";
        private static final String PCODIGO = "p_codigo";
        private static final String PTIPO = "p_tipo";
        private static final String PIDIOMA = "p_idioma";

        public VisualizaTratamientoPLSQL(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(true);
            setSql(SQL);
            declareParameter(new SqlOutParameter("response", Types.VARCHAR));
            declareParameter(new SqlParameter(PCODIGO, Types.VARCHAR));
            declareParameter(new SqlParameter(PTIPO, Types.VARCHAR));
            declareParameter(new SqlParameter(PIDIOMA, Types.VARCHAR));
            compile();
        }

        public String visualizaTratamientoPLSQL(String codigo, String tipo, String idioma) {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PCODIGO, codigo);
            inParams.put(PTIPO, tipo);
            inParams.put(PIDIOMA, idioma);

            Map<String, Object> results = execute(inParams);
            return results.get("response").toString();
        }
    }

    private class RegistraTratamientoPLSQL extends StoredProcedure {
        private static final String SQL = "gri_per.registra_tratamiento_rgpd";
        private static final String PCODIGO = "p_cod_tratamiento";
        private static final String PPERSONA = "p_perid";
        private static final String PCURSO = "p_curso_aca";

        public RegistraTratamientoPLSQL(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(true);
            setSql(SQL);
            declareParameter(new SqlOutParameter("response", Types.VARCHAR));
            declareParameter(new SqlParameter(PCODIGO, Types.VARCHAR));
            declareParameter(new SqlParameter(PPERSONA, Types.BIGINT));
            declareParameter(new SqlParameter(PCURSO, Types.BIGINT));
            compile();
        }

        public String registraTratamientoPLSQL(String codigo, Long persona) {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PCODIGO, codigo);
            inParams.put(PPERSONA, persona);
            inParams.put(PCURSO, -1L);

            Map<String, Object> results = execute(inParams);
            return results.get("response").toString();
        }
    }

    private class EstaRegistradoTratamientoPLSQL extends StoredProcedure {
        private static final String SQL = "gri_per.is_reg_trat_rgpd";
        private static final String PCODIGO = "p_cod_tratamiento";
        private static final String PPERSONA = "p_perid";
        private static final String PCURSO = "p_curso_aca";

        public EstaRegistradoTratamientoPLSQL(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(true);
            setSql(SQL);
            declareParameter(new SqlOutParameter("response", Types.VARCHAR));
            declareParameter(new SqlParameter(PCODIGO, Types.VARCHAR));
            declareParameter(new SqlParameter(PPERSONA, Types.BIGINT));
            declareParameter(new SqlParameter(PCURSO, Types.BIGINT));
            compile();
        }

        public String estaRegistradoTratamientoPLSQL(String codigo, Long persona) {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PCODIGO, codigo);
            inParams.put(PPERSONA, persona);
            inParams.put(PCURSO, -1L);

            Map<String, Object> results = execute(inParams);
            return results.get("response").toString();
        }
    }

}