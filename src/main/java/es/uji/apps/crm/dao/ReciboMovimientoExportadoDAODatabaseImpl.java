package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QReciboMovimientoExportado;
import es.uji.apps.crm.model.ReciboMovimientoExportado;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ReciboMovimientoExportadoDAODatabaseImpl extends BaseDAODatabaseImpl implements ReciboMovimientoExportadoDAO {

    @Override
    public List<ReciboMovimientoExportado> getReciboMovimientoExportado(Long movimientoId) {

        JPAQuery query = new JPAQuery(entityManager);

        QReciboMovimientoExportado reciboMovimientoExportado = QReciboMovimientoExportado.reciboMovimientoExportado;

        query.from(reciboMovimientoExportado)
                .where(reciboMovimientoExportado.reciboMovimiento.id.eq(movimientoId));

        return query.list(reciboMovimientoExportado);

    }

    @Override
    public ReciboMovimientoExportado getReciboMovimientoExportadoById(Long reciboMovimientoExportadoId) {
        JPAQuery query = new JPAQuery(entityManager);

        QReciboMovimientoExportado reciboMovimientoExportado = QReciboMovimientoExportado.reciboMovimientoExportado;
        query.from(reciboMovimientoExportado)
                .where(reciboMovimientoExportado.reciboMovimiento.id.eq(reciboMovimientoExportadoId)
                        .and(reciboMovimientoExportado.valido.isTrue()));

        List<ReciboMovimientoExportado> lista = query.list(reciboMovimientoExportado);

        if (lista.isEmpty())
        {
            return null;
        }
        else
        {
            return lista.get(0);
        }

    }
}