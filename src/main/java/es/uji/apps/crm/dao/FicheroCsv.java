package es.uji.apps.crm.dao;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

@Component
public class FicheroCsv {
    private static DataSource dataSource;
    private AltaCampanya ficheroCsv;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        FicheroCsv.dataSource = dataSource;
    }

    public void init() {
        this.ficheroCsv = new AltaCampanya(dataSource);
    }

    public void altaCampanya(Long connectedUserId, Long campanyaId, Boolean nacimiento, Boolean uji, Boolean direccionPostal, Boolean nacionalidad, Long estado, Boolean correoPersonal, Boolean zonaPrivada) {
        ficheroCsv.altaCampanya(connectedUserId, campanyaId, nacimiento, uji, direccionPostal, nacionalidad, estado, correoPersonal, zonaPrivada);
    }

    private class AltaCampanya extends StoredProcedure {
        private static final String SQL = "alta_campanyas.procesar_fichero_csv";
        private static final String PPERSONA = "p_persona";
        private static final String PCAMPANYA = "p_campanya";
        private static final String PNACIMIENTO = "p_nacimiento";
        private static final String PUJI = "p_uji";
        private static final String PDIRECCIONPOSTAL = "p_direccion_postal";
        private static final String PNACIONALIDAD = "p_nacionalidad";
        private static final String PESTADO = "p_estado";
        private static final String PCORREOPERSONAL = "p_correo_personal";
        private static final String PZONAPRIVADA = "p_zona_privada";

        public AltaCampanya(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(PPERSONA, Types.BIGINT));
            declareParameter(new SqlParameter(PCAMPANYA, Types.BIGINT));
            declareParameter(new SqlParameter(PNACIMIENTO, Types.BIGINT));
            declareParameter(new SqlParameter(PUJI, Types.BIGINT));
            declareParameter(new SqlParameter(PDIRECCIONPOSTAL, Types.BIGINT));
            declareParameter(new SqlParameter(PNACIONALIDAD, Types.BIGINT));
            declareParameter(new SqlParameter(PESTADO, Types.BIGINT));
            declareParameter(new SqlParameter(PCORREOPERSONAL, Types.BIGINT));
            declareParameter(new SqlParameter(PZONAPRIVADA, Types.BIGINT));

            compile();
        }

        public void altaCampanya(Long connectedUserId, Long campanyaId, Boolean nacimiento, Boolean uji, Boolean direccionPostal, Boolean nacionalidad, Long estado, Boolean correoPersonal, Boolean zonaPrivada) {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PPERSONA, connectedUserId);
            inParams.put(PCAMPANYA, campanyaId);
            inParams.put(PNACIMIENTO, nacimiento);
            inParams.put(PUJI, uji);
            inParams.put(PDIRECCIONPOSTAL, direccionPostal);
            inParams.put(PNACIONALIDAD, nacionalidad);
            inParams.put(PESTADO, estado);
            inParams.put(PCORREOPERSONAL, correoPersonal);
            inParams.put(PZONAPRIVADA, zonaPrivada);

            Map<String, Object> results = execute(inParams);
        }
    }

}