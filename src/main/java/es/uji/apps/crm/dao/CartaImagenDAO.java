package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.CartaImagen;
import es.uji.commons.db.BaseDAO;

public interface CartaImagenDAO extends BaseDAO {
    List<CartaImagen> getCartaImagenes();

    List<CartaImagen> getCartaImagenesByCampanyaCartaId(Long campanyaCartaId);

    CartaImagen getCartaImagenById(Long cartaImagenId);
}