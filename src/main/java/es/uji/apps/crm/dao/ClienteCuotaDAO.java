package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.ClienteCuota;
import es.uji.apps.crm.model.ClienteTarifa;
import es.uji.commons.db.BaseDAO;

public interface ClienteCuotaDAO extends BaseDAO {
    List<ClienteCuota> getClienteCuotasByClienteTarifaId(Long clienteTarifa);

//    void deleteCuotasByTarifa(Cliente cliente, TipoTarifa tipoTarifa);

    ClienteCuota getClienteCuotaById(Long clienteCuotaId);

    void deleteClienteCuotasByClienteTarifa(ClienteTarifa clienteTarifa);
}