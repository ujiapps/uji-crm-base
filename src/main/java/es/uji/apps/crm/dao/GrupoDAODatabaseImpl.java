package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Grupo;
import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.QCampanya;
import es.uji.apps.crm.model.QCampanyaItem;
import es.uji.apps.crm.model.QEnvioItem;
import es.uji.apps.crm.model.QGrupo;
import es.uji.apps.crm.model.QItem;
import es.uji.apps.crm.model.QPersonaPrograma;
import es.uji.apps.crm.model.QPrograma;
import es.uji.apps.crm.model.QTipoAcceso;
import es.uji.apps.crm.model.domains.TipoAccesoProgramaUsuario;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class GrupoDAODatabaseImpl extends BaseDAODatabaseImpl implements GrupoDAO {
    QGrupo grupo = QGrupo.grupo;
    QPrograma programa = QPrograma.programa;
    QTipoAcceso tipoAcceso = QTipoAcceso.tipoAcceso;
    QCampanya qCampanya = QCampanya.campanya1;
    QItem item = QItem.item;
    QEnvioItem envioItem = QEnvioItem.envioItem;
    QCampanyaItem campanyaItem = QCampanyaItem.campanyaItem;
    QPersonaPrograma personaPrograma = QPersonaPrograma.personaPrograma;
    @Autowired
    private ItemDAO itemDAO;

    @Override
    public List<Grupo> getGrupos(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(grupo)
                .join(grupo.programa, programa).fetch()
                .join(grupo.tipoAcceso, tipoAcceso).fetch()
                .join(programa.personaProgramas, personaPrograma)
                .where(personaPrograma.persona.id.eq(connectedUserId)
                        .and(personaPrograma.tipoAcceso.eq(TipoAccesoProgramaUsuario.ADMIN.getNombre())));

        return query.orderBy(grupo.orden.asc()).orderBy(grupo.nombreCa.asc()).list(grupo);
    }

    @Override
    public List<Grupo> getGruposByProgramaId(Long connectedUserId, Long programaId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(grupo)
                .join(grupo.programa, programa).fetch()
                .join(programa.personaProgramas, personaPrograma)
                .where(grupo.programa.id.eq(programaId)
                        .and(personaPrograma.tipoAcceso.eq(TipoAccesoProgramaUsuario.ADMIN.getNombre())));

        return query.orderBy(grupo.orden.asc()).orderBy(grupo.nombreCa.asc()).list(grupo);
    }

    @Override
    public List<Grupo> getGruposAsociadosByCampanyaId(Long campanyaId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(grupo)
                .join(grupo.programa, programa)
                .join(programa.campanyas, qCampanya)
                .where(qCampanya.id.eq(campanyaId));

        return query.orderBy(grupo.orden.asc()).orderBy(grupo.nombreCa.asc()).list(grupo);
    }

    @Override
    public Grupo getGrupoById(Long grupoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(grupo)
                .where(grupo.id.eq(grupoId));

        List<Grupo> listaGrupos = query.orderBy(grupo.orden.asc()).orderBy(grupo.nombreCa.asc()).list(grupo);
        if (listaGrupos.size() > 0)
        {
            return listaGrupos.get(0);
        }
        return null;
    }

    @Override
    public List<Grupo> getGruposByCampanyaId(Long grupo_id, Long campanya_id) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(grupo)
                .join(grupo.items, item)
                .join(item.campanyaItems, campanyaItem)
                .where(campanyaItem.campanya.id.eq(campanya_id)
                        .and(grupo.id.eq(grupo_id)));

        return query.orderBy(grupo.orden.asc()).orderBy(grupo.nombreCa.asc()).list(grupo);
    }

    @Override
    public List<Grupo> getGruposByEnvioId(Long grupo_id, Long envioId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(grupo)
                .join(grupo.items, item)
                .join(item.enviosItems, envioItem)
                .where(envioItem.envio.id.eq(envioId)
                        .and(grupo.id.eq(grupo_id)));

        return query.orderBy(grupo.orden.asc()).orderBy(grupo.nombreCa.asc()).list(grupo);
    }

    @Override
    public List<Grupo> getGruposFaltanByClienteId(Long connectedUserId, Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        List<Item> lista = itemDAO.getItemsByClienteId(clienteId);

        if (lista.isEmpty())
        {
            return getGrupos(connectedUserId);
        }
        else
        {

            query.from(grupo)
                    .join(grupo.items, item).fetch()
                    .join(grupo.programa, programa).fetch()
                    .join(grupo.tipoAcceso, tipoAcceso).fetch()
                    .join(programa.personaProgramas, personaPrograma)
                    .where(personaPrograma.persona.id.eq(connectedUserId)
                            .and(personaPrograma.tipoAcceso.eq(TipoAccesoProgramaUsuario.ADMIN.getNombre())
                                    .and(item.notIn(lista)))).distinct();
        }

        return query.orderBy(grupo.orden.asc()).orderBy(grupo.nombreCa.asc()).list(grupo);
    }

}