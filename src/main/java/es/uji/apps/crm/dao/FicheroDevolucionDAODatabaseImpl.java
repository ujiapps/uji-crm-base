package es.uji.apps.crm.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.FicheroDevolucion;
import es.uji.apps.crm.model.QFicheroDevolucion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class FicheroDevolucionDAODatabaseImpl extends BaseDAODatabaseImpl implements FicheroDevolucionDAO {

    @Override
    public List<FicheroDevolucion> getFicherosDevolucion() {
        JPAQuery query = new JPAQuery(entityManager);
        QFicheroDevolucion ficheroDevolucion = QFicheroDevolucion.ficheroDevolucion;

        query.from(ficheroDevolucion);
        return query.list(ficheroDevolucion.id, ficheroDevolucion.nombreFichero, ficheroDevolucion.fecha)
                .stream().map(fichero -> new FicheroDevolucion(fichero.get(ficheroDevolucion.id), fichero.get(ficheroDevolucion.nombreFichero), fichero.get(ficheroDevolucion.fecha)))
                .collect(Collectors.toList());
    }
}
