package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaCampanya;
import es.uji.apps.crm.model.QCampanyaCampanya;
import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CampanyaCampanyaDAODatabaseImpl extends BaseDAODatabaseImpl implements
        CampanyaCampanyaDAO {

    @Override
    public List<CampanyaCampanya> getCampanyasCampanyasDestinoByCampanya(Campanya campanya) {
        QCampanyaCampanya CampanyaCampanya = QCampanyaCampanya.campanyaCampanya;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(CampanyaCampanya).where(CampanyaCampanya.campanyaOrigen.id.eq(campanya.getId()));

        return query.list(CampanyaCampanya);

    }

    @Override
    public List<CampanyaCampanya> getCampanyasCampanyasOrigenByCampanyaId(Campanya campanya) {
        QCampanyaCampanya CampanyaCampanya = QCampanyaCampanya.campanyaCampanya;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(CampanyaCampanya)
                .where(CampanyaCampanya.campanyaDestino.id.eq(campanya.getId()));

        return query.list(CampanyaCampanya);
    }

    @Override
    public CampanyaCampanya getCampanyaVinculada(Campanya campanya, TipoEstadoCampanya estado) {

        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaCampanya qCampanyaCampanya = QCampanyaCampanya.campanyaCampanya;

        query.from(qCampanyaCampanya).where(
                qCampanyaCampanya.campanyaOrigen.eq(campanya).and(
                        qCampanyaCampanya.estadoOrigen.eq(estado)));

        List<CampanyaCampanya> campanyas = query.list(qCampanyaCampanya);

        if (campanyas.size() > 0)
        {
            return campanyas.get(0);
        }
        return null;

    }
}