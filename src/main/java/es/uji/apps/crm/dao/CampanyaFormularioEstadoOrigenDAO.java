package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.CampanyaFormulario;
import es.uji.apps.crm.model.CampanyaFormularioEstadoOrigen;
import es.uji.commons.db.BaseDAO;

public interface CampanyaFormularioEstadoOrigenDAO extends BaseDAO {

    List<CampanyaFormularioEstadoOrigen> getCampanyaFormularioEstadoOrigenByFormulario(Long formularioId);

    void deleteCampanyaFormularioEstadoOrigenByCampanyaFormularioId(CampanyaFormulario campanyaFormularioId);
}