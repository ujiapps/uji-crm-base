package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.ui.ClienteDatoOpcionUI;
import es.uji.apps.crm.ui.QClienteDatoOpcionUI;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ClienteDatoOpcion;
import es.uji.apps.crm.model.ClienteDatoTipo;
import es.uji.apps.crm.model.QCampanyaDato;
import es.uji.apps.crm.model.QClienteDatoOpcion;
import es.uji.apps.crm.model.QClienteDatoTipo;
import es.uji.apps.crm.model.domains.TipoDato;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ClienteDatoTipoDAO extends BaseDAODatabaseImpl {

    public List<ClienteDatoTipo> getClientesDatosTipos() {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDatoTipo clienteDatoTipo = QClienteDatoTipo.clienteDatoTipo;

        query.from(clienteDatoTipo);

        return query.list(clienteDatoTipo);
    }

    public ClienteDatoTipo getClienteDatoTipoById(Long clienteDatoTipoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDatoTipo clienteDatoTipo = QClienteDatoTipo.clienteDatoTipo;
        QClienteDatoOpcion clienteDatoOpcion = QClienteDatoOpcion.clienteDatoOpcion;

        query.from(clienteDatoTipo)
                .leftJoin(clienteDatoTipo.clientesDatosOpciones, clienteDatoOpcion).fetch()
                .where(clienteDatoTipo.id.eq(clienteDatoTipoId));

        List<ClienteDatoTipo> lista = query.list(clienteDatoTipo);
        if (!lista.isEmpty()) {
            return lista.get(0);
        }
        return null;
    }

    public List<ClienteDatoOpcion> getClienteDatoOpcionesByTipoId(Long tipoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDatoOpcion clienteDatoOpcion = QClienteDatoOpcion.clienteDatoOpcion;

        query.from(clienteDatoOpcion)
                .where(clienteDatoOpcion.clienteDatoTipo.id.eq(tipoId));

        return query.list(clienteDatoOpcion);
    }

    public List<ClienteDatoOpcionUI> getClienteDatoOpcionesByTipoId(Long tipoId, String idioma) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDatoOpcion clienteDatoOpcion = QClienteDatoOpcion.clienteDatoOpcion;

        query.from(clienteDatoOpcion)
                .where(clienteDatoOpcion.clienteDatoTipo.id.eq(tipoId)).orderBy(clienteDatoOpcion.orden.asc());

        return query.list(new QClienteDatoOpcionUI(clienteDatoOpcion.id,
                idioma.equals("es") ? clienteDatoOpcion.etiquetaES : idioma.equals("en") ? clienteDatoOpcion.etiquetaEN : idioma.equals("uk") ? clienteDatoOpcion.etiquetaEN : clienteDatoOpcion.etiqueta,
                clienteDatoOpcion.valor, clienteDatoOpcion.referencia));

    }

    public List<ClienteDatoTipo> getClientesDatosTiposClientes() {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDatoTipo clienteDatoTipo = QClienteDatoTipo.clienteDatoTipo;

        query.from(clienteDatoTipo).where(
                clienteDatoTipo.clienteDatoTipoTipo.id.notIn(TipoDato.CABECERA.getId(), TipoDato.ENUNCIADO.getId(),
                        TipoDato.ITEMS_SELECCION_MULTIPLE.getId(),
                        TipoDato.ITEMS_SELECCION_UNICA_CHECKS.getId(),
                        TipoDato.ITEMS_SELECCION_UNICA_DESPL.getId(),
                        TipoDato.ITEMS_SELECCION_MULTIPLE_CHECKS.getId()));

        return query.list(clienteDatoTipo);
    }

    public ClienteDatoTipo getTipoDatoEntradasByFormularioId(Long formularioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QClienteDatoTipo clienteDatoTipo = QClienteDatoTipo.clienteDatoTipo;
        QCampanyaDato campanyaDato = QCampanyaDato.campanyaDato1;

        query.from(clienteDatoTipo).join(clienteDatoTipo.campanyasDatos, campanyaDato)
                .where(campanyaDato.campanyaFormulario.id.eq(formularioId)
                        .and(campanyaDato.vinculadoCampanya.isTrue()
                                .and(clienteDatoTipo.clienteDatoTipoTipo.id.eq(20L)).and(campanyaDato.etiqueta.eq("ENTRADAS"))));

        List<ClienteDatoTipo> lista = query.list(clienteDatoTipo);
        if (!lista.isEmpty()) {
            return lista.get(0);

        } else {
            return null;
        }

    }

    public List<ClienteDatoOpcion> getClienteDatoOpcionesByTipoAndTamanyo(ClienteDatoTipo tipo, Long tamanyo) {

        JPAQuery query = new JPAQuery(entityManager);
        QClienteDatoOpcion clienteDatoOpcion = QClienteDatoOpcion.clienteDatoOpcion;

        query.from(clienteDatoOpcion)
                .where(clienteDatoOpcion.clienteDatoTipo.eq(tipo)
                        .and(clienteDatoOpcion.valor.loe(tamanyo)));

        return query.list(clienteDatoOpcion);
    }


}