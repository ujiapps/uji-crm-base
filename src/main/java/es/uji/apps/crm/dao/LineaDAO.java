package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Linea;
import es.uji.commons.db.BaseDAO;

public interface LineaDAO extends BaseDAO {
    List<Linea> getLineasByReciboId(Long reciboId);
}