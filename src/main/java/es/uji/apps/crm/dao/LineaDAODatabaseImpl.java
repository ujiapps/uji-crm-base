package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Linea;
import es.uji.apps.crm.model.QLinea;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class LineaDAODatabaseImpl extends BaseDAODatabaseImpl implements LineaDAO {
    @Override
    public List<Linea> getLineasByReciboId(Long reciboId) {

        JPAQuery query = new JPAQuery(entityManager);
        QLinea linea = QLinea.linea;

        query.from(linea).where(linea.recibo.id.eq(reciboId));

        return query.list(linea);
    }
}