package es.uji.apps.crm.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.crm.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Repository
public class CampanyaClienteDAO extends BaseDAODatabaseImpl {
    private static final Long MAX_RESULTADOS = 1000L;
    private static final Logger log = LoggerFactory.getLogger(CampanyaClienteDAO.class);
    private final QCampanyaCliente campanyaCliente = QCampanyaCliente.campanyaCliente;
    private final QCampanyaClienteBusqueda qCampanyaClienteBusqueda = QCampanyaClienteBusqueda.campanyaClienteBusqueda;
    private final QTipoEstadoCampanya tipoEstadoCampanya = QTipoEstadoCampanya.tipoEstadoCampanya;

    @Autowired
    private UtilsDAO utilsDAO;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public CampanyaClienteDAO() {
        relations.put("clienteNombre", qCampanyaClienteBusqueda.apellidosNombre);
        relations.put("campanyaClienteTipoNombre", qCampanyaClienteBusqueda.estadoCampanya.nombre);
        relations.put("identificacion", qCampanyaClienteBusqueda.identificacion);
        relations.put("fechaEstado", qCampanyaClienteBusqueda.fechaEstado);
    }

    public List<CampanyaClienteBusqueda> getCampanyaClientesByBusqueda(FiltroCliente filtro,
                                                                       Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);

        creaQueryBusqueda(query, filtro);

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    log.error("getCampanyaClientesByBusqueda", e);
                }
            } else {
                query.orderBy(qCampanyaClienteBusqueda.nombreOficial.desc());
            }
        } else
            query.limit(MAX_RESULTADOS);

        return query.list(qCampanyaClienteBusqueda);

    }

    private void creaQueryBusqueda(JPAQuery query, FiltroCliente filtro) {
        query.from(qCampanyaClienteBusqueda).join(qCampanyaClienteBusqueda.estadoCampanya, tipoEstadoCampanya).fetch().distinct();

        anyadeFiltroCampanya(query, filtro.getCampanyaId());
        anyadeFiltroCliente(query, filtro.getClienteId());
        anyadeFiltroClienteCampanyaEstado(query, filtro.getCampanyaClienteTipoId());
        anyadeFiltroEtiqueta(query, filtro.getEtiqueta());
        anyadeFiltroBusquedaGenerica(query, filtro.getBusquedaGenerica());
        anyadeFiltroItems(query, filtro.getItems(), filtro.getItemsOpcion());
        anyadeFiltroSeguimientoTipo(query, filtro.getSeguimientoTipos(), filtro.getCampanyaId(),
                filtro.getSeguimientoTipoOpcion());
    }

    private void anyadeFiltroCampanya(JPAQuery query, Long campanyaId) {
        if (campanyaId != null) {
            query.where(qCampanyaClienteBusqueda.campanya.id.eq(campanyaId));
        }
    }

    private void anyadeFiltroCliente(JPAQuery query, Long clienteId) {
        if (clienteId != null) {
            query.where(qCampanyaClienteBusqueda.cliente.id.eq(clienteId));
        }
    }

    private void anyadeFiltroClienteCampanyaEstado(JPAQuery query, Long estadoCampanyaId) {
        if (estadoCampanyaId != null) {
            query.where(qCampanyaClienteBusqueda.estadoCampanya.id.eq(estadoCampanyaId));
        }
    }

    private void anyadeFiltroEtiqueta(JPAQuery query, String etiqueta) {
        if (ParamUtils.isNotNull(etiqueta)) {

            query.where(utilsDAO.limpiaAcentos(qCampanyaClienteBusqueda.etiquetas).containsIgnoreCase(utilsDAO.limpiaAcentos(etiqueta)));
        }
    }

    private void anyadeFiltroBusquedaGenerica(JPAQuery query, String busqueda) {
        if (ParamUtils.isNotNull(busqueda)) {

            query.where(utilsDAO.limpiaAcentos(qCampanyaClienteBusqueda.estado)
                    .containsIgnoreCase(utilsDAO.limpiaAcentos(busqueda))
                    .or(utilsDAO.limpiaAcentos(qCampanyaClienteBusqueda.busqueda)
                            .containsIgnoreCase(utilsDAO.limpiaAcentos(busqueda)))
            );
        }
    }

    private void anyadeFiltroItems(JPAQuery query, List<Long> items, Long itemsOpcion) {

        if (!items.isEmpty()) {
            QClienteItem qClienteItem = QClienteItem.clienteItem;

            if (itemsOpcion == null || itemsOpcion == -2) {
                for (Long item : items) {
                    query.where(qCampanyaClienteBusqueda.cliente.id.in(new JPASubQuery()
                            .from(qClienteItem)
                            .where(qClienteItem.item.id.eq(item)
                                    .and(qClienteItem.cliente.id
                                            .eq(qCampanyaClienteBusqueda.cliente.id)))
                            .list(qClienteItem.cliente.id)));
                }
            } else if (itemsOpcion == -3) {
                query.leftJoin(qCampanyaClienteBusqueda.cliente.clientesItems, qClienteItem);
                query.where(qClienteItem.item.id.in(items));
            } else {
                query.where(qCampanyaClienteBusqueda.cliente.id.notIn(new JPASubQuery()
                        .from(qClienteItem)
                        .where(qClienteItem.cliente.id.eq(qCampanyaClienteBusqueda.cliente.id).and(
                                qClienteItem.item.id.in(items))).list(qClienteItem.cliente.id)));
            }

        }
    }

    private void anyadeFiltroSeguimientoTipo(JPAQuery query, List<Long> seguimientoTipos,
                                             Long campanyaId, Long seguimientoTipoOpcion) {
        if (!seguimientoTipos.isEmpty()) {
            QSeguimiento qSeguimiento = QSeguimiento.seguimiento;

            if (seguimientoTipoOpcion == null || seguimientoTipoOpcion == -2) {
                for (Long seguimientoTipo : seguimientoTipos) {
                    query.where(qCampanyaClienteBusqueda.cliente.id.in(new JPASubQuery()
                            .from(qSeguimiento)
                            .where(qSeguimiento.campanya.id
                                    .eq(qCampanyaClienteBusqueda.campanya.id).and(
                                            qSeguimiento.seguimientoTipo.id.eq(seguimientoTipo)))
                            .list(qSeguimiento.cliente.id)));
                }
            } else if (seguimientoTipoOpcion == -3) {
                query.join(qCampanyaClienteBusqueda.cliente.seguimientos, qSeguimiento);
                query.where(qSeguimiento.seguimientoTipo.id.in(seguimientoTipos));
                if (campanyaId != null) {
                    query.where(qSeguimiento.campanya.id.eq(campanyaId));
                }
            } else {
                query.where(qCampanyaClienteBusqueda.cliente.id.notIn(new JPASubQuery()
                        .from(qSeguimiento)
                        .where(qSeguimiento.campanya.id.eq(qCampanyaClienteBusqueda.campanya.id)
                                .and(qSeguimiento.seguimientoTipo.id.in(seguimientoTipos)))
                        .list(qSeguimiento.cliente.id)));
            }
        }
    }

    public CampanyaCliente getCampanyaClienteById(Long campanyaClienteId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCliente qCliente = QCliente.cliente;
        QTipoEstadoCampanya tipoEstadoCampanya = QTipoEstadoCampanya.tipoEstadoCampanya;

        query.from(campanyaCliente).join(campanyaCliente.cliente, qCliente).fetch().leftJoin(campanyaCliente.campanyaClienteTipo, tipoEstadoCampanya).fetch()
                .where(campanyaCliente.id.eq(campanyaClienteId));

        List<CampanyaCliente> lista = query.list(campanyaCliente);
        if (!lista.isEmpty()) {
            return lista.get(0);
        }
        return null;
    }

    public CampanyaCliente getCampanyaClienteByCampanyaAndclienteId(Long campanyaId, Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCliente qCliente = QCliente.cliente;
        QCampanya qCampanya = QCampanya.campanya1;
        QTipoEstadoCampanya tipoEstadoCampanya = QTipoEstadoCampanya.tipoEstadoCampanya;

        query.from(campanyaCliente).join(campanyaCliente.cliente, qCliente).fetch()
                .join(campanyaCliente.campanya, qCampanya).fetch()
                .join(campanyaCliente.campanyaClienteTipo, tipoEstadoCampanya).fetch()
                .where(campanyaCliente.campanya.id.eq(campanyaId).and(
                        campanyaCliente.cliente.id.eq(clienteId)));

        List<CampanyaCliente> lista = query.list(campanyaCliente);
        if (!lista.isEmpty()) {
            return lista.get(0);
        }
        return null;

    }

    public List<CampanyaCliente> getCampanyaClienteByClienteId(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCliente qCliente = QCliente.cliente;
        QCampanya qCampanya = QCampanya.campanya1;

        query.from(campanyaCliente).join(campanyaCliente.cliente, qCliente).fetch()
                .join(campanyaCliente.campanya, qCampanya).fetch()
                .join(campanyaCliente.campanyaClienteTipo, tipoEstadoCampanya).fetch()
                .where(campanyaCliente.cliente.id.eq(clienteId));

        return query.list(campanyaCliente);
    }

    public List<CampanyaCliente> getCampanyaClienteByClienteIdAndCampanyaPadreId(Long clienteId, Long campanyaPadreId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanya campanya = QCampanya.campanya1;
        QCampanyaCliente campanyaCliente = QCampanyaCliente.campanyaCliente;

        query.from(campanyaCliente).join(campanyaCliente.campanya, campanya).fetch()
                .where(campanya.campanya.id.eq(campanyaPadreId)
                        .and(campanyaCliente.cliente.id.eq(clienteId)));
        return query.list(campanyaCliente);
    }
}