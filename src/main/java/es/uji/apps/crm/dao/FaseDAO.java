package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Fase;
import es.uji.commons.db.BaseDAO;

public interface FaseDAO extends BaseDAO {
    List<Fase> getFases();

    Fase getFaseById(Long faseId);


}