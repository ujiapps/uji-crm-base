package es.uji.apps.crm.dao;

import es.uji.apps.crm.model.ClienteGeneral;
import es.uji.commons.db.BaseDAO;

public interface ClienteGeneralDAO extends BaseDAO {

    ClienteGeneral getClienteGeneralById(Long clienteGeneral);

    ClienteGeneral getClienteGeneralByIdentificacionAndPersona(String identificacion, Long personaId);

    ClienteGeneral getClienteGeneralByPerId(Long connectedUserId);

    ClienteGeneral getClienteGeneralByIdentificacion(String identificacion);

    ClienteGeneral getClienteGeneralByIdentificacionSinJoinPersona(String identificacion);
}