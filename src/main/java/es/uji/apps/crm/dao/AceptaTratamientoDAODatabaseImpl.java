package es.uji.apps.crm.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.AceptaTratamiento;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.QAceptaTratamiento;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AceptaTratamientoDAODatabaseImpl extends BaseDAODatabaseImpl implements AceptaTratamientoDAO {


    @Override
    public AceptaTratamiento getAceptaTratamientoByClienteAndCodigo(Cliente cliente, String codigotratamiento) {
        QAceptaTratamiento aceptaTratamiento = QAceptaTratamiento.aceptaTratamiento;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(aceptaTratamiento)
                .where(aceptaTratamiento.clienteGeneral.eq(cliente.getClienteGeneral())
                        .and(aceptaTratamiento.codigoTratamiento.eq(codigotratamiento)));

        return query.uniqueResult(aceptaTratamiento);
    }
}