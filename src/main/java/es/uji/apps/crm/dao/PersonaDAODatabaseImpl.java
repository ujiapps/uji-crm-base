package es.uji.apps.crm.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Persona;
import es.uji.apps.crm.model.QCliente;
import es.uji.apps.crm.model.QClienteGeneral;
import es.uji.apps.crm.model.QPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.json.lookup.LookupItem;

@Repository
public class PersonaDAODatabaseImpl extends BaseDAODatabaseImpl implements PersonaDAO {
    @Autowired
    private UtilsDAO utilsDAO;

    @Override
    public List<LookupItem> search(String query) {
        JPAQuery jpaQuery = new JPAQuery(entityManager);
        QPersona persona = QPersona.persona;

        List<LookupItem> result = new ArrayList<LookupItem>();

        if (query != null && !query.isEmpty())
        {
            jpaQuery.from(persona)
                    .where(utilsDAO.limpiaAcentos(persona.busqueda).containsIgnoreCase(utilsDAO.limpiaAcentos(query)));
//                            .or(utilsDAO.limpiaAcentos(persona.apellidosNombreBusqueda).like("%" + utilsDAO.limpiaAcentos(query) + "%")));

            List<Persona> listaPersonas = jpaQuery.list(persona);
            for (Persona personaItem : listaPersonas)
            {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(String.valueOf(personaItem.getId()));
                lookupItem.setNombre(personaItem.getNombre() + " " + personaItem.getApellidos());
                result.add(lookupItem);
            }
        }
        return result;
    }

    @Override
    public Persona getPersonaByIdentificacion(String identificacion) {
        JPAQuery query = new JPAQuery(entityManager);

        QPersona persona = QPersona.persona;
        query.from(persona)
                .where(persona.identificacion.lower().trim().eq(identificacion.toLowerCase().trim()));

        List<Persona> lista = query.list(persona);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
//        List<Persona> listaPersonas = query.list(persona);
//
//        if (listaPersonas.size() > 0) {
//            return listaPersonas.get(0);
//        }
//        return null;
    }

    @Override
    public List<Persona> getPersonas() {
        JPAQuery query = new JPAQuery(entityManager);

        QPersona persona = QPersona.persona;
        query.from(persona);
        return query.list(persona);

    }

    @Override
    public Persona getPersonaByClienteId(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);

        QPersona persona = QPersona.persona;
        QClienteGeneral clienteGeneral = QClienteGeneral.clienteGeneral;
        QCliente cliente = QCliente.cliente;

        query.from(persona)
                .join(persona.clientes, clienteGeneral).fetch()
                .leftJoin(clienteGeneral.clientes, cliente).fetch()
                .where(cliente.id.eq(clienteId));

        List<Persona> listaPersonas = query.list(persona);

        if (listaPersonas.size() > 0)
        {
            return listaPersonas.get(0);
        }
        return null;
    }
}