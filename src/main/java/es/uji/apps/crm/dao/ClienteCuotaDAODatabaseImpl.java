package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ClienteCuota;
import es.uji.apps.crm.model.ClienteTarifa;
import es.uji.apps.crm.model.QClienteCuota;
import es.uji.apps.crm.model.QClienteTarifa;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ClienteCuotaDAODatabaseImpl extends BaseDAODatabaseImpl implements ClienteCuotaDAO {
    @Override
    public List<ClienteCuota> getClienteCuotasByClienteTarifaId(Long clienteTarifa) {

        JPAQuery query = new JPAQuery(entityManager);
        QClienteCuota qClienteCuota = QClienteCuota.clienteCuota;
        QClienteTarifa qClienteTarifa = QClienteTarifa.clienteTarifa;

        query.from(qClienteCuota).join(qClienteCuota.clienteTarifa, qClienteTarifa).fetch()
                .where(qClienteCuota.clienteTarifa.id.eq(clienteTarifa));

        return query.list(qClienteCuota);
    }
//
//    @Transactional
//    @Override
//    public void deleteCuotasByTarifa(Cliente cliente, TipoTarifa tipoTarifa) {
//
//        QClienteCuota clienteCuota = QClienteCuota.clienteCuota;
//
//        JPADeleteClause query = new JPADeleteClause(entityManager, clienteCuota);
//        query.where(clienteCuota.cliente.id.eq(cliente.getId()).and(clienteCuota.tarifa.tipoTarifa.id.eq(tipoTarifa.getId()))).execute();
//    }

    @Override
    public ClienteCuota getClienteCuotaById(Long clienteCuotaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteCuota qClienteCuota = QClienteCuota.clienteCuota;

        query.from(qClienteCuota).where(qClienteCuota.id.eq(clienteCuotaId));
        return query.singleResult(qClienteCuota);

    }

    @Override
    public void deleteClienteCuotasByClienteTarifa(ClienteTarifa clienteTarifa) {
        QClienteCuota clienteCuota = QClienteCuota.clienteCuota;

        JPADeleteClause query = new JPADeleteClause(entityManager, clienteCuota);
        query.where(clienteCuota.clienteTarifa.id.eq(clienteTarifa.getId())).execute();
    }
}