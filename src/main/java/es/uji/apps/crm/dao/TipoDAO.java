package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ClienteDatoOpcion;
import es.uji.apps.crm.model.QClienteDatoOpcion;
import es.uji.apps.crm.model.QTipo;
import es.uji.apps.crm.model.Tipo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoDAO extends BaseDAODatabaseImpl{

    QTipo tipo = QTipo.tipo;

    public Tipo getTiposById(Long tipoId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(tipo).where(tipo.id.eq(tipoId));

        List<Tipo> lista = query.list(tipo);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

    public List<ClienteDatoOpcion> getTiposOpciones(Long tipoId) {
        JPAQuery query = new JPAQuery(entityManager);

        QClienteDatoOpcion clienteDatoOpcion = QClienteDatoOpcion.clienteDatoOpcion;

        query.from(clienteDatoOpcion)
                .where(clienteDatoOpcion.clienteDatoTipo.id.eq(tipoId));

        return query.list(clienteDatoOpcion);
    }

    public List<Tipo> getTipos(String columna) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(tipo)
                .where(tipo.tablaColumna.like(columna))
                .orderBy(tipo.id.asc());

        return query.list(tipo);
    }
}