package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ClienteEstadoCampanyaMotivo;
import es.uji.apps.crm.model.QCampanya;
import es.uji.apps.crm.model.QClienteEstadoCampanyaMotivo;
import es.uji.apps.crm.model.QTipoEstadoCampanya;
import es.uji.apps.crm.model.QTipoEstadoCampanyaMotivo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ClienteEstadoCampanyaMotivoDAODatabaseImpl extends BaseDAODatabaseImpl implements ClienteEstadoCampanyaMotivoDAO {

    @Override
    public ClienteEstadoCampanyaMotivo getClienteEstadoMotivoByCampanyaClienteId(Long campanyaClienteId, Long campanyaEstadoId) {

        QClienteEstadoCampanyaMotivo clienteEstadoCampanyaMotivo = QClienteEstadoCampanyaMotivo.clienteEstadoCampanyaMotivo;
        QTipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivo = QTipoEstadoCampanyaMotivo.tipoEstadoCampanyaMotivo;
        QTipoEstadoCampanya tipoEstadoCampanya = QTipoEstadoCampanya.tipoEstadoCampanya;
        QCampanya campanya = QCampanya.campanya1;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteEstadoCampanyaMotivo)
                .join(clienteEstadoCampanyaMotivo.campanyaEstadoMotivo, tipoEstadoCampanyaMotivo).fetch()
                .join(tipoEstadoCampanyaMotivo.tipoEstadoCampanya, tipoEstadoCampanya).fetch()
                .join(tipoEstadoCampanya.campanya, campanya)
                .where(clienteEstadoCampanyaMotivo.campanyaCliente.id.eq(campanyaClienteId)
                        .and(tipoEstadoCampanyaMotivo.tipoEstadoCampanya.id.eq(campanyaEstadoId)));

        List<ClienteEstadoCampanyaMotivo> lista = query.list(clienteEstadoCampanyaMotivo);

        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }
}