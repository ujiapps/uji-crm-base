package es.uji.apps.crm.dao;

import es.uji.apps.crm.model.AceptaTratamiento;
import es.uji.apps.crm.model.Cliente;
import es.uji.commons.db.BaseDAO;

public interface AceptaTratamientoDAO extends BaseDAO {
    AceptaTratamiento getAceptaTratamientoByClienteAndCodigo(Cliente cliente, String codigotratamiento);
}