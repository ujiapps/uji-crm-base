package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ProgramaPerfil;
import es.uji.apps.crm.model.QPerfil;
import es.uji.apps.crm.model.QPrograma;
import es.uji.apps.crm.model.QProgramaPerfil;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ProgramaPerfilDAODatabaseImpl extends BaseDAODatabaseImpl implements ProgramaPerfilDAO {
    QProgramaPerfil programaPerfil = QProgramaPerfil.programaPerfil;
    QPrograma programa = QPrograma.programa;
    QPerfil perfil = QPerfil.perfil;

    @Override
    public List<ProgramaPerfil> getProgramaPerfilesByProgramaId(Long programaId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(programaPerfil)
                .join(programaPerfil.perfil, perfil).fetch()
                .join(programaPerfil.programa, programa).fetch()
                .where(programaPerfil.programa.id.eq(programaId));

        return query.list(programaPerfil);
    }

    @Override
    public ProgramaPerfil getProgramaPerfilById(Long programaPerfilId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(programaPerfil)
                .join(programaPerfil.perfil, perfil).fetch()
                .join(programaPerfil.programa, programa).fetch()
                .where(programaPerfil.id.eq(programaPerfilId));

        List<ProgramaPerfil> listaProgramaPerfil = query.list(programaPerfil);
        if (listaProgramaPerfil.size() > 0)
        {
            return listaProgramaPerfil.get(0);
        }
        return null;

    }

}