package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.CampanyaDato;
import es.uji.commons.db.BaseDAO;

public interface CampanyaDatoDAO extends BaseDAO {

    CampanyaDato getCampanyaDatoById(Long campanyaDatoId);

    List<CampanyaDato> getCampanyaDatosPrincipalesByFormularioId(Long formularioId);

    List<CampanyaDato> getCampanyaDatosByFormularioId(Long formularioId);

    List<CampanyaDato> getCampanyaDatoRelacionados(Long campanyaDatoId);

    List<CampanyaDato> getCampanyaDatosSinDatoAsociadoByFormularioId(Long campanyaFormularioId);
}