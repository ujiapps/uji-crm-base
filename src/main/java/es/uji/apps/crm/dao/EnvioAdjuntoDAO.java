package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.EnvioAdjunto;
import es.uji.commons.db.BaseDAO;

public interface EnvioAdjuntoDAO extends BaseDAO {
    EnvioAdjunto getEnvioAdjuntoById(Long envioAdjuntoId);

    List<EnvioAdjunto> getEnvioAdjuntosByEnvioId(Long envioId);
}