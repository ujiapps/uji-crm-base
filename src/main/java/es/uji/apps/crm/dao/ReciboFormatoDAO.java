package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.ReciboFormato;
import es.uji.commons.db.BaseDAO;

public interface ReciboFormatoDAO extends BaseDAO {

    List<ReciboFormato> getReciboFormatos();

}