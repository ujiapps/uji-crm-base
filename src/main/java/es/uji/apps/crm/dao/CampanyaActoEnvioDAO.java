package es.uji.apps.crm.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.crm.model.CampanyaActoEnvio;
import es.uji.apps.crm.model.QCampanyaActoEnvio;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

@Repository
public class CampanyaActoEnvioDAO extends BaseDAODatabaseImpl {
    public CampanyaActoEnvio getCampanyasActoEnvioByCampanyaActo(Long campanyaActoId) {
        QCampanyaActoEnvio qCampanyaActoEnvio = QCampanyaActoEnvio.campanyaActoEnvio;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qCampanyaActoEnvio).where(qCampanyaActoEnvio.campanyaActo.id.eq(campanyaActoId));

        return query.singleResult(qCampanyaActoEnvio);
    }

    public CampanyaActoEnvio getCampanyaActoEnvioById(Long campanyaActoEnvioId) {
        QCampanyaActoEnvio qCampanyaActoEnvio = QCampanyaActoEnvio.campanyaActoEnvio;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qCampanyaActoEnvio).where(qCampanyaActoEnvio.id.eq(campanyaActoEnvioId));

        return query.singleResult(qCampanyaActoEnvio);
    }
}
