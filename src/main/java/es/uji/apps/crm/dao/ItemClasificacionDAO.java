package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ItemClasificacion;
import es.uji.apps.crm.model.QItemClasificacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ItemClasificacionDAO extends BaseDAODatabaseImpl {
    public List<ItemClasificacion> getItemClasificacionByClasificacionId(Long clasificacionId) {
        JPAQuery query = new JPAQuery(entityManager);
        QItemClasificacion itemClasificacion = QItemClasificacion.itemClasificacion;

        query.from(itemClasificacion).where(itemClasificacion.clasificacionEstudio.eq(clasificacionId));

        return query.list(itemClasificacion);
    }
}