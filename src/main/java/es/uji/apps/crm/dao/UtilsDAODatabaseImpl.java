package es.uji.apps.crm.dao;

import java.util.Date;

import org.springframework.stereotype.Repository;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.expr.StringExpression;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.StringPath;

import es.uji.apps.crm.model.FiltroFechas;
import es.uji.apps.crm.model.domains.TipoComparacion;
import es.uji.apps.crm.model.domains.TipoFecha;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;

@Repository
public class UtilsDAODatabaseImpl extends BaseDAODatabaseImpl implements UtilsDAO {


    //    Long TODOS = 0L;
    static Long TIENENFECHA = 1L;

//    Long MODIFICADO = 1L;

    static Long Y = 1L;
    static Long O = 2L;
    static Long YNO = 3L;
    static Long ONO = 4L;

    @Override
    public void creaQueryFecha(BooleanBuilder build, FiltroFechas filtro, DateTimePath<Date> fechaColumna) {

        Long tipoComparacion = filtro.getTipoComparacion();
        Date fecha = filtro.getFecha();
        Date fechaMax = filtro.getFechaMax();
        Long tipoFecha = filtro.getTipoFecha();
        Long mes = filtro.getMes();
        Long mesMax = filtro.getMesMax();
        Long anyo = filtro.getAnyo();
        Long anyoMax = filtro.getAnyoMax();
        Long comportamiento = filtro.getComportamiento();
        Long operador = filtro.getOperador();

        if (comportamiento.equals(TIENENFECHA))
        {

            if (ParamUtils.isNotNull(tipoFecha))
            {

                if ((tipoComparacion.equals(TipoComparacion.MAYORQUE.getId()) && operador.equals(Y)) ||
                        (tipoComparacion.equals(TipoComparacion.MAYOROIGUAL.getId()) && operador.equals(Y)) ||
                        (tipoComparacion.equals(TipoComparacion.MENORQUE.getId()) && operador.equals(YNO)) ||
                        (tipoComparacion.equals(TipoComparacion.MENOROIGUAL.getId()) && operador.equals(YNO)))
                {

                    if (tipoFecha.equals(TipoFecha.COMPLETA.getId()))
                    {
                        build.and(fechaColumna.gt(fecha));
                    }
                    if (tipoFecha.equals(TipoFecha.ANUAL.getId()))
                    {
                        build.and(fechaColumna.year().gt(anyo));
                    }
                    if (tipoFecha.equals(TipoFecha.MENSUAL.getId()))
                    {
                        build.and(fechaColumna.month().gt(mes));
                    }
                }

                if ((tipoComparacion.equals(TipoComparacion.MAYORQUE.getId()) && operador.equals(O)) ||
                        (tipoComparacion.equals(TipoComparacion.MAYOROIGUAL.getId()) && operador.equals(O)) ||
                        (tipoComparacion.equals(TipoComparacion.MENORQUE.getId()) && operador.equals(ONO)) ||
                        (tipoComparacion.equals(TipoComparacion.MENOROIGUAL.getId()) && operador.equals(ONO)))
                {

                    if (tipoFecha.equals(TipoFecha.COMPLETA.getId()))
                    {
                        build.or(fechaColumna.gt(fecha));
                    }
                    if (tipoFecha.equals(TipoFecha.ANUAL.getId()))
                    {
                        build.or(fechaColumna.year().gt(anyo));
                    }
                    if (tipoFecha.equals(TipoFecha.MENSUAL.getId()))
                    {
                        build.or(fechaColumna.month().gt(mes));
                    }
                }

                if ((tipoComparacion.equals(TipoComparacion.MENORQUE.getId()) && operador.equals(Y)) ||
                        (tipoComparacion.equals(TipoComparacion.MENOROIGUAL.getId()) && (operador.equals(Y))) ||
                        (tipoComparacion.equals(TipoComparacion.MAYORQUE.getId()) && operador.equals(YNO)) ||
                        (tipoComparacion.equals(TipoComparacion.MAYOROIGUAL.getId()) && operador.equals(YNO)))
                {

                    if (tipoFecha.equals(TipoFecha.COMPLETA.getId()))
                    {
                        build.and(fechaColumna.lt(fecha));
                    }
                    if (tipoFecha.equals(TipoFecha.ANUAL.getId()))
                    {
                        build.and(fechaColumna.year().lt(anyo));
                    }
                    if (tipoFecha.equals(TipoFecha.MENSUAL.getId()))
                    {
                        build.and(fechaColumna.month().lt(mes));
                    }
                }

                if ((tipoComparacion.equals(TipoComparacion.MENORQUE.getId()) && operador.equals(O)) ||
                        (tipoComparacion.equals(TipoComparacion.MENOROIGUAL.getId()) && operador.equals(O)) ||
                        (tipoComparacion.equals(TipoComparacion.MAYOROIGUAL.getId()) && operador.equals(ONO)) ||
                        (tipoComparacion.equals(TipoComparacion.MAYORQUE.getId()) && operador.equals(ONO)))
                {

                    if (tipoFecha.equals(TipoFecha.COMPLETA.getId()))
                    {
                        build.or(fechaColumna.lt(fecha));
                    }
                    if (tipoFecha.equals(TipoFecha.ANUAL.getId()))
                    {
                        build.or(fechaColumna.year().lt(anyo));
                    }
                    if (tipoFecha.equals(TipoFecha.MENSUAL.getId()))
                    {
                        build.or(fechaColumna.month().lt(mes));
                    }

                }

                if ((tipoComparacion.equals(TipoComparacion.IGUAL.getId()) && operador.equals(Y)) || (
                        tipoComparacion.equals(TipoComparacion.MAYOROIGUAL.getId()) && operador.equals(Y)) ||
                        (tipoComparacion.equals(TipoComparacion.MENOROIGUAL.getId()) && (operador.equals(Y))))
                {

                    if (tipoFecha.equals(TipoFecha.COMPLETA.getId()))
                    {
                        build.and(fechaColumna.eq(fecha));
                    }
                    if (tipoFecha.equals(TipoFecha.ANUAL.getId()))
                    {
                        build.and(fechaColumna.year().eq(anyo.intValue()));
                    }
                    if (tipoFecha.equals(TipoFecha.MENSUAL.getId()))
                    {
                        build.and(fechaColumna.month().eq(mes.intValue()));
                    }
                }

                if ((tipoComparacion.equals(TipoComparacion.IGUAL.getId()) && operador.equals(O)) ||
                        (tipoComparacion.equals(TipoComparacion.MAYOROIGUAL.getId()) && operador.equals(O)) ||
                        (tipoComparacion.equals(TipoComparacion.MAYORQUE.getId()) && operador.equals(YNO)) ||
                        (tipoComparacion.equals(TipoComparacion.MAYORQUE.getId()) && operador.equals(ONO)) ||
                        (tipoComparacion.equals(TipoComparacion.MENORQUE.getId()) && operador.equals(YNO)) ||
                        (tipoComparacion.equals(TipoComparacion.MENORQUE.getId()) && operador.equals(ONO)) ||
                        (tipoComparacion.equals(TipoComparacion.MENOROIGUAL.getId()) && operador.equals(O)))
                {

                    if (tipoFecha.equals(TipoFecha.COMPLETA.getId()))
                    {
                        build.or(fechaColumna.eq(fecha));
                    }
                    if (tipoFecha.equals(TipoFecha.ANUAL.getId()))
                    {
                        build.or(fechaColumna.year().eq(anyo.intValue()));
                    }
                    if (tipoFecha.equals(TipoFecha.MENSUAL.getId()))
                    {
                        build.or(fechaColumna.month().eq(mes.intValue()));
                    }

                }

                if ((tipoComparacion.equals(TipoComparacion.IGUAL.getId()) && operador.equals(YNO)))
                {
                    if (tipoFecha.equals(TipoFecha.COMPLETA.getId()))
                    {
                        build.and(fechaColumna.ne(fecha));
                    }
                    if (tipoFecha.equals(TipoFecha.ANUAL.getId()))
                    {
                        build.and(fechaColumna.year().ne(anyo.intValue()));
                    }
                    if (tipoFecha.equals(TipoFecha.MENSUAL.getId()))
                    {
                        build.and(fechaColumna.month().ne(mes.intValue()));
                    }
                }

                if ((tipoComparacion.equals(TipoComparacion.IGUAL.getId()) && operador.equals(ONO)))
                {
                    if (tipoFecha.equals(TipoFecha.COMPLETA.getId()))
                    {
                        build.or(fechaColumna.ne(fecha));
                    }
                    if (tipoFecha.equals(TipoFecha.ANUAL.getId()))
                    {
                        build.or(fechaColumna.year().ne(anyo.intValue()));
                    }
                    if (tipoFecha.equals(TipoFecha.MENSUAL.getId()))
                    {
                        build.or(fechaColumna.month().ne(mes.intValue()));
                    }
                }


                if (tipoComparacion.equals(TipoComparacion.ENTRE.getId()))
                {

                    if (operador.equals(Y))
                    {

                        if (tipoFecha.equals(TipoFecha.COMPLETA.getId()))
                        {
                            build.and(fechaColumna.between(fecha, fechaMax));
                        }
                        if (tipoFecha.equals(TipoFecha.ANUAL.getId()))
                        {
                            build.and(fechaColumna.year().between(anyo, anyoMax));
                        }
                        if (tipoFecha.equals(TipoFecha.MENSUAL.getId()))
                        {
                            build.and(fechaColumna.month().between(mes, mesMax));
                        }
                    }

                    if (operador.equals(YNO))
                    {
                        if (tipoFecha.equals(TipoFecha.COMPLETA.getId()))
                        {
                            build.and(fechaColumna.notBetween(fecha, fechaMax));
                        }
                        if (tipoFecha.equals(TipoFecha.ANUAL.getId()))
                        {
                            build.and(fechaColumna.year().notBetween(anyo, anyoMax));
                        }
                        if (tipoFecha.equals(TipoFecha.MENSUAL.getId()))
                        {
                            build.and(fechaColumna.month().notBetween(mes, mesMax));
                        }
                    }

                    if (operador.equals(O))
                    {

                        if (tipoFecha.equals(TipoFecha.COMPLETA.getId()))
                        {
                            build.or(fechaColumna.between(fecha, fechaMax));
                        }
                        if (tipoFecha.equals(TipoFecha.ANUAL.getId()))
                        {
                            build.or(fechaColumna.year().between(anyo, anyoMax));
                        }
                        if (tipoFecha.equals(TipoFecha.MENSUAL.getId()))
                        {
                            build.or(fechaColumna.month().between(mes, mesMax));
                        }
                    }

                    if (operador.equals(ONO))
                    {
                        if (tipoFecha.equals(TipoFecha.COMPLETA.getId()))
                        {
                            build.or(fechaColumna.notBetween(fecha, fechaMax));
                        }
                        if (tipoFecha.equals(TipoFecha.ANUAL.getId()))
                        {
                            build.or(fechaColumna.year().notBetween(anyo, anyoMax));
                        }
                        if (tipoFecha.equals(TipoFecha.MENSUAL.getId()))
                        {
                            build.or(fechaColumna.month().notBetween(mes, mesMax));
                        }
                    }
                }

            }
            else

            {
                if (!ParamUtils.isNotNull(operador) || operador.equals(Y))
                {
                    build.and(fechaColumna.isNotNull());
                }
                else
                {
                    if (operador.equals(YNO))
                    {
                        build.and(fechaColumna.isNull());
                    }
                    if (operador.equals(O))
                    {
                        build.or(fechaColumna.isNotNull());
                    }
                    if (operador.equals(ONO))
                    {
                        build.or(fechaColumna.isNull());
                    }
                }
            }
        }

    }

    @Override
    public StringExpression limpiaAcentos(StringPath nombre) {
        return Expressions.stringTemplate(
                "upper(translate({0}, 'âàãáÁÂÀÃéèêÉÈÊíÍóôõòÓÔÕÒüúÜÚ', 'AAAAAAAAEEEEEEIIOOOOOOOOUUUU'))",
                nombre);
    }

    @Override
    public String limpiaAcentos(String cadena) {
        return StringUtils.limpiaAcentos(cadena).trim().toUpperCase();
    }

}