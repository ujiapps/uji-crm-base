package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ClienteGeneral;
import es.uji.apps.crm.model.QClienteGeneral;
import es.uji.apps.crm.model.QPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ClienteGeneralDAODatabaseImpl extends BaseDAODatabaseImpl implements ClienteGeneralDAO {


    @Override
    public ClienteGeneral getClienteGeneralById(Long clienteGeneralId) {
        JPAQuery query = new JPAQuery(entityManager);

        QClienteGeneral clienteGeneral = QClienteGeneral.clienteGeneral;
        QPersona persona = QPersona.persona;

        query.from(clienteGeneral).leftJoin(clienteGeneral.persona, persona).where(clienteGeneral.id.eq(clienteGeneralId));

        List<ClienteGeneral> lista = query.list(clienteGeneral);

//        if (lista.size() > 0) return lista.get(0);
        return query.singleResult(clienteGeneral);

    }

    @Override
    public ClienteGeneral getClienteGeneralByIdentificacionAndPersona(String identificacion, Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);

        QClienteGeneral clienteGeneral = QClienteGeneral.clienteGeneral;

        query.from(clienteGeneral).where(clienteGeneral.identificacion.eq(identificacion).and(clienteGeneral.persona.id.eq(personaId)));

        List<ClienteGeneral> lista = query.list(clienteGeneral);

        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

    @Override
    public ClienteGeneral getClienteGeneralByPerId(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        QClienteGeneral clienteGeneral = QClienteGeneral.clienteGeneral;

        query.from(clienteGeneral).where(clienteGeneral.persona.id.eq(connectedUserId));

        List<ClienteGeneral> lista = query.list(clienteGeneral);

        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

    @Override
    public ClienteGeneral getClienteGeneralByIdentificacion(String identificacion) {
        JPAQuery query = new JPAQuery(entityManager);

        QClienteGeneral clienteGeneral = QClienteGeneral.clienteGeneral;
        QPersona persona = QPersona.persona;

        query.from(clienteGeneral).leftJoin(clienteGeneral.persona, persona).fetch().where(clienteGeneral.identificacion.upper().eq(identificacion.toUpperCase().trim()));

        return query.uniqueResult(clienteGeneral);

    }

    @Override
    public ClienteGeneral getClienteGeneralByIdentificacionSinJoinPersona(String identificacion) {
        JPAQuery query = new JPAQuery(entityManager);

        QClienteGeneral clienteGeneral = QClienteGeneral.clienteGeneral;
        QPersona persona = QPersona.persona;

        query.from(clienteGeneral).where(clienteGeneral.identificacion.upper().eq(identificacion.toUpperCase().trim()));

        if (!query.list(clienteGeneral).isEmpty()) return query.list(clienteGeneral).get(0);
        else return new ClienteGeneral();
//        return query.uniqueResult(clienteGeneral);

    }
}