package es.uji.apps.crm.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.crm.model.PersonaSimple;
import es.uji.apps.crm.model.QPersonaSimple;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonaSimpleDAO extends BaseDAODatabaseImpl {
    public PersonaSimple getPersonaByIdentificacion(String identificacion) {
        JPAQuery query = new JPAQuery(entityManager);

        QPersonaSimple qPersonaSimple = QPersonaSimple.personaSimple;
        query.from(qPersonaSimple)
                .where(qPersonaSimple.identificacion.containsIgnoreCase(identificacion));

        List<PersonaSimple> lista = query.list(qPersonaSimple);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }
}
