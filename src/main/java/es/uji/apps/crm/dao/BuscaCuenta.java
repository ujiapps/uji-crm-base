package es.uji.apps.crm.dao;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import es.uji.commons.rest.ParamUtils;

@Component
public class BuscaCuenta {
    private static DataSource dataSource;
    private BuscaCuentaUJI buscaCuentaUJI;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        BuscaCuenta.dataSource = dataSource;
    }

    @PostConstruct
    public void init() {
        this.buscaCuentaUJI = new BuscaCuentaUJI(dataSource);
    }

    public String buscaCuentaUJI(Long clienteId) {
        return buscaCuentaUJI.buscaCuenta(clienteId);
    }

    private class BuscaCuentaUJI extends StoredProcedure {
        private static final String SQL = "uji_crm.busca_cuenta_crm";
        private static final String PCLIENTE = "p_cliente_id";

        public BuscaCuentaUJI(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(true);
            setSql(SQL);
            declareParameter(new SqlOutParameter("response", Types.VARCHAR));
            declareParameter(new SqlParameter(PCLIENTE, Types.VARCHAR));

            compile();
        }

        public String buscaCuenta(Long clienteId) {
            Map<String, Object> inParams = new HashMap<String, Object>();

            inParams.put(PCLIENTE, clienteId);

            Map<String, Object> results = execute(inParams);
            if (ParamUtils.isNotNull(results.get("response")))
            {
                return results.get("response").toString();
            }
            else
            {
                return "";
            }
        }
    }

}