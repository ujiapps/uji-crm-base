package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.crm.model.CampanyaClienteArchivo;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.model.QCampanyaClienteArchivo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CampanyaClienteArchivoDAO extends BaseDAODatabaseImpl {

    private QCampanyaClienteArchivo campanyaClienteArchivo = QCampanyaClienteArchivo.campanyaClienteArchivo;

    public List<CampanyaClienteArchivo> getListaPersonasSinAnyadirACampanya(Long campanyaId, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager);
        QCampanyaClienteArchivo campanyaClienteArchivo = QCampanyaClienteArchivo.campanyaClienteArchivo;

        query.from(campanyaClienteArchivo).where(campanyaClienteArchivo.campanya.id.eq(campanyaId).and(campanyaClienteArchivo.anyadir.isFalse()).and(campanyaClienteArchivo.fechaAnyadido.isNull()));

        if (paginacion != null)
        {
            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }
        else
        {
            query.limit(500);
        }

        return query.list(campanyaClienteArchivo);

    }

    public void updateCampanyaClienteArchivo(Long campanyaId, Long connectedUserId) {

        try
        {

            JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, campanyaClienteArchivo);

            updateClause.where(campanyaClienteArchivo.campanya.id.eq(campanyaId).and(campanyaClienteArchivo.anyadir.isFalse()))
                    .set(campanyaClienteArchivo.anyadir, true)
                    .set(campanyaClienteArchivo.admin, connectedUserId)
                    .execute();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

}