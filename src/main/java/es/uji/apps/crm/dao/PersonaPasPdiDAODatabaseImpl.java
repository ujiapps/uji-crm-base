package es.uji.apps.crm.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.PersonaPasPdi;
import es.uji.apps.crm.model.QPersonaPasPdi;
import es.uji.apps.crm.model.QProgramaUsuario;
import es.uji.apps.crm.model.QServicioUsuario;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.json.lookup.LookupItem;

@Repository
public class PersonaPasPdiDAODatabaseImpl extends BaseDAODatabaseImpl implements PersonaPasPdiDAO {
    @Autowired
    private UtilsDAO utilsDAO;

    @Override
    public List<PersonaPasPdi> getPersonasByServicioId(Long servicioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QServicioUsuario servicioUsuario = QServicioUsuario.servicioUsuario;
        QPersonaPasPdi persona = QPersonaPasPdi.personaPasPdi;

        query.from(persona)
                .join(persona.serviciosUsuario, servicioUsuario)
                .where(servicioUsuario.servicio.id.eq(servicioId))
                .orderBy(persona.nombre.desc());

        return query.list(persona);
    }

    @Override
    public PersonaPasPdi getPersonaById(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);

        QPersonaPasPdi persona = QPersonaPasPdi.personaPasPdi;

        query.from(persona).where(persona.id.eq(personaId));

        List<PersonaPasPdi> listaPersonas = query.list(persona);

        if (listaPersonas.size() > 0)
        {
            return listaPersonas.get(0);
        }
        return null;
    }

    @Override
    public List<PersonaPasPdi> getPersonasByProgramaId(Long programaId) {
        JPAQuery query = new JPAQuery(entityManager);

        QProgramaUsuario programaUsuario = QProgramaUsuario.programaUsuario;
        QPersonaPasPdi personaPasPdi = QPersonaPasPdi.personaPasPdi;

        query.from(personaPasPdi)
                .join(personaPasPdi.programasUsuarios, programaUsuario)
                .where(programaUsuario.programa.id.eq(programaId))
                .orderBy(personaPasPdi.nombre.desc());

        return query.list(personaPasPdi);
    }

    @Override
    public List<LookupItem> search(String query) {
        JPAQuery jpaQuery = new JPAQuery(entityManager);
        QPersonaPasPdi personapaspdi = QPersonaPasPdi.personaPasPdi;

        List<LookupItem> result = new ArrayList<LookupItem>();

        if (query != null && !query.isEmpty())
        {
            jpaQuery.from(personapaspdi)
                    .where(utilsDAO.limpiaAcentos(personapaspdi.nombre).containsIgnoreCase(utilsDAO.limpiaAcentos(query)));

            List<PersonaPasPdi> listaPersonas = jpaQuery.list(personapaspdi);
            for (PersonaPasPdi persona : listaPersonas)
            {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(String.valueOf(persona.getId()));
                lookupItem.setNombre(persona.getNombre());
                result.add(lookupItem);
            }
        }
        return result;
    }
}