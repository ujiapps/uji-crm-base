package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.LineaFacturacion;
import es.uji.apps.crm.model.QCampanya;
import es.uji.apps.crm.model.QLineaFacturacion;
import es.uji.apps.crm.model.QPrograma;
import es.uji.apps.crm.model.QServicio;
import es.uji.apps.crm.model.Servicio;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class LineaFacturacionDAODatabaseImpl extends BaseDAODatabaseImpl implements LineaFacturacionDAO {

    @Override
    public List<LineaFacturacion> getLineasFacturacionByServicio(Long servicio) {
        JPAQuery query = new JPAQuery(entityManager);
        QLineaFacturacion lineaFacturacion = QLineaFacturacion.lineaFacturacion;

        query.from(lineaFacturacion)
                .where(lineaFacturacion.servicio.id.eq(servicio));

        return query.list(lineaFacturacion);
    }

    @Override
    public List<LineaFacturacion> getLineasFacturacionByCampanya(Long campanyaId) {

        JPAQuery query = new JPAQuery(entityManager);
        QLineaFacturacion lineaFacturacion = QLineaFacturacion.lineaFacturacion;
        QServicio servicio = QServicio.servicio;
        QPrograma programa = QPrograma.programa;
        QCampanya campanya = QCampanya.campanya1;

        query.from(lineaFacturacion)
                .join(lineaFacturacion.servicio, servicio)
                .join(servicio.programas, programa)
                .join(programa.campanyas, campanya)
                .where(campanya.id.eq(campanyaId));

        return query.list(lineaFacturacion);
    }

    @Override
    public LineaFacturacion getLineaFacturacionActoByServicio(Servicio servicio) {
        JPAQuery query = new JPAQuery(entityManager);
        QLineaFacturacion lineaFacturacion = QLineaFacturacion.lineaFacturacion;

        query.from(lineaFacturacion)
                .where(lineaFacturacion.servicio.id.eq(servicio.getId()).and(lineaFacturacion.tipoRecibo.startsWith("GRAD")));

        return query.uniqueResult(lineaFacturacion);
    }
}