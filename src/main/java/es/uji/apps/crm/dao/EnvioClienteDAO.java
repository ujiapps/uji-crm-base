package es.uji.apps.crm.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.EnvioCliente;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.model.QCliente;
import es.uji.apps.crm.model.QEnvio;
import es.uji.apps.crm.model.QEnvioCliente;
import es.uji.apps.crm.model.QEnvioMensajeria;
import es.uji.apps.crm.model.QPersonaPasPdi;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Repository
public class EnvioClienteDAO extends BaseDAODatabaseImpl {
    QEnvioCliente qEnvioCliente = QEnvioCliente.envioCliente;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public EnvioClienteDAO() {
        relations.put("id", qEnvioCliente.id);
        relations.put("fechaEnvio", qEnvioCliente.fechaEnvio);
        relations.put("para", qEnvioCliente.para);
        relations.put("asunto", qEnvioCliente.envio.asunto);
        relations.put("estado", qEnvioCliente.envioMensajeria.estado);
        relations.put("nombre", qEnvioCliente.envio.nombre);
        relations.put("envioTipoNombre", qEnvioCliente.envio.envioTipo.nombre);
    }

    public List<EnvioCliente> getClientesByEnvioId(Long envioId, Paginacion paginacion) {
        QCliente cliente = QCliente.cliente;
        QEnvio envio = QEnvio.envio;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEnvioCliente)
                .join(qEnvioCliente.cliente, cliente).fetch()
                .join(qEnvioCliente.envio, envio).fetch()
                .where(qEnvioCliente.envio.id.eq(envioId));

        paginacion.setTotalCount(query.count());

        if (paginacion.getStart() != -1)
        {
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }
        return query.list(qEnvioCliente);
    }

    public List<EnvioCliente> getClientesByEnvioId(Long envioId) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEnvioCliente)
                .where(qEnvioCliente.envio.id.eq(envioId));

        return query.list(qEnvioCliente);
    }

    public List<EnvioCliente> getEnvioClientesByClienteId(Long clienteId, Paginacion paginacion) {

        QEnvio envio = QEnvio.envio;
        QCliente cliente = QCliente.cliente;
        QEnvioMensajeria qEnvioMensajeria = QEnvioMensajeria.envioMensajeria;
        QPersonaPasPdi qPersona = QPersonaPasPdi.personaPasPdi;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEnvioCliente)
                .join(qEnvioCliente.cliente, cliente).fetch()
                .join(qEnvioCliente.envio, envio).fetch()
                .leftJoin(qEnvioCliente.envioMensajeria, qEnvioMensajeria).fetch()
                .join(envio.persona, qPersona).fetch()
                .where(cliente.id.eq(clienteId));


        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return query.list(qEnvioCliente);
    }

    public EnvioCliente getEnvioClienteById(Long envioClienteId) {

        QEnvio qEnvio = QEnvio.envio;
        QCliente qCliente = QCliente.cliente;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEnvioCliente)
                .join(qEnvioCliente.envio, qEnvio).fetch()
                .join(qEnvioCliente.cliente, qCliente).fetch()
                .where(qEnvioCliente.id.eq(envioClienteId));

        List<EnvioCliente> enviosCliente = query.list(qEnvioCliente);

        if (enviosCliente.isEmpty())
        {
            return null;
        }
        else
        {
            return enviosCliente.get(0);
        }
    }

    @Transactional
    public void delEnvioClientesTodos(Long envioId) {

        String del = "delete crm_envios_clientes where envio_id = " + envioId;
        entityManager.createNativeQuery(del).executeUpdate();

    }
}