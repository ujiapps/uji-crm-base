package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.crm.model.Programa;
import es.uji.apps.crm.model.QCampanyaItem;
import es.uji.apps.crm.model.QClienteItem;
import es.uji.apps.crm.model.QEnvioItem;
import es.uji.apps.crm.model.QGrupo;
import es.uji.apps.crm.model.QItem;
import es.uji.apps.crm.model.QPersonaPrograma;
import es.uji.apps.crm.model.QPrograma;
import es.uji.apps.crm.model.QServicio;
import es.uji.apps.crm.model.QServicioUsuario;
import es.uji.apps.crm.model.QTipoAcceso;
import es.uji.apps.crm.model.domains.TipoAccesoProgramaUsuario;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ProgramaDAO extends BaseDAODatabaseImpl {
    QPrograma programa = QPrograma.programa;
    QServicio servicio = QServicio.servicio;
    QServicioUsuario servicioUsuario = QServicioUsuario.servicioUsuario;
    QGrupo grupo = QGrupo.grupo;
    QItem item = QItem.item;
    QTipoAcceso tipoAcceso = QTipoAcceso.tipoAcceso;
    QCampanyaItem campanyaItem = QCampanyaItem.campanyaItem;
    QEnvioItem envioItem = QEnvioItem.envioItem;
    QClienteItem clienteItem = QClienteItem.clienteItem;

    public List<Programa> getProgramasByServicioId(Long servicioId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(programa)
                .where(programa.servicio.id.eq(servicioId));
        return query.list(programa);
    }

    public List<Programa> getProgramas(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(programa).join(programa.servicio, servicio).fetch()
                .join(programa.tipoAcceso, tipoAcceso).fetch()
                .join(servicio.serviciosUsuario, servicioUsuario)
                .where(servicioUsuario.personaPasPdi.id.eq(connectedUserId));

        return query.distinct().list(programa);
    }

    public List<Programa> getProgramasYGrupos(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(programa)
                .join(programa.grupos, grupo).fetch()
                .join(grupo.items, item)
                .join(item.clientesItems, clienteItem)
                .where(clienteItem.cliente.id.eq(clienteId)
                        .and(clienteItem.campanya.isNull()
                                .and(clienteItem.clienteDato.isNull()
                                        .and(clienteItem.clienteItemRelacionado.isNull()))));

        return query.distinct().list(programa);
    }

    public List<Programa> getProgramasAdmin(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);
        QPersonaPrograma personaPrograma = QPersonaPrograma.personaPrograma;

        return query
                .from(programa)
                .leftJoin(programa.grupos, grupo).fetch()
                .join(programa.personaProgramas, personaPrograma)
                .where(personaPrograma.tipoAcceso.eq(TipoAccesoProgramaUsuario.ADMIN.getNombre())
                        .and(personaPrograma.persona.id.eq(connectedUserId)))
                .distinct()
                .list(programa);
    }

    public List<Programa> getProgramasUser(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);
        QPersonaPrograma personaPrograma = QPersonaPrograma.personaPrograma;

        return query.from(programa)
                .leftJoin(programa.grupos, grupo).fetch()
                .join(programa.personaProgramas, personaPrograma)
                .where(personaPrograma.persona.id.eq(connectedUserId))
                .distinct()
                .list(programa);
    }

    @Transactional
    public void updatePrograma(Programa programaUp) {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, programa);

        updateClause.where(programa.id.eq(programaUp.getId()))
                .set(programa.nombreEs, programaUp.getNombreEs())
                .set(programa.nombreUk, programaUp.getNombreUk())
                .set(programa.nombreCa, programaUp.getNombreCa())
                .set(programa.plantilla, programaUp.getPlantilla())
                .set(programa.servicio, programaUp.getServicio())
                .set(programa.orden, programaUp.getOrden())
                .set(programa.visible, programaUp.getVisible())
                .set(programa.tipoAcceso, programaUp.getTipoAcceso()).execute();
    }

    public Programa getProgramaById(Long programaId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(programa)
                .join(programa.servicio, servicio).fetch()
                .join(programa.tipoAcceso, tipoAcceso).fetch()
                .where(programa.id.eq(programaId));

        List<Programa> listaProgramas = query.list(programa);
        if (listaProgramas.size() > 0)
        {
            return listaProgramas.get(0);
        }
        return null;
    }
}