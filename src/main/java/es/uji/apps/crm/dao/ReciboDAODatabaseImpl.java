package es.uji.apps.crm.dao;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.DateTimePath;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.FiltroCampanya;
import es.uji.apps.crm.model.FiltroEstadoRecibo;
import es.uji.apps.crm.model.FiltroExportacion;
import es.uji.apps.crm.model.FiltroFechas;
import es.uji.apps.crm.model.FiltroRecibo;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.model.Persona;
import es.uji.apps.crm.model.QCampanyaCliente;
import es.uji.apps.crm.model.QCliente;
import es.uji.apps.crm.model.QClienteCuota;
import es.uji.apps.crm.model.QLinea;
import es.uji.apps.crm.model.QMovimientoRecibo;
import es.uji.apps.crm.model.QPersona;
import es.uji.apps.crm.model.QRecibo;
import es.uji.apps.crm.model.QReciboMovimientoExportado;
import es.uji.apps.crm.model.QReciboRemesaExportado;
import es.uji.apps.crm.model.QTipoTarifa;
import es.uji.apps.crm.model.Recibo;
import es.uji.apps.crm.model.domains.TipoEstadoRecibo;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Repository
public class ReciboDAODatabaseImpl extends BaseDAODatabaseImpl implements ReciboDAO {

    static Long Y = 1L;
    static Long O = 2L;
    static Long YNO = 3L;
    static Long ONO = 4L;
    QRecibo recibo = QRecibo.recibo;
    QReciboMovimientoExportado reciboMovimientoExportado = QReciboMovimientoExportado.reciboMovimientoExportado;
    Long EMISORASAUJI = 2162L;
    @Autowired
    private UtilsDAO utilsDAO;

    @Override
    public List<Recibo> getRecibosByPersonaId(Long personaId) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(recibo).where(recibo.persona.id.eq(personaId)).orderBy(recibo.id.desc());

        return query.list(recibo);
    }

    @Override
    public List<Recibo> getRecibos(FiltroRecibo filtro, Paginacion paginacion) throws ParseException {

        JPAQuery query = new JPAQuery(entityManager);
        BooleanBuilder build = new BooleanBuilder();
        QPersona persona = QPersona.persona;

        query.from(recibo)
                .join(recibo.persona, persona).fetch()
                .where(recibo.emisora_id.eq(EMISORASAUJI))
                .distinct();

        creaQueryBusqueda(query, build, filtro);
        query.where(build);

        if (paginacion != null)
        {
            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        query.orderBy(recibo.id.desc());
        return query.list(recibo);
    }

    private void creaQueryBusqueda(JPAQuery query, BooleanBuilder build, FiltroRecibo filtro) throws ParseException {

        creaQueryCampanya(query, build, filtro);
        creaQueryFechas(build, filtro, recibo.fechaCreacion);
        creaQueryEstado(build, filtro);
        creaQueryExportaciones(query, build, filtro);

    }

    private void creaQueryFechas(BooleanBuilder build, FiltroRecibo filtroRecibo, DateTimePath<Date> fechaCreacion)
            throws ParseException {

        for (FiltroFechas filtroFechas : filtroRecibo.getFiltroFechas())
        {
            utilsDAO.creaQueryFecha(build, filtroFechas, fechaCreacion);
        }
    }

    private void creaQueryCampanya(JPAQuery query, BooleanBuilder build, FiltroRecibo filtro) {

        if (!filtro.getFiltroCampanyas().isEmpty())
        {

            QLinea linea = QLinea.linea;
            QClienteCuota clienteCuota = QClienteCuota.clienteCuota;
            QTipoTarifa tipoTarifa = QTipoTarifa.tipoTarifa;
//            QTarifa tarifa = QTarifa.tarifa;
            QCliente cliente = QCliente.cliente;
            QCampanyaCliente campanyaCliente = QCampanyaCliente.campanyaCliente;
            query.join(recibo.lineas, linea)
                    .from(clienteCuota).where(linea.referencia1.eq(clienteCuota.id.intValue()));
//                    .join(clienteCuota.tarifa, tarifa)
//                    .join(tarifa.tipoTarifa, tipoTarifa);

            for (FiltroCampanya filtroCampanya : filtro.getFiltroCampanyas())
            {

                switch (filtroCampanya.getOperador())
                {
                    case Y:
                        build.and(tipoTarifa.campanya.id.eq(filtroCampanya.getCampanya()));
                        break;
                    case O:
                        build.or(tipoTarifa.campanya.id.eq(filtroCampanya.getCampanya()));
                        break;
                    case YNO:
                        build.and(tipoTarifa.campanya.id.ne(filtroCampanya.getCampanya()));
                        break;
                    case ONO:
                        build.or(tipoTarifa.campanya.id.ne(filtroCampanya.getCampanya()));
                        break;
                }

                if (ParamUtils.isNotNull(filtroCampanya.getCampanyaTipoEstado()))
                {
                    query.join(clienteCuota.cliente, cliente)
                            .join(cliente.campanyasClientes, campanyaCliente);

                    build.and(campanyaCliente.campanya.id.eq(filtroCampanya.getCampanya()));
                    build.and(campanyaCliente.campanyaClienteTipo.id.eq(filtroCampanya.getCampanyaTipoEstado()));

                }
            }
        }

    }

    private void creaQueryEstado(BooleanBuilder build, FiltroRecibo filtro) {

        for (FiltroEstadoRecibo filtroEstadoRecibo : filtro.getFiltroEstadoRecibos())
        {
            anyade_filtro_estado_recibo(build, filtroEstadoRecibo);
        }
    }

    private void anyade_filtro_estado_recibo(BooleanBuilder build, FiltroEstadoRecibo filtro) {


        if (ParamUtils.isNotNull(filtro.getTipoEstadoRecibo()))
        {

            Long tipoEstadoRecibo = filtro.getTipoEstadoRecibo();

            switch (filtro.getOperador())
            {
                case Y:
                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.PENDIENTEPAGO))
                    {
                        build.and(recibo.fechaPago.isNull());
                    }

                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.MOROSO))
                    {
                        build.and(recibo.moroso.isTrue());
                    }

                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.PENDIENTEENVIO))
                    {
                        build.and(recibo.fechaPagoLimite.after(new Date()).and(recibo.fechaPago.isNull()));
                    }

                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.PAGADO))
                    {
                        build.and(recibo.fechaPago.isNotNull());
                    }
                    break;
                case O:
                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.PENDIENTEPAGO))
                    {
                        build.or(recibo.fechaPago.isNull());
                    }

                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.MOROSO))
                    {
                        build.or(recibo.moroso.isTrue());
                    }

                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.PENDIENTEENVIO))
                    {
                        build.or(recibo.fechaPagoLimite.after(new Date()).and(recibo.fechaPago.isNull()));
                    }

                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.PAGADO))
                    {
                        build.or(recibo.fechaPago.isNotNull());
                    }
                    break;
                case YNO:
                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.PENDIENTEPAGO))
                    {
                        build.and(recibo.fechaPago.isNotNull());
                    }

                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.MOROSO))
                    {
                        build.and(recibo.moroso.isFalse());
                    }

                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.PENDIENTEENVIO))
                    {
                        build.and(recibo.fechaPago.isNotNull());
                    }

                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.PAGADO))
                    {
                        build.and(recibo.fechaPago.isNull());
                    }
                    break;
                case ONO:
                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.PENDIENTEPAGO))
                    {
                        build.or(recibo.fechaPago.isNotNull());
                    }

                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.MOROSO))
                    {
                        build.or(recibo.moroso.isFalse());
                    }

                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.PENDIENTEENVIO))
                    {
                        build.or(recibo.fechaPago.isNotNull());
                    }

                    if (tipoEstadoRecibo.equals(TipoEstadoRecibo.PAGADO))
                    {
                        build.or(recibo.fechaPago.isNull());
                    }
                    break;
            }
        }

    }

    private void creaQueryExportaciones(JPAQuery query, BooleanBuilder build, FiltroRecibo filtro) {

        if (!filtro.getFiltroExportaciones().isEmpty())
        {

            QReciboRemesaExportado reciboRemesaExportado = QReciboRemesaExportado.reciboRemesaExportado;
            QMovimientoRecibo movimientoRecibo = QMovimientoRecibo.movimientoRecibo;

            query.leftJoin(recibo.movimientosRecibo, movimientoRecibo)
                    .leftJoin(movimientoRecibo.exportados, reciboMovimientoExportado)
                    .where(reciboMovimientoExportado.isNull().or(reciboMovimientoExportado.valido.isTrue())
                            .and(movimientoRecibo.tipoConcilia.isNotNull()))
                    .leftJoin(reciboMovimientoExportado.remesa, reciboRemesaExportado);

            for (FiltroExportacion filtroExportacion : filtro.getFiltroExportaciones())
            {
                anyade_filtro_exportacion(build, filtroExportacion, reciboRemesaExportado.fecha);
            }
        }

    }

    private void anyade_filtro_exportacion(BooleanBuilder build, FiltroExportacion filtro, DateTimePath<Date> fecha) {

        if (ParamUtils.isNotNull(filtro.getFecha().getFecha()) || ParamUtils.isNotNull(filtro.getFecha().getAnyo()) || ParamUtils.isNotNull(filtro.getFecha().getMes()))
        {
            utilsDAO.creaQueryFecha(build, filtro.getFecha(), fecha);
        }
        else
        {
            if (filtro.getOperador().equals(Y))
            {
                build.and(reciboMovimientoExportado.isNotNull());
            }

            if (filtro.getOperador().equals(O))
            {
                build.or(reciboMovimientoExportado.isNotNull());
            }

            if (filtro.getOperador().equals(YNO))
            {
                build.and(reciboMovimientoExportado.isNull().or(reciboMovimientoExportado.valido.isFalse()));

            }

            if (filtro.getOperador().equals(ONO))
            {
                build.or(reciboMovimientoExportado.isNull().or(reciboMovimientoExportado.valido.isFalse()));
            }
        }


    }

    @Override
    public List<Recibo> getRecibosConMovimientos(FiltroRecibo filtro, Paginacion paginacion) throws ParseException {

        JPAQuery query = new JPAQuery(entityManager);
        BooleanBuilder build = new BooleanBuilder();
        QPersona persona = QPersona.persona;
        QMovimientoRecibo movimientoRecibo = QMovimientoRecibo.movimientoRecibo;

        query.from(recibo)
                .join(recibo.persona, persona).fetch()
                .join(recibo.movimientosRecibo, movimientoRecibo).fetch()
                .where(recibo.emisora_id.eq(EMISORASAUJI))
                .distinct();

        creaQueryBusqueda(query, build, filtro);
        query.where(build);

        if (paginacion != null)
        {
            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        query.orderBy(recibo.id.desc());
        return query.list(recibo);
    }

    @Override
    public List<Recibo> getRecibosConMovimientosParaCsv(FiltroRecibo filtro, Paginacion paginacion)
            throws ParseException {
        JPAQuery query = new JPAQuery(entityManager);
        BooleanBuilder build = new BooleanBuilder();
        QPersona persona = QPersona.persona;
        QMovimientoRecibo movimientoRecibo = QMovimientoRecibo.movimientoRecibo;

        query.from(recibo)
                .join(recibo.persona, persona).fetch()
                .join(recibo.movimientosRecibo, movimientoRecibo).fetch()
                .where(recibo.emisora_id.eq(EMISORASAUJI).and(movimientoRecibo.tipoConcilia.isNotEmpty()))
                .distinct();

        creaQueryBusqueda(query, build, filtro);
        query.where(build);

        if (paginacion != null)
        {
            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }

        query.orderBy(recibo.id.desc());
        return query.list(recibo);
    }

    @Override
    public Recibo getReciboImpagadoByPersonaAndCampanya(Persona persona, Campanya campanya) {

        QClienteCuota clienteCuota = QClienteCuota.clienteCuota;
//        QTarifa tarifa = QTarifa.tarifa;
//        QTipoTarifa tipoTarifa = QTipoTarifa.tipoTarifa;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(recibo, clienteCuota)
                .where(recibo.id.eq(clienteCuota.reciboId)
                        .and(recibo.persona.id.eq(persona.getId()))
                        .and(recibo.fechaPago.isNull())
                        .and(clienteCuota.clienteTarifa.tipoTarifa.campanya.eq(campanya)));

        return query.uniqueResult(recibo);

    }

    @Override
    public Recibo getReciboByCuota(Long clienteCuotaId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(recibo).where(recibo.referencia.eq(clienteCuotaId)).uniqueResult(recibo);
    }
}