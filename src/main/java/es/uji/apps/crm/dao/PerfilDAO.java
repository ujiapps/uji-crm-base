package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Perfil;
import es.uji.commons.db.BaseDAO;

public interface PerfilDAO extends BaseDAO {
    List<Perfil> getPerfiles();

    List<Perfil> getPerfilesFaltanByProgramaId(Long programaId);

    List<Perfil> getPerfilesByProgramaId(Long programaId);

}