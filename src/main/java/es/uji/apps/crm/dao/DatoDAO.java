package es.uji.apps.crm.dao;


import java.util.List;

import es.uji.apps.crm.model.ClienteDatoVw;
import es.uji.commons.db.BaseDAO;

public interface DatoDAO extends BaseDAO {
    List<ClienteDatoVw> getDatosByClienteId(Long clienteId);

    List<ClienteDatoVw> getDatosByDatoId(Long datoId);

    List<ClienteDatoVw> getDatosByClienteIdAndCampanya(Long campanyaId, Long clienteId);
}