package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.MovimientoRecibo;
import es.uji.commons.db.BaseDAO;

public interface MovimientoReciboDAO extends BaseDAO {

    List<MovimientoRecibo> getMovimientosReciboByReciboId(Long reciboId);
}