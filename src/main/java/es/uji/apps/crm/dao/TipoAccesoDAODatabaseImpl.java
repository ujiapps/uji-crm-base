package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QTipoAcceso;
import es.uji.apps.crm.model.TipoAcceso;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoAccesoDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoAccesoDAO {

    @Override
    public TipoAcceso getTipoAccesoById(Long tipoAccesoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoAcceso tipoAcceso = QTipoAcceso.tipoAcceso;

        query.from(tipoAcceso).where(tipoAcceso.id.eq(tipoAccesoId));

        List<TipoAcceso> lista = query.list(tipoAcceso);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

    @Override
    public List<TipoAcceso> getTiposAcceso() {
        JPAQuery query = new JPAQuery(entityManager);

        QTipoAcceso tipoAcceso = QTipoAcceso.tipoAcceso;

        query.from(tipoAcceso).orderBy(tipoAcceso.nombre.asc());

        return query.list(tipoAcceso);
    }
}