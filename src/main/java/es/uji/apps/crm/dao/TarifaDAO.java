package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Tarifa;
import es.uji.apps.crm.model.TipoTarifa;
import es.uji.commons.db.BaseDAO;

public interface TarifaDAO extends BaseDAO {

    List<Tarifa> getTarifasByTipoTarifa(TipoTarifa tipoTarifa);

    Tarifa getTarifaVigente(TipoTarifa tipoTarifa);
}