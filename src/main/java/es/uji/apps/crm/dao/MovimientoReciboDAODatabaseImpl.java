package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.MovimientoRecibo;
import es.uji.apps.crm.model.QMovimientoRecibo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class MovimientoReciboDAODatabaseImpl extends BaseDAODatabaseImpl implements MovimientoReciboDAO {
    @Override
    public List<MovimientoRecibo> getMovimientosReciboByReciboId(Long reciboId) {

        JPAQuery query = new JPAQuery(entityManager);
        QMovimientoRecibo movimientoRecibo = QMovimientoRecibo.movimientoRecibo;

        query.from(movimientoRecibo).where(movimientoRecibo.recibo.id.eq(reciboId)).orderBy(movimientoRecibo.fecha.asc());

        return query.list(movimientoRecibo);
    }


}