package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.EnvioAdjunto;
import es.uji.apps.crm.model.QEnvioAdjunto;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EnvioAdjuntoDAODatabaseImpl extends BaseDAODatabaseImpl implements EnvioAdjuntoDAO {
    @Override
    public EnvioAdjunto getEnvioAdjuntoById(Long envioAdjuntoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QEnvioAdjunto envioAdjunto = QEnvioAdjunto.envioAdjunto;

        query.from(envioAdjunto).where(envioAdjunto.id.eq(envioAdjuntoId));

        List<EnvioAdjunto> lista = query.list(envioAdjunto);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

    @Override
    public List<EnvioAdjunto> getEnvioAdjuntosByEnvioId(Long envioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QEnvioAdjunto qEnvioAdjunto = QEnvioAdjunto.envioAdjunto;

        query.from(qEnvioAdjunto)
                .where(qEnvioAdjunto.envio.id.eq(envioId));

        return query.list(qEnvioAdjunto);
    }
}