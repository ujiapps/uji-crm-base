package es.uji.apps.crm.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ClasificacionEstudio;
import es.uji.apps.crm.model.QClasificacionEstudio;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.json.lookup.LookupItem;

@Repository
public class ClasificacionEstudioDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {
    public static final Long MAX_RESULTADOS = 50000L;
    @Autowired
    private UtilsDAO utilsDAO;

//    public List<ClasificacionEstudio> getClasificacionEstudios(String tipoEstudio, Paginacion paginacion,
//                                                               String columnaOrden, String tipoOrden)
//    {
//        JPAQuery query = new JPAQuery(entityManager);
//        QClasificacionEstudio clasificacionEstudio = QClasificacionEstudio.clasificacionEstudio;
//        query.from(clasificacionEstudio).where(clasificacionEstudio.tipo.eq(tipoEstudio)).distinct();
//
//        if (columnaOrden.equals("nombre"))
//        {
//            if (tipoOrden.equals("DESC"))
//            {
//                query.orderBy(utilsDAO.limpiaAcentos(clasificacionEstudio.nombre).desc());
//            }
//            else
//            {
//                query.orderBy(utilsDAO.limpiaAcentos(clasificacionEstudio.nombre).asc());
//            }
//        }
//
//        if (paginacion != null)
//        {
//            paginacion.setTotalCount(query.count());
//            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
//        }
//        else
//        {
//            query.limit(MAX_RESULTADOS);
//        }
//
//        return query.list(clasificacionEstudio);
//    }

    public List<ClasificacionEstudio> getClasificacionesEstudios(String columnaOrden, String tipoOrden) {
        JPAQuery query = new JPAQuery(entityManager);
        QClasificacionEstudio clasificacionEstudio = QClasificacionEstudio.clasificacionEstudio;
        query.from(clasificacionEstudio).distinct();

        if (columnaOrden.equals("nombre"))
        {
            if (tipoOrden.equals("DESC"))
            {
                query.orderBy(utilsDAO.limpiaAcentos(clasificacionEstudio.nombre).desc());
            }
            else
            {
                query.orderBy(utilsDAO.limpiaAcentos(clasificacionEstudio.nombre).asc());
            }
        }


        query.limit(MAX_RESULTADOS);

        return query.list(clasificacionEstudio);
    }

    public List<LookupItem> search(String cadena) {
        JPAQuery query = new JPAQuery(entityManager);
        QClasificacionEstudio qClasificacionEstudio = QClasificacionEstudio.clasificacionEstudio;

        String cadenaFiltrada = utilsDAO.limpiaAcentos(cadena);

        List<LookupItem> result = new ArrayList<LookupItem>();
        if (ParamUtils.isNotNull(cadenaFiltrada))
        {
            query.from(qClasificacionEstudio);
            query.where(utilsDAO.limpiaAcentos(qClasificacionEstudio.nombre).containsIgnoreCase(cadenaFiltrada));

            List<ClasificacionEstudio> clasificaciones = query.list(qClasificacionEstudio);

            for (ClasificacionEstudio clasificacionEstudio : clasificaciones)
            {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(clasificacionEstudio.getId().toString());
                lookupItem.setNombre(clasificacionEstudio.getNombre());
                lookupItem.addExtraParam("tipoEstudio", clasificacionEstudio.getTipoNombre());
                result.add(lookupItem);
            }
        }
        return result;
    }
}