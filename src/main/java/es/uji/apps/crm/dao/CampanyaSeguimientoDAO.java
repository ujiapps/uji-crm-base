package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.CampanyaSeguimiento;
import es.uji.commons.db.BaseDAO;

public interface CampanyaSeguimientoDAO extends BaseDAO {

    List<CampanyaSeguimiento> getCampanyaSeguimientosByCampanyaFaseId(Long campanyaFaseId);

    CampanyaSeguimiento getCampanyaSeguimientoById(Long campanyaSeguimientoId);

}