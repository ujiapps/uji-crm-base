package es.uji.apps.crm.dao;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

@Component
public class SubVinculos {
    private static DataSource dataSource;
    private ClienteCampanya subvinculo;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        SubVinculos.dataSource = dataSource;
    }

    public void init() {
        this.subvinculo = new ClienteCampanya(dataSource);
    }

    public void clienteCampanya(Long clienteCampanya, String tipo) {
        subvinculo.clienteCampanya(clienteCampanya, tipo);
    }

    private class ClienteCampanya extends StoredProcedure {
        private static final String SQL = "subvinculos.clienteCampanya";
        private static final String PCLIENTECAMPANYA = "p_cliente_campanya";
        private static final String PTIPO = "p_tipo";

        public ClienteCampanya(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(PCLIENTECAMPANYA, Types.BIGINT));
            declareParameter(new SqlParameter(PTIPO, Types.VARCHAR));
            compile();
        }

        public void clienteCampanya(Long clienteCampanya, String tipo) {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PCLIENTECAMPANYA, clienteCampanya);
            inParams.put(PTIPO, tipo);
            Map<String, Object> results = execute(inParams);
        }
    }

}