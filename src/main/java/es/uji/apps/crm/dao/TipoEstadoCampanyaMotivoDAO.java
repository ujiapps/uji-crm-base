package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.TipoEstadoCampanyaMotivo;
import es.uji.commons.db.BaseDAO;

public interface TipoEstadoCampanyaMotivoDAO extends BaseDAO {

    List<TipoEstadoCampanyaMotivo> getTiposEstadoCampanyaMotivosByEstadoCampanyaId(Long estadoCampanya);

    TipoEstadoCampanyaMotivo getTipoEstadoCampanyaById(Long campanyaEstadoMotivoId);

}