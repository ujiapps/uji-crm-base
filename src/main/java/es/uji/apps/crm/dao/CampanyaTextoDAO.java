package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.CampanyaTexto;
import es.uji.commons.db.BaseDAO;

public interface CampanyaTextoDAO extends BaseDAO {
    List<CampanyaTexto> getCampanyaTextos();

    CampanyaTexto getCampanyaTextoByCampanyaCodigo(String codigo);
}