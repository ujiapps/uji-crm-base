package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.TipoTarifaEnvio;
import es.uji.apps.crm.model.TipoTarifaEnvioProgramado;
import es.uji.commons.db.BaseDAO;

public interface TipoTarifaEnvioProgramadoDAO extends BaseDAO {
    List<TipoTarifaEnvioProgramado> getTipoTarifaEnvioProgramadoByTipoTarifaEnvio(TipoTarifaEnvio tipoTarifaEnvio);

    void deleteEnvioProgramadoByTipoTarifaEnvio(Long tarifaEnvioId);

//    List<Tarifa> getTarifasByTipoTarifa(TipoTarifa tipoTarifa);

}