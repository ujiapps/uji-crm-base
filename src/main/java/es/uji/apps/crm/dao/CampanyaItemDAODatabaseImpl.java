package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.CampanyaItem;
import es.uji.apps.crm.model.QCampanyaItem;
import es.uji.apps.crm.model.QGrupo;
import es.uji.apps.crm.model.QItem;
import es.uji.apps.crm.model.QPrograma;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CampanyaItemDAODatabaseImpl extends BaseDAODatabaseImpl implements CampanyaItemDAO {

    @Override
    public List<CampanyaItem> getCampanyaItemsByCampanyaId(Long campanyaId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaItem campanyaItem = QCampanyaItem.campanyaItem;
        QItem item = QItem.item;
        QGrupo grupo = QGrupo.grupo;
        QPrograma programa = QPrograma.programa;

        query.from(campanyaItem)
                .join(campanyaItem.item, item).fetch()
                .join(item.grupo, grupo).fetch()
                .join(grupo.programa, programa).fetch()
                .where(campanyaItem.campanya.id.eq(campanyaId));

        return query.list(campanyaItem);
    }

    @Override
    public CampanyaItem getCampanyaItemById(Long campanyaItemId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaItem campanyaItem = QCampanyaItem.campanyaItem;
        QItem item = QItem.item;

        query.from(campanyaItem)
                .join(campanyaItem.item, item).fetch()
                .where(campanyaItem.id.eq(campanyaItemId));

        List<CampanyaItem> lista = query.list(campanyaItem);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

}