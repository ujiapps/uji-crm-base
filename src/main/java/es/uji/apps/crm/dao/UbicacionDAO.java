package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Ubicacion;
import es.uji.commons.db.BaseDAO;

public interface UbicacionDAO extends BaseDAO {
    List<Ubicacion> getUbicaciones();

    Ubicacion getUbicacion(Long uestId);
}