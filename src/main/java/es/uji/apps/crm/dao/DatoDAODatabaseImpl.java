package es.uji.apps.crm.dao;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ClienteDatoVw;
import es.uji.apps.crm.model.QCliente;
import es.uji.apps.crm.model.QClienteDatoVw;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class DatoDAODatabaseImpl extends BaseDAODatabaseImpl implements DatoDAO {


    @Override
    public List<ClienteDatoVw> getDatosByClienteId(Long clienteId) {
        QClienteDatoVw clienteDato = QClienteDatoVw.clienteDatoVw;
        QCliente cliente = QCliente.cliente;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteDato).join(clienteDato.cliente, cliente).fetch()
                .where(clienteDato.cliente.id.eq(clienteId)
                        .and(clienteDato.clienteItemId.isNull()
                                .and(clienteDato.clienteDatoId.isNull()
                                        .and(clienteDato.campanyaId.isNull()))));

        return query.list(clienteDato);
    }

    @Override
    public List<ClienteDatoVw> getDatosByDatoId(Long datoId) {
        QClienteDatoVw clienteDato = QClienteDatoVw.clienteDatoVw;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteDato)
                .where(clienteDato.clienteItemId.eq(datoId).or(clienteDato.clienteDatoId.eq(datoId)));

        return query.list(clienteDato);
    }

    @Override
    public List<ClienteDatoVw> getDatosByClienteIdAndCampanya(Long campanyaId, Long clienteId) {
        QClienteDatoVw clienteDato = QClienteDatoVw.clienteDatoVw;
        QCliente cliente = QCliente.cliente;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(clienteDato)
                .join(clienteDato.cliente, cliente).fetch()
                .where(clienteDato.cliente.id.eq(clienteId)
                        .and(clienteDato.clienteItemId.isNull()
                                .and(clienteDato.clienteDatoId.isNull()
                                        .and(clienteDato.campanyaId.eq(campanyaId)))));

        return query.list(clienteDato);
    }
}
