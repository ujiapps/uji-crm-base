package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Persona;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.db.LookupDAO;


public interface PersonaDAO extends BaseDAO, LookupDAO {
    Persona getPersonaByIdentificacion(String identificacion);

    List<Persona> getPersonas();

    Persona getPersonaByClienteId(Long clienteId); //throws RegistroNoEncontradoException;
}