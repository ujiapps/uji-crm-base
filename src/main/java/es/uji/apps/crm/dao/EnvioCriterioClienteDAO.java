package es.uji.apps.crm.dao;

import com.sun.research.ws.wadl.Param;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;

import es.uji.apps.crm.model.QEnvioCriterioCliente;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EnvioCriterioClienteDAO extends BaseDAODatabaseImpl {

    @Transactional
    public void deleteEnvioCriterioCliente(Long envioId, Long clienteId, String correo) {

        QEnvioCriterioCliente qEnvioCriterioCliente = QEnvioCriterioCliente.envioCriterioCliente;

        if (ParamUtils.isNotNull(clienteId)) {
            JPADeleteClause query = new JPADeleteClause(entityManager, qEnvioCriterioCliente);
            query.where(qEnvioCriterioCliente.envio.id.eq(envioId).and(qEnvioCriterioCliente.cliente.id.eq(clienteId))).execute();
        }
        else{
            JPADeleteClause query = new JPADeleteClause(entityManager, qEnvioCriterioCliente);
            query.where(qEnvioCriterioCliente.envio.id.eq(envioId).and(qEnvioCriterioCliente.correo.eq(correo))).execute();
        }
    }
}