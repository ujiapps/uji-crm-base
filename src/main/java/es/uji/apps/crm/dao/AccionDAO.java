package es.uji.apps.crm.dao;

import es.uji.apps.crm.model.Accion;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.Cliente;
import es.uji.commons.db.BaseDAO;

public interface AccionDAO extends BaseDAO {

    Accion getAccionByReferencia(Cliente cliente, Campanya campanya);

}