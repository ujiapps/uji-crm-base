package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QTipoTarifaEnvio;
import es.uji.apps.crm.model.TipoTarifa;
import es.uji.apps.crm.model.TipoTarifaEnvio;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoTarifaEnvioDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoTarifaEnvioDAO {
    @Override
    public List<TipoTarifaEnvio> getTipoTarifaEnvioByTipoTarifa(TipoTarifa tipoTarifa) {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoTarifaEnvio qTipoTarifaEnvio = QTipoTarifaEnvio.tipoTarifaEnvio;

        query.from(qTipoTarifaEnvio).where(qTipoTarifaEnvio.tipoTarifa.eq(tipoTarifa));

        return query.list(qTipoTarifaEnvio);
    }

    @Override
    public TipoTarifaEnvio getTipoTarifaEnvioById(Long tipoTarifaEnvioId) {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoTarifaEnvio qTipoTarifaEnvio = QTipoTarifaEnvio.tipoTarifaEnvio;

        query.from(qTipoTarifaEnvio).where(qTipoTarifaEnvio.id.eq(tipoTarifaEnvioId));

        List<TipoTarifaEnvio> res = query.list(qTipoTarifaEnvio);
        if (res.size() > 0)
        {
            return res.get(0);
        }
        return null;
    }
}