package es.uji.apps.crm.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.crm.ui.OpcionUI;
import es.uji.apps.crm.ui.QOpcionUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QItemClasificacion;
import es.uji.apps.crm.model.QTipoEstudio;
import es.uji.apps.crm.model.TipoEstudio;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.json.lookup.LookupItem;

import static java.util.stream.Collectors.toList;

@Repository
public class TipoEstudioDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {

    @Autowired
    private UtilsDAO utilsDAO;

    public TipoEstudio getTipoEstudioById(Long clasificacion) {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoEstudio tipoEstudio = QTipoEstudio.tipoEstudio;
        List<TipoEstudio> lista = query.from(tipoEstudio).where(tipoEstudio.id.eq(clasificacion)).list(tipoEstudio);

        if (!lista.isEmpty()) {
            return lista.get(0);
        } else {
            return null;
        }
    }

    public List<TipoEstudio> getTipoEstudioByTipo(Long tipoEstudioTipo) {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoEstudio tipoEstudio = QTipoEstudio.tipoEstudio;
        return query.from(tipoEstudio).where(tipoEstudio.tipo.eq(tipoEstudioTipo)).list(tipoEstudio);
    }

    public List<TipoEstudio> getTiposEstudio() {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoEstudio tipoEstudio = QTipoEstudio.tipoEstudio;
        QItemClasificacion itemClasificacion = QItemClasificacion.itemClasificacion;


        return query.from(tipoEstudio)
                .list(tipoEstudio, new JPASubQuery().from(itemClasificacion).where(tipoEstudio.id.eq(itemClasificacion.clasificacionEstudio)).count())
                .stream().map(tuple -> {
                            TipoEstudio i = tuple.get(0, TipoEstudio.class);
                            if (i == null) {
                                return null;
                            }
                            i.setTieneItems(0L);
                            if (tuple.get(1, Long.class) > 0) {
                                i.setTieneItems(1L);
                            }
                            return i;
                        }
                ).collect(toList());
    }

    public List<LookupItem> search(String cadena) {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoEstudio qTipoEstudio = QTipoEstudio.tipoEstudio;

        String cadenaFiltrada = utilsDAO.limpiaAcentos(cadena);

        List<LookupItem> result = new ArrayList<>();
        if (ParamUtils.isNotNull(cadenaFiltrada)) {
            query.from(qTipoEstudio);
            query.where(utilsDAO.limpiaAcentos(qTipoEstudio.nombreEs).containsIgnoreCase(cadenaFiltrada)
                    .or(utilsDAO.limpiaAcentos(qTipoEstudio.nombreCa).containsIgnoreCase(cadenaFiltrada))
                    .or(utilsDAO.limpiaAcentos(qTipoEstudio.nombreUk).containsIgnoreCase(cadenaFiltrada)));

            List<TipoEstudio> tiposEstudio = query.list(qTipoEstudio);

            for (TipoEstudio tipoEstudio : tiposEstudio) {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(tipoEstudio.getId().toString());
                lookupItem.setNombre(tipoEstudio.getNombreCa());

                lookupItem.addExtraParam("tipoEstudio", tipoEstudio.getTipoDescripcion());
                result.add(lookupItem);
            }
        }
        return result;
    }

    public List<OpcionUI> getClasificacionEstudiosPorTipo(Long tipoId, String idioma) {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoEstudio qTipoEstudio = QTipoEstudio.tipoEstudio;
        query.from(qTipoEstudio).where(qTipoEstudio.tipo.eq(tipoId));
        if (idioma.equalsIgnoreCase("en") || idioma.equalsIgnoreCase("uk")) {
            query.orderBy(qTipoEstudio.nombreUk.asc());
            return query.list(new QOpcionUI(qTipoEstudio.id.stringValue(), qTipoEstudio.nombreUk));
        } else {
            if (idioma.equalsIgnoreCase("es")) {
                query.orderBy(qTipoEstudio.nombreEs.asc());
                return query.list(new QOpcionUI(qTipoEstudio.id.stringValue(), qTipoEstudio.nombreEs));
            } else {
                query.orderBy(qTipoEstudio.nombreCa.asc());
                return query.list(new QOpcionUI(qTipoEstudio.id.stringValue(), qTipoEstudio.nombreCa));
            }
        }

    }
}