package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ClienteEtiqueta;
import es.uji.apps.crm.model.QClienteEtiqueta;
import es.uji.apps.crm.model.QEtiqueta;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ClienteEtiquetaDAODatabaseImpl extends BaseDAODatabaseImpl implements
        ClienteEtiquetaDAO {

    @Override
    public List<ClienteEtiqueta> getClienteEtiquetasByClienteId(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteEtiqueta clienteEtiqueta = QClienteEtiqueta.clienteEtiqueta;
        QEtiqueta etiqueta = QEtiqueta.etiqueta1;

        query.from(clienteEtiqueta)
                .join(clienteEtiqueta.etiqueta, etiqueta).fetch()
                .where(clienteEtiqueta.cliente.id.eq(clienteId));

        return query.list(clienteEtiqueta);
    }

    @Override
    public ClienteEtiqueta getClienteEtiquetaById(Long clienteEtiquetaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteEtiqueta clienteEtiqueta = QClienteEtiqueta.clienteEtiqueta;

        query.from(clienteEtiqueta)
                .where(clienteEtiqueta.id.eq(clienteEtiquetaId));

        List<ClienteEtiqueta> listaClienteEtiquetas = query.list(clienteEtiqueta);
        if (listaClienteEtiquetas.size() > 0)
        {
            return listaClienteEtiquetas.get(0);
        }
        return null;
    }

}