package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.EnvioItem;
import es.uji.apps.crm.model.QEnvioItem;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EnvioItemDAODatabaseImpl extends BaseDAODatabaseImpl implements EnvioItemDAO {
    QEnvioItem envioItem = QEnvioItem.envioItem;

    @Override
    public EnvioItem getEnvioItemById(Long envioItemId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(envioItem).where(envioItem.id.eq(envioItemId));

        List<EnvioItem> lista = query.list(envioItem);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

    @Override
    public List<EnvioItem> getEnvioItemByEnvioId(Long envioId) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(envioItem).where(envioItem.envio.id.eq(envioId));
        return query.list(envioItem);
    }

    @Override
    public List<EnvioItem> getEnvioItemByItemIdAndEnvioId(Long itemId, Long envioId) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(envioItem).where(
                envioItem.envio.id.eq(envioId).and(envioItem.item.id.eq(itemId)));
        return query.list(envioItem);
    }
}