package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.ProgramaCurso;
import es.uji.commons.db.BaseDAO;

public interface ProgramaCursoDAO extends BaseDAO {
    List<ProgramaCurso> getProgramaCursosByProgramaId(Long programaId);

//    Accion getAccionByReferencia(Cliente cliente, Campanya campanya);

}