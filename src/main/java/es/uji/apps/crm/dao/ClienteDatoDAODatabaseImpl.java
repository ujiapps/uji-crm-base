package es.uji.apps.crm.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.crm.model.CampanyaDato;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteDato;
import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.QCampanyaCliente;
import es.uji.apps.crm.model.QCliente;
import es.uji.apps.crm.model.QClienteDato;
import es.uji.apps.crm.model.QClienteDatoOpcion;
import es.uji.apps.crm.model.QClienteDatoTipo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ClienteDatoDAODatabaseImpl extends BaseDAODatabaseImpl implements ClienteDatoDAO {

    @Override
    public ClienteDato getClienteDatoById(Long clienteDatoId) {

        JPAQuery query = new JPAQuery(entityManager);
        QClienteDato clienteDato = QClienteDato.clienteDato;

        query.from(clienteDato)
                .where(clienteDato.id.eq(clienteDatoId));

        List<ClienteDato> lista = query.list(clienteDato);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

    @Override
    public ClienteDato getClienteDatoByClienteAndTipoId(Cliente cliente, Long tipoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDato clienteDato = QClienteDato.clienteDato;
        QCliente qCliente = QCliente.cliente;

        query.from(clienteDato)
                .join(clienteDato.cliente, qCliente).fetch()
                .where(qCliente.id.eq(cliente.getId())
                        .and(clienteDato.clienteDatoTipo.id.eq(tipoId)));

        List<ClienteDato> lista = query.list(clienteDato);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;

    }

    @Override
    public ClienteDato getClienteDatoByClienteAndTipoIdAndItem(Cliente cliente, Long tipoId, Item item) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDato clienteDato = QClienteDato.clienteDato;

        query.from(clienteDato)
                .where(clienteDato.cliente.id.eq(cliente.getId())
                        .and(clienteDato.clienteDatoTipo.id.eq(tipoId)
                                .and(clienteDato.item.id.eq(item.getId()))));

        List<ClienteDato> lista = query.list(clienteDato);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;

    }

    @Override
    public ClienteDato getClienteDatoByClienteAndTipoIdAndClienteDato(Cliente cliente, Long tipoId, Long padreDatoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDato clienteDato = QClienteDato.clienteDato;

        query.from(clienteDato)
                .where(clienteDato.cliente.id.eq(cliente.getId())
                        .and(clienteDato.clienteDatoTipo.id.eq(tipoId)
                                .and(clienteDato.clienteDatoId.eq(padreDatoId))));

        List<ClienteDato> lista = query.list(clienteDato);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;

    }

    @Override
    public List<ClienteDato> getClienteDatoByClienteDatoId(Long datoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDato clienteDato = QClienteDato.clienteDato;

        query.from(clienteDato)
                .where(clienteDato.clienteDatoId.eq(datoId));

        return query.list(clienteDato);
    }

    @Override
    public List<ClienteDato> getClienteDatoByItemId(Long itemId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDato clienteDato = QClienteDato.clienteDato;

        query.from(clienteDato)
                .where(clienteDato.item.id.eq(itemId));

        return query.list(clienteDato);

    }

    @Override
    public List<ClienteDato> getClienteDatoByClienteAndItemId(Cliente cliente, Long itemId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDato clienteDato = QClienteDato.clienteDato;

        query.from(clienteDato)
                .where(clienteDato.cliente.id.eq(cliente.getId())
                        .and(clienteDato.item.id.eq(itemId)));

        return query.list(clienteDato);
    }


    @Override
    public List<ClienteDato> getDatosCampanyaByCliente(List<CampanyaDato> campanyaDatos, Cliente cliente) {
        JPAQuery query = new JPAQuery(entityManager);

        QClienteDato clienteDato = QClienteDato.clienteDato;

        List<Long> campanyaDatoId = new ArrayList<Long>();
        for (CampanyaDato campanyaDato : campanyaDatos)
        {
            campanyaDatoId.add(campanyaDato.getClienteDatoTipo().getId());
        }
        query.from(clienteDato)
                .where(clienteDato.cliente.eq(cliente)
                        .and(clienteDato.clienteDatoTipo.id.in(campanyaDatoId)));

        return query.list(clienteDato);
    }

    @Override
    public List<ClienteDato> getDatosAsociadosACampanya(Long campanyaId, Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);

        QClienteDato clienteDato = QClienteDato.clienteDato;
        QClienteDatoTipo clienteDatoTipo = QClienteDatoTipo.clienteDatoTipo;
        QClienteDatoOpcion clienteDatoOpcion = QClienteDatoOpcion.clienteDatoOpcion;

        query.from(clienteDato)
                .join(clienteDato.clienteDatoTipo, clienteDatoTipo).fetch()
                .leftJoin(clienteDatoTipo.clientesDatosOpciones, clienteDatoOpcion).fetch()
                .where(clienteDato.cliente.id.eq(clienteId)
                        .and(clienteDato.campanya.id.eq(campanyaId)));

        return query.list(clienteDato);

    }

    @Override
    public ClienteDato getClienteDatoByClienteAndTipoIdVinculadoCampanya(Cliente cliente, Long tipoDatoId, Long campanyaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDato clienteDato = QClienteDato.clienteDato;

        query.from(clienteDato)
                .where(clienteDato.cliente.id.eq(cliente.getId())
                        .and(clienteDato.clienteDatoTipo.id.eq(tipoDatoId)
                                .and(clienteDato.campanya.id.eq(campanyaId))));

        return query.singleResult(clienteDato);
    }

    @Override
    public List<ClienteDato> getClienteDatosByCampanyaClienteId(Long campanyaClienteId) {

        JPAQuery query = new JPAQuery(entityManager);
        QClienteDato clienteDato = QClienteDato.clienteDato;
        QCampanyaCliente campanyaCliente = QCampanyaCliente.campanyaCliente;

        query.from(clienteDato).from(campanyaCliente)
                .where(clienteDato.campanya.id.eq(campanyaCliente.campanya.id)
                        .and(campanyaCliente.id.eq(campanyaClienteId))
                        .and(campanyaCliente.cliente.id.eq(clienteDato.cliente.id)));

        return query.list(clienteDato);


    }

    @Transactional
    @Override
    public void addClienteDatoExtraEntradas(Long acto, Long entradas) {

        QClienteDato clienteDato = QClienteDato.clienteDato;

        JPAUpdateClause query = new JPAUpdateClause(entityManager, clienteDato);

        Long datoEntradaNueva = 26L;

        String insert = "";

        insert = "insert into crm_clientes_datos (id, tipo_id, tipo_acceso_id, campanya_id, cliente_id, valor_numero)" +
                " select hibernate_sequence.nextval, " + datoEntradaNueva + ", 9 , " +
                "  ca.campanya_id, cc.cliente_id, " +
                " case when nvl((select max(o_s.valor) from crm_clientes_datos_opciones o_s, crm_clientes_datos d_s, crm_campanyas_datos cd_s, crm_campanyas_formularios f_s where o_s.id = d_s.valor_numero and d_s.cliente_id = cc.cliente_id and d_s.campanya_id = f_s.campanya_id and d_s.tipo_id = cd_s.dato_id and cd_s.formulario_id = f_s.id and f_s.campanya_id = ca.campanya_id and cd_s.etiqueta = 'ENTRADAS-SOBRANTES'), 0) > " + entradas + "" +
                " then " + entradas +
                " else nvl((select max(o_s.valor) from crm_clientes_datos_opciones o_s, crm_clientes_datos d_s, crm_campanyas_datos cd_s, crm_campanyas_formularios f_s where o_s.id = d_s.valor_numero and d_s.cliente_id = cc.cliente_id and d_s.campanya_id = f_s.campanya_id and d_s.tipo_id = cd_s.dato_id and cd_s.formulario_id = f_s.id and f_s.campanya_id = ca.campanya_id and cd_s.etiqueta = 'ENTRADAS-SOBRANTES'),0) end" +
                " from crm_campanyas_clientes cc, crm_campanyas_actos ca where ca.campanya_id = cc.campanya_id and ca.id = " + acto +
                " and cc.tipo_id in (select id from crm_campanyas_estados e where accion = 'ESTADO-ALTA' and e.campanya_id = cc.campanya_id)" +
                " and not exists (select * from crm_clientes_datos where cliente_id = cc.cliente_id and campanya_id = cc.campanya_id and tipo_id = " + datoEntradaNueva + ")";

        entityManager.createNativeQuery(insert).executeUpdate();
    }

    @Override
    public ClienteDato getClienteDatoByClienteAndTipoIdAndClienteDatoAndVinculadoCampanya(Cliente cliente, Long tipoId, Long campanyaId, Long tipoVinculadoId) {

        JPAQuery query = new JPAQuery(entityManager);
        QClienteDato clienteDato = QClienteDato.clienteDato;
        QClienteDato clienteDatoRelacionado = QClienteDato.clienteDato;

        query.from(clienteDato, clienteDatoRelacionado)
                .where(clienteDato.cliente.id.eq(cliente.getId())
                        .and(clienteDato.clienteDatoId.eq(clienteDatoRelacionado.id))
                        .and(clienteDatoRelacionado.clienteDatoTipo.id.eq(tipoVinculadoId))
                        .and(clienteDato.clienteDatoTipo.id.eq(tipoId)
                                .and(clienteDato.campanya.id.eq(campanyaId))));

        List<ClienteDato> lista = query.list(clienteDato);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;


    }

    @Override
    public List<ClienteDato> getDatosByClienteId(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDato clienteDato = QClienteDato.clienteDato;

        query.from(clienteDato).where(clienteDato.cliente.id.eq(clienteId)
                .and(clienteDato.campanya.isNull())
                .and(clienteDato.clienteDatoId.isNull())
                .and(clienteDato.item.isNull()));
        return query.list(clienteDato);
    }

    @Override
    public List<ClienteDato> getAllDatosByClienteId(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteDato clienteDato = QClienteDato.clienteDato;

        query.from(clienteDato).where(clienteDato.cliente.id.eq(clienteId));
        return query.list(clienteDato);
    }
}