package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.TipoTarifa;
import es.uji.commons.db.BaseDAO;

public interface TipoTarifaDAO extends BaseDAO {

    List<TipoTarifa> getTiposTarifa(List<Long> campanyasAdmin);

    List<TipoTarifa> getTiposTarifaByCampanyaId(Long campanyaId);

    TipoTarifa getTipoTarifaById(Long tipoTarifaId);

    List<TipoTarifa> getTiposTarifa(Long connectedUserId);

}