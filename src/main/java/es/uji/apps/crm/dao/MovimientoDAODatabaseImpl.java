package es.uji.apps.crm.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.Movimiento;
import es.uji.apps.crm.model.QCampanya;
import es.uji.apps.crm.model.QCliente;
import es.uji.apps.crm.model.QMovimiento;
import es.uji.apps.crm.model.QTipo;
import es.uji.apps.crm.model.QTipoEstadoCampanya;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class MovimientoDAODatabaseImpl extends BaseDAODatabaseImpl implements MovimientoDAO {
    @Override
    public Movimiento getMovimientoByClienteIdAndCampanyaIdAndTipoId(Long clienteId,
                                                                     Long campanyaId, Long campanyaEnvioTipoId) {

        JPAQuery query = new JPAQuery(entityManager);
        QMovimiento qMovimiento = QMovimiento.movimiento;

        query.from(qMovimiento)
                .where(qMovimiento.cliente.id.eq(clienteId)
                        .and(qMovimiento.campanya.id.eq(campanyaId))
                        .and(qMovimiento.tipo.id.eq(campanyaEnvioTipoId)))
                .orderBy(qMovimiento.fecha.desc());

        List<Movimiento> result = query.list(qMovimiento);

        if (result.size() > 0)
        {
            return result.get(0);
        }
        return null;
    }

    @Override
    public List<Movimiento> getMovimientosByClienteId(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        QMovimiento qMovimiento = QMovimiento.movimiento;
        QCliente cliente = QCliente.cliente;
        QTipo tipo = QTipo.tipo;
        QCampanya campanya = QCampanya.campanya1;

        query.from(qMovimiento)
                .join(qMovimiento.cliente, cliente).fetch()
                .join(qMovimiento.tipo, tipo).fetch()
                .join(qMovimiento.campanya, campanya).fetch()
                .where(cliente.id.eq(clienteId));
        return query.list(qMovimiento);

    }

    @Override
    public List<Movimiento> getMovimientoByClienteIdAndCampanyaId(Long campanyaId, Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        QMovimiento qMovimiento = QMovimiento.movimiento;
        QCliente qCliente = QCliente.cliente;
        QTipo qTipo = QTipo.tipo;
        QCampanya qCampanya = QCampanya.campanya1;

        query.from(qMovimiento)
                .join(qMovimiento.cliente, qCliente).fetch()
                .join(qMovimiento.tipo, qTipo).fetch()
                .join(qMovimiento.campanya, qCampanya).fetch()
                .where(qMovimiento.cliente.id.eq(clienteId).and(
                        qMovimiento.campanya.id.eq(campanyaId))).orderBy(qMovimiento.fecha.desc());

        return query.list(qMovimiento);

    }

    @Transactional
    @Override
    public void deleteMovimientosByCampanyaCliente(Campanya campanya, Cliente cliente) {
        QMovimiento qMovimiento = QMovimiento.movimiento;

        JPADeleteClause query = new JPADeleteClause(entityManager, qMovimiento);
        query.where(qMovimiento.campanya.id.eq(campanya.getId()).and(qMovimiento.cliente.id.eq(cliente.getId()))).execute();
    }

    @Override
    public Date getFechaAlta(Long clienteId, Long campanyaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QMovimiento qMovimiento = QMovimiento.movimiento;
        QCampanya campanya = QCampanya.campanya1;
        QTipoEstadoCampanya estadoCampanya = QTipoEstadoCampanya.tipoEstadoCampanya;


        query.from(qMovimiento).join(qMovimiento.campanya, campanya)
                .join(campanya.campanyaEstados, estadoCampanya)
                .where(qMovimiento.cliente.id.eq(clienteId)
                        .and(qMovimiento.campanya.id.eq(campanyaId))
                        .and(estadoCampanya.accion.eq("ESTADO-ALTA")))
                .orderBy(qMovimiento.fecha.desc());

        List<Movimiento> result = query.list(qMovimiento);

        if (result.size() > 0)
        {
            return result.get(0).getFecha();
        }
        return null;
    }

    @Override
    public Date getFechaBaja(Long clienteId, Long campanyaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QMovimiento qMovimiento = QMovimiento.movimiento;
        QCampanya campanya = QCampanya.campanya1;
        QTipoEstadoCampanya estadoCampanya = QTipoEstadoCampanya.tipoEstadoCampanya;


        query.from(qMovimiento).join(qMovimiento.campanya, campanya).join(campanya.campanyaEstados, estadoCampanya)
                .where(qMovimiento.cliente.id.eq(clienteId)
                        .and(qMovimiento.campanya.id.eq(campanyaId))
                        .and(estadoCampanya.accion.eq("ESTADO-BAJA")))
                .orderBy(qMovimiento.fecha.desc());

        List<Movimiento> result = query.list(qMovimiento);

        if (result.size() > 0)
        {
            return result.get(0).getFecha();
        }
        return null;
    }


}