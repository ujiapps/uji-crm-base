package es.uji.apps.crm.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.PersonaPasPdi;
import es.uji.apps.crm.model.QPersonaPasPdi;
import es.uji.apps.crm.model.QServicioUsuario;
import es.uji.apps.crm.model.ServicioUsuario;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ServicioUsuarioDAODatabaseImpl extends BaseDAODatabaseImpl implements
        ServicioUsuarioDAO {

    @Override
    public List<PersonaPasPdi> getPersonas() {
        JPAQuery query = new JPAQuery(entityManager);

        QServicioUsuario servicioUsuario = QServicioUsuario.servicioUsuario;
        QPersonaPasPdi persona = QPersonaPasPdi.personaPasPdi;

        query.from(persona)
                .join(persona.serviciosUsuario, servicioUsuario)
                .orderBy(persona.nombre.desc());

        return query.list(persona);
    }

    @Override
    public List<ServicioUsuario> getServicioUSuario() {
        JPAQuery query = new JPAQuery(entityManager);
        QServicioUsuario servicioUsuario = QServicioUsuario.servicioUsuario;

        query.from(servicioUsuario)
                .orderBy(servicioUsuario.id.desc());

        return query.list(servicioUsuario);
    }

    @Override
    public List<ServicioUsuario> getServiciosUsuarioById(Long servicioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QServicioUsuario servicioUsuario = QServicioUsuario.servicioUsuario;
        QPersonaPasPdi personaPasPdi = QPersonaPasPdi.personaPasPdi;

        query.from(servicioUsuario)
                .leftJoin(servicioUsuario.personaPasPdi, personaPasPdi).fetch()
                .where(servicioUsuario.servicio.id.eq(servicioId));

        return query.list(servicioUsuario);
    }

    @Override
    public List<ServicioUsuario> getServiciosUsuariosByPersonaId(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);

        QServicioUsuario servicioUsuario = QServicioUsuario.servicioUsuario;

        query.from(servicioUsuario)
                .where(servicioUsuario.personaPasPdi.id.eq(personaId));

        return query.list(servicioUsuario);
    }

    @Override
    public List<Long> getServiciosIdByPersonaId(Long usuarioId) {
        List<ServicioUsuario> serviciosUsuarios = getServiciosUsuariosByPersonaId(usuarioId);
        List<Long> serviciosPersonaConectadaIds = new ArrayList<Long>();
        for (ServicioUsuario servicioUsuario : serviciosUsuarios)
        {
            serviciosPersonaConectadaIds.add(servicioUsuario.getServicio().getId());
        }
        return serviciosPersonaConectadaIds;
    }

}