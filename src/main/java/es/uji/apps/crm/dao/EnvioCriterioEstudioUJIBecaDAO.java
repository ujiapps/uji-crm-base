package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.EnvioCriterioEstudioUJIBeca;
import es.uji.apps.crm.model.QEnvioCriterioEstudioUJIBeca;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EnvioCriterioEstudioUJIBecaDAO extends BaseDAODatabaseImpl {
    public List<EnvioCriterioEstudioUJIBeca> getEnvioCriterioEstudioUJIBecaByEnvioCriterioId(Long envioCriterioId) {

        QEnvioCriterioEstudioUJIBeca qEnvioCriterioEstudioUJIBeca = QEnvioCriterioEstudioUJIBeca.envioCriterioEstudioUJIBeca;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEnvioCriterioEstudioUJIBeca).where(qEnvioCriterioEstudioUJIBeca.envioCriterio.id.eq(envioCriterioId));

        return query.list(qEnvioCriterioEstudioUJIBeca);
    }
}