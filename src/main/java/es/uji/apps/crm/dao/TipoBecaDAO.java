package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QTipoBeca;
import es.uji.apps.crm.model.TipoBeca;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoBecaDAO extends BaseDAODatabaseImpl {
    public List<TipoBeca> getTiposBeca() {

        QTipoBeca tipoBeca = QTipoBeca.tipoBeca;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(tipoBeca);

        return query.list(tipoBeca);
    }
}