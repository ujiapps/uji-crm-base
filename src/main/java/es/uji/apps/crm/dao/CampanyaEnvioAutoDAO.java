package es.uji.apps.crm.dao;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.crm.model.*;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

import static java.util.stream.Collectors.toList;

@Repository
public class CampanyaEnvioAutoDAO extends BaseDAODatabaseImpl{

    QCampanyaEnvioAuto campanyaEnvioAuto = QCampanyaEnvioAuto.campanyaEnvioAuto;
    QCampanyaCliente campanyaCliente = QCampanyaCliente.campanyaCliente;
    QTipoEstadoCampanya tipoEstadoCampanya = QTipoEstadoCampanya.tipoEstadoCampanya;


    public List<CampanyaEnvioAuto> getCampanyaEnviosAutoByCampanyaId(Long campanyaId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(campanyaEnvioAuto)
                .where(campanyaEnvioAuto.campanya.id.eq(campanyaId));

        return query.list(campanyaEnvioAuto);
    }

    public List<CampanyaEnvioAuto> getCampanyaEnvioAutoByCampanyaIdAndTipoId(Long campanyaId, Long tipoId, Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(campanyaEnvioAuto)
                .where(campanyaEnvioAuto.campanya.id.eq(campanyaId)
                        .and(campanyaEnvioAuto.tipoEnvio.id.in(86L, 88L)));

        if (ParamUtils.isNotNull(tipoId)) {
            query.where(campanyaEnvioAuto.campanyaEnvioTipo.id.eq(tipoId));
        } else {
            query.where(campanyaEnvioAuto.campanyaEnvioTipo.isNull());
        }

        return query.list(campanyaEnvioAuto)
                .stream().map(tuple -> {
                    if ((tuple.getTipoEnvio().getId() == 86L) || (tuple.getTipoEnvio().getId() == 88L && isAlumniSaujiPremium(clienteId) == 0)){
                        return tuple;
                    }
                    return null;
                }).filter(e -> e != null).collect(Collectors.toList());

    }

    private Long isAlumniSaujiPremium(Long clienteId){
        return new JPAQuery(entityManager)
                .from(campanyaCliente)
                .join(tipoEstadoCampanya).on(campanyaCliente.campanyaClienteTipo.id.eq(tipoEstadoCampanya.id))
                .where(campanyaCliente.campanya.id.eq(1070562L) //CAMPANYA ALUMNISAUJI PREMIUM
                        .and(campanyaCliente.cliente.id.eq(clienteId))
                        .and(tipoEstadoCampanya.accion.in("ESTADO-ALTA","ESTADO-ALTA-WEB")))
                .count();
    }
}