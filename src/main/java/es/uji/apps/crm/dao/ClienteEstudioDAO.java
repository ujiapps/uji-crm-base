package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.ClienteEstudio;
import es.uji.commons.db.BaseDAO;

public interface ClienteEstudioDAO extends BaseDAO {
    List<ClienteEstudio> getClienteEstudiosByPersona(Long personaId);
}