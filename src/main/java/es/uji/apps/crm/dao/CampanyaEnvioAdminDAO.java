package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.CampanyaEnvioAdmin;
import es.uji.commons.db.BaseDAO;

public interface CampanyaEnvioAdminDAO extends BaseDAO {
    List<CampanyaEnvioAdmin> getCampanyaEnviosAdminByCampanyaId(Long campanyaId);
}