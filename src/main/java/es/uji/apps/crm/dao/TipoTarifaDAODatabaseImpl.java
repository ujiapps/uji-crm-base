package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QCampanya;
import es.uji.apps.crm.model.QPersonaPrograma;
import es.uji.apps.crm.model.QPrograma;
import es.uji.apps.crm.model.QTipoTarifa;
import es.uji.apps.crm.model.TipoTarifa;
import es.uji.apps.crm.model.domains.TipoAccesoProgramaUsuario;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoTarifaDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoTarifaDAO {

    @Override
    public List<TipoTarifa> getTiposTarifa(List<Long> campanyasAdmin) {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoTarifa qTipoTarifa = QTipoTarifa.tipoTarifa;

        query.from(qTipoTarifa).where(qTipoTarifa.campanya.id.in(campanyasAdmin));
        return query.list(qTipoTarifa);
    }

    @Override
    public List<TipoTarifa> getTiposTarifaByCampanyaId(Long campanyaId) {

        JPAQuery query = new JPAQuery(entityManager);
        QTipoTarifa qTipoTarifa = QTipoTarifa.tipoTarifa;

        query.from(qTipoTarifa).where(qTipoTarifa.campanya.id.eq(campanyaId));
        return query.list(qTipoTarifa);
    }

    @Override
    public TipoTarifa getTipoTarifaById(Long tipoTarifaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoTarifa qTipoTarifa = QTipoTarifa.tipoTarifa;

        query.from(qTipoTarifa).where(qTipoTarifa.id.eq(tipoTarifaId));

        List<TipoTarifa> res = query.list(qTipoTarifa);
        if (res.size() > 0)
        {
            return res.get(0);
        }
        else
        {
            return null;
        }

    }

    @Override
    public List<TipoTarifa> getTiposTarifa(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoTarifa qTipoTarifa = QTipoTarifa.tipoTarifa;
        QCampanya campanya = QCampanya.campanya1;
        QPrograma programa = QPrograma.programa;
        QPersonaPrograma personaPrograma = QPersonaPrograma.personaPrograma;

        query.from(qTipoTarifa).join(qTipoTarifa.campanya, campanya).join(campanya.programa, programa).join(programa.personaProgramas, personaPrograma)
                .where(personaPrograma.tipoAcceso.eq(TipoAccesoProgramaUsuario.ADMIN.getNombre()).and(personaPrograma.persona.id.eq(connectedUserId)));
        return query.list(qTipoTarifa);
    }
}