package es.uji.apps.crm.dao;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

@Component
public class RecibosManager {
    private static DataSource dataSource;
    private Recibos recibo;
    private CreaRecibo creaRecibo;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        RecibosManager.dataSource = dataSource;
    }

    public void init() {
        this.recibo = new Recibos(dataSource);
        this.creaRecibo = new CreaRecibo(dataSource);
    }

    public void borra(Long reciboId) {
        recibo.borra(reciboId);
    }

    public Long creaRecibo(String xml) {
        return creaRecibo.creaRecibo(xml);
    }

    private class Recibos extends StoredProcedure {
        private static final String SQL = "uji_recibos.borra_recibo";
        private static final String PRECIBO = "p_recibo";

        public Recibos(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(PRECIBO, Types.BIGINT));
            compile();
        }

        public void borra(Long reciboId) {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PRECIBO, reciboId);
            Map<String, Object> results = execute(inParams);
        }
    }


    private class CreaRecibo extends StoredProcedure {
        private static final String SQL = "uji_crm.crea_recibo_xml";
        private static final String XML = "p_xml";
        private static final String RECIBO = "p_recibo";

        public CreaRecibo(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(XML, Types.VARCHAR));
            declareParameter(new SqlOutParameter(RECIBO, Types.BIGINT));
            compile();
        }

        public Long creaRecibo(String xml) {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(XML, xml);
            inParams.put(RECIBO, "");
            Map<String, Object> results = execute(inParams);
            return (Long) results.get(RECIBO);
        }
    }

}