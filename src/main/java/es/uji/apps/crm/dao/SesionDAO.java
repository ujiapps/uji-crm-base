package es.uji.apps.crm.dao;

import java.util.Date;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.QCliente;
import es.uji.apps.crm.model.QClienteGeneral;
import es.uji.apps.crm.model.QPersona;
import es.uji.apps.crm.model.QSesion;
import es.uji.apps.crm.model.Sesion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class SesionDAO extends BaseDAODatabaseImpl {
    private final QSesion qSesion = QSesion.sesion;
    private final QCliente qCliente = QCliente.cliente;
    private final QClienteGeneral qClienteGeneral = QClienteGeneral.clienteGeneral;
    private final QPersona qPersona = QPersona.persona;

    public Cliente getUserIdActiveSesion(String hash, Boolean zonaPrivada) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qSesion)
                .join(qSesion.cliente, qCliente).fetch()
                .join(qCliente.clienteGeneral, qClienteGeneral).fetch()
                .leftJoin(qClienteGeneral.persona, qPersona).fetch()
                .where(qSesion.id.eq(hash).and(qSesion.fechaCaducidad.after(new Date())));

        if (zonaPrivada) {
            query.where(qCliente.fichaZonaPrivada.isTrue()
                    .and(qCliente.servicio.id.eq(366L)));
        }

        Sesion sesion = query.singleResult(qSesion);

        if (sesion != null) {
            return sesion.getCliente();
        }
        return null;
    }

    public Boolean isValidSession(String hash) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qSesion)
                .where(qSesion.id.eq(hash).and(qSesion.fechaCaducidad.after(new Date())));

        return query.singleResult(qSesion) != null;
    }

    public Sesion getSesionByHash(String hash) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qSesion).join(qSesion.cliente, qCliente).fetch()
                .where(qSesion.id.eq(hash));

        return query.singleResult(qSesion);
    }

    @Transactional
    public void deleteSesion(Long id) {
        JPADeleteClause jpaDeleteClause = new JPADeleteClause(entityManager, qSesion);

        jpaDeleteClause.where(qSesion.cliente.id.eq(id).and(qSesion.envio.isNull())).execute();
    }

    public Boolean getHashBajaMensajeriaValido(String pHash) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qSesion)
                .where(qSesion.id.eq(pHash).and(qSesion.fechaCaducidad.after(new Date())).and(qSesion.envio.isNotNull()));

        Sesion sesion = query.singleResult(qSesion);

        if (sesion != null) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}