package es.uji.apps.crm.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.crm.model.EnvioPlantillaAdmin;
import es.uji.apps.crm.model.QEnvioPlantillaAdmin;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

@Repository
public class EnvioPlantillaAdminDAO extends BaseDAODatabaseImpl {
    public EnvioPlantillaAdmin getEnvioPlantillaAdminByNombre(String nombre) {
        QEnvioPlantillaAdmin qEnvioPlantillaAdmin = QEnvioPlantillaAdmin.envioPlantillaAdmin;
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qEnvioPlantillaAdmin).where(qEnvioPlantillaAdmin.nombre.eq(nombre)).singleResult(qEnvioPlantillaAdmin);
    }
}
