package es.uji.apps.crm.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaFormulario;
import es.uji.apps.crm.model.QCampanya;
import es.uji.apps.crm.model.QCampanyaCliente;
import es.uji.apps.crm.model.QCampanyaDato;
import es.uji.apps.crm.model.QCampanyaFormulario;
import es.uji.apps.crm.model.QCampanyaFormularioEstadoOrigen;
import es.uji.apps.crm.model.QCliente;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Repository
public class CampanyaFormularioDAO extends BaseDAODatabaseImpl {
    public List<CampanyaFormulario> getCampanyaFormulariosByCampanya(Campanya campanya) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaFormulario campanyaFormulario = QCampanyaFormulario.campanyaFormulario;

        query.from(campanyaFormulario)
                .where(campanyaFormulario.campanya.id.eq(campanya.getId()));

        return query.list(campanyaFormulario);
    }

    public CampanyaFormulario getFormularioById(Long campanyaFormularioId) {
        JPAQuery query = new JPAQuery(entityManager);
        QCampanyaFormulario campanyaFormulario = QCampanyaFormulario.campanyaFormulario;
        QCampanya campanya = QCampanya.campanya1;

        query.from(campanyaFormulario)
                .join(campanyaFormulario.campanya, campanya).fetch()
                .where(campanyaFormulario.id.eq(campanyaFormularioId))
                .orderBy(campanyaFormulario.titulo.asc());

        List<CampanyaFormulario> lista = query.list(campanyaFormulario);
        if (lista.size() > 0) {
            return lista.get(0);
        }
        return null;
    }

    public CampanyaFormulario getFormularioByCampanyaIdAndUsuario(Long campanyaId, Long connectedUserId) {

        JPAQuery queryCaso1 = new JPAQuery(entityManager);
        JPAQuery queryCaso2 = new JPAQuery(entityManager);

        QCampanyaFormulario campanyaFormulario = QCampanyaFormulario.campanyaFormulario;
        QCampanya campanya = QCampanya.campanya1;
        QCampanyaCliente campanyaCliente = QCampanyaCliente.campanyaCliente;
        QCliente cliente = QCliente.cliente;
        QCampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen = QCampanyaFormularioEstadoOrigen.campanyaFormularioEstadoOrigen;

        JPAQuery queryEstado = new JPAQuery(entityManager);
        Date hoy = new Date();

        if (ParamUtils.isNotNull(connectedUserId)) {

            queryEstado.from(campanyaCliente)
                    .join(campanyaCliente.cliente, cliente)
                    .where(cliente.clienteGeneral.persona.id.eq(connectedUserId)
                            .and(campanyaCliente.campanya.id.eq(campanyaId)));

            if (queryEstado.list(campanyaCliente).size() > 0) {
                Long estadoCliente = queryEstado.list(campanyaCliente).get(0).getCampanyaClienteTipo().getId();

                if (ParamUtils.isNotNull(estadoCliente)) {
                    queryCaso1.from(campanyaFormulario)
                            .join(campanyaFormulario.campanya, campanya).fetch()
                            .leftJoin(campanyaFormulario.campanyaFormularioEstadosOrigen, campanyaFormularioEstadoOrigen)
                            .where(campanyaFormulario.campanya.id.eq(campanyaId)
                                    .and((campanyaFormularioEstadoOrigen.estadoOrigen.id.eq(estadoCliente).or(campanyaFormularioEstadoOrigen.isNull())))
                                    .and(campanyaFormulario.fechaFin.after(hoy).or(campanyaFormulario.fechaFin.isNull()))
                                    .and(campanyaFormulario.fechaInicio.before(hoy).or(campanyaFormulario.fechaInicio.isNull())));
                    List<CampanyaFormulario> lista = queryCaso1.list(campanyaFormulario);
                    if (lista.size() > 0) {
                        return lista.get(0);
                    }
                }
            } else return getFormularioGenericoByCampanya(campanyaId);
        }

        return getFormularioGenericoByCampanya(campanyaId);
    }

    public CampanyaFormulario getCampanyaFormulariosByCampanyaAndEstado(Long campanyaId, Long estadoId) {

        JPAQuery query = new JPAQuery(entityManager);

        Date hoy = new Date();

        QCampanyaFormulario campanyaFormulario = QCampanyaFormulario.campanyaFormulario;
        query.from(campanyaFormulario)
                .where(campanyaFormulario.campanya.id.eq(campanyaId)
                        .and(campanyaFormulario.estadoDestino.id.eq(estadoId))
                        .and(campanyaFormulario.fechaFin.after(hoy).or(campanyaFormulario.fechaFin.isNull()))
                        .and(campanyaFormulario.fechaInicio.before(hoy).or(campanyaFormulario.fechaInicio.isNull())));

        List<CampanyaFormulario> lista = query.list(campanyaFormulario);
        if (lista.isEmpty()) return null;
        return lista.get(0);

    }

    @Transactional
    public void deleteFormularioDatos(Long campanyaFormularioId) {
        QCampanyaDato campanyaDato = QCampanyaDato.campanyaDato1;

        JPADeleteClause query = new JPADeleteClause(entityManager, campanyaDato);
        query.where(campanyaDato.campanyaFormulario.id.eq(campanyaFormularioId)).execute();
    }

    @Transactional
    public void deleteFormularioEstados(Long campanyaFormularioId) {
        QCampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen = QCampanyaFormularioEstadoOrigen.campanyaFormularioEstadoOrigen;

        JPADeleteClause query = new JPADeleteClause(entityManager, campanyaFormularioEstadoOrigen);
        query.where(campanyaFormularioEstadoOrigen.campanyaFormulario.id.eq(campanyaFormularioId)).execute();

    }

    public CampanyaFormulario getFormularioGenericoByCampanya(Long campanyaId) {
        QCampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen = QCampanyaFormularioEstadoOrigen.campanyaFormularioEstadoOrigen;

        JPAQuery query = new JPAQuery(entityManager);

        Date hoy = new Date();

        QCampanyaFormulario campanyaFormulario = QCampanyaFormulario.campanyaFormulario;
        query.from(campanyaFormulario).join(campanyaFormulario.campanyaFormularioEstadosOrigen, campanyaFormularioEstadoOrigen)
                .where(campanyaFormulario.campanya.id.eq(campanyaId)
                        .and(campanyaFormularioEstadoOrigen.estadoOrigen.isNull())
                        .and(campanyaFormulario.fechaFin.after(hoy).or(campanyaFormulario.fechaFin.isNull()))
                        .and(campanyaFormulario.fechaInicio.before(hoy).or(campanyaFormulario.fechaInicio.isNull())));

        List<CampanyaFormulario> lista = query.list(campanyaFormulario);
        if (lista.isEmpty()) return null;
        return lista.get(0);

    }

    public CampanyaFormulario getFormularioByFormularioId(Long formularioId, Long clienteId) {

        QCampanyaFormularioEstadoOrigen qCampanyaFormularioEstadoOrigen = QCampanyaFormularioEstadoOrigen.campanyaFormularioEstadoOrigen;
        QCampanyaCliente qCampanyaCliente = QCampanyaCliente.campanyaCliente;
        QCampanyaFormulario qCampanyaFormulario = QCampanyaFormulario.campanyaFormulario;
        QCampanya qCampanya = QCampanya.campanya1;
        QCliente qCliente = QCliente.cliente;

        Date hoy = new Date();

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qCampanyaFormulario)
                .join(qCampanyaFormulario.campanya, qCampanya)
                .join(qCampanya.campanyasClientes, qCampanyaCliente)
                .join(qCampanyaCliente.cliente, qCliente)
                .join(qCampanyaFormulario.campanyaFormularioEstadosOrigen, qCampanyaFormularioEstadoOrigen)
                .where(qCampanyaFormulario.id.eq(formularioId)
                        .and(qCliente.id.eq(clienteId))
                        .and((qCampanyaFormularioEstadoOrigen.estadoOrigen.eq(qCampanyaCliente.campanyaClienteTipo)).or(qCampanyaFormularioEstadoOrigen.estadoOrigen.isNull()))
                        .and(qCampanyaFormulario.fechaFin.after(hoy).or(qCampanyaFormulario.fechaFin.isNull()))
                        .and(qCampanyaFormulario.fechaInicio.before(hoy).or(qCampanyaFormulario.fechaInicio.isNull())));

        List<CampanyaFormulario> lista = query.list(qCampanyaFormulario);
        if (lista.isEmpty()) return null;
        return lista.get(0);
    }

    public CampanyaFormulario getFormularioByFormularioIdSinUsuario(Long formularioId) {
        QCampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen = QCampanyaFormularioEstadoOrigen.campanyaFormularioEstadoOrigen;

        JPAQuery query = new JPAQuery(entityManager);

        Date hoy = new Date();

        QCampanyaFormulario campanyaFormulario = QCampanyaFormulario.campanyaFormulario;
        query.from(campanyaFormulario).join(campanyaFormulario.campanyaFormularioEstadosOrigen, campanyaFormularioEstadoOrigen)
                .where(campanyaFormulario.id.eq(formularioId)
                        .and(campanyaFormulario.fechaFin.after(hoy).or(campanyaFormulario.fechaFin.isNull()))
                        .and(campanyaFormulario.fechaInicio.before(hoy).or(campanyaFormulario.fechaInicio.isNull())));

        List<CampanyaFormulario> lista = query.list(campanyaFormulario);
        if (lista.isEmpty()) return null;
        return lista.get(0);
    }

    public CampanyaFormulario getFormularioByCampanyaIdAndCliente(Long campanyaId, Long clienteId) {
        JPAQuery queryCaso1 = new JPAQuery(entityManager);
        JPAQuery queryCaso2 = new JPAQuery(entityManager);

        QCampanyaFormulario campanyaFormulario = QCampanyaFormulario.campanyaFormulario;
        QCampanya campanya = QCampanya.campanya1;
        QCampanyaCliente campanyaCliente = QCampanyaCliente.campanyaCliente;
        QCliente cliente = QCliente.cliente;
        QCampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen = QCampanyaFormularioEstadoOrigen.campanyaFormularioEstadoOrigen;

        JPAQuery queryEstado = new JPAQuery(entityManager);
        Date hoy = new Date();

        if (ParamUtils.isNotNull(clienteId)) {

            queryEstado.from(campanyaCliente)
                    .join(campanyaCliente.cliente, cliente)
                    .where(cliente.id.eq(clienteId)
                            .and(campanyaCliente.campanya.id.eq(campanyaId)));

            if (queryEstado.list(campanyaCliente).size() > 0) {
                Long estadoCliente = queryEstado.list(campanyaCliente).get(0).getCampanyaClienteTipo().getId();

                if (ParamUtils.isNotNull(estadoCliente)) {
                    queryCaso1.from(campanyaFormulario)
                            .join(campanyaFormulario.campanya, campanya).fetch()
                            .leftJoin(campanyaFormulario.campanyaFormularioEstadosOrigen, campanyaFormularioEstadoOrigen)
                            .where(campanyaFormulario.campanya.id.eq(campanyaId)
                                    .and((campanyaFormularioEstadoOrigen.estadoOrigen.id.eq(estadoCliente).or(campanyaFormularioEstadoOrigen.isNull())))
                                    .and(campanyaFormulario.fechaFin.after(hoy).or(campanyaFormulario.fechaFin.isNull()))
                                    .and(campanyaFormulario.fechaInicio.before(hoy).or(campanyaFormulario.fechaInicio.isNull())));
                    List<CampanyaFormulario> lista = queryCaso1.list(campanyaFormulario);
                    if (lista.size() > 0) {
                        return lista.get(0);
                    }
                }
            } else return getFormularioGenericoByCampanya(campanyaId);
        }

        return getFormularioGenericoByCampanya(campanyaId);
    }

}
