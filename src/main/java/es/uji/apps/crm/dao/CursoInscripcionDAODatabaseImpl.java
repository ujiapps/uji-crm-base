package es.uji.apps.crm.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.CursoInscripcion;
import es.uji.apps.crm.model.QCursoInscripcion;
import es.uji.apps.crm.model.QProgramaPersonaCurso;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CursoInscripcionDAODatabaseImpl extends BaseDAODatabaseImpl implements
        CursoInscripcionDAO {

    @Override
    public List<CursoInscripcion> getCursosInscripcionesByPersonaId(Long connectedUserId,
                                                                    Long personaId) {
        QProgramaPersonaCurso qProgramaPersonaCurso = QProgramaPersonaCurso.programaPersonaCurso;
        QCursoInscripcion qCursoInscripcion = QCursoInscripcion.cursoInscripcion;

        JPAQuery subqueryUestId = new JPAQuery(entityManager);
        subqueryUestId.from(qProgramaPersonaCurso).where(
                qProgramaPersonaCurso.persona.eq(connectedUserId));

        List<Long> listUEstId = subqueryUestId.list(qProgramaPersonaCurso.uest.id);

        if (listUEstId.size() > 0)
        {
            JPAQuery query = new JPAQuery(entityManager);
            query.from(qCursoInscripcion).where(
                    qCursoInscripcion.persona.id.eq(personaId).and(
                            qCursoInscripcion.tipo.id.in(listUEstId).or(
                                    qCursoInscripcion.tipoComparte.in(listUEstId))));

            return query.list(qCursoInscripcion);
        }
        else
        {
            return new ArrayList<>();
        }
    }
}