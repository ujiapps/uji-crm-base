package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QSeguimientoFichero;
import es.uji.apps.crm.model.SeguimientoFichero;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class SeguimientoFicheroDAODatabaseImpl extends BaseDAODatabaseImpl implements
        SeguimientoFicheroDAO {

    @Override
    public List<SeguimientoFichero> getSeguimientoFicherosByCampanyaSeguimientoId(
            Long campanyaSeguimientoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QSeguimientoFichero seguimientoFichero = QSeguimientoFichero.seguimientoFichero;

        query.from(seguimientoFichero)
                .where(seguimientoFichero.campanyaSeguimiento.id.eq(campanyaSeguimientoId));

        return query.list(seguimientoFichero);
    }

    @Override
    public SeguimientoFichero getSeguimientoFicheroById(Long seguimientoFicheroId) {
        JPAQuery query = new JPAQuery(entityManager);
        QSeguimientoFichero seguimientoFichero = QSeguimientoFichero.seguimientoFichero;

        query.from(seguimientoFichero)
                .where(seguimientoFichero.id.eq(seguimientoFicheroId));

        List<SeguimientoFichero> listaSeguimientoFicheros = query.list(seguimientoFichero);
        if (listaSeguimientoFicheros.size() > 0)
        {
            return listaSeguimientoFicheros.get(0);
        }
        return null;
    }

}