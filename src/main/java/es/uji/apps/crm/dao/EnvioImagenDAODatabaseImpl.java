package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.EnvioImagen;
import es.uji.apps.crm.model.QEnvioImagen;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EnvioImagenDAODatabaseImpl extends BaseDAODatabaseImpl implements EnvioImagenDAO {

    @Override
    public List<EnvioImagen> getEnvioImagenes() {
        JPAQuery query = new JPAQuery(entityManager);

        QEnvioImagen qEnvioImagen = QEnvioImagen.envioImagen;

        query.from(qEnvioImagen);
        return query.list(qEnvioImagen);
    }

    @Override
    public List<EnvioImagen> getEnvioImagenesByEnvioId(Long envioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QEnvioImagen qEnvioImagen = QEnvioImagen.envioImagen;

        query.from(qEnvioImagen).where(qEnvioImagen.envio.id.eq(envioId));
        return query.list(qEnvioImagen);
    }

    @Override
    public EnvioImagen getEnvioImagenById(Long envioImagenId) {
        JPAQuery query = new JPAQuery(entityManager);

        QEnvioImagen qEnvioImagen = QEnvioImagen.envioImagen;

        query.from(qEnvioImagen).where(qEnvioImagen.id.eq(envioImagenId));
        List<EnvioImagen> envioImagenes = query.list(qEnvioImagen);
        if (envioImagenes.isEmpty())
        {
            return null;
        }
        else
        {
            return envioImagenes.get(0);
        }
    }

    @Override
    public List<EnvioImagen> getEnvioImagenesByTarifaEnvioId(Long tarifaEnvioId) {
        JPAQuery query = new JPAQuery(entityManager);

        QEnvioImagen qEnvioImagen = QEnvioImagen.envioImagen;

        query.from(qEnvioImagen).where(qEnvioImagen.tarifaEnvio.id.eq(tarifaEnvioId));
        return query.list(qEnvioImagen);
    }

    @Override
    public List<EnvioImagen> getEnvioImagenesGenerico(String search) {
        JPAQuery query = new JPAQuery(entityManager);

        QEnvioImagen qEnvioImagen = QEnvioImagen.envioImagen;

        query.from(qEnvioImagen).where(qEnvioImagen.envio.isNull().and(qEnvioImagen.tarifaEnvio.isNull().and(qEnvioImagen.carta.isNull())));
        if (!search.equals("")) {
            query.where(qEnvioImagen.nombre.containsIgnoreCase(search));
        }
        return query.list(qEnvioImagen);
    }
}