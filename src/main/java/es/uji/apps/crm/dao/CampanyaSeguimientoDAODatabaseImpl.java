package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.CampanyaSeguimiento;
import es.uji.apps.crm.model.QCampanyaFase;
import es.uji.apps.crm.model.QCampanyaSeguimiento;
import es.uji.apps.crm.model.QFase;
import es.uji.apps.crm.model.QSeguimientoTipo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CampanyaSeguimientoDAODatabaseImpl extends BaseDAODatabaseImpl implements
        CampanyaSeguimientoDAO {

    @Override
    public List<CampanyaSeguimiento> getCampanyaSeguimientosByCampanyaFaseId(Long campanyaFaseId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaSeguimiento qCampanyaSeguimiento = QCampanyaSeguimiento.campanyaSeguimiento;
        QSeguimientoTipo qSeguimientoTipo = QSeguimientoTipo.seguimientoTipo;
        QCampanyaFase qCampanyaFase = QCampanyaFase.campanyaFase;
        QFase qFase = QFase.fase;

        query.from(qCampanyaSeguimiento)
                .join(qCampanyaSeguimiento.seguimientoTipo, qSeguimientoTipo).fetch()
                .join(qCampanyaSeguimiento.campanyaFase, qCampanyaFase).fetch()
                .join(qCampanyaFase.fase, qFase).fetch()
                .where(qCampanyaFase.id.eq(campanyaFaseId));

        return query.list(qCampanyaSeguimiento);
    }

    @Override
    public CampanyaSeguimiento getCampanyaSeguimientoById(Long campanyaSeguimientoId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaSeguimiento campanyaSeguimiento = QCampanyaSeguimiento.campanyaSeguimiento;

        query.from(campanyaSeguimiento)
                .where(campanyaSeguimiento.id.eq(campanyaSeguimientoId));

        List<CampanyaSeguimiento> lista = query.list(campanyaSeguimiento);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

}