package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.CampanyaEnvioAdmin;
import es.uji.apps.crm.model.QCampanyaEnvioAdmin;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CampanyaEnvioAdminDAODatabaseImpl extends BaseDAODatabaseImpl implements
        CampanyaEnvioAdminDAO {

    QCampanyaEnvioAdmin campanyaEnvioAdmin = QCampanyaEnvioAdmin.campanyaEnvioAdmin;

    @Override
    public List<CampanyaEnvioAdmin> getCampanyaEnviosAdminByCampanyaId(Long campanyaId) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(campanyaEnvioAdmin)
                .where(campanyaEnvioAdmin.campanya.id.eq(campanyaId));

        return query.list(campanyaEnvioAdmin);
    }
}