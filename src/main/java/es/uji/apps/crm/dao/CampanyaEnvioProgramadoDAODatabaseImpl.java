package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.CampanyaEnvioAuto;
import es.uji.apps.crm.model.CampanyaEnvioProgramado;
import es.uji.apps.crm.model.QCampanyaEnvioProgramado;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CampanyaEnvioProgramadoDAODatabaseImpl extends BaseDAODatabaseImpl implements CampanyaEnvioProgramadoDAO {
    @Override
    public List<CampanyaEnvioProgramado> getEnvioProgramadoByEnvio(CampanyaEnvioAuto envio) {
        JPAQuery query = new JPAQuery(entityManager);
        QCampanyaEnvioProgramado qCampanyaEnvioProgramado = QCampanyaEnvioProgramado.campanyaEnvioProgramado;

        query.from(qCampanyaEnvioProgramado)
                .where(qCampanyaEnvioProgramado.envio.eq(envio));

        return query.list(qCampanyaEnvioProgramado);
    }
}