package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QTipoValidacion;
import es.uji.apps.crm.model.TipoValidacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoValidacionDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoValidacionDAO {

    @Override
    public List<TipoValidacion> getTiposValidacion() {
        JPAQuery query = new JPAQuery(entityManager);

        QTipoValidacion tipoValidacion = QTipoValidacion.tipoValidacion;

        query.from(tipoValidacion).orderBy(tipoValidacion.nombre.asc());

        return query.list(tipoValidacion);
    }

}