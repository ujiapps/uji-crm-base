package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.FicheroDevolucion;
import es.uji.commons.db.BaseDAO;

public interface FicheroDevolucionDAO extends BaseDAO {
    List<FicheroDevolucion> getFicherosDevolucion();
}