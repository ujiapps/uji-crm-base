package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Etiqueta;
import es.uji.apps.crm.model.QEtiqueta;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EtiquetaDAODatabaseImpl extends BaseDAODatabaseImpl implements EtiquetaDAO {
    @Autowired
    private UtilsDAO utilsDAO;

    @Override
    public List<Etiqueta> getEtiquetas() {
        JPAQuery query = new JPAQuery(entityManager);
        QEtiqueta etiqueta = QEtiqueta.etiqueta1;

        query.from(etiqueta);

        return query.list(etiqueta);
    }

    @Override
    public List<Etiqueta> getEtiquetasByNombre(String busqueda) {
        JPAQuery query = new JPAQuery(entityManager);
        QEtiqueta etiqueta = QEtiqueta.etiqueta1;

        query.from(etiqueta)
                .where(utilsDAO.limpiaAcentos(etiqueta.nombre).containsIgnoreCase(utilsDAO.limpiaAcentos(busqueda)));

        return query.list(etiqueta);
    }

    @Override
    public Etiqueta getEtiquetaById(Long etiquetaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QEtiqueta etiqueta = QEtiqueta.etiqueta1;

        query.from(etiqueta).where(etiqueta.id.eq(etiquetaId));

        List<Etiqueta> listaEtiquetas = query.list(etiqueta);
        if (listaEtiquetas.size() > 0)
        {
            return listaEtiquetas.get(0);
        }
        return null;
    }

    @Override
    public Etiqueta getEtiquetaByNombre(String etiquetaNombre) {
        JPAQuery query = new JPAQuery(entityManager);
        QEtiqueta etiqueta = QEtiqueta.etiqueta1;

        query.from(etiqueta).where(etiqueta.nombre.eq(etiquetaNombre));

        List<Etiqueta> listaEtiquetas = query.list(etiqueta);
        if (listaEtiquetas.size() > 0)
        {
            return listaEtiquetas.get(0);
        }
        return null;
    }

}