package es.uji.apps.crm.dao;

import es.uji.apps.crm.model.PersonaFoto;
import es.uji.commons.db.BaseDAO;

public interface PersonaFotoDAO extends BaseDAO {

    PersonaFoto getPersonaFotoByClienteId(Long clienteId);
}