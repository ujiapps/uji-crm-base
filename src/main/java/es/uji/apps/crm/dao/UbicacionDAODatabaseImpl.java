package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QUbicacion;
import es.uji.apps.crm.model.Ubicacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class UbicacionDAODatabaseImpl extends BaseDAODatabaseImpl implements UbicacionDAO {
    @Override
    public List<Ubicacion> getUbicaciones() {

        JPAQuery query = new JPAQuery(entityManager);

        QUbicacion qUbicacion = QUbicacion.ubicacion;

        query.from(qUbicacion);
        return query.list(qUbicacion);
    }

    @Override
    public Ubicacion getUbicacion(Long uestId) {
        JPAQuery query = new JPAQuery(entityManager);

        QUbicacion qUbicacion = QUbicacion.ubicacion;

        query.from(qUbicacion).where(qUbicacion.id.eq(uestId));
        List<Ubicacion> result = query.list(qUbicacion);

        if (result.size() > 0)
        {
            return result.get(0);
        }
        else
        {
            return null;
        }

    }
}