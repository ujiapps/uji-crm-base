package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.ui.OpcionUI;
import es.uji.commons.db.BaseDAO;

public interface ItemDAO extends BaseDAO {
    List<Item> getItems(Long connectedUserId);

    List<Item> getItemsVisibles(Long connectedUserId);

    List<Item> getItemsByGrupoId(Long connectedUserId, Long grupoId);

    List<Item> getItemsVisiblesByGrupoId(Long connectedUserId, Long grupoId);

    List<Item> getItemsByGrupoId(Long grupoId);

    Item getItemById(Long ItemId);

    List<Item> getItemsAsociadosByCampanyaId(Long campanyaId);

    List<Item> getItemsByItemIdYCampanyaId(Long item_id, Long campanya_id);

    List<Item> getItemsByEnvioId(Long envioId);

    List<Item> getItemsByItemIdYEnvioId(Long item_id, Long envioId);

    List<Item> getItemsByClienteIdFaltan(Long connectedUserId, Long clienteId, Long grupoId);

    List<Item> getItemsByClienteId(Long clienteId);

    List<Item> getItemsByGrupoIdAndClienteId(Long grupoId, Long clienteId);

    List<Item> getItemsByDatoCampanya(Long datoCampanya);

    List<Item> getItemsByGrupId(Long grupoId);

    List<Item> getItemsVisiblesByProgramaId(Long programaId);

    List<Item> getItemsVisiblesByGrupId(Long grupoId);

    List<OpcionUI> getItemsByGrupoOrderByIdioma(Long grupoId, String idioma);
}