package es.uji.apps.crm.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Emisora;
import es.uji.apps.crm.model.QTipoRecibo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoReciboDAO extends BaseDAODatabaseImpl {
    public Long getTipoReciboByEmisoraAndTipo(Emisora emisora, String tipoReciboString) {

        QTipoRecibo tipoRecibo = QTipoRecibo.tipoRecibo1;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(tipoRecibo).where(tipoRecibo.emisora.eq(emisora.getId()).and(tipoRecibo.tipoRecibo.eq(tipoReciboString)));

        return query.uniqueResult(tipoRecibo.id);
    }

    public Long getTipoReciboByEmisora(Emisora emisora) {
        QTipoRecibo tipoRecibo = QTipoRecibo.tipoRecibo1;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(tipoRecibo).where(tipoRecibo.emisora.eq(emisora.getId()));

        return query.uniqueResult(tipoRecibo.id);
    }
}