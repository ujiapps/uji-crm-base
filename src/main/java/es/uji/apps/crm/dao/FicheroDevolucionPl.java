package es.uji.apps.crm.dao;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

@Component
public class FicheroDevolucionPl {
    private static DataSource dataSource;
    private SubirFicheroDevolucion ficheroDevolucion;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        FicheroDevolucionPl.dataSource = dataSource;
    }

    public void init() {
        this.ficheroDevolucion = new SubirFicheroDevolucion(dataSource);
    }

    public void subirFicheroDevolucion(String nombreFichero, String fichero) {
        ficheroDevolucion.subirFicheroDevolucion(nombreFichero, fichero);
    }

    private class SubirFicheroDevolucion extends StoredProcedure {
        private static final String SQL = "ficherosdevoluciones.subirFicheroDevoluciones";
        private static final String PNOMBREFICHERO = "p_nombre_fichero";
        private static final String PFICHERO = "p_fichero";

        public SubirFicheroDevolucion(DataSource dataSource) {
            setDataSource(dataSource);
            setFunction(false);
            setSql(SQL);
            declareParameter(new SqlParameter(PNOMBREFICHERO, Types.VARCHAR));
            declareParameter(new SqlParameter(PFICHERO, Types.CLOB));

            compile();
        }

        public void subirFicheroDevolucion(String nombreFichero, String fichero) {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(PNOMBREFICHERO, nombreFichero);
            inParams.put(PFICHERO, new SqlLobValue(fichero));

            Map<String, Object> results = execute(inParams);
        }
    }

}