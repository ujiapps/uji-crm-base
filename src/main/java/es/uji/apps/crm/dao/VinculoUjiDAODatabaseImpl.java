package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QVwVinculacionPersona;
import es.uji.apps.crm.model.VwVinculacionPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class VinculoUjiDAODatabaseImpl extends BaseDAODatabaseImpl implements VinculoUjiDAO {

    @Override
    public List<VwVinculacionPersona> getSubVinculosByPersonaId(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QVwVinculacionPersona vinculoUji = QVwVinculacionPersona.vwVinculacionPersona;
        query.from(vinculoUji).where(vinculoUji.persona.id.eq(personaId));

        return query.list(vinculoUji);
    }
}