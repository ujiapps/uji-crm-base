package es.uji.apps.crm.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.crm.ui.OpcionUI;
import es.uji.apps.crm.ui.QOpcionUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Provincia;
import es.uji.apps.crm.model.QProvincia;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ProvinciaDAO extends BaseDAODatabaseImpl {
    @Autowired
    private UtilsDAO utilsDAO;

    public List<Provincia> getProvincias(String idioma) {
        JPAQuery query = new JPAQuery(entityManager);
        QProvincia provincia = QProvincia.provincia;

        query.from(provincia).orderBy(provincia.orden.asc());

        if (idioma.equalsIgnoreCase("en") || idioma.equalsIgnoreCase("uk")) {
            query.orderBy(utilsDAO.limpiaAcentos(provincia.nombreEN).asc());
        } else {
            if (idioma.equalsIgnoreCase("es")) {
                query.orderBy(utilsDAO.limpiaAcentos(provincia.nombreES).asc());
            } else {
                query.orderBy(utilsDAO.limpiaAcentos(provincia.nombreCA).asc());
            }
        }
        return query.list(provincia);
    }

    public List<OpcionUI> getOpcionProvincias(String idioma) {
        JPAQuery query = new JPAQuery(entityManager);
        QProvincia provincia = QProvincia.provincia;

        query.from(provincia)
                .orderBy(provincia.orden.asc());

        List<OpcionUI> lista;

        if (idioma.equalsIgnoreCase("en") || idioma.equalsIgnoreCase("uk")) {
            query.orderBy(provincia.nombreENLimpio.asc());
            lista = query.list(new QOpcionUI(provincia.id.stringValue(), provincia.nombreEN));
        } else {
            if (idioma.equalsIgnoreCase("es")) {
                query.orderBy(provincia.nombreESLimpio.asc());
                lista = query.list(new QOpcionUI(provincia.id.stringValue(), provincia.nombreES));
            } else {
                query.orderBy(provincia.nombreCALimpio.asc());
                lista = query.list(new QOpcionUI(provincia.id.stringValue(), provincia.nombreCA));
            }
        }
        for (OpcionUI opcionUI: lista){
            if (opcionUI.getValor().equals("12")){
                List<Long> mostrarCastellon = new ArrayList<>();
                List<Long> ocultarCastellon = new ArrayList<>();
                mostrarCastellon.add(12L);
                ocultarCastellon.add(46L);
                opcionUI.setMostrar(mostrarCastellon);
                opcionUI.setOcultar(ocultarCastellon);
            }
            else {
                if (opcionUI.getValor().equals("46")) {
                    List<Long> mostrarValencia = new ArrayList<>();
                    List<Long> ocultarValencia = new ArrayList<>();
                    mostrarValencia.add(46L);
                    ocultarValencia.add(12L);
                    opcionUI.setMostrar(mostrarValencia);
                    opcionUI.setOcultar(ocultarValencia);
                }
                else{
                    List<Long> mostrar = new ArrayList<>();
                    List<Long> ocultar = new ArrayList<>();
                    ocultar.add(46L);
                    ocultar.add(12L);

                    opcionUI.setMostrar(mostrar);
                    opcionUI.setOcultar(ocultar);
                }
            }
        }
        return lista;
    }
}