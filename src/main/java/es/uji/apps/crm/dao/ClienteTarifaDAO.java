package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteTarifa;
import es.uji.apps.crm.model.TipoTarifa;
import es.uji.commons.db.BaseDAO;

public interface ClienteTarifaDAO extends BaseDAO {
    List<ClienteTarifa> getClienteTarifaByClienteIdAndCampanyaId(Long cliente, Long campanya);

    ClienteTarifa dameFechaMaxCaducaClienteTarifa(Cliente cliente);

    ClienteTarifa getClienteTarifaById(Long clienteTarifaId);

    List<ClienteTarifa> getClienteTarifasByClienteAndTipoTarifa(Cliente cliente, TipoTarifa tipoTarifa);
}