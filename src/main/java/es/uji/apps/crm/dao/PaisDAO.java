package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.ui.OpcionUI;
import es.uji.apps.crm.ui.QOpcionUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Pais;
import es.uji.apps.crm.model.QPais;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PaisDAO extends BaseDAODatabaseImpl {
    @Autowired
    private UtilsDAO utilsDAO;

    public List<Pais> getPaises() {
        JPAQuery query = new JPAQuery(entityManager);

        QPais qPais = QPais.pais;

        query.from(qPais);

        return query.list(qPais);
    }

    public List<Pais> getPaisesOrdenadosPorIdioma(String idioma) {
        JPAQuery query = new JPAQuery(entityManager);

        QPais qPais = QPais.pais;

        query.from(qPais).orderBy(qPais.orden.asc());

        if (idioma.equalsIgnoreCase("en") || idioma.equalsIgnoreCase("uk"))
        {
            query.orderBy(utilsDAO.limpiaAcentos(qPais.nombreEN).asc());
        }
        else
        {
            if (idioma.equalsIgnoreCase("es"))
            {
                query.orderBy(utilsDAO.limpiaAcentos(qPais.nombreES).asc());
            }
            else
            {
                query.orderBy(utilsDAO.limpiaAcentos(qPais.nombreCA).asc());
            }
        }
        return query.list(qPais);

    }

    public List<OpcionUI> getOpcionPaisesOrdenadosPorIdioma(String idioma) {
        JPAQuery query = new JPAQuery(entityManager);

        QPais qPais = QPais.pais;

        query.from(qPais).orderBy(qPais.orden.asc());

        if (idioma.equalsIgnoreCase("en") || idioma.equalsIgnoreCase("uk"))
        {
            query.orderBy(qPais.nombreENLimpio.asc());
            return query.list(new QOpcionUI(qPais.id, qPais.nombreEN));
        }
        else
        {
            if (idioma.equalsIgnoreCase("es"))
            {
                query.orderBy(qPais.nombreESLimpio.asc());
                return query.list(new QOpcionUI(qPais.id, qPais.nombreES));
            }
            else
            {
                query.orderBy(qPais.nombreCALimpio.asc());
                return query.list(new QOpcionUI(qPais.id, qPais.nombreCA));
            }
        }
    }
}