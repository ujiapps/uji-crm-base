package es.uji.apps.crm.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.EnvioClienteVista;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.model.QEnvioClienteVista;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EnvioClienteVistaDAO extends BaseDAODatabaseImpl {

    private QEnvioClienteVista qEnvioCliente = QEnvioClienteVista.envioClienteVista;

    private Map<String, Path> relations = new HashMap<String, Path>();

    public EnvioClienteVistaDAO() {
        relations.put("id", qEnvioCliente.id);
        relations.put("nombre", qEnvioCliente.nombre);
        relations.put("correo", qEnvioCliente.correo);
        relations.put("estadoCorreoId", qEnvioCliente.estadoCorreo.id);
        relations.put("recibeCorreo", qEnvioCliente.recibeCorreo);
    }

    public List<EnvioClienteVista> getClientesByEnvioId(Long envioId, Paginacion paginacion) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEnvioCliente)
                .where(qEnvioCliente.envio.id.eq(envioId));

        if (paginacion != null) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }

            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return query.list(qEnvioCliente);
    }

    public List<EnvioClienteVista> getClientesByEnvioId(Long envioId) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEnvioCliente)
                .where(qEnvioCliente.envio.id.eq(envioId));

        return query.list(qEnvioCliente);
    }
}