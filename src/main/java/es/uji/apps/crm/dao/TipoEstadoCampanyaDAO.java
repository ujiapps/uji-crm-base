package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.commons.db.BaseDAO;

public interface TipoEstadoCampanyaDAO extends BaseDAO {

    List<TipoEstadoCampanya> getTiposEstadoCampanyaByCampanyaId(Long campanya);

    TipoEstadoCampanya getTipoEstadoCampanyaById(Long tipoEstadoCampanyaId);

    TipoEstadoCampanya getTipoEstadoCampanyaByNombre(Long tipoEstadoCampanyaId, String nombreEstado);

    TipoEstadoCampanya getTipoEstadoCampanyaByAccion(Long campanyaId, String accion);

}