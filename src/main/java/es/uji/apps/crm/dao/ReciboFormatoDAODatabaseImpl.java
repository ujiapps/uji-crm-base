package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QReciboFormato;
import es.uji.apps.crm.model.ReciboFormato;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ReciboFormatoDAODatabaseImpl extends BaseDAODatabaseImpl implements ReciboFormatoDAO {

    @Override
    public List<ReciboFormato> getReciboFormatos() {
        JPAQuery query = new JPAQuery(entityManager);

        QReciboFormato reciboFormato = QReciboFormato.reciboFormato;
        return query.from(reciboFormato).list(reciboFormato);
    }
}