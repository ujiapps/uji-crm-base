package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.CampanyaTexto;
import es.uji.apps.crm.model.QCampanyaTexto;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CampanyaTextoDAODatabaseImpl extends BaseDAODatabaseImpl implements CampanyaTextoDAO {

    @Override
    public List<CampanyaTexto> getCampanyaTextos() {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaTexto qCampanyaTexto = QCampanyaTexto.campanyaTexto;

        query.from(qCampanyaTexto)
                .orderBy(qCampanyaTexto.nombre.asc());

        return query.list(qCampanyaTexto);
    }

    @Override
    public CampanyaTexto getCampanyaTextoByCampanyaCodigo(String codigo) {
        JPAQuery query = new JPAQuery(entityManager);

        QCampanyaTexto qCampanyaTexto = QCampanyaTexto.campanyaTexto;

        query.from(qCampanyaTexto).where(qCampanyaTexto.codigo.eq(codigo));

        List<CampanyaTexto> lista = query.list(qCampanyaTexto);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }
}