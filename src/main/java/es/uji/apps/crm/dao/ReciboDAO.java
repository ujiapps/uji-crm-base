package es.uji.apps.crm.dao;

import java.text.ParseException;
import java.util.List;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.FiltroRecibo;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.model.Persona;
import es.uji.apps.crm.model.Recibo;
import es.uji.commons.db.BaseDAO;

public interface ReciboDAO extends BaseDAO {

    List<Recibo> getRecibosByPersonaId(Long personaId);

    List<Recibo> getRecibos(FiltroRecibo filtro, Paginacion paginacion) throws ParseException;

    List<Recibo> getRecibosConMovimientos(FiltroRecibo filtro, Paginacion paginacion) throws ParseException;

    List<Recibo> getRecibosConMovimientosParaCsv(FiltroRecibo filtro, Paginacion paginacion) throws ParseException;

    Recibo getReciboImpagadoByPersonaAndCampanya(Persona persona, Campanya campanya);

    Recibo getReciboByCuota(Long clienteCuotaId);
}