package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.Emisora;
import es.uji.apps.crm.model.QEmisora;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EmisoraDAODatabaseImpl extends BaseDAODatabaseImpl implements EmisoraDAO {

    @Override
    public List<Emisora> getEmisoras() {

        JPAQuery query = new JPAQuery(entityManager);
        QEmisora emisora = QEmisora.emisora;

        return query.from(emisora).list(emisora);
    }
}