package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.*;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class VwEntradaCrmDAO extends BaseDAODatabaseImpl {


    public List<VwEntradaCrm> obtenerInfoEntradasCrm(Long acto) {

        QVwEntradaCrm entradaCrm = QVwEntradaCrm.vwEntradaCrm;
        QCliente qCliente = QCliente.cliente;
        QClienteGeneral qClienteGeneral = QClienteGeneral.clienteGeneral;
        QPersona qPersona = QPersona.persona;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(entradaCrm)
                .join(entradaCrm.cliente, qCliente).fetch()
                .join(qCliente.clienteGeneral, qClienteGeneral).fetch()
                .join(qClienteGeneral.persona, qPersona).fetch()
                .where(entradaCrm.acto.id.eq(acto));

        return query.list(entradaCrm);
    }

    public VwEntradaCrm getEntradasSolicitadas(Cliente cliente, CampanyaActo acto) {

        QVwEntradaCrm entradaCrm = QVwEntradaCrm.vwEntradaCrm;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(entradaCrm).where(entradaCrm.acto.id.eq(acto.getId()).and(entradaCrm.cliente.id.eq(cliente.getId())));

        List<VwEntradaCrm> lista = query.list(entradaCrm);
        if (lista.isEmpty()) {
            return null;
        } else {
            return lista.get(0);
        }
    }
}