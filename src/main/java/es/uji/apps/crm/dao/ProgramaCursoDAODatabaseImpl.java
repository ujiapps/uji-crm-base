package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ProgramaCurso;
import es.uji.apps.crm.model.QProgramaCurso;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ProgramaCursoDAODatabaseImpl extends BaseDAODatabaseImpl implements ProgramaCursoDAO {
    @Override
    public List<ProgramaCurso> getProgramaCursosByProgramaId(Long programaId) {

        JPAQuery query = new JPAQuery(entityManager);
        QProgramaCurso qProgramaCurso = QProgramaCurso.programaCurso;

        query.from(qProgramaCurso).where(qProgramaCurso.programa.id.eq(programaId));
        return query.list(qProgramaCurso);
    }
}