package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.EnvioItem;
import es.uji.commons.db.BaseDAO;

public interface EnvioItemDAO extends BaseDAO {
    EnvioItem getEnvioItemById(Long envioItemId);

    List<EnvioItem> getEnvioItemByEnvioId(Long envioId);

    List<EnvioItem> getEnvioItemByItemIdAndEnvioId(Long itemId, Long envioId);
}