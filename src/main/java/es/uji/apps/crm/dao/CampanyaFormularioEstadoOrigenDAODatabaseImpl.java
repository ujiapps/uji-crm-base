package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.CampanyaFormulario;
import es.uji.apps.crm.model.CampanyaFormularioEstadoOrigen;
import es.uji.apps.crm.model.QCampanyaFormularioEstadoOrigen;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CampanyaFormularioEstadoOrigenDAODatabaseImpl extends BaseDAODatabaseImpl implements CampanyaFormularioEstadoOrigenDAO {

    @Override
    public List<CampanyaFormularioEstadoOrigen> getCampanyaFormularioEstadoOrigenByFormulario(Long formularioId) {

        QCampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen = QCampanyaFormularioEstadoOrigen.campanyaFormularioEstadoOrigen;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(campanyaFormularioEstadoOrigen).where(campanyaFormularioEstadoOrigen.campanyaFormulario.id.eq(formularioId));

        return query.list(campanyaFormularioEstadoOrigen);
    }

    @Transactional
    @Override
    public void deleteCampanyaFormularioEstadoOrigenByCampanyaFormularioId(CampanyaFormulario campanyaFormulario) {
        QCampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen = QCampanyaFormularioEstadoOrigen.campanyaFormularioEstadoOrigen;

        JPADeleteClause query = new JPADeleteClause(entityManager, campanyaFormularioEstadoOrigen);
        query.where(campanyaFormularioEstadoOrigen.campanyaFormulario.eq(campanyaFormulario)).execute();

    }
}