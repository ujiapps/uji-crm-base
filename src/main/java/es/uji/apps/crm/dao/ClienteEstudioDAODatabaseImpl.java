package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ClienteEstudio;
import es.uji.apps.crm.model.QClienteEstudio;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ClienteEstudioDAODatabaseImpl extends BaseDAODatabaseImpl implements ClienteEstudioDAO {

    @Override
    public List<ClienteEstudio> getClienteEstudiosByPersona(Long personaId) {

        JPAQuery query = new JPAQuery(entityManager);
        QClienteEstudio qClienteEstudio = QClienteEstudio.clienteEstudio;

        query.from(qClienteEstudio).where(qClienteEstudio.persona.eq(personaId));

        return query.list(qClienteEstudio);
    }
}