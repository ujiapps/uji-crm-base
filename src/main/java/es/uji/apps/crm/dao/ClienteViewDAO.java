package es.uji.apps.crm.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.ClienteView;
import es.uji.apps.crm.model.QClienteView;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ClienteViewDAO extends BaseDAODatabaseImpl {
    public ClienteView getClienteViewById(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);
        QClienteView clienteView = QClienteView.clienteView;

        query.from(clienteView).where(clienteView.id.eq(clienteId));

        return query.list(clienteView).get(0);
    }
}