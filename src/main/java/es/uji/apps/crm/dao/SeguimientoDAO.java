package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Seguimiento;
import es.uji.commons.db.BaseDAO;

public interface SeguimientoDAO extends BaseDAO {

    List<Seguimiento> getSeguimientosByClienteId(Long clienteId);

    Seguimiento getSeguimientoById(Long seguimientoId);

    List<Seguimiento> getSeguimientosByClienteIdAndCampanyaId(Long clienteId, Long campanyaId);

    List<Seguimiento> getSeguimientosByCampanyaId(Long campanyaId);

}