package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Etiqueta;
import es.uji.commons.db.BaseDAO;

public interface EtiquetaDAO extends BaseDAO {
    List<Etiqueta> getEtiquetas();

    List<Etiqueta> getEtiquetasByNombre(String busqueda);

    Etiqueta getEtiquetaById(Long etiquetaId);

    Etiqueta getEtiquetaByNombre(String etiquetaNombre);
}