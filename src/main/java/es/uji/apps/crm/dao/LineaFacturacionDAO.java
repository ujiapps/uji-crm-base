package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.LineaFacturacion;
import es.uji.apps.crm.model.Servicio;
import es.uji.commons.db.BaseDAO;

public interface LineaFacturacionDAO extends BaseDAO {
    List<LineaFacturacion> getLineasFacturacionByServicio(Long servicio);

    List<LineaFacturacion> getLineasFacturacionByCampanya(Long campanyaId);

    LineaFacturacion getLineaFacturacionActoByServicio(Servicio servicio);


//    LineaFacturacion getLineasFacturacionActoByCampanya(Campanya campanya);
}