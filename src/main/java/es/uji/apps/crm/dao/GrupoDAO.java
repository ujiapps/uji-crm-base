package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.Grupo;
import es.uji.commons.db.BaseDAO;

public interface GrupoDAO extends BaseDAO {
    List<Grupo> getGrupos(Long connectedUserId);

    List<Grupo> getGruposByProgramaId(Long connectedUserId, Long programaId);

    List<Grupo> getGruposAsociadosByCampanyaId(Long campanyaId);

    Grupo getGrupoById(Long grupoId);

    List<Grupo> getGruposByCampanyaId(Long grupo_id, Long campanya_id);

    List<Grupo> getGruposByEnvioId(Long grupo_id, Long envioId);

    List<Grupo> getGruposFaltanByClienteId(Long connectedUserId, Long clienteId);

//    List<Grupo> getGruposTodosByUsuarioConectado(Long connectedUserId);
}