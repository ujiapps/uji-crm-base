package es.uji.apps.crm.dao;

import java.util.List;

import es.uji.apps.crm.model.VwVinculacionPersona;
import es.uji.commons.db.BaseDAO;

public interface VinculoUjiDAO extends BaseDAO {
    List<VwVinculacionPersona> getSubVinculosByPersonaId(Long personaId);
}