package es.uji.apps.crm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.crm.model.QCampanya;
import es.uji.apps.crm.model.QCliente;
import es.uji.apps.crm.model.QSeguimiento;
import es.uji.apps.crm.model.QSeguimientoTipo;
import es.uji.apps.crm.model.Seguimiento;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class SeguimientoDAODatabaseImpl extends BaseDAODatabaseImpl implements SeguimientoDAO {

    @Override
    public List<Seguimiento> getSeguimientosByClienteId(Long clienteId) {
        JPAQuery query = new JPAQuery(entityManager);

        QSeguimiento qSeguimiento = QSeguimiento.seguimiento;
        QSeguimientoTipo qSeguimientoTipo = QSeguimientoTipo.seguimientoTipo;
        QCampanya qCampanya = QCampanya.campanya1;
        QCliente qCliente = QCliente.cliente;

        query.from(qSeguimiento).join(qSeguimiento.cliente, qCliente).fetch()
                .join(qSeguimiento.seguimientoTipo, qSeguimientoTipo).fetch()
                .leftJoin(qSeguimiento.campanya, qCampanya).fetch()
                .where(qSeguimiento.cliente.id.eq(clienteId));

        return query.list(qSeguimiento);
    }

    @Override
    public Seguimiento getSeguimientoById(Long seguimientoId) {
        JPAQuery query = new JPAQuery(entityManager);

        QSeguimiento Seguimiento = QSeguimiento.seguimiento;

        query.from(Seguimiento)
                .where(Seguimiento.id.eq(seguimientoId));

        List<Seguimiento> lista = query.list(Seguimiento);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

    @Override
    public List<Seguimiento> getSeguimientosByClienteIdAndCampanyaId(Long clienteId, Long campanyaId) {
        JPAQuery query = new JPAQuery(entityManager);

        QSeguimiento qSeguimiento = QSeguimiento.seguimiento;
        QSeguimientoTipo qSeguimientoTipo = QSeguimientoTipo.seguimientoTipo;
        QCampanya qCampanya = QCampanya.campanya1;

        query.from(qSeguimiento)
                .join(qSeguimiento.seguimientoTipo, qSeguimientoTipo).fetch()
                .leftJoin(qSeguimiento.campanya, qCampanya).fetch()
                .where(qSeguimiento.cliente.id.eq(clienteId)
                        .and(qSeguimiento.campanya.id.eq(campanyaId)));

        return query.list(qSeguimiento);

    }

    @Override
    public List<Seguimiento> getSeguimientosByCampanyaId(Long campanyaId) {

        JPAQuery query = new JPAQuery(entityManager);

        QSeguimiento qSeguimiento = QSeguimiento.seguimiento;

        query.from(qSeguimiento)
                .where(qSeguimiento.campanya.id.eq(campanyaId));

        return query.list(qSeguimiento);
    }

}