package es.uji.apps.crm.dao;

import es.uji.apps.crm.model.ClienteEstadoCampanyaMotivo;
import es.uji.commons.db.BaseDAO;

public interface ClienteEstadoCampanyaMotivoDAO extends BaseDAO {

    ClienteEstadoCampanyaMotivo getClienteEstadoMotivoByCampanyaClienteId(Long campanyaClienteId, Long campanyaEstadoId);
}