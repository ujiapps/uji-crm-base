package es.uji.apps.crm.services;

import com.sun.jersey.core.header.FormDataContentDisposition;
import es.uji.apps.crm.model.*;
import es.uji.apps.crm.model.domains.TipoAccesoDatoUsuario;
import es.uji.apps.crm.ui.ClienteDatoOpcionUI;
import es.uji.apps.crm.ui.ClienteTitulacionNoUjiUI;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StreamUtils;
import es.uji.commons.web.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Service
public class AlumniFormularioService {

    @Context
    ServletContext servletContext;

    private final static Long TIPONIVEL = 8740244L;
    private final static Long CAMPANYAPREMIUM = 1070562L;
    private final static Long DATOEXTRALABORALACTIVO = 8740226L;
    private final static Long DATOEXTRALABORALESTUDIANTE = 8740229L;
    private final static Long ESTADOLABORALACTIVO = 996676L;
    private final static Long ESTADOLABORALESTUDIANTE = 1638182L;
    private final static Long PERFILLINKEDIN = 1645952L;
    private final static Long TIPOESTUDIANTE = 8740229L;

    private final ClienteDatoTipoService clienteDatoTipoService;
    private final PaisService paisService;
    private final ProvinciaService provinciaService;
    private final PoblacionService poblacionService;
    private final UtilsService utilsService;
    private final UniversidadService universidadService;
    private final ClienteTitulacionesUJIService clienteTitulacionesUJIService;
    private final ClasificacionEstudioNoUJIService clasificacionEstudioNoUJIService;
    private final SesionService sesionService;
    private final AlumniService alumniService;
    private final ItemService itemService;
    private final ClienteItemService clienteItemService;
    private final ClienteDatoService clienteDatoService;
    private final ClienteTitulacionesNoUJIService clienteTitulacionesNoUJIService;
    private final ClienteService clienteService;
    private final ClienteDatoOpcionService clienteDatoOpcionService;

    private final String directorioJavascript = "/crm/js/2024/";

    @Autowired
    public AlumniFormularioService(ClienteDatoTipoService clienteDatoTipoService,
                                   PaisService paisService,
                                   ProvinciaService provinciaService,
                                   PoblacionService poblacionService,
                                   UtilsService utilsService,
                                   UniversidadService universidadService,
                                   ClienteTitulacionesUJIService clienteTitulacionesUJIService,
                                   ClasificacionEstudioNoUJIService clasificacionEstudioNoUJIService,
                                   SesionService sesionService,
                                   AlumniService alumniService,
                                   ItemService itemService,
                                   ClienteItemService clienteItemService,
                                   ClienteDatoService clienteDatoService,
                                   ClienteTitulacionesNoUJIService clienteTitulacionesNoUJIService,
                                   ClienteService clienteService,
                                   ClienteDatoOpcionService clienteDatoOpcionService
    ) {
        this.clienteDatoTipoService = clienteDatoTipoService;
        this.paisService = paisService;
        this.provinciaService = provinciaService;
        this.poblacionService = poblacionService;
        this.utilsService = utilsService;
        this.universidadService = universidadService;
        this.clienteTitulacionesUJIService = clienteTitulacionesUJIService;
        this.clasificacionEstudioNoUJIService = clasificacionEstudioNoUJIService;
        this.sesionService = sesionService;
        this.alumniService = alumniService;
        this.itemService = itemService;
        this.clienteItemService = clienteItemService;
        this.clienteDatoService = clienteDatoService;
        this.clienteTitulacionesNoUJIService = clienteTitulacionesNoUJIService;
        this.clienteService = clienteService;
        this.clienteDatoOpcionService = clienteDatoOpcionService;
    }

    public Template getFormularioAltaPremium(String idioma, String hash, String path) throws ParseException {
        return getFormulario(idioma, hash, path, "premium/formularioalta", directorioJavascript + "formularioAltaPremium.js");
    }

    public Template getFormularioAltaOipep(String idioma, String hash, String path) throws ParseException {
        return getFormulario(idioma, hash, path, "oipep/formularioalta", directorioJavascript + "formularioAltaOipep.js");
    }

    public Template getFormulario(String idioma, String hash, String path, String contenido, String javascript) throws ParseException {

        Cliente cliente = sesionService.getUserIdActiveSesion(hash, Boolean.TRUE);

        if (ParamUtils.isNotNull(cliente)) {
            Template template = alumniService.getTemplateBaseAlumni(path, idioma, contenido);
            template.put("hash", hash);
            cargaDatosPersonales(template, cliente, idioma);
            cargaDatosLaborales(idioma, cliente, template);
            cargaDatosEstudios(idioma, cliente, template);
            cargaDatosOtros(idioma, template, cliente);
            template.put("footer", javascript);
            return template;
        }
        return alumniService.getTemplateAviso(idioma, "aviso.formulario.error.hash", "login", path);
    }

    private void cargaDatosOtros(String idioma, Template template, Cliente cliente) throws ParseException {

        final Long IBAN = 17L;
        final Long GRUPOCOMOHASCONOCIDO = 5115980L;

        template.put("comoHasConocido", itemService.getItemsByGrupoOrderByIdioma(GRUPOCOMOHASCONOCIDO, idioma));
        List<ClienteItem> conoce = clienteItemService.getItemsByGrupoIdAndClienteIdAndCampanyaId(GRUPOCOMOHASCONOCIDO, cliente.getId(), CAMPANYAPREMIUM);
        template.put("conoce", !conoce.isEmpty() ? conoce.get(0).getItem().getId() : null);
        ClienteDato iban = clienteDatoService.getClienteDatoByClienteAndTipoIdVinculadoCampanya(cliente, IBAN, CAMPANYAPREMIUM);
        template.put("iban", ParamUtils.isNotNull(iban) ? iban.getValorSegunTipo() : "");
        ClienteDato linkedin = clienteDatoService.getClienteDatoByClienteAndTipoId(cliente, PERFILLINKEDIN);
        template.put("linkedin", ParamUtils.isNotNull(linkedin) ? linkedin.getValorSegunTipo() : "");
    }

    private void cargaDatosEstudios(String idioma, Cliente cliente, Template template) {

        final Long TIPOESTUDIOGRADO = 1L;
        final Long TIPOESTUDIOSUPERIORES = 6L;

        template.put("listaEstudiosGrado", clasificacionEstudioNoUJIService.getClasificacionEstudiosPorTipo(TIPOESTUDIOGRADO, idioma));
        template.put("listaEstudiosSuperiores", clasificacionEstudioNoUJIService.getClasificacionEstudiosPorTipo(TIPOESTUDIOSUPERIORES, idioma));
        template.put("universidades", universidadService.getUniversidades(idioma));
        template.put("nivelEstudios", clienteDatoTipoService.getClienteDatoOpcionesByTipoId(TIPONIVEL, idioma));
        template.put("listaEstudioUJINoUJI", clienteDatoTipoService.getClienteDatoOpcionesByTipoId(TIPOESTUDIANTE, idioma));
        //CARGA DATOS ESTUDIO DEL CLIENTE
        List<ClienteTitulacionNoUjiUI> clienteTitulacionesNoUJIS = clienteTitulacionesNoUJIService.getClienteEstudiosNoUJIByPersona(cliente.getId(), idioma);
        if (!clienteTitulacionesNoUJIS.isEmpty()) {
            template.put("estudiosNoUJI", clienteTitulacionesNoUJIS);
        }
        List<ClienteTitulacionesUJI> clienteTitulacionesUJIS = clienteTitulacionesUJIService.getClienteEstudiosUJIByClienteJoinClasificacion(cliente.getId(), idioma);
        if (!clienteTitulacionesUJIS.isEmpty()) {
            template.put("estudiosUJI", clienteTitulacionesUJIS);
        }
    }

    private void cargaDatosLaborales(String idioma, Cliente cliente, Template template) throws ParseException {

        final Long GRUPOSECTOREMPRESARIAL = 807418L;
        final Long GRUPOLABORAL = 996675L;

        List<ClienteItem> situacionLaboral = clienteItemService.getItemsByGrupoIdAndClienteIdAndCampanyaId(GRUPOLABORAL, cliente.getId(), CAMPANYAPREMIUM);
        Long situacionLaboralId = !situacionLaboral.isEmpty() ? situacionLaboral.get(0).getItem().getId() : null;
        template.put("situacionLaboral", situacionLaboralId);
        template.put("sectorEmpresarialOpciones", itemService.getItemsByGrupoOrderByIdioma(GRUPOSECTOREMPRESARIAL, idioma));
        template.put("situacionLaboralOpciones", itemService.getItemsByGrupoOrderByIdioma(GRUPOLABORAL, idioma));
        List<ClienteDatoOpcionUI> tiposTrabajo = clienteDatoTipoService.getClienteDatoOpcionesByTipoId(DATOEXTRALABORALACTIVO, idioma);
        template.put("tiposTrabajo", tiposTrabajo);
        List<ClienteDatoOpcionUI> lugaresEstudiante = clienteDatoTipoService.getClienteDatoOpcionesByTipoId(DATOEXTRALABORALESTUDIANTE, idioma);
        template.put("lugaresEstudiante", lugaresEstudiante);

        if (ParamUtils.isNotNull(situacionLaboralId)) {
            if (situacionLaboralId.equals(ESTADOLABORALACTIVO)) {
                Item itemSectorEmpresarial = clienteItemService.getItemsByGrupoIdAndClienteIdAndCampanyaId(GRUPOSECTOREMPRESARIAL, cliente.getId(), CAMPANYAPREMIUM).get(0).getItem();
                template.put("sectorEmpresarial", itemSectorEmpresarial.getId());

                ClienteDato clienteDato = clienteDatoService.getClienteDatoByClienteAndTipoIdAndItem(cliente, DATOEXTRALABORALACTIVO, situacionLaboral.get(0).getItem());
                if (ParamUtils.isNotNull(clienteDato)) {
                    template.put("tipoTrabajo", clienteDato.getValorSegunTipo());
                }
                return;
            }

            if (situacionLaboralId.equals(ESTADOLABORALESTUDIANTE)) {
                ClienteDato clienteDato = clienteDatoService.getClienteDatoByClienteAndTipoIdAndItem(cliente, DATOEXTRALABORALESTUDIANTE, situacionLaboral.get(0).getItem());
                if (ParamUtils.isNotNull(clienteDato)) {
                    template.put("lugarEstudiante", clienteDato.getValorSegunTipo());
                }
            }
        }
    }

    private void cargaDatosPersonales(Template template, Cliente cliente, String idioma) {
        template.put("nombre", cliente.getNombre());
        template.put("apellidos", cliente.getApellidos());
        template.put("correo", cliente.getCorreo());
        template.put("identificacion", cliente.getClienteGeneral().getIdentificacion());
        template.put("fechaNacimiento", ParamUtils.isNotNull(cliente.getFechaNacimiento()) ? utilsService.fechaFormat(cliente.getFechaNacimiento(), "dd/MM/YYYY") : "");
        template.put("telefono", cliente.getMovil());
        template.put("nacionalidad", ParamUtils.isNotNull(cliente.getNacionalidad()) ? cliente.getNacionalidad().getId() : "");
        template.put("postal", cliente.getPostal());
        template.put("codigoPostal", cliente.getCodigoPostal());
        template.put("provinciaPostal", ParamUtils.isNotNull(cliente.getProvinciaPostal()) ? cliente.getProvinciaPostal().getId() : "");
        template.put("poblacionPostal", ParamUtils.isNotNull(cliente.getPoblacionPostal()) ? cliente.getPoblacionPostal().getId() : "");
        template.put("paisPostal", ParamUtils.isNotNull(cliente.getPaisPostal()) ? cliente.getPaisPostal().getId() : "");
        template.put("nacionalidades", paisService.getOpcionPaisesOrdenadosPorIdioma(idioma));
        template.put("paises", paisService.getOpcionPaisesOrdenadosPorIdioma(idioma));
        template.put("provincias", provinciaService.getOpcionProvincias(idioma));
        if (ParamUtils.isNotNull(cliente.getPoblacionPostal())) {
            template.put("poblaciones", poblacionService.getOpcionPoblacionesByProvinciaId(cliente.getProvinciaPostal().getId(), idioma));
        }
    }

    public void updateDatosCliente(Cliente cliente, String nombre, String apellidos, Date fechaNacimiento, String
            nacionalidad, String telefono, String paisPostal, Long provinciaPostal, Long poblacionPostal, String
                                           codigoPostal, String postal) {

        cliente.updateDatosFormularioPremium(nombre,
                apellidos,
                fechaNacimiento,
                nacionalidad,
                telefono,
                paisPostal,
                provinciaPostal,
                poblacionPostal,
                codigoPostal,
                postal);

        clienteService.updateCliente(cliente);
    }

    public void addDatosEstudios(Long nivelEstudios, String nombreEstudio, Long estudiosSuperiores, Long estudioUJINoUJI, String
            tipoEstudioUJINoUJI, Long listadoEstudioNoUji, String nombreEstudioNoUji, Long universidadEstudio, Long anyoFinalizacionEstudio, Cliente cliente) throws
            ParseException {

        if (!ParamUtils.isNotNull(nivelEstudios)) {
            return;
        }

        ClienteDatoOpcion clienteDatoOpcionNivelEstudio = clienteDatoOpcionService.getClienteDatoOpcionById(nivelEstudios);
        ClienteDato clienteDatoNivelEstudios = addClienteNivelEstudioDatosExtra(cliente, nivelEstudios);

        if ("SUP".equals(clienteDatoOpcionNivelEstudio.getReferencia())) {
            clienteDatoService.addClienteDatoExtra(cliente.getId(), TIPOESTUDIANTE, TipoAccesoDatoUsuario.INTERNO.getId(), null, estudioUJINoUJI.toString(), clienteDatoNivelEstudios.getId(), null, null);

            ClienteDatoOpcion clienteDatoOpcionTipoEstudio = clienteDatoOpcionService.getClienteDatoOpcionById(estudioUJINoUJI);

            if ("NOUJI".equals(clienteDatoOpcionTipoEstudio.getReferencia())) {
                anyadirEstudio(cliente, tipoEstudioUJINoUJI, null, listadoEstudioNoUji, nombreEstudioNoUji, anyoFinalizacionEstudio, universidadEstudio, clienteDatoNivelEstudios);
                return;
            }
            return;
        }
        if ("NO".equals(clienteDatoOpcionNivelEstudio.getReferencia())) {
            anyadirEstudio(cliente, clienteDatoOpcionNivelEstudio.getReferencia(), estudiosSuperiores, null, null, null, null, clienteDatoNivelEstudios);
            return;
        }
        anyadirEstudio(cliente, clienteDatoOpcionNivelEstudio.getReferencia(), null, null, nombreEstudio, null, null, clienteDatoNivelEstudios);
    }

    public void addDatosLaborales(Long laboral,
                                  Long sectorEmpresarial,
                                  String tipoTrabajo,
                                  String lugarEstudiante,
                                  Cliente cliente) throws ParseException {

        ClienteItem clienteItemEstadoLaboral = anyadirEstadoLaboral(laboral, cliente);

        if (laboral.equals(ESTADOLABORALACTIVO)) {
            Item itemSectorEmpresarial = itemService.getItemById(sectorEmpresarial);
            clienteItemService.addClienteItemSinDuplicados(cliente, itemSectorEmpresarial, clienteItemEstadoLaboral.getId(), null, CAMPANYAPREMIUM);

            addClienteDatoExtraLaboral(cliente.getId(), DATOEXTRALABORALACTIVO, tipoTrabajo, clienteItemEstadoLaboral);
            return;
        }

        if (laboral.equals(ESTADOLABORALESTUDIANTE)) {
            addClienteDatoExtraLaboral(cliente.getId(), DATOEXTRALABORALESTUDIANTE, lugarEstudiante, clienteItemEstadoLaboral);
        }
    }

    public void addPerfilLinkedin(String perfilLinkedin, Cliente cliente) throws ParseException {
        clienteDatoService.addClienteDatoExtra(cliente.getId(), PERFILLINKEDIN, TipoAccesoDatoUsuario.INTERNO.getId(), null, perfilLinkedin, null, null, null);
    }

    public void addArchivos(InputStream foto, FormDataContentDisposition fotoDetalle, InputStream doc, FormDataContentDisposition docDetalle, Cliente cliente) throws IOException {
        Long TIPOFOTO = 3996634L;
        Long TIPOARCHIVO = 4L;

        if (ParamUtils.isNotNull(docDetalle.getFileName())) {
            alumniService.anyadirArchivo(cliente, TIPOARCHIVO, docDetalle.getFileName(), servletContext.getMimeType(docDetalle.getFileName()),
                    StreamUtils.inputStreamToByteArray(doc));
        }
        if (ParamUtils.isNotNull(fotoDetalle.getFileName())) {
            alumniService.anyadirArchivo(cliente, TIPOFOTO, fotoDetalle.getFileName(), servletContext.getMimeType(fotoDetalle.getFileName()),
                    StreamUtils.inputStreamToByteArray(foto));
        }
    }

    private ClienteDato addClienteNivelEstudioDatosExtra(Cliente cliente, Long dato) throws ParseException {
        Long ESTUDIOSACTUALES = 8740244L;
        ClienteDato clienteDatoNivelEstudios = clienteDatoService.getClienteDatoByClienteAndTipoIdVinculadoCampanya(cliente, ESTUDIOSACTUALES, CAMPANYAPREMIUM);
        if (ParamUtils.isNotNull(clienteDatoNivelEstudios)) {
            clienteDatoService.delete(clienteDatoNivelEstudios.getId());
        }
        return clienteDatoService.addClienteDatoExtra(cliente.getId(), ESTUDIOSACTUALES, TipoAccesoDatoUsuario.INTERNO.getId(), null, dato.toString(), null, null, CAMPANYAPREMIUM);
    }

    private void addClienteDatoExtraLaboral(Long clienteId, Long tipoDatoExtra, String valor, ClienteItem clienteItem) throws
            ParseException {
        clienteDatoService.addClienteDatoExtra(clienteId, tipoDatoExtra, TipoAccesoDatoUsuario.INTERNO.getId(), null, valor, null, clienteItem.getItem().getId(), CAMPANYAPREMIUM);

    }

    private ClienteItem anyadirEstadoLaboral(Long laboral, Cliente cliente) {
        Item estadoLaboral = itemService.getItemById(laboral);
        List<ClienteItem> clienteItemsCampanyaLaboral = clienteItemService.getItemsByGrupoIdAndClienteIdAndCampanyaId(estadoLaboral.getGrupo().getId(), cliente.getId(), CAMPANYAPREMIUM);
        clienteItemsCampanyaLaboral.forEach(clienteItem -> clienteItemService.deleteClienteItem(clienteItem.getId()));

        return clienteItemService.addClienteItem(cliente.getId(), laboral, null, null, CAMPANYAPREMIUM, clienteItemService.valorItem(estadoLaboral.getCorreo().getId()), clienteItemService.valorItem(estadoLaboral.getCorreoPostal().getId()));
    }

    private void anyadirEstudio(Cliente cliente, String tipoEstudio, Long ambitoEstudiosNoUniversitariosId, Long gradoId, String textoEstudio, Long anyoEstudio, Long universidadId, ClienteDato clienteDatoEstudio) {

        switch (tipoEstudio) {
            case "ELEM":
                clienteTitulacionesNoUJIService.addClienteTitulacion(cliente, null, null, null, textoEstudio, 4L, clienteDatoEstudio);
                break;
            case "MIT":
                clienteTitulacionesNoUJIService.addClienteTitulacion(cliente, ambitoEstudiosNoUniversitariosId, null, null, textoEstudio, 5L, clienteDatoEstudio);
                break;
            case "NO":
                clienteTitulacionesNoUJIService.addClienteTitulacion(cliente, ambitoEstudiosNoUniversitariosId, null, null, textoEstudio, 6L, clienteDatoEstudio);
                break;
            case "GRADO":
                clienteTitulacionesNoUJIService.addClienteTitulacion(cliente, gradoId, anyoEstudio.toString(), universidadId, textoEstudio, 1L, clienteDatoEstudio);
                break;
            case "POST":
                clienteTitulacionesNoUJIService.addClienteTitulacion(cliente, null, anyoEstudio.toString(), universidadId, textoEstudio, 2L, clienteDatoEstudio);
                break;
            case "DOC":
                clienteTitulacionesNoUJIService.addClienteTitulacion(cliente, null, anyoEstudio.toString(), universidadId, textoEstudio, 3L, clienteDatoEstudio);
                break;
        }
    }

    public Response guardarDatosFormularioOipep(String hash,
                                                Long campanya,
                                                String nombre,
                                                String apellidos,
                                                Date fechaNacimiento,
                                                String nacionalidad,
                                                String telefono,
                                                String paisPostal,
                                                Long provinciaPostal,
                                                Long poblacionPostal,
                                                String codigoPostal,
                                                String postal,
                                                Long nivelEstudios,
                                                String nombreEstudio,
                                                Long estudiosSuperiores,
                                                Long estudioUJINoUJI,
                                                String tipoEstudioUJINoUJI,
                                                Long listaEstudiosGrado,
                                                String nombreEstudioNoGrado,
                                                Long universidadEstudio,
                                                Long anyoFinalizacionEstudio,
                                                Long laboral,
                                                Long sectorEmpresarial,
                                                String tipoTrabajo,
                                                String lugarEstudiante,
                                                String perfilLinkedin,
                                                InputStream foto,
                                                FormDataContentDisposition fotoDetalle,
                                                InputStream doc,
                                                FormDataContentDisposition docDetalle) throws SQLException, ParseException, IOException {

        Cliente cliente = sesionService.getUserIdActiveSesion(hash, Boolean.TRUE);
        updateDatosCliente(cliente, nombre, apellidos, fechaNacimiento, nacionalidad, telefono, paisPostal, provinciaPostal, poblacionPostal, codigoPostal, postal);
        alumniService.altaClienteOipep(cliente, null, campanya);

        addDatosEstudios(nivelEstudios, nombreEstudio, estudiosSuperiores, estudioUJINoUJI, tipoEstudioUJINoUJI, listaEstudiosGrado, nombreEstudioNoGrado, universidadEstudio, anyoFinalizacionEstudio, cliente);
        addDatosLaborales(laboral, sectorEmpresarial, tipoTrabajo, lugarEstudiante, cliente);
        addPerfilLinkedin(perfilLinkedin, cliente);
        addArchivos(foto, fotoDetalle, doc, docDetalle, cliente);

        return Response.ok().build();

    }

}
