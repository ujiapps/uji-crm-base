package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.services.CuentasCorporativasService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("cuentacorporativa")
public class CuentaCorporativaResource extends CoreBaseService {
    @InjectParam
    private CuentasCorporativasService cuentasCorporativasService;

    @GET
    @Path("{persona}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClienteItemById(@PathParam("persona") Long personaId) {

        return UIEntity.toUI(cuentasCorporativasService.getCuentasCorporativasByUserId(personaId));

    }
}