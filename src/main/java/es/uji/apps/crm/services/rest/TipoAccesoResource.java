package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.services.TipoAccesoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("tipoacceso")
public class TipoAccesoResource extends CoreBaseService {
    @InjectParam
    private TipoAccesoService consultaTipoAcceso;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposAcceso() {
        return UIEntity.toUI(consultaTipoAcceso.getTiposAcceso());
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getTipoAccesoById(@PathParam("id") Long tipoAccesoId) {
        return UIEntity.toUI(consultaTipoAcceso.getTipoAccesoById(tipoAccesoId));
    }

}