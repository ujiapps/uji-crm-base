package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.VinculoUjiDAO;
import es.uji.apps.crm.model.VwVinculacionPersona;

@Service
public class VinculoUjiService {
    @Autowired
    private VinculoUjiDAO vinculoUjiDAO;

    public List<VwVinculacionPersona> getSubVinculosByPersonaId(Long personaId) {

        return vinculoUjiDAO.getSubVinculosByPersonaId(personaId);
    }
}