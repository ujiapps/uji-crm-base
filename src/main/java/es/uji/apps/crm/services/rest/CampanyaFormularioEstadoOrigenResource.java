package es.uji.apps.crm.services.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.CampanyaFormulario;
import es.uji.apps.crm.model.CampanyaFormularioEstadoOrigen;
import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.apps.crm.services.CampanyaFormularioEstadoOrigenService;
import es.uji.apps.crm.services.CampanyaFormularioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("campanyaformularioestadoorigen")
public class CampanyaFormularioEstadoOrigenResource extends CoreBaseService {

    @InjectParam
    private CampanyaFormularioEstadoOrigenService campanyaFormularioEstadoOrigenService;
    @InjectParam
    private CampanyaFormularioService campanyaFormularioService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyaFormularioEstadosOrigenByFormulario(@QueryParam("campanyaFormularioId") Long formularioId) {


        return modelToUI(campanyaFormularioEstadoOrigenService.getCampanyaFormularioEstadosOrigenByFormulario(formularioId));
    }

    private List<UIEntity> modelToUI(List<CampanyaFormularioEstadoOrigen> campanyaFormularioEstadosOrigen) {
        return campanyaFormularioEstadosOrigen.stream().map(this::modelToUI).collect(Collectors.toList());
    }

//    @POST
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Path("estadosorigen")
//    public UIEntity addCampanyaFormularioEstadoOrigen(UIEntity entity) throws ParseException {
//        CampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen = UIToModel(entity);
//        campanyaFormularioEstadoOrigenService.addCampanyaFormularioEstadoOrigen(campanyaFormularioEstadoOrigen);
//        return modelToUI(campanyaFormularioEstadoOrigen);
//    }
//
//    @PUT
//    @Path("{id}")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    public UIEntity updateCampanyaFormulario(@PathParam("id") Long campanyaFormularioId, UIEntity entity) throws
//            ParseException {
//
//        CampanyaFormulario campanyaFormulario = UIToModel(entity);
//        campanyaFormulario.setId(campanyaFormularioId);
//
//        campanyaFormularioService.updateCampanyaFormulario(campanyaFormulario);
//        return modelToUI(campanyaFormulario);
//    }

    private UIEntity modelToUI(CampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen) {
        return UIEntity.toUI(campanyaFormularioEstadoOrigen);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public List<UIEntity> addCampanyaFormularioEstadoOrigen(@FormParam("campanyaFormularioId") Long campanyaFormularioId, @FormParam("arrayEstadosOrigen") JSONArray estadosOrigen)
            throws JSONException {

        CampanyaFormulario campanyaFormulario = campanyaFormularioService.getFormularioById(campanyaFormularioId);

        campanyaFormularioEstadoOrigenService.deleteCampanyaFormularioEstadoOrigenByCampanyaFormularioId(campanyaFormulario);
        for (int i = 0; i < estadosOrigen.length(); i++)
        {

            JSONObject campanyaFormularioEstadoOrigenJsonObject = estadosOrigen.getJSONObject(i);
            campanyaFormularioEstadoOrigenService.insertarCampanyaFormularioEstadoOrigen(campanyaFormulario, campanyaFormularioEstadoOrigenJsonObject);

        }
        return modelToUI(campanyaFormularioEstadoOrigenService.getCampanyaFormularioEstadosOrigenByFormulario(campanyaFormularioId));
//        CampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen = UIToModel(entity);
//        campanyaFormularioEstadoOrigenService.addCampanyaFormularioEstadoOrigen(campanyaFormularioEstadoOrigen);
//        return modelToUI(campanyaFormularioEstadoOrigen);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteCampanyaFormularioEstadoOrigen(@PathParam("id") Long campanyaFormularioEstadoOrigenId) {
        campanyaFormularioEstadoOrigenService.deleteCampanyaFormularioEstadoOrigen(campanyaFormularioEstadoOrigenId);
    }

    private CampanyaFormularioEstadoOrigen UIToModel(UIEntity entity) {

        CampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen = entity.toModel(CampanyaFormularioEstadoOrigen.class);

        CampanyaFormulario campanyaFormulario = new CampanyaFormulario();
        campanyaFormulario.setId(ParamUtils.parseLong(entity.get("formularioId")));
        campanyaFormularioEstadoOrigen.setCampanyaFormulario(campanyaFormulario);


        if (ParamUtils.isNotNull(entity.get("estadoOrigenId")))
        {
            TipoEstadoCampanya estadoOrigen = new TipoEstadoCampanya();
            estadoOrigen.setId(ParamUtils.parseLong(entity.get("estadoOrigenId")));
            campanyaFormularioEstadoOrigen.setEstadoOrigen(estadoOrigen);
        }

        return campanyaFormularioEstadoOrigen;
    }

}