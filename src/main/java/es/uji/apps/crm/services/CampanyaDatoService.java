package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaDatoDAO;
import es.uji.apps.crm.model.CampanyaDato;
import es.uji.apps.crm.model.CampanyaFormulario;
import es.uji.apps.crm.model.ClienteDatoTipo;
import es.uji.apps.crm.model.Grupo;
import es.uji.apps.crm.model.Tipo;

@Service
public class CampanyaDatoService {
    private CampanyaDatoDAO campanyaDatoDAO;

    @Autowired
    public CampanyaDatoService(CampanyaDatoDAO campanyaDatoDAO) {
        this.campanyaDatoDAO = campanyaDatoDAO;
    }

    public CampanyaDato getCampanyaDatoById(Long campanyaDatoId) {
        return campanyaDatoDAO.getCampanyaDatoById(campanyaDatoId);
    }

    public void updateCampanyaDato(CampanyaDato campanyaDato) {
        campanyaDatoDAO.update(campanyaDato);
    }

    public void deleteCampanyaDato(Long campanyaDatoId) {
        campanyaDatoDAO.delete(CampanyaDato.class, campanyaDatoId);
    }

    public List<CampanyaDato> getCampanyaDatosPrincipalesByFormularioId(Long formularioId) {
        return campanyaDatoDAO.getCampanyaDatosPrincipalesByFormularioId(formularioId);
    }

    public List<CampanyaDato> getCampanyaDatosByFormularioId(Long formularioId) {
        return campanyaDatoDAO.getCampanyaDatosByFormularioId(formularioId);
    }

    public List<CampanyaDato> getCampanyaDatoRelacionados(Long campanyaDatoId) {
        return campanyaDatoDAO.getCampanyaDatoRelacionados(campanyaDatoId);
    }

    public List<CampanyaDato> getCampanyaDatosSinDatoAsociadoByFormularioId(Long campanyaFormularioId) {
        return campanyaDatoDAO.getCampanyaDatosSinDatoAsociadoByFormularioId(campanyaFormularioId);
    }

    public void addCampanyaDatosGenericaActoBaja(CampanyaFormulario campanyaFormulario) {

        Tipo tipoAcceso = new Tipo();
        tipoAcceso.setId(4L);

        CampanyaDato campanyaDato = new CampanyaDato();
        campanyaDato.setCampanyaFormulario(campanyaFormulario);

        campanyaDato.setOrden(1);
        campanyaDato.setTextoCa("Motiu pel qual no assistiré a l'acte oficial de graduació ");
        campanyaDato.setTextoEs("Motivo por el cual no asistiré al acto oficial de graduación");
        campanyaDato.setRequerido(Boolean.TRUE);
        campanyaDato.setVinculadoCampanya(Boolean.FALSE);
        campanyaDato.setVisualizar(Boolean.TRUE);
        campanyaDato.setTipoAcceso(tipoAcceso);

        ClienteDatoTipo clienteDatoTipo = new ClienteDatoTipo();
        clienteDatoTipo.setId(3356207L);
        campanyaDato.setClienteDatoTipo(clienteDatoTipo);

        addCampanyaDato(campanyaDato);
    }

    public CampanyaDato addCampanyaDato(CampanyaDato campanyaDato) {
        campanyaDatoDAO.insert(campanyaDato);
        campanyaDato.setId(campanyaDato.getId());
        return campanyaDato;
    }

    public void addCampanyaDatosGenericaActoAlta(CampanyaFormulario campanyaFormulario) {

        Tipo tipoAcceso = new Tipo();
        tipoAcceso.setId(4L);

//        CampanyaDato campanyaDatoTitulacion = new CampanyaDato();
//        campanyaDatoTitulacion.setCampanyaFormulario(campanyaFormulario);
//
//        campanyaDatoTitulacion.setOrden(2);
//        campanyaDatoTitulacion.setTextoCa("Titulació finalitzada al curs acadèmic 2018/2019");
//        campanyaDatoTitulacion.setTextoEs("Titulación finalizada en el curso académico 2018-2019");
//        campanyaDatoTitulacion.setRequerido(Boolean.TRUE);
//        campanyaDatoTitulacion.setVinculadoCampanya(Boolean.FALSE);
//        campanyaDatoTitulacion.setVisualizar(Boolean.TRUE);
//        campanyaDatoTitulacion.setTipoAcceso(tipoAcceso);
//
//        Grupo grupo = new Grupo();
//        grupo.setId(4L);
//        campanyaDatoTitulacion.setGrupo(grupo);
//
//        ClienteDatoTipo clienteDatoTipo = new ClienteDatoTipo();
//        clienteDatoTipo.setId(16L);
//        campanyaDatoTitulacion.setClienteDatoTipo(clienteDatoTipo);

//        addCampanyaDato(campanyaDatoTitulacion);


        CampanyaDato campanyaDatoEntradas = new CampanyaDato();
        campanyaDatoEntradas.setCampanyaFormulario(campanyaFormulario);

        campanyaDatoEntradas.setOrden(3);
        campanyaDatoEntradas.setTextoCa("Nombre d'entrades per als acompanyants (màxim 2 entrades)");
        campanyaDatoEntradas.setTextoEs("Número de entradas para los acompañantes (máximo 2 entradas)");
        campanyaDatoEntradas.setRequerido(Boolean.TRUE);
        campanyaDatoEntradas.setVinculadoCampanya(Boolean.TRUE);
        campanyaDatoEntradas.setVisualizar(Boolean.TRUE);
        campanyaDatoEntradas.setTamanyo(2L);
        campanyaDatoEntradas.setEtiqueta("ENTRADAS");

        campanyaDatoEntradas.setTipoAcceso(tipoAcceso);


        ClienteDatoTipo clienteDatoTipoEntradas = new ClienteDatoTipo();
        clienteDatoTipoEntradas.setId(2494144L);
        campanyaDatoEntradas.setClienteDatoTipo(clienteDatoTipoEntradas);

        addCampanyaDato(campanyaDatoEntradas);

        CampanyaDato campanyaDatoEntradasExtra = new CampanyaDato();
        campanyaDatoEntradasExtra.setCampanyaFormulario(campanyaFormulario);

        campanyaDatoEntradasExtra.setOrden(4);
        campanyaDatoEntradasExtra.setTextoCa("Si queden places lliures al Paranimf i amics, necessitaries més entrades? (Indica la quantitat d'entrades extra que t'agradaria rebre)");
        campanyaDatoEntradasExtra.setTextoEs("Si quedan plazas libres en el Paranimf para familiares y amigos, necesitarías más entradas? (Indica la cantidad que te gustaría recibir) ");
        campanyaDatoEntradasExtra.setRequerido(Boolean.TRUE);
        campanyaDatoEntradasExtra.setVinculadoCampanya(Boolean.TRUE);
        campanyaDatoEntradasExtra.setVisualizar(Boolean.TRUE);
        campanyaDatoEntradasExtra.setTamanyo(6L);
        campanyaDatoEntradasExtra.setEtiqueta("ENTRADAS-SOBRANTES");

        campanyaDatoEntradasExtra.setTipoAcceso(tipoAcceso);


        ClienteDatoTipo clienteDatoTipoEntradasExtra = new ClienteDatoTipo();
        clienteDatoTipoEntradasExtra.setId(2544557L);
        campanyaDatoEntradasExtra.setClienteDatoTipo(clienteDatoTipoEntradasExtra);

        addCampanyaDato(campanyaDatoEntradasExtra);

    }
}