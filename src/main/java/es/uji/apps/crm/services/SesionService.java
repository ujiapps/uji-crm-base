package es.uji.apps.crm.services;

import java.util.Calendar;
import java.util.Date;

import com.sun.research.ws.wadl.Param;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.SesionDAO;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.Sesion;

@Service
public class SesionService {
    private SesionDAO sesionDAO;

    @Autowired
    public SesionService(SesionDAO sesionDAO) {
        this.sesionDAO = sesionDAO;
    }

    public Sesion addSesion(String hash, Cliente cliente) {

        Sesion sesion = sesionDAO.getSesionByHash(hash);
        if (ParamUtils.isNotNull(sesion)) {

            sesion.setFecha(new Date());

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.DAY_OF_YEAR, 90);
            sesion.setFechaCaducidad(calendar.getTime());

            sesion.setTipo(0L);
            sesionDAO.update(sesion);
        } else {
            sesion = new Sesion();
            sesion.setCliente(cliente);
            sesion.setFecha(new Date());

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.DAY_OF_YEAR, 90);
            sesion.setFechaCaducidad(calendar.getTime());

            sesion.setTipo(0L);

            sesion.setId(hash);

            sesionDAO.insert(sesion);
        }
        return sesion;
    }


    public Sesion renewSesion(Cliente cliente, String hash) {
        sesionDAO.deleteSesion(cliente.getId());

        Sesion sesion = new Sesion();
        sesion.setCliente(cliente);
        sesion.setFecha(new Date());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        sesion.setFechaCaducidad(calendar.getTime());
        sesion.setTipo(0L);
        sesion.setId(hash);

        sesionDAO.insert(sesion);
        return sesion;
    }

    public Sesion renewSesion(Cliente cliente, String hash, String correo) {
        sesionDAO.deleteSesion(cliente.getId());

        Sesion sesion = new Sesion();
        sesion.setCliente(cliente);
        sesion.setFecha(new Date());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        sesion.setFechaCaducidad(calendar.getTime());
        sesion.setTipo(0L);
        sesion.setId(hash);
        sesion.setCorreo(correo);

        sesionDAO.insert(sesion);
        return sesion;
    }

    public Sesion cambiaClienteSesion(Cliente cliente, String hash) {

        Sesion sesion = getSesion(hash);
        sesion.setCliente(cliente);

        sesionDAO.update(sesion);
        return sesion;
    }

    public Cliente getUserIdActiveSesion(String hash, Boolean fichaZonaPrivada) {

        return sesionDAO.getUserIdActiveSesion(hash, fichaZonaPrivada);

    }

    public Sesion getSesion (String hash){
        return sesionDAO.getSesionByHash(hash);
    }

    public void borraSesion(Cliente cliente) {
        sesionDAO.deleteSesion(cliente.getId());
    }

    public Boolean isValidSession(String hash) {
        return sesionDAO.isValidSession(hash);
    }

    public Boolean getHashBajaMensajeriaValido(String pHash) {
        return sesionDAO.getHashBajaMensajeriaValido(pHash);
    }
}