package es.uji.apps.crm.services;

//import es.uji.apps.crm.model.CampanyaCliente;
//import es.uji.apps.crm.model.TipoEstadoCampanyaMotivo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ClienteEstadoCampanyaMotivoDAO;
import es.uji.apps.crm.model.ClienteEstadoCampanyaMotivo;
import es.uji.commons.rest.ParamUtils;

@Service
public class ClienteEstadoCampanyaMotivoService {
    private ClienteEstadoCampanyaMotivoDAO clienteEstadoCampanyaMotivoDAO;
//    private CampanyaClienteService campanyaClienteService;

    @Autowired
    public ClienteEstadoCampanyaMotivoService(ClienteEstadoCampanyaMotivoDAO clienteEstadoCampanyaMotivoDAO) {
        this.clienteEstadoCampanyaMotivoDAO = clienteEstadoCampanyaMotivoDAO;
//        this.campanyaClienteService = campanyaClienteService;
    }

    public ClienteEstadoCampanyaMotivo addClienteEstadoMotivo(ClienteEstadoCampanyaMotivo clienteEstadoCampanyaMotivo, Long campanyaId) {

        ClienteEstadoCampanyaMotivo clienteEstadoCampanyaMotivoExiste = getClienteEstadoMotivoByCampanyaClienteId(clienteEstadoCampanyaMotivo.getCampanyaCliente().getId(), clienteEstadoCampanyaMotivo.getCampanyaCliente().getCampanyaClienteTipo().getId());

        if (ParamUtils.isNotNull(clienteEstadoCampanyaMotivoExiste))
        {
            clienteEstadoCampanyaMotivoExiste.setCampanyaEstadoMotivo(clienteEstadoCampanyaMotivo.getCampanyaEstadoMotivo());
            clienteEstadoCampanyaMotivoDAO.update(clienteEstadoCampanyaMotivoExiste);
        }
        else
        {
            clienteEstadoCampanyaMotivoDAO.insert(clienteEstadoCampanyaMotivo);
        }
        return clienteEstadoCampanyaMotivo;
    }

//    public ClienteEstadoCampanyaMotivo addClienteEstadoMotivo(UIEntity entity) {
//
//        ClienteEstadoCampanyaMotivo clienteEstadoCampanyaMotivo = new ClienteEstadoCampanyaMotivo();
//
//        TipoEstadoCampanyaMotivo estadoCampanyaMotivo = new TipoEstadoCampanyaMotivo();
//        estadoCampanyaMotivo.setId(ParamUtils.parseLong(entity.get("campanyaEstadoMotivoId")));
//        clienteEstadoCampanyaMotivo.setCampanyaEstadoMotivo(estadoCampanyaMotivo);
//
//        CampanyaCliente campanyaCliente = campanyaClienteService.getCampanyaClienteByCampanyaAndClienteId(ParamUtils.parseLong(entity.get("campanyaId")),
//                ParamUtils.parseLong(entity.get("clienteId")));
//
//        clienteEstadoCampanyaMotivo.setCampanyaCliente(campanyaCliente);
//
//        clienteEstadoCampanyaMotivoDAO.insert(clienteEstadoCampanyaMotivo);
//        return clienteEstadoCampanyaMotivo;
//    }


    public ClienteEstadoCampanyaMotivo getClienteEstadoMotivoByCampanyaClienteId(Long campanyaClienteId, Long campanyaEstadoId) {
        return clienteEstadoCampanyaMotivoDAO.getClienteEstadoMotivoByCampanyaClienteId(campanyaClienteId, campanyaEstadoId);
    }

    public void deleteClienteEstadoCampanyaMotivoByCampanyacliente(Long campanyaClienteId) {

        clienteEstadoCampanyaMotivoDAO.delete(ClienteEstadoCampanyaMotivo.class, "campanya_cliente_id = " + campanyaClienteId);
    }
}