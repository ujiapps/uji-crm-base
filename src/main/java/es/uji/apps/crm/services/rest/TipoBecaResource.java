package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.services.TipoBecaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("tipobeca")
public class TipoBecaResource extends CoreBaseService {

    @InjectParam
    private TipoBecaService tipoBecaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposBeca() {
        return UIEntity.toUI(tipoBecaService.getTiposBeca());
    }
}