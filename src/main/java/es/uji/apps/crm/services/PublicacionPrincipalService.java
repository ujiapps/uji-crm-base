package es.uji.apps.crm.services;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.CampanyaDato;
import es.uji.apps.crm.model.CampanyaFormulario;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteDato;
import es.uji.apps.crm.model.ClienteItem;
import es.uji.apps.crm.model.domains.TipoAccesoDatoUsuario;
import es.uji.apps.crm.model.domains.TipoDato;
import es.uji.apps.crm.model.domains.TipoSolicitudCampo;
import es.uji.apps.crm.ui.CampoUI;
import es.uji.commons.rest.ParamUtils;

@Service
public class PublicacionPrincipalService {

    private static final String NOMBRE_BASE_CAMPO_EXTRA = "campo-extra-";
    @InjectParam
    CampanyaDatoService campanyaDatoService;
    @InjectParam
    ClienteDatoService clienteDatoService;
    @InjectParam
    ClienteItemService clienteItemService;
    @InjectParam
    CampanyaFormularioService campanyaFormularioService;
    @Value("${uji.mac.privateKey}")
    private String privateKey;

    private CampoUI getCampoUIDesdeCampanyaDato(ClienteItem clienteItem, CampanyaDato campanyaDato)
            throws ParseException {

        CampoUI campoUI = new CampoUI(clienteItem.getId(),
                getNombreCampoDatoExtra(campanyaDato.getId()), clienteItem.getItem().getId().toString());
        campoUI.setModel("chosen");

        return campoUI;

    }

    public String getNombreCampoDatoExtra(Long campanyaDatoId) {
        return NOMBRE_BASE_CAMPO_EXTRA + campanyaDatoId.toString();
    }

    public List<CampoUI> getCamposFormularioByFormularioAndCliente(CampanyaFormulario campanyaFormulario, Cliente cliente)
            throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, ParseException {

        List<CampoUI> camposUI = new ArrayList<CampoUI>();

        CampoUI campoId = new CampoUI(0L, "clienteId", cliente.getId().toString());
        camposUI.add(campoId);
        CampoUI token = new CampoUI(1L, "token", getToken(cliente.getId().toString()));
        camposUI.add(token);
        camposUI.addAll(getCamposPrincipalesByFormularioAndCliente(campanyaFormulario, cliente));
        camposUI.addAll(getCamposAdicionalesByFormularioAndCliente(campanyaFormulario, cliente));

        return camposUI;
    }

    private List<CampoUI> getCamposPrincipalesByFormularioAndCliente(CampanyaFormulario campanyaFormulario, Cliente cliente) {
        List<CampoUI> camposUI = new ArrayList<CampoUI>();


        if (!campanyaFormulario.getSolicitarIdentificacion().equals(TipoSolicitudCampo.NO_SOLICITAR.getId()))
        {

            CampoUI campoTipoIdentificacionUI = new CampoUI(TipoDato.ITEMS_SELECCION_UNICA_DESPL.getId(), "tipoIdentificacion", cliente.getClienteGeneral().getTipoIdentificacion().getId().toString());

            if (campanyaFormulario.getSolicitarIdentificacion().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId()))
            {
                campoTipoIdentificacionUI.setReadOnly(true);
            }

            camposUI.add(campoTipoIdentificacionUI);

            CampoUI campoUI = new CampoUI(TipoDato.TEXTO.getId(),
                    "identificacion", cliente.getClienteGeneral().getIdentificacion());
            if (campanyaFormulario.getSolicitarIdentificacion().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId()))
            {
                campoUI.setReadOnly(true);
            }
            camposUI.add(campoUI);
        }

        if (!campanyaFormulario.getSolicitarNombre().equals(TipoSolicitudCampo.NO_SOLICITAR.getId()))
        {
            CampoUI campoNombreUI = new CampoUI(TipoDato.TEXTO.getId(), "nombreOficial", cliente.getNombre());
            if (campanyaFormulario.getSolicitarNombre().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId()))
            {
                campoNombreUI.setReadOnly(true);
            }
            camposUI.add(campoNombreUI);

            CampoUI campoApellidosUI = new CampoUI(TipoDato.TEXTO.getId(), "apellidosOficial", cliente.getApellidos());
            if (campanyaFormulario.getSolicitarNombre().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId()))
            {
                campoApellidosUI.setReadOnly(true);
            }
            camposUI.add(campoApellidosUI);
        }

        if (!campanyaFormulario.getSolicitarCorreo().equals(TipoSolicitudCampo.NO_SOLICITAR.getId()))
        {
            CampoUI campoUI = new CampoUI(TipoDato.EMAIL.getId(), "correo", cliente.getCorreo());
            if (campanyaFormulario.getSolicitarCorreo().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId()))
            {
                campoUI.setReadOnly(true);
            }
            camposUI.add(campoUI);
        }

        if (!campanyaFormulario.getSolicitarMovil().equals(TipoSolicitudCampo.NO_SOLICITAR.getId()))
        {

            CampoUI campoUI = new CampoUI(TipoDato.TELEFONO.getId(), "movil", cliente.getMovil());
            campoUI.setReadOnly(false);
            camposUI.add(campoUI);

            CampoUI campo2UI = new CampoUI(TipoDato.TEXTO.getId(), "prefijoTelefono", cliente.getPrefijoTelefono());
            if (campanyaFormulario.getSolicitarMovil().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId()))
            {
                campo2UI.setReadOnly(true);
            }
            camposUI.add(campo2UI);

        }

        if (!campanyaFormulario.getSolicitarPostal().equals(TipoSolicitudCampo.NO_SOLICITAR.getId()))
        {
            CampoUI campoUI = new CampoUI(TipoDato.TEXTO_LARGO.getId(), "postal", cliente.getPostal());
            if (campanyaFormulario.getSolicitarPostal().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId()))
            {
                campoUI.setReadOnly(true);
            }
            camposUI.add(campoUI);
        }

        return camposUI;
    }

    private List<CampoUI> getCamposAdicionalesByFormularioAndCliente(CampanyaFormulario campanyaFormulario, Cliente cliente)
            throws
            ParseException {

        List<CampoUI> camposUI = new ArrayList<CampoUI>();

        List<CampanyaDato> campanyaDatos = campanyaDatoService.getCampanyaDatosPrincipalesByFormularioId(campanyaFormulario.getId());

        for (CampanyaDato campanyaDato : campanyaDatos)
        {

            if (ParamUtils.isNotNull(campanyaDato.getGrupo()))
            {

                List<ClienteItem> clienteItem = clienteItemService.getItemsByGrupoIdAndClienteId(campanyaDato.getGrupo().getId(), cliente.getId());

                if (!clienteItem.isEmpty())
                {

                    CampoUI campoUI = getCampoUIDesdeCampanyaDato(clienteItem, campanyaDato);

                    if (campanyaDato.getTipoAcceso().getId().equals(TipoAccesoDatoUsuario.LECTURA.getId()) || campanyaDato.getTipoAcceso().getId().equals(TipoAccesoDatoUsuario.INTERNO.getId()))
                    {
                        campoUI.setReadOnly(Boolean.TRUE);
                    }

                    if (campanyaDato.getClienteDatoTipo().getClienteDatoTipoTipo().getId().equals(TipoDato.ITEMS_SELECCION_UNICA_CHECKS.getId()))
                    {
                        campoUI.setModel("button");
                        campoUI.setClienteDatoId(clienteItem.get(0).getId());
                    }
                    else if (campanyaDato.getClienteDatoTipo().getClienteDatoTipoTipo().getId().equals(TipoDato.ITEMS_SELECCION_MULTIPLE_CHECKS.getId()))
                    {
                        campoUI.setModel("checked");
                    }
                    camposUI.add(campoUI);

                    for (CampanyaDato campanyaDatoAsociado : campanyaDato.getCampanyaDatos())
                    {
                        ClienteDato clienteDatoAsociado = clienteDatoService.getClienteDatoByClienteAndTipoIdAndItem(cliente, campanyaDatoAsociado.getClienteDatoTipo().getId(), clienteItem.get(0).getItem());
                        if (ParamUtils.isNotNull(clienteDatoAsociado))
                        {
                            CampoUI campoUIAsociado = getCampoUIDesdeCampanyaDato(clienteDatoAsociado, campanyaDatoAsociado);
                            campoUIAsociado.setClienteDatoId(clienteDatoAsociado.getId());
                            camposUI.add(campoUIAsociado);
                        }
                    }
                }
            }
            if (campanyaDato.getVinculadoCampanya())
            {

                ClienteDato clienteDato = clienteDatoService.getClienteDatoByClienteAndTipoIdVinculadoCampanya(cliente, campanyaDato.getClienteDatoTipo().getId(), campanyaDato.getCampanyaFormulario().getCampanya().getId());
                if (ParamUtils.isNotNull(clienteDato))
                {
                    CampoUI campoUI = getCampoUIDesdeCampanyaDato(clienteDato, campanyaDato);

                    if (campanyaDato.getTipoAcceso().getId().equals(TipoAccesoDatoUsuario.LECTURA.getId()) || campanyaDato.getTipoAcceso().getId().equals(TipoAccesoDatoUsuario.INTERNO.getId()))
                    {
                        campoUI.setReadOnly(Boolean.TRUE);
                    }
                    campoUI.setClienteDatoId(clienteDato.getId());
                    camposUI.add(campoUI);
                    for (CampanyaDato campanyaDatoAsociado : campanyaDato.getCampanyaDatos())
                    {
                        ClienteDato clienteDatoAsociado = clienteDatoService.getClienteDatoByClienteAndTipoIdAndClienteDato(cliente, campanyaDatoAsociado.getClienteDatoTipo().getId(), clienteDato.getId());
                        if (ParamUtils.isNotNull(clienteDatoAsociado))
                        {
                            CampoUI campoUIAsociado = getCampoUIDesdeCampanyaDato(clienteDatoAsociado, campanyaDatoAsociado);
                            camposUI.add(campoUIAsociado);
                        }
                    }
                }
            }
            else
            {

                ClienteDato clienteDato = clienteDatoService.getClienteDatoByClienteAndTipoId(cliente, campanyaDato.getClienteDatoTipo().getId());
                if (ParamUtils.isNotNull(clienteDato))
                {
                    CampoUI campoUI = getCampoUIDesdeCampanyaDato(clienteDato, campanyaDato);

                    if (campanyaDato.getTipoAcceso().getId().equals(TipoAccesoDatoUsuario.LECTURA.getId()) || campanyaDato.getTipoAcceso().getId().equals(TipoAccesoDatoUsuario.INTERNO.getId()))
                    {
                        campoUI.setReadOnly(Boolean.TRUE);
                    }
                    campoUI.setClienteDatoId(clienteDato.getId());

                    camposUI.add(campoUI);
                    for (CampanyaDato campanyaDatoAsociado : campanyaDato.getCampanyaDatos())
                    {
                        ClienteDato clienteDatoAsociado = clienteDatoService.getClienteDatoByClienteAndTipoIdAndClienteDato(cliente, campanyaDatoAsociado.getClienteDatoTipo().getId(), clienteDato.getId());
                        if (ParamUtils.isNotNull(clienteDatoAsociado))
                        {
                            CampoUI campoUIAsociado = getCampoUIDesdeCampanyaDato(clienteDatoAsociado, campanyaDatoAsociado);
                            campoUIAsociado.setClienteDatoId(clienteDatoAsociado.getId());
                            camposUI.add(campoUIAsociado);
                        }
                    }
                }
            }
        }

        return camposUI;
    }

    private CampoUI getCampoUIDesdeCampanyaDato(ClienteDato clienteDato, CampanyaDato campanyaDato)
            throws ParseException {


        if (ParamUtils.isNotNull(clienteDato))
        {
            if (clienteDato.getClienteDatoTipo().getClienteDatoTipoTipo().getNombre().substring(0, 1).equals("F"))
            {
                CampoUI campoUI = new CampoUI(clienteDato.getClienteDatoTipo().getClienteDatoTipoTipo().getId(),
                        getNombreCampoDatoExtra(campanyaDato.getId()), clienteDato.getValorSegunTipo().substring(0, 10));

                return campoUI;

            }
            else
            {
                CampoUI campoUI = new CampoUI(clienteDato.getClienteDatoTipo().getClienteDatoTipoTipo().getId(),
                        getNombreCampoDatoExtra(campanyaDato.getId()), clienteDato.getValorSegunTipo());

                return campoUI;
            }
        }
        return null;

    }

    private CampoUI getCampoUIDesdeCampanyaDato(List<ClienteItem> clienteItems, CampanyaDato campanyaDato)
            throws ParseException {

        String valores = "";
        Integer count = 0;

        for (ClienteItem clienteItem : clienteItems)
        {

            if (count == 0)
            {
                valores = clienteItem.getItem().getId().toString();
                count = count + 1;
            }
            else
            {
                valores = valores + "," + clienteItem.getItem().getId().toString();
            }

        }

        CampoUI campoUI = new CampoUI(clienteItems.get(0).getId(),
                getNombreCampoDatoExtra(campanyaDato.getId()), valores);
        campoUI.setModel("chosen");

        return campoUI;

    }

    public String getToken(String mensaje) throws NoSuchAlgorithmException, InvalidKeyException,
            UnsupportedEncodingException {
        Mac mac = Mac.getInstance("hmacSHA256");
        SecretKeySpec secret = new SecretKeySpec(privateKey.getBytes("US-ASCII"),
                mac.getAlgorithm());
        mac.init(secret);
        return new String(Base64.encodeBase64(mac.doFinal(mensaje.getBytes("US-ASCII"))));
    }
}