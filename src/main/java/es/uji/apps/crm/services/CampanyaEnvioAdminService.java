package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaEnvioAdminDAO;
import es.uji.apps.crm.model.CampanyaEnvioAdmin;

@Service
public class CampanyaEnvioAdminService {
    @Autowired
    private CampanyaEnvioAdminDAO campanyaEnvioAdminDAO;

    public List<CampanyaEnvioAdmin> getCampanyaEnviosAdminByCampanyaId(Long campanyaId) {
        return campanyaEnvioAdminDAO.getCampanyaEnviosAdminByCampanyaId(campanyaId);
    }

    public CampanyaEnvioAdmin addCampanyaEnvioAdmin(CampanyaEnvioAdmin campanyaEnvioAdmin) {
        return campanyaEnvioAdminDAO.insert(campanyaEnvioAdmin);
    }

    public void updateCampanyaEnvioAdmin(CampanyaEnvioAdmin campanyaEnvioAdmin) {
        campanyaEnvioAdminDAO.update(campanyaEnvioAdmin);
    }

    public void delete(Long campanyaEnvioAdminId) {
        campanyaEnvioAdminDAO.delete(CampanyaEnvioAdmin.class, campanyaEnvioAdminId);
    }

}