package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.ItemClasificacion;
import es.uji.apps.crm.services.ItemClasificacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("itemclasificacion")
public class ItemClasificacionResource extends CoreBaseService {
    @InjectParam
    private ItemClasificacionService itemClasificacionService;

    @GET
    @Path("clasificacion/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItemClasificacionByClasificacionId(@PathParam("id") Long clasificacionId) {
        return modelToUI(itemClasificacionService.getItemClasificacionByClasificacionId(clasificacionId));
    }

    private List<UIEntity> modelToUI(List<ItemClasificacion> itemsClasificacion) {
        List<UIEntity> lista = new ArrayList<>();

        for (ItemClasificacion itemClasificacion : itemsClasificacion)
        {
            lista.add(modelToUI(itemClasificacion));
        }
        return lista;

    }

    private UIEntity modelToUI(ItemClasificacion itemClasificacion) {
        UIEntity entity = UIEntity.toUI(itemClasificacion);
        entity.put("grupoId", itemClasificacion.getItem().getGrupo().getId());
        entity.put("itemNombre", itemClasificacion.getItem().getNombreCa());
        return entity;
    }

//    private ItemItem UIToModel(UIEntity entity)
//    {
//        ItemItem itemItem = entity.toModel(ItemItem.class);
//
//        Item itemOrigen = new Item();
//        itemOrigen.setId(ParamUtils.parseLong(entity.get("itemIdOrigen")));
//        itemItem.setItemOrigen(itemOrigen);
//
//        Item itemDestino = new Item();
//        itemDestino.setId(ParamUtils.parseLong(entity.get("itemIdDestino")));
//        itemItem.setItemDestino(itemDestino);
//
//        return itemItem;
//    }

    @POST
    @Path("clasificacion/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity addItemClasificacion(@PathParam("id") Long clasificacionId, @FormParam("grupoId") Long grupoId, @FormParam("itemId") Long itemId) {
        return modelToUI(itemClasificacionService.addItemClasificacion(clasificacionId, grupoId, itemId));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long itemClasificacionId) {
        itemClasificacionService.deleteItemClasificacion(itemClasificacionId);
    }
}