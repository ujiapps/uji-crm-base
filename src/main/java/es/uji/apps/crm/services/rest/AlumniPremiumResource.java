package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import es.uji.apps.crm.services.AlumniPremiumService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;

public class AlumniPremiumResource extends CoreBaseService {

    @Path("aviso")
    public AlumniAvisoResource getPlatformItem(
            @InjectParam AlumniAvisoResource alumniAvisoResource) {
        return alumniAvisoResource;
    }

    @InjectParam
    private AlumniPremiumService alumniPremiumService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template getAcceso(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {
        return alumniPremiumService.getTemplateAcceso(idioma, request.getPathInfo());
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    public Template postAcceso(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                               @FormParam("tipoIdentificacion") Long tipoIdentificacionId,
                               @FormParam("identificacion") String identificacion,
                               @FormParam("nombre") String nombre,
                               @FormParam("apellidos") String apellidos,
                               @FormParam("correo") String correo,
                               @FormParam("fechaNacimiento") String fechaNacimiento
    ) throws ParseException {

        Date fechaNacimientoDate = UtilsService.fechaParse(fechaNacimiento, "yyyy-MM-dd");
        return alumniPremiumService.realizarEnvioDireccionFormularioPremium(idioma, tipoIdentificacionId, identificacion, nombre, apellidos, correo, fechaNacimientoDate);
    }

    @GET
    @Path("alta/{hash}")
    @Produces(MediaType.TEXT_HTML)
    public Template getFormularioAlta(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                      @PathParam("hash") String hash) throws
            ParseException {

        return alumniPremiumService.getFormularioAlta(idioma, hash, request.getPathInfo());
    }

    @POST
    @Path("alta/{hash}")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response postAltaPremium(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                    @PathParam("hash") String hash,
                                    @FormDataParam("nombre") String nombre,
                                    @FormDataParam("apellidos") String apellidos,
                                    @FormDataParam("fechaNacimiento") String fechaNacimiento,
                                    @FormDataParam("nacionalidad") String nacionalidad,
                                    @FormDataParam("telefono") String telefono,
                                    @FormDataParam("paisPostal") String paisPostal,
                                    @FormDataParam("provinciaPostal") Long provinciaPostal,
                                    @FormDataParam("poblacionPostal") Long poblacionPostal,
                                    @FormDataParam("codigoPostal") String codigoPostal,
                                    @FormDataParam("postal") String postal,
                                    @FormDataParam("nivelEstudios") Long nivelEstudios,
                                    @FormDataParam("nombreEstudio") String nombreEstudio,
                                    @FormDataParam("estudiosSuperiores") Long estudiosSuperiores,
                                    @FormDataParam("estudioUJINoUJI") Long estudioUJINoUJI,
                                    @FormDataParam("tipoEstudioUJINoUJI") String tipoEstudioUJINoUJI,
                                    @FormDataParam("listaEstudiosGrado") Long listaEstudiosGrado,
                                    @FormDataParam("nombreEstudioNoGrado") String nombreEstudioNoGrado,
                                    @FormDataParam("universidadEstudio") Long universidadEstudio,
                                    @FormDataParam("anyoFinalizacionEstudio") Long anyoFinalizacionEstudio,
                                    @FormDataParam("laboral") Long laboral,
                                    @FormDataParam("sectorEmpresarial") Long sectorEmpresarial,
                                    @FormDataParam("tipoTrabajo") String tipoTrabajo,
                                    @FormDataParam("lugarEstudiante") String lugarEstudiante,
                                    @FormDataParam("perfilLinkedin") String perfilLinkedin,
                                    @FormDataParam("cuentaBanco") String cuentaBanco,
                                    @FormDataParam("conoce") Long conoce,
                                    @FormDataParam("foto") InputStream foto,
                                    @FormDataParam("foto") FormDataContentDisposition fotoDetalle,
                                    @FormDataParam("doc") InputStream doc,
                                    @FormDataParam("doc") FormDataContentDisposition docDetalle
    ) throws ParseException, SQLException, IOException {

        return alumniPremiumService.guardarDatosFormularioPremium(hash, nombre, apellidos, UtilsService.fechaParse(fechaNacimiento), nacionalidad, telefono,
                paisPostal, provinciaPostal, poblacionPostal, codigoPostal, postal, nivelEstudios, nombreEstudio, estudiosSuperiores, estudioUJINoUJI, tipoEstudioUJINoUJI,
                listaEstudiosGrado, nombreEstudioNoGrado, universidadEstudio, anyoFinalizacionEstudio, laboral, sectorEmpresarial, tipoTrabajo, lugarEstudiante, perfilLinkedin,
                cuentaBanco, conoce, foto, fotoDetalle, doc, docDetalle);
    }
}