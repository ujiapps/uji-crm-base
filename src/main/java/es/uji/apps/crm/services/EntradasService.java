package es.uji.apps.crm.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.core.util.MultivaluedMapImpl;
import es.uji.apps.crm.model.*;
import es.uji.apps.crm.model.domains.TipoCorreoEnvio;
import es.uji.apps.crm.model.domains.TipoDato;
import es.uji.apps.crm.utils.Csv;
import es.uji.commons.rest.ResponseMessage;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.dao.VwEntradaCrmDAO;
import es.uji.apps.crm.model.domains.TipoAccesoDatoUsuario;
import es.uji.apps.crm.model.domains.TipoClienteDato;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;

@Service
public class EntradasService extends CoreBaseService {

    @Value("${uji.par.apiToken}")
    private String apiToken;
    @Value("${uji.par.url}")
    private String url;

    private final VwEntradaCrmDAO vwEntradaCrmDAO;
    private final CampanyaActoService campanyaActoService;
    private final ClienteDatoOpcionService clienteDatoOpcionService;
    private final ClienteDatoTipoService clienteDatoTipoService;
    private final ClienteDatoService clienteDatoService;
    private final CampanyaActoEnvioService campanyaActoEnvioService;
    private final ClienteEstudioService clienteEstudioService;
    private final ClienteService clienteService;

    @InjectParam
    private EnvioService envioService;
    @InjectParam
    private EnvioClienteService envioClienteService;

    @InjectParam
    Csv csv;


    @Autowired
    public EntradasService(VwEntradaCrmDAO vwEntradaCrmDAO, CampanyaActoService campanyaActoService, ClienteDatoOpcionService clienteDatoOpcionService, ClienteDatoService clienteDatoService,
                           ClienteDatoTipoService clienteDatoTipoService, CampanyaActoEnvioService campanyaActoEnvioService,
                           ClienteEstudioService clienteEstudioService, ClienteService clienteService) {
        this.vwEntradaCrmDAO = vwEntradaCrmDAO;
        this.campanyaActoService = campanyaActoService;
        this.clienteDatoOpcionService = clienteDatoOpcionService;
        this.clienteDatoService = clienteDatoService;
        this.clienteDatoTipoService = clienteDatoTipoService;
        this.campanyaActoEnvioService = campanyaActoEnvioService;
        this.clienteEstudioService = clienteEstudioService;
        this.clienteService = clienteService;
    }

    public String obtenerEntradas(Campanya campanya, Cliente cliente, Long numEntradas) throws JSONException {
        List<CampanyaActo> campanyasActo = campanyaActoService.getCampanyasActoByCampanya(campanya);
        String correo = envioClienteService.dameCorreoCliente(cliente, TipoCorreoEnvio.OFICIAL.getId());

        MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
        queryParams.add("nombre", cliente.getNombre());
        queryParams.add("apellidos", cliente.getApellidos());
        queryParams.add("email", correo);
        queryParams.add("entradas", String.valueOf(numEntradas));

        ClientResponse response = Client.create().resource(url + campanyasActo.get(0).getId() + "/gettickets")
                .queryParams(queryParams)
                .type(MediaType.APPLICATION_JSON)
                .header("x-api-key", apiToken)
                .post(ClientResponse.class);

        if (response.getStatus() == 200) {

            JSONObject responseJson = response.getEntity(JSONObject.class);
            return responseJson.getString("message");
        }
        return null;
    }


    public void anyadeDatoExtraEntradas(Cliente cliente, CampanyaDato campanyaDato, ClienteDato clienteDato) throws JSONException, ParseException {

        ClienteDatoTipo clienteDatoTipo = clienteDatoTipoService.getTipoDatoEntradasByFormularioId(campanyaDato.getCampanyaFormulario().getId());

        if (ParamUtils.isNotNull(clienteDatoTipo)) {
            if (campanyaDato.getClienteDatoTipo().getId().equals(clienteDatoTipo.getId())) {

                Long numEntradas = clienteDatoOpcionService.getClienteDatoOpcionById(ParamUtils.parseLong(clienteDato.getValorSegunTipo())).getValor();
                ClienteDato clienteEntradas = clienteDatoService.getClienteDatoByClienteAndTipoIdVinculadoCampanya(cliente, TipoClienteDato.WEB.getId(), campanyaDato.getCampanyaFormulario().getCampanya().getId());

                if (!ParamUtils.isNotNull(clienteEntradas)) {
                    String direccion = obtenerEntradas(campanyaDato.getCampanyaFormulario().getCampanya(), cliente, numEntradas);

                    if (ParamUtils.isNotNull(direccion)) {
                        ClienteDato entradas = new ClienteDato();
                        entradas.setCampanya(campanyaDato.getCampanyaFormulario().getCampanya());
                        entradas.setCliente(cliente);
                        entradas.setClienteDatoId(clienteDato.getId());

                        ClienteDatoTipo clienteDatoTipoEntradas = clienteDatoTipoService.getClienteDatoTipoById(TipoClienteDato.WEB.getId());
                        entradas.setClienteDatoTipo(clienteDatoTipoEntradas);

                        Tipo tipo = new Tipo();
                        tipo.setId(TipoAccesoDatoUsuario.INTERNO.getId());
                        entradas.setClienteDatoTipoAcceso(tipo);

                        clienteDatoService.addClienteDatoExtra(entradas, direccion, null);
                    }
                }
            }
        }
    }

    public List<VwEntradaCrm> obtenerInfoEntradasCrm(Long actoId) {

        return vwEntradaCrmDAO.obtenerInfoEntradasCrm(actoId);
    }

    public void asignarEntradasNuevas(Long actoId) throws ParseException, JSONException {

        List<VwEntradaCrm> lista = vwEntradaCrmDAO.obtenerInfoEntradasCrm(actoId);
        CampanyaActoEnvio campanyaActoEnvio = campanyaActoEnvioService.getCampanyaActoEnvioByCampanyaActoId(actoId);

        for (VwEntradaCrm entradaCrm : lista) {


            if (ParamUtils.isNotNull(entradaCrm.getNuevas()) && entradaCrm.getNuevas() > 0) {
                if (!ParamUtils.isNotNull(entradaCrm.getDadas()) || !entradaCrm.getDadas()) {
                    String direccion = obtenerEntradas(entradaCrm.getActo().getCampanya(), entradaCrm.getCliente(), entradaCrm.getNuevas());

                    if (ParamUtils.isNotNull(direccion)) {

                        ClienteDato entradas = new ClienteDato();
                        entradas.setCampanya(entradaCrm.getActo().getCampanya());
                        entradas.setCliente(entradaCrm.getCliente());
                        entradas.setClienteDatoId(entradaCrm.getNuevasId());

                        ClienteDatoTipo clienteDatoTipoEntradas = new ClienteDatoTipo();
                        clienteDatoTipoEntradas.setId(TipoClienteDato.WEB.getId());
                        Tipo texto = new Tipo();
                        texto.setId(TipoDato.TEXTO.getId());
                        texto.setNombre(TipoDato.TEXTO.getNombre());
                        clienteDatoTipoEntradas.setClienteDatoTipoTipo(texto);
                        entradas.setClienteDatoTipo(clienteDatoTipoEntradas);

                        Tipo tipo = new Tipo();
                        tipo.setId(TipoAccesoDatoUsuario.INTERNO.getId());
                        entradas.setClienteDatoTipoAcceso(tipo);

                        clienteDatoService.addClienteDatoExtra(entradas, direccion, null);
                        entradaCrm.setDadas(Boolean.TRUE);

                        ClienteDato darEntrada = new ClienteDato();

                        darEntrada.setCliente(entradas.getCliente());

                        ClienteDatoTipo tipoEntradaDada = new ClienteDatoTipo();
                        tipoEntradaDada.setId(27L);

                        Tipo numero = new Tipo();
                        numero.setId(TipoDato.NUMERO.getId());
                        numero.setNombre(TipoDato.NUMERO.getNombre());
                        tipoEntradaDada.setClienteDatoTipoTipo(numero);

                        darEntrada.setClienteDatoTipo(tipoEntradaDada);

                        darEntrada.setClienteDatoTipoAcceso(tipo);
                        darEntrada.setCampanya(entradaCrm.getActo().getCampanya());
                        darEntrada.setClienteDatoId(entradaCrm.getNuevasId());

                        clienteDatoService.addClienteDatoExtra(darEntrada, "1", null);

                        envioService.creaCorreoEntradasNuevas(direccion, entradaCrm.getCliente(), campanyaActoEnvio);

                    }
                }
            }
        }
    }

    public VwEntradaCrm getEntradasSolicitadas(Cliente cliente, CampanyaActo acto) {
        return vwEntradaCrmDAO.getEntradasSolicitadas(cliente, acto);
    }

    public String entradasToCSV(List<VwEntradaCrm> lista) {
        ArrayList<String> cabeceras = new ArrayList<>(Arrays.asList("Usuari", "Correu", "Sol·licitades", "Sol·licitades Extra", "Nuevas", "Titulació", "Fecha Alta"));

        List<List<String>> records = new ArrayList<>();
        for (VwEntradaCrm entradaCrm : lista) {
            List<String> recordMov = new ArrayList<>(creaListaString(entradaCrm));
            records.add(recordMov);
        }

        return csv.listToCSV(cabeceras, records);
    }

    private List<String> creaListaString(VwEntradaCrm entradaCrm) {

        String estudios = "";
        List<ClienteEstudio> listaEstudios = clienteEstudioService.getClienteEstudiosByPersona(entradaCrm.getCliente().getClienteGeneral().getPersona().getId());
        for (ClienteEstudio clienteEstudiosUJI : listaEstudios) {
            if (ParamUtils.isNotNull(estudios))
                estudios = estudios + ", " + clienteEstudiosUJI.getEstudio() + "(" + clienteEstudiosUJI.getCursoAcaFin() + ")";
            else
                estudios = clienteEstudiosUJI.getEstudio() + "(" + clienteEstudiosUJI.getCursoAcaFin() + ")";
        }
        return Arrays.asList(
                entradaCrm.getPersona(),
                entradaCrm.getCorreo(),
                (ParamUtils.isNotNull(entradaCrm.getEntradas())) ? entradaCrm.getEntradas().toString() : "0",
                (ParamUtils.isNotNull(entradaCrm.getSobrantes())) ? entradaCrm.getSobrantes().toString() : "0",
                (ParamUtils.isNotNull(entradaCrm.getNuevas())) ? entradaCrm.getNuevas().toString() : "0",
                estudios,
                (ParamUtils.isNotNull(entradaCrm.getFechaMovimiento())) ? entradaCrm.getFechaMovimiento().toString() : ""
        );
    }

    public ResponseMessage asignarEntradasNuevaAUsuario(Long campanyaActoId, Long clienteId, Long num, String desde, String responder, String asunto, String cuerpo) throws ParseException {

        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(false);

        CampanyaActo campanyaActo = campanyaActoService.getCampanyasActoById(campanyaActoId);
        Cliente cliente = clienteService.getClienteById(clienteId);
        String direccion;
        try {
            ClienteDatoOpcion opcion = clienteDatoOpcionService.getClienteDatoOpcionById(num);
            direccion = obtenerEntradas(campanyaActo.getCampanya(), cliente, opcion.getValor());

        } catch (Exception e) {
            responseMessage.setMessage("Error en obtindre les entrades");
            return responseMessage;
        }
        if (ParamUtils.isNotNull(direccion)) {

            ClienteDato numEntradas = new ClienteDato();
            numEntradas.setValorNumero(num);

            numEntradas.setCampanya(campanyaActo.getCampanya());
            numEntradas.setCliente(cliente);

            ClienteDatoTipo clienteDatoTipoEntradas = new ClienteDatoTipo();
            clienteDatoTipoEntradas.setId(TipoClienteDato.NUMENTRADAS.getId());
            Tipo select = new Tipo();
            select.setId(TipoDato.SELECCION.getId());
            select.setNombre(TipoDato.SELECCION.getNombre());
            clienteDatoTipoEntradas.setClienteDatoTipoTipo(select);
            numEntradas.setClienteDatoTipo(clienteDatoTipoEntradas);

            Tipo tipo = new Tipo();
            tipo.setId(TipoAccesoDatoUsuario.INTERNO.getId());
            numEntradas.setClienteDatoTipoAcceso(tipo);
            clienteDatoService.addClienteDatoExtra(numEntradas, num.toString(), null);

            ClienteDato entradas = new ClienteDato();

            entradas.setCampanya(campanyaActo.getCampanya());
            entradas.setCliente(cliente);
            entradas.setClienteDatoId(numEntradas.getId());

            ClienteDatoTipo clienteDatoTipoWeb = new ClienteDatoTipo();
            clienteDatoTipoWeb.setId(TipoClienteDato.WEB.getId());
            Tipo texto = new Tipo();
            texto.setId(TipoDato.TEXTO.getId());
            texto.setNombre(TipoDato.TEXTO.getNombre());
            clienteDatoTipoWeb.setClienteDatoTipoTipo(texto);
            entradas.setClienteDatoTipo(clienteDatoTipoWeb);

            entradas.setClienteDatoTipoAcceso(tipo);

            clienteDatoService.addClienteDatoExtra(entradas, direccion, null);

            CampanyaActoEnvio campanyaActoEnvio = new CampanyaActoEnvio();
            campanyaActoEnvio.setResponder(responder);
            campanyaActoEnvio.setDesde(desde);
            campanyaActoEnvio.setCuerpo(cuerpo);
            campanyaActoEnvio.setAsunto(asunto);
            campanyaActoEnvio.setCampanyaActo(campanyaActo);

            envioService.creaCorreoEntradasNuevas(direccion, cliente, campanyaActoEnvio);

            responseMessage.setSuccess(true);

        }
        return responseMessage;
    }
}
