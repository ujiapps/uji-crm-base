package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.crm.model.domains.TipoAccesoProgramaPerfil;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("tipoaccesoprogramaperfil")
public class TipoAccesoProgramaPerfilResource extends CoreBaseService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposAccesoProgramaPerfil() {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (TipoAccesoProgramaPerfil tipoAccesoProgramaPerfil : TipoAccesoProgramaPerfil.values())
        {
            UIEntity entity = new UIEntity();
            entity.put("nombre", tipoAccesoProgramaPerfil.toString());
            listaUI.add(entity);
        }
        return listaUI;
    }

}