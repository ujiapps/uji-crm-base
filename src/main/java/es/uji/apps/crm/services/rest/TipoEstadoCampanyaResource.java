package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.apps.crm.services.TipoEstadoCampanyaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("tipoestadocampanya")
public class TipoEstadoCampanyaResource extends CoreBaseService {

    @InjectParam
    private TipoEstadoCampanyaService tipoEstadoCampanyaService;

    @GET
    @Path("campanya")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposEstadoCampanyaByCampanyaId(@QueryParam("campanyaId") Long campanyaId) {
        return UIEntity.toUI(tipoEstadoCampanyaService.getTiposEstadoCampanyaByCampanyaId(campanyaId));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addTipoEstadoCampanyaByCampanyaId(UIEntity entity) {

        return UIEntity.toUI(tipoEstadoCampanyaService.addTipoEstadoCampanyaByCampanyaId(UIToModel(entity)));
    }

    private TipoEstadoCampanya UIToModel(UIEntity entity) {
        TipoEstadoCampanya tipoEstadoCampanya = entity.toModel(TipoEstadoCampanya.class);

        Campanya campanya = new Campanya();
        campanya.setId(ParamUtils.parseLong(entity.get("campanyaId")));
        tipoEstadoCampanya.setCampanya(campanya);

        return tipoEstadoCampanya;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateTipoEstadoCampanyaByCampanyaId(UIEntity entity) {
        return UIEntity.toUI(tipoEstadoCampanyaService.updateTipoEstadoCampanyaByCampanyaId(UIToModel(entity)));
    }

    @DELETE
    @Path("{id}")
    public void deleteTipoEstadoCampanya(@PathParam("id") Long tipoEstadoCampanyaId) {
        tipoEstadoCampanyaService.deleteTipoEstadoCampanya(tipoEstadoCampanyaId);
    }
}