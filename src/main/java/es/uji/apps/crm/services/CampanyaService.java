package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaDAO;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.LineaFacturacion;
import es.uji.apps.crm.model.Programa;
import es.uji.apps.crm.model.Seguimiento;
import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.apps.crm.model.domains.TipoAfiliacionUsuarioCampanya;

@Service
public class CampanyaService {
    private CampanyaDAO campanyaDAO;
    private CampanyaTextoService campanyaTextoService;
    private SeguimientoService seguimientoService;
    private ProgramaService programaService;
    private TipoEstadoCampanyaService tipoEstadoCampanyaService;
    private CampanyaActoService campanyaActoService;
    private CampanyaFormularioService campanyaFormularioService;
    private CampanyaEnvioAutoService campanyaEnvioAutoService;
    private TipoTarifaService tipoTarifaService;
    private LineaFacturacionService lineaFacturacionService;

    @Autowired
    public CampanyaService(CampanyaDAO campanyaDAO, CampanyaTextoService campanyaTextoService, SeguimientoService seguimientoService, ProgramaService programaService, TipoEstadoCampanyaService tipoEstadoCampanyaService,
                           CampanyaActoService campanyaActoService, CampanyaFormularioService campanyaFormularioService, CampanyaEnvioAutoService campanyaEnvioAutoService,
                           TipoTarifaService tipoTarifaService, LineaFacturacionService lineaFacturacionService) {
        this.campanyaDAO = campanyaDAO;
        this.campanyaTextoService = campanyaTextoService;
        this.seguimientoService = seguimientoService;
        this.programaService = programaService;
        this.tipoEstadoCampanyaService = tipoEstadoCampanyaService;
        this.campanyaActoService = campanyaActoService;
        this.campanyaFormularioService = campanyaFormularioService;
        this.campanyaEnvioAutoService = campanyaEnvioAutoService;
        this.tipoTarifaService = tipoTarifaService;
        this.lineaFacturacionService = lineaFacturacionService;
    }

    public List<Campanya> getCampanyas() {
        return campanyaDAO.getCampanyas();
    }

    public List<Campanya> getCampanyasAdmin(Long connectedUserId) {
        return campanyaDAO.getCampanyasAdmin(connectedUserId);
    }

    public List<Campanya> getCampanyasAdminTree(Long node, Long connectedUserId) {
        if (node.equals(0L))
        {
            return campanyaDAO.getCampanyasAdminRoot(connectedUserId);
        }
        else
        {
            Campanya campanya = new Campanya();
            campanya.setId(node);
            return campanyaDAO.getCampanyasAdminHijas(connectedUserId, campanya);
        }
    }

    public List<Campanya> getCampanyasUser(Long connectedUserId) {
        return campanyaDAO.getCampanyasUser(connectedUserId);
    }

    public List<Campanya> getCampanyasByServicio(Long connectedUserId, Long servicioId) {
        return campanyaDAO.getCampanyasByServicio(connectedUserId, servicioId);
    }

    public Campanya getCampanyaById(Long campanyaId) {
        return campanyaDAO.getCampanyaById(campanyaId);
    }

    public void updateCampanya(Campanya campanya) {
        campanyaDAO.update(campanya);
    }

    public void deleteCampanya(Long campanyaId) {
        List<Seguimiento> seguimientosCampanya = seguimientoService
                .getSeguimientosByCampanyaId(campanyaId);
        for (Seguimiento seguimiento : seguimientosCampanya)
        {
            seguimientoService.deleteSeguimiento(seguimiento.getId());
        }
        campanyaDAO.delete(Campanya.class, campanyaId);
    }

    public List<Campanya> getCampanyasByClienteId(Long clienteId) {

        return campanyaDAO.getCampanyasByClienteId(clienteId);
    }

    public List<Campanya> getCampanyasByClienteIdAndAccion(Long clienteId, String accion) {
        return campanyaDAO.getCampanyasByClienteIdAndAccion(clienteId, accion);
    }

    private List<Campanya> getCampanyasHijas(Long connectedUserId, Campanya campanya) {
        return campanyaDAO.getCampanyasHijas(connectedUserId, campanya);
    }

    public Campanya getCampanyasByActoId(Long actoId) {

        return campanyaDAO.getCampanyaByActoId(actoId);
    }

    public List<Campanya> getCampanyasUserConTarifa(Long connectedUserId) {
        return campanyaDAO.getCampanyasUserConTarifa(connectedUserId);
    }

    public TipoEstadoCampanya getEstadoCampanyaByCampanya(Campanya campanya, TipoAfiliacionUsuarioCampanya baja) {
        return campanyaDAO.getEstadoCampanyaByCampanya(campanya, baja);
    }

    public List<Campanya> getCampanyasByClienteIdAndCampanyaPadreId(Long clienteId, Long campanyaPadreId) {
        return campanyaDAO.getCampanyasByClienteIdAndCampanyaPadreId(clienteId, campanyaPadreId);
    }

    public Campanya addCampanyaGenericaActo(Long connectedUserId) {

        Campanya campanya = new Campanya();
        campanya.setNombre("MODIFICAR -- Campanya Generica");
        campanya.setDescripcion("MODIFICAR -- Descrició de la campanya");
        campanya.setFideliza(Boolean.FALSE);

        Programa programa = programaService.getProgramasAdmin(connectedUserId).get(0);
        campanya.setPrograma(programa);

        campanya.setCampanyaTexto(campanyaTextoService.getCampanyaTextoByCampanyaCodigo("UJI"));

        addCampanya(campanya);

        tipoEstadoCampanyaService.addTiposEstadoCampanyaGenericaActo(campanya);

        campanyaActoService.addCampanyaActoGenericaActo(campanya);
        campanyaFormularioService.addCampanyaFormulariosGenericaActo(campanya, TipoAfiliacionUsuarioCampanya.DEFINITIVO);
        campanyaEnvioAutoService.addCampanyaEnvioAutoGenericaActo(campanya);

        return campanya;

    }

    public Campanya addCampanya(Campanya campanya) {
        campanyaDAO.insert(campanya);
        campanya.setId(campanya.getId());
        return campanya;
    }

    public Campanya addCampanyaGenericaActoConPago(Long connectedUserId) {

        Campanya campanya = new Campanya();
        campanya.setNombre("MODIFICAR -- Campanya Generica con pago");
        campanya.setDescripcion("MODIFICAR -- Descrició de la campanya");
        campanya.setFideliza(Boolean.FALSE);

        Programa programa = programaService.getProgramasAdmin(connectedUserId).get(0);
        campanya.setPrograma(programa);

        campanya.setCampanyaTexto(campanyaTextoService.getCampanyaTextoByCampanyaCodigo("UJI"));

        LineaFacturacion lineaFacturacion = lineaFacturacionService.getLineaFacturacionActoByServicio(programa.getServicio());
        campanya.setLineaFacturacion(lineaFacturacion);

        campanya.setCorreoAvisaCobro("noreply@uji.es");
        addCampanya(campanya);

        tipoEstadoCampanyaService.addTiposEstadoCampanyaGenericaActoConPago(campanya);

        campanyaActoService.addCampanyaActoGenericaActo(campanya);
        campanyaFormularioService.addCampanyaFormulariosGenericaActo(campanya, TipoAfiliacionUsuarioCampanya.PENDIENTE);
        campanyaEnvioAutoService.addCampanyaEnvioAutoGenericaActoConPago(campanya);
        tipoTarifaService.addTipoTarifaGenericaActoConPago(campanya);

        return campanya;

    }

    public boolean tieneHijos(Long campanyaId) {
        return campanyaDAO.tieneHijos(campanyaId);
    }

    public List<Campanya> getCampanyasTree(Long node, Long connectedUserId) {
        if (node.equals(0L))
        {
            return campanyaDAO.getCampanyasRoot(connectedUserId);
        }
        else
        {
            Campanya campanya = new Campanya();
            campanya.setId(node);
            return campanyaDAO.getCampanyasHijas(connectedUserId, campanya);
        }
    }

    // public TreeRow toTreeSinCheckedGrupo(Campanya dato)
    // {
    // TreeRow row = new TreeRow();
    // row.setId(dato.getId().toString());
    // row.setText(dato.getNombre());
    // row.setLeaf("true");
    // return row;
    // }
}