package es.uji.apps.crm.services;

import es.uji.apps.crm.model.ClienteEstudio;
import es.uji.apps.crm.model.ClienteTitulacionesNoUJI;
import es.uji.apps.crm.model.TipoEstudio;
import es.uji.apps.crm.ui.ClienteEstudioUI;
import es.uji.apps.crm.ui.ClienteTitulacionNoUjiUI;
import es.uji.apps.crm.ui.TipoEstudioUI;
import es.uji.apps.crm.ui.UniversidadUI;
import es.uji.apps.crm.utils.AppInfo;
import es.uji.commons.web.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class AlumniAcademicosService {

    private final static Long TIPOESTUDIOOTROS = 5L;
    private final static Long TIPOESTUDIOGRADO = 1L;
    private final static Long TIPOESTUDIOPOST = 2L;
    private final static Long TIPOESTUDIODOC = 3L;
    private final static Long TIPOESTUDIOSUP = 6L;


    private final ClienteEstudioService clienteEstudioService;
    private final TipoEstudioService tipoEstudioService;
    private final UniversidadService universidadService;
    private final AlumniService alumniService;
    private final ClienteTitulacionesNoUJIService clienteTitulacionesNoUJIService;

    @Autowired
    public AlumniAcademicosService(ClienteEstudioService clienteEstudioService,
                                   TipoEstudioService tipoEstudioService,
                                   UniversidadService universidadService, AlumniService alumniService,
                                   ClienteTitulacionesNoUJIService clienteTitulacionesNoUJIService) {
        this.clienteEstudioService = clienteEstudioService;
        this.tipoEstudioService = tipoEstudioService;
        this.universidadService = universidadService;
        this.alumniService = alumniService;
        this.clienteTitulacionesNoUJIService = clienteTitulacionesNoUJIService;
    }

    public Template getTemplateAcademicos(Long connectUserId, String idioma, String path) throws ParseException {

        Template template = alumniService.getTemplateBaseAlumni(path, idioma, "academicos");
        List<ClienteEstudio> estudiosPersona = new ArrayList<>();

        if (connectUserId != -1L) {
            estudiosPersona = clienteEstudioService.getClienteEstudiosByPersona(connectUserId);
        }

        template.put("estudiosPersona", ClienteEstudioUI.toUI(estudiosPersona, idioma));
        List<TipoEstudio> ambitoEstudios = tipoEstudioService.getTipoEstudioByTipo(TIPOESTUDIOOTROS);
        template.put("ambitoEstudios", TipoEstudioUI.toUI(ambitoEstudios, idioma));
        List<TipoEstudio> estudiosSuperiores = tipoEstudioService.getTipoEstudioByTipo(TIPOESTUDIOSUP);
        template.put("estudiosSuperiores", TipoEstudioUI.toUI(estudiosSuperiores, idioma));
        List<TipoEstudio> grados = tipoEstudioService.getTipoEstudioByTipo(TIPOESTUDIOGRADO);
        template.put("grados", TipoEstudioUI.toUI(grados, idioma));
        List<UniversidadUI> universidades = universidadService.getUniversidades(idioma);
        List<TipoEstudio> estudiosPostgrado = tipoEstudioService.getTipoEstudioByTipo(TIPOESTUDIOPOST);
        List<TipoEstudio> estudiosDoctorado = tipoEstudioService.getTipoEstudioByTipo(TIPOESTUDIODOC);
        template.put("universidades", universidades);
        template.put("estudiosPostgrado", TipoEstudioUI.toUI(estudiosPostgrado, idioma));
        template.put("estudiosDoctorado", TipoEstudioUI.toUI(estudiosDoctorado, idioma));
        return template;
    }

    public Template getTemplateAcademicosNoUJI(String path, String idioma, Long clienteId) throws ParseException {
        List<ClienteTitulacionesNoUJI> clienteTitulacionesNoUJI = clienteTitulacionesNoUJIService.getClienteEstudiosNoUJIByPersona(clienteId);

        String pathPlantilla = "crm/" + path.split("/")[1] + "/";

        Template template = AppInfo.buildPagina(pathPlantilla + "estudios-no-uji", idioma);
        Map<String, List<ClienteTitulacionNoUjiUI>> lista = ClienteTitulacionNoUjiUI.toUI(clienteTitulacionesNoUJI, idioma);
        template.put("clienteTitulacionesNoUJI", lista);

        return template;
    }
}
