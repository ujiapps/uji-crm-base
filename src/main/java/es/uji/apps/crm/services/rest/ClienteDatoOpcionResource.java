package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.ClienteDatoOpcion;
import es.uji.apps.crm.model.ClienteDatoTipo;
import es.uji.apps.crm.services.ClienteDatoOpcionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("clientedatoopcion")
public class ClienteDatoOpcionResource extends CoreBaseService {

    @InjectParam
    private ClienteDatoOpcionService clienteDatoOpcionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClienteDatoOpcionOpciones(
            @QueryParam("clienteDatoTipoId") Long clienteDatoTipoId) {
        return modelToUI(clienteDatoOpcionService
                .getClienteDatoOpcionesByTipoId(clienteDatoTipoId));
    }

    private List<UIEntity> modelToUI(List<ClienteDatoOpcion> clienteDatoOpcions) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (ClienteDatoOpcion clienteDatoOpcion : clienteDatoOpcions)
        {
            listaUI.add(modelToUI(clienteDatoOpcion));
        }
        return listaUI;
    }

    private UIEntity modelToUI(ClienteDatoOpcion clienteDatoOpcion) {
        return UIEntity.toUI(clienteDatoOpcion);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addClienteDatoOpcion(UIEntity entity) {
        ClienteDatoOpcion clienteDatoOpcion = UIToModel(entity);
        clienteDatoOpcionService.addClienteDatoOpcion(clienteDatoOpcion);
        return modelToUI(clienteDatoOpcion);
    }

    private ClienteDatoOpcion UIToModel(UIEntity entity) {
        ClienteDatoOpcion clienteDatoOpcion = entity.toModel(ClienteDatoOpcion.class);

        ClienteDatoTipo clienteDatoTipo = new ClienteDatoTipo();
        clienteDatoTipo.setId(Long.parseLong(entity.get("clienteDatoTipoId")));
        clienteDatoOpcion.setClienteDatoTipo(clienteDatoTipo);

        return clienteDatoOpcion;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateClienteDatoOpcion(@PathParam("id") Long id,
                                            UIEntity entity) {
        ClienteDatoOpcion clienteDatoOpcion = UIToModel(entity);
        clienteDatoOpcion.setId(id);
        clienteDatoOpcionService.updateClienteDatoOpcion(clienteDatoOpcion);
        return modelToUI(clienteDatoOpcion);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long id) {
        clienteDatoOpcionService.deleteClienteDatoOpcion(id);
    }

}