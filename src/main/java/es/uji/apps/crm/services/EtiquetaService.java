package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.EtiquetaDAO;
import es.uji.apps.crm.model.Etiqueta;

@Service
public class EtiquetaService {
    private EtiquetaDAO etiquetaDAO;

    @Autowired
    public EtiquetaService(EtiquetaDAO etiquetaDAO) {
        this.etiquetaDAO = etiquetaDAO;
    }

    public List<Etiqueta> getEtiquetas(String busqueda) {
        if (busqueda != null && !busqueda.isEmpty())
        {
            return etiquetaDAO.getEtiquetasByNombre(busqueda);
        }

        return etiquetaDAO.getEtiquetas();
    }

    public Etiqueta getEtiquetaById(Long etiquetaId) {
        return etiquetaDAO.getEtiquetaById(etiquetaId);
    }

    public Etiqueta getEtiquetaByNombre(String etiquetaNombre) {
        return etiquetaDAO.getEtiquetaByNombre(etiquetaNombre);
    }

    public Etiqueta addEtiqueta(Etiqueta etiqueta) {
        etiquetaDAO.insert(etiqueta);
        etiqueta.setId(etiqueta.getId());
        return etiqueta;
    }

    public void updateEtiqueta(Etiqueta etiqueta) {
        etiquetaDAO.update(etiqueta);
    }

    public void deleteEtiqueta(Long etiquetaId) {
        etiquetaDAO.delete(Etiqueta.class, etiquetaId);
    }


}