package es.uji.apps.crm.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.model.Campanya;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaCartaClienteDAO;
import es.uji.apps.crm.model.CampanyaCarta;
import es.uji.apps.crm.model.CampanyaCartaCliente;
import es.uji.apps.crm.model.Cliente;

@Service
public class CampanyaCartaClienteService {

    private ClienteService clienteService;
    private EnvioService envioService;
    private UtilsService utilsService;
    @InjectParam
    private CampanyaCartaService campanyaCartaService;

    private CampanyaCartaClienteDAO campanyaCartaClienteDAO;

    @Autowired
    public CampanyaCartaClienteService(CampanyaCartaClienteDAO campanyaCartaClienteDAO, ClienteService clienteService, EnvioService envioService, UtilsService utilsService) {
        this.campanyaCartaClienteDAO = campanyaCartaClienteDAO;
        this.clienteService = clienteService;
        this.envioService = envioService;
        this.utilsService = utilsService;
    }

    public CampanyaCartaCliente addCampanyaCartaCliente(CampanyaCarta campanyaCarta, Cliente cliente, Long campanyaId)
            throws ParseException {
        CampanyaCartaCliente campanyaCartaCliente = new CampanyaCartaCliente();

        campanyaCartaCliente.setFechaCreacion(new Date());
        campanyaCartaCliente.setCliente(cliente);

        String cuerpo = campanyaCarta.getCuerpo();

        cuerpo = cuerpo.replace("$[nom]", ParamUtils.isNotNull(cliente.getNombreApellidos())? cliente.getNombreApellidos() : "");
        cuerpo = cuerpo.replace("$[nombre_propio]", ParamUtils.isNotNull(cliente.getNombre())?cliente.getNombre():"");
        cuerpo = cuerpo.replace("$[correo]", ParamUtils.isNotNull(cliente.getCorreo())?cliente.getCorreo():"");
        cuerpo = cuerpo.replace("$[identificacion]", cliente.getClienteGeneral().getIdentificacion());
        cuerpo = cuerpo.replace("$[postal]", this.addPre(ParamUtils.isNotNull(cliente.getPostalCompleto())?cliente.getPostalCompleto():""));
        cuerpo = cuerpo.replace("$[fecha_alta]", utilsService.fechaFormat(campanyaCartaCliente.getFechaCreacion()));

        cuerpo = envioService.cambiaSexo(cuerpo, ParamUtils.isNotNull(cliente.getSexo())?cliente.getSexo():3L);

        campanyaCartaCliente.setClienteCarta(cuerpo);
        Campanya campanya = new Campanya();
        campanya.setId(campanyaId);
        campanyaCartaCliente.setCampanya(campanya);

        campanyaCartaClienteDAO.insert(campanyaCartaCliente);

        return campanyaCartaCliente;
    }

    public String addPre(String texto) {
        return "<span style=\"white-space:pre\">" + texto + "</span>";
    }

    public List<CampanyaCartaCliente> getClienteCartasByClienteId(Long clienteId, Long campanyaId) {
        return campanyaCartaClienteDAO.getClienteCartasByClienteId(clienteId, campanyaId);
    }

    public CampanyaCartaCliente getClienteCartaById(Long clienteCartaId) {
        return campanyaCartaClienteDAO.getClienteCartaById(clienteCartaId);
    }

    public CampanyaCartaCliente updateClienteCarta(CampanyaCartaCliente campanyaCartaCliente) {
        return campanyaCartaClienteDAO.update(campanyaCartaCliente);
    }

    public CampanyaCartaCliente addClienteCarta(Long clienteId, Long campanyaId, String carta) throws ParseException {

        CampanyaCarta campanyaCarta = new CampanyaCarta();
        campanyaCarta.setCuerpo(carta);

        Cliente cliente = clienteService.getClienteById(clienteId);
        return addCampanyaCartaCliente(campanyaCarta, cliente, campanyaId);
    }

    public void deleteClienteCarta(Long clienteCartaId) {
        campanyaCartaClienteDAO.delete(CampanyaCartaCliente.class, clienteCartaId);
    }

    public List<CampanyaCartaCliente> addClienteCartasTodas(Long clienteId, Long campanyaId) throws ParseException {

        Cliente cliente = clienteService.getClienteById(clienteId);
        List<CampanyaCarta> campanyaCartas = campanyaCartaService.getCampanyaCartasByCampanyaId(campanyaId);
        List <CampanyaCartaCliente> lista = new ArrayList<CampanyaCartaCliente>();
        for (CampanyaCarta campanyaCarta : campanyaCartas)
        {
            lista.add(addCampanyaCartaCliente(campanyaCarta, cliente, campanyaId));
        }

        return lista;
    }
}