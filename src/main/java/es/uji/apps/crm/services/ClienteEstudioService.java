package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ClienteEstudioDAO;
import es.uji.apps.crm.model.ClienteEstudio;

@Service
public class ClienteEstudioService {

    private ClienteEstudioDAO clienteEstudioDAO;

    @Autowired
    public ClienteEstudioService(ClienteEstudioDAO clienteEstudioDAO) {

        this.clienteEstudioDAO = clienteEstudioDAO;
    }


    public List<ClienteEstudio> getClienteEstudiosByPersona(Long personaId) {

        return clienteEstudioDAO.getClienteEstudiosByPersona(personaId);
    }
}