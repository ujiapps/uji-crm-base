package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.FaseDAO;
import es.uji.apps.crm.model.Fase;

@Service
public class FaseService {
    private FaseDAO faseDAO;

    @Autowired
    public FaseService(FaseDAO faseDAO) {
        this.faseDAO = faseDAO;
    }

    public List<Fase> getFases() {
        return faseDAO.getFases();
    }

    public Fase getFaseById(Long faseId) {
        return faseDAO.getFaseById(faseId);

    }

}