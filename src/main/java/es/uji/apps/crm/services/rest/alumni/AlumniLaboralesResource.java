package es.uji.apps.crm.services.rest.alumni;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.auth.UserSessionManager;
import es.uji.apps.crm.model.ClienteDato;
import es.uji.apps.crm.model.ClienteItem;
import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.datos.Laborales;
import es.uji.apps.crm.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.util.List;

public class AlumniLaboralesResource extends CoreBaseService {

    private final static Boolean FICHAZONAPRIVADA = Boolean.TRUE;
    private final static Long PERFILLINKEDIN = 1645952L;

    @InjectParam
    private ClienteDatoService clienteDatoService;
    @InjectParam
    private ItemService itemService;
    @InjectParam
    private ClienteItemService clienteItemService;
    @InjectParam
    private UserSessionManager userSessionManager;
    @InjectParam
    private AlumniLaboralesService alumniLaboralesService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getAlumniLaborales(@CookieParam("p_hash") String
                                               pHash, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        List<ClienteDato> listaDatosCliente = clienteDatoService.getAllDatosByClienteId(userSessionManager.getCliente().getId());
        List<ClienteItem> listaItemsCliente = clienteItemService.getAllItemsByClienteId(userSessionManager.getCliente().getId());
        ClienteDato linkedin = clienteDatoService.getClienteDatoByClienteAndTipoId(userSessionManager.getCliente(), PERFILLINKEDIN);
        Laborales laborales = new Laborales(listaDatosCliente, listaItemsCliente, linkedin);
        return UIEntity.toUI(laborales);
    }

    @GET
    @Path("zonaprivada")
    @Produces(MediaType.TEXT_HTML)
    public Template getAlumniTemplateLaborales(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {
        return alumniLaboralesService.getTemplateLaboral(request.getPathInfo(), idioma);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity postAlumniLaborales(@CookieParam("p_hash") String pHash,
                                        @CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                        @FormParam("estadoLaboralId") Long estadoLaboralId,
                                        @FormParam("sectorEmpresarial") Long sectorEmpresarial,
                                        @FormParam("empresa") String empresa,
                                        @FormParam("cargo") String cargo,
                                        @FormParam("nombreEstudioActual") String nombreEstudioActual,
                                        @FormParam("linkedin") String linkedin)
            throws ParseException {
        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        Item estadoLaboral = itemService.getItemById(estadoLaboralId);
        alumniLaboralesService.actualizaLaborales(userSessionManager.getCliente(), estadoLaboral, sectorEmpresarial,
                empresa, cargo, nombreEstudioActual, linkedin);
        return new UIEntity();
    }

}
