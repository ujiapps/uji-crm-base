package es.uji.apps.crm.services;

import es.uji.apps.crm.utils.AppInfo;
import es.uji.commons.web.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;

@Service
public class AlumniLoginService {

    private final String directorioPlantillas = "crm/alumni2024/";
    private final String plantillaBase = directorioPlantillas + "base";
    private final BannerService bannerService;

    @Autowired
    public AlumniLoginService(BannerService bannerService) {
        this.bannerService = bannerService;
    }

    public Template getTemplateLogin(String idioma) throws ParseException {
        Template template = AppInfo.buildPagina(plantillaBase, idioma);
        template.put("contenido", directorioPlantillas + "login");
        template.put("urlBase", "/crm/rest/alumni2024/login/?");
        template.put("imagenesBanner", bannerService.getAllBanners());
        template.put("banner", directorioPlantillas + "banner");
        return template;
    }
}
