package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.services.DatoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("dato")
public class DatoResource extends CoreBaseService {
    @InjectParam
    private DatoService datoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDatosByClienteId(@QueryParam("clienteId") Long clienteId) {
        return UIEntity.toUI(datoService.getDatosByClienteId(clienteId));
    }

    @GET
    @Path("campanya")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDatosByClienteIdAndCampanya(@QueryParam("campanyaId") Long campanyaId, @QueryParam("clienteId") Long clienteId) {

        return UIEntity.toUI(datoService.getDatosByClienteIdAndCampanya(campanyaId, clienteId));
    }

    @GET
    @Path("relacionados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDatosByDatoId(@QueryParam("dato") Long datoId) {
        return UIEntity.toUI(datoService.getDatosByDatoId(datoId));
    }

}