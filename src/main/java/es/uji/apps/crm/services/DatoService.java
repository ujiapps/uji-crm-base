package es.uji.apps.crm.services;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.DatoDAO;
import es.uji.apps.crm.model.ClienteDatoVw;

@Service
public class DatoService {

    private DatoDAO datoDAO;

    @Autowired
    public DatoService(DatoDAO datoDAO) {
        this.datoDAO = datoDAO;
    }

    public List<ClienteDatoVw> getDatosByClienteId(Long clienteId) {
        return datoDAO.getDatosByClienteId(clienteId);

    }

    public List<ClienteDatoVw> getDatosByDatoId(Long datoId) {

        return datoDAO.getDatosByDatoId(datoId);
    }

    public List<ClienteDatoVw> getDatosByClienteIdAndCampanya(Long campanyaId, Long clienteId) {
        return datoDAO.getDatosByClienteIdAndCampanya(campanyaId, clienteId);

    }
}