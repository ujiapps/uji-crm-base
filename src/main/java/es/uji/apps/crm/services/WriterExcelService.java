package es.uji.apps.crm.services;


import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;

@Service
public class WriterExcelService {

    private final String DEFAULT_FORMAT_DATE = "dd/MM/yyyy";

    private HSSFWorkbook workbook;
    private CellStyle estilNegreta;

    private CellStyle estiloFecha;
    private HSSFSheet fulla;

    public WriterExcelService() {
        this.workbook = new HSSFWorkbook();
        generaEstilNegreta();
        getEstiloFecha();
    }

    public CellStyle getEstiloFecha() {
        return estiloFecha;
    }

    public void setEstiloFecha(CellStyle estiloFecha) {
        this.estiloFecha = estiloFecha;
    }

    public void addFulla(String nomFulla) {
        this.fulla = this.workbook.createSheet(nomFulla);
    }

    private void generaEstilNegreta() {
        CellStyle cs = this.workbook.createCellStyle();
        Font f = workbook.createFont();
        f.setBold(true);
        cs.setFont(f);
        this.estilNegreta = cs;
    }

    private void generaEstiloFecha() {
        CellStyle cs = this.workbook.createCellStyle();
        CreationHelper createHelper = this.workbook.getCreationHelper();
        cs.setDataFormat(createHelper.createDataFormat().getFormat(DEFAULT_FORMAT_DATE));
        this.estiloFecha = cs;
    }

    public void generaCabecera(String... textos) {
        generaCeldes(this.getEstilNegreta(), 0, textos);
    }

    public void generaCeldes(CellStyle estil, int rownum, String... textos) {
        Row row = getNewRow(rownum);
        int cellnum = 0;
        for (String text : textos) {
            addCell(cellnum++, text, estil, row);
        }
    }

    public CellStyle getEstilNegreta() {
        return estilNegreta;
    }

    public Row getNewRow(int rownum) {
        return this.fulla.createRow(rownum);
    }

    public void setEstilNegreta(CellStyle estilNegreta) {
        this.estilNegreta = estilNegreta;
    }

    public void addCell(int cellNumber, float valor, Row row) {
        Cell cell = row.createCell(cellNumber);
        CellStyle cellStyle = this.workbook.createCellStyle();
        cellStyle = addFloatStyle(cellStyle);
        cell.setCellValue(valor);
        cell.setCellStyle(cellStyle);
    }

    private CellStyle addFloatStyle(CellStyle style) {
        DataFormat format = this.workbook.createDataFormat();
        style.setDataFormat(format.getFormat("0.00"));
        return style;
    }

    public void addCell(int cellNumber, String text, CellStyle estil, Row row) {
        Cell cell = row.createCell(cellNumber);
        cell.setCellValue(text);

        if (estil != null)
            cell.setCellStyle(estil);
    }

    public void addCell(int cellNumber, int number, CellStyle estil, Row row) {
        Cell cell = row.createCell(cellNumber);
        cell.setCellValue((Integer) number);

        if (estil != null)
            cell.setCellStyle(estil);
    }

    public void addCell(int cellNumber, Date date, CellStyle estil, Row row) {
        Cell cell = row.createCell(cellNumber);
        cell.setCellValue(date);

        if (estil == null) {
            estil = this.getEstiloFecha();
        }
        cell.setCellStyle(estil);
    }

    public ByteArrayOutputStream getExcel() throws IOException {
        ByteArrayOutputStream outstream = new ByteArrayOutputStream();
        this.workbook.write(outstream);
        outstream.flush();
        outstream.close();

        return outstream;
    }

    public void addCellWithFormula(int cellnum, String formula, CellStyle estil, Row row) {
        Cell cell = row.createCell(cellnum);
        cell.setCellStyle(estil);
        cell.setCellFormula(formula);
    }

    public void addFloatCellWithFormula(int cellnum, String formula, CellStyle estil, Row row) {
        Cell cell = row.createCell(cellnum);
        estil = addFloatStyle(estil);
        cell.setCellStyle(estil);
        cell.setCellFormula(formula);
    }

    public void autoSizeColumns(Integer columnas) {
        for (int i = 0; i < columnas; i++) {
            fulla.autoSizeColumn(i);
        }
    }

    public void addMergedRegion(Integer firstRow, Integer lastRow, Integer firstCol, Integer lastCol) {
        fulla.addMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
    }
}
