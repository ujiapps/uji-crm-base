package es.uji.apps.crm.services.rest;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaActo;
import es.uji.apps.crm.model.CampanyaFormulario;
import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.apps.crm.services.CampanyaFormularioService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("campanyaformulario")
public class CampanyaFormularioResource extends CoreBaseService {
    @InjectParam
    private CampanyaFormularioService campanyaFormularioService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyaFormulariosByCampanya(@QueryParam("campanyaId") Long campanyaId) {

        return modelToUI(campanyaFormularioService.getCampanyaFormulariosByCampanya(campanyaId));
    }

    private List<UIEntity> modelToUI(List<CampanyaFormulario> campanyaFormularios) {
        return campanyaFormularios.stream().map(this::modelToUI).collect(Collectors.toList());
    }

    private UIEntity modelToUI(CampanyaFormulario campanya) {
        UIEntity entity = new UIEntity();

        if (ParamUtils.isNotNull(campanya))
        {
            entity = UIEntity.toUI(campanya);
            entity.put("autenticaFormulario", campanya.getAutenticaFormulario());
        }
        entity.put("accionDestino", campanya.getEstadoDestino().getAccion());

        return entity;
    }

    @GET
    @Path("estado")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getCampanyaFormulariosByCampanyaAndEstado(@QueryParam("campanyaId") Long campanyaId, @QueryParam("estadoId") Long estadoId) {

        CampanyaFormulario campanyaFormulario = campanyaFormularioService.getCampanyaFormulariosByCampanyaAndEstado(campanyaId, estadoId);

        if (ParamUtils.isNotNull(campanyaFormulario))
        {
            return modelToUI(campanyaFormulario);
        }
        else
        {
            return null;
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanyaFormulario(UIEntity entity) throws ParseException {
        CampanyaFormulario campanyaFormulario = UIToModel(entity);
        campanyaFormularioService.addCampanyaFormulario(campanyaFormulario);
        return modelToUI(campanyaFormulario);
    }

    private CampanyaFormulario UIToModel(UIEntity entity) throws ParseException {
        CampanyaFormulario campanyaFormulario = entity.toModel(CampanyaFormulario.class);

        String campanyaId = entity.get("campanyaId");
        if (campanyaId != null)
        {
            Campanya campanya = new Campanya();
            campanya.setId(Long.parseLong(campanyaId));
            campanyaFormulario.setCampanya(campanya);
        }

        Long estadoDestinoId = ParamUtils.parseLong(entity.get("estadoDestinoId"));
        if (ParamUtils.isNotNull(estadoDestinoId))
        {
            TipoEstadoCampanya estadoDestino = new TipoEstadoCampanya();
            estadoDestino.setId(estadoDestinoId);
            campanyaFormulario.setEstadoDestino(estadoDestino);
        }

        String fechaInicio = entity.get("fechaInicio");
        if (ParamUtils.isNotNull(fechaInicio))
        {
            Date fecha = UtilsService.fechaParse(fechaInicio.substring(0, 10));
            campanyaFormulario.setFechaInicio(fecha);
        }

        String fechaFin = entity.get("fechaFin");
        if (ParamUtils.isNotNull(fechaFin))
        {
            Date fecha = UtilsService.fechaParse(fechaFin.substring(0, 10));
            campanyaFormulario.setFechaFin(fecha);
        }

        String actoId = entity.get("actoId");
        if (actoId != null && !actoId.equals("0"))
        {
            CampanyaActo acto = new CampanyaActo();
            acto.setId(Long.parseLong(actoId));
            campanyaFormulario.setActo(acto);
        }
        else
        {
            campanyaFormulario.setActo(null);
        }


        return campanyaFormulario;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("duplicar")
    public UIEntity duplicarCampanyaFormulario(@FormParam("campanyaFormularioId") Long campanyaFormularioId) {
        CampanyaFormulario campanyaFormulario = campanyaFormularioService.getFormularioById(campanyaFormularioId);
        CampanyaFormulario duplicado = campanyaFormularioService.duplicarFormulario(campanyaFormulario);
        return modelToUI(duplicado);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateCampanyaFormulario(@PathParam("id") Long campanyaFormularioId, UIEntity entity) throws
            ParseException {

        CampanyaFormulario campanyaFormulario = UIToModel(entity);
        campanyaFormulario.setId(campanyaFormularioId);

        campanyaFormularioService.updateCampanyaFormulario(campanyaFormulario);
        return modelToUI(campanyaFormulario);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteCampanyaFormulario(@PathParam("id") Long campanyaFormularioId) {
        campanyaFormularioService.deleteCampanyaFormulario(campanyaFormularioId);
    }

}