package es.uji.apps.crm.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ClienteCuotaDAO;
import es.uji.apps.crm.dao.ClienteTarifaDAO;
import es.uji.apps.crm.dao.RecibosManager;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteCuota;
import es.uji.apps.crm.model.ClienteTarifa;
import es.uji.apps.crm.model.TipoTarifa;
import es.uji.commons.rest.ParamUtils;

@Service
public class ClienteTarifaService {

    //    @InjectParam
    private TipoTarifaService tipoTarifaService;
    private ClienteCuotaService clienteCuotaService;
    private ClienteTarifaDAO clienteTarifaDAO;
    private ClienteCuotaDAO clienteCuotaDAO;

    @Autowired
    public ClienteTarifaService(ClienteTarifaDAO clienteTarifaDAO, ClienteCuotaService clienteCuotaService, TipoTarifaService tipoTarifaService, ClienteCuotaDAO clienteCuotaDAO) {
        this.clienteTarifaDAO = clienteTarifaDAO;
        this.tipoTarifaService = tipoTarifaService;
        this.clienteCuotaDAO = clienteCuotaDAO;
        this.clienteCuotaService = clienteCuotaService;

    }

    public void addClienteTarifa(ClienteTarifa clienteTarifa) {

        TipoTarifa tipoTarifa = tipoTarifaService.getTipoTarifaById(clienteTarifa.getTipoTarifa().getId());

        Calendar calendar = Calendar.getInstance();

        ClienteTarifa clienteTarifaMax = dameFechaMaxCaducaClienteTarifa(clienteTarifa.getCliente());

        if (ParamUtils.isNotNull(clienteTarifaMax))
        {
            calendar.setTime(clienteTarifaMax.getFechaCaducidad());
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            clienteTarifa.setFechaInicioTarifa(calendar.getTime());
        }
        else
        {
            clienteTarifa.setFechaInicioTarifa(new Date());
        }

        calendar.setTime(clienteTarifa.getFechaInicioTarifa());

//        if (ParamUtils.isNotNull(clienteTarifaMax)) {
//            calendar.setTime(clienteTarifaMax.getFechaCaducidad());
//        } else {
//            calendar.setTime(new Date());
//        }

        if (ParamUtils.isNotNull(tipoTarifa.getMesesCaduca()) && tipoTarifa.getMesesCaduca() > 0)
        {
            calendar.add(Calendar.MONTH, tipoTarifa.getMesesCaduca().intValue());
            calendar.add(Calendar.DAY_OF_YEAR, -1);
            clienteTarifa.setFechaCaducidad(calendar.getTime());
        }
        else
        {
            clienteTarifa.setFechaCaducidad(null);
        }


        clienteTarifa.setFechaUltimaModificacion(new Date());
        clienteTarifaDAO.insert(clienteTarifa);

//        clienteCuotaService.addClienteCuota(clienteTarifa);
    }

    private ClienteTarifa dameFechaMaxCaducaClienteTarifa(Cliente cliente) {

        return clienteTarifaDAO.dameFechaMaxCaducaClienteTarifa(cliente);
    }

    public void addClienteTarifa(TipoTarifa tipoTarifa, Cliente cliente) {

        ClienteTarifa clienteTarifa = new ClienteTarifa();
        clienteTarifa.setCliente(cliente);
        clienteTarifa.setTipoTarifa(tipoTarifa);
        clienteTarifa.setFechaInicioTarifa(new Date());

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, tipoTarifa.getMesesCaduca().intValue());
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        clienteTarifa.setFechaCaducidad(calendar.getTime());

        clienteTarifa.setFechaUltimaModificacion(new Date());

        clienteTarifaDAO.insert(clienteTarifa);

        clienteCuotaService.addClienteCuota(clienteTarifa);

    }

    private ClienteTarifa getClienteTarifaById(Long clienteTarifaId) {
        return clienteTarifaDAO.getClienteTarifaById(clienteTarifaId);
    }

    public ClienteTarifa updateClienteTarifa(ClienteTarifa clienteTarifa) {
        clienteTarifa.setFechaUltimaModificacion(new Date());
        return clienteTarifaDAO.update(clienteTarifa);
    }

    public void deleteClienteTarifaByCampanyaAndCliente(Campanya campanya, Cliente cliente) {

        List<ClienteTarifa> clienteTarifas = getClienteTarifaByClienteIdAndCampanyaId(cliente.getId(), campanya.getId());

        for (ClienteTarifa clienteTarifa : clienteTarifas)
        {
//            List<ClienteTarifa> clienteTarifasMismoTipo = getClienteTarifasByClienteAndTipoTarifa(cliente, clienteTarifa.getTipoTarifa());
//            if (clienteTarifasMismoTipo.size() == 1) {
            clienteCuotaService.deleteClienteCuotasByClienteTarifa(clienteTarifa);
//            }
            delete(clienteTarifa.getId());
        }
    }

    public List<ClienteTarifa> getClienteTarifaByClienteIdAndCampanyaId(Long cliente, Long campanya) {
        return clienteTarifaDAO.getClienteTarifaByClienteIdAndCampanyaId(cliente, campanya);
    }

    public void delete(Long clienteTarifaId) {

        List<ClienteCuota> clienteCuotas = clienteCuotaDAO.getClienteCuotasByClienteTarifaId(clienteTarifaId);

        if (clienteCuotas.size() > 0)
        {
            for (ClienteCuota clienteCuota : clienteCuotas)
            {
                RecibosManager recibos = new RecibosManager();
                recibos.init();
                recibos.borra(clienteCuota.getReciboId());
                clienteCuotaDAO.delete(ClienteCuota.class, clienteCuota.getId());
            }
        }

        clienteTarifaDAO.delete(ClienteTarifa.class, clienteTarifaId);
    }

    public List<ClienteTarifa> getClienteTarifasByClienteAndTipoTarifa(Cliente cliente, TipoTarifa tipoTarifa) {
        return clienteTarifaDAO.getClienteTarifasByClienteAndTipoTarifa(cliente, tipoTarifa);
    }
}