package es.uji.apps.crm.services.rest.eventos.rss;

import java.util.Map;

import org.jdom.Namespace;

import com.sun.syndication.feed.module.Module;

public interface MetadataEntry extends Module {
    public static final String URI = "http://www.uji.es/namespaces/rss#";
    public static final String PREFIX_CRM_GENERAL = "crm-general";
    public static final Namespace namespace_general = Namespace.getNamespace(MetadataEntryImplCRM.PREFIX_CRM_GENERAL,
            MetadataEntryImplCRM.URI);
    public static final String PREFIX_CRM_METADATO = "crm-metadato";
    public static final Namespace namespace_metadato = Namespace.getNamespace(MetadataEntryImplCRM.PREFIX_CRM_METADATO,
            MetadataEntryImplCRM.URI);

    Map<String, ValorAtributo> getFields();

    void setFields(Map<String, ValorAtributo> fields);

    void addField(String name, ValorAtributo valorAtributo);

    Namespace getNamespace(String namespace);
}