package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Linea;
import es.uji.apps.crm.model.Persona;
import es.uji.apps.crm.model.Recibo;
import es.uji.apps.crm.services.LineaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("linea")
public class LineaResource extends CoreBaseService {
    @InjectParam
    private LineaService lineaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("recibo/{id}")
    public List<UIEntity> getLineasByLineaId(@PathParam("id") Long LineaId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Linea> lista = lineaService.getLineasByLineaId(LineaId);
        return modelToUI(lista);
    }

    private List<UIEntity> modelToUI(List<Linea> lineas) {
        List<UIEntity> listaUI = new ArrayList<>();

        for (Linea linea : lineas)
        {
            listaUI.add(modelToUI(linea));
        }
        return listaUI;
    }

    private UIEntity modelToUI(Linea linea) {
        return UIEntity.toUI(linea);
    }

    private Linea UIToModel(UIEntity entity) {

        Linea linea = entity.toModel(Linea.class);

        Persona persona = new Persona();
        persona.setId(Long.parseLong(entity.get("personaId")));
        linea.setPersona(persona);

        Recibo recibo = new Recibo();
        recibo.setId(Long.parseLong(entity.get("reciboId")));
        linea.setRecibo(recibo);


        return linea;
    }

}