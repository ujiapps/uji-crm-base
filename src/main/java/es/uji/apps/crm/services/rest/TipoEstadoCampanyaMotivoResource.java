package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.apps.crm.model.TipoEstadoCampanyaMotivo;
import es.uji.apps.crm.services.TipoEstadoCampanyaMotivoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("tipoestadocampanyamotivo")
public class TipoEstadoCampanyaMotivoResource extends CoreBaseService {

    @InjectParam
    private TipoEstadoCampanyaMotivoService tipoEstadoCampanyaMotivoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getTiposEstadoCampanyaByCampanyaId(@QueryParam("tipoEstadoCampanyaId") Long campanyaEstadoId) {

        List<TipoEstadoCampanyaMotivo> lista = tipoEstadoCampanyaMotivoService.getTiposEstadoCampanyaByCampanyaId(campanyaEstadoId);

        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setTotalCount(lista.size());
        responseMessage.setData(UIEntity.toUI(lista));
        return responseMessage;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addTipoEstadoCampanyaByCampanyaId(UIEntity entity) {

        return UIEntity.toUI(tipoEstadoCampanyaMotivoService.addTipoEstadoCampanyaByCampanyaId(UIToModel(entity)));
    }

    private TipoEstadoCampanyaMotivo UIToModel(UIEntity entity) {
        TipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivo = entity.toModel(TipoEstadoCampanyaMotivo.class);

        TipoEstadoCampanya tipoEstadoCampanya = new TipoEstadoCampanya();
        tipoEstadoCampanya.setId(ParamUtils.parseLong(entity.get("tipoEstadoCampanyaId")));
        tipoEstadoCampanyaMotivo.setTipoEstadoCampanya(tipoEstadoCampanya);

        return tipoEstadoCampanyaMotivo;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateTipoEstadoCampanyaByCampanyaId(UIEntity entity) {
        return UIEntity.toUI(tipoEstadoCampanyaMotivoService.updateTipoEstadoCampanyaByCampanyaId(UIToModel(entity)));
    }

    @DELETE
    @Path("{id}")
    public void deleteTipoEstadoCampanya(@PathParam("id") Long tipoEstadoCampanyaMotivoId) {
        tipoEstadoCampanyaMotivoService.deleteTipoEstadoCampanyaMotivo(tipoEstadoCampanyaMotivoId);
    }
}