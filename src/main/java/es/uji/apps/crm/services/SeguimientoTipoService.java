package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.SeguimientoTipoDAO;
import es.uji.apps.crm.model.SeguimientoTipo;

@Service
public class SeguimientoTipoService {
    private SeguimientoTipoDAO seguimientoTipoDAO;

    @Autowired
    public SeguimientoTipoService(SeguimientoTipoDAO seguimientoTipoDAO) {
        this.seguimientoTipoDAO = seguimientoTipoDAO;
    }

    public List<SeguimientoTipo> getSeguimientoTipos() {
        return seguimientoTipoDAO.getSeguimientoTipos();
    }

    public SeguimientoTipo addSeguimientoTipos(SeguimientoTipo seguimientoTipo) {
        return seguimientoTipoDAO.insert(seguimientoTipo);
    }

    public void deleteSeguimientoTipo(Long seguimientoTipoId) {
        seguimientoTipoDAO.delete(SeguimientoTipo.class, seguimientoTipoId);
    }

    public SeguimientoTipo updateSeguimientoTipos(SeguimientoTipo seguimientoTipo) {
        return seguimientoTipoDAO.update(seguimientoTipo);
    }
}