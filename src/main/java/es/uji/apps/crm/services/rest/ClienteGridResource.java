package es.uji.apps.crm.services.rest;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.exceptions.BusquedaVaciaException;
import es.uji.apps.crm.model.ClienteDatoTipo;
import es.uji.apps.crm.model.ClienteGrid;
import es.uji.apps.crm.model.FiltroCliente;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.services.ClienteDatoOpcionService;
import es.uji.apps.crm.services.ClienteDatoService;
import es.uji.apps.crm.services.ClienteDatoTipoService;
import es.uji.apps.crm.services.ClienteGeneralService;
import es.uji.apps.crm.services.ClienteService;
import es.uji.apps.crm.services.GrupoService;
import es.uji.apps.crm.services.ItemService;
import es.uji.apps.crm.services.PersonaService;
import es.uji.apps.crm.services.TipoService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("clientegrid")
public class ClienteGridResource extends CoreBaseService {
    @InjectParam
    private ClienteService clienteService;
    @InjectParam
    private ClienteDatoService consultaClienteDato;
    @InjectParam
    private ClienteDatoOpcionService consultaClienteDatoOpcion;
    @InjectParam
    private UtilsService utilsService;
    @InjectParam
    private PersonaService personaService;
    @InjectParam
    private ClienteDatoTipoService clienteDatoTipoService;
    @InjectParam
    private GrupoService grupoService;
    @InjectParam
    private ItemService itemService;
    @InjectParam
    private TipoService tipoService;
    @InjectParam
    private ClienteGeneralService clienteGeneralService;

    @InjectParam
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getClientes(@QueryParam("busqueda") String busqueda,
                                       @QueryParam("etiqueta") String etiqueta, @QueryParam("tipo") String tipo,
                                       @QueryParam("items") String itemsString, @QueryParam("campanya") Long campanya,
                                       @QueryParam("tipoDato") String tipoDato, @QueryParam("valorDato") String valor,
                                       @QueryParam("correo") String correo, @QueryParam("movil") String movil,
                                       @QueryParam("campanyaTipoEstado") Long campanyaTipoEstado, @QueryParam("postal") String postal,
                                       @QueryParam("programa") Long programa, @QueryParam("grupo") Long grupo,
                                       @QueryParam("tipoComparacion") Long tipoComparacion, @QueryParam("fecha") String fecha,
                                       @QueryParam("fechaMax") String fechaMax, @QueryParam("item") Long item,
                                       @QueryParam("tipoFecha") Long tipoFecha, @QueryParam("mes") Long mes,
                                       @QueryParam("anyo") Long anyo, @QueryParam("anyoMax") Long anyoMax,
                                       @QueryParam("mesMax") Long mesMax,
                                       @QueryParam("nacionalidad") String nacionalidad,
                                       @QueryParam("codigoPostal") String codigoPostal,
                                       @QueryParam("paisDomicilio") String paisDomicilio,
                                       @QueryParam("provinciaDomicilio") Long provinciaDomicilio,
                                       @QueryParam("poblacionDomicilio") Long poblacionDomicilio,
                                       @QueryParam("estudioUJI") Long estudioUJI,
                                       @QueryParam("estudioNoUJI") Long estudioNoUJI,
                                       @QueryParam("estudioUJIBeca") List<Long> estudioUJIBeca,
                                       @QueryParam("start") @DefaultValue("0") Long start,
                                       @QueryParam("limit") @DefaultValue("25") Long limit)
            throws BusquedaVaciaException,
            ParseException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        FiltroCliente filtro = new FiltroCliente();
        filtro.setBusquedaGenerica(busqueda);
        filtro.setEtiqueta(etiqueta);
        filtro.setItems(itemsString);
        filtro.setTipoCliente(tipo);
        filtro.setCampanyaId(campanya);
        filtro.setCampanyaTipoEstado(campanyaTipoEstado);
        filtro.setValorDato(valor);
        filtro.setCorreo(correo);
        filtro.setMovil(movil);
        filtro.setPrograma(programa);
        filtro.setGrupo(grupo);
        filtro.setItem(item);
        filtro.setTipoComparacion(tipoComparacion);
        filtro.setTipoFecha(tipoFecha);
        filtro.setMes(mes);
        filtro.setAnyo(anyo);
        filtro.setMesMax(mesMax);
        filtro.setAnyoMax(anyoMax);
        filtro.setPostal(postal);
        filtro.setNacionalidad(nacionalidad);
        filtro.setCodigoPostal(codigoPostal);
        filtro.setPaisDomicilio(paisDomicilio);
        filtro.setProvinciaDomicilio(provinciaDomicilio);
        filtro.setPoblacionDomiclio(poblacionDomicilio);
        filtro.setEstudioUJI(estudioUJI);
        filtro.setEstudioUJIBeca(estudioUJIBeca);
        filtro.setEstudioNoUJI(estudioNoUJI);

        if (ParamUtils.isNotNull(fecha))
        {
            filtro.setFecha(utilsService.fechaParse(fecha.substring(0, 10), "yyyy-MM-dd"));
        }

        if (ParamUtils.isNotNull(fechaMax))
        {
            filtro.setFechaMax(utilsService.fechaParse(fechaMax.substring(0, 10), "yyyy-MM-dd"));
        }

        if (ParamUtils.isNotNull(tipoDato))
        {
            ClienteDatoTipo clienteDatoTipo = clienteDatoTipoService
                    .getClienteDatoTipoById(ParamUtils.parseLong(tipoDato));
            filtro.setTipoDato(clienteDatoTipo);
        }

        Paginacion paginacion = new Paginacion(start, limit);

        List<ClienteGrid> lista = new ArrayList<>();

        if (ParamUtils.isNotNull(busqueda) || ParamUtils.isNotNull(etiqueta) || campanya != null || ParamUtils.isNotNull(tipoDato) || ParamUtils.isNotNull(correo)
                || ParamUtils.isNotNull(movil) || ParamUtils.isNotNull(programa) || ParamUtils.isNotNull(postal) || ParamUtils.isNotNull(tipo)
                || ParamUtils.isNotNull(nacionalidad) || ParamUtils.isNotNull(codigoPostal) || ParamUtils.isNotNull(paisDomicilio) || ParamUtils.isNotNull(provinciaDomicilio)
                || ParamUtils.isNotNull(poblacionDomicilio) || ParamUtils.isNotNull(estudioUJI) || ParamUtils.isNotNull(estudioNoUJI) || !estudioUJIBeca.isEmpty())
        {
            lista = clienteService.getClientesByBusqueda(connectedUserId, filtro, paginacion);
        }

        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
        responseMessage.setData(modelToUI(lista));
        return responseMessage;

    }

    private List<UIEntity> modelToUI(List<ClienteGrid> clientes) {

        List<UIEntity> lista = new ArrayList<>();

        for (ClienteGrid cliente : clientes)
        {
            UIEntity entity = modelToUI(cliente);
            lista.add(entity);
        }

        return lista;
    }

//    @DELETE
//    @Path("{id}")
//    @Consumes(MediaType.APPLICATION_JSON)
//    public void delete(@PathParam("id") Long clienteId) {
//
//        Long connectedUserId = AccessManager.getConnectedUserId(request);
//        clienteGeneralService.delete(connectedUserId, clienteId);
//    }

    private UIEntity modelToUI(ClienteGrid cliente) {
        UIEntity entity = UIEntity.toUI(cliente);
        entity.put("definitivo", cliente.getDefinitivo());

//        String nombreCompleto = cliente.getNombreApellidos();
//        if (!nombreCompleto.isEmpty()) {
//            entity.put("nombreCompleto", cliente.getNombreApellidos());
//        }
//        entity.put("promotorNombre", "");
//
//        Persona promotor = cliente.getPromotor();
//        if (promotor != null) {
//            entity.put("promotorNombre", promotor.getNombreApellidos());
//        }
//
//        if (cliente.getCorreoEstado() == null) {
//            entity.put("correoEstadoId", TipoEstadoConfirmacionDato.VALIDO.getId());
//        }
//
//        if (cliente.getMovilEstado() == null) {
//            entity.put("movilEstadoId", TipoEstadoConfirmacionDato.VALIDO.getId());
//        }
//
//        if (cliente.getPostalEstado() == null) {
//            entity.put("postalEstadoId", TipoEstadoConfirmacionDato.VALIDO.getId());
//        }

//        if (ParamUtils.isNotNull(cliente.getTipoIdentificacion())) {
//            entity.put("tipoIdentificacion", cliente.getTipoIdentificacion().getId());
//        }

        return entity;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public ResponseMessage getClientesGridById(@PathParam("id") Long clienteGeneralId) {

        Long connectedUserId = AccessManager.getConnectedUserId(request);


        List<ClienteGrid> lista = new ArrayList<>();


        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setTotalCount(1);
        responseMessage.setData(modelToUI(clienteService.getClientesGridByClienteGeneralId(connectedUserId, clienteGeneralId)));
        return responseMessage;

    }

    @GET
    @Path("excel")
    @Produces("application/vnd.ms-excel")
    public Response getExcelClientesGrid(@QueryParam("busqueda") String busqueda,
                                         @QueryParam("etiqueta") String etiqueta, @QueryParam("tipo") String tipo,
                                         @QueryParam("items") String itemsString, @QueryParam("campanya") Long campanya,
                                         @QueryParam("tipoDato") String tipoDato, @QueryParam("valorDato") String valor,
                                         @QueryParam("correo") String correo, @QueryParam("movil") String movil,
                                         @QueryParam("campanyaTipoEstado") Long campanyaTipoEstado, @QueryParam("postal") String postal,
                                         @QueryParam("programa") Long programa, @QueryParam("grupo") Long grupo,
                                         @QueryParam("tipoComparacion") Long tipoComparacion, @QueryParam("fecha") String fecha,
                                         @QueryParam("fechaMax") String fechaMax, @QueryParam("item") Long item,
                                         @QueryParam("tipoFecha") Long tipoFecha, @QueryParam("mes") Long mes,
                                         @QueryParam("anyo") Long anyo, @QueryParam("anyoMax") Long anyoMax,
                                         @QueryParam("mesMax") Long mesMax,
                                         @QueryParam("nacionalidad") String nacionalidad,
                                         @QueryParam("codigoPostal") String codigoPostal,
                                         @QueryParam("paisDomicilio") String paisDomicilio,
                                         @QueryParam("provinciaDomicilio") Long provinciaDomicilio,
                                         @QueryParam("poblacionDomicilio") Long poblacionDomicilio,
                                         @QueryParam("estudioUJI") Long estudioUJI,
                                         @QueryParam("estudioNoUJI") Long estudioNoUJI,
                                         @QueryParam("estudioUJIBeca") List<Long> estudioUJIBeca) throws Exception {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        FiltroCliente filtro = new FiltroCliente();
        filtro.setBusquedaGenerica(busqueda);
        filtro.setEtiqueta(etiqueta);
        filtro.setItems(itemsString);
        filtro.setTipoCliente(tipo);
        filtro.setCampanyaId(campanya);
        filtro.setCampanyaTipoEstado(campanyaTipoEstado);
        filtro.setValorDato(valor);
        filtro.setCorreo(correo);
        filtro.setMovil(movil);
        filtro.setPrograma(programa);
        filtro.setGrupo(grupo);
        filtro.setItem(item);
        filtro.setTipoComparacion(tipoComparacion);
        filtro.setTipoFecha(tipoFecha);
        filtro.setMes(mes);
        filtro.setAnyo(anyo);
        filtro.setMesMax(mesMax);
        filtro.setAnyoMax(anyoMax);
        filtro.setPostal(postal);
        filtro.setNacionalidad(nacionalidad);
        filtro.setCodigoPostal(codigoPostal);
        filtro.setPaisDomicilio(paisDomicilio);
        filtro.setProvinciaDomicilio(provinciaDomicilio);
        filtro.setPoblacionDomiclio(poblacionDomicilio);
        filtro.setEstudioUJI(estudioUJI);
        filtro.setEstudioUJIBeca(estudioUJIBeca);
        filtro.setEstudioNoUJI(estudioNoUJI);

        if (ParamUtils.isNotNull(fecha))
        {
            filtro.setFecha(utilsService.fechaParse(fecha.substring(0, 10), "yyyy-MM-dd"));
        }

        if (ParamUtils.isNotNull(fechaMax))
        {
            filtro.setFechaMax(utilsService.fechaParse(fechaMax.substring(0, 10), "yyyy-MM-dd"));
        }

        if (ParamUtils.isNotNull(tipoDato))
        {
            ClienteDatoTipo clienteDatoTipo = clienteDatoTipoService
                    .getClienteDatoTipoById(ParamUtils.parseLong(tipoDato));
            filtro.setTipoDato(clienteDatoTipo);
        }

        ByteArrayOutputStream ostream = clienteService.getExcelClientesGrid(connectedUserId, filtro);

        if (ostream != null) {
            return Response.ok(ostream.toByteArray(), MediaType.APPLICATION_OCTET_STREAM).header("content-disposition", "attachment; filename = datos_clientes_filtrados.xls").build();
        }
        throw new Exception("Sense dades");

    }
}