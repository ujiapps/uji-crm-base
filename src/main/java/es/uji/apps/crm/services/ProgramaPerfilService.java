package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ProgramaPerfilDAO;
import es.uji.apps.crm.model.ProgramaPerfil;

@Service
public class ProgramaPerfilService {
    private ProgramaPerfilDAO programaPerfilDAO;

    @Autowired
    public ProgramaPerfilService(ProgramaPerfilDAO programaPerfilDAO) {
        this.programaPerfilDAO = programaPerfilDAO;
    }

    public List<ProgramaPerfil> getProgramaPerfilesByProgramaId(Long programaId) {
        return programaPerfilDAO.getProgramaPerfilesByProgramaId(programaId);
    }

    public ProgramaPerfil getProgramaPerfilById(Long programaPerfilId) {
        return programaPerfilDAO.getProgramaPerfilById(programaPerfilId);
    }

    public ProgramaPerfil addProgramaPerfil(ProgramaPerfil programaPerfil) {
        programaPerfilDAO.insert(programaPerfil);
        programaPerfil.setId(programaPerfil.getId());
        return programaPerfil;
    }

    public void updateProgramaPerfil(ProgramaPerfil programaPerfil) {
        programaPerfilDAO.update(programaPerfil);
    }

    public void deleteProgramaPerfil(Long programaPerfilId) {
        programaPerfilDAO.delete(ProgramaPerfil.class, programaPerfilId);
    }

}