package es.uji.apps.crm.services;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.dao.EnvioCriterioClienteDAO;
import es.uji.apps.crm.exceptions.BusquedaVaciaException;
import es.uji.apps.crm.model.*;
import es.uji.apps.crm.model.domains.TipoFicheroExcel;
import es.uji.commons.rest.ResponseMessage;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.EnvioCriterioDAO;
import es.uji.apps.crm.ui.EnvioCriterioUI;
import es.uji.commons.rest.ParamUtils;

@Service
public class EnvioCriterioService {

    @InjectParam
    private ClienteService clienteService;

    private EnvioService envioService;
    private CampanyaService campanyaService;
    private EnvioCriterioDAO envioCriterioDAO;
    private EnvioCriterioCirculoService envioCriterioCirculoService;
    private EnvioCriterioEstudioUJIService envioCriterioEstudioUJIService;
    private EnvioCriterioCampanyaEstadosService envioCriterioCampanyaEstadosService;
    private EnvioCriterioEstudioNoUJIService envioCriterioEstudioNoUJIService;
    private EnvioCriterioEstudioUJIBecaService envioCriterioEstudioUJIBecaService;
    private EnvioCriterioClienteService envioCriterioClienteService;
    private EnvioCriterioClienteDAO envioCriterioClienteDAO;

    @Autowired
    public EnvioCriterioService(EnvioCriterioDAO envioCriterioDAO, EnvioService envioService, CampanyaService campanyaService, TipoEstadoCampanyaService tipoEstadoCampanyaService,
                                EnvioCriterioCirculoService envioCriterioCirculoService, EnvioCriterioCampanyaEstadosService envioCriterioCampanyaEstadosService,
                                EnvioCriterioEstudioUJIService envioCriterioEstudioUJIService, EnvioCriterioEstudioNoUJIService envioCriterioEstudioNoUJIService,
                                EnvioCriterioEstudioUJIBecaService envioCriterioEstudioUJIBecaService, EnvioCriterioClienteService envioCriterioClienteService,
                                EnvioCriterioClienteDAO envioCriterioClienteDAO) {
        this.envioCriterioDAO = envioCriterioDAO;
        this.envioService = envioService;
        this.campanyaService = campanyaService;
        this.envioCriterioCirculoService = envioCriterioCirculoService;
        this.envioCriterioCampanyaEstadosService = envioCriterioCampanyaEstadosService;
        this.envioCriterioEstudioUJIService = envioCriterioEstudioUJIService;
        this.envioCriterioEstudioNoUJIService = envioCriterioEstudioNoUJIService;
        this.envioCriterioEstudioUJIBecaService = envioCriterioEstudioUJIBecaService;
        this.envioCriterioClienteService = envioCriterioClienteService;
        this.envioCriterioClienteDAO = envioCriterioClienteDAO;
    }

    public void addEnvioCriteriosByEnvioId(EnvioCriterioUI envioCriterioUI) {

        Envio envio = envioService.getEnvioById(envioCriterioUI.getEnvio());

        EnvioCriterio envioCriterio = getEnvioCriterioByEnvioId(envio.getId());

        envioCriterio.setCampanya(null);
        envioCriterioCampanyaEstadosService.deleteEnvioCriterioCampanyaEstados(envioCriterio.getId());

        if (ParamUtils.isNotNull(envioCriterioUI.getCampanya())) {
            Campanya campanya = campanyaService.getCampanyaById(envioCriterioUI.getCampanya());
            envioCriterio.setCampanya(campanya);

            envioCriterioCampanyaEstadosService.insertEnvioCriterioCampanyaEstados(envioCriterioUI.getCampanyaEstados(), envioCriterio.getId());
        }

        if (ParamUtils.isNotNull(envioCriterioUI.getMesNacimiento())) {
            envioCriterio.setMesNacimiento(envioCriterioUI.getMesNacimiento().toString());
        } else {
            envioCriterio.setMesNacimiento(null);
        }

        if (ParamUtils.isNotNull(envioCriterioUI.getAnyoNacimientoDesde())) {
            envioCriterio.setAnyoNacimientoDesde(envioCriterioUI.getAnyoNacimientoDesde());
        } else {
            envioCriterio.setAnyoNacimientoDesde(null);
        }

        if (ParamUtils.isNotNull(envioCriterioUI.getAnyoNacimientoHasta())) {
            envioCriterio.setAnyoNacimientoHasta(envioCriterioUI.getAnyoNacimientoHasta());
        } else {
            envioCriterio.setAnyoNacimientoHasta(null);
        }

        envioCriterio.setCombinadoCampanyas(envioCriterioUI.getCombinadoCampanyas());
        envioCriterio.setTipoSeleccionCampanya(envioCriterioUI.getTipoSeleccionCampanya());

        if (ParamUtils.isNotNull(envioCriterio.getEnvio())) {
            envioCriterioDAO.update(envioCriterio);
        } else {
            envioCriterio.setEnvio(envio);
            envioCriterio = envioCriterioDAO.insert(envioCriterio);
        }


        if (envioCriterioUI.getCriteriosCirculo().size() > 0 && envioCriterioUI.getCriteriosCirculo().get(0) != null) {
            envioCriterioCirculoService.deleteEnvioCriteriosCirculo(envioCriterio.getId());
            envioCriterioCirculoService.insertEnvioCriteriosCirculo(envioCriterioUI.getCriteriosCirculo(), envioCriterio.getId());
        }

        if (envioCriterioUI.getCriteriosEstudioUJI().size() > 0 && envioCriterioUI.getCriteriosEstudioUJI().get(0) != null) {
            envioCriterioEstudioUJIService.deleteEnvioCriteriosEstudioUJI(envioCriterio.getId());
            envioCriterioEstudioUJIService.insertEnvioCriteriosEstudioUJI(envioCriterioUI.getCriteriosEstudioUJI(), envioCriterio.getId(), envioCriterioUI.getAnyoFinalizacionEstudioUJIInicio(),
                    envioCriterioUI.getAnyoFinalizacionEstudioUJIFin());
        }

        if (envioCriterioUI.getCriteriosEstudioUJIBeca().size() > 0 && envioCriterioUI.getCriteriosEstudioUJIBeca().get(0) != null) {
            envioCriterioEstudioUJIBecaService.deleteEnvioCriteriosEstudioUJIBeca(envioCriterio.getId());
            envioCriterioEstudioUJIBecaService.insertEnvioCriteriosEstudioUJIBeca(envioCriterioUI.getCriteriosEstudioUJIBeca(), envioCriterio.getId());
        }

        if (envioCriterioUI.getCriteriosEstudioNoUJI().size() > 0 && envioCriterioUI.getCriteriosEstudioNoUJI().get(0) != null) {
            envioCriterioEstudioNoUJIService.deleteEnvioCriteriosEstudioNoUJI(envioCriterio.getId());
            envioCriterioEstudioNoUJIService.insertEnvioCriteriosEstudioNoUJI(envioCriterioUI.getCriteriosEstudioNoUJI(), envioCriterio.getId(),
                    envioCriterioUI.getAnyoFinalizacionEstudioNoUJIInicio(), envioCriterioUI.getAnyoFinalizacionEstudioNoUJIFin());
        }

    }

    public EnvioCriterio getEnvioCriterioByEnvioId(Long envioId) {
        List<EnvioCriterio> envioCriterios = envioCriterioDAO.get(EnvioCriterio.class, "envio_id = " + envioId);
        if (envioCriterios.size() != 0) {
            return envioCriterios.get(0);
        } else {
            EnvioCriterio envioCriterio = new EnvioCriterio();
            Envio envio = new Envio();
            envio.setId(envioId);
            envioCriterio.setEnvio(envio);
            return envioCriterioDAO.insert(envioCriterio);
        }
    }

    public void deleteEnvioCriteriosByEnvioId(Long envioId) {

        EnvioCriterio envioCriterio = getEnvioCriterioByEnvioId(envioId);
        envioCriterioCirculoService.deleteEnvioCriteriosCirculo(envioCriterio.getId());
        envioCriterioEstudioUJIService.deleteEnvioCriteriosEstudioUJI(envioCriterio.getId());
        envioCriterioEstudioUJIBecaService.deleteEnvioCriteriosEstudioUJIBeca(envioCriterio.getId());
        envioCriterioEstudioNoUJIService.deleteEnvioCriteriosEstudioNoUJI(envioCriterio.getId());

        envioCriterioCampanyaEstadosService.deleteEnvioCriterioCampanyaEstados(envioCriterio.getId());

        envioCriterioDAO.delete(EnvioCriterio.class, "envio_id = " + envioId);
    }

    public void addEnvioCriterioClienteByEnvioId(Long envioId, Long clienteId) {
        envioCriterioClienteService.addEnvioCriterioClienteByEnvioId(envioId, clienteId);
    }

    public void deleteEnvioCriterioClienteByEnvioId(Long envioId, Long clienteId, String correo) {
        envioCriterioClienteService.deleteEnvioCriterioClienteByEnvioId(envioId, clienteId, correo);
    }

    public ResponseMessage addEnvioCriterioClienteArchivoByEnvioId(Long envioId, InputStream multiPart, TipoFicheroExcel tipoFicheroExcel, Long connectUserId) throws IOException, BusquedaVaciaException, ParseException {

        ResponseMessage response = new ResponseMessage();
        response.setSuccess(true);
        response.setMessage("Archiu processat");

        if (tipoFicheroExcel.equals(TipoFicheroExcel.XLS)) {
            valorExtractorFromXLS(envioId, multiPart, connectUserId);
            return response;

        }

        valorExtractorFromXLSX(envioId, multiPart, connectUserId);
        return response;
    }

    private void valorExtractorFromXLS(Long envioId, InputStream multiPart, Long connectUserId) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook(multiPart);
        HSSFSheet sheet = workbook.getSheetAt(0);

        Envio envio = new Envio();
        envio.setId(envioId);

        Iterator<Row> rowIterator = sheet.iterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (row.getCell(0) != null && row.getCell(0).getCellType() != CellType.BLANK) {
                String correo = "", identificacion = "";
                if (!row.getCell(0).getStringCellValue().isEmpty()) {
                    correo = row.getCell(0).getStringCellValue();
                }
                if (row.getCell(1) != null && row.getCell(1).getCellType() != CellType.BLANK) {
                    if (!row.getCell(1).getStringCellValue().isEmpty()) {
                        identificacion = row.getCell(1).getStringCellValue();
                    }
                }
                if (ParamUtils.isNotNull(correo)) {
                    EnvioCriterioCliente envioCriterioCliente = new EnvioCriterioCliente();
                    envioCriterioCliente.setCorreo(correo);
                    envioCriterioCliente.setAccion(1L);

                    envioCriterioCliente.setEnvio(envio);

                    if (ParamUtils.isNotNull(identificacion)) {

                        List<ClienteGrid> clientes = clienteService.getClientesGridByIdentificacion(connectUserId, identificacion);

                        if (clientes.size() > 0) {
                            for (ClienteGrid clienteGrid : clientes) {

                                EnvioCriterioCliente envioCriterioClienteIdentificacion = new EnvioCriterioCliente();
                                envioCriterioClienteIdentificacion.setCorreo(correo);
                                envioCriterioClienteIdentificacion.setAccion(1L);

                                envioCriterioClienteIdentificacion.setEnvio(envio);

                                Cliente cliente = new Cliente ();
                                cliente.setId(clienteGrid.getClienteId());
                                envioCriterioClienteIdentificacion.setCliente(cliente);
                                envioCriterioClienteDAO.insert(envioCriterioClienteIdentificacion);
                            }
                        }
                    }
                }
            }
        }
    }

    private void valorExtractorFromXLSX(Long envioId, InputStream multiPart, Long connectUserId) throws IOException {
        XSSFWorkbook workbook  = new XSSFWorkbook(multiPart);
        XSSFSheet sheet = workbook.getSheetAt(0);

        Envio envio = new Envio();
        envio.setId(envioId);

        Iterator<Row> rowIterator = sheet.iterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (row.getCell(0) != null && row.getCell(0).getCellType() != CellType.BLANK) {
                String correo = "", identificacion = "";
                if (!row.getCell(0).getStringCellValue().isEmpty()) {
                    correo = row.getCell(0).getStringCellValue();
                }
                if (row.getCell(1) != null && row.getCell(1).getCellType() != CellType.BLANK) {
                    if (!row.getCell(1).getStringCellValue().isEmpty()) {
                        identificacion = row.getCell(1).getStringCellValue();
                    }
                }
                if (ParamUtils.isNotNull(correo)) {
                    EnvioCriterioCliente envioCriterioCliente = new EnvioCriterioCliente();
                    envioCriterioCliente.setCorreo(correo);
                    envioCriterioCliente.setAccion(1L);

                    envioCriterioCliente.setEnvio(envio);

                    if (ParamUtils.isNotNull(identificacion)) {

                        List<ClienteGrid> clientes = clienteService.getClientesGridByIdentificacion(connectUserId, identificacion);

                        if (clientes.size() > 0) {
                            for (ClienteGrid clienteGrid : clientes) {

                                EnvioCriterioCliente envioCriterioClienteIdentificacion = new EnvioCriterioCliente();
                                envioCriterioClienteIdentificacion.setCorreo(correo);
                                envioCriterioClienteIdentificacion.setAccion(1L);

                                envioCriterioClienteIdentificacion.setEnvio(envio);

                                Cliente cliente = new Cliente ();
                                cliente.setId(clienteGrid.getClienteId());
                                envioCriterioClienteIdentificacion.setCliente(cliente);
                                envioCriterioClienteDAO.insert(envioCriterioClienteIdentificacion);
                            }
                        }
                        else {

                            envioCriterioClienteDAO.insert(envioCriterioCliente);
                        }
                    }
                    else {

                        envioCriterioClienteDAO.insert(envioCriterioCliente);
                    }
                }
            }
        }
    }
}