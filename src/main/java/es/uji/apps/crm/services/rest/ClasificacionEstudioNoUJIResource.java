package es.uji.apps.crm.services.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.TipoEstudio;
import es.uji.apps.crm.services.ClasificacionEstudioNoUJIService;
import es.uji.apps.crm.services.ItemClasificacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("clasificacionestudionouji")
public class ClasificacionEstudioNoUJIResource extends CoreBaseService {
    @InjectParam
    private ClasificacionEstudioNoUJIService clasificacionEstudioNoUJIService;

    @InjectParam
    private ItemClasificacionService itemClasificacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClasificacionEstudios() {
        return modelToUI(clasificacionEstudioNoUJIService.getClasificacionEstudios());
    }

    private List<UIEntity> modelToUI(List<TipoEstudio> tipoEstudios) {
        return tipoEstudios.stream().map(this::modelToUI).collect(Collectors.toList());
    }

    private UIEntity modelToUI(TipoEstudio tipoEstudio) {
        UIEntity entity = UIEntity.toUI(tipoEstudio);
//        entity.put("tieneItems", itemClasificacionService.getItemClasificacionByClasificacionId(tipoEstudio.getId()).isEmpty()?0:1);
        return entity;

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addClasificacionEstudiosNoUJI(UIEntity entity) {
        return modelToUI(clasificacionEstudioNoUJIService.addClasificacionEstudiosNoUJI(entity));
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public UIEntity updateClasificacionEstudiosNoUJI(UIEntity entity) {
        return modelToUI(clasificacionEstudioNoUJIService.updateClasificacionEstudiosNoUJI(entity));
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public void deleteClasificacionEstudioNoUJI(@PathParam("id") Long clasificacionEstudioId) {
        clasificacionEstudioNoUJIService.deleteClasificacionEstudioNoUji(clasificacionEstudioId);
    }
}