package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.EnvioPlantilla;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.services.EnvioPlantillaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("envioplantilla")
public class EnvioPlantillaResource extends CoreBaseService {
    @InjectParam
    private EnvioPlantillaService envioPlantillaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getEnvioPlantillas(@QueryParam("start") @DefaultValue("0") Long start,
                                              @QueryParam("limit") @DefaultValue("25") Long limit) {
        Paginacion paginacion = new Paginacion(start, limit);

        List<EnvioPlantilla> envioPlantillas = envioPlantillaService.getEnvioPlantillas(paginacion);

        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
        responseMessage.setData(UIEntity.toUI(envioPlantillas));

        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addEnvioPlantilla(@FormParam("nombre") String nombre, @FormParam("plantilla") String plantilla) {
        return UIEntity.toUI(envioPlantillaService.addEnvioPlantilla(nombre, plantilla));
    }

    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public UIEntity updateEnvioPlantilla(@PathParam("id") Long envioPlantillaId, @FormParam("nombre") String nombre, @FormParam("plantilla") String plantilla) {
        return UIEntity.toUI(envioPlantillaService.updateEnvioPlantilla(envioPlantillaId, nombre, plantilla));
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("autoguardado")
    public UIEntity autoGuardadoEnvioPlantilla(@FormParam("plantilla") String plantilla, @FormParam("nombre") String nombre) {
        return UIEntity.toUI(envioPlantillaService.autoGuardadoEnvioPlantilla(plantilla, nombre));
    }

    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("autoguardado")
    public UIEntity autoGuardadoEnvioPlantilla(@FormParam("envioPlantillaId") Long envioPlantillaId, @FormParam("plantilla") String plantilla, @FormParam("nombre") String nombre) {
        return UIEntity.toUI(envioPlantillaService.autoGuardadoEnvioPlantilla(envioPlantillaId, plantilla , nombre));
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public void deleteEnvioPlantilla(@PathParam("id") Long envioPlantillaId) {
        envioPlantillaService.deteleEnvioPlantilla(envioPlantillaId);
    }
}