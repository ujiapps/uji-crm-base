package es.uji.apps.crm.services.rest.alumni;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.auth.UserSessionManager;
import es.uji.apps.crm.services.*;
import es.uji.apps.crm.services.rest.AlumniAvisoResource;
import es.uji.apps.crm.services.rest.AlumniPremiumResource;
import es.uji.apps.crm.services.rest.PoblacionResource;
import es.uji.commons.web.template.Template;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.text.ParseException;

public class AlumniHashResource {

    private final static Boolean FICHAZONAPRIVADA = Boolean.TRUE;

    @Context
    protected HttpServletResponse response;
    @Context
    protected HttpServletRequest request;

    @InjectParam
    private TratamientoRGPDService tratamientoRGPDService;
    @InjectParam
    private AlumniService alumniService;
    @InjectParam
    private UserSessionManager userSessionManager;

    @Path("campanyas")
    public AlumniCampanyaResource getPlatformItem(
            @InjectParam AlumniCampanyaResource alumniCampanyaResource) {
        return alumniCampanyaResource;
    }

    @Path("laborales")
    public AlumniLaboralesResource getPlatformItem(
            @InjectParam AlumniLaboralesResource alumniLaboralesResource) {
        return alumniLaboralesResource;
    }

    @Path("academicos")
    public AlumniAcademicosResource getPlatformItem(
            @InjectParam AlumniAcademicosResource alumniAcademicosResource) {
        return alumniAcademicosResource;
    }

    @Path("espacio")
    public AlumniEspaciosResource getPlatformItem(
            @InjectParam AlumniEspaciosResource alumniEspaciosResource) {
        return alumniEspaciosResource;
    }

    @Path("personales")
    public AlumniPersonalesResource getPlatformItem(
            @InjectParam AlumniPersonalesResource alumniPersonalesResource) {
        return alumniPersonalesResource;
    }

    @Path("poblacion")
    public PoblacionResource getPlatformItem(
            @InjectParam PoblacionResource poblacionResource) {
        return poblacionResource;
    }

    @Path("premium")
    public AlumniPremiumResource getPlatformItem(
            @InjectParam AlumniPremiumResource alumniPremiumResource) {
        return alumniPremiumResource;
    }

    @Path("aviso")
    public AlumniAvisoResource getPlatformItem(
            @InjectParam AlumniAvisoResource alumniAvisoResource) {
        return alumniAvisoResource;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template getAlumni(@CookieParam("p_hash") String pHash,
                              @CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException {
        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);

        return alumniService.getZonaPrivadaAlumni(request.getPathInfo(), idioma);
    }

    @POST
    @Path("tratamiento")
    public String registraTratamiento(@CookieParam("p_hash") String
                                              pHash, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        return tratamientoRGPDService.registraTratamientoPLSQL("U004", userSessionManager.getConnectedUserId());
    }

    @GET
    @Path("logout")
    public Response desconectarUsuario(@CookieParam("p_hash") javax.ws.rs.core.Cookie cookie)
            throws URISyntaxException {

        return alumniService.deslogeaUsuarioZonaPrivada(request, response, cookie);

    }
}
