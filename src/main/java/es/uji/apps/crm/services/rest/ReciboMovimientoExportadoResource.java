package es.uji.apps.crm.services.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.ReciboMovimientoExportado;
import es.uji.apps.crm.services.ReciboMovimientoExportadoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("recibomovimientoexportado")
public class ReciboMovimientoExportadoResource extends CoreBaseService {

    @InjectParam
    private ReciboMovimientoExportadoService reciboMovimientoExportadoService;

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateMovimientoExportado(@PathParam("id") Long reciboMovimientoExportadoId) {

        ReciboMovimientoExportado reciboMovimientoExportado = reciboMovimientoExportadoService.getReciboMovimientoExportadoById(reciboMovimientoExportadoId);
        reciboMovimientoExportadoService.update(reciboMovimientoExportado);

        return modelToUI(reciboMovimientoExportado);
    }

    private UIEntity modelToUI(ReciboMovimientoExportado reciboMovimientoExportado) {
        return UIEntity.toUI(reciboMovimientoExportado);
    }
}