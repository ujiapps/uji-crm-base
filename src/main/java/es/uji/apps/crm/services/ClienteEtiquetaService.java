package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ClienteEtiquetaDAO;
import es.uji.apps.crm.model.ClienteEtiqueta;

@Service
public class ClienteEtiquetaService {

    private ClienteEtiquetaDAO clienteEtiquetaDAO;

    @Autowired
    public ClienteEtiquetaService(ClienteEtiquetaDAO clienteEtiquetaDAO) {
        this.clienteEtiquetaDAO = clienteEtiquetaDAO;
    }

    public List<ClienteEtiqueta> getClienteEtiquetasbyClienteId(Long clienteId) {
        return clienteEtiquetaDAO.getClienteEtiquetasByClienteId(clienteId);
    }

    public ClienteEtiqueta getClienteEtiquetaById(Long clienteEtiquetaId) {
        return clienteEtiquetaDAO.getClienteEtiquetaById(clienteEtiquetaId);
    }

    public ClienteEtiqueta addClienteEtiqueta(ClienteEtiqueta clienteEtiqueta) {
        clienteEtiquetaDAO.insert(clienteEtiqueta);
        clienteEtiqueta.setId(clienteEtiqueta.getId());
        return clienteEtiqueta;
    }

    public void updateClienteEtiqueta(ClienteEtiqueta clienteEtiqueta) {
        clienteEtiquetaDAO.update(clienteEtiqueta);
    }

    public void deleteClienteEtiqueta(Long clienteEtiquetaId) {
        clienteEtiquetaDAO.delete(ClienteEtiqueta.class, clienteEtiquetaId);
    }

    public Integer getCountClientesEtiquetasByEtiquetaId(Long envioTipoId, Long etiquetaId) {
        return clienteEtiquetaDAO.getCount(ClienteEtiqueta.class, "etiqueta_id = " + etiquetaId);
    }
}