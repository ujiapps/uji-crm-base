package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.TipoBecaDAO;
import es.uji.apps.crm.model.TipoBeca;

@Service
public class TipoBecaService {

    private TipoBecaDAO tipoBecaDAO;

    @Autowired
    public TipoBecaService(TipoBecaDAO tipoBecaDAO) {
        this.tipoBecaDAO = tipoBecaDAO;
    }

    public List<TipoBeca> getTiposBeca() {
        return tipoBecaDAO.getTiposBeca();
    }
}