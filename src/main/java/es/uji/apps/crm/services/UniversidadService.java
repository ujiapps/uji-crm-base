package es.uji.apps.crm.services;

import java.util.List;

import es.uji.apps.crm.ui.UniversidadUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.UniversidadDAO;
import es.uji.apps.crm.model.Universidad;

@Service
public class UniversidadService {
    private UniversidadDAO universidadDAO;

    @Autowired
    public UniversidadService(UniversidadDAO universidadDAO) {
        this.universidadDAO = universidadDAO;
    }

    public List<UniversidadUI> getUniversidades(String idioma) {
        return universidadDAO.getUniversidades(idioma);
    }
}