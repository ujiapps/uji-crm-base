package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.CampanyaTexto;
import es.uji.apps.crm.services.CampanyaTextoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("campanyatexto")
public class CampanyaTextoResource extends CoreBaseService {
    @InjectParam
    private CampanyaTextoService campanyaTextoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyasTextos() {
        List<CampanyaTexto> lista = campanyaTextoService.getCampanyasTextos();
        return UIEntity.toUI(lista);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addClienteDatoTipo(UIEntity entity) {
        CampanyaTexto campanyaTexto = UIToModel(entity);
        campanyaTextoService.addCampanyaTexto(campanyaTexto);
        return modelToUI(campanyaTexto);
    }

    private CampanyaTexto UIToModel(UIEntity entity) {
        return entity.toModel(CampanyaTexto.class);
    }

    private UIEntity modelToUI(CampanyaTexto campanyaTexto) {
        return UIEntity.toUI(campanyaTexto);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateClienteDatoTipo(@PathParam("id") Long id, UIEntity entity) {
        CampanyaTexto campanyaTexto = UIToModel(entity);
        campanyaTexto.setId(id);
        campanyaTextoService.updateCampanyaTexto(campanyaTexto);
        return modelToUI(campanyaTexto);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long id) {
        campanyaTextoService.deleteCampanyaTexto(id);
    }

}