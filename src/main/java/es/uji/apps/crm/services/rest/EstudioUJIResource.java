package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.EstudioUJI;
import es.uji.apps.crm.services.EstudioUJIService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("estudioUJI")
public class EstudioUJIResource extends CoreBaseService {
    @PathParam("clasificacionId")
    private Long clasificacionId;

    @InjectParam
    private EstudioUJIService estudioUJIService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEstudios() {
        List<EstudioUJI> lista = estudioUJIService.getEstudiosUJI(this.clasificacionId);
        return UIEntity.toUI(lista);
    }

//    @Path("grado")
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<UIEntity> getEstudiosGrado()
//    {
//        List<EstudioUJI> lista = estudioUJIService.getEstudiosUJI(this.clasificacionId, "G");
//        return UIEntity.toUI(lista);
//    }
//
//    @Path("postgrado")
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<UIEntity> getClasificacionEstudiosPostGrado()
//    {
//        List<EstudioUJI> lista = estudioUJIService.getEstudiosUJI(this.clasificacionId, "M");
//        return UIEntity.toUI(lista);
//    }
//
//    @Path("doctorado")
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<UIEntity> getClasificacionEstudiosDoctorado()
//    {
//        List<EstudioUJI> lista = estudioUJIService.getEstudiosUJI(this.clasificacionId, "D");
//        return UIEntity.toUI(lista);
//    }
}