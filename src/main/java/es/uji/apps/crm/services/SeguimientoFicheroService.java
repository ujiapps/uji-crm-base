package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.SeguimientoFicheroDAO;
import es.uji.apps.crm.model.SeguimientoFichero;

@Service
public class SeguimientoFicheroService {
    private SeguimientoFicheroDAO seguimientoFicheroDAO;

    @Autowired
    public SeguimientoFicheroService(SeguimientoFicheroDAO seguimientoFicheroDAO) {
        this.seguimientoFicheroDAO = seguimientoFicheroDAO;
    }

    public List<SeguimientoFichero> getSeguimientoFicherosByCampanyaSeguimientoId(
            Long campanyaSeguimientoId) {
        return seguimientoFicheroDAO
                .getSeguimientoFicherosByCampanyaSeguimientoId(campanyaSeguimientoId);
    }

    public SeguimientoFichero getSeguimientoFicheroById(Long seguimientoFicheroId) {
        return seguimientoFicheroDAO.getSeguimientoFicheroById(seguimientoFicheroId);
    }

    public SeguimientoFichero addSeguimientoFichero(SeguimientoFichero seguimientoFichero) {
        seguimientoFicheroDAO.insert(seguimientoFichero);
        seguimientoFichero.setId(seguimientoFichero.getId());
        return seguimientoFichero;
    }

    public void updateSeguimientoFichero(SeguimientoFichero seguimientoFichero) {
        seguimientoFicheroDAO.update(seguimientoFichero);
    }

    public void deleteSeguimientoFichero(Long seguimientoFicheroId) {
        seguimientoFicheroDAO.delete(SeguimientoFichero.class, seguimientoFicheroId);
    }

}