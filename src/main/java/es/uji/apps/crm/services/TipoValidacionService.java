package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.TipoValidacionDAO;
import es.uji.apps.crm.model.TipoValidacion;

@Service
public class TipoValidacionService {
    private TipoValidacionDAO tipoValidacionDAO;

    @Autowired
    public TipoValidacionService(TipoValidacionDAO tipoValidacionDAO) {
        this.tipoValidacionDAO = tipoValidacionDAO;
    }

    public List<TipoValidacion> getTiposValidacion() {

        return tipoValidacionDAO.getTiposValidacion();
    }

//    public TipoAcceso getTipoAccesoById(Long tipoAccesoId)
//    {
//        return tipoAccesoDAO.getTipoAccesoById(tipoAccesoId);
//    }
}