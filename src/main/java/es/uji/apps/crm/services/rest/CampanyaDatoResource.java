package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.CampanyaDato;
import es.uji.apps.crm.model.CampanyaFormulario;
import es.uji.apps.crm.model.ClienteDatoOpcion;
import es.uji.apps.crm.model.ClienteDatoTipo;
import es.uji.apps.crm.model.Grupo;
import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.model.TipoValidacion;
import es.uji.apps.crm.model.domains.TipoDato;
import es.uji.apps.crm.services.CampanyaDatoService;
import es.uji.apps.crm.services.ClienteDatoTipoService;
import es.uji.apps.crm.services.ItemService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("campanyadato")
public class CampanyaDatoResource extends CoreBaseService {
    @InjectParam
    private CampanyaDatoService campanyaDatoService;
    @InjectParam
    private ItemService itemService;
    @InjectParam
    private ClienteDatoTipoService clienteDatoTipoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyaDatos(@QueryParam("formularioId") Long formularioId) {
        return campanyaDatoService.getCampanyaDatosByFormularioId(formularioId).stream().map(this::modelToUI).collect(Collectors.toList());
    }

    private UIEntity modelToUI(CampanyaDato campanyaDato) {
        UIEntity entity = UIEntity.toUI(campanyaDato);

        entity.put("accesoNombre", campanyaDato.getTipoAcceso().getNombre());
        entity.put("clienteDatoTipoNombre", campanyaDato.getClienteDatoTipo().getNombre());
        entity.put("clienteDatoTipoTamanyo", campanyaDato.getClienteDatoTipo().getTamanyo());
        entity.put("clienteDatoTipoTipoId", campanyaDato.getClienteDatoTipo()
                .getClienteDatoTipoTipo().getId());

        if (campanyaDato.getCampanyaDato() != null)
        {
            entity.put("campanyaDatoExtra", campanyaDato.getCampanyaDato().getTextoCa());
            entity.put("campanyaDatoExtraId", campanyaDato.getCampanyaDato().getId());
        }

        entity.put("requerido", campanyaDato.getRequerido());
        entity.put("vinculadoCampanya", campanyaDato.getVinculadoCampanya());
        entity.put("visualizar", campanyaDato.getVisualizar());
        Item item = campanyaDato.getItem();
        entity.put("itemNombre", (item != null) ? item.getNombreCa() : "");

        List<CampanyaDato> relacionados = campanyaDatoService.getCampanyaDatoRelacionados(campanyaDato.getId());

        Grupo grupo = campanyaDato.getGrupo();
        if (grupo != null)
        {
            entity.put("grupoNombre", grupo.getNombreCa());
            for (Item itemGrupo : itemService.getItemsByGrupoId(grupo.getId()))
            {
                UIEntity uiItem = new UIEntity();
                uiItem.put("id", itemGrupo.getId());
                uiItem.put("nombreCa", itemGrupo.getNombreCa());


                List<String> mostrar = new ArrayList<>();
                List<String> ocultar = new ArrayList<>();

                for (CampanyaDato campanyaDato1 : relacionados)
                {
                    if (ParamUtils.isNotNull(campanyaDato1.getRespuestaRelacionada()) && campanyaDato1.getRespuestaRelacionada().getId().equals(itemGrupo.getId()))
                    {
                        mostrar.add(campanyaDato1.getId().toString());
                    }
                    else
                    {
                        ocultar.add(campanyaDato1.getId().toString());
                    }
                }

                uiItem.put("mostrar", mostrar);
                uiItem.put("ocultar", ocultar);
                entity.put("itemsGrupo", uiItem);
            }
        }
        else
        {
            entity.put("grupoNombre", "");
        }

        ClienteDatoTipo clienteDatoTipo = campanyaDato.getClienteDatoTipo();
        if (clienteDatoTipo.getClienteDatoTipoTipo().getId().equals(TipoDato.SELECCION.getId()))
        {
            List<ClienteDatoOpcion> opciones = clienteDatoTipoService
                    .getClienteDatoOpcionesByTipoId(clienteDatoTipo.getId());
            for (ClienteDatoOpcion opcion : opciones)
            {
                UIEntity uiItem = new UIEntity();
                uiItem.put("id", opcion.getId());
                uiItem.put("nombre", opcion.getEtiqueta());
                uiItem.put("valor", opcion.getValor());
                entity.put("opciones", uiItem);
            }
        }

        if (ParamUtils.isNotNull(campanyaDato.getTipoValidacion()))
        {
            entity.put("validacionValue", campanyaDato.getTipoValidacion().getValidacion());
        }

        return entity;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getCampanyaDatoById(@PathParam("id") Long campanyaDatoId) {
        CampanyaDato campanyaDato = campanyaDatoService.getCampanyaDatoById(campanyaDatoId);
        return modelToUI(campanyaDato);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanyaDato(UIEntity entity) {
        CampanyaDato campanyaDato = UIToModel(entity);
        campanyaDatoService.addCampanyaDato(campanyaDato);
        campanyaDato = campanyaDatoService.getCampanyaDatoById(campanyaDato.getId());
        return modelToUI(campanyaDato);
    }

    private CampanyaDato UIToModel(UIEntity entity) {
        CampanyaDato campanyaDato = entity.toModel(CampanyaDato.class);

        CampanyaFormulario formulario = new CampanyaFormulario();
        formulario.setId(ParamUtils.parseLong(entity.get("campanyaFormularioId")));
        campanyaDato.setCampanyaFormulario(formulario);

        ClienteDatoTipo clienteDatoTipo = new ClienteDatoTipo();
        clienteDatoTipo.setId(ParamUtils.parseLong(entity.get("clienteDatoTipoId")));
        campanyaDato.setClienteDatoTipo(clienteDatoTipo);

        Tipo tipoAcceso = new Tipo();
        tipoAcceso.setId(ParamUtils.parseLong(entity.get("tipoAccesoId")));
        campanyaDato.setTipoAcceso(tipoAcceso);

        String itemId = entity.get("itemId");
        if (ParamUtils.isNotNull(itemId))
        {
            Item item = new Item();
            item.setId(ParamUtils.parseLong(itemId));
            campanyaDato.setItem(item);
        }

        String grupoId = entity.get("grupoId");
        if (ParamUtils.isNotNull(grupoId))
        {
            Grupo grupo = new Grupo();
            grupo.setId(ParamUtils.parseLong(grupoId));
            campanyaDato.setGrupo(grupo);
        }

        String campanyaDatoExtraId = entity.get("campanyaDatoExtraId");
        if (ParamUtils.isNotNull(campanyaDatoExtraId))
        {
            CampanyaDato campanyaDatoExtra = new CampanyaDato();
            campanyaDatoExtra.setId(ParamUtils.parseLong(campanyaDatoExtraId));
            campanyaDato.setCampanyaDato(campanyaDatoExtra);
        }

        String tipoValidacionId = entity.get("tipoValidacionId");
        if (ParamUtils.isNotNull(tipoValidacionId))
        {
            TipoValidacion tipoValidacion = new TipoValidacion();
            tipoValidacion.setId(ParamUtils.parseLong(tipoValidacionId));
            campanyaDato.setTipoValidacion(tipoValidacion);
        }

        return campanyaDato;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateCampanyaDato(@PathParam("id") Long campanyaDatoId, UIEntity entity) {
        CampanyaDato campanyaDato = UIToModel(entity);
        campanyaDato.setId(campanyaDatoId);
        campanyaDatoService.updateCampanyaDato(campanyaDato);
        campanyaDato = campanyaDatoService.getCampanyaDatoById(campanyaDatoId);
        return modelToUI(campanyaDato);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long campanyaDatoId) {
        campanyaDatoService.deleteCampanyaDato(campanyaDatoId);
    }

    private List<UIEntity> modelToUI(List<CampanyaDato> campanyaDatos) {
        List<UIEntity> listaUI = new ArrayList<>();

        for (CampanyaDato campanyaDato : campanyaDatos)
        {
            UIEntity entity = modelToUI(campanyaDato);
            listaUI.add(entity);
        }

        return listaUI;
    }

}