package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.MovimientoReciboDAO;
import es.uji.apps.crm.model.MovimientoRecibo;

@Service
public class MovimientoReciboService {
    @Autowired
    private MovimientoReciboDAO movimientoReciboDAO;

    public List<MovimientoRecibo> getMovimientosReciboByReciboId(Long reciboId) {
        return movimientoReciboDAO.getMovimientosReciboByReciboId(reciboId);
    }
}