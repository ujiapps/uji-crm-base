package es.uji.apps.crm.services.rest.alumni;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.auth.UserSessionManager;
import es.uji.apps.crm.model.ClienteItem;
import es.uji.apps.crm.model.datos.Academicos;
import es.uji.apps.crm.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.util.List;

public class AlumniAcademicosResource extends CoreBaseService {

    private final static Boolean FICHAZONAPRIVADA = Boolean.TRUE;
    @InjectParam
    private ClienteItemService clienteItemService;
    @InjectParam
    private ClienteTitulacionesNoUJIService clienteTitulacionesNoUJIService;
    @InjectParam
    private AlumniService alumniService;
    @InjectParam
    private UserSessionManager userSessionManager;
    @InjectParam
    private AlumniAcademicosService alumniAcademicosService;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getAlumniAcademicos(@CookieParam("p_hash") String
                                                pHash, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        List<ClienteItem> listaItemsCliente = clienteItemService.getAllItemsByClienteId(userSessionManager.getCliente().getId());
        Academicos academicos = new Academicos(listaItemsCliente);
        return UIEntity.toUI(academicos);
    }

    @GET
    @Path("zonaprivada")
    @Produces(MediaType.TEXT_HTML)
    public Template getAlumniAcademicosTemplate(@CookieParam("p_hash") String pHash, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {
        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);

        if (ParamUtils.isNotNull(userSessionManager.getClienteGeneral())) {
            return alumniAcademicosService.getTemplateAcademicos(userSessionManager.getConnectedUserId(), idioma, request.getPathInfo());
        } else {
            return alumniService.getTemplateBaseAlumni(request.getPathInfo(), idioma, "errorusuario");
        }
    }

    @GET
    @Path("estudios")
    @Produces(MediaType.TEXT_HTML)
    public Template getAlumniAcademicosNoUJI(@CookieParam("p_hash") String pHash, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        return alumniAcademicosService.getTemplateAcademicosNoUJI(request.getPathInfo(), idioma, userSessionManager.getCliente().getId());
    }

    @DELETE
    @Path("zonaprivada/estudios/{id}")
    @Produces(MediaType.TEXT_HTML)
    public Template deleteAlumniAcademicosNoUJI(@CookieParam("p_hash") String
                                                        pHash, @CookieParam("uji-lang") @DefaultValue("ca") String idioma
            , @PathParam("id") Long estudioId) throws ParseException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        clienteTitulacionesNoUJIService.deleteAlumniAcademicosNoUJI(estudioId);
        return alumniAcademicosService.getTemplateAcademicosNoUJI(request.getPathInfo(), idioma, userSessionManager.getCliente().getId());
    }

    @POST
    @Path("zonaprivada/estudios")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity postEstudiosNoUJI(@CookieParam("p_hash") String pHash,
                                      @CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                      @FormParam("tipoEstudio") String tipoEstudio,
                                      @FormParam("ambitoEstudiosMediosId") Long ambitoEstudiosMediosId,
                                      @FormParam("estudiosNoUniversitariosId") Long estudiosNoUniversitariosId,
                                      @FormParam("gradoId") Long gradoId,
                                      @FormParam("ambitoPostGradoId") Long ambitoPostGradoId,
                                      @FormParam("ambitoDoctoradoId") Long ambitoDoctoradoId,
                                      @FormParam("nombreEstudio") String textoEstudio,
                                      @FormParam("anyoFinalizacion") Long anyoEstudio,
                                      @FormParam("universidadId") Long universidadId

    ) {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        alumniService.anyadirEstudio(userSessionManager.getCliente(), tipoEstudio, ambitoEstudiosMediosId, estudiosNoUniversitariosId, gradoId, ambitoPostGradoId, ambitoDoctoradoId, textoEstudio, anyoEstudio, universidadId);
        return new UIEntity();
    }

}
