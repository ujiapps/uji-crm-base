package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Grupo;
import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.model.TipoAcceso;
import es.uji.apps.crm.services.ItemService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("item")
public class ItemResource extends CoreBaseService {
    @InjectParam
    private ItemService itemService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItems(@QueryParam("grupoId") Long grupoId, @DefaultValue("false") @QueryParam("soloVisibles") boolean soloVisibles, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Item> lista = soloVisibles ? itemService.getItemsVisibles(connectedUserId, grupoId) : itemService.getItems(connectedUserId, grupoId);
        return modelToUI(lista, idioma);
    }

    private List<UIEntity> modelToUI(List<Item> items, String idioma) {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        for (Item item : items)
        {
            lista.add(modelToUI(item, idioma));
        }
        return lista;

    }

    private UIEntity modelToUI(Item item, String idioma) {
        UIEntity entity = UIEntity.toUI(item);

        entity.put("visible", item.getVisible());
        entity.put("grupoNombre", item.getGrupo().getNombreCa());
        entity.put("tipoAccesoNombre", item.getTipoAcceso().getNombre());
        entity.put("nombre", item.getNombreByIdioma(idioma));
        return entity;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("datoCampanya")
    public List<UIEntity> getItemsByDatoCampanya(@QueryParam("datoCampanya") Long datoCampanya, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Item> lista = itemService.getItemsByDatoCampanya(connectedUserId, datoCampanya);
        return modelToUI(lista, idioma);
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getItemById(@PathParam("id") Long itemId) {
        return UIEntity.toUI(itemService.getItemById(itemId));
    }

    @GET
    @Path("campanya/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItemsByCampanyaId(@PathParam("id") Long campanyaId, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        return modelToUI(itemService.getItemsAsociadosByCampanyaId(campanyaId), idioma);
    }

    @GET
    @Path("estudio/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItemsByEstudioId(@PathParam("id") Long estudioId, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        return modelToUI(itemService.getItemsByEstudioId(estudioId), idioma);
    }

    @GET
    @Path("envio/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItemsByEnvioId(@PathParam("id") Long envioId) {
        return UIEntity.toUI(itemService.getItemsByEnvioId(envioId));
    }

    @GET
    @Path("cliente/{clienteId}/faltan")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItemsByClienteIdFaltan(@PathParam("clienteId") Long clienteId,
                                                    @QueryParam("grupoId") Long grupoId, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Item> lista = itemService.getItemsByClienteIdFaltan(connectedUserId, clienteId, grupoId);
        return modelToUI(lista, idioma);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addItem(UIEntity entity) {
        Item item = UIToModel(entity);
        itemService.addItem(item);
        return UIEntity.toUI(item);
    }

    private Item UIToModel(UIEntity entity) {
        Item item = entity.toModel(Item.class);

        TipoAcceso tipoAcceso = new TipoAcceso();
        tipoAcceso.setId(Long.parseLong(entity.get("tipoAccesoId")));
        item.setTipoAcceso(tipoAcceso);

        Tipo correo = new Tipo();
        correo.setId(Long.parseLong(entity.get("correoId")));
        item.setCorreo(correo);

        Tipo sms = new Tipo();
        sms.setId(Long.parseLong(entity.get("smsId")));
        item.setSms(sms);

        Tipo aplicacionMovil = new Tipo();
        aplicacionMovil.setId(Long.parseLong(entity.get("aplicacionMovilId")));
        item.setAplicacionMovil(aplicacionMovil);

        Tipo correoPostal = new Tipo();
        correoPostal.setId(Long.parseLong(entity.get("correoPostalId")));
        item.setCorreoPostal(correoPostal);

        Grupo grupo = new Grupo();
        grupo.setId(Long.parseLong(entity.get("grupoId")));
        item.setGrupo(grupo);

        return item;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateItem(@PathParam("id") Long itemId, UIEntity entity) {
        Item item = UIToModel(entity);
        item.setId(itemId);
        itemService.updateItem(item);
        return UIEntity.toUI(item);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long itemId) {
        itemService.deleteItem(itemId);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("preinscripcion")
    public List<UIEntity> getItemsPreInscripcion(@QueryParam("grupoId") @DefaultValue("-1") Long grupoId) {
        List<Item> lista = itemService.getItemsByGrupoId(grupoId);
        return UIEntity.toUI(lista);
    }
}