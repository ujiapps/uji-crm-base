package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Grupo;
import es.uji.apps.crm.services.GrupoService;
import es.uji.commons.rest.UIEntity;

public class CampanyaGrupoSubResource {
    @PathParam("campanyaId")
    Long campanyaId;
    @InjectParam
    private GrupoService grupoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGrupos() {
        List<Grupo> gruposAsociadosByCampanyaId = grupoService
                .getGruposAsociadosByCampanyaId(campanyaId);
        return modelToUI(gruposAsociadosByCampanyaId);
    }

    private List<UIEntity> modelToUI(List<Grupo> grupos) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (Grupo grupo : grupos)
        {
            listaUI.add(modelToUI(grupo));
        }

        return listaUI;
    }

    private UIEntity modelToUI(Grupo grupo) {
        UIEntity entity = UIEntity.toUI(grupo);

        entity.put("visible", grupo.getVisible());
        entity.put("programaNombre", grupo.getPrograma().getNombreCa());
        entity.put("tipoAccesoNombre", grupo.getTipoAcceso().getNombre());

        return entity;
    }
}