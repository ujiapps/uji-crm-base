package es.uji.apps.crm.services.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;
import com.sun.syndication.io.FeedException;

import es.uji.apps.crm.model.CampanyaActo;
import es.uji.apps.crm.services.CampanyaActoService;
import es.uji.apps.crm.services.rest.eventos.FeedEventos;
import es.uji.apps.crm.services.rest.eventos.Metadato;
import es.uji.apps.crm.services.rest.eventos.rss.ContenidoRSS;
import es.uji.apps.crm.services.rest.eventos.rss.Enclosure;
import es.uji.apps.crm.services.rest.eventos.rss.FeedRSS;

@Path("rss")
public class RssResource {
    @Context
    protected HttpServletResponse response;

    @InjectParam
    CampanyaActoService campanyaActoService;

    @GET
    public Response rss() throws FeedException, ParseException {
        FeedEventos feed = new FeedEventos();

        List<CampanyaActo> actosRss = campanyaActoService.getCampanyaActosRss();

        FeedRSS items = new FeedRSS();

        for (CampanyaActo campanyaActo : actosRss)
        {

            ContenidoRSS item = new ContenidoRSS();

            List<Metadato> metaDatos = new ArrayList<Metadato>();

            Metadato tipo = new Metadato("tipo", "tipo", "ActoGraduacion");
            metaDatos.add(tipo);

            Metadato asientos = new Metadato("seientsnumerats", "seientsnumerats", "no");
            metaDatos.add(asientos);

            Enclosure imagen = new Enclosure();
            imagen.setType(campanyaActo.getImagenTipo());
            imagen.setUrl("https://ujiapps.uji.es/ade/rest/storage/" + campanyaActo.getImagenReferencia());

            List<Enclosure> imagenes = new ArrayList<Enclosure>();
            imagenes.add(imagen);
            item.setEnclosures(imagenes);

            item.setMetadatos(metaDatos);
            item.setContenido(campanyaActo.getContenido());
            item.setContenidoId(campanyaActo.getId());
            item.setDuracion(campanyaActo.getDuracion().toString());
            item.setFecha(campanyaActo.getFecha().toString().substring(0, 10));
            item.setHora(campanyaActo.getHora());
            item.setResumen(campanyaActo.getResumen());
            item.setTitulo(campanyaActo.getTitulo());
            item.setHoraApertura(campanyaActo.getHoraApertura());

            items.put("rss" + item, item);
        }

        feed.addEntries(items);


//        Document prueba = feed.getXML();
        return Response.ok(feed.getXML()).type("text/xml").build();
    }
}