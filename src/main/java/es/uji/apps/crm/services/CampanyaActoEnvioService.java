package es.uji.apps.crm.services;

import es.uji.apps.crm.dao.CampanyaActoEnvioDAO;
import es.uji.apps.crm.model.*;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CampanyaActoEnvioService {

    private CampanyaActoEnvioDAO campanyaActoEnvioDAO;
    private CampanyaEnvioModeloService campanyaEnvioModeloService;

    @Autowired
    public CampanyaActoEnvioService(CampanyaActoEnvioDAO campanyaActoEnvioDAO, CampanyaEnvioModeloService campanyaEnvioModeloService) {
        this.campanyaActoEnvioDAO = campanyaActoEnvioDAO;
        this.campanyaEnvioModeloService = campanyaEnvioModeloService;
    }

    public CampanyaActoEnvio getCampanyasActoEnvioByCampanyaActo(Long campanyaActoId) {
        return campanyaActoEnvioDAO.getCampanyasActoEnvioByCampanyaActo(campanyaActoId);
    }

    public void addCampanyaActoEnvio(CampanyaActo campanyaActo) {
        CampanyaEnvioModelo campanyaEnvioModelo = campanyaEnvioModeloService.getCampanyaEnvioModelo("ACTO-ENTRADAS");
        CampanyaActoEnvio campanyaActoEnvio = new CampanyaActoEnvio();
        campanyaActoEnvio.setCampanyaActo(campanyaActo);
        String cuerpo = campanyaEnvioModelo.getCuerpo();

        campanyaActoEnvio.setCuerpo(cuerpo);
        campanyaActoEnvio.setAsunto("MODIFICAR -- " + campanyaEnvioModelo.getAsunto());
        campanyaActoEnvio.setDesde(campanyaEnvioModelo.getDesde());
        campanyaActoEnvio.setResponder(campanyaEnvioModelo.getResponder());

        campanyaActoEnvioDAO.insert(campanyaActoEnvio);
    }

    public CampanyaActoEnvio getCampanyaActoEnvioByCampanyaActoId(Long actoId) {
        return campanyaActoEnvioDAO.getCampanyasActoEnvioByCampanyaActo(actoId);
    }

    public CampanyaActoEnvio addCampanyaActoEnvio(Long campanyaActoId, String asunto, String cuerpo, String desde, String responder) {
        CampanyaActoEnvio campanyaActoEnvio = new CampanyaActoEnvio();
        campanyaActoEnvio.setAsunto(asunto);
        campanyaActoEnvio.setCuerpo(cuerpo);
        campanyaActoEnvio.setDesde(desde);
        campanyaActoEnvio.setResponder(responder);

        CampanyaActo campanyaActo = new CampanyaActo();
        campanyaActo.setId(campanyaActoId);
        campanyaActoEnvio.setCampanyaActo(campanyaActo);

        return campanyaActoEnvioDAO.insert(campanyaActoEnvio);
    }

    public CampanyaActoEnvio updateCampanyaActoEnvio(Long campanyaActoEnvioId, String asunto, String cuerpo, String desde, String responder) {
        CampanyaActoEnvio campanyaActoEnvio = getCampanyaActoEnvioById(campanyaActoEnvioId);

        campanyaActoEnvio.setAsunto(asunto);
        campanyaActoEnvio.setCuerpo(cuerpo);
        campanyaActoEnvio.setDesde(desde);
        campanyaActoEnvio.setResponder(responder);
        return campanyaActoEnvioDAO.update(campanyaActoEnvio);
    }

    private CampanyaActoEnvio getCampanyaActoEnvioById(Long campanyaActoEnvioId) {
        return campanyaActoEnvioDAO.getCampanyaActoEnvioById(campanyaActoEnvioId);
    }

    public void deleteCampanyaActoEnvio(Long campanyaActoId) {
        campanyaActoEnvioDAO.delete(CampanyaActoEnvio.class, "campanya_acto_id = " + campanyaActoId);
    }
}
