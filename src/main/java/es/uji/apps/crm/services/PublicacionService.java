package es.uji.apps.crm.services;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.*;

import es.uji.apps.crm.model.*;
import es.uji.apps.crm.ui.FormularioUI;
import es.uji.apps.crm.utils.AppInfo;
import es.uji.commons.web.template.Template;
import org.springframework.stereotype.Service;
import com.sun.jersey.api.core.InjectParam;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Service
public class PublicacionService extends CoreBaseService {
    @Context
    protected HttpServletResponse response;

    @InjectParam
    private FormularioService formularioService;
    @InjectParam
    private SesionService sesionService;

    //    @Value("${uji.crm.domain}")
    private String domain = "uji.es";

    public Response activarSesionPorHash(String pHash, String urlFormulario) throws URISyntaxException {

        if (ParamUtils.isNotNull(sesionService.getUserIdActiveSesion(pHash, Boolean.FALSE))) {
            Cookie hashCookie = new Cookie("p_hash", pHash);
            hashCookie.setPath("/");
            hashCookie.setHttpOnly(true);
            hashCookie.setDomain(domain);
            response.addCookie(hashCookie);
            URI url = new URI(urlFormulario);
            return Response.seeOther(url).build();
        } else {
            URI url = new URI(AppInfo.getHost() + "/crm/rest/publicacion/error_hash");
            return Response.seeOther(url).build();
        }
    }

    public Response getFormulario(Long campanyaId, Long clienteId) throws URISyntaxException {
        CampanyaFormulario formulario;

        formulario = formularioService.getByCampanyaId(campanyaId, clienteId);
        URI url = new URI(AppInfo.getHost() + "/crm/rest/publicacion/fail");

        if (ParamUtils.isNotNull(formulario)) {
            url = new URI(AppInfo.getHost() + "/crm/rest/publicacion/formulario/" + formulario.getId());

            if (formulario.getAutenticaFormulario() && (clienteId.equals(-1L) || !ParamUtils.isNotNull(clienteId))) {
                url = new URI(AppInfo.getHost() + "/crm/rest/publicacionprincipal/" + campanyaId);
            }
        }
        return Response.seeOther(url).build();
    }

    public Template dameTemplate(FormularioUI formulario, String cookieIdioma) throws ParseException {

        String idioma = getIdioma(cookieIdioma);

        if (!ParamUtils.isNotNull(formulario)) {
            String templateUrl = "crm/main";
            Template template = AppInfo.buildPagina(templateUrl, idioma);
            template.put("contenido", "/crm/formulario-no-encontrado");
            return template;
        }

        Date hoy = new Date();

        if (hoy.after(formulario.getFechaInicioFormulario()) && hoy.before(formulario.getFechaFinFormulario())) {

            String templateUrl = "crm/main";
            Template template = AppInfo.buildPagina(templateUrl, idioma);
            template.put("formulario", formulario);
            template.put("contenido", "/crm/form/form");
            template.put("campanyaId", formulario.getCampanya());
//            template.put("connectedUserId", connectedUserId);
            if (ParamUtils.isNotNull(formulario.getCabecera())) {
                template.put("cabecera", formulario.getCabecera());
            } else
                template.put("cabecera", "//static.uji.es/templates/uji2016_plantillas/static/images/logouji30a.jpg");
            template.put("plantilla", formulario.getPlantilla());
            return template;
        } else {
            String templateUrl = "crm/formulario-fuera-plazo";
            Template template = AppInfo.buildPagina(templateUrl, idioma);
            template.put("formulario", formulario);
            return template;
        }
    }

    public String getIdioma(String cookieIdioma) {
        if (ParamUtils.isNotNull(cookieIdioma)) {
            return cookieIdioma.toUpperCase();
        } else {
            return "CA";
        }
    }


}
