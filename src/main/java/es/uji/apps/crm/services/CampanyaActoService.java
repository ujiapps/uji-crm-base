package es.uji.apps.crm.services;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import es.uji.apps.crm.model.CampanyaI18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import es.uji.apps.crm.dao.CampanyaActoDAO;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaActo;

import javax.imageio.ImageIO;
import javax.ws.rs.core.MediaType;

@Service
public class CampanyaActoService {

    @Value("${uji.par.apiToken}")
    private String apiToken;
    @Value("${uji.par.url}")
    private String url;

    private final CampanyaActoDAO campanyaActoDAO;
    private final CampanyaActoEnvioService campanyaActoEnvioService;
    private final ADEClienteService adeClienteService;


    @Autowired
    public CampanyaActoService(CampanyaActoDAO campanyaActoDAO, CampanyaActoEnvioService campanyaActoEnvioService, ADEClienteService adeClienteService) {
        this.campanyaActoDAO = campanyaActoDAO;
        this.campanyaActoEnvioService = campanyaActoEnvioService;
        this.adeClienteService = adeClienteService;
    }


    public List<CampanyaActo> getCampanyasActoByCampanya(Campanya campanya) {
        return campanyaActoDAO.getCampanyasActoByCampanya(campanya);

    }

    public CampanyaActo updateCampanyaActo(CampanyaActo campanyaActo) {
        return campanyaActoDAO.update(campanyaActo);
    }

    public void deleteCampanyaActo(Long campanyaActoId) {
        campanyaActoEnvioService.deleteCampanyaActoEnvio(campanyaActoId);
        campanyaActoDAO.delete(CampanyaActo.class, campanyaActoId);
    }

    public List<CampanyaActo> getCampanyaActosRss() {
        return campanyaActoDAO.getCampanyasActoRss();
    }

    public CampanyaActo getCampanyasActoById(Long campanyaActoId) {
        return campanyaActoDAO.getCampanyaActoById(campanyaActoId);
    }

    public void addCampanyaActoGenericaActo(Campanya campanya) {

        CampanyaActo campanyaActo = new CampanyaActo(campanya);
        addCampanyaActo(campanyaActo);

        campanyaActoEnvioService.addCampanyaActoEnvio(campanyaActo);

    }

    public CampanyaActo addCampanyaActo(CampanyaActo campanyaActo) {
        campanyaActoDAO.insert(campanyaActo);
        campanyaActo.setId(campanyaActo.getId());
        return campanyaActo;
    }

    public ClientResponse generarEvento(CampanyaActo acto) {
        Map<String, Object> jsonObject = new HashMap<>();
        List<CampanyaI18N> titulos = new ArrayList<>();
        CampanyaI18N tituloCa = new CampanyaI18N("ca", acto.getTitulo());
        CampanyaI18N tituloEs = new CampanyaI18N("es", acto.getTitulo());
        titulos.add(tituloCa);
        titulos.add(tituloEs);

        List<CampanyaI18N> descripciones = new ArrayList<>();
        CampanyaI18N descripcionCa = new CampanyaI18N("ca", acto.getResumen());
        CampanyaI18N descripcionEs = new CampanyaI18N("es", acto.getResumen());
        descripciones.add(descripcionCa);
        descripciones.add(descripcionEs);

        List<CampanyaI18N> duraciones = new ArrayList<>();
        CampanyaI18N duracionCa = new CampanyaI18N("ca", String.valueOf(acto.getDuracion()));
        CampanyaI18N duracionEs = new CampanyaI18N("es", String.valueOf(acto.getDuracion()));
        duraciones.add(duracionCa);
        duraciones.add(duracionEs);

        //Required
        jsonObject.put("id", String.valueOf(acto.getId()));
        jsonObject.put("asientosNumerados", false);
        jsonObject.put("entradasNominales", false);
        jsonObject.put("publico", false);
        jsonObject.put("tipoEvento", "ActoGraduacion");
        jsonObject.put("titulos", titulos);

        //Optional
        jsonObject.put("duracion", duraciones);
        jsonObject.put("descripciones", descripciones);
        BufferedImage imagen = adeClienteService.getImagen(acto.getImagenReferencia());
        if (imagen != null) {
            String imagenBase64 = imageToBase64String(imagen, acto.getImagenTipo().split("/")[1]);
            jsonObject.put("eventImageBase64", imagenBase64);
        }

        BufferedImage imagenPubli = adeClienteService.getImagen(acto.getImagenPubliReferencia());
        if (imagenPubli != null) {
            String imagenPubliBase64 = imageToBase64String(imagenPubli, acto.getImagenPubliTipo().split("/")[1]);
            jsonObject.put("eventPubliImageBase64", imagenPubliBase64);
        }
        return Client.create().resource(url)
                .entity(jsonObject, "application/json")
                .type(MediaType.APPLICATION_JSON)
                .header("x-api-key", apiToken)
                .post(ClientResponse.class);
    }

    public ClientResponse generarSesion(CampanyaActo acto) {
        Map<String, Object> jsonObject = new HashMap<>();
        jsonObject.put("dateTime", new SimpleDateFormat("yyyy-MM-dd").format(acto.getFecha()) + " " + acto.getHora());
        jsonObject.put("opening", acto.getHoraApertura());

        return Client.create().resource(url + acto.getId() + "/session")
                .entity(jsonObject, "application/json")
                .type(MediaType.APPLICATION_JSON)
                .header("x-api-key", apiToken)
                .post(ClientResponse.class);
    }

    public void activarRssEvento(CampanyaActo acto) {
        campanyaActoDAO.activarRssEvento(acto.getId());
    }

    private String imageToBase64String(BufferedImage imagen, String tipoImagen) {
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(imagen, tipoImagen, os);
            String result = Base64.getEncoder().encodeToString(os.toByteArray());
            os.close();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}