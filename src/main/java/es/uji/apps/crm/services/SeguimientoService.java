package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.SeguimientoDAO;
import es.uji.apps.crm.model.Seguimiento;

@Service
public class SeguimientoService {
    private SeguimientoDAO seguimientoDAO;

    @Autowired
    public SeguimientoService(SeguimientoDAO seguimientoDAO) {
        this.seguimientoDAO = seguimientoDAO;
    }

    public List<Seguimiento> getSeguimientosByClienteId(Long clienteId) {
        return seguimientoDAO.getSeguimientosByClienteId(clienteId);
    }

    public Seguimiento getSeguimientoById(Long seguimientoId) {
        return seguimientoDAO.getSeguimientoById(seguimientoId);
    }

    public List<Seguimiento> getSeguimientosByCampanyaId(Long campanyaId) {
        return seguimientoDAO.getSeguimientosByCampanyaId(campanyaId);
    }

    public Seguimiento addSeguimiento(Seguimiento seguimiento) {
//        seguimientoDAO.insert(seguimiento);
//        seguimiento.setId(seguimiento.getId());
        return seguimientoDAO.insert(seguimiento);
    }

    public Seguimiento updateSeguimiento(Seguimiento seguimiento) {
        return seguimientoDAO.update(seguimiento);
    }

    public void deleteSeguimiento(Long seguimientoId) {
        seguimientoDAO.delete(Seguimiento.class, seguimientoId);
    }

    public List<Seguimiento> getSeguimientosByClienteIdAndCampanyaId(Long clienteId, Long campanyaId) {
        return seguimientoDAO.getSeguimientosByClienteIdAndCampanyaId(clienteId, campanyaId);
    }
}