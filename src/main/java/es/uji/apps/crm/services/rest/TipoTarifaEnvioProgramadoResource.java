package es.uji.apps.crm.services.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.TipoTarifaEnvio;
import es.uji.apps.crm.model.TipoTarifaEnvioProgramado;
import es.uji.apps.crm.services.TipoTarifaEnvioProgramadoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("tipotarifaenvioprogramado")
public class TipoTarifaEnvioProgramadoResource extends CoreBaseService {

    @InjectParam
    private TipoTarifaEnvioProgramadoService tipoTarifaEnvioProgramadoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTipoTarifasEnvioByTipoTarifa(@QueryParam("tipoTarifaEnvioId") Long tipoTarifaEnvioId) {
        TipoTarifaEnvio tipoTarifaEnvio = new TipoTarifaEnvio();
        tipoTarifaEnvio.setId(tipoTarifaEnvioId);
        return modelToUI(tipoTarifaEnvioProgramadoService.getTipoTarifaEnvioProgramadoByTipoTarifa(tipoTarifaEnvio));
    }

    private List<UIEntity> modelToUI(List<TipoTarifaEnvioProgramado> tipoTarifaEnvioProgramados) {
        return tipoTarifaEnvioProgramados.stream().map(this::modelToUI).collect(Collectors.toList());
    }

    private UIEntity modelToUI(TipoTarifaEnvioProgramado tipoTarifaEnvioProgramado) {
        return UIEntity.toUI(tipoTarifaEnvioProgramado);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addTipoTarifaEnvioProgramado(UIEntity entity) {
        TipoTarifaEnvioProgramado tipoTarifaEnvioProgramado = UIToModel(entity);
        tipoTarifaEnvioProgramadoService.addTipoTarifaEnvioProgramado(tipoTarifaEnvioProgramado);
        return modelToUI(tipoTarifaEnvioProgramado);
    }

    private TipoTarifaEnvioProgramado UIToModel(UIEntity entity) {

        TipoTarifaEnvioProgramado tipoTarifaEnvioProgramado = entity.toModel(TipoTarifaEnvioProgramado.class);

        TipoTarifaEnvio tipoTarifaEnvio = new TipoTarifaEnvio();
        tipoTarifaEnvio.setId(ParamUtils.parseLong(entity.get("tarifaTipoEnvioId")));

        tipoTarifaEnvioProgramado.setTarifaTipoEnvio(tipoTarifaEnvio);

        return tipoTarifaEnvioProgramado;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateTipoTarifaEnvioProgramado(UIEntity entity) {

        TipoTarifaEnvioProgramado tipoTarifaEnvioProgramado = UIToModel(entity);
        tipoTarifaEnvioProgramadoService.updateTipoTarifaEnvioProgramado(tipoTarifaEnvioProgramado);
        return modelToUI(tipoTarifaEnvioProgramado);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long tarifaEnvioProgramadoId) {
        tipoTarifaEnvioProgramadoService.deleteTipoTarifaEnvioProgramado(tarifaEnvioProgramadoId);
    }
}