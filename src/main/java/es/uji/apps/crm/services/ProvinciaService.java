package es.uji.apps.crm.services;

import java.util.List;

import es.uji.apps.crm.ui.OpcionUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ProvinciaDAO;
import es.uji.apps.crm.model.Provincia;

@Service
public class ProvinciaService {
    private ProvinciaDAO provinciaDAO;

    @Autowired
    public ProvinciaService(ProvinciaDAO provinciaDAO) {
        this.provinciaDAO = provinciaDAO;
    }

    public List<Provincia> getProvincias(String idioma) {
        return provinciaDAO.getProvincias(idioma);
    }

    public List<OpcionUI> getOpcionProvincias(String idioma) {
        return provinciaDAO.getOpcionProvincias(idioma);
    }


}