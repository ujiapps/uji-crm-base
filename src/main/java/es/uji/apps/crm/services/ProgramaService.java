package es.uji.apps.crm.services;

import es.uji.apps.crm.dao.*;
import es.uji.apps.crm.exceptions.ErrorEnBorradoDeRegistroException;
import es.uji.apps.crm.model.Grupo;
import es.uji.apps.crm.model.Programa;
import es.uji.apps.crm.utils.TreeRow;
import es.uji.apps.crm.utils.TreeRowset;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class ProgramaService {
    private ProgramaDAO programaDAO;

    private GrupoDAO grupoDAO;

    private ItemDAO itemDAO;

    private ClienteItemDAO clienteItemDAO;

    private CampanyaClienteDAO campanyaClienteDAO;

    @Autowired
    public ProgramaService(ProgramaDAO programaDAO, GrupoDAO grupoDAO, ItemDAO itemDAO,
                           ClienteItemDAO clienteItemDAO, CampanyaClienteDAO campanyaClienteDAO) {
        this.programaDAO = programaDAO;
        this.grupoDAO = grupoDAO;
        this.itemDAO = itemDAO;
        this.clienteItemDAO = clienteItemDAO;
        this.campanyaClienteDAO = campanyaClienteDAO;
    }

    public List<Programa> getProgramas(Long connectedUserId) {
        return programaDAO.getProgramas(connectedUserId);
    }

    public List<Programa> getProgramasAdmin(Long connectedUserId) {
        return programaDAO.getProgramasAdmin(connectedUserId);
    }

    public List<Programa> getProgramasUser(Long connectedUserId) {
        return programaDAO.getProgramasUser(connectedUserId);
    }

    public Programa getProgramaById(Long programaId) {
        return programaDAO.getProgramaById(programaId);
    }

    public List<Programa> getProgramasByServicioId(Long servicioId) {
        return programaDAO.getProgramasByServicioId(servicioId);
    }

    public Programa addPrograma(Programa programa) {
        programaDAO.insert(programa);
        programa.setId(programa.getId());
        return programa;
    }

    public void updatePrograma(Programa programa) {
        programaDAO.updatePrograma(programa);
    }

    public void delete(Long programaId) throws ErrorEnBorradoDeRegistroException {
        try {
            programaDAO.delete(Programa.class, programaId);
        } catch (DataIntegrityViolationException | ConstraintViolationException ex) {
            throw new ErrorEnBorradoDeRegistroException("No s'ha pogut esborrar correctament, el registre té elements associats.");
        }
    }

    public List<Programa> getProgramasYGrupos(Long clienteId) {
        return programaDAO.getProgramasYGrupos(clienteId);
    }

    public TreeRowset creaArbolSinChecked(List<Programa> programaList) {
        TreeRowset rowset = new TreeRowset();

        for (Programa dato : programaList) {
            if (dato.getGrupos().size() > 0) {
                TreeRow row = toTreeSinChecked(dato);
                rowset.getRow().add(row);
            }
        }

        return rowset;
    }

    public TreeRow toTreeSinChecked(Programa dato) {
        TreeRow row = new TreeRow();
        row.setId(dato.getId().toString());
        row.setText(dato.getNombreCa());
        if (dato.getGrupos().size() > 0) {
            row.setLeaf("false");
            List<TreeRow> lista = new ArrayList<TreeRow>();
            Iterator<Grupo> iterator = dato.getGrupos().iterator();
            while (iterator.hasNext()) {
                Grupo grupo = iterator.next();
                lista.add(toTreeSinCheckedGrupo(grupo));
            }
            row.setHijos(lista);

        } else {
            row.setLeaf("true");
        }
        return row;
    }

    public TreeRow toTreeSinCheckedGrupo(Grupo dato) {
        TreeRow row = new TreeRow();
        row.setId(dato.getId().toString());
        row.setText(dato.getNombreCa());
        row.setLeaf("true");
        return row;
    }
}