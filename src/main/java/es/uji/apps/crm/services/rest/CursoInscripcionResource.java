package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.CursoInscripcion;
import es.uji.apps.crm.services.CursoInscripcionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;


@Path("cursoinscripcion")
public class CursoInscripcionResource extends CoreBaseService {
    @InjectParam
    private CursoInscripcionService cursoInscripcionService;

    @GET
    @Path("persona/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCursosInscripcionesByPersonaId(@PathParam("id") Long personaId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<CursoInscripcion> lista = cursoInscripcionService.getCursosInscripcionesByPersonaId(connectedUserId, personaId);
        return modelToUI(lista);
    }

    private List<UIEntity> modelToUI(List<CursoInscripcion> lista) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (CursoInscripcion cursoInscripcion : lista)
        {
            listaUI.add(modelToUI(cursoInscripcion));
        }
        return listaUI;

    }

    private UIEntity modelToUI(CursoInscripcion cursoInscripcion) {
        UIEntity entity = UIEntity.toUI(cursoInscripcion);
        entity.put("anulaInscripcion", cursoInscripcion.getAnulaInscripcion());
        entity.put("aprovechamiento", cursoInscripcion.getAprovechamiento());
        entity.put("asistencia", cursoInscripcion.getAsistencia());
        entity.put("confirmacion", cursoInscripcion.getConfirmacion());
        entity.put("espera", cursoInscripcion.getEspera());

        return entity;
    }
}