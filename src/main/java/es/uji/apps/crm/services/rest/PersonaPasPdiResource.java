package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.services.PersonaPasPdiService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("personapaspdi")
public class PersonaPasPdiResource extends CoreBaseService {
    @InjectParam
    private PersonaPasPdiService consultaPersonaPasPdi;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPersonasbyServicioId(@PathParam("id") Long servicioId) {
        return UIEntity.toUI(consultaPersonaPasPdi.getPersonasByServicioId(servicioId));
    }

}