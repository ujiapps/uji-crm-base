package es.uji.apps.crm.services.rest;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.uji.apps.crm.ui.RecursoUI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.crm.exceptions.ErrorEnBorradoDeDocumentoException;
import es.uji.apps.crm.exceptions.ErrorSubiendoDocumentoException;
import es.uji.apps.crm.exceptions.FicheroException;
import es.uji.apps.crm.model.Envio;
import es.uji.apps.crm.model.EnvioImagen;
import es.uji.apps.crm.model.TipoTarifaEnvio;
import es.uji.apps.crm.services.ADEClienteService;
import es.uji.apps.crm.services.EnvioImagenService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StreamUtils;
import es.uji.commons.rest.UIEntity;


@Component
@Path("envioimagen")
public class EnvioImagenResource extends CoreBaseService {
    private static String host;
    @InjectParam
    private EnvioImagenService envioImagenService;
    @InjectParam
    private ADEClienteService adeClienteService;

    @Value("${uji.crm.host}")
    public void setHost(String host) {
        this.host = host;
    }

    @GET
    public List<UIEntity> getEnvioImagenesGenericos() {
        return UIEntity.toUI(envioImagenService.getEnvioImagenesGenerico(""));
    }

    @GET
    @Path("{id}")
    public List<UIEntity> getEnvioImagenesByEnvioId(@PathParam("id") Long envioId) {
        return UIEntity.toUI(envioImagenService.getEnvioImagenesByEnvioId(envioId));
    }

    @GET
    @Path("tarifaenvio/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEnvioImagenesByTarifaEnvioId(@PathParam("id") Long tarifaEnvioId) {
        return UIEntity.toUI(envioImagenService.getEnvioImagenesByTarifaEnvioId(tarifaEnvioId));
    }

    @GET
    @Path("visualizar/tarifaenvio/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getWebEnvioImagenesByTarifaEnvioId(@PathParam("id") Long tarifaEnvioId, @QueryParam("CKEditorFuncNum") Long editor,
                                                       @QueryParam("query") String search)
            throws ParseException {

        UIEntity uiEntity = new UIEntity();
        String urlAde = adeClienteService.getUrl();
        if (!search.equals("") && search.contains("*")) {
            search = search.replace("*", "");
        }
        List<EnvioImagen> envioImagenesGenericas = envioImagenService.getEnvioImagenesGenerico(search);
        List<EnvioImagen> envioImagenes = envioImagenService.getEnvioImagenesByTarifaEnvioId(tarifaEnvioId);
        envioImagenes.addAll(envioImagenesGenericas);
        List<RecursoUI> result = envioImagenService.imageToRecursoUI(envioImagenes, urlAde);
        uiEntity.put("numDocumentos", envioImagenes.size());
        uiEntity.put("resultados", UIEntity.toUI(result));
        return uiEntity;
    }

    @GET
    @Path("visualizar/")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getWebEnvioImagenesGenerico(@QueryParam("CKEditorFuncNum") Long editor,
                                                @QueryParam("query") String search) throws ParseException {

        search = search.replace("*", "");
        UIEntity uiEntity = new UIEntity();
        String urlAde = adeClienteService.getUrl();
        List<EnvioImagen> envioImagenes = envioImagenService.getEnvioImagenesGenerico(search);
        List<RecursoUI> result = envioImagenService.imageToRecursoUI(envioImagenes, urlAde);
        uiEntity.put("numDocumentos", envioImagenes.size());
        uiEntity.put("resultados", UIEntity.toUI(result));
        return uiEntity;
    }

    @GET
    @Path("visualizar/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getWebEnvioImagenesByEnvioId(@PathParam("id") Long envioId, @QueryParam("CKEditorFuncNum") Long editor,
                                                 @QueryParam("query") String search)
            throws ParseException {
        if (!search.equals("") && search.contains("*")) {
            search = search.replace("*", "");
        }
        UIEntity uiEntity = new UIEntity();
        String urlAde = adeClienteService.getUrl();
        List<EnvioImagen> envioImagenesGenericas = envioImagenService.getEnvioImagenesGenerico(search);
        List<EnvioImagen> envioImagenes = envioImagenService.getEnvioImagenesByEnvioId(envioId);
        envioImagenes.addAll(envioImagenesGenericas);

        List<RecursoUI> result = envioImagenService.imageToRecursoUI(envioImagenes, urlAde);
        uiEntity.put("numDocumentos", envioImagenes.size());
        uiEntity.put("resultados", UIEntity.toUI(result));
        return uiEntity;

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity addEnvioImagenGenerico(FormDataMultiPart multiPart)
            throws FicheroException, IOException, ErrorSubiendoDocumentoException {

        return UIEntity.toUI(addImagen(multiPart));
    }

    private EnvioImagen addImagen(FormDataMultiPart multiPart)
            throws IOException, FicheroException, ErrorSubiendoDocumentoException {
        EnvioImagen envioImagen = new EnvioImagen();

        Map<String, Object> datosDocumento = extraeDatosDeMultipart(multiPart);
        envioImagen.setNombre(datosDocumento.get("nombre").toString());

        String ref = adeClienteService.addDocumento(datosDocumento.get("nombre").toString(),
                datosDocumento.get("mimetype").toString(),
                (byte[]) datosDocumento.get("contenido"));
        //String ref = "PRUEBAS";
        envioImagen.setReferencia(ref);
        envioImagenService.addEnvioImagen(envioImagen);
        return envioImagen;
    }

    private Map<String, Object> extraeDatosDeMultipart(FormDataMultiPart multiPart)
            throws FicheroException, IOException {
        Map<String, Object> datos = new HashMap<>();

        String fileName = "";
        String mimeType = "";
        InputStream documento = null;

        for (BodyPart bodyPart : multiPart.getBodyParts())
        {
            mimeType = bodyPart.getHeaders().getFirst("Content-Type");
            if (mimeType.contains("image"))
            {
                if (ParamUtils.isNotNull(mimeType))
                {
                    String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                    Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                    Matcher m = fileNamePattern.matcher(header);
                    if (m.matches())
                    {
                        fileName = m.group(1);
                    }
                    BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                    documento = bpe.getInputStream();
                }
            }
            else
            {
                throw new FicheroException("El archivo no es una imagen");
            }
        }

        if (ParamUtils.isNotNull(documento))
        {
            datos.put("nombre", fileName);
            datos.put("mimetype", mimeType);
            datos.put("contenido", StreamUtils.inputStreamToByteArray(documento));
        }

        return datos;
    }

    @POST
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity addEnvioImagen(@PathParam("id") Long envioId, FormDataMultiPart multiPart)
            throws FicheroException, IOException, ErrorSubiendoDocumentoException {

        Envio envio = new Envio();
        envio.setId(envioId);

        EnvioImagen envioImagen = addImagen(multiPart);
        envioImagen.setEnvio(envio);

        return UIEntity.toUI(envioImagenService.updateEnvioImagen(envioImagen));
    }

    @POST
    @Path("tarifaenvio/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity addTarifaEnvioImagen(@PathParam("id") Long tarifaEnvioId, FormDataMultiPart multiPart)
            throws FicheroException, IOException, ErrorSubiendoDocumentoException {

        TipoTarifaEnvio tarifaEnvio = new TipoTarifaEnvio();
        tarifaEnvio.setId(tarifaEnvioId);

        EnvioImagen envioImagen = addImagen(multiPart);
        envioImagen.setTarifaEnvio(tarifaEnvio);

        return UIEntity.toUI(envioImagenService.updateEnvioImagen(envioImagen));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long envioImagenId)
            throws MalformedURLException, ErrorEnBorradoDeDocumentoException {

        EnvioImagen envioImagen = envioImagenService.getEnvioImagenesById(envioImagenId);

        adeClienteService.deleteDocumento(envioImagen.getReferencia());

        envioImagenService.deleteEnvioImagen(envioImagenId);

    }

}