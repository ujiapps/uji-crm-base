package es.uji.apps.crm.services.rest.eventos;

public class Metadato {
    private String clave;
    private String titulo;
    private String valor;

    public Metadato(String clave, String titulo, String valor) {
        this.valor = valor;
        this.titulo = titulo;
        this.clave = clave;
    }


    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
