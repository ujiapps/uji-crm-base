package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Etiqueta;
import es.uji.apps.crm.services.EtiquetaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("etiqueta")
public class EtiquetaResource extends CoreBaseService {
    @InjectParam
    private EtiquetaService etiquetaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEtiquetas(@QueryParam("busqueda") String busqueda) {
        List<Etiqueta> lista = etiquetaService.getEtiquetas(busqueda);

        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (Etiqueta etiqueta : lista)
        {
            listaUI.add(UIEntity.toUI(etiqueta));
        }
        return listaUI;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getEtiquetaById(@PathParam("id") Long etiquetaId) {
        return UIEntity.toUI(etiquetaService.getEtiquetaById(etiquetaId));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addEtiqueta(UIEntity entity) {
        Etiqueta etiqueta = UIToModel(entity);
        etiquetaService.addEtiqueta(etiqueta);
        return UIEntity.toUI(etiqueta);
    }

    private Etiqueta UIToModel(UIEntity entity) {
        return entity.toModel(Etiqueta.class);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateEtiqueta(@PathParam("id") Long etiquetaId, UIEntity entity) {
        Etiqueta etiqueta = UIToModel(entity);
        etiqueta.setId(etiquetaId);
        etiquetaService.updateEtiqueta(etiqueta);
        return UIEntity.toUI(etiqueta);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long etiquetaId) {
        etiquetaService.deleteEtiqueta(etiquetaId);
    }
}