package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.crm.dao.CampanyaClienteArchivoDAO;
import es.uji.apps.crm.dao.FicheroCsv;
import es.uji.apps.crm.model.CampanyaClienteArchivo;
import es.uji.apps.crm.model.Paginacion;

@Service
public class CampanyaClienteArchivoService {

    private CampanyaClienteArchivoDAO campanyaClienteArchivoDAO;

    @Autowired
    public CampanyaClienteArchivoService(CampanyaClienteArchivoDAO campanyaClienteArchivoDAO) {
        this.campanyaClienteArchivoDAO = campanyaClienteArchivoDAO;
    }

    public CampanyaClienteArchivo addCampanyaClienteArchivoService(CampanyaClienteArchivo campanyaClienteArchivo) {
        return campanyaClienteArchivoDAO.insert(campanyaClienteArchivo);
    }

    @Transactional
    public void anyadePersonasACampanya(Long connectedUserId, Long campanyaId, Boolean nacimiento, Boolean uji, Boolean direccionPostal, Boolean nacionalidad, Long estado,
                                        Boolean correoPersonal, Boolean zonaPrivada) {


        campanyaClienteArchivoDAO.updateCampanyaClienteArchivo(campanyaId, connectedUserId);

        FicheroCsv ficheroCsv = new FicheroCsv();
        ficheroCsv.init();
        ficheroCsv.altaCampanya(connectedUserId, campanyaId, nacimiento, uji, direccionPostal, nacionalidad, estado, correoPersonal, zonaPrivada);
    }

    public List<CampanyaClienteArchivo> getListaPersonasSinAnyadirACampanya(Long campanyaId, Paginacion paginacion) {

        return campanyaClienteArchivoDAO.getListaPersonasSinAnyadirACampanya(campanyaId, paginacion);
    }

    public void deleteCampanyaClienteArchivoByCampanyaId(Long campanyaId) {
        campanyaClienteArchivoDAO.delete(CampanyaClienteArchivo.class, "campanya_id = " + campanyaId);
    }

    public void delete(Long personaFicheroId) {
        campanyaClienteArchivoDAO.delete(CampanyaClienteArchivo.class, personaFicheroId);
    }
}