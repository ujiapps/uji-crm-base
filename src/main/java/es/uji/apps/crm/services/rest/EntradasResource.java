package es.uji.apps.crm.services.rest;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.commons.rest.ResponseMessage;
import org.codehaus.jettison.json.JSONException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.VwEntradaCrm;
import es.uji.apps.crm.services.EntradasService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Component
@Path("entradas")
public class EntradasResource extends CoreBaseService {

    @Value("${uji.par.apiToken}")
    private String apiToken;
    @Value("${uji.par.url}")
    private String url;

    @InjectParam
    private EntradasService entradasService;

    @GET
    @Path("infoentradas/{actoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonNode obtenerInfoEntradasActo(@PathParam("actoId") Long actoId) {
        ClientResponse response = Client.create().resource(url + actoId + "/statistics")
                .header("X-API-KEY", apiToken)
                .get(ClientResponse.class);

        if (response.getStatus() == 200)
        {
            return response.getEntity(JsonNode.class);
        }
        return null;

    }

    @GET
    @Path("export")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCSVCampanyaActo (@QueryParam("campanyaActoId") Long campanyaActoId){

        List<VwEntradaCrm> lista = entradasService.obtenerInfoEntradasCrm(campanyaActoId);
        Response.ResponseBuilder res = null;

        res = Response.ok();
        res.header("Content-Disposition", "attachment; filename = infoEntradas.csv");

        res.entity(entradasService.entradasToCSV(lista));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();
    }

    @GET
    @Path("crm/{actoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> obtenerInfoEntradasCrm(@PathParam("actoId") Long actoId) {

        return modelToUI(entradasService.obtenerInfoEntradasCrm(actoId));

    }

//    @GET
//    @Path("falloActo/{actoId}")
//    public void enviarEntradasFallo (@PathParam("actoId") Long actoId) throws ParseException, UnsupportedEncodingException, JSONException {
//
//
//        entradasService.enviarEntradasFallo(actoId);
//    }

    private List<UIEntity> modelToUI(List<VwEntradaCrm> entradas) {
        return entradas.stream().map(this::modelToUI).collect(Collectors.toList());
    }

    private UIEntity modelToUI(VwEntradaCrm entrada) {

        UIEntity entity = UIEntity.toUI(entrada);
        entity.put("dadas", entrada.getDadas());

        return entity;

    }

    @POST
    @Path("todas")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity asignarEntradasNuevas(@FormParam("actoId") Long actoId)
            throws ParseException, UnsupportedEncodingException, JSONException {

        entradasService.asignarEntradasNuevas(actoId);
        return null;
    }

    @POST
    @Path("nueva")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage asignarEntradasNuevaAUsuario(@FormParam("campanyaActoId") Long campanyaActoId, @FormParam("clienteId") Long clienteId, @FormParam("numEntradas") Long numEntradas,
                                                        @FormParam("desde") String desde, @FormParam("responder") String responder, @FormParam("asunto") String asunto,
                                                        @FormParam("cuerpo") String cuerpo) throws ParseException, UnsupportedEncodingException, JSONException {

        return entradasService.asignarEntradasNuevaAUsuario(campanyaActoId, clienteId, numEntradas, desde, responder, asunto, cuerpo);
    }
}