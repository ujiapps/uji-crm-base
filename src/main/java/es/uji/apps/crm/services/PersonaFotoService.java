package es.uji.apps.crm.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.PersonaFotoDAO;
import es.uji.apps.crm.model.PersonaFoto;

@Service
public class PersonaFotoService {
    private PersonaFotoDAO personaFotoDAO;

    @Autowired
    public PersonaFotoService(PersonaFotoDAO personaFotoDAO) {
        this.personaFotoDAO = personaFotoDAO;
    }

    public PersonaFoto getPersonaFotoByClienteId(Long clienteId) {
        return personaFotoDAO.getPersonaFotoByClienteId(clienteId);
    }

}