package es.uji.apps.crm.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ReciboRemesaExportadoDAO;
import es.uji.apps.crm.model.ReciboRemesaExportado;

@Service
public class ReciboRemesaExportadoService {

    @Autowired
    private ReciboRemesaExportadoDAO reciboRemesaExportadoDAO;


    public ReciboRemesaExportado addRemesa() {

        ReciboRemesaExportado reciboRemesaExportado = new ReciboRemesaExportado();
        reciboRemesaExportado.setFecha(new Date());
        reciboRemesaExportadoDAO.insert(reciboRemesaExportado);

        return reciboRemesaExportado;
    }
}