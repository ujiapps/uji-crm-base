package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.crm.dao.ActualizaPersona;
import es.uji.apps.crm.exceptions.SoloSeAdmitenFicherosJpegException;
import es.uji.apps.crm.model.*;
import es.uji.apps.crm.model.domains.TipoAccesoDatoUsuario;
import es.uji.apps.crm.services.ClienteDatoService;
import es.uji.apps.crm.services.ClienteService;
import es.uji.apps.crm.services.PersonaFotoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;


public class ClienteFotoResource extends CoreBaseService {

    @PathParam("id")
    private Long clienteId;
    @InjectParam
    private ClienteDatoService consultaClienteDato;
    @InjectParam
    private PersonaFotoService personaFotoService;
    @InjectParam
    private ActualizaPersona actualizaPersona;
    @InjectParam
    private ClienteService clienteService;

    private Long TIPOFOTO = 3996634L;

    @Context
    ServletContext servletContext;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getClienteFotos(@PathParam("id") Long clienteId) throws Exception {
        Cliente cliente = new Cliente();
        cliente.setId(clienteId);
        ClienteDato clienteDato = consultaClienteDato.getClienteDatoByClienteAndTipoId(cliente, TIPOFOTO);
        UIEntity ui = new UIEntity();
        if (ParamUtils.isNotNull(clienteDato) && ParamUtils.isNotNull(clienteDato.getValorFicheroMimetype()) && ParamUtils.isNotNull(clienteDato.getValorFicheroBinario())) {
            String foto = "data:" + clienteDato.getValorFicheroMimetype().toLowerCase() + ";base64," + new String(Base64.getEncoder().encode(clienteDato.getValorFicheroBinario()));
            ui.put("binarioCRM", foto);
        }

        PersonaFoto personaFoto = personaFotoService.getPersonaFotoByClienteId(clienteId);
        if (ParamUtils.isNotNull(personaFoto))
        {
            String foto2 = "data:" + personaFoto.getMimeType().toLowerCase() + ";base64," + new String(Base64.getEncoder().encode(personaFoto.getBinario()));
            ui.put("binarioPersona", foto2);
        }
        return ui;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseMessage addFotoCRM(FormDataMultiPart form)
            throws IOException, SoloSeAdmitenFicherosJpegException {

        String mimeType = form.getBodyParts().get(0).getHeaders().getFirst("Content-Type");

        if (!mimeType.equals("image/jpeg") && !mimeType.equals("image/jpg")) {
            throw new SoloSeAdmitenFicherosJpegException();
        } else {
            String nombre = form.getBodyParts().get(0).getContentDisposition().getFileName();

            BodyPartEntity fileV = (BodyPartEntity) form.getBodyParts().get(0).getEntity();
            InputStream documento = fileV.getInputStream();

            if(ParamUtils.isNotNull(documento) && nombre!=null){
                byte[] contenido = IOUtils.toByteArray(documento);
                Cliente cliente = new Cliente();
                cliente.setId(clienteId);
                ClienteDato clienteDato = consultaClienteDato.getClienteDatoByClienteAndTipoId(cliente, TIPOFOTO);

                if(ParamUtils.isNotNull(clienteDato)){
                    clienteDato.setValorFicheroMimetype(mimeType);
                    clienteDato.setValorFicheroNombre(nombre);
                    clienteDato.setValorFicheroBinario(contenido);
                    consultaClienteDato.update(clienteDato);
                }else{
                    clienteDato = new ClienteDato();

                    clienteDato.setCliente(cliente);
                    ClienteDatoTipo clienteDatoTipo = new ClienteDatoTipo();
                    clienteDatoTipo.setId(TIPOFOTO);
                    clienteDato.setClienteDatoTipo(clienteDatoTipo);

                    Tipo tipo = new Tipo();
                    tipo.setId(TipoAccesoDatoUsuario.INTERNO.getId());
                    clienteDato.setClienteDatoTipoAcceso(tipo);

                    clienteDato.setValorFicheroMimetype(mimeType);
                    clienteDato.setValorFicheroNombre(nombre);
                    clienteDato.setValorFicheroBinario(contenido);

                    consultaClienteDato.insertaClienteDatoFichero(clienteDato);
                }
            }
            return new ResponseMessage(true);
        }
    }

    @GET
    @Path("descargar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDescargarFotoCRM(@PathParam("id") Long clienteId) throws Exception {
        Cliente cliente = new Cliente();
        cliente.setId(clienteId);
        ClienteDato clienteDato = consultaClienteDato.getClienteDatoByClienteAndTipoId(cliente, TIPOFOTO);
        String nombreFichero = null;
        String contentType = null;
        byte[] data = clienteDato.getValorFicheroBinario();
        if (data != null)
        {
            nombreFichero = clienteDato.getValorFicheroNombre();
            contentType = clienteDato.getValorFicheroMimetype();
            return Response.ok(data)
                    .header("Content-Disposition", "attachment; filename = \"" + nombreFichero + "\"")
                    .header("Content-Length", data.length).header("Content-Type", contentType).build();
        }

        return Response.ok().build();
    }

    @POST
    @Path("validar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response verificaFotoCRM(@PathParam("id") Long clienteId) throws Exception {
        actualizaPersona.guardaFotoCarnet(clienteId);
        return Response.ok().build();
    }


}