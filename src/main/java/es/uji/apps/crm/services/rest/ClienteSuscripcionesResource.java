package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.services.ClienteSuscripcionesService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

public class ClienteSuscripcionesResource extends CoreBaseService {

    @InjectParam
    private ClienteSuscripcionesService clienteSuscripcionesService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItemsClientesSuscripcionesByCliente(@QueryParam("clienteId") Long clienteId) {
        ParamUtils.checkNotNull(clienteId);
        List<UIEntity> res = clienteSuscripcionesService.getListaItemsUnificada(clienteId);
//        List<UIEntity> res = unificaListas(modelToUI(clienteSuscripcionesService.getItemsClientesSuscripcionesByCliente(clienteId)), clienteItemResource.getClientesItemsByClienteId(clienteId));
        return res;
    }


}