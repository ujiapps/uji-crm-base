package es.uji.apps.crm.services;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.crm.model.*;
import es.uji.apps.crm.ui.ClienteTitulacionNoUjiUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ClienteTitulacionesNoUJIDAO;

@Service
public class ClienteTitulacionesNoUJIService {

    private final Tipo tipo;
    private ClienteTitulacionesNoUJIDAO clienteTitulacionesNoUJIDAO;

    @Autowired
    public ClienteTitulacionesNoUJIService(ClienteTitulacionesNoUJIDAO clienteTitulacionesNoUJIDAO, Tipo tipo) {

        this.clienteTitulacionesNoUJIDAO = clienteTitulacionesNoUJIDAO;
        this.tipo = tipo;
    }


    public List<ClienteTitulacionNoUjiUI> getClienteEstudiosNoUJIByPersona(Long clienteId, String idioma) {

        return clienteTitulacionesNoUJIDAO.getClienteEstudiosNoUJIByPersona(clienteId).stream().map(clienteTitulacionesNoUJI -> {
            return new ClienteTitulacionNoUjiUI(clienteTitulacionesNoUJI, idioma);
        }).collect(Collectors.toList());
    }

    public List<ClienteTitulacionesNoUJI> getClienteEstudiosNoUJIByPersona(Long clienteId) {

        return clienteTitulacionesNoUJIDAO.getClienteEstudiosNoUJIByPersona(clienteId);
    }

    public void addClienteTitulacion(Cliente cliente, Long clasificacionId, String anyoEstudio, Long universidadId, String textoEstudio, Long tipoEstudioId) {
        ClienteTitulacionesNoUJI clienteTitulacionesNoUJI = new ClienteTitulacionesNoUJI(cliente, clasificacionId, anyoEstudio, universidadId, textoEstudio,
                tipoEstudioId, null);
        clienteTitulacionesNoUJIDAO.insert(clienteTitulacionesNoUJI);
    }

    public void addClienteTitulacion(Cliente cliente, Long clasificacionId, String anyoEstudio, Long universidadId, String textoEstudio, Long tipoEstudioId, ClienteDato clienteDatoEstudio) {
        ClienteTitulacionesNoUJI clienteTitulacionesNoUJI = new ClienteTitulacionesNoUJI(cliente, clasificacionId, anyoEstudio, universidadId, textoEstudio,
                tipoEstudioId, clienteDatoEstudio);
        clienteTitulacionesNoUJIDAO.insert(clienteTitulacionesNoUJI);
    }

    public void deleteAlumniAcademicosNoUJI(Long estudioId) {
        clienteTitulacionesNoUJIDAO.delete(ClienteTitulacionesNoUJI.class, estudioId);
    }

    public ClienteTitulacionesNoUJI updateClienteEstudiosNoUJI(Long id, Long clienteId, String nombre, Long universidadId, Long clasificacionId,
                                                               String anyoFinalizacion, Long tipoEstudioId) {
        ClienteTitulacionesNoUJI clienteTitulacionesNoUJI = uiToModel(clienteId, nombre, universidadId, clasificacionId, anyoFinalizacion, tipoEstudioId);
        clienteTitulacionesNoUJI.setId(id);
        return clienteTitulacionesNoUJIDAO.update(clienteTitulacionesNoUJI);
    }

    private ClienteTitulacionesNoUJI uiToModel(Long clienteId, String nombre, Long universidadId, Long clasificacionId, String anyoFinalizacion, Long tipoEstudioId) {
        return new ClienteTitulacionesNoUJI(new Cliente(clienteId), clasificacionId, anyoFinalizacion, universidadId, nombre, tipoEstudioId, null);
    }

    public ClienteTitulacionesNoUJI addClienteEstudiosNoUJI(Long clienteId, String nombre, Long universidadId, Long clasificacionId, String anyoFinalizacion, Long tipoEstudioId) {
        ClienteTitulacionesNoUJI clienteTitulacionesNoUJI = uiToModel(clienteId, nombre, universidadId, clasificacionId, anyoFinalizacion, tipoEstudioId);
        return clienteTitulacionesNoUJIDAO.insert(clienteTitulacionesNoUJI);
    }

    public ClienteTitulacionesNoUJI getClienteEstudiosNoUJIByClienteDato(Long clienteDatoNivelEstudioId) {
        return clienteTitulacionesNoUJIDAO.getClienteEstudiosNoUJIByClienteDato(clienteDatoNivelEstudioId);
    }
}