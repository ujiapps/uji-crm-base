package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CursoInscripcionDAO;
import es.uji.apps.crm.model.CursoInscripcion;

@Service
public class CursoInscripcionService {
    private CursoInscripcionDAO cursoInscripcionDAO;

    @Autowired
    public CursoInscripcionService(CursoInscripcionDAO cursoInscripcionDAO) {
        this.cursoInscripcionDAO = cursoInscripcionDAO;
    }

    public List<CursoInscripcion> getCursosInscripcionesByPersonaId(Long connectedUserId, Long personaId) {

        return cursoInscripcionDAO.getCursosInscripcionesByPersonaId(connectedUserId, personaId);
    }
}