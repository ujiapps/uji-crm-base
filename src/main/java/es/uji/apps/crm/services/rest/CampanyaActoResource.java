package es.uji.apps.crm.services.rest;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.databind.JsonNode;
import com.sun.jersey.api.client.ClientResponse;
import es.uji.commons.rest.*;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.FormDataParam;

import es.uji.apps.crm.exceptions.ErrorEnBorradoDeDocumentoException;
import es.uji.apps.crm.exceptions.ErrorSubiendoDocumentoException;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaActo;
import es.uji.apps.crm.services.ADEClienteService;
import es.uji.apps.crm.services.CampanyaActoService;
import es.uji.apps.crm.services.UtilsService;

@Component
@Path("campanyaacto")
public class CampanyaActoResource extends CoreBaseService {
    @InjectParam
    private CampanyaActoService campanyaActoService;

    @InjectParam
    private ADEClienteService adeClienteService;

    @Path("{id}/envio")
    public CampanyaActoEnvioResource getPlatformItem(
            @InjectParam CampanyaActoEnvioResource campanyaActoEnvioResource) {
        return campanyaActoEnvioResource;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyasActosByCampanya(@QueryParam("campanyaId") Long campanyaId) {
        Campanya campanya = new Campanya();
        campanya.setId(campanyaId);

        return modelToUI(campanyaActoService.getCampanyasActoByCampanya(campanya));
    }

    private List<UIEntity> modelToUI(List<CampanyaActo> campanyaActos) {
        List<UIEntity> lista = new ArrayList<>();

        for (CampanyaActo campanyaActo : campanyaActos)
        {
            lista.add(modelToUI(campanyaActo));
        }
        return lista;

    }

    private UIEntity modelToUI(CampanyaActo campanyaActo) {
        UIEntity entity = UIEntity.toUI(campanyaActo);

        entity.put("rssActivo", campanyaActo.getRssActivo());
        return entity;
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("{campanyaId}")
    public UIEntity addCampanyaActo(@PathParam("campanyaId") Long campanyaId, @FormDataParam("fecha") String fecha, @FormDataParam("titulo") String titulo,
                                    @FormDataParam("contenido") String contenido, @FormDataParam("duracion") Long duracion, @FormDataParam("hora") String hora,
                                    @FormDataParam("resumen") String resumen, @FormDataParam("rssActivo") Boolean rssActivo, @FormDataParam("horaApertura") String horaApertura,
                                    FormDataMultiPart multiPart)
            throws ParseException, IOException, ErrorSubiendoDocumentoException {

        CampanyaActo campanyaActo = new CampanyaActo();

        Campanya campanya = new Campanya();
        campanya.setId(campanyaId);
        campanyaActo.setCampanya(campanya);

        Map<String, Object> datosDocumento = extraeDatosDeMultipart(multiPart);
        if (ParamUtils.isNotNull(datosDocumento))
        {
            if (ParamUtils.isNotNull(datosDocumento.get("nombre")))
            {
                campanyaActo.setImagenNombre(datosDocumento.get("nombre").toString());
            }
            else
            {
                campanyaActo.setImagenNombre("No imagen");
            }

            String ref = adeClienteService.addDocumento(datosDocumento.get("nombre").toString(),
                    datosDocumento.get("mimetype").toString(),
                    (byte[]) datosDocumento.get("contenido"));

            campanyaActo.setImagenReferencia(ref);
            campanyaActo.setImagenTipo(datosDocumento.get("mimetype").toString());
        }

        if (fecha != null && !fecha.equals("")) {
            campanyaActo.setFecha(UtilsService.fechaParse(fecha));
        }
        campanyaActo.setTitulo(titulo);
        campanyaActo.setContenido(contenido);
        campanyaActo.setDuracion(duracion);
        campanyaActo.setHora(hora);
        campanyaActo.setHoraApertura(horaApertura);
        campanyaActo.setResumen(resumen);
        campanyaActo.setRssActivo(ParamUtils.isNotNull(rssActivo));
        campanyaActoService.addCampanyaActo(campanyaActo);

        return modelToUI(campanyaActo);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("generarEvento")
    public ResponseMessage generarEvento(UIEntity entity) {
        ResponseMessage responseMessage = new ResponseMessage();
        CampanyaActo acto = entity.toModel(CampanyaActo.class);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            if (ParamUtils.isNotNull(entity.get("fecha"))) {
                acto.setFecha(sdf.parse(entity.get("fecha")));
            } else {
                responseMessage.setSuccess(false);
                responseMessage.setMessage("La data no pot ser nula");
                return responseMessage;
            }
        } catch (Exception e) {
            responseMessage.setSuccess(false);
            responseMessage.setMessage("Error amb la data");
            e.printStackTrace();
        }

        try {
            ClientResponse postEventResponse = campanyaActoService.generarEvento(acto);
            if (postEventResponse.getStatus() == 200) {
                responseMessage.setSuccess(true);
                responseMessage.setMessage("Event actualitzat correctament");
            } else if (postEventResponse.getStatus() == 201) {
                ClientResponse postSessionResponse = campanyaActoService.generarSesion(acto);
                if (postSessionResponse.getStatus() == 201) {
                    campanyaActoService.activarRssEvento(acto);
                    responseMessage.setSuccess(true);
                    responseMessage.setMessage("Event i sessió creats correctament");
                } else if (postEventResponse.getStatus() == 401) {
                    responseMessage.setSuccess(false);
                    responseMessage.setMessage("Error d'autenticació per a la sessió");
                } else {
                    JsonNode response = postEventResponse.getEntity(JsonNode.class);
                    responseMessage.setMessage(response.get("message") + ": " + response.get("detail"));
                }
            } else if (postEventResponse.getStatus() == 401) {
                responseMessage.setSuccess(false);
                responseMessage.setMessage("Error d'autenticació per a l'event");
            } else {
                JsonNode response = postEventResponse.getEntity(JsonNode.class);
                responseMessage.setMessage(response.get("message") + ": " + response.get("detail"));
            }
        } catch (Exception e) {
            responseMessage.setSuccess(false);
            e.printStackTrace();
        }
        return responseMessage;
    }

    private Map<String, Object> extraeDatosDeMultipart(FormDataMultiPart multiPart)
            throws IOException {
        Map<String, Object> datos = new HashMap<>();

        String fileName = "";
        String mimeType = "";
        InputStream documento = null;

        for (BodyPart bodyPart : multiPart.getBodyParts())
        {
            mimeType = bodyPart.getHeaders().getFirst("Content-Type");
            if (ParamUtils.isNotNull(mimeType))
            {
                if (mimeType != null && !mimeType.isEmpty())
                {
                    String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                    Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                    Matcher m = fileNamePattern.matcher(header);
                    if (m.matches())
                    {
                        fileName = m.group(1);
                    }
                    BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                    documento = bpe.getInputStream();
                }
            }
        }

        if (ParamUtils.isNotNull(fileName) && ParamUtils.isNotNull(documento))
        {
            datos.put("nombre", fileName);
            datos.put("mimetype", mimeType);
            datos.put("contenido", StreamUtils.inputStreamToByteArray(documento));

            return datos;
        }
        else
        {
            return null;
        }
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("actualizar/{campanyaActoId}")
    public UIEntity updateCampanyaActo(@PathParam("campanyaActoId") Long campanyaActoId, @FormDataParam("fecha") String fecha, @FormDataParam("titulo") String titulo,
                                       @FormDataParam("contenido") String contenido, @FormDataParam("duracion") Long duracion, @FormDataParam("hora") String hora,
                                       @FormDataParam("resumen") String resumen, @FormDataParam("rssActivo") Boolean rssActivo, @FormDataParam("horaApertura") String horaApertura,
                                       FormDataMultiPart multiPart)
            throws ParseException, IOException, ErrorSubiendoDocumentoException, ErrorEnBorradoDeDocumentoException {

        CampanyaActo campanyaActo = campanyaActoService.getCampanyasActoById(campanyaActoId);
        Map<String, Object> datosDocumento = extraeDatosDeMultipart(multiPart);

        if (ParamUtils.isNotNull(campanyaActo.getImagenReferencia()))
        {
            if (ParamUtils.isNotNull(datosDocumento) && ParamUtils.isNotNull(datosDocumento.get("nombre")))
            {
                if (!datosDocumento.get("nombre").toString().equals(campanyaActo.getImagenNombre()))
                {
                    adeClienteService.deleteDocumento(campanyaActo.getImagenReferencia());
                }
            }
        }


        if (ParamUtils.isNotNull(datosDocumento))
        {
            if (ParamUtils.isNotNull(datosDocumento.get("nombre")))
            {
                campanyaActo.setImagenNombre(datosDocumento.get("nombre").toString());
            }
            else
            {
                campanyaActo.setImagenNombre("No imagen");
            }

            String ref = adeClienteService.addDocumento(datosDocumento.get("nombre").toString(),
                    datosDocumento.get("mimetype").toString(),
                    (byte[]) datosDocumento.get("contenido"));

            campanyaActo.setImagenReferencia(ref);
            campanyaActo.setImagenTipo(datosDocumento.get("mimetype").toString());
        }
        if (fecha != null && !fecha.equals("")) {
            campanyaActo.setFecha(UtilsService.fechaParse(fecha));
        }
        campanyaActo.setTitulo(titulo);
        campanyaActo.setContenido(contenido);
        campanyaActo.setDuracion(duracion);
        campanyaActo.setHora(hora);
        campanyaActo.setHoraApertura(horaApertura);
        campanyaActo.setResumen(resumen);
        if (ParamUtils.isNotNull(rssActivo))
        {
            campanyaActo.setRssActivo(true);
        }
        else
        {
            campanyaActo.setRssActivo(false);
        }
        campanyaActoService.updateCampanyaActo(campanyaActo);

        return modelToUI(campanyaActo);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long campanyaActoId)
            throws MalformedURLException, ErrorEnBorradoDeDocumentoException {

        CampanyaActo campanyaActo = campanyaActoService.getCampanyasActoById(campanyaActoId);
        if (ParamUtils.isNotNull(campanyaActo.getImagenReferencia()))
        {
            adeClienteService.deleteDocumento(campanyaActo.getImagenReferencia());
        }
        campanyaActoService.deleteCampanyaActo(campanyaActoId);
    }

    private CampanyaActo UIToModel(UIEntity entity) throws ParseException {
        CampanyaActo campanyaActo = entity.toModel(CampanyaActo.class);

        Campanya campanya = new Campanya();
        campanya.setId(ParamUtils.parseLong(entity.get("campanyaId")));
        campanyaActo.setCampanya(campanya);

        campanyaActo.setFecha(UtilsService.fechaParse(entity.get("fecha")));

        return campanyaActo;
    }

}