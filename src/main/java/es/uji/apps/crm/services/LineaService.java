package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.LineaDAO;
import es.uji.apps.crm.model.Linea;

@Service
public class LineaService {
    @Autowired
    private LineaDAO lineaDAO;

    public List<Linea> getLineasByLineaId(Long reciboId) {
        return lineaDAO.getLineasByReciboId(reciboId);
    }
}