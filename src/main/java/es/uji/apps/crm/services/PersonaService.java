package es.uji.apps.crm.services;

import java.util.List;

import es.uji.apps.crm.dao.PersonaSimpleDAO;
import es.uji.apps.crm.model.PersonaSimple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.PersonaDAO;
import es.uji.apps.crm.model.Persona;

@Service
public class PersonaService {
    private PersonaDAO personaDAO;
    private PersonaSimpleDAO personaSimpleDAO;

    @Autowired
    public PersonaService(PersonaDAO personaDAO, PersonaSimpleDAO personaSimpleDAO) {
        this.personaDAO = personaDAO;
        this.personaSimpleDAO = personaSimpleDAO;
    }

    public List<Persona> getPersonas() {
        return personaDAO.getPersonas();
    }

    public Persona getPersonaByIdentificacion(String identificacion) {
        return personaDAO.getPersonaByIdentificacion(identificacion);
    }

    public Persona getPersonaByClienteId(Long clienteId)// throws RegistroNoEncontradoException
    {
        return personaDAO.getPersonaByClienteId(clienteId);
    }

    public Persona getPersonaById(Long personaId) {
        return personaDAO.get(Persona.class, personaId).get(0);
    }

    public PersonaSimple getPersonaSimpleByIdentificacion(String palabra) {
        return personaSimpleDAO.getPersonaByIdentificacion(palabra);
    }
}