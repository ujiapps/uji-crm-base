package es.uji.apps.crm.services;

import java.util.List;

import es.uji.apps.crm.dao.ClienteDatoTipoDAO;
import es.uji.apps.crm.ui.ClienteDatoOpcionUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.model.ClienteDatoOpcion;
import es.uji.apps.crm.model.ClienteDatoTipo;
import es.uji.apps.crm.model.domains.TipoDato;

@Service
public class ClienteDatoTipoService {
    private ClienteDatoTipoDAO clienteDatoTipoDAO;

    @Autowired
    public ClienteDatoTipoService(ClienteDatoTipoDAO clienteDatoTipoDAO) {
        this.clienteDatoTipoDAO = clienteDatoTipoDAO;
    }

    public List<ClienteDatoTipo> getClientesDatosTipos() {
        return clienteDatoTipoDAO.getClientesDatosTipos();
    }

    public ClienteDatoTipo getClienteDatoTipoById(Long clienteDatoTipoId) {
        return clienteDatoTipoDAO.getClienteDatoTipoById(clienteDatoTipoId);
    }

    public List<ClienteDatoOpcion> getClienteDatoOpcionesByTipoId(Long tipoId) {
        return clienteDatoTipoDAO.getClienteDatoOpcionesByTipoId(tipoId);
    }

    public List<ClienteDatoOpcionUI> getClienteDatoOpcionesByTipoId(Long tipoId, String idioma) {
        return clienteDatoTipoDAO.getClienteDatoOpcionesByTipoId(tipoId, idioma);
    }

    public List<ClienteDatoTipo> getClientesDatosTiposClientes() {
        return clienteDatoTipoDAO.getClientesDatosTiposClientes();
    }

    public ClienteDatoTipo addClienteDatoTipo(ClienteDatoTipo clienteDatoTipo) {
        clienteDatoTipoDAO.insert(clienteDatoTipo);
        return clienteDatoTipo;
    }

    public void updateClienteDatoTipo(ClienteDatoTipo clienteDatoTipo) throws Exception {
        checkCambioTipoDeDatoValido(clienteDatoTipo);
        clienteDatoTipoDAO.update(clienteDatoTipo);
    }

    private void checkCambioTipoDeDatoValido(ClienteDatoTipo clienteDatoTipo) throws Exception {
        ClienteDatoTipo actualClienteDatoTipo = clienteDatoTipoDAO
                .getClienteDatoTipoById(clienteDatoTipo.getId());
        if (actualClienteDatoTipo.getClienteDatoTipoTipo().getId() == TipoDato.SELECCION.getId()
                && actualClienteDatoTipo.getClientesDatosOpciones() != null
                && !actualClienteDatoTipo.getClientesDatosOpciones().isEmpty()
                && clienteDatoTipo.getClienteDatoTipoTipo().getId() != TipoDato.SELECCION.getId())
        {
            throw new Exception(
                    "No es pot canviar una dada de tipus Selecció si no s'eliminen primer les opcions");
        }
    }

    public void deleteClienteDatoTipo(Long clienteDatoTipoId) {
        clienteDatoTipoDAO.delete(ClienteDatoTipo.class, clienteDatoTipoId);
    }

    public ClienteDatoTipo getTipoDatoEntradasByFormularioId(Long formularioId) {
        return clienteDatoTipoDAO.getTipoDatoEntradasByFormularioId(formularioId);
    }

    public List<ClienteDatoOpcion> getClienteDatoOpcionesByTipoAndTamanyo(ClienteDatoTipo clienteDatoTipo, Long tamanyo) {
        return clienteDatoTipoDAO.getClienteDatoOpcionesByTipoAndTamanyo(clienteDatoTipo, tamanyo);
    }
}