package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.crm.exceptions.ErrorEnBorradoDeDocumentoException;
import es.uji.apps.crm.exceptions.ErrorSubiendoDocumentoException;
import es.uji.apps.crm.exceptions.FicheroException;
import es.uji.apps.crm.model.CampanyaCarta;
import es.uji.apps.crm.model.CartaImagen;
import es.uji.apps.crm.services.ADEClienteService;
import es.uji.apps.crm.services.CartaImagenService;
import es.uji.apps.crm.ui.RecursoUI;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StreamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Path("cartaimagen")
public class CartaImagenResource extends CoreBaseService {

    @InjectParam
    private CartaImagenService cartaImagenService;

    @InjectParam
    private ADEClienteService adeClienteService;

    @GET
    @Path("{id}")
    public List<UIEntity> getCartaImagenesByCartaId(@PathParam("id") Long campanyaCartaId) {
        return UIEntity.toUI(cartaImagenService.getCartaImagenesByCampanyaCartaId(campanyaCartaId));
    }

    @GET
    @Path("visualizar/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getWebCartaImagenesByCartaId(@PathParam("id") Long campanyaCartaId, @QueryParam("CKEditorFuncNum") Long editor,
                                                 @QueryParam("query") String search)
            throws ParseException {

        UIEntity uiEntity = new UIEntity();
        if (!search.equals("") && search.contains("*")) {
            search = search.replace("*", "");
        }
        String urlAde = adeClienteService.getUrl();
        List<CartaImagen> cartaImagenes = cartaImagenService.getCartaImagenesByCampanyaCartaId(campanyaCartaId);
        List<RecursoUI> result = cartaImagenService.imageToRecursoUI(cartaImagenes, urlAde);
        uiEntity.put("numDocumentos", cartaImagenes.size());
        uiEntity.put("resultados", UIEntity.toUI(result));

        return uiEntity;

    }

    @POST
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity addCartaImagen(@PathParam("id") Long cartaId, FormDataMultiPart multiPart)
            throws FicheroException, IOException, ErrorSubiendoDocumentoException {

        CartaImagen cartaImagen = new CartaImagen();

        CampanyaCarta campanyaCarta = new CampanyaCarta();
        campanyaCarta.setId(cartaId);
        cartaImagen.setCampanyaCarta(campanyaCarta);

        Map<String, Object> datosDocumento = extraeDatosDeMultipart(multiPart);
        cartaImagen.setNombre(datosDocumento.get("nombre").toString());
        String ref = adeClienteService.addDocumento(datosDocumento.get("nombre").toString(),
                datosDocumento.get("mimetype").toString(),
                (byte[]) datosDocumento.get("contenido"));

        cartaImagen.setReferencia(ref);
        cartaImagenService.addCartaImagen(cartaImagen);
        return UIEntity.toUI(cartaImagen);
    }

    private Map<String, Object> extraeDatosDeMultipart(FormDataMultiPart multiPart)
            throws FicheroException, IOException {
        Map<String, Object> datos = new HashMap<>();

        String fileName = "";
        String mimeType = "";
        InputStream documento = null;

        for (BodyPart bodyPart : multiPart.getBodyParts())
        {
            mimeType = bodyPart.getHeaders().getFirst("Content-Type");
            if (mimeType.contains("image"))
            {
                if (ParamUtils.isNotNull(mimeType))
                {
                    String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                    Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                    Matcher m = fileNamePattern.matcher(header);
                    if (m.matches())
                    {
                        fileName = m.group(1);
                    }
                    BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                    documento = bpe.getInputStream();
                }
            }
            else
            {
                throw new FicheroException("El archivo no es una imagen");
            }
        }

        if (ParamUtils.isNotNull(documento))
        {
            datos.put("nombre", fileName);
            datos.put("mimetype", mimeType);
            datos.put("contenido", StreamUtils.inputStreamToByteArray(documento));
        }

        return datos;
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long cartaImagenId)
            throws MalformedURLException, ErrorEnBorradoDeDocumentoException {

        CartaImagen cartaImagen = cartaImagenService.getCartaImagenesById(cartaImagenId);

        adeClienteService.deleteDocumento(cartaImagen.getReferencia());

        cartaImagenService.deleteCartaImagen(cartaImagenId);

    }

}