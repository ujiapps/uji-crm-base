package es.uji.apps.crm.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ClienteCuotaDAO;
import es.uji.apps.crm.dao.RecibosManager;
import es.uji.apps.crm.model.ClienteCuota;
import es.uji.apps.crm.model.ClienteTarifa;
import es.uji.apps.crm.model.LineaFacturacion;
import es.uji.apps.crm.model.Persona;
import es.uji.apps.crm.model.Tarifa;
import es.uji.apps.crm.model.TipoTarifa;
import es.uji.apps.crm.model.domains.TipoCorreoEnvio;
import es.uji.apps.crm.model.domains.TipoEstadoCuota;
import es.uji.commons.rest.ParamUtils;

@Service
public class ClienteCuotaService {

    private ClienteCuotaDAO clienteCuotaDAO;
    private TarifaService tarifaService;
    //    private ReciboService reciboService;
//    private ServicioService servicioService;
    private TipoReciboService tipoReciboService;
    private LineaFacturacionService lineaFacturacionService;
    private PersonaService personaService;
    private TipoTarifaService tipoTarifaService;
    private EnvioClienteService envioClienteService;


    @Autowired
    public ClienteCuotaService(ClienteCuotaDAO clienteCuotaDAO, TarifaService tarifaService, TipoReciboService tipoReciboService, LineaFacturacionService lineaFacturacionService,
                               PersonaService personaService, TipoTarifaService tipoTarifaService, EnvioClienteService envioClienteService) {
        this.clienteCuotaDAO = clienteCuotaDAO;
        this.tarifaService = tarifaService;
//        this.reciboService = reciboService;
//        this.servicioService = servicioService;
        this.tipoReciboService = tipoReciboService;
        this.lineaFacturacionService = lineaFacturacionService;
        this.personaService = personaService;
        this.tipoTarifaService = tipoTarifaService;
        this.envioClienteService = envioClienteService;
    }

    public List<ClienteCuota> getClienteCuotasByClienteTarifaId(Long clienteTarifa) {
        return clienteCuotaDAO.getClienteCuotasByClienteTarifaId(clienteTarifa);
    }

    public ClienteCuota getClienteCuotaById(Long clienteCuotaId) {

        return clienteCuotaDAO.getClienteCuotaById(clienteCuotaId);
    }

    public ClienteCuota updateClienteCuota(ClienteCuota clienteCuota) {

        clienteCuotaDAO.update(clienteCuota);
        return clienteCuota;
    }

    public void addClienteCuota(ClienteTarifa clienteTarifa) {

        ClienteCuota clienteCuota = new ClienteCuota();
        clienteCuota.setFechaInicio(new Date());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, clienteTarifa.getTipoTarifa().getMesesVigencia().intValue());
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        clienteCuota.setFechaFin(calendar.getTime());
        clienteCuota.setCliente(clienteTarifa.getCliente());

        Tarifa tarifa = tarifaService.getTarifaVigente(clienteTarifa.getTipoTarifa());
        clienteCuota.setImporte(tarifa.getImporte().intValue());

        clienteCuota.setClienteTarifa(clienteTarifa);
        clienteCuota.setEstado(TipoEstadoCuota.VALIDO.getId());
        clienteCuota = clienteCuotaDAO.insert(clienteCuota);

        RecibosManager reciboManager = new RecibosManager();
        reciboManager.init();

        //Recuperar tipo de recibo.
        TipoTarifa tipoTarifa = tipoTarifaService.getTipoTarifaById(clienteTarifa.getTipoTarifa().getId());
        LineaFacturacion lineaFacturacion = lineaFacturacionService.getLineaFacturacionById(tipoTarifa.getLineaFacturacion().getId());
        Long tipoReciboId;
        if (ParamUtils.isNotNull(lineaFacturacion.getTipoRecibo()))
        {
            tipoReciboId = tipoReciboService.getTipoReciboByEmisoraAndTipo(lineaFacturacion.getEmisora(), lineaFacturacion.getTipoRecibo());
        }
        else
        {
            tipoReciboId = tipoReciboService.getTipoReciboByEmisora(lineaFacturacion.getEmisora());
        }

        Persona persona = personaService.getPersonaByClienteId(clienteTarifa.getCliente().getId());
        String correoOficial = envioClienteService.dameCorreoCliente(clienteTarifa.getCliente(), TipoCorreoEnvio.OFICIAL.getId());

        String reciboXML = "<Registro>";
        reciboXML = reciboXML + "<TipoRecibo>" + tipoReciboId + "</TipoRecibo>";
        reciboXML = reciboXML + "<TipoCobro>5</TipoCobro>";
        reciboXML = reciboXML + "<PersonaId>" + persona.getId() + "</PersonaId>";
        reciboXML = reciboXML + "<Descripcion></Descripcion>";
        reciboXML = reciboXML + "<Referencia1>" + clienteCuota.getId() + "</Referencia1>";
        reciboXML = reciboXML + "<Referencia2></Referencia2>";
        reciboXML = reciboXML + "<CuentaBancaria></CuentaBancaria>";
        reciboXML = reciboXML + "<Correo>" + correoOficial + "</Correo>";
        reciboXML = reciboXML + "<CorreoAviso>" + clienteTarifa.getTipoTarifa().getCampanya().getCorreoAvisaCobro() + "</CorreoAviso>";
        reciboXML = reciboXML + "<Plazos>";
        reciboXML = reciboXML + "  <Plazo id=\"1\">";
        reciboXML = reciboXML + "    <FechaPagoLimite></FechaPagoLimite>";
        reciboXML = reciboXML + "    <Conceptos>";
        reciboXML = reciboXML + "      <Concepto>";
        reciboXML = reciboXML + "        <Referencia1>" + clienteCuota.getId() + "</Referencia1>";
        reciboXML = reciboXML + "        <Referencia2></Referencia2>";
        reciboXML = reciboXML + "        <Descripcion>Pago acto de graduación</Descripcion>";
        reciboXML = reciboXML + "        <Importe>" + tarifa.getImporte() * 100 + "</Importe>";
        reciboXML = reciboXML + "      </Concepto>";
        reciboXML = reciboXML + "    </Conceptos>";
        reciboXML = reciboXML + "  </Plazo>";
        reciboXML = reciboXML + "</Plazos>";
        reciboXML = reciboXML + "</Registro>";


        Long reciboId = reciboManager.creaRecibo(reciboXML);

        clienteCuota.setReciboId(reciboId);
        clienteCuotaDAO.update(clienteCuota);

    }

    public void deleteClienteCuotasByClienteTarifa(ClienteTarifa clienteTarifa) {
        clienteCuotaDAO.deleteClienteCuotasByClienteTarifa(clienteTarifa);
    }
}