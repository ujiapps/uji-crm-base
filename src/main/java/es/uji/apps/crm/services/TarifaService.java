package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.TarifaDAO;
import es.uji.apps.crm.model.Tarifa;
import es.uji.apps.crm.model.TipoTarifa;

@Service
public class TarifaService {
    @Autowired
    private TarifaDAO tarifaDAO;

    public List<Tarifa> getTarifasByTipoTarifa(TipoTarifa tipoTarifa) {
        return tarifaDAO.getTarifasByTipoTarifa(tipoTarifa);
    }

    public Tarifa addTarifa(Tarifa tarifa) {
        tarifaDAO.insert(tarifa);
        tarifa.setId(tarifa.getId());
        return tarifa;
    }

    public void deleteTarifa(Long tarifaId) {
        tarifaDAO.delete(Tarifa.class, tarifaId);
    }

    public Tarifa updateTarifa(Tarifa tarifa) {
        tarifaDAO.update(tarifa);
        return tarifa;
    }

    public Tarifa getTarifaVigente(TipoTarifa tipoTarifa) {
        return tarifaDAO.getTarifaVigente(tipoTarifa);
    }

}