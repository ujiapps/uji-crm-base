package es.uji.apps.crm.services.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.crm.model.FicheroDevolucion;
import es.uji.apps.crm.services.FicheroDevolucionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Component
@Path("ficherodevolucion")
public class FicheroDevolucionResource extends CoreBaseService {

    @InjectParam
    private FicheroDevolucionService ficheroDevolucionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getFicherosDevolucion() {

        return modelToUI(ficheroDevolucionService.getFicherosDevolucion());
    }

    private List<UIEntity> modelToUI(List<FicheroDevolucion> ficherosDevolucion) {
        List<UIEntity> lista = new ArrayList<>();

        for (FicheroDevolucion ficheroDevolucion : ficherosDevolucion)
        {
            lista.add(modelToUI(ficheroDevolucion));
        }
        return lista;

    }

    private UIEntity modelToUI(FicheroDevolucion ficheroDevolucion) {
        UIEntity entity = new UIEntity();// UIEntity.toUI(ficheroDevolucion);
        entity.put("fecha", ficheroDevolucion.getFecha());
        entity.put("nombreFichero", ficheroDevolucion.getNombreFichero());
        entity.put("id", ficheroDevolucion.getId());
        return entity;
    }

    @POST
    public UIEntity subirFicheroDevolucion(FormDataMultiPart form) throws IOException {


        FicheroDevolucion ficheroDevolucion = UIToModel(form);
        ficheroDevolucionService.addFicheroDevolucion(ficheroDevolucion);

        return modelToUI(ficheroDevolucion);
    }

    private FicheroDevolucion UIToModel(FormDataMultiPart form) throws IOException {

        Map<String, Object> ficheroDevolucion = extraeDatosDeMultipart(form);

        FicheroDevolucion fichero = new FicheroDevolucion();
        fichero.setFichero(ficheroDevolucion.get("contenido").toString());
        fichero.setNombreFichero(ficheroDevolucion.get("nombre").toString());

        return fichero;

    }

    private Map<String, Object> extraeDatosDeMultipart(FormDataMultiPart multiPart)
            throws IOException {
        Map<String, Object> datos = new HashMap<>();

        String fileName = "";
        String mimeType = "";
        InputStream documento = null;

        for (BodyPart bodyPart : multiPart.getBodyParts())
        {
            mimeType = bodyPart.getHeaders().getFirst("Content-Type");
            if (ParamUtils.isNotNull(mimeType))
            {
                if (mimeType != null && !mimeType.isEmpty())
                {
                    String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                    Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                    Matcher m = fileNamePattern.matcher(header);
                    if (m.matches())
                    {
                        fileName = m.group(1);
                    }
                    BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                    documento = bpe.getInputStream();
                }
            }
        }

        if (ParamUtils.isNotNull(documento))
        {
            String myString = IOUtils.toString(documento, "UTF-8");
            datos.put("nombre", fileName);
            datos.put("mimetype", mimeType);
            datos.put("contenido", myString);
        }

        return datos;
    }

    private FicheroDevolucion UIToModel(UIEntity entity) {

        return entity.toModel(FicheroDevolucion.class);
    }

}