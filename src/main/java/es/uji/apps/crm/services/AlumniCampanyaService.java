package es.uji.apps.crm.services;

import es.uji.apps.crm.model.CampanyaCliente;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.utils.HashUtils;
import es.uji.commons.web.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import es.uji.commons.rest.ParamUtils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class AlumniCampanyaService {

    private final static Long CAMPANYABASIC = 3L;
    private final static Long CAMPANYASOIPEP = 4104036L;

    private final CampanyaClienteService campanyaClienteService;
    private final MovimientoService movimientoService;
    private final AlumniService alumniService;
    private final SesionService sesionService;
    private final AlumniFormularioService alumniFormularioService;

    @Autowired
    public AlumniCampanyaService(CampanyaClienteService campanyaClienteService,
                                 MovimientoService movimientoService,
                                 AlumniService alumniService,
                                 SesionService sesionService,
                                 AlumniFormularioService alumniFormularioService) {
        this.campanyaClienteService = campanyaClienteService;
        this.movimientoService = movimientoService;
        this.alumniService = alumniService;
        this.sesionService = sesionService;
        this.alumniFormularioService = alumniFormularioService;
    }

    public Template getTemplateCampanya(String path, Long clienteId, String idioma) throws ParseException {

        Template template = alumniService.getTemplateBaseAlumni(path, idioma, "campanya");

        introduceBasicEstadoEnPlantillaZonaPrivada(clienteId, template);
        introduceOipepEstadoEnPlantillaZonaPrivada(clienteId, template);
        alumniService.introducePremiumEstadoEnPlantillaZonaPrivada(template);
        return template;
    }

    private void introduceBasicEstadoEnPlantillaZonaPrivada(Long clienteId, Template template) {
        Boolean basic = Boolean.FALSE;
        Boolean optaBasic = Boolean.FALSE;
        CampanyaCliente campanyaClienteBasic = campanyaClienteService.getCampanyaClienteByCampanyaAndClienteId(CAMPANYABASIC, clienteId);
        if (ParamUtils.isNotNull(campanyaClienteBasic)) {
            optaBasic = Boolean.TRUE;
            if (ParamUtils.isNotNull(campanyaClienteBasic.getCampanyaClienteTipo().getAccion()) && campanyaClienteBasic.getCampanyaClienteTipo().getAccion().equals("ESTADO-ALTA")) {
                basic = Boolean.TRUE;
                template.put("fechaBasicAlta", movimientoService.getFechaAlta(clienteId, CAMPANYABASIC));
            }
        }

        template.put("basic", basic);
        template.put("optaBasic", optaBasic);
    }

    private void introduceOipepEstadoEnPlantillaZonaPrivada(Long clienteId, Template template) {
        Boolean oipep = Boolean.FALSE;

        List<CampanyaCliente> campanyaClienteOIPEP = campanyaClienteService.getCampanyaClienteByClienteIdAndCampanyaPadreId(clienteId, CAMPANYASOIPEP);
        for (CampanyaCliente campanyaCliente : campanyaClienteOIPEP) {
            if (ParamUtils.isNotNull(campanyaCliente.getCampanyaClienteTipo().getAccion()) && (campanyaCliente.getCampanyaClienteTipo().getAccion().equals("ESTADO-PROPUESTO") || campanyaCliente.getCampanyaClienteTipo().getAccion().equalsIgnoreCase("ESTADO-ALTA-WEB"))) {
                oipep = Boolean.TRUE;
                template.put("campanyaOipep", campanyaCliente.getCampanya().getId());

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                Date fechaInicioActiva = calendar.getTime();
                if (ParamUtils.isNotNull(campanyaCliente.getCampanya().getFechaInicioActiva())) {
                    fechaInicioActiva = campanyaCliente.getCampanya().getFechaInicioActiva();
                }

                calendar.add(Calendar.DAY_OF_YEAR, 2);
                Date fechaFinActiva = calendar.getTime();
                if (ParamUtils.isNotNull(campanyaCliente.getCampanya().getFechaFinActiva())) {
                    fechaFinActiva = campanyaCliente.getCampanya().getFechaFinActiva();
                }

                Date hoy = new Date();
                if (hoy.after(fechaInicioActiva) && hoy.before(fechaFinActiva)) {
                    template.put("campanyaOipepActiva", true);
                }
                if (campanyaCliente.getCampanyaClienteTipo().getAccion().equalsIgnoreCase("ESTADO-ALTA-WEB")) {
                    template.put("campanyaOipepSolicitada", true);
                }
            }
        }
        template.put("oipep", oipep);
    }

    public Template getAlumniTemplateErrorCampanya(String path, String idioma) throws ParseException {
        String pathPlantilla = "crm/rest/" + path.split("/")[1] + "/" + path.split("/")[2] + "/";
        return alumniService.getTemplateAviso(idioma, "form.formulario.envioerror", "/" + pathPlantilla + "campanyas/zonaprivada/", path);
    }

    public String getHashFormularioZonaPrivada(Cliente cliente) {
        String hash = HashUtils.stringToHashLogin();
        sesionService.renewSesion(cliente, hash);

        return hash;
    }


}
