package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.EnvioCriterioEstudioUJIDAO;
import es.uji.apps.crm.model.ClasificacionEstudio;
import es.uji.apps.crm.model.EnvioCriterio;
import es.uji.apps.crm.model.EnvioCriterioEstudioUJI;

@Service
public class EnvioCriterioEstudioUJIService {
    private EnvioCriterioEstudioUJIDAO envioCriterioEstudioUJIDAO;

    @Autowired
    public EnvioCriterioEstudioUJIService(EnvioCriterioEstudioUJIDAO envioCriterioEstudioUJIDAO) {
        this.envioCriterioEstudioUJIDAO = envioCriterioEstudioUJIDAO;
    }

    public void insertEnvioCriteriosEstudioUJI(List<Long> arrayCriteriosEstudioUJI, Long envioCriterioId, Long anyoFinalizacionEstudioUJIInicio, Long anyoFinalizacionEstudioUJIFin) {

        for (Long estudioUJIId : arrayCriteriosEstudioUJI)
        {
            insertEnvioCriterioEstudioUJI(estudioUJIId, envioCriterioId, anyoFinalizacionEstudioUJIInicio, anyoFinalizacionEstudioUJIFin);
        }

    }

    public void insertEnvioCriterioEstudioUJI(Long estudioUJIId, Long envioCriterioId, Long anyoFinalizacionEstudioUJIInicio, Long anyoFinalizacionEstudioUJIFin) {

        EnvioCriterioEstudioUJI envioCriterioEstudioUJI = new EnvioCriterioEstudioUJI();

        ClasificacionEstudio estudio = new ClasificacionEstudio();
        estudio.setId(estudioUJIId);
        envioCriterioEstudioUJI.setEstudioUJI(estudio);

        EnvioCriterio envioCriterio = new EnvioCriterio();
        envioCriterio.setId(envioCriterioId);
        envioCriterioEstudioUJI.setEnvioCriterio(envioCriterio);

        envioCriterioEstudioUJI.setAnyoFinalizacionEstudioUJIInicio(anyoFinalizacionEstudioUJIInicio);
        envioCriterioEstudioUJI.setAnyoFinalizacionEstudioUJIFin(anyoFinalizacionEstudioUJIFin);

        envioCriterioEstudioUJIDAO.insert(envioCriterioEstudioUJI);

    }

    public void deleteEnvioCriteriosEstudioUJI(Long envioCriterioId) {
        envioCriterioEstudioUJIDAO.delete(EnvioCriterioEstudioUJI.class, "envio_criterio_id = " + envioCriterioId);

    }

    public List<EnvioCriterioEstudioUJI> getEnvioCriterioEstudioUJIByEnvioCriterioId(Long envioCriterioId) {
        return envioCriterioEstudioUJIDAO.get(EnvioCriterioEstudioUJI.class, "envio_criterio_id = " + envioCriterioId);
    }
}