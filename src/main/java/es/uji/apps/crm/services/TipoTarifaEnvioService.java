package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.TipoTarifaEnvioDAO;
import es.uji.apps.crm.dao.TipoTarifaEnvioProgramadoDAO;
import es.uji.apps.crm.model.TipoTarifa;
import es.uji.apps.crm.model.TipoTarifaEnvio;


@Service
public class TipoTarifaEnvioService {
    @Autowired
    private TipoTarifaEnvioDAO tipoTarifaEnvioDAO;

    @Autowired
    private TipoTarifaEnvioProgramadoDAO tipoTarifaEnvioProgramadoDAO;

    public List<TipoTarifaEnvio> getTipoTarifaEnvioByTipoTarifa(TipoTarifa tipoTarifa) {
        return tipoTarifaEnvioDAO.getTipoTarifaEnvioByTipoTarifa(tipoTarifa);
    }

    public TipoTarifaEnvio addTipoTarifaEnvio(TipoTarifaEnvio tipoTarifaEnvio) {
        return tipoTarifaEnvioDAO.insert(tipoTarifaEnvio);
    }

    public TipoTarifaEnvio updateTarifa(TipoTarifaEnvio tipoTarifaEnvio) {
        return tipoTarifaEnvioDAO.update(tipoTarifaEnvio);
    }

    public void deleteTipoTarifaEnvio(Long tarifaEnvioId) {

        tipoTarifaEnvioProgramadoDAO.deleteEnvioProgramadoByTipoTarifaEnvio(tarifaEnvioId);
        tipoTarifaEnvioDAO.delete(TipoTarifaEnvio.class, tarifaEnvioId);
    }

    public TipoTarifaEnvio updateTarifaEnvioCuerpo(Long tipoTarifaEnvioId, String cuerpo) {

        TipoTarifaEnvio tipoTarifaEnvio = getTipoTarifaEnvioById(tipoTarifaEnvioId);
        tipoTarifaEnvio.setCuerpo(cuerpo);

        return tipoTarifaEnvioDAO.update(tipoTarifaEnvio);
    }

    private TipoTarifaEnvio getTipoTarifaEnvioById(Long tipoTarifaEnvioId) {
        return tipoTarifaEnvioDAO.getTipoTarifaEnvioById(tipoTarifaEnvioId);
    }

    public void autoguardadoTarifaEnvio(Long id, String cuerpo) {
        TipoTarifaEnvio tipoTarifaEnvio = getTipoTarifaEnvioById(id);
        tipoTarifaEnvio.setCuerpo(cuerpo);
        tipoTarifaEnvioDAO.update(tipoTarifaEnvio);
    }
}