package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.services.ItemService;
import es.uji.commons.rest.UIEntity;

public class CampanyaItemSubResource {
    @PathParam("campanyaId")
    Long campanyaId;
    @InjectParam
    private ItemService itemService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItems() {
        List<Item> lista = itemService.getItemsAsociadosByCampanyaId(campanyaId);
        return modelToUI(lista);
    }

    private List<UIEntity> modelToUI(List<Item> items) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (Item item : items)
        {
            listaUI.add(modelToUI(item));
        }
        return listaUI;
    }

    private UIEntity modelToUI(Item item) {
        UIEntity entity = UIEntity.toUI(item);

        entity.put("grupoNombre", item.getGrupo().getNombreCa());

        return entity;
    }

}