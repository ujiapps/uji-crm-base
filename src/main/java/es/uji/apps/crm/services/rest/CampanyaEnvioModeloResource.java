package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.model.CampanyaEnvioModelo;
import es.uji.apps.crm.services.CampanyaEnvioModeloService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("campanyaenviomodelo")
public class CampanyaEnvioModeloResource extends CoreBaseService {
    @InjectParam
    private CampanyaEnvioModeloService campanyaEnvioModeloService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getCampanyaEnviosModeloByTipo(
            @QueryParam("tipo") String tipo) {
        CampanyaEnvioModelo campanyaEnvioModelo = campanyaEnvioModeloService
                .getCampanyaEnvioModelo(tipo);

        return UIEntity.toUI(campanyaEnvioModelo);
    }

}