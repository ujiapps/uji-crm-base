package es.uji.apps.crm.services.rest;

import java.text.ParseException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteDato;
import es.uji.apps.crm.model.ClienteDatoTipo;
import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.model.domains.TipoAccesoDatoUsuario;
import es.uji.apps.crm.services.ClienteDatoService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("itemdato")
public class ItemDatoResource extends CoreBaseService {
    @InjectParam
    private ClienteDatoService clienteDatoService;

    @InjectParam
    private UtilsService utilsService;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity anyadirItemDato(UIEntity entity) throws ParseException {

        ClienteDato clienteDato = UIToModel(entity);
        clienteDatoService.addClienteDatoExtra(clienteDato, entity.get("valor"), null);
        clienteDato.setId(clienteDato.getId());
        return modelToUI(clienteDato);
    }

    private UIEntity modelToUI(ClienteDato clienteDato) throws ParseException {

        UIEntity entity = UIEntity.toUI(clienteDato);

        entity.put("tipoDatoId", clienteDato.getClienteDatoTipo().getId());
        entity.put("tipoDatoNombre", clienteDato.getClienteDatoTipo().getNombre());
        if (clienteDato.getClienteDatoTipo() != null)
        {
            entity.put("tipoDatoTipoNombre", clienteDato.getClienteDatoTipo()
                    .getClienteDatoTipoTipo().getNombre());
            entity.put("tipoDatoTipoId", clienteDato.getClienteDatoTipo().getClienteDatoTipoTipo()
                    .getId());
        }
        entity.put("tipoAccesoId", clienteDato.getClienteDatoTipoAcceso().getId());
        entity.put("tipoAccesoNombre", clienteDato.getClienteDatoTipoAcceso().getNombre());
        entity.put("clienteId", clienteDato.getCliente().getId());

        String tipo = clienteDato.getClienteDatoTipo().getClienteDatoTipoTipo().getNombre();

        if (ParamUtils.isNotNull(tipo))
        {
            entity.put("valor", clienteDato.getValorSegunTipo());
        }

        return entity;
    }

    private ClienteDato UIToModel(UIEntity entity) {
        ClienteDato clienteDato = entity.toModel(ClienteDato.class);

        String itemId = entity.get("itemId");
        if (itemId != null)
        {
            Item item = new Item();
            item.setId(Long.parseLong(itemId));
            clienteDato.setItem(item);
        }

        Cliente cliente = new Cliente();
        cliente.setId(Long.parseLong(entity.get("clienteId")));
        clienteDato.setCliente(cliente);

        Tipo tipoAcceso = new Tipo();
        String tipoAccesoId = entity.get("tipoAccesoId");
        if (tipoAccesoId != null)
        {
            tipoAcceso.setId(Long.parseLong(tipoAccesoId));
        }
        else
        {
            tipoAcceso.setId(TipoAccesoDatoUsuario.INTERNO.getId());
        }
        clienteDato.setClienteDatoTipoAcceso(tipoAcceso);

        Tipo tipoDato = new Tipo();
        tipoDato.setId(Long.parseLong(entity.get("tipoDatoTipoId")));
        tipoDato.setNombre((entity.get("tipoDatoTipoNombre")));

        ClienteDatoTipo clienteDatoTipo = new ClienteDatoTipo();
        clienteDatoTipo.setId(Long.parseLong(entity.get("tipoDatoId")));
        clienteDatoTipo.setNombre(entity.get("tipoDatoNombre"));
        clienteDatoTipo.setClienteDatoTipoTipo(tipoDato);
        clienteDato.setClienteDatoTipo(clienteDatoTipo);

//        clienteDatoService.setValorSegunTipo(clienteDato, entity.get("valor"), null);

        return clienteDato;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateItemDato(@PathParam("id") Long clienteDatoId, UIEntity entity)
            throws ParseException {
        ClienteDato clienteDato = UIToModel(entity);
        clienteDato.setId(clienteDatoId);
        clienteDatoService.update(clienteDato);
        clienteDato = clienteDatoService.getClienteDatoById(clienteDatoId);
        return modelToUI(clienteDato);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long itemDatoId) {
        clienteDatoService.delete(itemDatoId);
    }
}