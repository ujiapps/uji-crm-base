package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaSeguimientoDAO;
import es.uji.apps.crm.model.CampanyaSeguimiento;

@Service
public class CampanyaSeguimientoService {
    private CampanyaSeguimientoDAO campanyaSeguimientoDAO;

    @Autowired
    public CampanyaSeguimientoService(CampanyaSeguimientoDAO campanyaSeguimientoDAO) {
        this.campanyaSeguimientoDAO = campanyaSeguimientoDAO;
    }

    public List<CampanyaSeguimiento> getCampanyaSeguimientosByCampanyaFaseId(Long campanyaFaseId) {
        return campanyaSeguimientoDAO.getCampanyaSeguimientosByCampanyaFaseId(campanyaFaseId);
    }

    public CampanyaSeguimiento getCampanyaSeguimientoById(Long campanyaSeguimientoId) {
        return campanyaSeguimientoDAO.getCampanyaSeguimientoById(campanyaSeguimientoId);
    }

    public CampanyaSeguimiento addCampanyaSeguimiento(CampanyaSeguimiento campanyaSeguimiento) {
        campanyaSeguimientoDAO.insert(campanyaSeguimiento);
        campanyaSeguimiento.setId(campanyaSeguimiento.getId());
        return campanyaSeguimiento;
    }

    public void updateCampanyaSeguimiento(CampanyaSeguimiento campanyaSeguimiento) {
        campanyaSeguimientoDAO.update(campanyaSeguimiento);
    }

    public void deleteCampanyaSeguimiento(Long campanyaSeguimientoId) {
        campanyaSeguimientoDAO.delete(CampanyaSeguimiento.class, campanyaSeguimientoId);
    }

}