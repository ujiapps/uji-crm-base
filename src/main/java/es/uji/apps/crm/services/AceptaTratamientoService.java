package es.uji.apps.crm.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.AceptaTratamientoDAO;
import es.uji.apps.crm.model.AceptaTratamiento;
import es.uji.apps.crm.model.Cliente;

@Service
public class AceptaTratamientoService {

    private AceptaTratamientoDAO aceptaTratamientoDAO;

    @Autowired
    public AceptaTratamientoService(AceptaTratamientoDAO aceptaTratamientoDAO) {

        this.aceptaTratamientoDAO = aceptaTratamientoDAO;
    }

    public AceptaTratamiento getAceptaTratamientoByClienteAndCodigo(Cliente cliente, String codigotratamiento) {
        return aceptaTratamientoDAO.getAceptaTratamientoByClienteAndCodigo(cliente, codigotratamiento);
    }

    public void addAceptaTratamiento(Cliente cliente, String codigoTratamiento) {
        AceptaTratamiento aceptaTratamiento = new AceptaTratamiento();
        aceptaTratamiento.setClienteGeneral(cliente.getClienteGeneral());
        aceptaTratamiento.setCodigoTratamiento(codigoTratamiento);
        aceptaTratamiento.setFecha(new Date());
        aceptaTratamientoDAO.insert(aceptaTratamiento);
    }
}