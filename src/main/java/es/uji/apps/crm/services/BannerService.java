package es.uji.apps.crm.services;

import es.uji.apps.crm.dao.BannerDAO;
import es.uji.apps.crm.model.Banner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BannerService {
    private final BannerDAO bannerDAO;

    @Autowired
    public BannerService(BannerDAO bannerDAO) {
        this.bannerDAO = bannerDAO;
    }

    public List<Banner> getAllBanners() {
        return bannerDAO.getBanners();
    }

    public void addBanner(Banner banner) {
        bannerDAO.insert(banner);
    }

    public void updateBanner(Banner banner) {
        bannerDAO.updateBanner(banner);
    }

    public void deleteBanner(Long bannerId) {
        bannerDAO.deleteBanner(bannerId);
    }
}
