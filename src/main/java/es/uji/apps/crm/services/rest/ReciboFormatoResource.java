package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.ReciboFormato;
import es.uji.apps.crm.services.ReciboFormatoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("reciboformato")
public class ReciboFormatoResource extends CoreBaseService {

    @InjectParam
    private ReciboFormatoService reciboFormatoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getReciboFormatos() {

        return modelToUI(reciboFormatoService.getReciboFormatos());
    }

    private List<UIEntity> modelToUI(List<ReciboFormato> reciboFormatos) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (ReciboFormato reciboFormato : reciboFormatos)
        {
            listaUI.add(modelToUI(reciboFormato));
        }
        return listaUI;
    }

    private UIEntity modelToUI(ReciboFormato reciboFormato) {
        UIEntity entity = UIEntity.toUI(reciboFormato);
        return entity;
    }
}