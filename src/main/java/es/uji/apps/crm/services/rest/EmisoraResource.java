package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Emisora;
import es.uji.apps.crm.services.EmisoraService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("emisora")
public class EmisoraResource extends CoreBaseService {
    @InjectParam
    private EmisoraService emisoraService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEmisoras() {
        List<Emisora> lista = emisoraService.getEmisoras();
        return modelToUI(lista);
    }

    private List<UIEntity> modelToUI(List<Emisora> emisoras) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (Emisora emisora : emisoras)
        {
            listaUI.add(modelToUI(emisora));
        }
        return listaUI;
    }

    private UIEntity modelToUI(Emisora emisora) {
        UIEntity entity = UIEntity.toUI(emisora);
        return entity;
    }

}