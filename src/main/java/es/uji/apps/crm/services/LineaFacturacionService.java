package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.LineaFacturacionDAO;
import es.uji.apps.crm.model.LineaFacturacion;
import es.uji.apps.crm.model.Servicio;

@Service
public class LineaFacturacionService {

    private LineaFacturacionDAO lineaFacturacionDAO;

    @Autowired
    public LineaFacturacionService(LineaFacturacionDAO lineaFacturacionDAO) {
        this.lineaFacturacionDAO = lineaFacturacionDAO;
    }

    public List<LineaFacturacion> getLineasFacturacionByServicio(Long servicio) {
        return lineaFacturacionDAO.getLineasFacturacionByServicio(servicio);
    }

    public List<LineaFacturacion> getLineasFacturacionByCampanya(Long campanyaId) {
        return lineaFacturacionDAO.getLineasFacturacionByCampanya(campanyaId);
    }

    public LineaFacturacion addLineaFacturacion(LineaFacturacion lineaFacturacion) {
        return lineaFacturacionDAO.insert(lineaFacturacion);
    }

    public void deleteLineaFacturacion(Long lineaFacturacionId) {
        lineaFacturacionDAO.delete(LineaFacturacion.class, lineaFacturacionId);
    }

    public LineaFacturacion updateLineaFacturacion(LineaFacturacion lineaFacturacion) {
        return lineaFacturacionDAO.update(lineaFacturacion);
    }

    public LineaFacturacion getLineaFacturacionById(Long id) {
        return lineaFacturacionDAO.get(LineaFacturacion.class, id).get(0);
    }

    public LineaFacturacion getLineaFacturacionActoByServicio(Servicio servicio) {
        return lineaFacturacionDAO.getLineaFacturacionActoByServicio(servicio);
    }
}