package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaCampanya;
import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.apps.crm.services.CampanyaCampanyaService;
import es.uji.apps.crm.services.TipoEstadoCampanyaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("campanyacampanya")
public class CampanyaCampanyaResource extends CoreBaseService {
    @InjectParam
    private CampanyaCampanyaService campanyaCampanyaService;
    @InjectParam
    private TipoEstadoCampanyaService tipoEstadoCampanyaService;

    @GET
    @Path("campanya/destino")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyasCampanyasDestinoByCampanya(@QueryParam("campanyaId") Long campanyaId) {
        Campanya campanya = new Campanya();
        campanya.setId(campanyaId);

        return modelToUI(campanyaCampanyaService.getCampanyasCampanyasDestinoByCampanya(campanya));
    }

    private List<UIEntity> modelToUI(List<CampanyaCampanya> campanyaCampanyas) {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        for (CampanyaCampanya campanyaCampanya : campanyaCampanyas)
        {
            lista.add(modelToUI(campanyaCampanya));
        }
        return lista;

    }

    private UIEntity modelToUI(CampanyaCampanya campanyaCampanya) {
        UIEntity entity = UIEntity.toUI(campanyaCampanya);

//        entity.put("campanyaOrigen", campanyaCampanya.getCampanyaOrigen().getNombre());
//        entity.put("campanyaDestino", campanyaCampanya.getCampanyaDestino().getNombre());
        entity.put("estadoOrigen", campanyaCampanya.getEstadoOrigen().getNombre());
        entity.put("estadoDestino", campanyaCampanya.getEstadoDestino().getNombre());

        return entity;
    }

    @GET
    @Path("campanya/origen/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyascampanyasOrigenByCampanya(@QueryParam("campanyaId") Long campanyaId) {
        Campanya campanya = new Campanya();
        campanya.setId(campanyaId);

        return modelToUI(campanyaCampanyaService.getCampanyasCampanyasOrigenByCampanya(campanya));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanyaCampanya(UIEntity entity) {
        CampanyaCampanya campanyaCampanya = UIToModel(entity);
        campanyaCampanyaService.addCampanyaCampanya(campanyaCampanya);
        return modelToUI(campanyaCampanya);
    }

    private CampanyaCampanya UIToModel(UIEntity entity) {
        CampanyaCampanya campanyaCampanya = entity.toModel(CampanyaCampanya.class);

        Campanya campanyaOrigen = new Campanya();
        campanyaOrigen.setId(ParamUtils.parseLong(entity.get("campanyaOrigenId")));
        campanyaCampanya.setCampanyaOrigen(campanyaOrigen);

        Campanya campanyaDestino = new Campanya();
        campanyaDestino.setId(ParamUtils.parseLong(entity.get("campanyaDestinoId")));
        campanyaCampanya.setCampanyaDestino(campanyaDestino);

        TipoEstadoCampanya estadoOrigen = tipoEstadoCampanyaService.getTipoEstadoCampanyaById(ParamUtils.parseLong(entity.get("estadoOrigenId")));
        campanyaCampanya.setEstadoOrigen(estadoOrigen);

        TipoEstadoCampanya estadoDestino = tipoEstadoCampanyaService.getTipoEstadoCampanyaById(ParamUtils.parseLong(entity.get("estadoDestinoId")));
        campanyaCampanya.setEstadoDestino(estadoDestino);

        return campanyaCampanya;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateCampanyaCampanya(UIEntity entity) {
        CampanyaCampanya campanyaCampanya = UIToModel(entity);
        campanyaCampanyaService.updateCampanyaCampanya(campanyaCampanya);
        return modelToUI(campanyaCampanya);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long campanyaCampanyaId) {
        campanyaCampanyaService.deleteCampanyaCampanya(campanyaCampanyaId);
    }
}