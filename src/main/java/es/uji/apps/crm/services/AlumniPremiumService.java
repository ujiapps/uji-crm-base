package es.uji.apps.crm.services;

import com.sun.jersey.core.header.FormDataContentDisposition;
import es.uji.apps.crm.model.*;
import es.uji.apps.crm.model.domains.TipoColumna;
import es.uji.apps.crm.model.domains.TipoEstadoConfirmacionDato;
import es.uji.apps.crm.utils.AppInfo;
import es.uji.apps.crm.utils.HashUtils;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.web.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Service
public class AlumniPremiumService {

    private final AlumniFormularioService alumniFormularioService;

    private final AlumniService alumniService;
    private final CampanyaClienteService campanyaClienteService;

    private final String directorioJavascript = "/crm/js/2024/";

    private final static Long CAMPANYAPREMIUM = 1070562L;
    private final static Long SERVICIOCOMUNICACIONES = 366L;

    @Context
    protected HttpServletResponse response;
    private final ClienteService clienteService;
    private final SesionService sesionService;
    private final EnvioService envioService;
    private final EnvioClienteService envioClienteService;
    private final ClienteGeneralService clienteGeneralService;
    private final EnvioPlantillaAdminService envioPlantillaAdminService;

    private final TipoService tipoService;

    @Autowired
    public AlumniPremiumService(ClienteService clienteService,
                                SesionService sesionService,
                                EnvioService envioService,
                                EnvioClienteService envioClienteService,
                                ClienteGeneralService clienteGeneralService,
                                AlumniService alumniService,
                                EnvioPlantillaAdminService envioPlantillaAdminService,
                                TipoService tipoService,
                                CampanyaClienteService campanyaClienteService, AlumniFormularioService alumniFormularioService) {
        this.clienteService = clienteService;
        this.sesionService = sesionService;
        this.envioService = envioService;
        this.envioClienteService = envioClienteService;
        this.clienteGeneralService = clienteGeneralService;
        this.alumniService = alumniService;
        this.envioPlantillaAdminService = envioPlantillaAdminService;
        this.tipoService = tipoService;
        this.campanyaClienteService = campanyaClienteService;
        this.alumniFormularioService = alumniFormularioService;
    }

    private Cliente compruebaExisteUsuarioConCorreoEIdentificacion(String correo, String identificacion) {
        Cliente cliente = clienteService.getClienteByCorreoAndIdentificacionAndServicio(correo, identificacion, SERVICIOCOMUNICACIONES);
        if (ParamUtils.isNotNull(cliente)) {
            return cliente;
        }
        return null;

    }

    public Template realizarEnvioDireccionFormularioPremium(String idioma, Long tipoIdentificacionId, String identificacion, String nombre, String apellidos, String correo, Date fechaNacimiento) throws ParseException {

        Cliente cliente = compruebaExisteUsuarioConCorreoEIdentificacion(correo, identificacion);
        if (!ParamUtils.isNotNull(cliente)) {
            ClienteGeneral clienteGeneral = clienteGeneralService.getClienteGeneralByIdentificacionSinJoinPersona(identificacion);
            //TODO si la obtención de clienteGeneral falla capturar error y mostrar al usuario que hable con el servicio.
            if (!ParamUtils.isNotNull(clienteGeneral.getId())) {
                clienteGeneral = new ClienteGeneral(identificacion, new Tipo(tipoIdentificacionId), Boolean.FALSE);
                clienteGeneralService.addClienteGeneral(clienteGeneral);
            }
            cliente = new Cliente(clienteGeneral, nombre, apellidos, correo, null, "F"
                    , null, null, null, null, null, null,
                    null,
                    null,
                    TipoEstadoConfirmacionDato.PENDIENTE.getId(),
                    TipoEstadoConfirmacionDato.PENDIENTE.getId(),
                    TipoEstadoConfirmacionDato.PENDIENTE.getId(),
                    Boolean.TRUE,
                    Boolean.TRUE,
                    Boolean.FALSE,
                    Boolean.TRUE,
                    null,
                    null,
                    fechaNacimiento,
                    SERVICIOCOMUNICACIONES);
            cliente = clienteService.addCliente(cliente);
        }

        String hash = HashUtils.stringToHashLogin();
        sesionService.renewSesion(cliente, hash);

        if (esClientePremium(cliente)){
            realizarEnvioZonaPrivada(cliente, idioma, hash);
            return alumniService.getTemplateAviso(idioma, "aviso.formulario.envioHash", "/crm/rest/alumni2024/login/", "crm/alumni2024/acceso");
        }
        //Generar el envío en CRM para que se visualice en la ficha del usuario y se envíe a mensajería a través del cron
        realizarEnvioDireccionAltaPremium(cliente, idioma, hash);
        return alumniService.getTemplateAviso(idioma, "aviso.formulario.envioHash", "/crm/rest/alumni2024/login/", "crm/alumni2024/acceso");
    }

    private boolean esClientePremium(Cliente cliente) {
        return campanyaClienteService.getCampanyaClienteByClienteId(cliente.getId()).stream().anyMatch(campanyaCliente -> campanyaCliente.getCampanya().getId().equals(CAMPANYAPREMIUM) && (campanyaCliente.getCampanyaClienteTipo().getAccion().equalsIgnoreCase("ESTADO-ALTA") || campanyaCliente.getCampanyaClienteTipo().getAccion().equalsIgnoreCase("ESTADO-PENDIENTE") || campanyaCliente.getCampanyaClienteTipo().getAccion().equalsIgnoreCase("ESTADO-ALTA-WEB")));
    }

    private void realizarEnvioZonaPrivada(Cliente cliente, String idioma, String hash) {

        String direccionWeb = AppInfo.getHost() +  "/crm/rest/alumni2024/login/hash/" + hash;

        EnvioPlantillaAdmin envioPlantillaAdmin = envioPlantillaAdminService.getEnvioPlantillaAdminByNombre("ACCESO_ZONA_PRIVADA");
        Envio envio = new Envio(envioPlantillaAdmin, idioma, direccionWeb);
        envioService.insert(envio);
        envioClienteService.addEnvioCliente(cliente, envio, cliente.getCorreo());
    }

    private void realizarEnvioDireccionAltaPremium(Cliente cliente, String idioma, String hash) {
        String direccionWeb = AppInfo.getHost() + "/crm/rest/alumni2024/premium/alta/" + hash;

        EnvioPlantillaAdmin envioPlantillaAdmin = envioPlantillaAdminService.getEnvioPlantillaAdminByNombre("ACCESO_FORMULARIO_PREMIUM");
        Envio envio = new Envio(envioPlantillaAdmin, idioma, direccionWeb);
        envioService.insert(envio);
        envioClienteService.addEnvioCliente(cliente, envio, cliente.getCorreo());
    }

    public Response guardarDatosFormularioPremium(String hash, String nombre, String apellidos,
                                                  Date fechaNacimiento, String nacionalidad, String telefono, String paisPostal, Long provinciaPostal,
                                                  Long poblacionPostal, String codigoPostal, String postal, Long nivelEstudios,
                                                  String nombreEstudio, Long estudiosSuperiores, Long estudioUJINoUJI, String tipoEstudioUJINoUJI, Long listaEstudiosGrado,
                                                  String nombreEstudioNoGrado, Long universidadEstudio, Long anyoFinalizacionEstudio, Long laboral,
                                                  Long sectorEmpresarial, String tipoTrabajo, String lugarEstudiante,
                                                  String perfilLinkedin, String cuentaBanco, Long conoce,
                                                  InputStream foto, FormDataContentDisposition fotoDetalle,
                                                  InputStream doc, FormDataContentDisposition docDetalle) throws
            ParseException, SQLException, IOException {

        Cliente cliente = sesionService.getUserIdActiveSesion(hash, Boolean.TRUE);
        alumniFormularioService.updateDatosCliente(cliente, nombre, apellidos, fechaNacimiento, nacionalidad, telefono, paisPostal, provinciaPostal, poblacionPostal, codigoPostal, postal);
        alumniService.altaClientePremium(cliente, null, conoce, cuentaBanco, CAMPANYAPREMIUM);

        alumniFormularioService.addDatosEstudios(nivelEstudios, nombreEstudio, estudiosSuperiores, estudioUJINoUJI, tipoEstudioUJINoUJI, listaEstudiosGrado, nombreEstudioNoGrado, universidadEstudio, anyoFinalizacionEstudio, cliente);
        alumniFormularioService.addDatosLaborales(laboral, sectorEmpresarial, tipoTrabajo, lugarEstudiante, cliente);
        alumniFormularioService.addPerfilLinkedin(perfilLinkedin, cliente);
        alumniFormularioService.addArchivos(foto, fotoDetalle, doc, docDetalle, cliente);

        sesionService.borraSesion(cliente);
        return Response.ok().build();
    }

    public Template getTemplateAcceso(String idioma, String path) throws ParseException {
        Template template = alumniService.getTemplateBaseAlumni(path, idioma, "premium/acceso");

        List<Tipo> tiposIdentificacion = tipoService.getTipos(TipoColumna.IDENTIFICACION.getValue());
        template.put("tiposIdentificacion", tiposIdentificacion);
        template.put("footer", directorioJavascript + "acceso.js");

        return template;
    }

    public Template getFormularioAlta(String idioma, String hash, String pathInfo) throws ParseException {
        return alumniFormularioService.getFormularioAltaPremium(idioma, hash, pathInfo);
    }
}