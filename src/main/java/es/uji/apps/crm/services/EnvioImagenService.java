package es.uji.apps.crm.services;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.crm.ui.RecursoUI;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.EnvioImagenDAO;
import es.uji.apps.crm.model.EnvioImagen;

@Service
public class EnvioImagenService {

    private EnvioImagenDAO envioImagenDAO;

    @Autowired
    public EnvioImagenService(EnvioImagenDAO envioImagenDAO) {
        this.envioImagenDAO = envioImagenDAO;
    }

    public List<EnvioImagen> getEnvioImagenesByEnvioId(Long envioId) {
        return envioImagenDAO.getEnvioImagenesByEnvioId(envioId);
    }

    public EnvioImagen addEnvioImagen(EnvioImagen envioImagen) {
        envioImagenDAO.insert(envioImagen);
        envioImagen.setId(envioImagen.getId());
        return envioImagen;
    }

    public void deleteEnvioImagen(Long envioImagen) {
        envioImagenDAO.delete(EnvioImagen.class, envioImagen);
    }

    public EnvioImagen getEnvioImagenesById(Long envioImagenId) {
        return envioImagenDAO.getEnvioImagenById(envioImagenId);
    }

    public EnvioImagen updateEnvioImagen(EnvioImagen envioImagen) {
        return envioImagenDAO.update(envioImagen);
    }

    public List<EnvioImagen> getEnvioImagenesByTarifaEnvioId(Long tarifaEnvioId) {
        return envioImagenDAO.getEnvioImagenesByTarifaEnvioId(tarifaEnvioId);
    }

    public List<EnvioImagen> getEnvioImagenesGenerico(String search) {
        return envioImagenDAO.getEnvioImagenesGenerico(search);
    }

    public List<RecursoUI> imageToRecursoUI(List<EnvioImagen> envioImagenes, String urlAde ) {
        List<RecursoUI> result = envioImagenes.stream().map(envioImagen -> {
            return new RecursoUI(envioImagen, urlAde);
        }).collect(Collectors.toList());
        return result;
    }
}