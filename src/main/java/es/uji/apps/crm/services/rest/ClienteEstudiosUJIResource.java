package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.ClienteTitulacionesUJI;
import es.uji.apps.crm.services.ClienteTitulacionesUJIService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("clienteestudiouji")
public class ClienteEstudiosUJIResource extends CoreBaseService {

    @InjectParam
    private ClienteTitulacionesUJIService clienteTitulacionesUJIService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClienteEstudiosUJIByCliente(@QueryParam("clienteId") Long clienteId) {
        List<ClienteTitulacionesUJI> clienteEstudios = clienteTitulacionesUJIService.getClienteEstudiosUJIByCliente(clienteId);
        return UIEntity.toUI(clienteEstudios);
    }

}