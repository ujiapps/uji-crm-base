package es.uji.apps.crm.services;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.crm.model.ClienteDatoOpcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ClienteTitulacionesUJIDAO;
import es.uji.apps.crm.model.ClienteTitulacionesUJI;

@Service
public class ClienteTitulacionesUJIService {

    private final ClienteTitulacionesUJIDAO clienteTitulacionesUJIDAO;

    @Autowired
    public ClienteTitulacionesUJIService(ClienteTitulacionesUJIDAO clienteTitulacionesUJIDAO) {

        this.clienteTitulacionesUJIDAO = clienteTitulacionesUJIDAO;
    }

    public List<ClienteTitulacionesUJI> getClienteEstudiosUJIByCliente(Long clienteId) {
        return clienteTitulacionesUJIDAO.getClienteEstudiosUJIByCliente(clienteId);
    }

    public List<ClienteTitulacionesUJI> getClienteEstudiosUJIByClienteJoinClasificacion(Long clienteId, String idioma) {
        return clienteTitulacionesUJIDAO.getClienteEstudiosUJIByClienteJoinClasificacion(clienteId).stream().map(
                tuple -> {
                    ClienteTitulacionesUJI clienteTitulacionesUJI = tuple.get(0, ClienteTitulacionesUJI.class);
                    ClienteDatoOpcion clienteDatoOpcion = tuple.get(1, ClienteDatoOpcion.class);

                    if (clienteTitulacionesUJI == null || clienteDatoOpcion == null) {
                        return null;
                    }
                    switch (idioma) {
                        case "es":
                            clienteTitulacionesUJI.setTipoEstudio(clienteDatoOpcion.getEtiquetaES());
                            break;
                        case "en":
                        case "uk":
                            clienteTitulacionesUJI.setTipoEstudio(clienteDatoOpcion.getEtiquetaEN());
                            break;
                        default:
                            clienteTitulacionesUJI.setTipoEstudio(clienteDatoOpcion.getEtiqueta());
                            break;
                    }
                    return clienteTitulacionesUJI;
                }
        ).collect(Collectors.toList());
    }
}