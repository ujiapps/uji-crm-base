package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ProgramaUsuarioDAO;
import es.uji.apps.crm.model.PersonaPasPdi;
import es.uji.apps.crm.model.Programa;
import es.uji.apps.crm.model.ProgramaUsuario;

@Service
public class ProgramaUsuarioService {
    private ProgramaUsuarioDAO programaUsuarioDAO;

    @Autowired
    public ProgramaUsuarioService(ProgramaUsuarioDAO programaUsuarioDAO) {
        this.programaUsuarioDAO = programaUsuarioDAO;
    }

    public List<ProgramaUsuario> getProgramasUsuariosByProgramaId(Long programaId) {
        return programaUsuarioDAO.getProgramaUsuariosByProgramaId(programaId);
    }

    public ProgramaUsuario insertaNuevoProgramaUsuario(Long personaId, Long programaId, String tipo) {
        ProgramaUsuario programaUsuario = new ProgramaUsuario();

        PersonaPasPdi personaPasPdi = new PersonaPasPdi();
        personaPasPdi.setId(personaId);
        programaUsuario.setPersonaPasPdi(personaPasPdi);

        Programa programa = new Programa();
        programa.setId(programaId);
        programaUsuario.setPrograma(programa);

        programaUsuario.setTipo(tipo);

        programaUsuarioDAO.insert(programaUsuario);
        programaUsuario.setId(programaUsuario.getId());

        return programaUsuario;
    }

    public ProgramaUsuario modificaProgramaUsuario(String tipo, Long programaUsuarioId) {
        ProgramaUsuario programaUsuario = getProgramasUsuariosById(programaUsuarioId);

        programaUsuario.setTipo(tipo);

        programaUsuarioDAO.update(programaUsuario);
        return programaUsuario;

    }

    private ProgramaUsuario getProgramasUsuariosById(Long programaUsuarioId) {
        return programaUsuarioDAO.getProgramasUsuarioById(programaUsuarioId);
    }

    public void delete(Long programaUsuarioId) {
        programaUsuarioDAO.delete(ProgramaUsuario.class, programaUsuarioId);

    }

}