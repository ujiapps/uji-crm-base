package es.uji.apps.crm.services.rest;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.crm.exceptions.ErrorEnBorradoDeDocumentoException;
import es.uji.apps.crm.exceptions.ErrorSubiendoDocumentoException;
import es.uji.apps.crm.model.Envio;
import es.uji.apps.crm.model.EnvioAdjunto;
import es.uji.apps.crm.model.EnvioCliente;
import es.uji.apps.crm.model.FiltroEnvio;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.services.EnvioClienteService;
import es.uji.apps.crm.services.EnvioService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.apps.crm.ui.EnvioUI;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.model.FieldValidationException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Component
@Path("envio")
public class EnvioResource extends CoreBaseService {

    @InjectParam
    private EnvioService envioService;

    @InjectParam
    private EnvioClienteService envioClienteService;

    @InjectParam
    private UtilsService utilsService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getEnvios(@QueryParam("busqueda") String busqueda,
                                     @QueryParam("fechaDesde") String fechaDesde,
                                     @QueryParam("fechaHasta") String fechaHasta,
                                     @QueryParam("tipoEnvioId") Long tipoEnvioId,
                                     @QueryParam("persona") Long persona,
                                     @QueryParam("estado") Long estado,
                                     @QueryParam("email") String email,
                                     @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                     @QueryParam("start") @DefaultValue("0") Long start,
                                     @QueryParam("limit") @DefaultValue("25") Long limit,
                                     @QueryParam("page") @DefaultValue("0") Integer pag) throws ParseException {


        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit);

            if (sortJson.length() > 2) {
                List<Map<String, String>> sortList = new ObjectMapper().readValue(sortJson, new TypeReference<List<Map<String, String>>>() {
                });
                paginacion.setOrdenarPor(sortList.get(0).get("property"));
                paginacion.setDireccion(sortList.get(0).get("direction"));
            }

            Date desde = (fechaDesde != null && !fechaDesde.isEmpty()) ? utilsService.fechaParse(
                    fechaDesde.substring(0, 10), "dd/MM/yyyy") : null;
            Date hasta = (fechaHasta != null && !fechaHasta.isEmpty()) ? utilsService.fechaParse(
                    fechaHasta.substring(0, 10), "dd/MM/yyyy") : null;

            FiltroEnvio filtroEnvio = new FiltroEnvio();
            filtroEnvio.setBusqueda(busqueda);
            filtroEnvio.setFechaDesde(desde);
            filtroEnvio.setFechaHasta(hasta);
            filtroEnvio.setTipoEnvio(tipoEnvioId);
            filtroEnvio.setEmail(email);
            filtroEnvio.setPersona(persona);
            filtroEnvio.setEstado(estado);

            Long connectedUserId = AccessManager.getConnectedUserId(request);

            List<Envio> lista = envioService.getEnviosGenerico(connectedUserId, filtroEnvio, paginacion);

            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(UIEntity.toUI(lista));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            e.printStackTrace();
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Path("seguimiento/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEnviosSeguimiento(@PathParam("id") Long seguimientoId) {
        List<Envio> lista = envioService.getEnviosSeguimiento(seguimientoId);
        return UIEntity.toUI(lista);
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getEnvioById(@PathParam("id") Long envioId) {
        return UIEntity.toUI(envioService.getEnvioById(envioId));
    }

    @GET
    @Path("{id}/adjunto")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAdjuntosByEnvioId(@PathParam("id") Long envioId) {
        List<EnvioAdjunto> listaAdjuntos = envioService.getAdjuntosByEnvioId(envioId);
        return UIEntity.toUI(listaAdjuntos);
    }

    @GET
    @Path("cliente/{clienteId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEnviosByClienteId(@PathParam("clienteId") Long clienteId) {
        return UIEntity.toUI(envioService.getEnviosByClienteId(clienteId));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addEnvio(UIEntity entity) throws ParseException {
        String nombre = entity.get("nombre");
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String tipoId = entity.get("envioTipoId");
        String seguimientoId = entity.get("seguimientoId");

        ParamUtils.checkNotNull(nombre, tipoId);
        Envio envio;
        if (ParamUtils.isNotNull(seguimientoId)) {
            envio = envioService.getEnvioNuevoConSeguimiento(connectedUserId,
                    ParamUtils.parseLong(tipoId), ParamUtils.parseLong(seguimientoId));
        } else {
            envio = envioService.getEnvioNuevo(connectedUserId, ParamUtils.parseLong(tipoId));
        }
        if (envio == null) {
            envio = envioService.addEnvio(connectedUserId, new EnvioUI(connectedUserId, entity).getEnvio());
        }
        return UIEntity.toUI(envio);
    }

    @POST
    @Path("cliente")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addEnvioClienteEnviar(FormDataMultiPart multiPart) throws ErrorSubiendoDocumentoException,
            IOException, FieldValidationException, MessageNotSentException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String para = multiPart.getField("comboMail").getValue();
        Long clienteId = ParamUtils.parseLong(String.valueOf(multiPart.getField("clienteId").getValue()));

        Envio envio = null;
        if (multiPart.getField("envioId").getValue().equals("")) {
            envio = new EnvioUI(connectedUserId, multiPart).getEnvio();
            envioService.addEnvio(connectedUserId, envio);
        } else {
            envio = envioService.getEnvioById(ParamUtils.parseLong(multiPart.getField("envioId").getValue()));
        }

        EnvioCliente envioCliente = null;
        if (multiPart.getField("id").getValue().equals("")) {
            envioCliente = envioClienteService.addEnvioACliente(envio.getId(), clienteId, para);
        } else {
            envioCliente = envioClienteService.getEnvioClienteById(ParamUtils.parseLong(multiPart.getField("id").getValue()));
        }
        envioService.marcarEnviado(envio);
        envioClienteService.marcarEnviado(envioCliente);

        Map<String, Object> datosDocumento = null;
        if (multiPart.getField("anyadirArchivo") != null &&
                !multiPart.getField("anyadirArchivo").getFormDataContentDisposition().getFileName().equals("")) {
            datosDocumento = envioService.insertarAdjunto(multiPart, envio);
        }
        envioService.enviaCorreo(envio, envioCliente.getId(),  para, datosDocumento);
        return UIEntity.toUI(envio);
    }

    @POST
    @Path("cliente/{clienteId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addEnvioCliente(UIEntity uiEntity, @PathParam("clienteId") Long clienteId) throws ParseException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String para = uiEntity.get("comboMail");
        uiEntity.put("envioTipoId", 27);
        Envio envio = new EnvioUI(connectedUserId, uiEntity).getEnvio();
        envio.setNombre("Envio Manual");
        envioService.addEnvio(connectedUserId, envio);
        envioClienteService.addEnvioACliente(envio.getId(), clienteId, para);
        return UIEntity.toUI(envio);
    }

    @PUT
    @Path("{envioId}/cliente")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateEnvioCliente(@PathParam("envioId") Long envioId, UIEntity entity) throws ParseException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        entity.put("envioTipoId", 27);
        Envio envio = new EnvioUI(connectedUserId, entity).getEnvio();
        envio.setNombre("Envio Manual");
        return UIEntity.toUI(envioService.update(envio));
    }

    @POST
    @Path("prueba")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage addEnvioPrueba(MultivaluedMap<String, String> params) throws MessageNotSentException {
        String correo = params.getFirst("correoPrueba");
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(false);
        if (ParamUtils.isNotNull(correo)) {
            envioService.envioPrueba(params);
            responseMessage.setSuccess(true);
        }
        return responseMessage;
    }

    @PUT
    @Path("autoguardado")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void autoguardado (@FormParam("envioId") Long envioId, @FormParam("cuerpo") String cuerpo){
        envioService.autoguardado(envioId, cuerpo);
    }

    @POST
    @Path("{envioId}/adjunto/")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity addClienteFichero(FormDataMultiPart multiPart, @PathParam("envioId") Long envioId)
            throws IOException, ErrorSubiendoDocumentoException {

        Envio envio = new Envio();
        envio.setId(envioId);

        Map<String, Object> datosDocumento = envioService.insertarAdjunto(multiPart, envio);

        return UIEntity.toUI(datosDocumento.get("envioAdjunto"));
    }

    @POST
    @Path("{envioId}/duplicar/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity duplicaEnvioGenerico(@PathParam("envioId") Long envioId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(envioService.duplicarEnvioGenerico(envioId, connectedUserId));
    }

    @PUT
    @Path("{id}/enviar/{enviar}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity updateEnvio(MultivaluedMap<String, String> params,
                                @PathParam("id") String envioId, @PathParam("enviar") Long enviar)
            throws ParseException {
        Envio envioParam = envioService.getEnvioById(ParamUtils.parseLong(envioId));
        String nombre = params.getFirst("nombre");

        ParamUtils.checkNotNull(nombre, envioId);
        return UIEntity.toUI(envioService.update(new EnvioUI(envioParam, params, enviar).getEnvio()));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteEnvio(@PathParam("id") Long envioId)
            throws MalformedURLException, ErrorEnBorradoDeDocumentoException {
        envioService.deleteEnvio(envioId);
    }

    @DELETE
    @Path("{envioId}/adjunto/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long envioAdjuntoId)
            throws MalformedURLException, ErrorEnBorradoDeDocumentoException {
        envioService.deleteEnvioAdjunto(envioAdjuntoId);
    }

//    private UIEntity modelToUI(Envio envio) {
//        UIEntity entity = UIEntity.toUI(envio);
//        entity.put("envioTipoNombre", envio.getEnvioTipo().getNombre());
//        return entity;
//    }
//
//    private List<UIEntity> modelToUI(List<Envio> lista) {
//        List<UIEntity> listaUI = new ArrayList<>();
//
//        for (Envio envio : lista) {
//            listaUI.add(modelToUI(envio));
//        }
//        return listaUI;
//
//    }
}