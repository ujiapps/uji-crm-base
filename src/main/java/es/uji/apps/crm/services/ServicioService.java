package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ServicioDAO;
import es.uji.apps.crm.model.Servicio;

@Service
public class ServicioService {
    private ServicioDAO servicioDAO;

    @Autowired
    public ServicioService(ServicioDAO servicioDAO) {
        this.servicioDAO = servicioDAO;
    }

    public List<Servicio> getServicios() {
        return servicioDAO.getServicios();
    }

    public Servicio getServicioById(Long servicioId) {
        return servicioDAO.getServicioById(servicioId);
    }

    public List<Servicio> getServiciosByUsuario(Long connectedUserId) {
        return servicioDAO.getServiciosByUsuario(connectedUserId);
    }

    public Servicio addServicio(Servicio servicio) {
        servicioDAO.insert(servicio);
        servicio.setId(servicio.getId());
        return servicio;
    }

    public Servicio updateServicio(Servicio servicio) {
        return servicioDAO.update(servicio);

    }

    public void delete(Long servicioId) {
        servicioDAO.delete(Servicio.class, servicioId);
    }

}