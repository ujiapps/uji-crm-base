package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.EnvioCriterioEstudioNoUJIDAO;
import es.uji.apps.crm.model.EnvioCriterio;
import es.uji.apps.crm.model.EnvioCriterioEstudioNoUJI;
import es.uji.apps.crm.model.TipoEstudio;

@Service
public class EnvioCriterioEstudioNoUJIService {
    private EnvioCriterioEstudioNoUJIDAO envioCriterioEstudioNoUJIDAO;

    @Autowired
    public EnvioCriterioEstudioNoUJIService(EnvioCriterioEstudioNoUJIDAO envioCriterioEstudioNoUJIDAO) {
        this.envioCriterioEstudioNoUJIDAO = envioCriterioEstudioNoUJIDAO;
    }

    public void insertEnvioCriteriosEstudioNoUJI(List<Long> arrayCriteriosEstudioNoUJI, Long envioCriterioId, Long anyoFinalizacionEstudioNoUJIInicio, Long anyoFinalizacionEstudioNoUJIFin) {

        for (Long estudioNoUJIId : arrayCriteriosEstudioNoUJI)
        {
            insertEnvioCriterioEstudioNoUJI(estudioNoUJIId, envioCriterioId, anyoFinalizacionEstudioNoUJIInicio, anyoFinalizacionEstudioNoUJIFin);
        }

    }

    public void insertEnvioCriterioEstudioNoUJI(Long estudioNoUJIId, Long envioCriterioId, Long anyoFinalizacionEstudioNoUJIInicio, Long anyoFinalizacionEstudioNoUJIFin) {

        EnvioCriterioEstudioNoUJI envioCriterioEstudioNoUJI = new EnvioCriterioEstudioNoUJI();

        TipoEstudio estudio = new TipoEstudio();
        estudio.setId(estudioNoUJIId);
        envioCriterioEstudioNoUJI.setEstudioNoUJI(estudio);

        EnvioCriterio envioCriterio = new EnvioCriterio();
        envioCriterio.setId(envioCriterioId);
        envioCriterioEstudioNoUJI.setEnvioCriterio(envioCriterio);

        envioCriterioEstudioNoUJI.setAnyoFinalizacionEstudioNoUJIInicio(anyoFinalizacionEstudioNoUJIInicio);
        envioCriterioEstudioNoUJI.setAnyoFinalizacionEstudioNoUJIFin(anyoFinalizacionEstudioNoUJIFin);

        envioCriterioEstudioNoUJIDAO.insert(envioCriterioEstudioNoUJI);

    }

    public List<EnvioCriterioEstudioNoUJI> getEnvioCriterioEstudioNoUJIByEnvioCriterioId(Long envioCriterioId) {
        return envioCriterioEstudioNoUJIDAO.get(EnvioCriterioEstudioNoUJI.class, "envio_criterio_id = " + envioCriterioId);
    }

    public void deleteEnvioCriteriosEstudioNoUJI(Long envioCriterioId) {
        envioCriterioEstudioNoUJIDAO.delete(EnvioCriterioEstudioNoUJI.class, "envio_criterio_id = " + envioCriterioId);

    }
}