package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.ProgramaUsuario;
import es.uji.apps.crm.services.PersonaPasPdiService;
import es.uji.apps.crm.services.ProgramaUsuarioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("programausuario")
public class ProgramaUsuarioResource extends CoreBaseService {
    @InjectParam
    private ProgramaUsuarioService consultaProgramaUsuario;
    @InjectParam
    private PersonaPasPdiService consultaPersonaPasPdi;

    @GET
    @Path("programa")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getProgramaUsuariosByProgramaId(@QueryParam("programaId") Long programaId) {
        List<ProgramaUsuario> programasUsuario = consultaProgramaUsuario
                .getProgramasUsuariosByProgramaId(programaId);

        return modelToUI(programasUsuario);
    }

    private List<UIEntity> modelToUI(List<ProgramaUsuario> programaUsuarios) {
        List<UIEntity> listaUI = new ArrayList<>();
        for (ProgramaUsuario programaUsuario : programaUsuarios)
        {
            listaUI.add(modelToUI(programaUsuario));
        }
        return listaUI;
    }

    private UIEntity modelToUI(ProgramaUsuario programaUsuario) {
        UIEntity entity = UIEntity.toUI(programaUsuario);
        entity.put("nombrePersona", programaUsuario.getPersonaPasPdi().getNombre());
        return entity;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addPersonaUsuario(UIEntity entity) {
        String personaId = entity.get("personaPasPdiId");
        String programaId = entity.get("programaId");
        String tipo = entity.get("tipo");

        ParamUtils.checkNotNull(personaId, programaId, tipo);

        ProgramaUsuario programaUsuario = consultaProgramaUsuario.insertaNuevoProgramaUsuario(
                ParamUtils.parseLong(personaId), ParamUtils.parseLong(programaId), tipo);

        return UIEntity.toUI(programaUsuario);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity modificaPersonaUsuario(@PathParam("id") Long programaUsuarioId, UIEntity entity) {
        String tipo = entity.get("tipo");

        ParamUtils.checkNotNull(tipo);

        ProgramaUsuario programaUsuario = consultaProgramaUsuario.modificaProgramaUsuario(tipo,
                programaUsuarioId);

        return UIEntity.toUI(programaUsuario);

    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long programaUsuarioId) {
        consultaProgramaUsuario.delete(programaUsuarioId);
    }

}