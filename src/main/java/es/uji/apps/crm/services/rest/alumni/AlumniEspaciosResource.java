package es.uji.apps.crm.services.rest.alumni;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.auth.UserSessionManager;
import es.uji.apps.crm.dao.ActualizaPersona;
import es.uji.apps.crm.services.AlumniEspacioService;
import es.uji.apps.crm.services.AlumniService;
import es.uji.apps.crm.services.ClienteSuscripcionesService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.util.List;

public class AlumniEspaciosResource extends CoreBaseService {

    private final static Boolean FICHAZONAPRIVADA = Boolean.TRUE;

    @InjectParam
    private AlumniService alumniService;
    @InjectParam
    private ActualizaPersona actualizaPersona;
    @InjectParam
    private ClienteSuscripcionesService clienteSuscripcionService;
    @InjectParam
    private AlumniEspacioService alumniEspacioService;

    @InjectParam
    private UserSessionManager userSessionManager;

    @GET
    @Path("suscripciones")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAlumniCampanyas(@CookieParam("p_hash") String
                                                     pHash, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        return clienteSuscripcionService.getListaItemsUnificada(userSessionManager.getCliente().getId());
    }

    @GET
    @Path("zonaprivada")
    @Produces(MediaType.TEXT_HTML)
    public Template getAlumniTemplateEspacio(@CookieParam("p_hash") String pHash,
                                             @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        return alumniEspacioService.getAlumniTemplateEspacio(userSessionManager.getCliente(), request.getPathInfo(), idioma);
    }

    @POST
    @Path("suscripciones")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity actualizaSuscriptores(@CookieParam("p_hash") String pHash,
                                          @CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                          @FormParam("checked") Boolean checked,
                                          @FormParam("itemId") Long itemId){

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        alumniService.actualizaSuscriptores(itemId, userSessionManager.getCliente(), checked);

        return new UIEntity();
    }

    @POST
    @Path("corporativo")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity creaCorporativo(@CookieParam("p_hash") String
                                            pHash, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        String usuario = actualizaPersona.creaCuentaCorporativa(userSessionManager.getConnectedUserId());
        UIEntity entity = new UIEntity();
        entity.put("usuario", usuario);
        return entity;
    }


}
