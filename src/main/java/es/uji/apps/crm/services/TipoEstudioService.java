package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.TipoEstudioDAO;
import es.uji.apps.crm.model.TipoEstudio;

@Service
public class TipoEstudioService {
    private TipoEstudioDAO tipoEstudioDAO;

    @Autowired
    public TipoEstudioService(TipoEstudioDAO tipoEstudioDAO) {
        this.tipoEstudioDAO = tipoEstudioDAO;
    }

    public TipoEstudio getTipoEstudioById(Long clasificacion) {
        return tipoEstudioDAO.getTipoEstudioById(clasificacion);
    }

    public List<TipoEstudio> getTipoEstudioByTipo(Long tipoEstudioTipo) {
        return tipoEstudioDAO.getTipoEstudioByTipo(tipoEstudioTipo);
    }
}