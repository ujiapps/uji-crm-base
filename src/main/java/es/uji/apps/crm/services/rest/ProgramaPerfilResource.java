package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Perfil;
import es.uji.apps.crm.model.Programa;
import es.uji.apps.crm.model.ProgramaPerfil;
import es.uji.apps.crm.services.ProgramaPerfilService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("programaperfil")
public class ProgramaPerfilResource extends CoreBaseService {
    @InjectParam
    private ProgramaPerfilService programaPerfilService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getProgramaPerfilesByProgramaId(@QueryParam("programaId") Long programaId) {
        List<ProgramaPerfil> lista = programaPerfilService
                .getProgramaPerfilesByProgramaId(programaId);
        return modelToUI(lista);
    }

    private List<UIEntity> modelToUI(List<ProgramaPerfil> programaPerfiles) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (ProgramaPerfil programaPerfil : programaPerfiles)
        {
            listaUI.add(modelToUI(programaPerfil));
        }
        return listaUI;
    }

    private UIEntity modelToUI(ProgramaPerfil programaPerfil) {
        UIEntity entity = UIEntity.toUI(programaPerfil);

        ProgramaPerfil fetchProgramaPerfil = programaPerfilService
                .getProgramaPerfilById(programaPerfil.getId());

        entity.put("programaNombre", fetchProgramaPerfil.getPrograma().getNombreCa());
        entity.put("perfilNombre", fetchProgramaPerfil.getPerfil().getNombre());

        return entity;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getProgramaPerfilById(@PathParam("id") Long programaPerfilId) {
        return modelToUI(programaPerfilService.getProgramaPerfilById(programaPerfilId));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addProgramaPerfil(UIEntity entity) {
        ProgramaPerfil programaPerfil = UIToModel(entity);
        programaPerfilService.addProgramaPerfil(programaPerfil);
        return modelToUI(programaPerfil);
    }

    private ProgramaPerfil UIToModel(UIEntity entity) {
        ProgramaPerfil programaPerfil = entity.toModel(ProgramaPerfil.class);

        Perfil perfil = new Perfil();
        perfil.setId(Long.parseLong(entity.get("perfilId")));
        programaPerfil.setPerfil(perfil);

        Programa programa = new Programa();
        programa.setId(Long.parseLong(entity.get("programaId")));
        programaPerfil.setPrograma(programa);

        return programaPerfil;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateProgramaPerfil(@PathParam("id") Long programaPerfilId, UIEntity entity) {
        ProgramaPerfil programaPerfil = UIToModel(entity);
        programaPerfilService.updateProgramaPerfil(programaPerfil);
        return modelToUI(programaPerfil);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long programaPerfilId) {
        programaPerfilService.deleteProgramaPerfil(programaPerfilId);
    }

}