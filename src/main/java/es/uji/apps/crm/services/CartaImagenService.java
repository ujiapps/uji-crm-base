package es.uji.apps.crm.services;

import es.uji.apps.crm.dao.CartaImagenDAO;
import es.uji.apps.crm.model.CartaImagen;
import es.uji.apps.crm.ui.RecursoUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CartaImagenService {

    private CartaImagenDAO cartaImagenDAO;

    @Autowired
    public CartaImagenService(CartaImagenDAO cartaImagenDAO) {
        this.cartaImagenDAO = cartaImagenDAO;
    }

    public List<CartaImagen> getCartaImagenesByCampanyaCartaId(Long campanyaCartaId) {
        return cartaImagenDAO.getCartaImagenesByCampanyaCartaId(campanyaCartaId);
    }

    public CartaImagen addCartaImagen(CartaImagen cartaImagen) {
        cartaImagenDAO.insert(cartaImagen);
        cartaImagen.setId(cartaImagen.getId());
        return cartaImagen;
    }

    public void deleteCartaImagen(Long cartaImagen) {
        cartaImagenDAO.delete(CartaImagen.class, cartaImagen);
    }

    public CartaImagen getCartaImagenesById(Long cartaImagenId) {
        return cartaImagenDAO.getCartaImagenById(cartaImagenId);
    }

    public List<RecursoUI> imageToRecursoUI(List<CartaImagen> cartaImagenes, String urlAde) {
        List<RecursoUI> result = cartaImagenes.stream().map(cartaImagen -> {
            return new RecursoUI(cartaImagen, urlAde);
        }).collect(Collectors.toList());
        return result;
    }
}