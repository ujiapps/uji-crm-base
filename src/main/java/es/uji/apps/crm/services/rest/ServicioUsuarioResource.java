package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.model.ServicioUsuario;
import es.uji.apps.crm.services.PersonaPasPdiService;
import es.uji.apps.crm.services.ServicioUsuarioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("serviciousuario")
public class ServicioUsuarioResource extends CoreBaseService {
    @InjectParam
    private ServicioUsuarioService consultaServicioUsuario;

    @InjectParam
    private PersonaPasPdiService consultaPersonaPasPdi;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getServiciosUsuarios() {
        return UIEntity.toUI(consultaServicioUsuario.getServiciosUsuarios());
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getServiciosUsuarios(@PathParam("id") Long servicioId) {
        List<ServicioUsuario> serviciosUsuariosByServicioId = consultaServicioUsuario
                .getServiciosUsuariosByServicioId(servicioId);
        return modelToUI(serviciosUsuariosByServicioId);
    }

    private List<UIEntity> modelToUI(List<ServicioUsuario> servicioUsuarios) {
        List<UIEntity> listaUI = new ArrayList<>();

        for (ServicioUsuario servicioUsuario : servicioUsuarios)
        {
            listaUI.add(modelToUI(servicioUsuario));
        }
        return listaUI;
    }

    private UIEntity modelToUI(ServicioUsuario servicioUsuario) {
        UIEntity entity = UIEntity.toUI(servicioUsuario);
        entity.put("nombrePersona", servicioUsuario.getPersonaPasPdi().getNombre());
        return entity;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addServicioUsuario(UIEntity entity) {
        String personaId = entity.get("personaPasPdiId");
        String servicioId = entity.get("servicioId");

        ParamUtils.checkNotNull(personaId, servicioId);

        ServicioUsuario servicioUsuario = consultaServicioUsuario.insertaNuevoServicioUsuario(
                Long.parseLong(personaId), Long.parseLong(servicioId));

        return UIEntity.toUI(servicioUsuario);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long servicioUsuarioId) {
        consultaServicioUsuario.delete(servicioUsuarioId);
    }
}