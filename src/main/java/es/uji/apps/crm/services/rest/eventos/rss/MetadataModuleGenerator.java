package es.uji.apps.crm.services.rest.eventos.rss;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.jdom.Element;
import org.jdom.Namespace;

import com.sun.syndication.feed.module.Module;
import com.sun.syndication.io.ModuleGenerator;

public class MetadataModuleGenerator implements ModuleGenerator {

    public String getNamespaceUri() {
        return MetadataEntry.URI;
    }

    public java.util.Set<Namespace> getNamespaces() {
        Set<Namespace> namespaces = new HashSet<Namespace>();

        namespaces.add(MetadataEntry.namespace_general);
        namespaces.add(MetadataEntry.namespace_metadato);

        return namespaces;
    }

    public void generate(Module module, Element element) {
        MetadataEntry modulePortalMetadata = (MetadataEntry) module;

        for (Map.Entry<String, ValorAtributo> field : modulePortalMetadata.getFields().entrySet())
        {
            element.addContent(generateSimpleElement(field.getKey(), field.getValue()));
        }
    }

    protected Element generateSimpleElement(String name, ValorAtributo values) {
        Element element = new Element(name, new MetadataEntryImplCRM().getNamespace(values
                .getNamespace()));
        element.addContent(values.getValor());

        return element;
    }
}