package es.uji.apps.crm.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.dao.BuscaCuenta;
import es.uji.apps.crm.dao.CampanyaClienteAnyadirAutoDAO;
import es.uji.apps.crm.dao.ClienteDAO;
import es.uji.apps.crm.exceptions.BusquedaVaciaException;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaClienteAnyadirAuto;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteDato;
import es.uji.apps.crm.model.ClienteGeneral;
import es.uji.apps.crm.model.ClienteGrid;
import es.uji.apps.crm.model.EnvioCliente;
import es.uji.apps.crm.model.FiltroCliente;
import es.uji.apps.crm.model.Movimiento;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.model.Pais;
import es.uji.apps.crm.model.Poblacion;
import es.uji.apps.crm.model.Provincia;
import es.uji.apps.crm.model.Servicio;
import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.model.domains.TipoCliente;
import es.uji.apps.crm.model.domains.TipoEstadoConfirmacionDato;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClienteService {

    @InjectParam
    ClienteGeneralService clienteGeneralService;
    private final EnvioClienteService envioClienteService;
    private final MovimientoService movimientoService;
    private final ServicioService servicioService;
    private final ClienteDAO clienteDAO;
    private final CampanyaClienteAnyadirAutoDAO campanyaClienteAnyadirAutoDAO;
    private final BuscaCuenta buscaCuenta;

    @Autowired
    public ClienteService(ClienteDAO clienteDAO, CampanyaClienteAnyadirAutoDAO campanyaClienteAnyadirAutoDAO, EnvioClienteService envioClienteService,
                          MovimientoService movimientoService, BuscaCuenta buscaCuenta, ServicioService servicioService) {
        this.clienteDAO = clienteDAO;
        this.campanyaClienteAnyadirAutoDAO = campanyaClienteAnyadirAutoDAO;
        this.movimientoService = movimientoService;
        this.envioClienteService = envioClienteService;
        this.buscaCuenta = buscaCuenta;
        this.servicioService = servicioService;
    }

    public List<ClienteGrid> getClientesByBusqueda(Long connectedUserId, FiltroCliente filtro, Paginacion paginacion)
            throws BusquedaVaciaException, ParseException {
        if (!ParamUtils.isNotNull(filtro.getBusquedaGenerica())
                && !ParamUtils.isNotNull(filtro.getEtiqueta())
                && !ParamUtils.isNotNull(filtro.getCampanyaId())
                && !ParamUtils.isNotNull(filtro.getTipoDato())
                && !ParamUtils.isNotNull(filtro.getCorreo())
                && !ParamUtils.isNotNull(filtro.getMovil())
                && !ParamUtils.isNotNull(filtro.getPostal())
                && !ParamUtils.isNotNull(filtro.getPrograma())
                && !ParamUtils.isNotNull(filtro.getTipoCliente())
                && !ParamUtils.isNotNull(filtro.getNacionalidad())
                && !ParamUtils.isNotNull(filtro.getCodigoPostal())
                && !ParamUtils.isNotNull(filtro.getPaisDomicilio())
                && !ParamUtils.isNotNull(filtro.getProvinciaDomicilio())
                && !ParamUtils.isNotNull(filtro.getPoblacionDomiclio())
                && !ParamUtils.isNotNull(filtro.getEstudioUJI())
                && filtro.getEstudioUJIBeca().isEmpty()
                && !ParamUtils.isNotNull(filtro.getEstudioNoUJI())) {
            throw new BusquedaVaciaException();
        }

        return clienteDAO.getClienteByBusqueda(connectedUserId, filtro, paginacion);
    }

    public List<ClienteDato> getClientesDatosLista(Long clienteId) {
        return clienteDAO.getClientesDatos(clienteId);
    }

    public List<Cliente> getClientesByItemId(Long itemId) {
        return clienteDAO.getClientesByItemId(itemId);
    }

    public List<Cliente> getClientesByEnvioId(Long envioId) {

        return clienteDAO.getClientesByEnvioId(envioId);
    }

    public Cliente addCliente(Cliente cliente) {

        if (ParamUtils.isNotNull(cliente.getCorreo())) {
            if (!ParamUtils.isNotNull(cliente.getCorreoEstado())) {
                Tipo tipo = new Tipo();
                tipo.setId(TipoEstadoConfirmacionDato.VALIDO.getId());
                cliente.setCorreoEstado(tipo);
            }
        }

        if (ParamUtils.isNotNull(cliente.getMovil())) {
            if (!ParamUtils.isNotNull(cliente.getMovilEstado())) {
                Tipo tipo = new Tipo();
                tipo.setId(TipoEstadoConfirmacionDato.VALIDO.getId());
                cliente.setMovilEstado(tipo);
            }
        }

        if (ParamUtils.isNotNull(cliente.getPostal())) {
            if (!ParamUtils.isNotNull(cliente.getPostalEstado())) {
                Tipo tipo = new Tipo();
                tipo.setId(TipoEstadoConfirmacionDato.VALIDO.getId());
                cliente.setPostalEstado(tipo);
            }
        }

        clienteDAO.insert(cliente);
        cliente.setId(cliente.getId());

        return cliente;
    }

    public Cliente UIToModel(UIEntity entity) {
        Cliente cliente = entity.toModel(Cliente.class);


        String paisId = entity.get("paisPostalId");
        if (ParamUtils.isNotNull(paisId)) {
            cliente.setPaisPostal(new Pais(paisId));
        }

        String poblacionPostalId = entity.get("poblacionPostalId");
        if (ParamUtils.isNotNull(poblacionPostalId)) {
            cliente.setPoblacionPostal(new Poblacion(ParamUtils.parseLong(poblacionPostalId)));
        }

        String provinciaPostalId = entity.get("provinciaPostalId");
        if (ParamUtils.isNotNull(provinciaPostalId)) {
            cliente.setProvinciaPostal(new Provincia(ParamUtils.parseLong(provinciaPostalId)));
        }

        cliente.setFechaNacimiento(entity.getDate("fechaNacimiento"));

        String correoEstadoId = entity.get("correoEstadoId");
        if ((correoEstadoId != null) && (!correoEstadoId.isEmpty())) {
            Tipo correoEstado = new Tipo();
            correoEstado.setId(Long.parseLong(correoEstadoId));
            cliente.setCorreoEstado(correoEstado);
        }

        String movilEstadoId = entity.get("movilEstadoId");
        if ((movilEstadoId != null) && (!movilEstadoId.isEmpty())) {
            Tipo movilEstado = new Tipo();
            movilEstado.setId(Long.parseLong(movilEstadoId));
            cliente.setMovilEstado(movilEstado);
        }

        String postalEstadoId = entity.get("postalEstadoId");
        if ((postalEstadoId != null) && (!postalEstadoId.isEmpty())) {
            Tipo postalEstado = new Tipo();
            postalEstado.setId(Long.parseLong(postalEstadoId));
            cliente.setPostalEstado(postalEstado);
        }
        cliente.setRecibePostal(Boolean.TRUE);
        cliente.setRecibeCorreo(Boolean.TRUE);

        return cliente;
    }

    public Cliente updateCliente(Cliente cliente) {
        clienteDAO.update(cliente);
        return cliente;
    }

    public void addGridClientesACampanya(FiltroCliente filtro, Long campanyaAnyadir,
                                         Long estadoAnyadir) {

        List<Long> clientes = clienteDAO.addGridClientesACampanya(filtro
        );

        Campanya campanya = new Campanya();
        campanya.setId(campanyaAnyadir);

        Tipo estado = new Tipo();
        estado.setId(estadoAnyadir);

        CampanyaClienteAnyadirAuto campanyaCliente = new CampanyaClienteAnyadirAuto();
        campanyaCliente.setCampanya(campanya);
        campanyaCliente.setCampanyaClienteEstado(estado);
        campanyaCliente.setClientes(clientes.toString().replace("[", "").replace("]", "").replace(" ", ""));
        campanyaClienteAnyadirAutoDAO.insert(campanyaCliente);
    }

    public void delete(Long connectedUserId, Long clienteId) {

        List<EnvioCliente> enviosCliente = envioClienteService.getEnvioClientesByClienteId(clienteId, null);
        for (EnvioCliente envioCliente : enviosCliente) {
            envioClienteService.delete(envioCliente.getId());
        }

        List<Movimiento> movimientos = movimientoService.getMovimientosByClienteId(clienteId);
        for (Movimiento movimiento : movimientos) {
            movimientoService.delete(movimiento.getId());
        }
        Cliente cliente = getClienteById(clienteId);
        Long clienteGeneralId = cliente.getClienteGeneral().getId();
        List<UIEntity> clientes = getClientesByClienteGeneralId(connectedUserId, clienteGeneralId);
        clienteDAO.delete(Cliente.class, clienteId);

        if (clientes.size() == 1L) {
            clienteGeneralService.delete(clienteGeneralId);
        }
    }

    public Cliente getClienteById(Long clienteId) {
        return clienteDAO.getClienteById(clienteId);
    }

    public List<UIEntity> getClientesByClienteGeneralId(Long connectedUserId, Long clienteGeneralId) {

        return clienteDAO.getClientesByClienteGeneralId(connectedUserId, clienteGeneralId).stream().map(c -> {
            UIEntity entity = UIEntity.toUI(c);
            entity.put("correoOficial", buscaCuenta.buscaCuentaUJI(c.getId()));
            return entity;
        }).collect(Collectors.toList());
    }

    public List<ClienteGrid> getClientesGridByClienteGeneralId(Long connectedUserId, Long clienteGeneralId) {
        return clienteDAO.getClientesGridByClienteGeneralId(connectedUserId, clienteGeneralId);
    }

    public Cliente getClienteByClienteGeneralId(Long clienteGeneralId, Boolean zonaPrivada) {
        return clienteDAO.getClienteByClienteGeneralId(clienteGeneralId, zonaPrivada);
    }

    public Cliente UIToModel(Long clienteId, String tipoUsuario, String nombre, String apellidos, String correo, Long correoEstadoId,
                             String prefijoTelefono, String movil, Long movilEstadoId, String paisPostalId, Long provinciaPostalId, Long poblacionPostalId, String postal,
                             String codigoPostal, Long postalEstadoId, Date fechaNacimiento, Long sexo, Long servicioId, Boolean fichaZonaPrivada, String nacionalidadId,
                             String provinciaPostalNombre, String poblacionPostalNombre, Boolean recibePostal, Boolean recibeCorreo) {

        Cliente cliente = new Cliente();

        cliente.setId(clienteId);
        if (ParamUtils.isNotNull(tipoUsuario)) {
            cliente.setTipo(tipoUsuario);
        } else {
            cliente.setTipo(TipoCliente.FISICA.getId());
        }
        cliente.setNombre(nombre);
        cliente.setApellidos(apellidos);
        cliente.setCorreo(correo);
        cliente.setFichaZonaPrivada(fichaZonaPrivada);
        cliente.setRecibeCorreo(recibeCorreo);
        cliente.setRecibePostal(recibePostal);

        Tipo correoEstado = new Tipo();
        if (ParamUtils.isNotNull(correoEstadoId)) {
            correoEstado.setId(correoEstadoId);
        } else {
            correoEstado.setId(1L);
        }
        cliente.setCorreoEstado(correoEstado);

        cliente.setPrefijoTelefono(prefijoTelefono);
        cliente.setMovil(movil);

        Tipo movilEstado = new Tipo();
        if (ParamUtils.isNotNull(movilEstadoId)) {
            movilEstado.setId(movilEstadoId);
        } else {
            movilEstado.setId(1L);
        }
        cliente.setMovilEstado(movilEstado);


        if (ParamUtils.isNotNull(paisPostalId)) {
            cliente.setPaisPostal(new Pais(paisPostalId));
        }

        if (ParamUtils.isNotNull(poblacionPostalId)) {
            cliente.setPoblacionPostal(new Poblacion(poblacionPostalId));
        }

        if (ParamUtils.isNotNull(provinciaPostalId)) {
            cliente.setProvinciaPostal(new Provincia(provinciaPostalId));
        }

        cliente.setPostal(postal);
        cliente.setCodigoPostal(codigoPostal);

        Tipo postalEstado = new Tipo();
        if (ParamUtils.isNotNull(postalEstadoId)) {
            postalEstado.setId(postalEstadoId);
        } else {
            postalEstado.setId(1L);
        }
        cliente.setPostalEstado(postalEstado);

        if (ParamUtils.isNotNull(sexo)) {
            cliente.setSexo(sexo);
        }

        cliente.setFechaNacimiento(fechaNacimiento);

        Servicio servicio = new Servicio();
        servicio.setId(servicioId);
        cliente.setServicio(servicio);

        if (ParamUtils.isNotNull(nacionalidadId)) {
            cliente.setNacionalidad(new Pais(nacionalidadId));
        }

        cliente.setProvinciaPostalNombre(provinciaPostalNombre);
        cliente.setPoblacionPostalNombre(poblacionPostalNombre);
        return cliente;
    }

    public Cliente getClienteByPersonaIdAndServicio(Long personaId, Long servicioId) {
        return clienteDAO.getClienteByPersonaIdAndServicio(personaId, servicioId);
    }

    @Transactional
    public Cliente addClienteProvisional(UIEntity datosCliente, Servicio servicio) {
        ClienteGeneral clienteGeneral = new ClienteGeneral();

        Tipo tipo = new Tipo();
        tipo.setId(ParamUtils.parseLong(datosCliente.get("tipoIdentificacion")));
        clienteGeneral.setTipoIdentificacion(tipo);

        clienteGeneral.setIdentificacion(datosCliente.get("identificacion"));
        clienteGeneral.setDefinitivo(Boolean.FALSE);
        clienteGeneral = clienteGeneralService.addClienteGeneral(clienteGeneral);


        Cliente cliente = UIToModel(datosCliente);
        cliente.setNombre(datosCliente.get("nombreOficial"));
        cliente.setApellidos(datosCliente.get("apellidosOficial"));
        cliente.setClienteGeneral(clienteGeneral);
        cliente.setFichaZonaPrivada(Boolean.FALSE);
        cliente.setRecibePostal(Boolean.TRUE);
        cliente.setRecibeCorreo(Boolean.TRUE);
        cliente.setServicio(servicio);
        clienteDAO.insert(cliente);
        return cliente;
    }

    public Cliente getClienteByCorreoAndServicio(String correo, Long servicioId) {
        return clienteDAO.getClienteByCorreoAndServicio(correo, servicioId);
    }

    public ClienteGrid getClienteParaFormularioByCorreoAndServicio(String correo, Long servicioId) {
        return clienteDAO.getClienteParaFormularioByCorreoAndServicio(correo, servicioId);
    }

    public List<ClienteGrid> getClientesParaFormularioByCorreoAndServicio(String correo, Long servicioId) {
        return clienteDAO.getClientesParaFormularioByCorreoAndServicio(correo, servicioId);
    }

    public List<ClienteGrid> getClientesGridByIdentificacion(Long connectedUserId, String identificacion) {
        return clienteDAO.getClientesGridByIdentificacion(connectedUserId, identificacion);
    }

    public ByteArrayOutputStream getExcelClientesGrid(Long connectedUserId, FiltroCliente filtro) {
        try {
            List<ClienteGrid> clientes = clienteDAO.getClienteByBusqueda(connectedUserId, filtro, null);
            if (ParamUtils.isNotNull(clientes)) {
                WriterExcelService writerExcelService = new WriterExcelService();
                cabeceraClientes(writerExcelService);
                cuerpoClientes(writerExcelService, clientes);
                writerExcelService.autoSizeColumns(5);
                return writerExcelService.getExcel();
            } else {
                return null;
            }
        } catch (ParseException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void cuerpoClientes(WriterExcelService writerExcelService, List<ClienteGrid> clientes) {
        Integer rownum = 2;

        for (ClienteGrid clienteGrid : clientes) {
            int cellnum = 0;
            Row row = writerExcelService.getNewRow(++rownum);
            writerExcelService.addCell(cellnum++, clienteGrid.getIdentificacion(), null, row);
            writerExcelService.addCell(cellnum++, clienteGrid.getNombreCompleto(), null, row);
            writerExcelService.addCell(cellnum++, String.valueOf(servicioService.getServicioById(clienteGrid.getServicio()).getNombre()), null, row);
            writerExcelService.addCell(cellnum++, clienteGrid.getTipo().equals("F") ? "Física" : "Empresa", null, row);
            writerExcelService.addCell(cellnum++, clienteGrid.getCorreo(), null, row);
            writerExcelService.addCell(cellnum++, clienteGrid.getMovil(), null, row);
        }
    }

    private void cabeceraClientes(WriterExcelService writerExcelService) {
        Integer rownum = 0;
        writerExcelService.addFulla("Resultados búsqueda");

        Row resumen = writerExcelService.getNewRow(rownum);
        writerExcelService.addCell(0, "Dades de clients filtrats", writerExcelService.getEstilNegreta(), resumen);
        writerExcelService.addMergedRegion(rownum, rownum, 0, 6);

        String[] columnas = new String[]{"Identificació", "Nom", "Servei", "Tipus", "Correu", "Mòbil"};
        writerExcelService.generaCeldes(writerExcelService.getEstilNegreta(), 2, columnas);

    }

    public Cliente getClienteByIdentificacionAndServicio(String identificacion) {
        return clienteDAO.getClienteByIdentificacionAndServicio(identificacion, 366L);
    }

    public Cliente getClienteByCorreoAndIdentificacionAndServicio(String correo, String identificacion, Long servicioId) {
        return clienteDAO.getClienteByCorreoAndIdentificacionAndServicio(correo, identificacion, servicioId);
    }
}