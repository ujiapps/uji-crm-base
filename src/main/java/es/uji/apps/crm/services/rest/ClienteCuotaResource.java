package es.uji.apps.crm.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteCuota;
import es.uji.apps.crm.services.ClienteCuotaService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("clientecuota")
public class ClienteCuotaResource extends CoreBaseService {

    @InjectParam
    private ClienteCuotaService clienteCuotaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClienteCuotasByClienteTarifaId(@QueryParam("clienteTarifa") Long clienteTarifa) {
        return UIEntity.toUI(clienteCuotaService.getClienteCuotasByClienteTarifaId(clienteTarifa));
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateClienteCuota(@PathParam("id") Long clienteCuotaId, UIEntity entity) throws ParseException {

        ClienteCuota clienteCuota = UIToModel(entity);
        return UIEntity.toUI(clienteCuotaService.updateClienteCuota(clienteCuota));
    }

    private ClienteCuota UIToModel(UIEntity entity) throws ParseException {
        ClienteCuota clienteCuota = entity.toModel(ClienteCuota.class);

        Cliente cliente = new Cliente();
        cliente.setId(Long.parseLong(entity.get("clienteId")));
        clienteCuota.setCliente(cliente);

        clienteCuota.setFechaInicio(UtilsService.fechaParse(entity.get("fechaInicio").substring(0, 10)));
        clienteCuota.setFechaFin(UtilsService.fechaParse(entity.get("fechaFin").substring(0, 10)));

        return clienteCuota;
    }

//    private UIEntity modelToUI(ClienteCuota clienteCuota) {
//        return UIEntity.toUI(clienteCuota);
//    }
//
//    private List<UIEntity> modelToUI(List<ClienteCuota> clienteCuotas) {
//        List<UIEntity> clienteCuotasUI = new ArrayList<>();
//        for (ClienteCuota clienteCuota : clienteCuotas) {
//            clienteCuotasUI.add(modelToUI(clienteCuota));
//        }
//        return clienteCuotasUI;
//    }
}