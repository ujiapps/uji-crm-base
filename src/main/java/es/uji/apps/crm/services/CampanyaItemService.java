package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaItemDAO;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaItem;
import es.uji.apps.crm.model.Item;

@Service
public class CampanyaItemService {
    private CampanyaItemDAO campanyaItemDAO;

    @Autowired
    public CampanyaItemService(CampanyaItemDAO campanyaItemDAO) {
        this.campanyaItemDAO = campanyaItemDAO;
    }

    public List<CampanyaItem> getCampanyaItemsByCampanyaId(Long campanyaId) {
        return campanyaItemDAO.getCampanyaItemsByCampanyaId(campanyaId);
    }

    public CampanyaItem getCampanyaItemById(Long campanyaItemId) {
        return campanyaItemDAO.getCampanyaItemById(campanyaItemId);
    }

    public void updateCampanyaItem(CampanyaItem campanyaItem) {
        campanyaItemDAO.update(campanyaItem);
    }

    public void deleteCampanyaItem(Long campanyaItemId) {
        campanyaItemDAO.delete(CampanyaItem.class, campanyaItemId);
    }

    public CampanyaItem addCampanyaItemArray(Long campanyaId, Long itemId) {
        try
        {
            CampanyaItem campanyaItem = new CampanyaItem();

            Campanya campanya = new Campanya();
            campanya.setId(campanyaId);

            Item item = new Item();
            item.setId(itemId);

            campanyaItem.setCampanya(campanya);
            campanyaItem.setItem(item);
            return addCampanyaItem(campanyaItem);

        }
        catch (Exception e)
        {
        }

        return null;
    }

    public CampanyaItem addCampanyaItem(CampanyaItem campanyaItem) {
        campanyaItemDAO.insert(campanyaItem);
        campanyaItem.setId(campanyaItem.getId());
        return campanyaItem;
    }
}