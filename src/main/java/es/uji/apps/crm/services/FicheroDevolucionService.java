package es.uji.apps.crm.services;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.FicheroDevolucionDAO;
import es.uji.apps.crm.dao.FicheroDevolucionPl;
import es.uji.apps.crm.model.FicheroDevolucion;

@Service
public class FicheroDevolucionService {
    @Autowired
    private FicheroDevolucionDAO ficheroDevolucionDAO;


    public List<FicheroDevolucion> getFicherosDevolucion() {
        return ficheroDevolucionDAO.getFicherosDevolucion();
    }

    public void addFicheroDevolucion(FicheroDevolucion ficheroDevolucion) {

        FicheroDevolucionPl fichero = new FicheroDevolucionPl();
        fichero.init();
        fichero.subirFicheroDevolucion(ficheroDevolucion.getNombreFichero(), ficheroDevolucion.getFichero());

    }
}