package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ProgramaCursoDAO;
import es.uji.apps.crm.model.ProgramaCurso;

@Service
public class ProgramaCursoService {
    private ProgramaCursoDAO programaCursoDAO;

    @Autowired
    public ProgramaCursoService(ProgramaCursoDAO programaCursoDAO) {
        this.programaCursoDAO = programaCursoDAO;
    }

    public List<ProgramaCurso> getProgramaCursosByProgramaId(Long programaId) {

        return programaCursoDAO.getProgramaCursosByProgramaId(programaId);
    }

    public void deleteProgramaCurso(Long programaCursoId) {
        programaCursoDAO.delete(ProgramaCurso.class, programaCursoId);
    }

    public ProgramaCurso addProgramaCurso(ProgramaCurso programaCurso) {
        programaCursoDAO.insert(programaCurso);
        programaCurso.setId(programaCurso.getId());
        return programaCurso;

    }

    public void updateProgramaCurso(ProgramaCurso programaCurso) {
        programaCursoDAO.update(programaCurso);
    }

    // public List<ProgramaPerfil> getProgramaPerfilesByProgramaId(Long programaId)
    // {
    // return programaPerfilDAO.getProgramaPerfilesByProgramaId(programaId);
    // }
    //
    // public ProgramaPerfil getProgramaPerfilById(Long programaPerfilId)
    // {
    // return programaPerfilDAO.getProgramaPerfilById(programaPerfilId);
    // }
    //
    // public ProgramaPerfil addProgramaPerfil(ProgramaPerfil programaPerfil)
    // {
    // programaPerfilDAO.insert(programaPerfil);
    // programaPerfil.setId(programaPerfil.getId());
    // return programaPerfil;
    // }
    //
    // public void updateProgramaPerfil(ProgramaPerfil programaPerfil)
    // {
    // programaPerfilDAO.update(programaPerfil);
    // }
    //
    // public void deleteProgramaPerfil(Long programaPerfilId)
    // {
    // programaPerfilDAO.delete(ProgramaPerfil.class, programaPerfilId);
    // }

}