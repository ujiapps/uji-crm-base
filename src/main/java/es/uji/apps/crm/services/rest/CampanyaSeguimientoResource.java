package es.uji.apps.crm.services.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.CampanyaFase;
import es.uji.apps.crm.model.CampanyaSeguimiento;
import es.uji.apps.crm.model.SeguimientoTipo;
import es.uji.apps.crm.services.CampanyaSeguimientoService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("campanyaseguimiento")
public class CampanyaSeguimientoResource extends CoreBaseService {
    @InjectParam
    private CampanyaSeguimientoService campanyaSeguimientoService;
    @InjectParam
    private UtilsService utilsService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyaSeguimientos(@QueryParam("campanyaFaseId") Long campanyaFaseId) {
        List<CampanyaSeguimiento> campanyaSeguimientos = campanyaSeguimientoService
                .getCampanyaSeguimientosByCampanyaFaseId(campanyaFaseId);
        return modelToUI(campanyaSeguimientos);
    }

    private List<UIEntity> modelToUI(List<CampanyaSeguimiento> campanyaSeguimientos) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (CampanyaSeguimiento campanyaSeguimiento : campanyaSeguimientos)
        {
            listaUI.add(modelToUI(campanyaSeguimiento));
        }
        return listaUI;
    }

    private UIEntity modelToUI(CampanyaSeguimiento campanyaSeguimiento) {
        UIEntity entity = UIEntity.toUI(campanyaSeguimiento);
        entity.put("seguimientoTipoNombre", campanyaSeguimiento.getSeguimientoTipo().getNombre());
        return entity;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getCampanyaSeguimientoById(@PathParam("id") Long campanyaSeguimientoId) {
        return modelToUI(campanyaSeguimientoService
                .getCampanyaSeguimientoById(campanyaSeguimientoId));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanyaSeguimiento(UIEntity entity) throws ParseException {
        CampanyaSeguimiento campanyaSeguimiento = UIToModel(entity);
        campanyaSeguimientoService.addCampanyaSeguimiento(campanyaSeguimiento);
        campanyaSeguimiento = campanyaSeguimientoService
                .getCampanyaSeguimientoById(campanyaSeguimiento.getId());
        return modelToUI(campanyaSeguimiento);
    }

    private CampanyaSeguimiento UIToModel(UIEntity entity) throws ParseException {
        CampanyaSeguimiento campanyaSeguimiento = entity.toModel(CampanyaSeguimiento.class);

        campanyaSeguimiento.setFecha(utilsService.fechaParse(entity.get("fecha")));

        SeguimientoTipo seguimientoTipo = new SeguimientoTipo();
        seguimientoTipo.setId(Long.parseLong(entity.get("seguimientoTipoId")));
        campanyaSeguimiento.setSeguimientoTipo(seguimientoTipo);

        CampanyaFase campanyaFase = new CampanyaFase();
        campanyaFase.setId(Long.parseLong(entity.get("campanyaFaseId")));
        campanyaSeguimiento.setCampanyaFase(campanyaFase);

        return campanyaSeguimiento;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateCampanyaSeguimiento(@PathParam("id") Long campanyaSeguimientoId,
                                              UIEntity entity) throws ParseException {
        CampanyaSeguimiento campanyaSeguimiento = UIToModel(entity);
        campanyaSeguimiento.setId(campanyaSeguimientoId);
        campanyaSeguimientoService.updateCampanyaSeguimiento(campanyaSeguimiento);
        campanyaSeguimiento = campanyaSeguimientoService
                .getCampanyaSeguimientoById(campanyaSeguimientoId);
        return modelToUI(campanyaSeguimiento);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long campanyaSeguimientoId) {
        campanyaSeguimientoService.deleteCampanyaSeguimiento(campanyaSeguimientoId);
    }
}