package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.CampanyaCliente;
import es.uji.apps.crm.model.ClienteEstadoCampanyaMotivo;
import es.uji.apps.crm.model.TipoEstadoCampanyaMotivo;
import es.uji.apps.crm.services.CampanyaClienteService;
import es.uji.apps.crm.services.CampanyaService;
import es.uji.apps.crm.services.ClienteEstadoCampanyaMotivoService;
import es.uji.apps.crm.services.TipoEstadoCampanyaMotivoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Component
@Path("clienteestadocampanyamotivo")
public class ClienteEstadoCampanyaMotivoResource extends CoreBaseService {

    @InjectParam
    private ClienteEstadoCampanyaMotivoService clienteEstadoCampanyaMotivoService;
    @InjectParam
    private CampanyaClienteService campanyaClienteService;
    @InjectParam
    private TipoEstadoCampanyaMotivoService tipoEstadoCampanyaMotivoService;
    @InjectParam
    private CampanyaService campanyaService;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addClienteEstadoMotivo(UIEntity entity) {

        ClienteEstadoCampanyaMotivo clienteEstadoCampanyaMotivo = clienteEstadoCampanyaMotivoService.addClienteEstadoMotivo(UIToModel(entity), ParamUtils.parseLong(entity.get("campanyaId")));
        return modelToUI(clienteEstadoCampanyaMotivo);

    }

    private ClienteEstadoCampanyaMotivo UIToModel(UIEntity entity) {

        ClienteEstadoCampanyaMotivo clienteEstadoCampanyaMotivo = entity.toModel(ClienteEstadoCampanyaMotivo.class);

        TipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivo = tipoEstadoCampanyaMotivoService.getTipoEstadoCampanyaById(ParamUtils.parseLong(entity.get("campanyaEstadoMotivoId")));
        clienteEstadoCampanyaMotivo.setCampanyaEstadoMotivo(tipoEstadoCampanyaMotivo);

        CampanyaCliente campanyaCliente = campanyaClienteService.getCampanyaClienteByCampanyaAndClienteId(ParamUtils.parseLong(entity.get("campanyaId")),
                ParamUtils.parseLong(entity.get("clienteId")));

        if (ParamUtils.isNotNull(campanyaCliente))
        {
            clienteEstadoCampanyaMotivo.setCampanyaCliente(campanyaCliente);
            return clienteEstadoCampanyaMotivo;
        }
        return null;
    }

    private UIEntity modelToUI(ClienteEstadoCampanyaMotivo clienteEstadoCampanyaMotivo) {
        return UIEntity.toUI(clienteEstadoCampanyaMotivo);
    }

    private List<UIEntity> modelToUI(List<ClienteEstadoCampanyaMotivo> clienteEstadoCampanyaMotivos) {
        List<UIEntity> clienteTarifasUI = new ArrayList<>();
        for (ClienteEstadoCampanyaMotivo clienteEstadoCampanyaMotivo : clienteEstadoCampanyaMotivos)
        {
            clienteTarifasUI.add(modelToUI(clienteEstadoCampanyaMotivo));
        }
        return clienteTarifasUI;
    }
}