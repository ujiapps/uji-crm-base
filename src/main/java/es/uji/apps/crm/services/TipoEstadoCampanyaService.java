package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.TipoEstadoCampanyaDAO;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.apps.crm.model.domains.TipoAfiliacionUsuarioCampanya;

@Service
public class TipoEstadoCampanyaService {
    private TipoEstadoCampanyaDAO tipoEstadoCampanyaDAO;
    private TipoEstadoCampanyaMotivoService tipoEstadoCampanyaMotivoService;

    @Autowired
    public TipoEstadoCampanyaService(TipoEstadoCampanyaDAO tipoEstadoCampanyaDAO, TipoEstadoCampanyaMotivoService tipoEstadoCampanyaMotivoService) {
        this.tipoEstadoCampanyaDAO = tipoEstadoCampanyaDAO;
        this.tipoEstadoCampanyaMotivoService = tipoEstadoCampanyaMotivoService;

    }

    public List<TipoEstadoCampanya> getTiposEstadoCampanyaByCampanyaId(Long campanya) {
        return tipoEstadoCampanyaDAO.getTiposEstadoCampanyaByCampanyaId(campanya);
    }

    public TipoEstadoCampanya updateTipoEstadoCampanyaByCampanyaId(TipoEstadoCampanya tipoEstadoCampanya) {
        return tipoEstadoCampanyaDAO.update(tipoEstadoCampanya);
    }

    public void deleteTipoEstadoCampanya(Long tipoEstadoCampanyaId) {

        tipoEstadoCampanyaMotivoService.deleteTiposEstadoCampanyaMotivo(tipoEstadoCampanyaId);
        tipoEstadoCampanyaDAO.delete(TipoEstadoCampanya.class, tipoEstadoCampanyaId);
    }

    public TipoEstadoCampanya getTipoEstadoCampanyaById(Long tipoEstadoCampanyaId) {
        return tipoEstadoCampanyaDAO.getTipoEstadoCampanyaById(tipoEstadoCampanyaId);
    }

    public TipoEstadoCampanya getTipoEstadoCampanyaByNombre(Long campanyaId, String tipoEstadoNombre) {
        return tipoEstadoCampanyaDAO.getTipoEstadoCampanyaByNombre(campanyaId, tipoEstadoNombre);
    }

    public TipoEstadoCampanya getTipoEstadoCampanyaByAccion(Long campanya, String accion) {
        return tipoEstadoCampanyaDAO.getTipoEstadoCampanyaByAccion(campanya, accion);
    }

    public void addTiposEstadoCampanyaGenericaActo(Campanya campanya) {

        TipoEstadoCampanya tipoEstadoCampanyaPropuesto = new TipoEstadoCampanya();
        tipoEstadoCampanyaPropuesto.setCampanya(campanya);
        tipoEstadoCampanyaPropuesto.setAccion(TipoAfiliacionUsuarioCampanya.PROPUESTO.getTipo());
        tipoEstadoCampanyaPropuesto.setNombre("Proposat");
        addTipoEstadoCampanyaByCampanyaId(tipoEstadoCampanyaPropuesto);

        TipoEstadoCampanya tipoEstadoCampanyaDefinitivo = new TipoEstadoCampanya();
        tipoEstadoCampanyaDefinitivo.setCampanya(campanya);
        tipoEstadoCampanyaDefinitivo.setAccion(TipoAfiliacionUsuarioCampanya.DEFINITIVO.getTipo());
        tipoEstadoCampanyaDefinitivo.setNombre("Definitiu");
        addTipoEstadoCampanyaByCampanyaId(tipoEstadoCampanyaDefinitivo);

        TipoEstadoCampanya tipoEstadoCampanyaBaja = new TipoEstadoCampanya();
        tipoEstadoCampanyaBaja.setCampanya(campanya);
        tipoEstadoCampanyaBaja.setAccion(TipoAfiliacionUsuarioCampanya.BAJAWEB.getTipo());
        tipoEstadoCampanyaBaja.setNombre("Baixa");
        addTipoEstadoCampanyaByCampanyaId(tipoEstadoCampanyaBaja);

        tipoEstadoCampanyaMotivoService.addTiposEstadoCampanyaMotivoGenericaActo(tipoEstadoCampanyaBaja);

    }

    public TipoEstadoCampanya addTipoEstadoCampanyaByCampanyaId(TipoEstadoCampanya tipoEstadoCampanya) {
        return tipoEstadoCampanyaDAO.insert(tipoEstadoCampanya);
    }

    public void addTiposEstadoCampanyaGenericaActoConPago(Campanya campanya) {

        TipoEstadoCampanya tipoEstadoCampanyaPropuesto = new TipoEstadoCampanya(campanya, TipoAfiliacionUsuarioCampanya.PROPUESTO.getTipo(), "Proposat");
//        tipoEstadoCampanyaPropuesto.setCampanya(campanya);
//        tipoEstadoCampanyaPropuesto.setAccion(TipoAfiliacionUsuarioCampanya.PROPUESTO.getTipo());
//        tipoEstadoCampanyaPropuesto.setNombre("Proposat");
        addTipoEstadoCampanyaByCampanyaId(tipoEstadoCampanyaPropuesto);

        TipoEstadoCampanya tipoEstadoCampanyaDefinitivo = new TipoEstadoCampanya(campanya, TipoAfiliacionUsuarioCampanya.DEFINITIVO.getTipo(), "Definitiu");
//        tipoEstadoCampanyaDefinitivo.setCampanya(campanya);
//        tipoEstadoCampanyaDefinitivo.setAccion(TipoAfiliacionUsuarioCampanya.DEFINITIVO.getTipo());
//        tipoEstadoCampanyaDefinitivo.setNombre("Definitiu");
        addTipoEstadoCampanyaByCampanyaId(tipoEstadoCampanyaDefinitivo);

        TipoEstadoCampanya tipoEstadoCampanyaBaja = new TipoEstadoCampanya(campanya, TipoAfiliacionUsuarioCampanya.BAJAWEB.getTipo(), "Baixa");
//        tipoEstadoCampanyaBaja.setCampanya(campanya);
//        tipoEstadoCampanyaBaja.setAccion(TipoAfiliacionUsuarioCampanya.BAJAWEB.getTipo());
//        tipoEstadoCampanyaBaja.setNombre("Baixa");
        addTipoEstadoCampanyaByCampanyaId(tipoEstadoCampanyaBaja);
        tipoEstadoCampanyaMotivoService.addTiposEstadoCampanyaMotivoGenericaActo(tipoEstadoCampanyaBaja);

        TipoEstadoCampanya tipoEstadoCampanyaPendiente = new TipoEstadoCampanya(campanya, TipoAfiliacionUsuarioCampanya.PENDIENTE.getTipo(), "Pendent");
//        tipoEstadoCampanyaPendiente.setCampanya(campanya);
//        tipoEstadoCampanyaPendiente.setNombre("Pendent");
        addTipoEstadoCampanyaByCampanyaId(tipoEstadoCampanyaPendiente);
    }

}