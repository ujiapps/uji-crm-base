package es.uji.apps.crm.services;

import java.util.List;

import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.EnvioPlantillaDAO;
import es.uji.apps.crm.model.EnvioPlantilla;
import es.uji.apps.crm.model.Paginacion;

@Service
public class EnvioPlantillaService {

    private EnvioPlantillaDAO envioPlantillaDAO;

    @Autowired
    public EnvioPlantillaService(EnvioPlantillaDAO envioPlantillaDAO) {
        this.envioPlantillaDAO = envioPlantillaDAO;
    }


    public List<EnvioPlantilla> getEnvioPlantillas(Paginacion paginacion) {
        return envioPlantillaDAO.getEnvioPlantillas(paginacion);
    }

    public EnvioPlantilla addEnvioPlantilla(String nombre, String plantilla) {
        EnvioPlantilla envioPlantilla = new EnvioPlantilla();
        envioPlantilla.setNombre(nombre);
        envioPlantilla.setPlantilla(plantilla);
        envioPlantilla.setGuardadoAutomatico(0L);
        return envioPlantillaDAO.insert(envioPlantilla);
    }

    public EnvioPlantilla updateEnvioPlantilla(Long envioPlantillaId, String nombre, String plantilla) {
        EnvioPlantilla envioPlantilla = getEnvioPlantillaById(envioPlantillaId);
        envioPlantilla.setNombre(nombre);
        envioPlantilla.setPlantilla(plantilla);
        envioPlantilla.setGuardadoAutomatico(0L);
        return envioPlantillaDAO.update(envioPlantilla);

    }

    public EnvioPlantilla autoGuardadoEnvioPlantilla(String plantilla, String nombre) {
        EnvioPlantilla envioPlantilla = new EnvioPlantilla();
        envioPlantilla.setPlantilla(plantilla);
        envioPlantilla.setNombre((ParamUtils.isNotNull(nombre)) ? nombre : "Autoguardado");
        envioPlantilla.setGuardadoAutomatico(1L);
        return envioPlantillaDAO.insert(envioPlantilla);
    }

    public EnvioPlantilla autoGuardadoEnvioPlantilla(Long envioPlantillaId, String plantilla, String nombre) {
        EnvioPlantilla envioPlantilla = getEnvioPlantillaById(envioPlantillaId);
        envioPlantilla.setPlantilla(plantilla);
        envioPlantilla.setNombre(nombre);
        return envioPlantillaDAO.update(envioPlantilla);

    }

    private EnvioPlantilla getEnvioPlantillaById(Long envioPlantillaId) {
        return envioPlantillaDAO.getEnvioPlantillaById(envioPlantillaId);
    }

    public void deteleEnvioPlantilla(Long envioPlantillaId) {
        envioPlantillaDAO.delete(EnvioPlantilla.class, envioPlantillaId);
    }
}