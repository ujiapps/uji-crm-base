package es.uji.apps.crm.services.rest;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Persona;
import es.uji.apps.crm.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("persona")
public class PersonaResource extends CoreBaseService {
    @InjectParam
    private PersonaService consultaPersona;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPersona() {
        return UIEntity.toUI(consultaPersona.getPersonas());
    }

    @GET
    @Path("identificacion/{identificacion}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getPersonabyIdentificacion(@PathParam("identificacion") String identificacion) {
        Persona persona = consultaPersona.getPersonaByIdentificacion(identificacion);
        ResponseMessage responseMessage = new ResponseMessage();

        if (!ParamUtils.isNotNull(persona))
        {
            responseMessage.setSuccess(false);
            responseMessage.setMessage("No es troba la persona especificada");
            return responseMessage;
        }

        responseMessage.setData(UIEntity.toUI(Collections.singletonList(persona)));
        responseMessage.setSuccess(true);
        return responseMessage;
    }

}