package es.uji.apps.crm.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ClienteGeneralDAO;
import es.uji.apps.crm.model.ClienteGeneral;
import es.uji.apps.crm.model.Persona;
import es.uji.apps.crm.model.Tipo;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Service
public class ClienteGeneralService {

    private ClienteGeneralDAO clienteGeneralDAO;
    private ClienteService clienteService;

    @Autowired
    public ClienteGeneralService(ClienteGeneralDAO clienteGeneralDAO, ClienteService clienteService) {
        this.clienteGeneralDAO = clienteGeneralDAO;
        this.clienteService = clienteService;
    }

    public ClienteGeneral update(ClienteGeneral clienteGeneral) {
        clienteGeneralDAO.update(clienteGeneral);
        return clienteGeneral;
    }

    public ClienteGeneral UIToModel(UIEntity entity) {

        ClienteGeneral clienteGeneral = new ClienteGeneral();

        if (ParamUtils.isNotNull(entity.get("id"))) {
            clienteGeneral = getClienteGeneralById(ParamUtils.parseLong(entity.get("id")));
            clienteGeneral.setIdentificacion(entity.get("identificacion"));
        } else {
            clienteGeneral = entity.toModel(ClienteGeneral.class);
        }

        String personaId = entity.get("personaId");
        if ((personaId != null) && (!personaId.isEmpty())) {
            Persona persona = new Persona();
            persona.setId(Long.parseLong(personaId));
            clienteGeneral.setPersona(persona);
        }

        String tipoIdentificacionId = entity.get("tipoIdentificacion");
        if (ParamUtils.isNotNull(tipoIdentificacionId)) {
            Tipo tipoIdentificacion = new Tipo();
            tipoIdentificacion.setId(Long.parseLong(tipoIdentificacionId));
            clienteGeneral.setTipoIdentificacion(tipoIdentificacion);
        }

        return clienteGeneral;
    }

    public ClienteGeneral getClienteGeneralById(Long clienteGeneral) {
        return clienteGeneralDAO.getClienteGeneralById(clienteGeneral);
    }

    public ClienteGeneral addClienteGeneral(ClienteGeneral clienteGeneral) {

        ClienteGeneral clienteGeneralAux = new ClienteGeneral();
        if (ParamUtils.isNotNull(clienteGeneral.getPersona())) {
            clienteGeneralAux = getClienteGeneralByIdentificacionAndPersona(clienteGeneral.getIdentificacion(), clienteGeneral.getPersona().getId());

            if (ParamUtils.isNotNull(clienteGeneralAux)) {
                clienteGeneral = clienteGeneralAux;
            } else {
                if (!ParamUtils.isNotNull(clienteGeneral.getTipoIdentificacion())) {
                    Tipo tipoIdentificacion = new Tipo();
                    tipoIdentificacion.setId(74L);
                    clienteGeneral.setTipoIdentificacion(tipoIdentificacion);
                }

                clienteGeneralDAO.insert(clienteGeneral);
            }
        } else {
            clienteGeneralDAO.insert(clienteGeneral);
        }
        return clienteGeneral;
    }

    private ClienteGeneral getClienteGeneralByIdentificacionAndPersona(String identificacion, Long personaId) {
        return clienteGeneralDAO.getClienteGeneralByIdentificacionAndPersona(identificacion, personaId);
    }

//    public void delete(Long connectedUserId, Long clienteGeneralId) {
//
//        clienteService.deleteClientes(connectedUserId, clienteGeneralId);
//
//        clienteGeneralDAO.delete(ClienteGeneral.class, clienteGeneralId);
//    }

    public void delete(Long clienteGeneralId) {
        clienteGeneralDAO.delete(ClienteGeneral.class, clienteGeneralId);
    }

    public ClienteGeneral getClienteGeneralByPerId(Long connectedUserId) {
        return clienteGeneralDAO.getClienteGeneralByPerId(connectedUserId);

    }

    public ClienteGeneral getClienteGeneralByIdentificacion(String identificacion) {
        return clienteGeneralDAO.getClienteGeneralByIdentificacion(identificacion);
    }

    public ClienteGeneral getClienteGeneralByIdentificacionSinJoinPersona(String identificacion) {
        return clienteGeneralDAO.getClienteGeneralByIdentificacionSinJoinPersona(identificacion);
    }

    public ClienteGeneral UIToModel(Long clienteGeneralId, Long tipoIdentificacionId, String identificacion, Long personaId) {

        ClienteGeneral clienteGeneral = new ClienteGeneral();
        if (ParamUtils.isNotNull(clienteGeneralId))
            clienteGeneral = getClienteGeneralById(clienteGeneralId);

        if (ParamUtils.isNotNull(personaId)) {
            Persona persona = new Persona();
            persona.setId(personaId);
            clienteGeneral.setPersona(persona);
        }

        if (ParamUtils.isNotNull(tipoIdentificacionId)) {
            Tipo tipoIdentificacion = new Tipo();
            tipoIdentificacion.setId(tipoIdentificacionId);
            clienteGeneral.setTipoIdentificacion(tipoIdentificacion);
        }

        clienteGeneral.setIdentificacion(identificacion);

        return clienteGeneral;

    }
}