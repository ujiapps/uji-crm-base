package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CorreoNoValidoControlDAO;
import es.uji.apps.crm.dao.CorreoNoValidoDAO;
import es.uji.apps.crm.model.CorreoNoValidoControl;
import es.uji.apps.crm.model.FiltroCorreoNoValido;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.model.VwCorreoNoValido;

@Service
public class CorreoNoValidoService {

    private CorreoNoValidoDAO correoNoValidoDAO;
    private CorreoNoValidoControlDAO correoNoValidoControlDAO;

    @Autowired
    public CorreoNoValidoService(CorreoNoValidoDAO correoNoValidoDAO, CorreoNoValidoControlDAO correoNoValidoControlDAO) {
        this.correoNoValidoDAO = correoNoValidoDAO;
        this.correoNoValidoControlDAO = correoNoValidoControlDAO;
    }

    public List<VwCorreoNoValido> getCorreosNoValidos(FiltroCorreoNoValido filtroCorreoNoValido, Paginacion paginacion, String sort, String dir) {
        return correoNoValidoDAO.getCorreosNoValidos(filtroCorreoNoValido, paginacion, sort, dir);
    }

    public VwCorreoNoValido getCorreoNoValido(VwCorreoNoValido correoNoValido) {
        return correoNoValidoDAO.getCorreoNoValido(correoNoValido);
    }

    public VwCorreoNoValido updateCorreoNoValido(VwCorreoNoValido correoNoValido, Boolean modificado) {

        CorreoNoValidoControl correoNoValidoControl = correoNoValido.getCorreoNoValidoControl();
        correoNoValidoControl.setModificado(modificado);
        correoNoValidoControlDAO.update(correoNoValidoControl);
        return correoNoValido;

    }

    public VwCorreoNoValido insertCorreoNoValido(VwCorreoNoValido correoNoValido, boolean modificado) {

        CorreoNoValidoControl correoNoValidoControl = new CorreoNoValidoControl();

        correoNoValidoControl.setCorreoNoValido(correoNoValido);
        correoNoValidoControl.setModificado(modificado);
        correoNoValidoControlDAO.insert(correoNoValidoControl);

        return correoNoValido;

    }
}