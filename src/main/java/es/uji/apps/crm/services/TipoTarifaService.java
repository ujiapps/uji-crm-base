package es.uji.apps.crm.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.TipoTarifaDAO;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.Tarifa;
import es.uji.apps.crm.model.TipoTarifa;

@Service
public class TipoTarifaService {
    private TipoTarifaDAO tipoTarifaDAO;
    //    private CampanyaService campanyaService;
    private TarifaService tarifaService;
    private LineaFacturacionService lineaFacturacionService;

    @Autowired
    public TipoTarifaService(TipoTarifaDAO tipoTarifaDAO, TarifaService tarifaService, LineaFacturacionService lineaFacturacionService) {
        this.tipoTarifaDAO = tipoTarifaDAO;
//        this.campanyaService = campanyaService;
        this.tarifaService = tarifaService;
        this.lineaFacturacionService = lineaFacturacionService;
    }


    public List<TipoTarifa> getTiposTarifa(Long connectedUserId) {
//        List<Campanya> campanyas = campanyaService.getCampanyasAdmin(connectedUserId);
//        List<Long> campanyasIds = new ArrayList<Long>();
//        for (Campanya campanya : campanyas)
//        {
//            campanyasIds.add(campanya.getId());
//        }
        return tipoTarifaDAO.getTiposTarifa(connectedUserId);
    }

    public List<TipoTarifa> getTiposTarifaByCampanyaId(Long campanyaId) {
        return tipoTarifaDAO.getTiposTarifaByCampanyaId(campanyaId);
    }

    public TipoTarifa getTipoTarifaById(Long tipoTarifaId) {
        return tipoTarifaDAO.getTipoTarifaById(tipoTarifaId);
    }

    public TipoTarifa addTipoTarifa(TipoTarifa tipoTarifa) {
        tipoTarifaDAO.insert(tipoTarifa);
        tipoTarifa.setId(tipoTarifa.getId());
        return tipoTarifa;
    }

    public void deleteTipoTarifa(Long tipoTarifaId) {
        tipoTarifaDAO.delete(TipoTarifa.class, tipoTarifaId);
    }

    public TipoTarifa updateTipoTarifa(TipoTarifa tipoTarifa) {
        tipoTarifaDAO.update(tipoTarifa);
        return tipoTarifa;
    }

    public void addTipoTarifaGenericaActoConPago(Campanya campanya) {

        TipoTarifa tipoTarifa = new TipoTarifa();
        tipoTarifa.setCampanya(campanya);
        tipoTarifa.setDefecto(Boolean.TRUE);
        tipoTarifa.setMesesCaduca(0L);
        tipoTarifa.setMesesVigencia(0L);
        tipoTarifa.setNombre("MODIFICAR -- Nueva tarifa acto de graduacion");
//        LineaFacturacion lineaFacturacion = lineaFacturacionService.getLineaFacturacionActoByCampanya(campanya);
        tipoTarifa.setLineaFacturacion(campanya.getLineaFacturacion());

        tipoTarifaDAO.insert(tipoTarifa);

        Tarifa tarifa = new Tarifa();
        tarifa.setFechaInicio(new Date());
        tarifa.setFechaFin(new Date());
        tarifa.setImporte(4L);
        tarifa.setTipoTarifa(tipoTarifa);

        tarifaService.addTarifa(tarifa);


    }
}