package es.uji.apps.crm.services;

import java.util.List;

import es.uji.apps.crm.dao.ClienteDatoOpcionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.model.ClienteDatoOpcion;

@Service
public class ClienteDatoOpcionService {
    private ClienteDatoOpcionDAO clienteDatoOpcionDAO;

    @Autowired
    public ClienteDatoOpcionService(ClienteDatoOpcionDAO clienteDatoOpcionDAO) {
        this.clienteDatoOpcionDAO = clienteDatoOpcionDAO;
    }

    public ClienteDatoOpcion getClienteDatoOpcionById(Long clienteDatoOpcionId) {
        return clienteDatoOpcionDAO.getClienteDatoOpcionById(clienteDatoOpcionId);
    }

    public List<ClienteDatoOpcion> getClienteDatoOpcionesByTipoId(Long clienteDatoTipoId) {
        return clienteDatoOpcionDAO.getClienteDatoOpcionesByTipoId(clienteDatoTipoId);
    }

    public ClienteDatoOpcion addClienteDatoOpcion(ClienteDatoOpcion clienteDatoOpcion) {
        clienteDatoOpcionDAO.insert(clienteDatoOpcion);
        return clienteDatoOpcion;
    }

    public void updateClienteDatoOpcion(ClienteDatoOpcion clienteDatoOpcion) {
        clienteDatoOpcionDAO.update(clienteDatoOpcion);
    }

    public void deleteClienteDatoOpcion(Long id) {
        clienteDatoOpcionDAO.delete(ClienteDatoOpcion.class, id);
    }
}