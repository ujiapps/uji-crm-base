package es.uji.apps.crm.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.TipoDAO;
import es.uji.apps.crm.model.ClienteDatoOpcion;
import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.model.TipoModel;
import es.uji.apps.crm.model.domains.TipoDato;

@Service
public class TipoService {
    private TipoDAO tipoDAO;

    @Autowired
    public TipoService(TipoDAO tipoDAO) {
        this.tipoDAO = tipoDAO;
    }

    public Tipo getTiposById(Long tipoId) {
        return tipoDAO.getTiposById(tipoId);
    }


    public List<ClienteDatoOpcion> getTiposOpciones(Long tipoId) {
        return tipoDAO.getTiposOpciones(tipoId);
    }

    public List<Tipo> getTiposTipoDato() {
        List<Tipo> tipos = new ArrayList<>();

        for (TipoDato tipoDato : TipoDato.values()) {
            Tipo tipo = new Tipo();
            tipo.setId(tipoDato.getId());
            tipo.setNombre(tipoDato.getDescripcion());
            tipos.add(tipo);
        }
        return tipos;
    }

    public TipoModel addTipo(TipoModel tipo) {
        tipoDAO.insert(tipo);
        tipo.setId(tipo.getId());
        return tipo;
    }

    public void updateTipo(TipoModel tipo) {
        tipoDAO.update(tipo);
    }

    public void deleteTipo(Long tipoVinculacionId) {
        tipoDAO.delete(TipoModel.class, tipoVinculacionId);
    }

    public List<Tipo> getTipos(String columna) {
        return tipoDAO.getTipos(columna);
    }
}