package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.model.domains.TipoColumna;
import es.uji.apps.crm.services.AlumniInicioService;
import es.uji.apps.crm.services.AlumniLoginService;
import es.uji.apps.crm.services.AlumniService;
import es.uji.apps.crm.services.TipoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;

public class AlumniLoginResource extends CoreBaseService {

    @InjectParam
    private AlumniService alumniService;
    @InjectParam
    private AlumniLoginService alumniLoginService;
    @InjectParam
    private AlumniInicioService alumniInicioService;
    @InjectParam
    private TipoService tipoService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template getPantallaLogin(@CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException {
        return alumniLoginService.getTemplateLogin(idioma);
    }

    @GET
    @Path("avisook")
    @Produces(MediaType.TEXT_HTML)
    public Template getAvisoOk(@CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException {
        return alumniService.getTemplateAviso(idioma, "aviso.contacto.envio.ok", "/crm/rest/alumni2024/login/", request.getPathInfo());
    }

    @GET
    @Path("avisoko")
    @Produces(MediaType.TEXT_HTML)
    public Template getAvisoKo(@CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException {
        return alumniService.getTemplateAviso(idioma, "aviso.contacto.envio.ko", "/crm/rest/alumni2024/login/", request.getPathInfo());
    }

    @GET
    @Path("hash/{p_hash}")
    @Produces(MediaType.TEXT_HTML)
    public Response getCookieParam(@PathParam("p_hash") String pHash) throws URISyntaxException {

        return alumniInicioService.activarSesionPorHash(pHash, "alumni2024", Boolean.FALSE);
    }

    @POST
    @Path("hash")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage addSessionUser(@FormParam("correo") String correo, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {

        //Dar de alta un hash en crm_session y Enviar correo hash
        return alumniInicioService.anyadirSesionUsuario(correo, idioma, request);

    }

    @GET
    @Path("formulario")
    @Produces(MediaType.TEXT_HTML)
    public Template getFormulario(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {

        Template template = alumniService.getTemplateBaseAlumni(request.getPathInfo(), idioma, "formulario");
        List<Tipo> tiposIdentificacion = tipoService.getTipos(TipoColumna.IDENTIFICACION.getValue());
        template.put("tiposIdentificacion", tiposIdentificacion);
        return template;
    }
}