package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.EnvioCriterioCampanyaEstadosDAO;
import es.uji.apps.crm.model.EnvioCriterio;
import es.uji.apps.crm.model.EnvioCriterioCampanyaEstado;
import es.uji.apps.crm.model.TipoEstadoCampanya;

@Service
public class EnvioCriterioCampanyaEstadosService {

    private EnvioCriterioCampanyaEstadosDAO envioCriterioCampanyaEstadosDAO;

    @Autowired
    public EnvioCriterioCampanyaEstadosService(EnvioCriterioCampanyaEstadosDAO envioCriterioCampanyaEstadosDAO) {
        this.envioCriterioCampanyaEstadosDAO = envioCriterioCampanyaEstadosDAO;
    }

    public void insertEnvioCriterioCampanyaEstados(List<Long> arrayCriteriosCampanyaEstado, Long envioCriterioId) {

        for (Long campanyaEstadoId : arrayCriteriosCampanyaEstado)
        {
            insertEnvioCriterioCampanyaEstados(campanyaEstadoId, envioCriterioId);
        }

    }

    public void insertEnvioCriterioCampanyaEstados(Long campanyaEstadoId, Long envioCriterioId) {

        EnvioCriterioCampanyaEstado envioCriterioCampanyaEstado = new EnvioCriterioCampanyaEstado();

        TipoEstadoCampanya tipoEstadoCampanya = new TipoEstadoCampanya();
        tipoEstadoCampanya.setId(campanyaEstadoId);
        envioCriterioCampanyaEstado.setCampanyaEstado(tipoEstadoCampanya);

        EnvioCriterio envioCriterio = new EnvioCriterio();
        envioCriterio.setId(envioCriterioId);
        envioCriterioCampanyaEstado.setEnvioCriterio(envioCriterio);

        envioCriterioCampanyaEstadosDAO.insert(envioCriterioCampanyaEstado);


    }

    public void deleteEnvioCriterioCampanyaEstados(Long envioCriterioId) {
        envioCriterioCampanyaEstadosDAO.delete(EnvioCriterioCampanyaEstado.class, "envio_criterio_id = " + envioCriterioId);

    }

    public List<EnvioCriterioCampanyaEstado> getEnvioCriterioCampanyaEstadosByEnvioCriterioId(Long envioCriterioId) {
        return envioCriterioCampanyaEstadosDAO.get(EnvioCriterioCampanyaEstado.class, "envio_criterio_id = " + envioCriterioId);
    }
}