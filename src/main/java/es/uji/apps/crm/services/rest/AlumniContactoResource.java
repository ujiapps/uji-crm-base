package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.services.AlumniService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;

@Path("contacto")
public class AlumniContactoResource extends CoreBaseService {

    @InjectParam
    private AlumniService alumniService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template getFormularioContacto(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {

        return alumniService.getTemplateBaseAlumni(request.getPathInfo(), idioma, "contacto");
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    public Template enviarFormularioContacto(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                             @FormParam("tipoIdentificacion") Long tipoIdentificacionId,
                                             @FormParam("identificacion") String identificacion,
                                             @FormParam("nombre") String nombre,
                                             @FormParam("apellidos") String apellidos,
                                             @FormParam("correo") String correo,
                                             @FormParam("telefono") String telefono,
                                             @FormParam("categoria") Long categoria,
                                             @FormParam("comentario") String comentario) throws ParseException {
        Boolean contacto = alumniService.enviarFormularioContacto(tipoIdentificacionId, identificacion, nombre, apellidos, correo,
                telefono, categoria, comentario);
        if (contacto) {
            return alumniService.getTemplateAviso(idioma,  "aviso.formulario.contacto.ok", "/crm/rest/alumni2024/login/", request.getPathInfo());
        }
        return alumniService.getTemplateAviso(idioma,  "aviso.formulario.contacto.ok", "/crm/rest/alumni2024/login/", request.getPathInfo());
    }

}
