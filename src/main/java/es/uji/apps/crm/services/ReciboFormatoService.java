package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ReciboFormatoDAO;
import es.uji.apps.crm.model.ReciboFormato;

@Service
public class ReciboFormatoService {
    private ReciboFormatoDAO reciboFormatoDAO;

    @Autowired
    public ReciboFormatoService(ReciboFormatoDAO reciboFormatoDAO) {
        this.reciboFormatoDAO = reciboFormatoDAO;
    }

    public List<ReciboFormato> getReciboFormatos() {
        return reciboFormatoDAO.getReciboFormatos();
    }
}