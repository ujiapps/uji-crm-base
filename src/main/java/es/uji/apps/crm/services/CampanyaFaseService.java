package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaFaseDAO;
import es.uji.apps.crm.model.CampanyaFase;

@Service
public class CampanyaFaseService {
    private CampanyaFaseDAO campanyaFaseDAO;

    @Autowired
    public CampanyaFaseService(CampanyaFaseDAO campanyaFaseDAO) {
        this.campanyaFaseDAO = campanyaFaseDAO;
    }

    public CampanyaFase getCampanyaFasesById(Long campanyaFaseId) {
        return campanyaFaseDAO.getCampanyaFasesById(campanyaFaseId);
    }

    public List<CampanyaFase> getCampanyaFasesByCampanyaId(Long campanyaId) {
        return campanyaFaseDAO.getCampanyaFasesByCampanyaId(campanyaId);
    }

    public CampanyaFase addCampanyaFase(CampanyaFase campanyaFase) {
        campanyaFaseDAO.insert(campanyaFase);
        campanyaFase.setId(campanyaFase.getId());
        return campanyaFase;
    }

    public void updateCampanyaFase(CampanyaFase grupo) {
        campanyaFaseDAO.update(grupo);
    }

    public void delete(Long campanyaFaseId) {
        campanyaFaseDAO.delete(CampanyaFase.class, campanyaFaseId);
    }

}