package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.model.TipoModel;
import es.uji.apps.crm.model.domains.TipoColumna;
import es.uji.apps.crm.model.domains.TipoDato;
import es.uji.apps.crm.services.TipoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("tipo")
public class TipoResource extends CoreBaseService {
    @InjectParam
    private TipoService tipoService;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getTiposById(@PathParam("id") Long tipoId) {
        return UIEntity.toUI(tipoService.getTiposById(tipoId));
    }

    @GET
    @Path("estadosenvios")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposEstadosEnvios() {
        return UIEntity.toUI(tipoService.getTipos(TipoColumna.ESTADOENVIO.getValue()));
    }

    @GET
    @Path("acceso")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposAccesos() {
        return UIEntity.toUI(tipoService.getTipos(TipoColumna.ACCESO.getValue()));
    }

    @GET
    @Path("recepcioninfo")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposRecepcionInfo() {
        return UIEntity.toUI(tipoService.getTipos(TipoColumna.RECEPCIONINFO.getValue()));
    }

    @GET
    @Path("opciones/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposOpciones(@PathParam("id") Long tipoId,
                                           @QueryParam("tipoDatoNombre") String tipoDatoNombre) {
        if (tipoDatoNombre.equals(TipoDato.SELECCION.getNombre())) {
            return UIEntity.toUI(tipoService.getTiposOpciones(tipoId));
        }
        return null;
    }

    @GET
    @Path("campanyasclientes")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposCampanyasClientes() {
        return UIEntity.toUI(tipoService.getTipos(TipoColumna.CAMPANYACLIENTE.getValue()));
    }

    @GET
    @Path("movimientos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposMovimientos() {
        return UIEntity.toUI(tipoService.getTipos(TipoColumna.MOVIMIENTO.getValue()));
    }

    @GET
    @Path("comparaciones")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposComparaciones() {
        return UIEntity.toUI(tipoService.getTipos(TipoColumna.COMPARACION.getValue()));
    }

    @GET
    @Path("comparacionesenvios")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposComparacionesEnvios() {
        return UIEntity.toUI(tipoService.getTipos(TipoColumna.COMPARACIONENVIOS.getValue()));
    }

    @GET
    @Path("vinculaciones")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposVinculaciones() {
        return UIEntity.toUI(tipoService.getTipos(TipoColumna.VINCULACION.getValue()));
    }

    @POST
    @Path("vinculaciones")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addEtiqueta(UIEntity entity) {
        TipoModel tipo = entity.toModel(TipoModel.class);
        tipo.setTablaColumna("vinculaciones.tipoDato");
        tipoService.addTipo(tipo);
        return UIEntity.toUI(tipo);
    }

    @PUT
    @Path("vinculaciones/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateEtiqueta(@PathParam("id") Long tipoVinculacionId, UIEntity entity) {
        TipoModel tipo = entity.toModel(TipoModel.class);
        tipo.setId(tipoVinculacionId);
        tipo.setTablaColumna("vinculaciones.tipoDato");
        tipoService.updateTipo(tipo);
        return UIEntity.toUI(tipo);
    }

    @DELETE
    @Path("vinculaciones/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long tipoVinculacionId) {
        tipoService.deleteTipo(tipoVinculacionId);
    }

    @GET
    @Path("envio")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposEnvios() {
        return UIEntity.toUI(tipoService.getTipos(TipoColumna.ENVIO.getValue()));
    }

    @GET
    @Path("tipoenvioauto")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposEnvioCampanyaAuto() {
        return UIEntity.toUI(tipoService.getTipos(TipoColumna.ENVIOCAMPANYAAUTO.getValue()));
    }

    @GET
    @Path("tipodatos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposTipoDato() {
        return UIEntity.toUI(tipoService.getTiposTipoDato());
    }

    @GET
    @Path("tipoidentificacion")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposIdentificacion() {
        return UIEntity.toUI(tipoService.getTipos(TipoColumna.IDENTIFICACION.getValue()));
    }

    @GET
    @Path("estadocuota")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposEstadoCuota() {
        return UIEntity.toUI(tipoService.getTipos(TipoColumna.ESTADOCUOTA.getValue()));
    }
}