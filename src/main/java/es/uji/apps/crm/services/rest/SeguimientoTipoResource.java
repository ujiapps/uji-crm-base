package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.SeguimientoTipo;
import es.uji.apps.crm.services.SeguimientoTipoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("seguimientotipo")
public class SeguimientoTipoResource extends CoreBaseService {
    @InjectParam
    private SeguimientoTipoService seguimientoTipoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSeguimientoTipos() {
        List<SeguimientoTipo> lista = seguimientoTipoService.getSeguimientoTipos();
        return UIEntity.toUI(lista);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addSeguimientoTipos(UIEntity entity) {
        SeguimientoTipo seguimientoTipo = UIToModel(entity);

        seguimientoTipoService.addSeguimientoTipos(seguimientoTipo);
        return modelToUI(seguimientoTipo);
    }

    private SeguimientoTipo UIToModel(UIEntity entity) {
        SeguimientoTipo seguimientoTipo = entity.toModel(SeguimientoTipo.class);
        return seguimientoTipo;
    }

    private UIEntity modelToUI(SeguimientoTipo seguimientoTipo) {
        return UIEntity.toUI(seguimientoTipo);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateSeguimientoTipos(@PathParam("id") Long seguimientoTipoId, UIEntity entity) {
        SeguimientoTipo seguimientoTipo = UIToModel(entity);
        seguimientoTipo.setId(seguimientoTipoId);
        seguimientoTipoService.updateSeguimientoTipos(seguimientoTipo);

        return modelToUI(seguimientoTipo);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long seguimientoTipoId) {
        seguimientoTipoService.deleteSeguimientoTipo(seguimientoTipoId);
    }
}