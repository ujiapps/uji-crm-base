package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaTextoDAO;
import es.uji.apps.crm.model.CampanyaTexto;

@Service
public class CampanyaTextoService {
    private CampanyaTextoDAO campanyaTextoDAO;

    @Autowired
    public CampanyaTextoService(CampanyaTextoDAO campanyaTextoDAO) {
        this.campanyaTextoDAO = campanyaTextoDAO;
    }

    public List<CampanyaTexto> getCampanyasTextos() {
        return campanyaTextoDAO.getCampanyaTextos();
    }

    public CampanyaTexto addCampanyaTexto(CampanyaTexto campanyaTexto) {
        campanyaTextoDAO.insert(campanyaTexto);
        return campanyaTexto;
    }

    public void updateCampanyaTexto(CampanyaTexto campanyaTexto) {
        campanyaTextoDAO.update(campanyaTexto);
    }

    public void deleteCampanyaTexto(Long campanyaTextoId) {
        campanyaTextoDAO.delete(CampanyaTexto.class, campanyaTextoId);
    }

    public CampanyaTexto getCampanyaTextoByCampanyaCodigo(String codigo) {
        return campanyaTextoDAO.getCampanyaTextoByCampanyaCodigo(codigo);
    }
}