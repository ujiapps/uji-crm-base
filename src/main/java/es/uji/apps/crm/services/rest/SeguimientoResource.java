package es.uji.apps.crm.services.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.Seguimiento;
import es.uji.apps.crm.model.SeguimientoTipo;
import es.uji.apps.crm.services.SeguimientoService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("seguimiento")
public class SeguimientoResource extends CoreBaseService {
    @InjectParam
    private SeguimientoService seguimientoService;
    @InjectParam
    private UtilsService utilsService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSeguimientos(@QueryParam("clienteId") Long clienteId) {
        List<Seguimiento> lista = seguimientoService.getSeguimientosByClienteId(clienteId);
        return modelToUI(lista);
    }

    private List<UIEntity> modelToUI(List<Seguimiento> seguimientos) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (Seguimiento seguimiento : seguimientos)
        {
            listaUI.add(modelToUI(seguimiento));
        }
        return listaUI;
    }

    private UIEntity modelToUI(Seguimiento seguimiento) {
        UIEntity entity = UIEntity.toUI(seguimiento);
//        entity.put("seguimientoTipoNombre", seguimiento.getSeguimientoTipo().getNombre());
//        entity.put("campanyaNombre", "");

//        Campanya campanya = seguimiento.getCampanya();
//        if (campanya != null)
//        {
//            entity.put("campanyaNombre", seguimiento.getCampanya().getNombre());
//        }

        return entity;
    }

    @GET
    @Path("campanya")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSeguimientosByClienteIdAndCampanyaId(@QueryParam("clienteId") Long clienteId, @QueryParam("campanyaId") Long campanyaId) {
        List<Seguimiento> lista = seguimientoService.getSeguimientosByClienteIdAndCampanyaId(clienteId, campanyaId);
        return modelToUI(lista);
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getSeguimientoById(@PathParam("id") Long seguimientoId) {
        return modelToUI(seguimientoService.getSeguimientoById(seguimientoId));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addSeguimiento(UIEntity entity) throws ParseException {
        Seguimiento seguimiento = UIToModel(entity);
//        seguimiento = seguimientoService.addSeguimiento(seguimiento);
//        seguimiento = seguimientoService.getSeguimientoById(seguimiento.getId());
        return modelToUI(seguimientoService.addSeguimiento(seguimiento));
    }

    private Seguimiento UIToModel(UIEntity entity) throws ParseException {
        Seguimiento seguimiento = entity.toModel(Seguimiento.class);
        seguimiento.setFecha(utilsService.fechaParse(entity.get("fecha")));

        SeguimientoTipo seguimientoTipo = new SeguimientoTipo();
        seguimientoTipo.setId(Long.parseLong(entity.get("seguimientoTipoId")));
        seguimiento.setSeguimientoTipo(seguimientoTipo);

        Cliente cliente = new Cliente();
        cliente.setId(Long.parseLong(entity.get("clienteId")));
        seguimiento.setCliente(cliente);

        String campanyaId = entity.get("campanyaId");
        if (campanyaId != null)
        {
            Campanya campanya = new Campanya();
            campanya.setId(Long.parseLong(campanyaId));
            seguimiento.setCampanya(campanya);
        }
        return seguimiento;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateSeguimiento(@PathParam("id") Long seguimientoId, UIEntity entity)
            throws ParseException {
        Seguimiento seguimiento = UIToModel(entity);
//        seguimiento.setId(seguimientoId);
//        seguimientoService.updateSeguimiento(seguimiento);
//        seguimiento = seguimientoService.getSeguimientoById(seguimientoId);
        return modelToUI(seguimientoService.updateSeguimiento(seguimiento));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long seguimientoId) {
        seguimientoService.deleteSeguimiento(seguimientoId);
    }
}