package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.UbicacionDAO;
import es.uji.apps.crm.model.Ubicacion;

@Service
public class UbicacionService {
    @Autowired
    private UbicacionDAO ubicacionDAO;


    public List<Ubicacion> getUbicaciones() {
        return ubicacionDAO.getUbicaciones();
    }

    public Ubicacion getUbicacion(Long uestId) {
        return ubicacionDAO.getUbicacion(uestId);
    }
}