package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteEtiqueta;
import es.uji.apps.crm.model.Etiqueta;
import es.uji.apps.crm.services.ClienteEtiquetaService;
import es.uji.apps.crm.services.EtiquetaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("clienteetiqueta")
public class ClienteEtiquetaResource extends CoreBaseService {
    @InjectParam
    private ClienteEtiquetaService clienteEtiquetaService;
    @InjectParam
    private EtiquetaService etiquetaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClienteEtiquetasByClienteId(@QueryParam("clienteId") Long clienteId) {
        List<ClienteEtiqueta> clienteEtiquetas = clienteEtiquetaService
                .getClienteEtiquetasbyClienteId(clienteId);
        return modelToUI(clienteEtiquetas);
    }

    private List<UIEntity> modelToUI(List<ClienteEtiqueta> clienteEtiquetas) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (ClienteEtiqueta clienteEtiqueta : clienteEtiquetas)
        {
            listaUI.add(modelToUI(clienteEtiqueta));
        }
        return listaUI;
    }

    private UIEntity modelToUI(ClienteEtiqueta clienteEtiqueta) {
        UIEntity entity = UIEntity.toUI(clienteEtiqueta);
        entity.put("etiquetaNombre", clienteEtiqueta.getEtiqueta().getNombre());
        return entity;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getClienteEtiquetaById(@PathParam("id") Long clienteEtiquetaId) {
        ClienteEtiqueta clienteEtiqueta = clienteEtiquetaService
                .getClienteEtiquetaById(clienteEtiquetaId);
        return modelToUI(clienteEtiqueta);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addClienteEtiqueta(UIEntity entity) {
        ClienteEtiqueta clienteEtiqueta = UIToModel(entity);
        clienteEtiquetaService.addClienteEtiqueta(clienteEtiqueta);
        return modelToUI(clienteEtiqueta);
    }

    private ClienteEtiqueta UIToModel(UIEntity entity) {
        ClienteEtiqueta clienteEtiqueta = entity.toModel(ClienteEtiqueta.class);

        Etiqueta etiqueta = getEtiqueta(entity);
        clienteEtiqueta.setEtiqueta(etiqueta);

        Cliente cliente = new Cliente();
        cliente.setId(Long.parseLong(entity.get("clienteId")));
        clienteEtiqueta.setCliente(cliente);

        return clienteEtiqueta;
    }

    private Etiqueta getEtiqueta(UIEntity entity) {
        Etiqueta etiqueta;
        String etiquetaNombre = entity.get("etiquetaNombre");
        if (etiquetaNombre != null)
        {
            etiqueta = etiquetaService.getEtiquetaByNombre(etiquetaNombre);
            if (etiqueta == null)
            {
                etiqueta = new Etiqueta();
                etiqueta.setNombre(etiquetaNombre);
                etiquetaService.addEtiqueta(etiqueta);
            }
        }
        else
        {
            etiqueta = new Etiqueta();
            etiqueta.setId(Long.parseLong(entity.get("etiquetaId")));
        }
        return etiqueta;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateClienteEtiqueta(@PathParam("id") Long clienteEtiquetaId, UIEntity entity) {
        ClienteEtiqueta clienteEtiqueta = UIToModel(entity);
        clienteEtiqueta.setId(clienteEtiquetaId);
        clienteEtiquetaService.updateClienteEtiqueta(clienteEtiqueta);
        return modelToUI(clienteEtiqueta);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long clienteEtiquetaId) {
        clienteEtiquetaService.deleteClienteEtiqueta(clienteEtiquetaId);
    }
}