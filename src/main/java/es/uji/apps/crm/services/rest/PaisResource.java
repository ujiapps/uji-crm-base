package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Pais;
import es.uji.apps.crm.services.PaisService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("pais")
public class PaisResource extends CoreBaseService {
    @InjectParam
    private PaisService paisService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPaises(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        return modelToUI(paisService.getPaises(), idioma);
    }

    private List<UIEntity> modelToUI(List<Pais> paises, String idioma) {

        List<UIEntity> lista = new ArrayList<>();
        for (Pais pais : paises)
        {
            lista.add(modelToUI(pais, idioma));
        }

        return lista;
    }


    private UIEntity modelToUI(Pais pais, String idioma) {
        UIEntity entity = UIEntity.toUI(pais);

        if (idioma.equalsIgnoreCase("ca"))
        {
            entity.put("nombre", pais.getNombreCA());
        }
        if (idioma.equalsIgnoreCase("en") || idioma.equalsIgnoreCase("uk"))
        {
            entity.put("nombre", pais.getNombreEN());
        }
        if (idioma.equalsIgnoreCase("es"))
        {
            entity.put("nombre", pais.getNombreES());
        }
        return entity;
    }


}