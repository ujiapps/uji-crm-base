package es.uji.apps.crm.services.rest;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.crm.model.CampanyaSeguimiento;
import es.uji.apps.crm.model.SeguimientoFichero;
import es.uji.apps.crm.services.SeguimientoFicheroService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("seguimientofichero")
public class SeguimientoFicheroResource extends CoreBaseService {
    @InjectParam
    private SeguimientoFicheroService seguimientoFicheroService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSeguimientoFicheros(
            @QueryParam("campanyaSeguimientoId") Long campanyaSeguimientoId) {
        List<UIEntity> listaUI = new ArrayList<>();
        if (campanyaSeguimientoId == null)
        {
            return listaUI;
        }

        List<SeguimientoFichero> lista = seguimientoFicheroService
                .getSeguimientoFicherosByCampanyaSeguimientoId(campanyaSeguimientoId);

        for (SeguimientoFichero seguimientoFichero : lista)
        {
            listaUI.add(UIEntity.toUI(seguimientoFichero));
        }

        return listaUI;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getSeguimientoFicheroById(@PathParam("id") Long seguimientoFicheroId) {
        SeguimientoFichero seguimientoFichero = seguimientoFicheroService
                .getSeguimientoFicheroById(seguimientoFicheroId);
        return UIEntity.toUI(seguimientoFichero);
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity addSeguimientoFichero(FormDataMultiPart multiPart) {
        SeguimientoFichero seguimientoFichero = new SeguimientoFichero();

        String campanyaSeguimientoId = multiPart.getField("campanyaSeguimientoId").getValue();
        CampanyaSeguimiento campanyaSeguimiento = new CampanyaSeguimiento();
        campanyaSeguimiento.setId(ParamUtils.parseLong(campanyaSeguimientoId));

        seguimientoFichero.setCampanyaSeguimiento(campanyaSeguimiento);

        for (BodyPart bodyPart : multiPart.getBodyParts())
        {
            String mimeType = bodyPart.getHeaders().getFirst("Content-Type");
            if (mimeType != null && !mimeType.isEmpty())
            {
                String fileName = "";
                String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                Matcher m = fileNamePattern.matcher(header);
                if (m.matches())
                {
                    fileName = m.group(1);
                }
                BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                InputStream documento = bpe.getInputStream();
                seguimientoFichero.setContentType(mimeType);
                seguimientoFichero.setNombreFichero(fileName);
                break;
            }
        }

        seguimientoFichero = seguimientoFicheroService.addSeguimientoFichero(seguimientoFichero);
        return UIEntity.toUI(seguimientoFichero);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateSeguimientoFichero(@PathParam("id") Long seguimientoFicheroId,
                                             UIEntity entity) {
        SeguimientoFichero seguimientoFichero = UIToModel(entity);
        seguimientoFichero.setId(seguimientoFicheroId);
        seguimientoFicheroService.updateSeguimientoFichero(seguimientoFichero);
        return UIEntity.toUI(seguimientoFichero);
    }

    private SeguimientoFichero UIToModel(UIEntity entity) {
        SeguimientoFichero seguimientoFichero = entity.toModel(SeguimientoFichero.class);

        CampanyaSeguimiento campanyaSeguimiento = new CampanyaSeguimiento();
        campanyaSeguimiento.setId(Long.parseLong(entity.get("campanyaSeguimientoId")));
        seguimientoFichero.setCampanyaSeguimiento(campanyaSeguimiento);

        return seguimientoFichero;
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long seguimientoFicheroId) {
        seguimientoFicheroService.deleteSeguimientoFichero(seguimientoFicheroId);
    }
}