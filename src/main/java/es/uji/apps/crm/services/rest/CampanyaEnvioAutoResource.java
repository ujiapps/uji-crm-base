package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaEnvioAuto;
import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.apps.crm.services.CampanyaEnvioAutoService;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("campanyaenvioauto")
public class CampanyaEnvioAutoResource extends CoreBaseService {
    @InjectParam
    private CampanyaEnvioAutoService campanyaEnvioAutoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyaEnviosAutoByCampanyaId(
            @QueryParam("campanyaId") Long campanyaId) {
        List<CampanyaEnvioAuto> campanyaEnviosAuto = campanyaEnvioAutoService
                .getCampanyaEnviosAutoByCampanyaId(campanyaId);

        return modelToUI(campanyaEnviosAuto);
    }

    private List<UIEntity> modelToUI(List<CampanyaEnvioAuto> campanyaEnviosAuto) {

        List<UIEntity> entityCampanyasEnvioAuto = new ArrayList<>();

        for (CampanyaEnvioAuto campanyaEnvioAuto : campanyaEnviosAuto)
        {
            entityCampanyasEnvioAuto.add(modelToUI(campanyaEnvioAuto));
        }

        return entityCampanyasEnvioAuto;
    }

    private UIEntity modelToUI(CampanyaEnvioAuto campanyaEnvioAuto) {
        UIEntity entity = UIEntity.toUI(campanyaEnvioAuto);

        entity.put("campanyaEnvioTipoNombre", campanyaEnvioAuto.getCampanyaEnvioTipo().getNombre());

        return entity;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanyaEnvioAuto(UIEntity entity) {

        CampanyaEnvioAuto campanyaEnvioAuto = UIToModel(entity);
        campanyaEnvioAutoService.addCampanyaEnvioAuto(campanyaEnvioAuto);

        return modelToUI(campanyaEnvioAuto);
    }

    private CampanyaEnvioAuto UIToModel(UIEntity entity) {
        CampanyaEnvioAuto campanyaEnvioAuto = entity.toModel(CampanyaEnvioAuto.class);
        campanyaEnvioAuto.setCuerpo(campanyaEnvioAuto.getCuerpo().replace("http:", ""));

        Campanya campanya = new Campanya();
        campanya.setId(Long.parseLong(entity.get("campanyaId")));
        campanyaEnvioAuto.setCampanya(campanya);

        TipoEstadoCampanya tipoEnvio = new TipoEstadoCampanya();
        tipoEnvio.setId(Long.parseLong(entity.get("campanyaEnvioTipoId")));
        campanyaEnvioAuto.setCampanyaEnvioTipo(tipoEnvio);

        return campanyaEnvioAuto;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanyaEnvioAuto(@PathParam("id") Long envioAutoId, UIEntity entity) {
        CampanyaEnvioAuto campanyaEnvioAuto = UIToModel(entity);
        campanyaEnvioAuto.setId(envioAutoId);
        campanyaEnvioAutoService.updateCampanyaEnvioAuto(campanyaEnvioAuto);

        return modelToUI(campanyaEnvioAuto);
    }

    @PUT
    @Path("autoguardado")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void autoguardadoCampanyaEnvioAuto(@FormParam("campanyaEnvioAutoId") Long id, @FormParam("asunto") String asunto, @FormParam("cuerpo") String cuerpo){
        campanyaEnvioAutoService.autoguardadoCampanyaEnvioAuto(id, asunto, cuerpo);
    }

    @POST
    @Path("autoguardado")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity autoguardadoCampanyaEnvioAuto(@FormParam("asunto") String asunto, @FormParam("cuerpo") String cuerpo, @FormParam("campanyaId") Long campanyaId,
                                                  @FormParam("campanyaEnvioTipoId") Long campanyaEnvioTipoId, @FormParam("tipoEnvioId") Long tipoEnvioId){
        return UIEntity.toUI(campanyaEnvioAutoService.autoguardadoCampanyaEnvioAuto(asunto, cuerpo, campanyaId, campanyaEnvioTipoId, tipoEnvioId));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long campanyaEnvioAutoId) {
        campanyaEnvioAutoService.delete(campanyaEnvioAutoId);
    }

    @POST
    @Path("prueba")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity enviarCorreoDePrueba(UIEntity entity) throws MessageNotSentException {
        String email = entity.get("email");
        Long campanyaenvioId = entity.getLong("id");
        ParamUtils.checkNotNull(email, campanyaenvioId);

        CampanyaEnvioAuto campanyaEnvioAuto = campanyaEnvioAutoService.getById(campanyaenvioId);

        MailMessage mensaje = new MailMessage("CRM");
        mensaje.setTitle(campanyaEnvioAuto.getAsunto());
        mensaje.setContentType(MediaType.TEXT_HTML);
        String cuerpo = campanyaEnvioAuto.getCuerpo();
        if (ParamUtils.isNotNull(campanyaEnvioAuto.getBase()))
        {
            int title = cuerpo.indexOf("</title>");
            String cuerpo_inicio = cuerpo.substring(0, title + 8);
            String cuerpo_final = cuerpo.substring(title + 9);
            cuerpo = cuerpo_inicio + campanyaEnvioAuto.getBase() + cuerpo_final;
        }
        mensaje.setContent(cuerpo);
        mensaje.setReplyTo("no-reply@uji.es");
        mensaje.setSender("CRM-Prueba <no-reply@uji.es>");
        mensaje.addToRecipient(email);

        MessagingClient cliente = new MessagingClient();
        cliente.send(mensaje);

        return entity;
    }

}