package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaEnvioAdmin;
import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.services.CampanyaEnvioAdminService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("campanyaenvioadmin")
public class CampanyaEnvioAdminResource extends CoreBaseService {
    @InjectParam
    private CampanyaEnvioAdminService campanyaEnvioAdminService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyaEnviosAdminByCampanyaId(
            @QueryParam("campanyaId") Long campanyaId) {
        List<CampanyaEnvioAdmin> campanyaEnviosAdmin = campanyaEnvioAdminService
                .getCampanyaEnviosAdminByCampanyaId(campanyaId);

        return modelToUI(campanyaEnviosAdmin);
    }

    private List<UIEntity> modelToUI(List<CampanyaEnvioAdmin> campanyaEnviosAdmin) {

        List<UIEntity> entityCampanyasEnvioAdmin = new ArrayList<>();

        for (CampanyaEnvioAdmin campanyaEnvioAdmin : campanyaEnviosAdmin)
        {
            entityCampanyasEnvioAdmin.add(modelToUI(campanyaEnvioAdmin));
        }

        return entityCampanyasEnvioAdmin;
    }

    private UIEntity modelToUI(CampanyaEnvioAdmin campanyaEnvioAdmin) {
        UIEntity entity = UIEntity.toUI(campanyaEnvioAdmin);

        entity.put("campanyaEnvioTipoNombre", campanyaEnvioAdmin.getCampanyaEnvioTipo().getNombre());

        return entity;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanyaEnvioAdmin(UIEntity entity) {

        CampanyaEnvioAdmin campanyaEnvioAdmin = UIToModel(entity);
        campanyaEnvioAdminService.addCampanyaEnvioAdmin(campanyaEnvioAdmin);

        return modelToUI(campanyaEnvioAdmin);
    }

    private CampanyaEnvioAdmin UIToModel(UIEntity entity) {
        CampanyaEnvioAdmin campanyaEnvioAdmin = entity.toModel(CampanyaEnvioAdmin.class);

        Campanya campanya = new Campanya();
        campanya.setId(Long.parseLong(entity.get("campanyaId")));
        campanyaEnvioAdmin.setCampanya(campanya);

        Tipo tipoEnvio = new Tipo();
        tipoEnvio.setId(Long.parseLong(entity.get("campanyaEnvioTipoId")));
        campanyaEnvioAdmin.setCampanyaEnvioTipo(tipoEnvio);

        return campanyaEnvioAdmin;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateCampanyaEnvioAdmin(@PathParam("id") Long envioAdminId, UIEntity entity) {
        CampanyaEnvioAdmin campanyaEnvioAdmin = UIToModel(entity);
        campanyaEnvioAdmin.setId(envioAdminId);
        campanyaEnvioAdminService.updateCampanyaEnvioAdmin(campanyaEnvioAdmin);

        return modelToUI(campanyaEnvioAdmin);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long campanyaEnvioAdminId) {
        campanyaEnvioAdminService.delete(campanyaEnvioAdminId);
    }
}