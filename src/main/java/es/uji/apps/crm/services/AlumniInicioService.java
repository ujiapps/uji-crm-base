package es.uji.apps.crm.services;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.crm.model.EnvioPlantillaAdmin;
import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.model.domains.TipoCategoriaFormulario;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.Envio;
import es.uji.apps.crm.utils.AppInfo;
import es.uji.apps.crm.utils.HashUtils;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;

@Service
public class AlumniInicioService {

    public final static Logger log = LoggerFactory.getLogger(CampanyaClienteService.class);

    @Context
    protected HttpServletResponse response;
    private final ClienteService clienteService;
    private final TipoService tipoService;
    private final SesionService sesionService;
    private final EnvioService envioService;
    private final EnvioClienteService envioClienteService;
    private final EnvioPlantillaAdminService envioPlantillaAdminService;

//    @Value("${uji.crm.domain}")
    private final String domain = "uji.es";

    @Autowired
    public AlumniInicioService(ClienteService clienteService, TipoService tipoService, SesionService sesionService, EnvioService envioService,
                               EnvioClienteService envioClienteService, EnvioPlantillaAdminService envioPlantillaAdminService) {
        this.clienteService = clienteService;
        this.tipoService = tipoService;
        this.sesionService = sesionService;
        this.envioService = envioService;
        this.envioClienteService = envioClienteService;
        this.envioPlantillaAdminService = envioPlantillaAdminService;
    }

    public Response activarSesionPorHash(String pHash, String path, Boolean formularioPremium) throws URISyntaxException {

        if (ParamUtils.isNotNull(sesionService.getUserIdActiveSesion(pHash, Boolean.TRUE))) {
            Cookie hashCookie = new Cookie("p_hash", pHash);
            hashCookie.setPath("/");
            hashCookie.setHttpOnly(true);
            hashCookie.setDomain(domain);
            response.addCookie(hashCookie);
            URI url;
            if (formularioPremium) {
                url = new URI(AppInfo.getHost() + "/crm/rest/" + path + "/hash/formulariopremium");
            } else {
                url = new URI(AppInfo.getHost() + "/crm/rest/" + path + "/hash/");
                System.out.println(url);
            }

            return Response.seeOther(url).build();
        } else {
            if (path.equals("alumni")) {
                URI url = new URI(AppInfo.getHost() + "/crm/rest/" + path + "/inici?error_hash=1");
                return Response.seeOther(url).build();
            }
            URI url = new URI(AppInfo.getHost() + "/crm/rest/" + path + "/login?error_hash=1");
            return Response.seeOther(url).build();
        }
    }

    public ResponseMessage enviarFormularioContacto(Long tipoIdentificacionId, String identificacion, String nombre, String apellidos, String correo, String fechaNacimiento, String telefono,
                                                    Long categoria, String nacionalidad) {

        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(false);

        try {
            MailMessage mensaje = new MailMessage("CRM");

            mensaje.setTitle("Formulario Contacto Usuario ALUMNI");
            mensaje.setContentType(MediaType.TEXT_HTML);

            Tipo tipoIdentificacion = tipoService.getTiposById(tipoIdentificacionId);
            String cuerpo = "Tipo Identificacion: " + tipoIdentificacion.getNombre() + "<br />";
            cuerpo = cuerpo + "Identificacion: " + identificacion + "<br />";
            cuerpo = cuerpo + "Nombre: " + nombre + "<br />";
            cuerpo = cuerpo + "Apellidos: " + apellidos + "<br />";
            cuerpo = cuerpo + "Correo: " + correo + "<br />";
            cuerpo = cuerpo + "Telefono: " + telefono + "<br />";
            cuerpo = cuerpo + "Fecha Nacimiento: " + fechaNacimiento + "<br />";
            cuerpo = cuerpo + "Categoria: " + TipoCategoriaFormulario.getTextoById(categoria) + "<br />";
            cuerpo = cuerpo + "Nacionalidad: " + nacionalidad + "<br />";

            mensaje.setContent(cuerpo);
            mensaje.setReplyTo("alumnisauji@uji.es");
            mensaje.setSender("Programa AlumniSAUJI <alumnisauji@uji.es>");
            mensaje.addToRecipient("alumnisauji@uji.es");

            MessagingClient cliente = new MessagingClient();
            cliente.send(mensaje);

            responseMessage.setSuccess(true);
        } catch (Exception e) {
            log.error("No se ha podido enviar correo formulario contacto", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }


    public ResponseMessage anyadirSesionUsuario(@FormParam("correo") String correo,
                                                @DefaultValue("ca") @CookieParam("uji-lang") String idioma,
                                                HttpServletRequest request) {
        Cliente cliente = clienteService.getClienteByCorreoAndServicio(correo, 366L);
        ResponseMessage responseMessage = new ResponseMessage();
        if (ParamUtils.isNotNull(cliente) && cliente.getCorreo().equals(correo)) {
            //generar session
            String path = request.getPathInfo();
            String pathWeb = "/crm/rest/" + path.split("/")[1] + "/";

            generarSession(cliente, idioma, pathWeb, "");
            responseMessage.setSuccess(true);

            return responseMessage;

        }
        responseMessage.setSuccess(false);
        return responseMessage;
    }

    private void generarSession(Cliente cliente, String idioma, String base, String destino) {
        String hash = HashUtils.stringToHashLogin();
        sesionService.renewSesion(cliente, hash);
        String direccionWeb;
        if (base.split("/")[1].equals("alumni")) {
            direccionWeb = AppInfo.getHost() + base + "inici/hash/" + hash;
        } else {
            direccionWeb = AppInfo.getHost() + base + "login/hash/" + hash;
        }

        if (ParamUtils.isNotNull(destino)) {
            direccionWeb = direccionWeb + "/" + destino;
        }
        EnvioPlantillaAdmin envioPlantillaAdmin = envioPlantillaAdminService.getEnvioPlantillaAdminByNombre("ENVIO_PARA_HASH");
        Envio envio = new Envio(envioPlantillaAdmin, idioma, direccionWeb);
        envioService.insert(envio);
        envioClienteService.addEnvioCliente(cliente, envio, cliente.getCorreo());
    }
}