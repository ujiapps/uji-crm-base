package es.uji.apps.crm.services.rest;

import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.exceptions.ErrorEnBorradoDeDocumentoException;
import es.uji.apps.crm.model.EnvioCliente;
import es.uji.apps.crm.model.EnvioClienteVista;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.services.EnvioClienteService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("enviocliente")
public class EnvioClienteResource extends CoreBaseService {
    @InjectParam
    private EnvioClienteService envioClienteService;

    @GET
    @Path("envio/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getClientesByEnvioId(@PathParam("id") Long envioId,
                                                @QueryParam("start") @DefaultValue("-1") Long start,
                                                @QueryParam("limit") @DefaultValue("5") Long limit,
                                                @QueryParam("sort") @DefaultValue("[]") String sortJson) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit);

            if (sortJson.length() > 2) {
                List<Map<String, String>> sortList = new ObjectMapper().readValue(sortJson, new TypeReference<List<Map<String, String>>>() {
                });
                paginacion.setOrdenarPor(sortList.get(0).get("property"));
                paginacion.setDireccion(sortList.get(0).get("direction"));
            }

            List<EnvioClienteVista> listaEnvioCliente = envioClienteService.getEnvioClientesByEnvioId(envioId, paginacion);

            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(UIEntity.toUI(listaEnvioCliente));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            e.printStackTrace();
            responseMessage.setSuccess(false);
        }
        return responseMessage;

    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getClientesEnvioByClienteId(@PathParam("id") Long clienteId,
                                                       @QueryParam("start") @DefaultValue("0") Long start,
                                                       @QueryParam("limit") @DefaultValue("25") Long limit,
                                                       @QueryParam("page") @DefaultValue("0") Integer pag,
                                                       @QueryParam("sort") @DefaultValue("[]") String sortJson) {

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit);

            if (sortJson.length() > 2) {
                List<Map<String, String>> sortList = new ObjectMapper().readValue(sortJson, new TypeReference<List<Map<String, String>>>() {
                });
                paginacion.setOrdenarPor(sortList.get(0).get("property"));
                paginacion.setDireccion(sortList.get(0).get("direction"));
            }

            List<EnvioCliente> listaEnvioCliente = envioClienteService.getEnvioClientesByClienteId(clienteId, paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(modelToUI(listaEnvioCliente));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            e.printStackTrace();
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    private List<UIEntity> modelToUI(List<EnvioCliente> enviosCliente) {
        List<UIEntity> result = new ArrayList<UIEntity>();
        for (EnvioCliente envioCliente : enviosCliente) {
            UIEntity entity = modelToUI(envioCliente);
            result.add(entity);
        }

        return result;
    }

    private UIEntity modelToUI(EnvioCliente envioCliente) {
        UIEntity entity = UIEntity.toUI(envioCliente);

        entity.put("clienteNombre", envioCliente.getCliente().getNombreApellidos());

        entity.put("nombre", envioCliente.getEnvio().getNombre());
        entity.put("asunto", envioCliente.getEnvio().getAsunto());
        entity.put("cuerpo", envioCliente.getEnvio().getCuerpo());
        entity.put("responder", envioCliente.getEnvio().getResponder());
        entity.put("desde", envioCliente.getEnvio().getDesde());
        entity.put("envioTipoNombre", envioCliente.getEnvio().getEnvioTipo().getNombre());
        if (ParamUtils.isNotNull(envioCliente.getEnvioMensajeria())) {
            entity.put("estado", envioCliente.getEnvioMensajeria().getEstado());
        }
//        entity.put("estado", envioCliente.getEnvio());

        return entity;
    }

    @POST
    @Path("duplicaenvio/{envioClienteId}")
    public void duplicaEnvio(@PathParam("envioClienteId") Long envioClienteId) throws ParseException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        envioClienteService.duplicaEnvio(connectedUserId, envioClienteId);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteEnvioCliente(@PathParam("id") Long envioClienteId)
            throws MalformedURLException, ErrorEnBorradoDeDocumentoException {
        envioClienteService.deleteEnvioCliente(envioClienteId);
    }

    private UIEntity modelToUILong(Long count) {
        UIEntity entity = new UIEntity();
        entity.put("count", count);
        return entity;
    }

}