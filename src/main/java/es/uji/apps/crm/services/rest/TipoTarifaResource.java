package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.TipoTarifa;
import es.uji.apps.crm.services.CampanyaService;
import es.uji.apps.crm.services.TipoTarifaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("tipotarifa")
public class TipoTarifaResource extends CoreBaseService {
    @InjectParam
    private TipoTarifaService tipoTarifaService;
    @InjectParam
    private CampanyaService campanyaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposTarifa(@QueryParam("campanyaId") Long campanyaId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        if (ParamUtils.isNotNull(campanyaId))
        {
            return modelToUI(tipoTarifaService.getTiposTarifaByCampanyaId(campanyaId));
        }
        return modelToUI(tipoTarifaService.getTiposTarifa(connectedUserId));
    }

    private List<UIEntity> modelToUI(List<TipoTarifa> tipoTarifas) {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        for (TipoTarifa tipoTarifa : tipoTarifas)
        {
            lista.add(modelToUI(tipoTarifa));
        }
        return lista;

    }

    private UIEntity modelToUI(TipoTarifa tipoTarifa) {
        UIEntity entity = UIEntity.toUI(tipoTarifa);
        Campanya campanya = campanyaService.getCampanyaById(tipoTarifa.getCampanya().getId());
        entity.put("campanyaNombre", campanya.getNombre());
        entity.put("defecto", tipoTarifa.getDefecto());
        return entity;
    }

    @GET
    @Path("campanya/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposTarifaByCampanyaId(@PathParam("id") Long campanyaId) {
        return modelToUI(tipoTarifaService.getTiposTarifaByCampanyaId(campanyaId));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addTipoTarifa(UIEntity entity) {
        TipoTarifa tipoTarifa = UIToModel(entity);
        tipoTarifaService.addTipoTarifa(tipoTarifa);
        return modelToUI(tipoTarifa);
    }

    private TipoTarifa UIToModel(UIEntity entity) {
        TipoTarifa tipoTarifa = entity.toModel(TipoTarifa.class);

        Campanya campanya = new Campanya();
        campanya.setId(ParamUtils.parseLong(entity.get("campanyaId")));

        tipoTarifa.setCampanya(campanya);

        return tipoTarifa;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateTipoTarifa(UIEntity entity) {

        TipoTarifa tipoTarifa = UIToModel(entity);
        tipoTarifaService.updateTipoTarifa(tipoTarifa);
        return modelToUI(tipoTarifa);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long tipoTarifaId) {
        tipoTarifaService.deleteTipoTarifa(tipoTarifaId);
    }
}