package es.uji.apps.crm.services;

import java.awt.image.BufferedImage;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.crm.exceptions.ErrorEnBorradoDeDocumentoException;
import es.uji.apps.crm.exceptions.ErrorSubiendoDocumentoException;

@Component
public class ADEClienteService {

    private String authToken;
    private String url;

    public String getAuthToken() {
        return authToken;
    }

    @Autowired
    public void setAuthToken(@Value("${uji.ade.authToken}") String authToken) {
        this.authToken = authToken;
    }

    public String getUrl() {
        return url;
    }

    @Autowired
    public void setUrl(@Value("${uji.ade.url}") String url) {
        this.url = url;
    }


    public BufferedImage getImagen(String referencia) {
        try {
            return ImageIO.read(new URL(url + referencia));
        } catch (Exception e) {
            return null;
        }
    }

    public String addDocumento(String nombre, String mimetype, byte[] contenido)
            throws MalformedURLException, ErrorSubiendoDocumentoException {

        WebResource adePostClient = Client.create().resource(url);

        FormDataMultiPart responseMultipart = new FormDataMultiPart();
        responseMultipart.field("name", nombre);
        responseMultipart.field("mimetype", mimetype);
        responseMultipart.field("contents", contenido, MediaType.valueOf(mimetype));

        ClientResponse response = adePostClient.type(MediaType.MULTIPART_FORM_DATA)
                .header("X-UJI-AuthToken", authToken).post(ClientResponse.class, responseMultipart);

        JsonNode responseMessage = response.getEntity(JsonNode.class);
        JsonNode payload = responseMessage.path("data");

        if (response.getStatus() == 200 && responseMessage.path("success").asBoolean())
        {
            return payload.path("reference").asText();
        }

        throw new ErrorSubiendoDocumentoException();
    }

    public void deleteDocumento(String reference) throws ErrorEnBorradoDeDocumentoException, MalformedURLException {

        URL del = new URL(new URL(url), reference);
        WebResource adePostClient = Client.create().resource(del.toString());

        ClientResponse response = adePostClient.header("X-UJI-AuthToken", authToken).delete(
                ClientResponse.class);

        if (response.getStatus() < 200 && response.getStatus() >= 300)
        {
            throw new ErrorEnBorradoDeDocumentoException();
        }
    }

}