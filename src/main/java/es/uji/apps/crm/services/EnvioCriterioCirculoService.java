package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.EnvioCriterioCirculoDAO;
import es.uji.apps.crm.model.EnvioCriterio;
import es.uji.apps.crm.model.EnvioCriterioCirculo;
import es.uji.apps.crm.model.Item;

@Service
public class EnvioCriterioCirculoService {

    private EnvioCriterioCirculoDAO envioCriterioCirculoDAO;

    @Autowired
    public EnvioCriterioCirculoService(EnvioCriterioCirculoDAO envioCriterioCirculoDAO) {
        this.envioCriterioCirculoDAO = envioCriterioCirculoDAO;
    }

    public void insertEnvioCriteriosCirculo(List<Long> arrayCriteriosCirculo, Long envioCriterioId) {

        for (Long circuloId : arrayCriteriosCirculo)
        {
            insertEnvioCriterioCirculo(circuloId, envioCriterioId);
        }

    }

    public void insertEnvioCriterioCirculo(Long circuloId, Long envioCriterioId) {

        EnvioCriterioCirculo envioCriterioCirculo = new EnvioCriterioCirculo();

        Item circulo = new Item();
        circulo.setId(circuloId);
        envioCriterioCirculo.setCirculo(circulo);

        EnvioCriterio envioCriterio = new EnvioCriterio();
        envioCriterio.setId(envioCriterioId);
        envioCriterioCirculo.setEnvioCriterio(envioCriterio);

        envioCriterioCirculoDAO.insert(envioCriterioCirculo);


    }

    public void deleteEnvioCriteriosCirculo(Long envioCriterioId) {
        envioCriterioCirculoDAO.delete(EnvioCriterioCirculo.class, "envio_criterio_id = " + envioCriterioId);

    }

    public List<EnvioCriterioCirculo> getEnvioCriterioCirculosByEnvioCriterioId(Long envioCriterioId) {
        return envioCriterioCirculoDAO.get(EnvioCriterioCirculo.class, "envio_criterio_id = " + envioCriterioId);
    }
}