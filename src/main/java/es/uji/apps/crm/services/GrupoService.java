package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.GrupoDAO;
import es.uji.apps.crm.model.Grupo;

@Service
public class GrupoService {
    private GrupoDAO grupoDAO;

    @Autowired
    public GrupoService(GrupoDAO grupoDAO) {
        this.grupoDAO = grupoDAO;
    }

    public List<Grupo> getGrupos(Long connectedUserId, Long programaId) {
        if (programaId != null)
        {
            return grupoDAO.getGruposByProgramaId(connectedUserId, programaId);
        }

        return grupoDAO.getGrupos(connectedUserId);
    }

    public List<Grupo> getGruposFaltanByClienteId(Long connectedUserId, Long clienteId) {
        return grupoDAO.getGruposFaltanByClienteId(connectedUserId, clienteId);
    }

    public List<Grupo> getGruposAsociadosByCampanyaId(Long campanyaId) {
        return grupoDAO.getGruposAsociadosByCampanyaId(campanyaId);
    }

    public Grupo getGrupoById(Long grupoId) {
        return grupoDAO.getGrupoById(grupoId);
    }

    public Grupo addGrupo(Grupo grupo) {
        grupoDAO.insert(grupo);
        grupo.setId(grupo.getId());
        return grupo;
    }

    public void updateGrupo(Grupo grupo) {
        grupoDAO.update(grupo);
    }

    public void deleteGrupo(Long grupoId) {
        grupoDAO.delete(Grupo.class, grupoId);
    }


    public List<Grupo> getGruposTodosByUsuarioConectado(Long connectedUserId) {
        return grupoDAO.getGrupos(connectedUserId);
    }
}