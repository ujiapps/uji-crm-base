package es.uji.apps.crm.services.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Tarifa;
import es.uji.apps.crm.model.TipoTarifa;
import es.uji.apps.crm.services.TarifaService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("tarifa")
public class TarifaResource extends CoreBaseService {
    @InjectParam
    private TarifaService tarifaService;
    @InjectParam
    private UtilsService utilsService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTarifasByTipoTarifa(@QueryParam("tipoTarifaId") Long tipoTarifaId) {
        TipoTarifa tipoTarifa = new TipoTarifa();
        tipoTarifa.setId(tipoTarifaId);
        return modelToUI(tarifaService.getTarifasByTipoTarifa(tipoTarifa));
    }

    private List<UIEntity> modelToUI(List<Tarifa> tarifas) {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        for (Tarifa tarifa : tarifas)
        {
            lista.add(modelToUI(tarifa));
        }
        return lista;

    }

    private UIEntity modelToUI(Tarifa tarifa) {
        UIEntity entity = UIEntity.toUI(tarifa);
        return entity;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addTarifa(UIEntity entity) throws ParseException {
        Tarifa tarifa = UIToModel(entity);
        tarifaService.addTarifa(tarifa);
        return modelToUI(tarifa);
    }

    private Tarifa UIToModel(UIEntity entity) throws ParseException {
        Tarifa tarifa = entity.toModel(Tarifa.class);

        TipoTarifa tipoTarifa = new TipoTarifa();
        tipoTarifa.setId(ParamUtils.parseLong(entity.get("tipoTarifaId")));

        tarifa.setTipoTarifa(tipoTarifa);

        String fechaIni = entity.get("fechaInicio");
        if (ParamUtils.isNotNull(fechaIni))
        {
            tarifa.setFechaInicio(utilsService.fechaParse(fechaIni));
        }

        String fechaFin = entity.get("fechaFin");
        if (ParamUtils.isNotNull(fechaFin))
        {
            tarifa.setFechaFin(utilsService.fechaParse(fechaFin));
        }

        return tarifa;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateTarifa(UIEntity entity) throws ParseException {

        Tarifa tarifa = UIToModel(entity);
        tarifaService.updateTarifa(tarifa);
        return modelToUI(tarifa);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long tarifaId) {
        tarifaService.deleteTarifa(tarifaId);
    }
}