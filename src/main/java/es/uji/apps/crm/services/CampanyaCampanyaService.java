package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaCampanyaDAO;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaCampanya;
import es.uji.apps.crm.model.TipoEstadoCampanya;

@Service
public class CampanyaCampanyaService {
    private CampanyaCampanyaDAO campanyaCampanyaDAO;

    @Autowired
    public CampanyaCampanyaService(CampanyaCampanyaDAO campanyaCampanyaDAO) {
        this.campanyaCampanyaDAO = campanyaCampanyaDAO;
    }

//    public CampanyaCampanya getCampanyaCampanyaById(Long campanyaCampanyaId) {
//        return null;
//    }

    public List<CampanyaCampanya> getCampanyasCampanyasDestinoByCampanya(Campanya Campanya) {
        return campanyaCampanyaDAO.getCampanyasCampanyasDestinoByCampanya(Campanya);

    }

    public List<CampanyaCampanya> getCampanyasCampanyasOrigenByCampanya(Campanya campanya) {
        return campanyaCampanyaDAO.getCampanyasCampanyasOrigenByCampanyaId(campanya);
    }

    public CampanyaCampanya addCampanyaCampanya(CampanyaCampanya campanyaCampanya) {
        campanyaCampanyaDAO.insert(campanyaCampanya);
        campanyaCampanya.setId(campanyaCampanya.getId());
        return campanyaCampanya;
    }

    public CampanyaCampanya updateCampanyaCampanya(CampanyaCampanya campanyaCampanya) {
        return campanyaCampanyaDAO.update(campanyaCampanya);
    }

    public void deleteCampanyaCampanya(Long campanyaCampanyaId) {
        campanyaCampanyaDAO.delete(CampanyaCampanya.class, campanyaCampanyaId);
    }

    public CampanyaCampanya getCampanyaVinculada(Campanya campanya, TipoEstadoCampanya estado) {
        return campanyaCampanyaDAO.getCampanyaVinculada(campanya, estado);
    }
}