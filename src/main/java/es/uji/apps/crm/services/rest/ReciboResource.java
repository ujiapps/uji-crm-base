package es.uji.apps.crm.services.rest;


import java.io.StringWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.opencsv.CSVWriter;
import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.FiltroCampanya;
import es.uji.apps.crm.model.FiltroEstadoRecibo;
import es.uji.apps.crm.model.FiltroExportacion;
import es.uji.apps.crm.model.FiltroFechas;
import es.uji.apps.crm.model.FiltroRecibo;
import es.uji.apps.crm.model.MovimientoRecibo;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.model.Recibo;
import es.uji.apps.crm.model.ReciboRemesaExportado;
import es.uji.apps.crm.services.ReciboMovimientoExportadoService;
import es.uji.apps.crm.services.ReciboRemesaExportadoService;
import es.uji.apps.crm.services.ReciboService;
import es.uji.apps.crm.services.TipoEstadoCampanyaService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("recibo")
public class ReciboResource extends CoreBaseService {
    @InjectParam
    private ReciboService reciboService;
    @InjectParam
    private UtilsService utilsService;
    @InjectParam
    private ReciboMovimientoExportadoService reciboMovimientoExportadoService;
    @InjectParam
    private ReciboRemesaExportadoService reciboRemesaExportadoService;

    @InjectParam
    private TipoEstadoCampanyaService tipoEstadoCampanyaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getRecibos(@QueryParam("arrayCampanyas") JSONArray arrayCampanyas,
                                      @QueryParam("arrayFechas") JSONArray arrayFechas,
                                      @QueryParam("arrayEstados") JSONArray arrayEstados,
                                      @QueryParam("arrayExportaciones") JSONArray arrayExportaciones,
                                      @QueryParam("start") @DefaultValue("-1") Long start,
                                      @QueryParam("limit") @DefaultValue("5") Long limit)
            throws ParseException, JSONException {


        FiltroRecibo filtro = new FiltroRecibo();

        anyadeFiltroFechas(arrayFechas, filtro);
        anyadeFiltroCampanya(arrayCampanyas, filtro);
        anyadeFiltroEstado(arrayEstados, filtro);
        anyadeFiltroExportacion(arrayExportaciones, filtro);

        Paginacion paginacion = new Paginacion(start, limit);

        List<Recibo> recibos = reciboService.getRecibos(filtro, paginacion);

        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
        responseMessage.setData(modelToUI(recibos));

        return responseMessage;
    }

    private void anyadeFiltroExportacion(JSONArray arrayExportaciones, FiltroRecibo filtroRecibo)
            throws JSONException, ParseException {

        List<FiltroExportacion> listaFiltrosExportaciones = new ArrayList<>();
        if (ParamUtils.isNotNull(arrayExportaciones))
        {
            for (int i = 0; i < arrayExportaciones.length(); i++)
            {

                JSONObject jsonObject = arrayExportaciones.getJSONObject(i);

                FiltroExportacion filtro = new FiltroExportacion();
                filtro.setOperador(ParamUtils.parseLong(jsonObject.getString("operador")));

                JSONObject jsonFecha = jsonObject.getJSONObject("campoFecha");
                FiltroFechas filtroFechas = new FiltroFechas();
                anyadeFiltroFecha(filtroFechas, jsonFecha);
                filtro.setFecha(filtroFechas);

                listaFiltrosExportaciones.add(filtro);

            }
        }

        filtroRecibo.setFiltroExportaciones(listaFiltrosExportaciones);
    }

    private void anyadeFiltroEstado(JSONArray arrayEstados, FiltroRecibo filtro) throws JSONException {
        List<FiltroEstadoRecibo> listaFiltrosEstado = new ArrayList<>();
        if (ParamUtils.isNotNull(arrayEstados))
        {
            for (int i = 0; i < arrayEstados.length(); i++)
            {

                JSONObject jsonObjectEstados = arrayEstados.getJSONObject(i);

                FiltroEstadoRecibo filtroEstadoRecibo = new FiltroEstadoRecibo();
                filtroEstadoRecibo.setOperador(ParamUtils.parseLong(jsonObjectEstados.getString("operador")));
                filtroEstadoRecibo.setTipoEstadoRecibo(ParamUtils.parseLong(jsonObjectEstados.getString("estado")));

                listaFiltrosEstado.add(filtroEstadoRecibo);

            }
        }
        filtro.setFiltroEstadoRecibos(listaFiltrosEstado);
    }

    private void anyadeFiltroCampanya(JSONArray arrayCampanyas, FiltroRecibo filtro) throws JSONException {
        List<FiltroCampanya> listaFiltrosCampanyas = new ArrayList<>();


        if (ParamUtils.isNotNull(arrayCampanyas))
        {
            for (int i = 0; i < arrayCampanyas.length(); i++)
            {
                JSONObject jsonObjectCampanyas = arrayCampanyas.getJSONObject(i);
                FiltroCampanya filtroCampanya = new FiltroCampanya();

                Long campanyaId = ParamUtils.parseLong(jsonObjectCampanyas.getString("campanya"));
                String estadoNombre = jsonObjectCampanyas.getString("campanyaTipoEstadoNombre");
                Long estadoId = tipoEstadoCampanyaService.getTipoEstadoCampanyaByNombre(campanyaId, estadoNombre).getId();

                filtroCampanya.setOperador(ParamUtils.parseLong(jsonObjectCampanyas.getString("operador")));
                filtroCampanya.setCampanya(campanyaId);

                if (estadoId != null)
                {
                    filtroCampanya.setCampanyaTipoEstado(estadoId);
                }
                else
                {
                    filtroCampanya.setCampanyaTipoEstado(-1L);
                }
                listaFiltrosCampanyas.add(filtroCampanya);
            }
        }

        filtro.setFiltroCampanyas(listaFiltrosCampanyas);
    }

    private void anyadeFiltroFechas(JSONArray arrayFechas, FiltroRecibo filtro) throws JSONException, ParseException {

        List<FiltroFechas> listaFiltrosFechas = new ArrayList<>();

        if (ParamUtils.isNotNull(arrayFechas))
        {
            for (int i = 0; i < arrayFechas.length(); i++)
            {
                JSONObject jsonObject = arrayFechas.getJSONObject(i);

                FiltroFechas filtroFechas = new FiltroFechas();

                filtroFechas.setOperador(ParamUtils.parseLong(jsonObject.getString("operador")));

                JSONObject jsonFecha = jsonObject.getJSONObject("campoFecha");

                anyadeFiltroFecha(filtroFechas, jsonFecha);

                listaFiltrosFechas.add(filtroFechas);
            }
        }

        filtro.setFiltroFechas(listaFiltrosFechas);
    }

    private void anyadeFiltroFecha(FiltroFechas filtro, JSONObject json) throws JSONException, ParseException {

        filtro.setTipoComparacion(ParamUtils.parseLong(json.getString("tipoComparacion")));
        filtro.setTipoFecha(ParamUtils.parseLong(json.getString("tipoFecha")));
        filtro.setMes(ParamUtils.parseLong(json.getString("mes")));
        filtro.setAnyo(ParamUtils.parseLong(json.getString("anyo")));
        filtro.setMesMax(ParamUtils.parseLong(json.getString("mesMax")));
        filtro.setAnyoMax(ParamUtils.parseLong(json.getString("anyoMax")));
        filtro.setComportamiento(1L);

        if (!json.getString("fecha").equals("null"))
        {
            filtro.setFecha(utilsService.fechaParse(json.getString("fecha").substring(0, 10), "yyyy-MM-dd"));
        }

        if (!json.getString("fechaMax").equals("null"))
        {
            filtro.setFechaMax(utilsService.fechaParse(json.getString("fechaMax").substring(0, 10), "yyyy-MM-dd"));
        }
    }

    private List<UIEntity> modelToUI(List<Recibo> recibos) {
        List<UIEntity> listaUI = new ArrayList<>();

        for (Recibo recibo : recibos)
        {
            listaUI.add(modelToUI(recibo));
        }
        return listaUI;
    }

    private UIEntity modelToUI(Recibo recibo) {
        UIEntity entity = UIEntity.toUI(recibo);
        entity.put("moroso", recibo.getMoroso());
        return entity;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{personaId}")
    public List<UIEntity> getRecibosByPersonaId(@PathParam("personaId") Long personaId) {
        return modelToUI(reciboService.getRecibosByPersonaId(personaId));
    }

    @GET
    @Path("csv")
    public Response getRecibosCsv(@QueryParam("arrayFechas") JSONArray arrayFechas,
                                  @QueryParam("arrayCampanyas") JSONArray arrayCampanyas,
                                  @QueryParam("arrayExportaciones") JSONArray arrayExportaciones,
                                  @QueryParam("arrayEstados") JSONArray arrayEstados)
            throws ParseException, JSONException {

        FiltroRecibo filtro = new FiltroRecibo();

        anyadeFiltroFechas(arrayFechas, filtro);
        anyadeFiltroCampanya(arrayCampanyas, filtro);
        anyadeFiltroEstado(arrayEstados, filtro);
        anyadeFiltroExportacion(arrayExportaciones, filtro);

        Paginacion paginacion = new Paginacion(0L, 1000000L);

        List<Recibo> recibos = reciboService.getRecibosConMovimientosParaCsv(filtro, paginacion);

        Response.ResponseBuilder res;

        res = Response.ok();
        res.header("Content-Disposition", "attachment; filename = result.csv");

        res.entity(entityToCSV(recibos));
        res.type(MediaType.TEXT_PLAIN);

        return res.build();

    }

    private String entityToCSV(List<Recibo> lista) {

        final char DELIMITER = ';';

        StringWriter writer = new StringWriter();
        CSVWriter csvWriter;

        ReciboRemesaExportado remesaExportado = reciboRemesaExportadoService.addRemesa();

        try
        {
            csvWriter = new CSVWriter(writer, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");

            List<String[]> records = new ArrayList<>();
            List<String> record = new ArrayList<>(Arrays.asList("Nombre rebut", "Data creació rebut", "Importe Neto rebut", "signo", "Data Moviment", "Tipus Moviment", "Remesa"));

            String[] recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);

            for (Recibo recibo : lista)
            {

                Set<MovimientoRecibo> movimientos = recibo.getMovimientosRecibo();

                if (movimientos.size() > 0)
                {

                    for (MovimientoRecibo movimientoRecibo : movimientos)
                    {

                        String remesa = "";
                        if (ParamUtils.isNotNull(movimientoRecibo.getRemesa()))
                        {
                            remesa = movimientoRecibo.getRemesa().toString();
                        }

                        List<String> recordMov = new ArrayList<>(
                                Arrays.asList(
                                        recibo.getCodificacion(),
                                        utilsService.fechaFormat(recibo.getFechaCreacion()),
                                        recibo.getImporteNeto().toString(),
                                        movimientoRecibo.getSigno(),
                                        utilsService.fechaFormat(movimientoRecibo.getFecha()),
                                        movimientoRecibo.getTipoMovimiento(),
                                        remesa));

                        String[] recordArrayMov = new String[recordMov.size()];
                        recordMov.toArray(recordArrayMov);
                        records.add(recordArrayMov);

                        reciboMovimientoExportadoService.movimientoExportado(movimientoRecibo, remesaExportado);

                    }
                }
            }
            csvWriter.writeAll(records);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return writer.toString();
    }
}