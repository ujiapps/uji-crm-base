package es.uji.apps.crm.services.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.FiltroCorreoNoValido;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.model.VwCorreoNoValido;
import es.uji.apps.crm.services.CorreoNoValidoService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("correonovalido")
public class CorreoNoValidoResource extends CoreBaseService {
    @InjectParam
    private CorreoNoValidoService correoNoValidoService;
    @InjectParam
    private UtilsService utilsService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
//    public ResponseMessage getCorreosNoValidos(@QueryParam("arrayBusqueda") JSONArray arrayFiltroCorreoNoValido,
//                                               @QueryParam("modificado") Long modificado,
//                                               @QueryParam("operador") Long operador,
//                                               @QueryParam("start") @DefaultValue("0") Long start,
//                                               @QueryParam("limit") @DefaultValue("40") Long limit,
//                                               @QueryParam("sort") String orden) throws ParseException, JSONException {
    public ResponseMessage getCorreosNoValidos(@QueryParam("correo") String correo,
//                                               @QueryParam("modificado") Long modificado,
//                                               @QueryParam("operador") Long operador,
                                               @QueryParam("start") @DefaultValue("0") Long start,
                                               @QueryParam("limit") @DefaultValue("40") Long limit,
                                               @QueryParam("sort") String orden) throws ParseException, JSONException {
//        List<FiltroFechas> listaFiltrosFechas = new ArrayList<>();


//        if (ParamUtils.isNotNull(arrayFiltroCorreoNoValido)) {
//            for (int i = 0; i < arrayFiltroCorreoNoValido.length(); i++) {
//                JSONObject jsonObject = arrayFiltroCorreoNoValido.getJSONObject(i);
//
//                FiltroFechas filtro = new FiltroFechas();
//
//                filtro.setTipoComparacion(ParamUtils.parseLong(jsonObject.getString("tipoComparacion")));
//                filtro.setTipoFecha(ParamUtils.parseLong(jsonObject.getString("tipoFecha")));
//                filtro.setMes(ParamUtils.parseLong(jsonObject.getString("mes")));
//                filtro.setAnyo(ParamUtils.parseLong(jsonObject.getString("anyo")));
//                filtro.setMesMax(ParamUtils.parseLong(jsonObject.getString("mesMax")));
//                filtro.setAnyoMax(ParamUtils.parseLong(jsonObject.getString("anyoMax")));
//                filtro.setOperador(ParamUtils.parseLong(jsonObject.getString("operador")));
//
//                JSONObject comportamiento = (JSONObject) jsonObject.get("comportamiento");
//                filtro.setComportamiento(ParamUtils.parseLong(comportamiento.getString("radioFecha")));
//
//                if (!jsonObject.getString("fecha").equals("null")) {
//                    filtro.setFecha(utilsService.fechaParse(jsonObject.getString("fecha").substring(0, 10), "yyyy-MM-dd"));
//                }
//
//                if (!jsonObject.getString("fechaMax").equals("null")) {
//                    filtro.setFechaMax(utilsService.fechaParse(jsonObject.getString("fechaMax").substring(0, 10), "yyyy-MM-dd"));
//                }
//
//                listaFiltrosFechas.add(filtro);
//            }
//        }

        FiltroCorreoNoValido filtroCorreoNoValido = new FiltroCorreoNoValido();
//        filtroCorreoNoValido.setModificado(modificado);
//        filtroCorreoNoValido.setOperador(operador);
//        filtroCorreoNoValido.setListaFiltroFechas(listaFiltrosFechas);
        filtroCorreoNoValido.setCorreo(correo);

        Paginacion paginacion = new Paginacion(start, limit);

        String sort, dir;
        if (ParamUtils.isNotNull(orden))
        {
            JSONArray array = new JSONArray(orden);
            JSONObject obj = array.getJSONObject(0);
            sort = (String) obj.get("property");
            dir = (String) obj.get("direction");
        }
        else
        {
            sort = "nombre";
            dir = "ASC";
        }

        List<VwCorreoNoValido> lista = correoNoValidoService.getCorreosNoValidos(filtroCorreoNoValido, paginacion, sort, dir);

        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
        responseMessage.setData(modelToUI(lista));
        return responseMessage;
    }

    private List<UIEntity> modelToUI(List<VwCorreoNoValido> lista) {
        List<UIEntity> listaUI = new ArrayList<>();

        for (VwCorreoNoValido correoNoValido : lista)
        {
            listaUI.add(modelToUI(correoNoValido));
        }
        return listaUI;

    }

    private UIEntity modelToUI(VwCorreoNoValido correoNoValido) {
        UIEntity entity = UIEntity.toUI(correoNoValido);
        if (ParamUtils.isNotNull(correoNoValido.getCorreoNoValidoControl()))
        {
            entity.put("modificado", correoNoValido.getCorreoNoValidoControl().getModificado());
        }
        else
        {
            entity.put("modificado", Boolean.FALSE);
        }
        return entity;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateCorreosNoValidosControl(UIEntity entity) {

        VwCorreoNoValido correoNoValido = UIToModel(entity);

        correoNoValido = correoNoValidoService.getCorreoNoValido(correoNoValido);
        if (ParamUtils.isNotNull(correoNoValido.getCorreoNoValidoControl()))
        {
            correoNoValido = correoNoValidoService.updateCorreoNoValido(correoNoValido, Boolean.parseBoolean(entity.get("modificado")));
        }
        else
        {
            correoNoValido = correoNoValidoService.insertCorreoNoValido(correoNoValido, Boolean.parseBoolean(entity.get("modificado")));
        }

        return modelToUI(correoNoValido);
    }

    private VwCorreoNoValido UIToModel(UIEntity entity) {
        return entity.toModel(VwCorreoNoValido.class);
    }
}