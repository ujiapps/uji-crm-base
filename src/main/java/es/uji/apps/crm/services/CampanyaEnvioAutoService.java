package es.uji.apps.crm.services;

import java.util.List;

import es.uji.apps.crm.model.*;
import es.uji.apps.crm.model.domains.TipoEnvio;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaEnvioAutoDAO;
import es.uji.apps.crm.model.domains.TipoAfiliacionUsuarioCampanya;

@Service
public class CampanyaEnvioAutoService {
    private TipoEstadoCampanyaService tipoEstadoCampanyaService;
    private CampanyaEnvioAutoDAO campanyaEnvioAutoDAO;
    private CampanyaEnvioModeloService campanyaEnvioModeloService;


    @Autowired
    public CampanyaEnvioAutoService(CampanyaEnvioAutoDAO campanyaEnvioAutoDAO, TipoEstadoCampanyaService tipoEstadoCampanyaService, CampanyaEnvioModeloService campanyaEnvioModeloService) {
        this.tipoEstadoCampanyaService = tipoEstadoCampanyaService;
        this.campanyaEnvioAutoDAO = campanyaEnvioAutoDAO;
        this.campanyaEnvioModeloService = campanyaEnvioModeloService;
    }

    public CampanyaEnvioAuto getById(Long campanyaEnvioAutoId) {
        List<CampanyaEnvioAuto> lista = campanyaEnvioAutoDAO.get(CampanyaEnvioAuto.class, campanyaEnvioAutoId);
        if (lista.size() > 0) {
            return lista.get(0);
        }
        return null;
    }

    public List<CampanyaEnvioAuto> getCampanyaEnviosAutoByCampanyaId(Long campanyaId) {
        return campanyaEnvioAutoDAO.getCampanyaEnviosAutoByCampanyaId(campanyaId);
    }

    public List<CampanyaEnvioAuto> getCampanyaEnvioAutoByCampanyaIdAndTipoId(Long campanyaId, Long tipoId, Long clienteId) {
        return campanyaEnvioAutoDAO.getCampanyaEnvioAutoByCampanyaIdAndTipoId(campanyaId, tipoId, clienteId);
    }

    public void updateCampanyaEnvioAuto(CampanyaEnvioAuto campanyaEnvioAuto) {
        campanyaEnvioAutoDAO.update(campanyaEnvioAuto);
    }

    public void delete(Long campanyaEnvioAutoId) {
        campanyaEnvioAutoDAO.delete(CampanyaEnvioAuto.class, campanyaEnvioAutoId);
    }

    public void addCampanyaEnvioAutoGenericaActo(Campanya campanya) {

        addCampanyaEnvioAutoGenerico(campanya, "PROPUESTO", TipoAfiliacionUsuarioCampanya.PROPUESTO.getTipo());
        addCampanyaEnvioAutoGenerico(campanya, "DEFINITIVO", TipoAfiliacionUsuarioCampanya.DEFINITIVO.getTipo());
        addCampanyaEnvioAutoGenerico(campanya, "BAJA", TipoAfiliacionUsuarioCampanya.BAJAWEB.getTipo());
    }

    private void addCampanyaEnvioAutoGenerico(Campanya campanya, String tipoModelo, String tipoAfiliacion) {

        CampanyaEnvioModelo campanyaEnvioModelo = campanyaEnvioModeloService.getCampanyaEnvioModelo(tipoModelo);
        CampanyaEnvioAuto campanyaEnvioAuto = new CampanyaEnvioAuto();
        campanyaEnvioAuto.setCampanya(campanya);
        String cuerpo = campanyaEnvioModelo.getCuerpo();
        cuerpo = cuerpo.replace("$[formularioAlta]", "https://ujiapps.uji.es/crm/rest/publicacion/" + campanya.getId() + "/hash/$[hash]");
        cuerpo = cuerpo.replace("$[formularioBaja]", "https://ujiapps.uji.es/crm/rest/publicacion/" + campanya.getId() + "/baja/hash/$[hash]");
        campanyaEnvioAuto.setCuerpo(cuerpo);
        campanyaEnvioAuto.setAsunto("MODIFICAR -- " + campanyaEnvioModelo.getAsunto());
        campanyaEnvioAuto.setTipoCorreoEnvio(campanyaEnvioModelo.getTipoCorreoEnvio());
        campanyaEnvioAuto.setDesde(campanyaEnvioModelo.getDesde());
        campanyaEnvioAuto.setBase(campanyaEnvioModelo.getBase());
        campanyaEnvioAuto.setResponder(campanyaEnvioModelo.getResponder());

        TipoEstadoCampanya tipoEstadoCampanyaPropuesto = tipoEstadoCampanyaService.getTipoEstadoCampanyaByAccion(campanya.getId(), tipoAfiliacion);
        campanyaEnvioAuto.setCampanyaEnvioTipo(tipoEstadoCampanyaPropuesto);

        Tipo tipoEnvio = new Tipo();
        tipoEnvio.setId(86L);
        campanyaEnvioAuto.setTipoEnvio(tipoEnvio);

        addCampanyaEnvioAuto(campanyaEnvioAuto);

    }

    public CampanyaEnvioAuto addCampanyaEnvioAuto(CampanyaEnvioAuto campanyaEnvioAuto) {
        return campanyaEnvioAutoDAO.insert(campanyaEnvioAuto);
    }

    public void addCampanyaEnvioAutoGenericaActoConPago(Campanya campanya) {

        addCampanyaEnvioAutoGenerico(campanya, "PROPUESTO-CON-PAGO", TipoAfiliacionUsuarioCampanya.PROPUESTO.getTipo());
        addCampanyaEnvioAutoGenerico(campanya, "DEFINITIVO-CON-PAGO", TipoAfiliacionUsuarioCampanya.DEFINITIVO.getTipo());
        addCampanyaEnvioAutoGenerico(campanya, "BAJA-CON-PAGO", TipoAfiliacionUsuarioCampanya.BAJAWEB.getTipo());
        addCampanyaEnvioAutoGenerico(campanya, "PENDIENTE-CON-PAGO", TipoAfiliacionUsuarioCampanya.PENDIENTE.getTipo());


    }

    public void autoguardadoCampanyaEnvioAuto(Long id, String asunto, String cuerpo) {
        CampanyaEnvioAuto campanyaEnvioAuto = getById(id);
        campanyaEnvioAuto.setCuerpo(cuerpo);
        campanyaEnvioAuto.setAsunto(asunto);
        campanyaEnvioAutoDAO.update(campanyaEnvioAuto);
    }

    public CampanyaEnvioAuto autoguardadoCampanyaEnvioAuto(String asunto, String cuerpo, Long campanyaId, Long campanyaEnvioTipoId, Long tipoEnvioId) {
        CampanyaEnvioAuto campanyaEnvioAuto = new CampanyaEnvioAuto();
        campanyaEnvioAuto.setCuerpo(cuerpo);
        campanyaEnvioAuto.setAsunto((ParamUtils.isNotNull(asunto)) ? asunto : "Autoguardado");
        Campanya campanya = new Campanya();
        campanya.setId(campanyaId);
        campanyaEnvioAuto.setCampanya(campanya);
        TipoEstadoCampanya tipoEstadoCampanya = new TipoEstadoCampanya();
        tipoEstadoCampanya.setId(ParamUtils.isNotNull(campanyaEnvioTipoId) ? campanyaEnvioTipoId : 13290200L);
        campanyaEnvioAuto.setCampanyaEnvioTipo(tipoEstadoCampanya);
        Tipo tipoEnvio = new Tipo();
        tipoEnvio.setId(ParamUtils.isNotNull(tipoEnvioId) ? tipoEnvioId : 86L);
        campanyaEnvioAuto.setTipoEnvio(tipoEnvio);
        return campanyaEnvioAutoDAO.insert(campanyaEnvioAuto);
    }
}