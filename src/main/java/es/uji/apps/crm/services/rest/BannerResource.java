package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.model.Banner;
import es.uji.apps.crm.services.BannerService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("banner")
public class BannerResource extends CoreBaseService {
    @InjectParam
    private BannerService bannerService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getBanners() {
        ResponseMessage responseMessage = new ResponseMessage();

        try{
            List<Banner> banners = bannerService.getAllBanners();
            responseMessage.setTotalCount(banners.size());
            responseMessage.setData(UIEntity.toUI(banners));
            responseMessage.setSuccess(true);
        }
        catch (Exception e) {
            e.printStackTrace();
            responseMessage.setSuccess(false);
        }

        return responseMessage;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addBanner(UIEntity entity) {
        Banner banner = UIToModel(entity);
        bannerService.addBanner(banner);
        return UIEntity.toUI(banner);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateBanner(@PathParam("id") Long bannerId, UIEntity entity) {
        Banner banner = UIToModel(entity);
        banner.setId(bannerId);
        bannerService.updateBanner(banner);
        return UIEntity.toUI(banner);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteBanner(@PathParam("id") Long bannerId) {
        bannerService.deleteBanner(bannerId);
    }

    private Banner UIToModel(UIEntity entity) {
        return entity.toModel(Banner.class);
    }

}
