package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.crm.model.domains.TipoSolicitudCampo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("tiposolicitudcampo")
public class TipoSolicitudCampoResource extends CoreBaseService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposSolicitudCampo() {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (TipoSolicitudCampo tipoSolicitudCampo : TipoSolicitudCampo.values())
        {
            UIEntity entity = new UIEntity();
            entity.put("id", tipoSolicitudCampo.getId());
            entity.put("nombre", tipoSolicitudCampo.toString());
            listaUI.add(entity);
        }

        return listaUI;
    }

}