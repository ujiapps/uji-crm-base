package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.TipoAccesoDAO;
import es.uji.apps.crm.model.TipoAcceso;

@Service
public class TipoAccesoService {
    private TipoAccesoDAO tipoAccesoDAO;

    @Autowired
    public TipoAccesoService(TipoAccesoDAO tipoAccesoDAO) {
        this.tipoAccesoDAO = tipoAccesoDAO;
    }

    public List<TipoAcceso> getTiposAcceso() {

        return tipoAccesoDAO.getTiposAcceso();
    }

    public TipoAcceso getTipoAccesoById(Long tipoAccesoId) {
        return tipoAccesoDAO.getTipoAccesoById(tipoAccesoId);
    }
}