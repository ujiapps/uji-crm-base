package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.EnvioCriterioEstudioUJIBecaDAO;
import es.uji.apps.crm.model.EnvioCriterio;
import es.uji.apps.crm.model.EnvioCriterioEstudioUJIBeca;
import es.uji.apps.crm.model.TipoBeca;

@Service
public class EnvioCriterioEstudioUJIBecaService {
    private EnvioCriterioEstudioUJIBecaDAO envioCriterioEstudioUJIBecaDAO;

    @Autowired
    public EnvioCriterioEstudioUJIBecaService(EnvioCriterioEstudioUJIBecaDAO envioCriterioEstudioUJIBecaDAO) {
        this.envioCriterioEstudioUJIBecaDAO = envioCriterioEstudioUJIBecaDAO;
    }

    public List<EnvioCriterioEstudioUJIBeca> getEnvioCriterioEstudioUJIBecaByEnvioCriterioId(Long envioCriterioId) {
        return envioCriterioEstudioUJIBecaDAO.getEnvioCriterioEstudioUJIBecaByEnvioCriterioId(envioCriterioId);
    }

    public void deleteEnvioCriteriosEstudioUJIBeca(Long envioCriterioId) {
        envioCriterioEstudioUJIBecaDAO.delete(EnvioCriterioEstudioUJIBeca.class, "envio_criterio_id = " + envioCriterioId);
    }

    public void insertEnvioCriteriosEstudioUJIBeca(List<Long> criteriosEstudioUJIBeca, Long envioCriterioId) {
        for (Long tipoBecaId : criteriosEstudioUJIBeca)
        {
            insertEnvioCriterioEstudioUJIBeca(tipoBecaId, envioCriterioId);
        }
    }

    public void insertEnvioCriterioEstudioUJIBeca(Long tipoBecaId, Long envioCriterioId) {

        EnvioCriterioEstudioUJIBeca envioCriterioEstudioUJIBeca = new EnvioCriterioEstudioUJIBeca();

        TipoBeca tipoBeca = new TipoBeca();
        tipoBeca.setId(tipoBecaId);
        envioCriterioEstudioUJIBeca.setTipoBeca(tipoBeca);

        EnvioCriterio envioCriterio = new EnvioCriterio();
        envioCriterio.setId(envioCriterioId);
        envioCriterioEstudioUJIBeca.setEnvioCriterio(envioCriterio);

        envioCriterioEstudioUJIBecaDAO.insert(envioCriterioEstudioUJIBeca);

    }
}