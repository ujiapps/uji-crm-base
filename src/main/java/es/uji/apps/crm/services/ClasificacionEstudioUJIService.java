package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ClasificacionEstudioDAO;
import es.uji.apps.crm.model.ClasificacionEstudio;

@Service
public class ClasificacionEstudioUJIService {
    private ClasificacionEstudioDAO clasificacionEstudioDAO;

    @Autowired
    public ClasificacionEstudioUJIService(ClasificacionEstudioDAO clasificacionEstudioDAO) {

        this.clasificacionEstudioDAO = clasificacionEstudioDAO;
    }

//    public List<ClasificacionEstudio> getClasificacionEstudios(String tipoEstudio, Paginacion paginacion,
//                                                               String columnaOrden, String tipoOrden)
//    {
//        return clasificacionEstudioDAO.getClasificacionEstudios(tipoEstudio, paginacion, columnaOrden, tipoOrden);
//    }

    public List<ClasificacionEstudio> getClasificacionesEstudios(String columnaOrden, String tipoOrden) {
        return clasificacionEstudioDAO.getClasificacionesEstudios(columnaOrden, tipoOrden);
    }
}