package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.web.template.Template;
import org.springframework.stereotype.Component;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.text.ParseException;

@Component
@Path("publicacion")
public class PublicacionResource extends CoreBaseService {

    @InjectParam
    private PublicacionService publicacionService;


    @Path("formulario")
    public FormularioResource getPlatformItem(
            @InjectParam FormularioResource formularioResource) {
        return formularioResource;
    }

    @GET
    @Path("{campanyaId}")
    @Produces(MediaType.TEXT_HTML)
    public Response getFormularioSin(@PathParam("campanyaId") Long campanyaId,
                                     @CookieParam("uji-lang") String cookieIdioma) throws URISyntaxException {

        return publicacionService.getFormulario(campanyaId, -1L);
    }

    @GET
    @Path("fail")
    public Template getError (@CookieParam("uji-lang") String cookieIdioma) throws ParseException {

        Template template = publicacionService.dameTemplate(null, cookieIdioma);
        return template;

    }

    @GET
    @Path("{campanyaId}/hash/{hash}")
    @Produces(MediaType.TEXT_HTML)
    public Response getFormularioPorHash(@PathParam("campanyaId") Long campanyaId,
                                         @PathParam("hash") String hash,
                                         @CookieParam("uji-lang") String cookieIdioma) throws URISyntaxException {

        return publicacionService.activarSesionPorHash(hash, "publicacionprincipal/" + campanyaId);
    }

    @GET
    @Path("{campanyaId}/baja/hash/{hash}")
    @Produces(MediaType.TEXT_HTML)
    public Response getFormularioBajaPorHash(@PathParam("campanyaId") Long campanyaId,
                                         @PathParam("hash") String hash,
                                         @CookieParam("uji-lang") String cookieIdioma) throws URISyntaxException {

        return publicacionService.activarSesionPorHash(hash, "publicacionprincipal/" + campanyaId + "/baja");
    }
}
