package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.exceptions.ErrorEnBorradoDeRegistroException;
import es.uji.apps.crm.model.Programa;
import es.uji.apps.crm.model.Servicio;
import es.uji.apps.crm.model.TipoAcceso;
import es.uji.apps.crm.services.CampanyaClienteService;
import es.uji.apps.crm.services.ClienteItemService;
import es.uji.apps.crm.services.ClienteService;
import es.uji.apps.crm.services.ProgramaService;
import es.uji.apps.crm.utils.TreeRowset;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("programa")
public class ProgramaResource extends CoreBaseService {
    @InjectParam
    private ProgramaService programaService;
    @InjectParam
    private ClienteService clienteService;
    @InjectParam
    private ClienteItemService clienteItemService;
    @InjectParam
    private CampanyaClienteService campanyaClienteService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getProgramas(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Programa> lista = programaService.getProgramas(connectedUserId);
        return modelToUI(lista, idioma);
    }

    private List<UIEntity> modelToUI(List<Programa> programas, String idioma) {

        List<UIEntity> UIEntities = new ArrayList<>();

        for (Programa programa : programas)
        {
            UIEntities.add(modelToUI(programa, idioma));
        }

        return UIEntities;

    }

    private UIEntity modelToUI(Programa programa, String idioma) {
        UIEntity entity = UIEntity.toUI(programa);

        // Arreglar posteriormente cuando la UIEntity corriga los booleans
        entity.put("visible", programa.getVisible());

        Programa fetchPrograma = programaService.getProgramaById(programa.getId());

        entity.put("tipoAccesoNombre", fetchPrograma.getTipoAcceso().getNombre());
        entity.put("servicioNombre", fetchPrograma.getServicio().getNombre());
        entity.put("nombre", programa.getNombre(idioma));
        return entity;
    }

    @GET
    @Path("admin/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getProgramasAdmin(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Programa> lista = programaService.getProgramasAdmin(connectedUserId);

        return modelToUI(lista, idioma);
    }

    @GET
    @Path("user/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getProgramasUser(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Programa> lista = programaService.getProgramasUser(connectedUserId);

        return modelToUI(lista, idioma);
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getProgramasByServicio(@PathParam("id") Long servicioId, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        return modelToUI(programaService.getProgramasByServicioId(servicioId), idioma);
    }

    @GET
    @Path("cliente/{id}/tree/root/")
    @Produces(MediaType.APPLICATION_JSON)
    public TreeRowset getProgramasTree(@PathParam("id") Long clienteId) {
        // Cliente cliente = clienteService.getClienteById(clienteId);
        List<Programa> lista = programaService.getProgramasYGrupos(clienteId);

        return programaService.creaArbolSinChecked(lista);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addPrograma(UIEntity entity, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        Programa programa = UIToModel(entity);
        programaService.addPrograma(programa);
        return modelToUI(programa, idioma);
    }

    private Programa UIToModel(UIEntity entity) {
        Programa programa = entity.toModel(Programa.class);

        TipoAcceso tipoAcceso = new TipoAcceso();
        tipoAcceso.setId(Long.parseLong(entity.get("tipoAccesoId")));
        programa.setTipoAcceso(tipoAcceso);

        Servicio servicio = new Servicio();
        servicio.setId(Long.parseLong(entity.get("servicioId")));
        programa.setServicio(servicio);

        return programa;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updatePrograma(@PathParam("id") Long programaId, UIEntity entity, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        Programa programa = UIToModel(entity);
        programaService.updatePrograma(programa);
        return modelToUI(programa, idioma);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long programaId) throws ErrorEnBorradoDeRegistroException {
        programaService.delete(programaId);
    }

}