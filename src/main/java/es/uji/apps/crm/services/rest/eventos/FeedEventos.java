package es.uji.apps.crm.services.rest.eventos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.w3c.dom.Document;

import com.sun.syndication.feed.synd.SyndEnclosure;
import com.sun.syndication.feed.synd.SyndEnclosureImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedOutput;

import es.uji.apps.crm.services.rest.eventos.rss.ContenidoRSS;
import es.uji.apps.crm.services.rest.eventos.rss.Enclosure;
import es.uji.apps.crm.services.rest.eventos.rss.FeedRSS;
import es.uji.apps.crm.services.rest.eventos.rss.MetadataEntry;
import es.uji.apps.crm.services.rest.eventos.rss.MetadataEntryImplCRM;
import es.uji.apps.crm.services.rest.eventos.rss.ValorAtributo;
import es.uji.commons.rest.ParamUtils;

public class FeedEventos {
    private SyndFeed feed;

    public FeedEventos() {

        feed = new SyndFeedImpl();

        feed.getModules().add(new MetadataEntryImplCRM());

        feed.setFeedType("rss_2.0");
        feed.setTitle("Universitat Jaume I CRM");
        feed.setLink("https://www.uji.es/crm/");
        feed.setLanguage("es-es");
        feed.setCopyright("(c) 2013 Universitat Jaume I");
        feed.setDescription("Canal RSS Universitat Jaume I CRM");
    }

    public void addEntries(FeedRSS contenidosRSS) throws ParseException {
        for (Map.Entry<String, ContenidoRSS> contenidoRSS : contenidosRSS.entrySet())
        {
            addEntry(contenidoRSS.getValue());
        }
    }

    public void addEntry(ContenidoRSS contenidoRSS) throws ParseException {
        SyndEntry entry = new SyndEntryImpl();

        addEntryFields(contenidoRSS, entry);
        addMetadataFields(contenidoRSS, entry);
        addGeneralFields(contenidoRSS, entry);
        addEnclosures(contenidoRSS, entry);

        feed.getEntries().add(entry);
    }

    private void addGeneralFields(ContenidoRSS contenidoRSS, SyndEntry entry) {
        MetadataEntry generalEntry = new MetadataEntryImplCRM();

        Map<String, ValorAtributo> fields = new HashMap<String, ValorAtributo>();

        fields.put("contenidoId", getValorAtributoGeneral(String.valueOf(contenidoRSS.getContenidoId())));
        fields.put("titulo", getValorAtributoGeneral(contenidoRSS.getTitulo()));
        fields.put("resumen", getValorAtributoGeneral(contenidoRSS.getResumen()));
        fields.put("contenido", getValorAtributoGeneral(contenidoRSS.getContenido()));
        fields.put("duracio", getValorAtributoGeneral(contenidoRSS.getDuracion()));
        fields.put("apertura", getValorAtributoGeneral(contenidoRSS.getHoraApertura()));

        generalEntry.setFields(fields);

        entry.getModules().add(generalEntry);

    }

    private ValorAtributo getValorAtributoGeneral(String valor) {
        return new ValorAtributo(valor, MetadataEntry.PREFIX_CRM_GENERAL);
    }

    private void addMetadataFields(ContenidoRSS contenidoRSS, SyndEntry entry) {
        MetadataEntry metadatosEntry = new MetadataEntryImplCRM();

        if (ParamUtils.isNotNull(contenidoRSS.getMetadatos()))
        {
            for (Metadato metadato : contenidoRSS.getMetadatos())
            {
                metadatosEntry.addField(metadato.getClave(),
                        getValorAtributoMetadato(metadato.getValor()));
            }
        }

        entry.getModules().add(metadatosEntry);
    }

    private ValorAtributo getValorAtributoMetadato(String valor) {
        return new ValorAtributo(valor, MetadataEntry.PREFIX_CRM_METADATO);
    }

    private void addEnclosures(ContenidoRSS contenidoRSS, SyndEntry entry) {
        if (contenidoRSS.getEnclosures() == null)
        {
            return;
        }

        List<SyndEnclosure> enclosureList = new ArrayList<SyndEnclosure>();

        for (Enclosure enclosure : contenidoRSS.getEnclosures())
        {
            SyndEnclosure syndEnclosure = new SyndEnclosureImpl();
            syndEnclosure.setType(enclosure.getType());
            syndEnclosure.setUrl(enclosure.getUrl());

            enclosureList.add(syndEnclosure);
        }

        entry.setEnclosures(enclosureList);
    }

    private void addEntryFields(ContenidoRSS contenidoRSS, SyndEntry entry) throws ParseException {

        SimpleDateFormat formateadorFecha = new SimpleDateFormat("yyyy-MM-ddHH:mm");
        formateadorFecha.setTimeZone(TimeZone.getTimeZone("Europa/Madrid"));
        entry.setPublishedDate(formateadorFecha.parse(contenidoRSS.getFecha().concat(contenidoRSS.getHora())));
    }

    public Document getXML() throws FeedException {
        SyndFeedOutput output = new SyndFeedOutput();
        return output.outputW3CDom(feed);
    }
}