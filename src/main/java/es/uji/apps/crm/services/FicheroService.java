package es.uji.apps.crm.services;

import es.uji.apps.crm.model.PersonaSimple;
import es.uji.commons.rest.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaClienteArchivo;
import es.uji.commons.rest.ParamUtils;

import java.util.Arrays;
import java.util.List;

@Service
public class FicheroService {

    private PersonaService personaService;
    private CampanyaService campanyaService;
    private ClienteService clienteService;
    private ClienteGeneralService clienteGeneralService;
    private ClienteItemService clienteItemService;
    private ClienteEstudioService clienteEstudioService;
    private ClienteDatoService clienteDatoService;
    private CampanyaClienteService campanyaClienteService;
    private TipoEstadoCampanyaService tipoEstadoCampanyaService;
    private CampanyaClienteArchivoService campanyaClienteArchivoService;

    @Autowired
    public FicheroService(PersonaService personaService, CampanyaService campanyaService, ClienteService clienteService, ClienteGeneralService clienteGeneralService,
                          ClienteItemService clienteItemService, ClienteEstudioService clienteEstudioService, ClienteDatoService clienteDatoService, CampanyaClienteService campanyaClienteService,
                          TipoEstadoCampanyaService tipoEstadoCampanyaService, CampanyaClienteArchivoService campanyaClienteArchivoService) {

        this.personaService = personaService;
        this.campanyaService = campanyaService;
        this.clienteService = clienteService;
        this.clienteGeneralService = clienteGeneralService;
        this.clienteItemService = clienteItemService;
        this.clienteEstudioService = clienteEstudioService;
        this.clienteDatoService = clienteDatoService;
        this.campanyaClienteService = campanyaClienteService;
        this.tipoEstadoCampanyaService = tipoEstadoCampanyaService;
        this.campanyaClienteArchivoService = campanyaClienteArchivoService;
    }

    public ResponseMessage addFicheroPersonas(String fichero, Long campanyaId) {

        Campanya campanya = campanyaService.getCampanyaById(campanyaId);

        List<String> lineas = Arrays.asList(fichero.split("\\n"));
        lineas.stream().filter(l -> l.length()>2).forEach(linea -> {
            String[] campos = linea.replaceAll("\\r", "").split(";");
            if (campos.length >0 && ParamUtils.isNotNull(campos[0])) {
                PersonaSimple persona = personaService.getPersonaSimpleByIdentificacion(campos[0]);

                CampanyaClienteArchivo campanyaClienteArchivo = new CampanyaClienteArchivo();
                campanyaClienteArchivo.setCampanya(campanya);
                campanyaClienteArchivo.setAnyadir(false);
                campanyaClienteArchivo.setCreadaFicha(Boolean.FALSE);

                campanyaClienteArchivo.setIdentificacion(campos[0]);
                if (campos.length> 1 &&  ParamUtils.isNotNull(campos[1]))
                    campanyaClienteArchivo.setCorreo(campos[1]);
                if (campos.length> 2 &&  ParamUtils.isNotNull(campos[2]))
                    campanyaClienteArchivo.setNombre(campos[2]);
                if (campos.length> 3 &&  ParamUtils.isNotNull(campos[3]))
                    campanyaClienteArchivo.setApellido1(campos[3]);
                if (campos.length> 4 &&  ParamUtils.isNotNull(campos[4]))
                    campanyaClienteArchivo.setApellido2(campos[4]);

                if (ParamUtils.isNotNull(persona)) {
                    campanyaClienteArchivo.setPersona(persona);
                }
                campanyaClienteArchivoService.addCampanyaClienteArchivoService(campanyaClienteArchivo);
            }
        });

        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(true);
        return responseMessage;
    }

    public void deleteFicheroPersonas(Long campanyaId) {
        campanyaClienteArchivoService.deleteCampanyaClienteArchivoByCampanyaId(campanyaId);
    }

    public void deleteFicheroPersona(Long personaFicheroId) {
        campanyaClienteArchivoService.delete(personaFicheroId);
    }
}