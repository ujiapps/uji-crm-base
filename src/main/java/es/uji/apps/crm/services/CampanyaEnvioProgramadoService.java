package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaEnvioProgramadoDAO;
import es.uji.apps.crm.model.CampanyaEnvioAuto;
import es.uji.apps.crm.model.CampanyaEnvioProgramado;

@Service
public class CampanyaEnvioProgramadoService {
    @Autowired
    private CampanyaEnvioProgramadoDAO campanyaEnvioProgramadoDAO;

    public List<CampanyaEnvioProgramado> getCampanyaEnvioProgramadoByEnvio(CampanyaEnvioAuto envio) {
        return campanyaEnvioProgramadoDAO.getEnvioProgramadoByEnvio(envio);
    }

    public CampanyaEnvioProgramado updateCampanyaEnvioProgramado(CampanyaEnvioProgramado campanyaEnvioProgramado) {
        return campanyaEnvioProgramadoDAO.update(campanyaEnvioProgramado);
    }

    public CampanyaEnvioProgramado addCampanyaEnvioProgramado(CampanyaEnvioProgramado campanyaEnvioProgramado) {
        return campanyaEnvioProgramadoDAO.insert(campanyaEnvioProgramado);
    }

    public void deleteCampanyaEnvioProgramado(Long envioId) {
        campanyaEnvioProgramadoDAO.delete(CampanyaEnvioProgramado.class, envioId);
    }
}