package es.uji.apps.crm.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaClienteItemDAO;
import es.uji.apps.crm.dao.ClienteCirculoDAO;
import es.uji.apps.crm.model.ClienteItem;
import es.uji.commons.rest.UIEntity;

@Service
public class ClienteSuscripcionesService {

    private ClienteCirculoDAO clienteCirculoDAO;
    private ClienteItemService clienteItemService;
    private CampanyaClienteItemDAO campanyaClienteItemDAO;

    @Autowired
    public ClienteSuscripcionesService(ClienteCirculoDAO clienteCirculoDAO, ClienteItemService clienteItemService, CampanyaClienteItemDAO campanyaClienteItemDAO) {
        this.clienteCirculoDAO = clienteCirculoDAO;
        this.clienteItemService = clienteItemService;
        this.campanyaClienteItemDAO = campanyaClienteItemDAO;
    }

    public List<UIEntity> getListaItemsUnificada(Long clienteId) {
        List<UIEntity> res = unificaListas(getItemsClientesSuscripcionesByCliente(clienteId), modelToUIItem(clienteItemService.getItemsByClienteId(clienteId)));
        return res;
    }

    public List<UIEntity> getItemsClientesSuscripcionesByCliente(Long clienteId) {
        List<UIEntity> lista = clienteCirculoDAO.getItemsClientesSuscripcionesByCliente(clienteId);
        lista.addAll(campanyaClienteItemDAO.getCampanyaClienteItemByCliente(clienteId));
        return lista;
    }

    private List<UIEntity> unificaListas(List<UIEntity> listaCirculos, List<UIEntity> listaItems) {
        listaItems.addAll(listaCirculos.stream().filter(r -> !listaItems.stream().map(s -> s.get("itemId")).collect(Collectors.toList()).contains(r.get("itemId"))).collect(Collectors.toList()));
        return listaItems;
    }

//    private UIEntity modelToUI(ClienteCirculo clienteCirculo) {
//
//        UIEntity entity = UIEntity.toUI(clienteCirculo);
//        entity.put("nombre", clienteCirculo.getItem().getNombreCa());
//        entity.put("tipo", "AUTO");
//
//        if (clienteCirculo.getCliente() != null) {
//            entity.put("clienteId", clienteCirculo.getCliente().getId());
//        }
//        if (clienteCirculo.getItem() != null && clienteCirculo.getItem().getGrupo() != null) {
//            entity.put("grupoId", clienteCirculo.getItem().getGrupo().getId());
//            entity.put("grupoNombre", clienteCirculo.getItem().getGrupo().getNombreCa());
//        }
//        return entity;
//    }
//
//    private List<UIEntity> modelToUI(List<ClienteCirculo> lista) {
//        List<UIEntity> listaEntity = new ArrayList<>();
//
//        for (ClienteCirculo clienteCirculo : lista) {
//            listaEntity.add(modelToUI(clienteCirculo));
//        }
//        return listaEntity;
//    }

    public List<UIEntity> modelToUIItem(List<ClienteItem> lista) {
        List<UIEntity> listaEntity = new ArrayList<>();

        for (ClienteItem clienteItem : lista)
        {
            listaEntity.add(modelToUI(clienteItem));
        }
        return listaEntity;
    }

    private UIEntity modelToUI(ClienteItem clienteItem) {

        UIEntity entity = UIEntity.toUI(clienteItem);
        entity.put("nombre", clienteItem.getItem().getNombreCa());
        entity.put("tipo", "MANUAL");

        if (clienteItem.getCliente() != null)
        {
            entity.put("clienteId", clienteItem.getCliente().getId());
        }
        if (clienteItem.getItem() != null && clienteItem.getItem().getGrupo() != null)
        {
            entity.put("grupoId", clienteItem.getItem().getGrupo().getId());
            entity.put("grupoNombre", clienteItem.getItem().getGrupo().getNombreCa());
        }
        return entity;
    }
}