package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CuentasCorporativasDAO;
import es.uji.apps.crm.model.CuentaCorportativa;

@Service
public class CuentasCorporativasService {
    private CuentasCorporativasDAO cuentasCorporativasDAO;

    @Autowired
    public CuentasCorporativasService(CuentasCorporativasDAO cuentasCorporativasDAO) {
        this.cuentasCorporativasDAO = cuentasCorporativasDAO;
    }


    public List<CuentaCorportativa> getCuentasCorporativasByUserId(Long userId) {
        return cuentasCorporativasDAO.getCuentasCorporativasByUserId(userId);
    }

    public List<CuentaCorportativa> getCuentasCorporativasActivasByUserId(Long userId){
        return cuentasCorporativasDAO.getCuentasCorporativasActivasByUserId(userId);
    }
}