package es.uji.apps.crm.services;

import java.util.List;

import es.uji.apps.crm.ui.OpcionUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.TipoEstudioDAO;
import es.uji.apps.crm.model.TipoEstudio;
import es.uji.commons.rest.UIEntity;

@Service
public class ClasificacionEstudioNoUJIService {

    private TipoEstudioDAO tipoEstudioDAO;

    @Autowired
    public ClasificacionEstudioNoUJIService(TipoEstudioDAO tipoEstudioDAO) {
        this.tipoEstudioDAO = tipoEstudioDAO;
    }

    public List<TipoEstudio> getClasificacionEstudios() {
        return tipoEstudioDAO.getTiposEstudio();
    }

    public TipoEstudio addClasificacionEstudiosNoUJI(UIEntity entity) {
        TipoEstudio tipoEstudio = entity.toModel(TipoEstudio.class);
        return tipoEstudioDAO.insert(tipoEstudio);
    }

    public void deleteClasificacionEstudioNoUji(Long clasificacionEstudioId) {
        tipoEstudioDAO.delete(TipoEstudio.class, clasificacionEstudioId);
    }

    public TipoEstudio updateClasificacionEstudiosNoUJI(UIEntity entity) {
        TipoEstudio tipoEstudio = entity.toModel(TipoEstudio.class);
        return tipoEstudioDAO.update(tipoEstudio);
    }

    public List<OpcionUI> getClasificacionEstudiosPorTipo(Long tipoId, String idioma) {
        return tipoEstudioDAO.getClasificacionEstudiosPorTipo(tipoId, idioma);
    }
}