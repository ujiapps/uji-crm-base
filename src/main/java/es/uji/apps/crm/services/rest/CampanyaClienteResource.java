package es.uji.apps.crm.services.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.model.*;
import es.uji.apps.crm.model.domains.TipoCliente;
import es.uji.apps.crm.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Path("campanyacliente")
public class CampanyaClienteResource extends CoreBaseService {
    private static final Logger log = LoggerFactory.getLogger(CampanyaClienteResource.class);
    @InjectParam
    private CampanyaClienteService campanyaClienteService;
    @InjectParam
    private ClienteService clienteService;
    @InjectParam
    private CampanyaService campanyaService;
    @InjectParam
    private ClienteDatoTipoService clienteDatoTipoService;
    @InjectParam
    private TipoEstadoCampanyaService tipoEstadoCampanyaService;
    @InjectParam
    private ClienteEstadoCampanyaMotivoService clienteEstadoCampanyaMotivoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getCampanyaClientes(@QueryParam("campanyaId") Long campanyaId,
                                               @QueryParam("clienteId") Long clienteId,
                                               @QueryParam("campanyaClienteTipoId") Long campanyaClienteTipoId,
                                               @QueryParam("busqueda") String busqueda,
                                               @QueryParam("etiqueta") String etiqueta,
                                               @QueryParam("seguimientoTipo") String seguimientoTipos,
                                               @QueryParam("seguimientoTipoOpcion") Long seguimientoTipoOpcion,
                                               @QueryParam("items") String itemsString,
                                               @QueryParam("itemsOpcion") Long itemsOpcion,
                                               @QueryParam("fechaEstado") String fechaEstado,
                                               @QueryParam("start") @DefaultValue("0") Long start,
                                               @QueryParam("limit") @DefaultValue("25") Long limit,
                                               @QueryParam("filter") @DefaultValue("[]") String filterJson,
                                               @QueryParam("sort") @DefaultValue("[]") String sortJson) throws IOException {

        List<Map<String, String>> searchMap;
        FiltroCliente filtro = new FiltroCliente();
        if (!filterJson.equals("[]")) {
            searchMap = new ObjectMapper().readValue(filterJson, new TypeReference<List<Map<String, String>>>(){});
            if (searchMap != null) {
                try {
                    searchMap.forEach(item -> {
                        String property = item.get("property");
                        String value = item.get("value");

                        if (property.equals("campanyaId")) {
                            filtro.setCampanyaId(value != null ? Long.parseLong(value) : null);
                        }
                        if (property.equals("clienteId")) {
                            filtro.setClienteId(value != null ? Long.parseLong(value) : null);
                        }
                        if (property.equals("campanyaClienteTipoId")) {
                            filtro.setCampanyaClienteTipoId(value != null ? Long.parseLong(value) : null);
                        }
                        if (property.equals("busqueda")) {
                            filtro.setBusquedaGenerica(value);
                        }
                        if (property.equals("etiqueta")) {
                            filtro.setEtiqueta(value);
                        }
                        if (property.equals("items")) {
                            filtro.setItems(value);
                        }
                        if (property.equals("itemsOpcion")) {
                            filtro.setItemsOpcion(value != null ? Long.parseLong(value) : null);
                        }
                        if (property.equals("seguimientoTipo")) {
                            filtro.setSeguimientoTipos(value);
                        }
                        if (property.equals("seguimientoTipoOpcion")) {
                            filtro.setSeguimientoTipoOpcion(value != null ? Long.parseLong(value) : null);
                        }
                    });
                } catch (Exception e) {
                    log.error("Error getCampanyaClientes", e);
                }
            }
        } else {
            filtro.setCampanyaId(campanyaId);
            filtro.setClienteId(clienteId);
            filtro.setCampanyaClienteTipoId(campanyaClienteTipoId);
            filtro.setBusquedaGenerica(busqueda);
            filtro.setEtiqueta(etiqueta);
            filtro.setItems(itemsString);
            filtro.setItemsOpcion(itemsOpcion);
            filtro.setSeguimientoTipos(seguimientoTipos);
            filtro.setSeguimientoTipoOpcion(seguimientoTipoOpcion);
        }

        ResponseMessage responseMessage = new ResponseMessage();
        try {
            Paginacion paginacion = new Paginacion(start, limit);

            if (sortJson.length() > 2) {
                List<Map<String, String>> sortList = new ObjectMapper().readValue(sortJson, new TypeReference<List<Map<String, String>>>() {
                });
                paginacion.setOrdenarPor(sortList.get(0).get("property"));
                paginacion.setDireccion(sortList.get(0).get("direction"));
            }

            List<CampanyaClienteBusqueda> lista = campanyaClienteService.getCampanyaClientes(filtro,
                    paginacion);
            responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
            responseMessage.setData(modelToUICampanya(lista));
            responseMessage.setSuccess(true);

        } catch (Exception e) {
            log.error("getCampanyaClientes", e);
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    private List<UIEntity> modelToUICampanya(List<CampanyaClienteBusqueda> campanyasCliente) {
        return campanyasCliente.stream().map(this::modelToUICampanya).collect(Collectors.toList());
    }

    private UIEntity modelToUICampanya(CampanyaClienteBusqueda campanyaClienteBusqueda) {
        if (ParamUtils.isNotNull(campanyaClienteBusqueda.getCampanya()))
        {
            CampanyaCliente campanyaCliente = new CampanyaCliente();
            campanyaCliente.setId(campanyaClienteBusqueda.getId());
            campanyaCliente.setCampanyaClienteTipo(campanyaClienteBusqueda.getEstadoCampanya());
            campanyaCliente.setCliente(campanyaClienteBusqueda.getCliente());
            campanyaCliente.setComentariosEstado(campanyaClienteBusqueda.getEstado());

            UIEntity entity = UIEntity.toUI(campanyaCliente);

            if (ParamUtils.isNotNull(campanyaClienteBusqueda.getCliente().getClienteGeneral()))
            {
                entity.put("identificacion", campanyaClienteBusqueda.getCliente().getClienteGeneral().getIdentificacion());
                entity.put("clienteGeneralId", campanyaClienteBusqueda.getCliente().getClienteGeneral().getId());
            }

            if (campanyaClienteBusqueda.getCliente().getTipo().equals(TipoCliente.FISICA.getId()))
            {
                entity.put("clienteNombre", campanyaClienteBusqueda.getCliente().getApellidosNombre());
            }
            else
            {
                entity.put("clienteNombre", campanyaClienteBusqueda.getCliente().getNombre());
            }

            entity.put("campanyaClienteTipoNombre", campanyaClienteBusqueda.getEstadoCampanya().getNombre());

            entity.put("campanyaNombre", campanyaClienteBusqueda.getCampanyaNombre());
            entity.put("numSeguimientos", campanyaClienteBusqueda.getNumSeguimientos());
            entity.put("campanyaId", campanyaClienteBusqueda.getCampanya().getId());
            entity.put("correo", campanyaClienteBusqueda.getCliente().getCorreo());
            entity.put("fechaEstado", campanyaClienteBusqueda.getFechaEstado());
            entity.put("definitivo", campanyaClienteBusqueda.isDefinitivo());

            return entity;
        }
        return null;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getCampanyaClienteById(@PathParam("id") Long campanyaClienteId) {

        CampanyaCliente campanyaCliente = campanyaClienteService.getCampanyaClienteById(campanyaClienteId);
        return modelToUI(campanyaCliente);
    }

    private UIEntity modelToUI(CampanyaCliente campanyaCliente) {
        UIEntity entity = UIEntity.toUI(campanyaCliente);

//        Cliente cliente = clienteService.getClienteById(campanyaCliente.getCliente().getId());

        entity.put("identificacion", campanyaCliente.getCliente().getClienteGeneral().getIdentificacion());
        entity.put("clienteNombre", campanyaCliente.getCliente().getApellidosNombre());
        entity.put("correo", campanyaCliente.getCliente().getCorreo());
        entity.put("campanyaClienteTipoNombre", campanyaCliente.getCampanyaClienteTipo().getNombre());
        entity.put("campanyaNombre", campanyaCliente.getCampanya().getNombre());
        entity.put("clienteGeneralId", campanyaCliente.getCliente().getClienteGeneral().getId());

        ClienteEstadoCampanyaMotivo clienteEstadoCampanyaMotivo = clienteEstadoCampanyaMotivoService.getClienteEstadoMotivoByCampanyaClienteId(campanyaCliente.getId(), campanyaCliente.getCampanyaClienteTipo().getId());
        if (ParamUtils.isNotNull(clienteEstadoCampanyaMotivo))
        {
            entity.put("campanyaClienteMotivoNombre", clienteEstadoCampanyaMotivo.getCampanyaEstadoMotivo().getNombreCa());
            entity.put("campanyaClienteMotivoId", clienteEstadoCampanyaMotivo.getCampanyaEstadoMotivo().getId());
        }
        return entity;
    }

    @GET
    @Path("cliente/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyaClienteByClienteId(@PathParam("id") Long clienteId) {
        List<CampanyaCliente> campanyaCliente = campanyaClienteService
                .getCampanyaClienteByClienteId(clienteId);
        return modelToUI(campanyaCliente);
    }

    private List<UIEntity> modelToUI(List<CampanyaCliente> campanyasCliente) {
        return campanyasCliente.stream().map(this::modelToUI).collect(Collectors.toList());
    }

    @POST
    @Path("campanya/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanyaCliente(@PathParam("id") Long campanyaId, UIEntity entity)
            throws Exception {
        Map<String, List<UIEntity>> obj = entity.getRelations();

        List<UIEntity> listaObjClienteGeneral = obj.get("clienteGeneral");
        List<UIEntity> listaObjCliente = obj.get("cliente");
        List<UIEntity> listaObjDatosExtra = obj.get("datosExtraFormulario");

        CampanyaCliente campanyacliente = campanyaClienteService.addCampanyaClienteFormulario(listaObjClienteGeneral.get(0), listaObjCliente.get(0), listaObjDatosExtra, campanyaId, "dd/MM/yyyy");

        return modelToUI(campanyacliente);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanyaClienteSinFormulario(UIEntity entity) {

        CampanyaCliente campanyacliente = campanyaClienteService.addCampanyaCliente(UIToModel(entity));
        return modelToUI(campanyacliente);
    }

    private CampanyaCliente UIToModel(UIEntity entity) {
        CampanyaCliente campanyaCliente = entity.toModel(CampanyaCliente.class);

        TipoEstadoCampanya campanyaClienteTipo = tipoEstadoCampanyaService.getTipoEstadoCampanyaById(Long.parseLong(entity.get("campanyaClienteTipoId")));
        campanyaCliente.setCampanyaClienteTipo(campanyaClienteTipo);

        Campanya campanya = campanyaService.getCampanyaById(ParamUtils.parseLong(entity
                .get("campanyaId")));
        campanyaCliente.setCampanya(campanya);

        Cliente cliente = clienteService.getClienteById(ParamUtils.parseLong(entity
                .get("clienteId")));
        campanyaCliente.setCliente(cliente);

        return campanyaCliente;
    }

    @POST
    @Path("anyadirgridacampanya")
    public void addGridClientesACampanya(@FormParam("busqueda") String busqueda,
                                         @FormParam("etiqueta") String etiqueta, @FormParam("tipo") String tipo,
                                         @FormParam("items") String itemsString, @FormParam("campanya") Long campanya,
                                         @FormParam("tipoDato") String tipoDato, @FormParam("valorDato") String valor,
                                         @FormParam("correo") String correo, @FormParam("movil") String movil,
                                         @FormParam("postal") String postal,
                                         @FormParam("campanyaTipoEstado") Long campanyaTipoEstado,
                                         @FormParam("programa") Long programa, @FormParam("grupo") Long grupo,
                                         @FormParam("item") Long item, @FormParam("tipoComparacion") Long tipoComparacion,
                                         @FormParam("fecha") String fecha, @FormParam("fechaMax") String fechaMax,
                                         @FormParam("tipoFecha") Long tipoFecha, @FormParam("mes") Long mes,
                                         @FormParam("anyo") Long anyo, @FormParam("anyoMax") Long anyoMax,
                                         @FormParam("mesMax") Long mesMax, @FormParam("campanyaAnyadir") Long campanyaAnyadir,
                                         @FormParam("estudioUJI") Long estudioUJI,
                                         @FormParam("estudioNoUJI") Long estudioNoUJI,
                                         @FormParam("estudioUJIBeca") List<Long> estudioUJIBeca,
                                         @FormParam("nacionalidad") String nacionalidad,
                                         @FormParam("codigoPostal") String codigoPostal,
                                         @FormParam("paisDomicilio") String paisDomicilio,
                                         @FormParam("provinciaDomicilio") Long provinciaDomicilio,
                                         @FormParam("poblacionDomicilio") Long poblacionDomicilio,
                                         @FormParam("estadoAnyadir") Long estadoAnyadir) throws ParseException {

        if (campanyaAnyadir != null && estadoAnyadir != null)
        {
            FiltroCliente filtro = new FiltroCliente();
            filtro.setBusquedaGenerica(busqueda);
            filtro.setEtiqueta(etiqueta);
            filtro.setItems(itemsString);
            filtro.setTipoCliente(tipo);
            filtro.setCampanyaId(campanya);
            filtro.setCampanyaTipoEstado(campanyaTipoEstado);
            filtro.setValorDato(valor);
            filtro.setCorreo(correo);
            filtro.setMovil(movil);
            filtro.setPrograma(programa);
            filtro.setGrupo(grupo);
            filtro.setItem(item);
            filtro.setTipoComparacion(tipoComparacion);
            filtro.setTipoFecha(tipoFecha);
            filtro.setMes(mes);
            filtro.setAnyo(anyo);
            filtro.setMesMax(mesMax);
            filtro.setAnyoMax(anyoMax);
            filtro.setPostal(postal);
            filtro.setNacionalidad(nacionalidad);
            filtro.setCodigoPostal(codigoPostal);
            filtro.setPaisDomicilio(paisDomicilio);
            filtro.setProvinciaDomicilio(provinciaDomicilio);
            filtro.setPoblacionDomiclio(poblacionDomicilio);
            filtro.setEstudioUJI(estudioUJI);
            filtro.setEstudioUJIBeca(estudioUJIBeca);
            filtro.setEstudioNoUJI(estudioNoUJI);

            if (ParamUtils.isNotNull(fecha))
            {
                filtro.setFecha(UtilsService.fechaParse(fecha.substring(0, 10), "yyyy-MM-dd"));
            }

            if (ParamUtils.isNotNull(fechaMax))
            {
                filtro.setFechaMax(UtilsService.fechaParse(fechaMax.substring(0, 10), "yyyy-MM-dd"));
            }

            if (ParamUtils.isNotNull(tipoDato))
            {
                ClienteDatoTipo clienteDatoTipo = clienteDatoTipoService
                        .getClienteDatoTipoById(ParamUtils.parseLong(tipoDato));
                filtro.setTipoDato(clienteDatoTipo);
            }

            if (ParamUtils.isNotNull(busqueda) || ParamUtils.isNotNull(etiqueta)
                    || campanya != null || ParamUtils.isNotNull(tipoDato)
                    || ParamUtils.isNotNull(correo) || ParamUtils.isNotNull(movil)
                    || ParamUtils.isNotNull(programa) || ParamUtils.isNotNull(postal))
            {
                clienteService.addGridClientesACampanya(filtro, campanyaAnyadir, estadoAnyadir);
            }
        }
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateCampanyaCliente(@PathParam("id") Long campanyaClienteId, UIEntity entity) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        CampanyaCliente campanyaCliente = UIToModel(entity);
        campanyaCliente = campanyaClienteService.updateCampanyaCliente(connectedUserId, campanyaCliente, Boolean.TRUE);
        return modelToUI(campanyaCliente);
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateCampanyaCliente(UIEntity entity) throws ParseException, SQLException {
        CampanyaCliente campanyaCliente = campanyaClienteService.updateCampanyaCliente(entity);
        return modelToUI(campanyaClienteService.getCampanyaClienteById(campanyaCliente.getId()));
    }

    @PUT
    @Path("{id}/cliente/{clienteId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity changeClienteInCampanyaCliente(@PathParam("id") Long campanyaClienteId,
                                                   @PathParam("clienteId") Long clienteId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        CampanyaCliente campanyaCliente = campanyaClienteService.getCampanyaClienteById(campanyaClienteId);

        Cliente cliente = clienteService.getClienteById(clienteId);
        campanyaCliente.setCliente(cliente);
        campanyaClienteService.updateCampanyaCliente(connectedUserId, campanyaCliente, Boolean.TRUE);

        return modelToUI(campanyaCliente);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long campanyaClienteId) {
        campanyaClienteService.deleteCampanyaCliente(campanyaClienteId);
    }

    @GET
    @Path("excel")
    @Produces("application/vnd.ms-excel")
    public Response getExcelCampanyaCliente(@QueryParam("campanyaId") Long campanyaId,
                                            @QueryParam("campanyaClienteTipoId") Long campanyaClienteTipoId,
                                            @QueryParam("busqueda") String busqueda,
                                            @QueryParam("etiqueta") String etiqueta,
                                            @QueryParam("items") String itemsString,
                                            @QueryParam("itemsOpcion") Long itemsOpcion,
                                            @QueryParam("seguimientoTipo") String seguimientoTipos,
                                            @QueryParam("seguimientoTipoOpcion") Long seguimientoTipoOpcion) throws Exception {
        FiltroCliente filtro = new FiltroCliente();
        filtro.setCampanyaId(campanyaId);
        filtro.setCampanyaClienteTipoId(campanyaClienteTipoId);
        filtro.setBusquedaGenerica(busqueda);
        filtro.setEtiqueta(etiqueta);
        filtro.setItems(itemsString);
        filtro.setItemsOpcion(itemsOpcion);
        filtro.setSeguimientoTipos(seguimientoTipos);
        filtro.setSeguimientoTipoOpcion(seguimientoTipoOpcion);

        ByteArrayOutputStream ostream = campanyaClienteService.getExcelCampanyaCliente(filtro);
        if (ostream != null)
            return Response.ok(ostream.toByteArray(), MediaType.APPLICATION_OCTET_STREAM).header("content-disposition", "attachment; filename = datos_clientes_campanyas.xls").build();
        throw new Exception("Sense dades");
    }
}