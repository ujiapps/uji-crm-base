package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.services.SubVinculoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("subvinculo")
public class SubVinculoResource extends CoreBaseService {
    @InjectParam
    private SubVinculoService subVinculoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSubvinculos() {
        return UIEntity.toUI(subVinculoService.getSubVinculos());
    }

    @GET
    @Path("programa")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSubVinculosByPrograma(@QueryParam("programaId") Long programaId) {
        return UIEntity.toUI(subVinculoService.getSubVinculosByPrograma(programaId));
    }

}