package es.uji.apps.crm.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.TipoReciboDAO;
import es.uji.apps.crm.model.Emisora;

@Service
public class TipoReciboService {

    private TipoReciboDAO tipoReciboDAO;

    @Autowired
    public TipoReciboService(TipoReciboDAO tipoReciboDAO) {
        this.tipoReciboDAO = tipoReciboDAO;
    }


    public Long getTipoReciboByEmisoraAndTipo(Emisora emisora, String tipoRecibo) {
        return tipoReciboDAO.getTipoReciboByEmisoraAndTipo(emisora, tipoRecibo);
    }

    public Long getTipoReciboByEmisora(Emisora emisora) {
        return tipoReciboDAO.getTipoReciboByEmisora(emisora);
    }
}