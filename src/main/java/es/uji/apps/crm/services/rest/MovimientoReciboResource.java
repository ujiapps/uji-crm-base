package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.MovimientoRecibo;
import es.uji.apps.crm.model.Recibo;
import es.uji.apps.crm.services.MovimientoReciboService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("movimientorecibo")
public class MovimientoReciboResource extends CoreBaseService {

    @InjectParam
    private MovimientoReciboService movimientoReciboService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("recibo/{id}")
    public List<UIEntity> getMovimientosReciboByReciboId(@PathParam("id") Long reciboId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<MovimientoRecibo> lista = movimientoReciboService.getMovimientosReciboByReciboId(reciboId);
        return modelToUI(lista);
    }

    private List<UIEntity> modelToUI(List<MovimientoRecibo> movimientosRecibo) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (MovimientoRecibo movimientoRecibo : movimientosRecibo)
        {
            listaUI.add(modelToUI(movimientoRecibo));
        }
        return listaUI;
    }

    private UIEntity modelToUI(MovimientoRecibo movimientoRecibo) {
        UIEntity entity = UIEntity.toUI(movimientoRecibo);
        return entity;
    }

    private MovimientoRecibo UIToModel(UIEntity entity) {

        MovimientoRecibo movimientoRecibo = entity.toModel(MovimientoRecibo.class);

        Recibo recibo = new Recibo();
        recibo.setId(ParamUtils.parseLong(entity.get("reciboId")));
        movimientoRecibo.setRecibo(recibo);


        return movimientoRecibo;
    }

}