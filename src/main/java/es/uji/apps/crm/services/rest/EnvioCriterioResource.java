package es.uji.apps.crm.services.rest;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.crm.exceptions.BusquedaVaciaException;
import es.uji.apps.crm.exceptions.SoloSeAdmitenFicherosExcelException;
import es.uji.apps.crm.model.EnvioCriterio;
import es.uji.apps.crm.model.EnvioCriterioCampanyaEstado;
import es.uji.apps.crm.model.EnvioCriterioCirculo;
import es.uji.apps.crm.model.EnvioCriterioEstudioNoUJI;
import es.uji.apps.crm.model.EnvioCriterioEstudioUJI;
import es.uji.apps.crm.model.EnvioCriterioEstudioUJIBeca;
import es.uji.apps.crm.model.domains.TipoFicheroExcel;
import es.uji.apps.crm.services.ClienteDatoOpcionService;
import es.uji.apps.crm.services.EnvioCriterioCampanyaEstadosService;
import es.uji.apps.crm.services.EnvioCriterioCirculoService;
import es.uji.apps.crm.services.EnvioCriterioEstudioNoUJIService;
import es.uji.apps.crm.services.EnvioCriterioEstudioUJIBecaService;
import es.uji.apps.crm.services.EnvioCriterioEstudioUJIService;
import es.uji.apps.crm.services.EnvioCriterioService;
import es.uji.apps.crm.services.TipoService;
import es.uji.apps.crm.ui.EnvioCriterioUI;
import es.uji.commons.rest.*;
import es.uji.commons.sso.AccessManager;

@Path("enviocriterio")
public class EnvioCriterioResource extends CoreBaseService {
    @InjectParam
    private EnvioCriterioService envioCriterioService;
    @InjectParam
    private TipoService tipoService;
    @InjectParam
    private ClienteDatoOpcionService clienteDatoOpcionService;
    @InjectParam
    private EnvioCriterioCirculoService envioCriterioCirculoService;
    @InjectParam
    private EnvioCriterioCampanyaEstadosService envioCriterioCampanyaEstadosService;
    @InjectParam
    private EnvioCriterioEstudioUJIService envioCriterioEstudioUJIService;
    @InjectParam
    private EnvioCriterioEstudioUJIBecaService envioCriterioEstudioUJIBecaService;
    @InjectParam
    private EnvioCriterioEstudioNoUJIService envioCriterioEstudioNoUJIService;


    @GET
    @Path("envio/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getEnvioCriterioByEnvioId(@PathParam("id") Long envioId) {

        EnvioCriterio envioCriterio = envioCriterioService.getEnvioCriterioByEnvioId(envioId);
        return modelToUI(envioCriterio);
    }

    private UIEntity modelToUI(EnvioCriterio envioCriterio) {
        UIEntity entity = UIEntity.toUI(envioCriterio);

        List<EnvioCriterioCirculo> envioCriterioCirculos = envioCriterioCirculoService.getEnvioCriterioCirculosByEnvioCriterioId(envioCriterio.getId());
        entity.put("criterioCirculoId", envioCriterioCirculos.stream().map(circulo -> circulo.getCirculo().getId().toString()).collect(Collectors.toList()));

        List<EnvioCriterioEstudioUJI> envioCriteriosEstudioUJI = envioCriterioEstudioUJIService.getEnvioCriterioEstudioUJIByEnvioCriterioId(envioCriterio.getId());
        entity.put("criterioEstudioUJIId", envioCriteriosEstudioUJI.stream().map(estudioUJI -> estudioUJI.getEstudioUJI().getId().toString()).collect(Collectors.toList()));

        List<EnvioCriterioEstudioUJIBeca> envioCriteriosEstudioUJIBeca = envioCriterioEstudioUJIBecaService.getEnvioCriterioEstudioUJIBecaByEnvioCriterioId(envioCriterio.getId());
        entity.put("criterioEstudioUJIBeca", envioCriteriosEstudioUJIBeca.stream().map(estudioUJIBeca -> estudioUJIBeca.getTipoBeca().getId().toString()).collect(Collectors.toList()));

        if (envioCriteriosEstudioUJI.size() > 0)
        {
            entity.put("anyoFinalizacionEstudioUJIInicio", envioCriteriosEstudioUJI.get(0).getAnyoFinalizacionEstudioUJIInicio());
            entity.put("anyoFinalizacionEstudioUJIFin", envioCriteriosEstudioUJI.get(0).getAnyoFinalizacionEstudioUJIFin());
        }

        List<EnvioCriterioEstudioNoUJI> envioCriteriosEstudioNoUJI = envioCriterioEstudioNoUJIService.getEnvioCriterioEstudioNoUJIByEnvioCriterioId(envioCriterio.getId());
        entity.put("criterioEstudioNoUJIId", envioCriteriosEstudioNoUJI.stream().map(estudioNoUJI -> estudioNoUJI.getEstudioNoUJI().getId().toString()).collect(Collectors.toList()));

        if (envioCriteriosEstudioNoUJI.size() > 0)
        {
            entity.put("anyoFinalizacionEstudioNoUJIInicio", envioCriteriosEstudioNoUJI.get(0).getAnyoFinalizacionEstudioNoUJIInicio());
            entity.put("anyoFinalizacionEstudioNoUJIFin", envioCriteriosEstudioNoUJI.get(0).getAnyoFinalizacionEstudioNoUJIFin());
        }

        List<EnvioCriterioCampanyaEstado> envioCriterioCampanyaEstados = envioCriterioCampanyaEstadosService.getEnvioCriterioCampanyaEstadosByEnvioCriterioId(envioCriterio.getId());
        entity.put("tipoEstadoCampanyaId", envioCriterioCampanyaEstados.stream().map(campanyaEstado -> campanyaEstado.getCampanyaEstado().getId().toString()).collect(Collectors.toList()));

        if (ParamUtils.isNotNull(envioCriterio.getCampanya()))
        {
            entity.put("programaId", envioCriterio.getCampanya().getPrograma().getId());
        }
        return entity;
    }

    @POST
    @Path("envio/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void addEnvioCriteriosByEnvioId(@PathParam("id") Long envioId, @FormParam("campanyaId") Long campanyaId, @FormParam("tipoEstadoCampanyaId") List<Long> arrayCampanyaEstadoId,
                                           @FormParam("criterioCirculoId") List<Long> arrayCriterioCirculo, @FormParam("mesNacimiento") Long mesNacimiento,
                                           @FormParam("combinadoCampanyas") Long combinadoCampanyas, @FormParam("tipoSeleccionCampanya") Long tipoSeleccionCampanya,
                                           @FormParam("anyoNacimientoDesde") Long anyoNacimientoDesde,
                                           @FormParam("anyoNacimientoHasta") Long anyoNacimientoHasta,
                                           @FormParam("criterioEstudioUJIId") List<Long> arrayCriterioEstudioUJI,
                                           @FormParam("criterioEstudioUJIBeca") List<Long> arrayCriterioEstudioUJIBeca,
                                           @FormParam("anyoFinalizacionEstudioUJIInicio") Long anyoFinalizacionEstudioUJIInicio,
                                           @FormParam("anyoFinalizacionEstudioUJIFin") Long anyoFinalizacionEstudioUJIFin,
                                           @FormParam("criterioEstudioNoUJIId") List<Long> arrayCriterioEstudioNoUJI,
                                           @FormParam("anyoFinalizacionEstudioNoUJIInicio") Long anyoFinalizacionEstudioNoUJIInicio,
                                           @FormParam("anyoFinalizacionEstudioNoUJIFin") Long anyoFinalizacionEstudioNoUJIFin) {


        EnvioCriterioUI envioCriterioUI = new EnvioCriterioUI();

        if (tipoSeleccionCampanya.equals(1L))
        {
            envioCriterioUI.setCampanya(campanyaId);
            envioCriterioUI.setCampanyaEstados(arrayCampanyaEstadoId);
            envioCriterioUI.setCombinadoCampanyas(null);
        }

        if (tipoSeleccionCampanya.equals(2L))
        {
            envioCriterioUI.setCampanya(null);
            envioCriterioUI.setCampanyaEstados(null);
            envioCriterioUI.setCombinadoCampanyas(combinadoCampanyas);
        }

        envioCriterioUI.setTipoSeleccionCampanya(tipoSeleccionCampanya);

        envioCriterioUI.setEnvio(envioId);
        envioCriterioUI.setCriteriosCirculo(arrayCriterioCirculo);
        envioCriterioUI.setCriteriosEstudioUJI(arrayCriterioEstudioUJI);
        envioCriterioUI.setCriteriosEstudioUJIBeca(arrayCriterioEstudioUJIBeca);
        envioCriterioUI.setCriteriosEstudioNoUJI(arrayCriterioEstudioNoUJI);
        envioCriterioUI.setMesNacimiento(mesNacimiento);
        envioCriterioUI.setAnyoNacimientoDesde(anyoNacimientoDesde);
        envioCriterioUI.setAnyoNacimientoHasta(anyoNacimientoHasta);
        envioCriterioUI.setAnyoFinalizacionEstudioUJIInicio(anyoFinalizacionEstudioUJIInicio);
        envioCriterioUI.setAnyoFinalizacionEstudioUJIFin(anyoFinalizacionEstudioUJIFin);
        envioCriterioUI.setAnyoFinalizacionEstudioNoUJIInicio(anyoFinalizacionEstudioNoUJIInicio);
        envioCriterioUI.setAnyoFinalizacionEstudioNoUJIFin(anyoFinalizacionEstudioNoUJIFin);

        envioCriterioService.addEnvioCriteriosByEnvioId(envioCriterioUI);


    }

    @POST
    @Path("envio/cliente")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void addEnvioCriterioClienteByEnvioId(@FormParam("envioId") Long envioId, @FormParam("clienteId") Long clienteId) {

        envioCriterioService.addEnvioCriterioClienteByEnvioId(envioId, clienteId);
    }

    @POST
    @Path("envio/{id}/archivo")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseMessage addEnvioCriterioClienteArchivoByEnvioId(@PathParam("id") Long envioId, FormDataMultiPart multiPart) throws IOException, BusquedaVaciaException, ParseException, SoloSeAdmitenFicherosExcelException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        for (BodyPart bodyPart : multiPart.getBodyParts())
        {
            String mimeType = bodyPart.getHeaders().getFirst("Content-Type");
            if (mimeType != null && !mimeType.isEmpty())
            {
                BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                InputStream documento = bpe.getInputStream();
                return envioCriterioService.addEnvioCriterioClienteArchivoByEnvioId(envioId, documento, getTipoFicheroExcel(mimeType), connectedUserId);
            }
        }

        ResponseMessage response = new ResponseMessage();
        response.setSuccess(false);
        response.setMessage("Archivo incorrecto");
        return response;
    }

    private TipoFicheroExcel getTipoFicheroExcel(String mimeType) throws SoloSeAdmitenFicherosExcelException
    {
        if ("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet".equals(mimeType))
        {
            return TipoFicheroExcel.XLSX;
        }

        if ("application/vnd.ms-excel".equals(mimeType))
        {
            return TipoFicheroExcel.XLS;
        }

        throw new SoloSeAdmitenFicherosExcelException();
    }


    @DELETE
    @Path("envio/cliente/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void deleteEnvioCriterioClienteByEnvioId(@FormParam("envioId") Long envioId, @FormParam("clienteId") Long clienteId, @FormParam("correo") String correo) {
        envioCriterioService.deleteEnvioCriterioClienteByEnvioId(envioId, clienteId, correo);
    }

    @DELETE
    @Path("envio/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteEnvioCriteriosByEnvioId(@PathParam("id") Long envioId) {

        envioCriterioService.deleteEnvioCriteriosByEnvioId(envioId);
    }
}