package es.uji.apps.crm.services.rest;

import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteItem;
import es.uji.apps.crm.model.Envio;
import es.uji.apps.crm.model.EnvioCriterio;
import es.uji.apps.crm.model.EnvioCriterioCirculo;
import es.uji.apps.crm.services.ClienteItemService;
import es.uji.apps.crm.services.ClienteSuscripcionesService;
import es.uji.apps.crm.services.EnvioCriterioCirculoService;
import es.uji.apps.crm.services.EnvioCriterioService;
import es.uji.apps.crm.services.EnvioService;
import es.uji.apps.crm.services.SesionService;
import es.uji.apps.crm.ui.SuscriptoresBajaMensajeriaUI;
import es.uji.apps.crm.utils.AppInfo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.Template;

@Path("baja")
public class BajaMensajeriaResource extends CoreBaseService {

    @InjectParam
    private SesionService sesionService;
    @InjectParam
    private EnvioService envioService;
    @InjectParam
    private EnvioCriterioService envioCriterioService;
    @InjectParam
    private EnvioCriterioCirculoService envioCriterioCirculoService;
    @InjectParam
    private ClienteSuscripcionesService clienteSuscripcionesService;
    @InjectParam
    private ClienteItemService clienteItemService;


    @GET
    @Path("{p_hash}")
    @Produces(MediaType.TEXT_HTML)
    public Template getBajaMailing(@PathParam("p_hash") String pHash, @CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException {

        if (ParamUtils.isNotNull(pHash) && sesionService.getHashBajaMensajeriaValido(pHash))
        {

            Envio envio = envioService.getEnvioByHash(pHash);
            Cliente cliente = sesionService.getUserIdActiveSesion(pHash, Boolean.FALSE);
            EnvioCriterio criterio = envioCriterioService.getEnvioCriterioByEnvioId(envio.getId());

            Template template;

            if (ParamUtils.isNotNull(envio.getPrograma()) && ParamUtils.isNotNull(envio.getPrograma().getPlantilla()))
            {
                template = AppInfo.buildPagina(envio.getPrograma().getPlantilla(), idioma);
            }
            else
            {
                template = AppInfo.buildPagina("crm/base", idioma);
            }

            template.put("contenido", "crm/baja/mailing");
            template.put("tipoSeleccionCampanya", criterio.getTipoSeleccionCampanya());
            template.put("hash", pHash);
            if (ParamUtils.isNotNull(criterio.getCombinadoCampanyas()))
            {
                template.put("combinadoCampanya", criterio.getCombinadoCampanyas());
            }
            if (ParamUtils.isNotNull(criterio.getCampanya()))
            {
                template.put("campanya", criterio.getCampanya());
            }

            List<EnvioCriterioCirculo> envioCriterioCirculos = envioCriterioCirculoService.getEnvioCriterioCirculosByEnvioCriterioId(criterio.getId());
            List<UIEntity> clienteSuscripciones = clienteSuscripcionesService.getListaItemsUnificada(cliente.getId());

            Map<String, List<SuscriptoresBajaMensajeriaUI>> suscriptores = envioCriterioCirculos.stream().map(envioCriterioCirculo -> {
                SuscriptoresBajaMensajeriaUI suscriptor = new SuscriptoresBajaMensajeriaUI(envioCriterioCirculo.getCirculo(), idioma);
                suscriptor.setChecked(Boolean.FALSE);
                for (UIEntity entity : clienteSuscripciones)
                {
                    if (ParamUtils.parseLong(entity.get("itemId")).equals(envioCriterioCirculo.getCirculo().getId()))
                    {
                        suscriptor.setChecked(new Boolean(entity.get("correo")));
                        return suscriptor;
                    }
                }
                return suscriptor;


            }).collect(Collectors.groupingBy(SuscriptoresBajaMensajeriaUI::getGrupo));
            if (ParamUtils.isNotNull(envioCriterioCirculos))
            {
                template.put("suscriptores", suscriptores);
            }
            //            template.put("validHash", validHash);

            return template;

        }
        else
        {
            Template templateErrorUsuario = AppInfo.buildPagina("crm/alumni/base", idioma);
            templateErrorUsuario.put("contenido", "crm/alumni/errorusuario");
            return templateErrorUsuario;
        }

    }

    @POST
    public void actualizaBajaSuscripcion(@FormParam("checked") Boolean checked, @FormParam("itemId") Long itemId, @FormParam("hash") String hash) {

        Cliente cliente = sesionService.getUserIdActiveSesion(hash, Boolean.FALSE);
        ClienteItem clienteItem = clienteItemService.getClienteItemByItemAndCliente(cliente.getId(), itemId);

        if (ParamUtils.isNotNull(clienteItem))
        {
            clienteItem.setCorreo(checked);
            clienteItemService.updateClienteItem(clienteItem);
        }
        else
        {
            clienteItemService.addClienteItem(cliente.getId(), itemId, null, null, null, checked, Boolean.FALSE);
        }
    }


}