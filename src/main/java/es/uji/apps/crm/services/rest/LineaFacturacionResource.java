package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Emisora;
import es.uji.apps.crm.model.LineaFacturacion;
import es.uji.apps.crm.model.ReciboFormato;
import es.uji.apps.crm.model.Servicio;
import es.uji.apps.crm.services.LineaFacturacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("lineafacturacion")
public class LineaFacturacionResource extends CoreBaseService {
    @InjectParam
    private LineaFacturacionService lineaFacturacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getLineasFacturacionByServicio(@QueryParam("servicioId") Long servicioId) {

        List<LineaFacturacion> lista = lineaFacturacionService.getLineasFacturacionByServicio(servicioId);
        return modelToUI(lista);
    }

    private List<UIEntity> modelToUI(List<LineaFacturacion> lineasFacturacion) {
        List<UIEntity> listaUI = new ArrayList<>();

        for (LineaFacturacion lineaFacturacion : lineasFacturacion)
        {
            listaUI.add(modelToUI(lineaFacturacion));
        }
        return listaUI;
    }

    private UIEntity modelToUI(LineaFacturacion lineaFacturacion) {
        return UIEntity.toUI(lineaFacturacion);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("campanya")
    public List<UIEntity> getLineasFacturacionByCampanya(@QueryParam("campanyaId") Long campanyaId) {

        List<LineaFacturacion> lista = lineaFacturacionService.getLineasFacturacionByCampanya(campanyaId);
        return modelToUI(lista);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addLineaFacturacion(UIEntity entity) {

        LineaFacturacion lineaFacturacion = UIToModel(entity);
        return modelToUI(lineaFacturacionService.addLineaFacturacion(lineaFacturacion));
    }

    private LineaFacturacion UIToModel(UIEntity entity) {

        LineaFacturacion lineaFacturacion = entity.toModel(LineaFacturacion.class);

        Servicio servicio = new Servicio();
        servicio.setId(ParamUtils.parseLong(entity.get("servicioId")));
        lineaFacturacion.setServicio(servicio);

        if (ParamUtils.isNotNull(entity.get("emisoraId")))
        {
            Emisora emisora = new Emisora();
            emisora.setId(ParamUtils.parseLong(entity.get("emisoraId")));
            lineaFacturacion.setEmisora(emisora);
        }

        if (ParamUtils.isNotNull(entity.get("reciboFormatoId")))
        {
            ReciboFormato reciboFormato = new ReciboFormato();
            reciboFormato.setId(ParamUtils.parseLong(entity.get("reciboFormatoId")));
            lineaFacturacion.setReciboFormato(reciboFormato);
        }

        return lineaFacturacion;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public UIEntity updateLineaFacturacion(UIEntity entity) {
        LineaFacturacion lineaFacturacion = UIToModel(entity);
        return modelToUI(lineaFacturacionService.updateLineaFacturacion(lineaFacturacion));
    }

    @DELETE
    @Path("{id}")
    public void deleteLineaFacturacion(@PathParam("id") Long lineaFacturacionId) {
        lineaFacturacionService.deleteLineaFacturacion(lineaFacturacionId);
    }

}