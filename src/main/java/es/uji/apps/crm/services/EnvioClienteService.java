package es.uji.apps.crm.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.dao.BuscaCuenta;
import es.uji.apps.crm.dao.EnvioClienteDAO;
import es.uji.apps.crm.dao.EnvioClienteVistaDAO;
import es.uji.apps.crm.exceptions.ErrorEnBorradoDeDocumentoException;
import es.uji.apps.crm.model.*;
import es.uji.apps.crm.model.domains.TipoCorreoEnvio;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Service
public class EnvioClienteService {

    @InjectParam
    private EnvioService envioService;
    @InjectParam
    private ClienteService clienteService;
    @InjectParam
    private BuscaCuenta buscaCuenta;

    private EnvioClienteDAO envioClienteDAO;
    private EnvioClienteVistaDAO envioClienteVistaDAO;

    @Autowired
    public EnvioClienteService(EnvioClienteDAO envioClienteDAO, EnvioClienteVistaDAO envioClienteVistaDAO) {
        this.envioClienteDAO = envioClienteDAO;
        this.envioClienteVistaDAO = envioClienteVistaDAO;
    }


    public List<EnvioClienteVista> getEnvioClientesByEnvioId(Long envioId, Paginacion paginacion) {
        return envioClienteVistaDAO.getClientesByEnvioId(envioId, paginacion);
    }

    public void addEnvioCliente(CampanyaEnvioAuto campanyaEnvioAuto,
                                Cliente cliente, String hash) throws ParseException {

        String correo = dameCorreoCliente(cliente, campanyaEnvioAuto.getTipoCorreoEnvio());
        if (ParamUtils.isNotNull(correo)) {
            Envio envio = envioService.addEnvioAutomatico(campanyaEnvioAuto,
                    cliente, hash, 0L);

            addEnvioCliente(cliente, envio, correo);
        }
    }

    public String dameCorreoCliente(Cliente cliente, Long tipoCorreoEnvio) {

        if (ParamUtils.isNotNull(tipoCorreoEnvio)) {
            if (tipoCorreoEnvio.equals(TipoCorreoEnvio.OFICIAL.getId())) {
                String correoOficial = buscaCuenta.buscaCuentaUJI(cliente.getId());
                if (ParamUtils.isNotNull(correoOficial)) {
                    return correoOficial;
                }
            }
        }

        return cliente.getCorreo();
    }

    public void addEnvioCliente(Cliente cliente, Envio envio, String correo) {

        EnvioCliente envioCliente = new EnvioCliente();

        envioCliente.setPara(correo);
        envioCliente.setCliente(cliente);
        envioCliente.setEnvio(envio);

        insert(envioCliente);
    }

    public EnvioCliente insert(EnvioCliente envioCliente) {
        envioClienteDAO.insert(envioCliente);
        envioCliente.setId(envioCliente.getId());
        return envioCliente;
    }

    public void addEnvioCliente(CampanyaEnvioAuto campanyaEnvioAuto,
                                Cliente cliente, String hash, CampanyaFormulario formulario) throws ParseException {

        String correo = dameCorreoCliente(cliente, campanyaEnvioAuto.getTipoCorreoEnvio());

        if (ParamUtils.isNotNull(correo)) {
            Envio envio = envioService.addEnvioAutomatico(campanyaEnvioAuto,
                    cliente, hash, formulario, 0L);

            addEnvioCliente(cliente, envio, correo);
        }
    }

    public void addEnvioCliente(CampanyaEnvioAuto campanyaEnvioAuto,
                                Cliente cliente, String hash, Long dias) throws ParseException {

        String correo = dameCorreoCliente(cliente, campanyaEnvioAuto.getTipoCorreoEnvio());

        if (ParamUtils.isNotNull(correo)) {
            Envio envio = envioService.addEnvioAutomatico(campanyaEnvioAuto,
                    cliente, hash, dias);

            addEnvioCliente(cliente, envio, correo);
        }
    }

    public void addEnvioCliente(CampanyaEnvioAuto campanyaEnvioAuto,
                                Cliente cliente, String hash, CampanyaFormulario formulario, Long dias)
            throws ParseException {

        String correo = dameCorreoCliente(cliente, campanyaEnvioAuto.getTipoCorreoEnvio());

        if (ParamUtils.isNotNull(correo)) {
            Envio envio = envioService.addEnvioAutomatico(campanyaEnvioAuto,
                    cliente, hash, formulario, dias);

            addEnvioCliente(cliente, envio, correo);
        }
    }

    public void duplicaEnvio(Long connectedUserId, Long envioClienteId) throws ParseException {

        EnvioCliente envioCliente = getEnvioClienteById(envioClienteId);

        Envio envio = new Envio();
        envio.setResponder(envioCliente.getEnvio().getResponder());
        envio.setFecha(new Date());
        envio.setFechaEnvioDestinatario(new Date());
        envio.setDesde(envioCliente.getEnvio().getDesde());
        envio.setAsunto(envioCliente.getEnvio().getAsunto());
        envio.setCuerpo(envioCliente.getEnvio().getCuerpo());
        envio.setEnvioTipo(envioCliente.getEnvio().getEnvioTipo());
        envio.setNombre(envioCliente.getEnvio().getNombre());
        envio.setResponder(envioCliente.getEnvio().getResponder());

        PersonaPasPdi persona = new PersonaPasPdi();
        persona.setId(connectedUserId);
        envio.setPersona(persona);

        envioService.addEnvio(connectedUserId, envio);


        addEnvioACliente(envio.getId(), envioCliente.getCliente().getId(),
                envioCliente.getCliente().getCorreo());

    }

    public EnvioCliente addEnvioACliente(Long envioId, Long clienteId, String para) {
        EnvioCliente envioCliente = new EnvioCliente();

        Envio envio = new Envio();
        envio.setId(envioId);

        envioCliente.setEnvio(envio);

        Cliente cliente = clienteService.getClienteById(clienteId);
        envioCliente.setCliente(cliente);

        envioCliente.setPara(para);

        return insert(envioCliente);
    }

    public EnvioCliente getEnvioClienteById(Long envioClienteId) {

        return envioClienteDAO.getEnvioClienteById(envioClienteId);
    }

    public List<EnvioCliente> getEnvioClientesByClienteId(Long clienteId, Paginacion paginacion) {

        return envioClienteDAO.getEnvioClientesByClienteId(clienteId, paginacion);
    }

    public void delete(Long envioClienteId) {
        envioClienteDAO.delete(EnvioCliente.class, envioClienteId);
    }

    public void marcarEnviado(EnvioCliente envioCliente) {
        envioCliente.setFechaEnvio(new Date());
        envioClienteDAO.update(envioCliente);
    }

    public void deleteEnvioClienteByEnvioId(Long envioId) {
        envioClienteDAO.delEnvioClientesTodos(envioId);
    }

    public void deleteEnvioCliente(Long envioClienteId)
            throws MalformedURLException, ErrorEnBorradoDeDocumentoException {

        EnvioCliente envioCliente = getEnvioClienteById(envioClienteId);
        List<EnvioCliente> enviosCliente = getEnvioClientesByEnvioId(envioCliente.getEnvio().getId());
        if (enviosCliente.size() == 1) {
            envioService.deleteEnvio(envioCliente.getEnvio().getId());
        } else if (enviosCliente.size() > 0) {
            envioClienteDAO.delete(EnvioCliente.class, envioClienteId);
        }

    }

    private List<EnvioCliente> getEnvioClientesByEnvioId(Long envioId) {
        return envioClienteDAO.getClientesByEnvioId(envioId);
    }
}