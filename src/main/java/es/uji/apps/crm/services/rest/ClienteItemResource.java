package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.ClienteItem;
import es.uji.apps.crm.services.ClienteItemService;
import es.uji.apps.crm.services.ItemService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("clienteitem")
public class ClienteItemResource extends CoreBaseService {
    @InjectParam
    private ClienteItemService clienteItemService;

    @InjectParam
    private ItemService itemService;

    @Path("suscripciones")
    public ClienteSuscripcionesResource getPlatformItem(
            @InjectParam ClienteSuscripcionesResource clienteSuscripcionesResource) {
        return clienteSuscripcionesResource;
    }

    @GET
    @Path("{clienteitem}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getClienteItemById(@PathParam("clienteitem") Long clienteItemId) {

        return modelToUI(clienteItemService.getClienteItemById(clienteItemId));

    }

    private UIEntity modelToUI(ClienteItem clienteItem) {

        UIEntity entity = UIEntity.toUI(clienteItem);
        entity.put("nombre", clienteItem.getItem().getNombreCa());
        entity.put("tipo", "MANUAL");

        if (clienteItem.getCliente() != null)
        {
            entity.put("clienteId", clienteItem.getCliente().getId());
        }
        if (clienteItem.getItem() != null && clienteItem.getItem().getGrupo() != null)
        {
            entity.put("grupoId", clienteItem.getItem().getGrupo().getId());
            entity.put("grupoNombre", clienteItem.getItem().getGrupo().getNombreCa());
        }
        return entity;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("cliente/item")
    public UIEntity getClienteItemByItemAndCliente(@QueryParam("clienteId") Long clienteId,
                                                   @QueryParam("itemId") Long itemId) {
        ParamUtils.checkNotNull(clienteId, itemId);

        return modelToUI(clienteItemService.getClienteItemByItemAndCliente(clienteId, itemId));

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItemsClientesByGrupoAndCliente(@QueryParam("grupoId") Long grupoId,
                                                            @QueryParam("clienteId") Long clienteId) {
        ParamUtils.checkNotNull(clienteId);

        if (grupoId != null)
        {
            return modelToUI(clienteItemService.getItemsByGrupoIdAndClienteId(grupoId, clienteId));
        }
        else
        {
            return modelToUI(clienteItemService.getItemsByClienteId(clienteId));
        }
    }

    public List<UIEntity> modelToUI(List<ClienteItem> lista) {
        List<UIEntity> listaEntity = new ArrayList<>();

        for (ClienteItem clienteItem : lista)
        {
            listaEntity.add(modelToUI(clienteItem));
        }
        return listaEntity;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("item/{id}")
    public UIEntity getClientesItemsByItemId(@PathParam("id") Long itemId) {

        Long numClientesItem = clienteItemService.getClientesItemByItemId(itemId);
        UIEntity count = new UIEntity();
        count.put("count", numClientesItem);

        return count;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("cliente/{id}")
    public List<UIEntity> getClientesItemsByClienteId(@PathParam("id") Long clienteId) {

        List<ClienteItem> listaClienteItems = clienteItemService.getItemsByClienteId(clienteId);

        return modelToUI(listaClienteItems);
    }

//    @PUT
//    @Path("{id}")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    public UIEntity updateClienteItemFromForm(@PathParam("id") Long clienteItemId, @FormParam("itemId") Long itemId, @FormParam("correo") Integer correo,
//                                              @FormParam("postal") Integer postal) {
//
//        return modelToUI(clienteItemService.updateClienteItem(clienteItemId, itemId, correo, postal));
//    }

    @GET
    @Path("{id}/itemrelacionados/{itemId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItemRelacionadosByClienteIdAndItemId(@PathParam("id") Long clienteId,
                                                                  @PathParam("itemId") Long itemId) {
        return modelToUI(clienteItemService.getItemRelacionadosByClienteIdAndItemId(clienteId, itemId));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity addClienteItem(@FormParam("clienteId") Long clienteId, @FormParam("itemId") Long itemId, @DefaultValue("0") @FormParam("correo") Integer correo,
                                   @DefaultValue("0") @FormParam("postal") Integer postal, @FormParam("clienteItemRelacionado") Long clienteItemRelacionado,
                                   @FormParam("clienteDato") Long clienteDato, @FormParam("campanyaId") Long campanyaId) {

        ParamUtils.checkNotNull(clienteId, itemId);

        ClienteItem clienteItem = clienteItemService.addClienteItem(clienteId, itemId, clienteItemRelacionado, clienteDato, campanyaId, correo != 0, postal != 0);
        return modelToUI(clienteItem);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity updateClienteItemFromForm(@PathParam("id") Long clienteItemId, @FormParam("correo") Integer correo, @FormParam("postal") Integer postal) {

        return modelToUI(clienteItemService.updateClienteItem(clienteItemId, correo, postal));
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateClienteItem(@PathParam("id") Long clienteItemId, UIEntity entity) {

        ClienteItem clienteItem = clienteItemService.updateClienteItem(entity);
        return modelToUI(clienteItem);
    }

    @PUT
    @Path("{id}/cliente/{clienteId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateClienteIdByClienteItem(@PathParam("id") Long clienteItemId,
                                                 @PathParam("clienteId") Long clienteId) {
        ClienteItem clienteItem = clienteItemService
                .updateClienteByItemId(clienteItemId, clienteId);
        return UIEntity.toUI(clienteItem);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteClienteItem(@PathParam("id") Long clienteItemId) {
        clienteItemService.deleteClienteItem(clienteItemId);
    }

    public Long getCountClientesItemsByItemId(Long tipoEnvioId, Long itemId) {
        return clienteItemService.getCountClientesItem(tipoEnvioId, itemId);
    }
}