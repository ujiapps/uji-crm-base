package es.uji.apps.crm.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaEnvioModeloDAO;
import es.uji.apps.crm.model.CampanyaEnvioModelo;

@Service
public class CampanyaEnvioModeloService {

    private CampanyaEnvioModeloDAO campanyaEnvioModeloDAO;

    @Autowired
    public CampanyaEnvioModeloService(CampanyaEnvioModeloDAO campanyaEnvioModeloDAO) {
        this.campanyaEnvioModeloDAO = campanyaEnvioModeloDAO;
    }

    public CampanyaEnvioModelo getCampanyaEnvioModelo(String tipoModelo) {
        return campanyaEnvioModeloDAO.getCampanyaEnvioModelo(tipoModelo);
    }
}