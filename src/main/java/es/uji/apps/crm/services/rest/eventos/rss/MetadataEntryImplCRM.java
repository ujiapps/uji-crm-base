package es.uji.apps.crm.services.rest.eventos.rss;

import java.util.HashMap;
import java.util.Map;

import org.jdom.Namespace;

public class MetadataEntryImplCRM implements MetadataEntry, Cloneable {
    private Map<String, ValorAtributo> fields = new HashMap<String, ValorAtributo>();

    @Override
    public String getUri() {
        return URI;
    }

    @Override
    @SuppressWarnings("rawtypes")
    public Class getInterface() {
        return getClass();
    }

    @Override
    public void copyFrom(Object o) {
        MetadataEntry module = (MetadataEntry) o;
        setFields(module.getFields());
    }

    @Override
    public Object clone() {
        MetadataEntryImplCRM info = new MetadataEntryImplCRM();
        info.copyFrom(this);

        return info;
    }

    @Override
    public Map<String, ValorAtributo> getFields() {
        return fields;
    }

    @Override
    public void setFields(Map<String, ValorAtributo> fields) {
        this.fields = fields;
    }

    @Override
    public void addField(String name, ValorAtributo valorAtributo) {
        this.fields.put(name, valorAtributo);
    }

    public Namespace getNamespace(String namespace) {
        return Namespace.getNamespace(namespace, MetadataEntryImplCRM.URI);
    }
}