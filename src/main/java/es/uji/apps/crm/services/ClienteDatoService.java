package es.uji.apps.crm.services;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import es.uji.apps.crm.dao.ClienteDatoTipoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ClienteDatoDAO;
import es.uji.apps.crm.model.CampanyaActo;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteDato;
import es.uji.apps.crm.model.ClienteDatoTipo;
import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.model.TipoAcceso;
import es.uji.commons.rest.ParamUtils;

@Service
public class ClienteDatoService {

    private final ClienteDatoDAO clienteDatoDAO;
    private final ClienteDatoTipoDAO clienteDatoTipoDAO;
    private final CampanyaActoService campanyaActoService;
    private final ClienteService clienteService;

    @Autowired
    public ClienteDatoService(ClienteDatoDAO clienteDatoDAO,
                              ClienteDatoTipoDAO clienteDatoTipoDAO,
                              CampanyaActoService campanyaActoService,
                              ClienteService clienteService) {
        this.clienteDatoDAO = clienteDatoDAO;
        this.clienteDatoTipoDAO = clienteDatoTipoDAO;
        this.campanyaActoService = campanyaActoService;
        this.clienteService = clienteService;
    }

    public List<ClienteDato> getClienteDatoByClienteAndItemId(Cliente cliente, Long itemId) {
        return clienteDatoDAO.getClienteDatoByClienteAndItemId(cliente, itemId);
    }

    public ClienteDato getClienteDatoByClienteAndTipoIdAndItem(Cliente cliente, Long tipoId,
                                                               Item item) {
        return clienteDatoDAO.getClienteDatoByClienteAndTipoIdAndItem(cliente, tipoId, item);
    }

    public ClienteDato getClienteDatoByClienteAndTipoIdAndClienteDato(Cliente cliente, Long tipoId, Long padreDatoId) {
        return clienteDatoDAO.getClienteDatoByClienteAndTipoIdAndClienteDato(cliente, tipoId, padreDatoId);
    }

    public ClienteDato addClienteDatoExtra(Long clienteId, Long tipoDatoId, Long tipoAccesoId, String observaciones, String valor, Long clienteDatoId,
                                           Long itemId, Long campanyaId)
            throws ParseException {

        Cliente cliente = clienteService.getClienteById(clienteId);
        ClienteDatoTipo clienteDatoTipo = getClienteDatoTipoById(tipoDatoId);
        ClienteDato clienteDato;
        if (ParamUtils.isNotNull(campanyaId)) {
            clienteDato = getClienteDatoByClienteAndTipoIdVinculadoCampanya(cliente, clienteDatoTipo.getId(), campanyaId);
        } else {
            clienteDato = getClienteDatoByClienteAndTipoId(cliente, clienteDatoTipo.getId());
        }
        if (valor.isEmpty()) {
            return clienteDato;
        }

        if (ParamUtils.isNotNull(clienteDato) && !clienteDatoTipo.isDuplicar()) {

            if (ParamUtils.isNotNull(clienteDatoTipo) && clienteDatoTipo.isModificar()) {
                updateClienteDato(clienteDato, valor, clienteDatoTipo.getClienteDatoTipoTipo().getNombre().equals("F") ? "dd/MM/yyyy" : null);
            }
            return clienteDato;
        }
        String format = ParamUtils.isNotNull(clienteDatoTipo) && clienteDatoTipo.getClienteDatoTipoTipo().getNombre().equals("F") ? "dd/MM/yyyy" : null;

        clienteDato = new ClienteDato(cliente.getId(), clienteDatoTipo, valor, format,
                tipoAccesoId, clienteDatoId, itemId, campanyaId, observaciones);
        clienteDato.setFechaUltimaModificacion(new Date());
        clienteDatoDAO.insert(clienteDato);

        return clienteDato;
    }

    public ClienteDatoTipo getClienteDatoTipoById(Long tipo) {
        return clienteDatoTipoDAO.getClienteDatoTipoById(tipo);
    }

    public ClienteDato getClienteDatoByClienteAndTipoId(Cliente cliente, Long tipoId) {
        return clienteDatoDAO.getClienteDatoByClienteAndTipoId(cliente, tipoId);
    }

    public ClienteDato addClienteDatoExtra(ClienteDato clienteDato, String valor, String format)
            throws ParseException {
        if (!valor.isEmpty()) {

            clienteDato.setValorSegunTipo(valor, format);
            clienteDato.setFechaUltimaModificacion(new Date());
            clienteDatoDAO.insert(clienteDato);
        }
        return clienteDato;
    }

    public ClienteDato updateClienteDato(ClienteDato clienteDato, String valor, String format)
            throws ParseException {

        clienteDato = limpiaTodosValores(clienteDato);
        clienteDato.setValorSegunTipo(valor, format);
        clienteDato.setFechaUltimaModificacion(new Date());
        clienteDatoDAO.update(clienteDato);
        return clienteDato;
    }

    private ClienteDato limpiaTodosValores(ClienteDato clienteDato) {

        clienteDato.setValorFecha(null);
        clienteDato.setValorNumero(null);
        clienteDato.setValorTexto(null);
        clienteDato.setValorFicheroNombre(null);

        clienteDatoDAO.update(clienteDato);

        return clienteDato;
    }

    public ClienteDato insertaClienteDatoFichero(ClienteDato clienteDato) {
        clienteDato.setFechaUltimaModificacion(new Date());
        clienteDatoDAO.insert(clienteDato);
        clienteDato.setId(clienteDato.getId());
        return clienteDato;
    }

    public ClienteDato updateClienteDato(Long clienteDatoId, String valor, String comentarios, Long tipoAcceso)
            throws ParseException {

        ClienteDato clienteDato = getClienteDatoById(clienteDatoId);

        clienteDato.setValorSegunTipo(valor, "dd/MM/yyyy");

        Tipo tipoTipoAcceso = new Tipo();
        tipoTipoAcceso.setId(tipoAcceso);
        clienteDato.setClienteDatoTipoAcceso(tipoTipoAcceso);

        clienteDato.setObservaciones(comentarios);
        clienteDato.setFechaUltimaModificacion(new Date());

        clienteDatoDAO.update(clienteDato);
        return clienteDato;
    }

    public ClienteDato updateClienteDato(Long clienteDatoId, String comentarios, Long tipoAcceso) {

        ClienteDato clienteDato = getClienteDatoById(clienteDatoId);

        Tipo tipoTipoAcceso = new Tipo();
        tipoTipoAcceso.setId(tipoAcceso);
        clienteDato.setClienteDatoTipoAcceso(tipoTipoAcceso);

        clienteDato.setObservaciones(comentarios);
        clienteDato.setFechaUltimaModificacion(new Date());

        clienteDatoDAO.update(clienteDato);
        return clienteDato;
    }

    public ClienteDato getClienteDatoById(Long clienteDatoId) {
        return clienteDatoDAO.getClienteDatoById(clienteDatoId);
    }

    public void update(ClienteDato clienteDato) {
        clienteDato.setFechaUltimaModificacion(new Date());
        clienteDatoDAO.update(clienteDato);
    }

    public ClienteDato updateClienteDatoClienteId(Long clienteDatoId, Long clienteId) {
        ClienteDato clienteDato = clienteDatoDAO.getClienteDatoById(clienteDatoId);
        Cliente cliente = new Cliente();
        cliente.setId(clienteId);

        ClienteDato clienteDatoDestino = clienteDatoDAO.getClienteDatoByClienteAndTipoId(cliente,
                clienteDato.getClienteDatoTipo().getId());

        clienteDato.setCliente(cliente);
        clienteDato.setFechaUltimaModificacion(new Date());

        clienteDatoDAO.update(clienteDato);

        if (clienteDatoDestino != null) {
            delete(clienteDatoDestino.getId());
        }
        return clienteDato;
    }

    public void delete(Long datoId) {
        List<ClienteDato> subDatos = clienteDatoDAO.getClienteDatoByClienteDatoId(datoId);
        for (ClienteDato subDato : subDatos) {
            clienteDatoDAO.delete(ClienteDato.class, subDato.getId());
        }

        clienteDatoDAO.delete(ClienteDato.class, datoId);
    }

    public ClienteDato getClienteDatoByClienteAndTipoIdVinculadoCampanya(Cliente cliente, Long tipoDatoId, Long campanyaId) {

        return clienteDatoDAO.getClienteDatoByClienteAndTipoIdVinculadoCampanya(cliente, tipoDatoId, campanyaId);
    }

    public void deleteClienteDatosByCampanyaClienteId(Long campanyaClienteId) {

        List<ClienteDato> clienteDatos = getClienteDatosByCampanyaClienteId(campanyaClienteId);
        for (ClienteDato clienteDato : clienteDatos) {
            delete(clienteDato.getId());
        }
    }

    private List<ClienteDato> getClienteDatosByCampanyaClienteId(Long campanyaClienteId) {
        return clienteDatoDAO.getClienteDatosByCampanyaClienteId(campanyaClienteId);
    }

    public void addClienteDatoExtraEntradas(Long acto, Long entradas) {
        clienteDatoDAO.addClienteDatoExtraEntradas(acto, entradas);
    }

    public void addClienteDatoExtraEntradasCliente(Long acto, Long entradas, Long clienteId) {

        Long TIPODATOENTRADASSOBRANTES = 26L;

        Cliente cliente = new Cliente();
        cliente.setId(clienteId);

        CampanyaActo campanyaActo = campanyaActoService.getCampanyasActoById(acto);

        ClienteDato clienteDato = clienteDatoDAO.getClienteDatoByClienteAndTipoIdVinculadoCampanya(cliente, TIPODATOENTRADASSOBRANTES, campanyaActo.getCampanya().getId());

        if (ParamUtils.isNotNull(clienteDato)) {
            clienteDato.setValorNumero(entradas);
            clienteDatoDAO.update(clienteDato);
        } else {

            ClienteDato dato = new ClienteDato();

            dato.setValorNumero(entradas);
            dato.setCliente(cliente);
            dato.setCampanya(campanyaActo.getCampanya());

            ClienteDatoTipo clienteDatoTipo = new ClienteDatoTipo();
            clienteDatoTipo.setId(TIPODATOENTRADASSOBRANTES);
            dato.setClienteDatoTipo(clienteDatoTipo);

            Tipo tipo = new Tipo();
            tipo.setId(TipoAcceso.ACCESO_PRIVADO);

            dato.setClienteDatoTipoAcceso(tipo);
            clienteDatoDAO.insert(dato);
        }
    }

    public List<ClienteDato> getAllDatosByClienteId(Long clienteId) {
        return clienteDatoDAO.getAllDatosByClienteId(clienteId);
    }
}