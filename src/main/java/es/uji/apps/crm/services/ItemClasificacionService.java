package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ItemClasificacionDAO;
import es.uji.apps.crm.model.Grupo;
import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.ItemClasificacion;

@Service
public class ItemClasificacionService {

    private ItemClasificacionDAO itemClasificacionDAO;

    @Autowired
    public ItemClasificacionService(ItemClasificacionDAO itemClasificacionDAO) {
        this.itemClasificacionDAO = itemClasificacionDAO;
    }

    public List<ItemClasificacion> getItemClasificacionByClasificacionId(Long clasificacionId) {
        return itemClasificacionDAO.getItemClasificacionByClasificacionId(clasificacionId);
    }

    public ItemClasificacion addItemClasificacion(Long clasificacionId, Long grupoId, Long itemId) {
        ItemClasificacion itemClasificacion = new ItemClasificacion();

        Item item = new Item();
        item.setId(itemId);

        Grupo grupo = new Grupo();
        grupo.setId(grupoId);
        item.setGrupo(grupo);

        itemClasificacion.setItem(item);

//        ClasificacionEstudio clasificacionEstudio = new ClasificacionEstudio();
//        clasificacionEstudio.setId(clasificacionId);
        itemClasificacion.setClasificacionEstudio(clasificacionId);

        return itemClasificacionDAO.insert(itemClasificacion);

    }

    public void deleteItemClasificacion(Long itemClasificacionId) {
        itemClasificacionDAO.delete(ItemClasificacion.class, itemClasificacionId);
    }
}