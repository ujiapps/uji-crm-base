package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.services.ClienteService;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;

@Path("pdf")
public class PdfResource {
    @InjectParam
    private ClienteService clienteService;

    @GET
    @Path("{envioId}/listadoPostal/pdf")
    @Produces("application/pdf")
    public Template generarListadoPostal(@PathParam("envioId") Long envioId) {
        List<Cliente> clientes = clienteService.getClientesByEnvioId(envioId);
        List<Map<String, String>> listadoPersonas = new ArrayList<Map<String, String>>();

        for (Integer i = 0; i < clientes.size(); i = i + 2)
        {
            Map<String, String> mapaPersonas = new HashMap<String, String>();
            mapaPersonas.put("direccion1", clientes.get(i).getPostal());
            if (clientes.size() - 1 > i)
            {
                mapaPersonas.put("direccion2", clientes.get(i + 1).getPostal());
            }

            listadoPersonas.add(mapaPersonas);
        }

        Template template = new PDFTemplate("crm/pdf/listadoPostal", new Locale("ES"), "crm");
        template.put("listadoPersonas", listadoPersonas);

        return template;
    }

    @GET
    @Path("{envioId}/listado/pdf")
    @Produces("application/pdf")
    public Template generarListado(@PathParam("envioId") Long envioId) {
        List<Cliente> clientes = clienteService.getClientesByEnvioId(envioId);
        List<Map<String, String>> listadoPersonas = new ArrayList<Map<String, String>>();

        for (Integer i = 0; i < clientes.size(); i++)
        {
            Map<String, String> mapaPersonas = new HashMap<String, String>();
            mapaPersonas.put("direccion", clientes.get(i).getPostal().replace("\n", " | "));

            listadoPersonas.add(mapaPersonas);
        }

        Template template = new PDFTemplate("crm/pdf/listado", new Locale("ES"), "crm");
        template.put("listadoPersonas", listadoPersonas);

        return template;
    }

}