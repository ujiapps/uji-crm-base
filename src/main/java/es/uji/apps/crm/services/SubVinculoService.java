package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.SubVinculoDAO;
import es.uji.apps.crm.model.SubVinculo;

@Service
public class SubVinculoService {
    @Autowired
    private SubVinculoDAO subVinculoDAO;

    public List<SubVinculo> getSubVinculos() {
        return subVinculoDAO.getSubVinculos();
    }

    public String getSubVinculoNombreById(Long subVinculo) {
        return subVinculoDAO.getSubVinculoNombreById(subVinculo);
    }

    public List<SubVinculo> getSubVinculosByPrograma(Long programaId) {

        return subVinculoDAO.getSubVinculosByPrograma(programaId);
    }
}
