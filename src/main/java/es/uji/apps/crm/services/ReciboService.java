package es.uji.apps.crm.services;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ReciboDAO;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.FiltroRecibo;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.model.Persona;
import es.uji.apps.crm.model.Recibo;

@Service
public class ReciboService {

//    @InjectParam
//    ReciboMovimientoExportadoService reciboMovimientoExportadoService;

    private ReciboDAO reciboDAO;

    @Autowired
    public ReciboService(ReciboDAO reciboDAO) {
        this.reciboDAO = reciboDAO;
    }

    public List<Recibo> getRecibosByPersonaId(Long personaId) {
        return reciboDAO.getRecibosByPersonaId(personaId);
    }

    public List<Recibo> getRecibos(FiltroRecibo filtro, Paginacion paginacion) throws ParseException {
        return reciboDAO.getRecibos(filtro, paginacion);

    }

    public List<Recibo> getRecibosConMovimientos(FiltroRecibo filtro, Paginacion paginacion) throws ParseException {
        return reciboDAO.getRecibosConMovimientos(filtro, paginacion);
    }

    public List<Recibo> getRecibosConMovimientosParaCsv(FiltroRecibo filtro, Paginacion paginacion)
            throws ParseException {
        return reciboDAO.getRecibosConMovimientosParaCsv(filtro, paginacion);
    }

    public Recibo getReciboImpagadoByPersonaAndCampanya(Persona persona, Campanya campanya) {
        return reciboDAO.getReciboImpagadoByPersonaAndCampanya(persona, campanya);
    }

    public Recibo getReciboByCuota(Long clienteCuotaId) {
        return reciboDAO.getReciboByCuota(clienteCuotaId);
    }
}