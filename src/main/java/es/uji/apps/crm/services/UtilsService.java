package es.uji.apps.crm.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Service;

@Service
public class UtilsService {

    public static Date fechaParse(String fecha) throws ParseException {
        SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd/MM/yyyy");
        return formateadorFecha.parse(fecha);
    }

    public static Date fechaParse(String fecha, String format) throws ParseException {
        SimpleDateFormat formateadorFecha = new SimpleDateFormat(format);
        return formateadorFecha.parse(fecha);
    }

    public String fechaFormat(Date fecha) {
        SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd/MM/yyyy");
        return formateadorFecha.format(fecha);
    }

    public String fechaFormat(Date fecha, String format) {
        SimpleDateFormat formateadorFecha = new SimpleDateFormat(format);
        return formateadorFecha.format(fecha);
    }
}