package es.uji.apps.crm.services;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.crm.model.*;
import es.uji.apps.crm.model.domains.*;
import es.uji.apps.crm.ui.CampoUI;
import es.uji.apps.crm.ui.FormularioUI;
import es.uji.apps.crm.ui.OpcionUI;
import es.uji.apps.crm.utils.AppInfo;
import es.uji.apps.crm.utils.HashUtils;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.StreamUtils;
import es.uji.commons.rest.UIEntity;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FormularioService {

    private static final String NOMBRE_BASE_CAMPO_EXTRA = "campo-extra-";
    private static final String NOMBRE_BASE_CAMPO_FICHERO = "campo-fichero-";
    private static final String NOMBRE_BASE_MOTIVOS = "motivos-";

    private Map<String, Properties> configurationRegistry;
    private String idioma;

    private CampanyaFormularioService campanyaFormularioService;
    private CampanyaDatoService campanyaDatoService;
    private ClienteDatoTipoService clienteDatoTipoService;
    private ItemService itemService;
    private CampanyaService campanyaService;
    private TipoEstadoCampanyaMotivoService tipoEstadoCampanyaMotivoService;
    private TipoService tipoService;
    private CampanyaClienteService campanyaClienteService;
    private SesionService sesionService;
    private EnvioPlantillaAdminService  envioPlantillaAdminService;


    @InjectParam
    private TipoEstadoCampanyaService tipoEstadoCampanyaService;
    @InjectParam
    private ClienteService clienteService;
    @InjectParam
    private ClienteGeneralService clienteGeneralService;
    @InjectParam
    private ClienteEstadoCampanyaMotivoService clienteEstadoCampanyaMotivoService;
    @InjectParam
    private ClienteDatoService clienteDatoService;
    @InjectParam
    private EntradasService entradasService;
    @InjectParam
    private ClienteItemService clienteItemService;
    @InjectParam
    private PaisService paisService;
    @InjectParam
    private ProvinciaService provinciaService;
    @InjectParam
    private PoblacionService poblacionService;
    @InjectParam
    private EnvioService envioService;
    @InjectParam
    private EnvioClienteService envioClienteService;

    @Autowired
    public FormularioService(CampanyaFormularioService campanyaFormularioService, CampanyaDatoService campanyaDatoService, ClienteDatoTipoService clienteDatoTipoService,
                             ItemService itemService, CampanyaService campanyaService, TipoEstadoCampanyaMotivoService tipoEstadoCampanyaMotivoService,
                             TipoService tipoService, CampanyaClienteService campanyaClienteService,
                             SesionService sesionService, EnvioPlantillaAdminService envioPlantillaAdminService) {
        this.campanyaFormularioService = campanyaFormularioService;
        this.campanyaDatoService = campanyaDatoService;
        this.clienteDatoTipoService = clienteDatoTipoService;
        this.itemService = itemService;
        this.campanyaService = campanyaService;
        this.tipoEstadoCampanyaMotivoService = tipoEstadoCampanyaMotivoService;
        this.tipoService = tipoService;
        this.campanyaClienteService = campanyaClienteService;
        this.sesionService = sesionService;
        this.envioPlantillaAdminService = envioPlantillaAdminService;
    }

    public CampanyaFormulario getByCampanyaId(Long campanyaId, Long clienteId) {

        CampanyaFormulario campanyaFormulario;
        if (!clienteId.equals(-1L))
            campanyaFormulario = campanyaFormularioService.getFormularioByCampanyaIdAndCliente(campanyaId, clienteId);
        else
            campanyaFormulario = campanyaFormularioService.getFormularioGenericoByCampanya(campanyaId);


        return campanyaFormulario;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        configurationRegistry = new HashMap<String, Properties>();

        try {
            Properties diskConfiguration = new Properties();
            FileInputStream fileInputStream = new FileInputStream(MessageFormat.format(
                    "/etc/uji/crm/i18n/i18n_{0}.properties", idioma.toLowerCase()));

            Reader reader = new InputStreamReader(fileInputStream, "UTF-8");

            diskConfiguration.load(reader);
            configurationRegistry.put(idioma, diskConfiguration);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        this.idioma = idioma;
    }

    private String traduce(String key) {
        Properties configuration = configurationRegistry.get(idioma);
        return configuration.getProperty(key);
    }

    private List<CampoUI> getCamposFormularioByFormulario(CampanyaFormulario formulario, Cliente cliente, String idioma) throws ParseException {
        List<CampoUI> camposUI = new ArrayList<CampoUI>();

        camposUI.addAll(getCamposPrincipalesByFormulario(formulario, cliente, idioma));
        camposUI.addAll(getCamposAdicionalesByFormulario(formulario, cliente, idioma));

        List<TipoEstadoCampanyaMotivo> listTipoEstadoCampanyaMotivo = tipoEstadoCampanyaMotivoService.getTiposEstadoCampanyaByCampanyaId(formulario.getEstadoDestino().getId());
        if (!listTipoEstadoCampanyaMotivo.isEmpty())
            camposUI.add(getMotivosByFormulario(listTipoEstadoCampanyaMotivo));

        return camposUI;
    }

    private CampoUI generaCampoFormularioTipoIdentificacion(CampanyaFormulario formulario, Cliente cliente, String model) {

        List<OpcionUI> opcionesDato = new ArrayList<OpcionUI>();
        List<Tipo> listaTiposIdentificacion = tipoService.getTipos(TipoColumna.IDENTIFICACION.getValue());
        opcionesDato.addAll(listaTiposIdentificacion.stream().map(opcion -> new OpcionUI(opcion.getId().toString(), opcion.getNombre())).collect(Collectors.toList()));
        Boolean requerido = formulario.getSolicitarIdentificacion().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId())
                || formulario.getSolicitarIdentificacion().equals(TipoSolicitudCampo.OBLIGATORIO_MODIFICABLE.getId());
        Boolean readOnly = formulario.getSolicitarIdentificacion().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId());

        return new CampoUI(traduce("form.datoscliente.tipoIdentificacion"), TipoDato.ITEMS_SELECCION_UNICA_DESPL.getId(), "tipoIdentificacion",
                opcionesDato, requerido, readOnly, model, (ParamUtils.isNotNull(cliente)) ? cliente.getClienteGeneral().getTipoIdentificacion().getId().toString() : null);
    }

    private CampoUI generaCampoFormularioIdentificacion(CampanyaFormulario formulario, Cliente cliente, String model) {
        CampoUI campoUI = new CampoUI(traduce("form.datoscliente.dni"), TipoDato.TEXTO.getId(), "identificacion",
                formulario.getSolicitarIdentificacion().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId())
                        || formulario.getSolicitarIdentificacion().equals(TipoSolicitudCampo.OBLIGATORIO_MODIFICABLE.getId()),
                formulario.getSolicitarIdentificacion().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId()),
                model, (ParamUtils.isNotNull(cliente)) ? cliente.getClienteGeneral().getIdentificacion() : null);

        campoUI.setTipoValidacion("IDENTIFICACION");
        campoUI.setAyuda(traduce("form.datoscliente.hintdni"));
        return campoUI;
    }

    private CampoUI generaCampoFormularioNombre(CampanyaFormulario formulario, Cliente cliente, String model) {

        return new CampoUI(traduce("form.datoscliente.nom"), TipoDato.TEXTO.getId(), "nombreOficial",
                formulario.getSolicitarNombre().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId())
                        || formulario.getSolicitarNombre().equals(TipoSolicitudCampo.OBLIGATORIO_MODIFICABLE.getId()),
                formulario.getSolicitarNombre().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId()),
                model, (ParamUtils.isNotNull(cliente)) ? cliente.getNombre() : null);

    }

    private CampoUI generaCampoFormularioApellidos(CampanyaFormulario formulario, Cliente cliente, String model) {
        return new CampoUI(traduce("form.datoscliente.cognoms"), TipoDato.TEXTO.getId(), "apellidosOficial",
                formulario.getSolicitarNombre().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId())
                        || formulario.getSolicitarNombre().equals(TipoSolicitudCampo.OBLIGATORIO_MODIFICABLE.getId()),
                formulario.getSolicitarNombre().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId()), model,
                (ParamUtils.isNotNull(cliente)) ? cliente.getApellidos() : null);
    }

    private CampoUI generaCampoFormularioPrefijoMovil(Cliente cliente, String model) {

        CampoUI campoPrefijoUI = new CampoUI(traduce("form.datoscliente.prefijoTelefono"), TipoDato.TEXTO.getId(), "prefijoTelefono", false, false,
                model, (ParamUtils.isNotNull(cliente)) ? cliente.getPrefijoTelefono() : null);
        campoPrefijoUI.setTipoValidacion("PREFIJO");
        campoPrefijoUI.setAyuda(traduce("form.datoscliente.hintprefijo"));
        return campoPrefijoUI;
    }

    private CampoUI generaCampoFormularioMovil(CampanyaFormulario formulario, Cliente cliente, String model) {
        return new CampoUI(traduce("form.datoscliente.movil"), TipoDato.TELEFONO.getId(), "movil",
                formulario.getSolicitarMovil().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId())
                        || formulario.getSolicitarMovil().equals(TipoSolicitudCampo.OBLIGATORIO_MODIFICABLE.getId()),
                formulario.getSolicitarMovil().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId()), model,
                (ParamUtils.isNotNull(cliente)) ? cliente.getMovil() : null);
    }

    private CampoUI generaCamposFormularioCorreo(CampanyaFormulario formulario, Cliente cliente, String model) {
        return new CampoUI(traduce("form.datoscliente.email"), TipoDato.EMAIL.getId(), "correo",
                formulario.getSolicitarCorreo().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId())
                        || formulario.getSolicitarCorreo().equals(TipoSolicitudCampo.OBLIGATORIO_MODIFICABLE.getId()),
                formulario.getSolicitarCorreo().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId()),
                model, (ParamUtils.isNotNull(cliente)) ? cliente.getCorreo() : null);
    }

    private CampoUI generaCampoFormularioPaisPostal(Cliente cliente, Boolean requerido, Boolean onlyRead, String model) {
        List<OpcionUI> listaPaises = paisService.getOpcionPaisesOrdenadosPorIdioma(idioma);
        return new CampoUI(traduce("form.datoscliente.paisResidencia"), TipoDato.ITEMS_SELECCION_UNICA_DESPL.getId(), "paisPostal", listaPaises,
                requerido, onlyRead, model, (ParamUtils.isNotNull(cliente)) ? cliente.getPaisPostal().getId() : null
        );
    }

    private CampoUI generaCampoFormularioProvinciaPostal(Cliente cliente, Boolean requerido, Boolean onlyRead, String model) {
        List<OpcionUI> listaProvincias = provinciaService.getOpcionProvincias(idioma);

        CampoUI campoProvincia = new CampoUI(traduce("form.datoscliente.provinciaResidencia"), TipoDato.ITEMS_SELECCION_UNICA_DESPL.getId(), "provinciaResidencia", listaProvincias,
                requerido, onlyRead, model, (ParamUtils.isNotNull(cliente)) ? cliente.getProvinciaPostal().getId().toString() : null);

        return campoProvincia;

    }

    private CampoUI generaCampoFormularioPoblacionPostal(Cliente cliente, Long provinciaId, Boolean requerido, Boolean onlyRead, String model) {
        List<OpcionUI> listaPoblaciones = poblacionService.getOpcionPoblacionesByProvinciaId(provinciaId, idioma);

        return new CampoUI(traduce("form.datoscliente.poblacionResidencia"), TipoDato.ITEMS_SELECCION_UNICA_DESPL.getId(), "poblacionResidencia" + provinciaId, listaPoblaciones,
                requerido, onlyRead, model, (ParamUtils.isNotNull(cliente) && cliente.getProvinciaPostal().getId().equals(provinciaId)) ? cliente.getPoblacionPostal().getId().toString() : null,
                provinciaId);

    }

    private CampoUI generaCampoFormularioPostal(Cliente cliente, Boolean requerido, Boolean onlyRead, String model) {
        CampoUI campoUI = new CampoUI(traduce("form.datoscliente.adreca"), TipoDato.TEXTO_LARGO.getId(), "postal",
                requerido, onlyRead, model, (ParamUtils.isNotNull(cliente)) ? cliente.getPostal() : null);

        campoUI.setAyuda(traduce("form.datoscliente.hintadreca"));
        campoUI.setPlaceHolder(traduce("form.datoscliente.placeholderadreca"));

        return campoUI;
    }

    private CampoUI generaCampoFormularioCodigoPostal(Cliente cliente, Boolean requerido, Boolean onlyRead, String model) {
        CampoUI campoUI = new CampoUI(traduce("form.datoscliente.codigoPostal"), TipoDato.NUMERO.getId(), "codigoPostal",
                requerido, onlyRead, model, (ParamUtils.isNotNull(cliente)) ? cliente.getCodigoPostal() : null);

        return campoUI;
    }

    private CampoUI generaCamposFormularioPostal(Cliente cliente, Boolean requerido, Boolean onlyRead, String model) {
        CampoUI campoUI = generaCampoFormularioPaisPostal(cliente, requerido, onlyRead, model);
        campoUI.addCampoDependiente(generaCampoFormularioProvinciaPostal(cliente, requerido, onlyRead, model));

        CampoUI campoPoblacionValencia = generaCampoFormularioPoblacionPostal(cliente, ParamUtils.parseLong("46"), requerido, onlyRead, model);
        if (!(ParamUtils.isNotNull(cliente) && cliente.getProvinciaPostal().getId().equals(46L)))
            campoPoblacionValencia.setVisible(Boolean.FALSE);
        campoUI.addCampoDependiente(campoPoblacionValencia);

        CampoUI campoPoblacionCastellon = generaCampoFormularioPoblacionPostal(cliente, ParamUtils.parseLong("12"), requerido, onlyRead, model);
        if (!(ParamUtils.isNotNull(cliente) && cliente.getProvinciaPostal().getId().equals(12L)))
            campoPoblacionCastellon.setVisible(Boolean.FALSE);
        campoUI.addCampoDependiente(campoPoblacionCastellon);

        campoUI.addCampoDependiente(generaCampoFormularioPostal(cliente, requerido, onlyRead, model));
        campoUI.addCampoDependiente(generaCampoFormularioCodigoPostal(cliente, requerido, onlyRead, model));
        return campoUI;
    }

    private List<CampoUI> getCamposPrincipalesByFormulario(CampanyaFormulario formulario, Cliente cliente, String idioma) {
        List<CampoUI> camposUI = new ArrayList<CampoUI>();
        String modelCliente = "cliente";

        if (!formulario.getSolicitarIdentificacion().equals(TipoSolicitudCampo.NO_SOLICITAR.getId())) {
            camposUI.add(generaCampoFormularioTipoIdentificacion(formulario, cliente, modelCliente));
            camposUI.add(generaCampoFormularioIdentificacion(formulario, cliente, modelCliente));
        }

        if (!formulario.getSolicitarNombre().equals(TipoSolicitudCampo.NO_SOLICITAR.getId())) {
            camposUI.add(generaCampoFormularioNombre(formulario, cliente, modelCliente));
            camposUI.add(generaCampoFormularioApellidos(formulario, cliente, modelCliente));
        }

        if (!formulario.getSolicitarCorreo().equals(TipoSolicitudCampo.NO_SOLICITAR.getId())) {
            camposUI.add(generaCamposFormularioCorreo(formulario, cliente, modelCliente));
        }

        if (!formulario.getSolicitarMovil().equals(TipoSolicitudCampo.NO_SOLICITAR.getId())) {
            camposUI.add(generaCampoFormularioPrefijoMovil(cliente, modelCliente));
            camposUI.add(generaCampoFormularioMovil(formulario, cliente, modelCliente));
        }

        if (!formulario.getSolicitarPostal().equals(TipoSolicitudCampo.NO_SOLICITAR.getId())) {

            camposUI.add(generaCamposFormularioPostal(cliente, formulario.getSolicitarPostal().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId())
                            || formulario.getSolicitarPostal().equals(TipoSolicitudCampo.OBLIGATORIO_MODIFICABLE.getId()),
                    formulario.getSolicitarPostal().equals(TipoSolicitudCampo.OBLIGATORIO_NO_MODIFICABLE.getId()), modelCliente));

        }

        return camposUI;
    }

    private List<CampoUI> getCamposAdicionalesByFormulario(CampanyaFormulario formulario, Cliente cliente, String idioma) throws ParseException {
        List<CampoUI> camposUI = new ArrayList<CampoUI>();

        List<CampanyaDato> campanyaDatos = campanyaDatoService.getCampanyaDatosByFormularioId(formulario.getId());
        List<ClienteDato> listaDatosCliente = new ArrayList<>();
        List<ClienteItem> listaItemsCliente = new ArrayList<>();

        if (ParamUtils.isNotNull(cliente)) {
            listaDatosCliente = clienteDatoService.getAllDatosByClienteId(cliente.getId());
            listaItemsCliente = clienteItemService.getAllItemsByClienteId(cliente.getId());
        }


        for (CampanyaDato campanyaDato : campanyaDatos) {
            if (!ParamUtils.isNotNull(campanyaDato.getCampanyaDato())) {
                CampoUI campoUI = getCampoUIDesdeCampanyaDato(campanyaDato, cliente, listaDatosCliente, listaItemsCliente);
                camposUI.add(campoUI);
            }
        }

        return camposUI;
    }

    private String buscaValorCampoCliente(CampanyaDato campanyaDato, Cliente cliente, List<ClienteDato> listaDatosCliente, List<ClienteItem> listaItemsCliente) throws ParseException {
        String clienteValor = "";
//        if (!ParamUtils.isNotNull(campanyaDato.getCampanyaDato())) {
        if (ParamUtils.isNotNull(cliente)) {
            if (ParamUtils.isNotNull(campanyaDato.getClienteDatoTipo())) {
                for (ClienteDato clienteDato : listaDatosCliente) {
                    if (clienteDato.getClienteDatoTipo().getId().equals(campanyaDato.getClienteDatoTipo().getClienteDatoTipoTipo().getId())) {
                        clienteValor = clienteDato.getValorSegunTipo();
                    }
                }
            }
            if (ParamUtils.isNotNull(campanyaDato.getGrupo())) {
                for (ClienteItem clienteItem : listaItemsCliente) {
                    if (clienteItem.getItem().getGrupo().getId().equals(campanyaDato.getGrupo().getId())) {
                        clienteValor = clienteItem.getItem().getId().toString();
                    }
                }
            }
//            }

        }
        return clienteValor;
    }

    private CampoUI getMotivosByFormulario(List<TipoEstadoCampanyaMotivo> listTipoEstadoCampanyaMotivo) {
        CampoUI campoUI = new CampoUI("", TipoDato.SELECCION.getId(), getNombreMotivosExtra(listTipoEstadoCampanyaMotivo.get(0).getTipoEstadoCampanya().getId()));

        campoUI.setModel("extra");
        campoUI.setRequerido(Boolean.TRUE);
        campoUI.setCampanyaDatoId(listTipoEstadoCampanyaMotivo.get(0).getTipoEstadoCampanya().getId());

        List<OpcionUI> opcionesDato = new ArrayList<OpcionUI>();

        opcionesDato.addAll(listTipoEstadoCampanyaMotivo.stream().map(opcion -> new OpcionUI(opcion.getId().toString(), opcion.getTextoByIdioma(idioma))).collect(Collectors.toList()));

        campoUI.setOpciones(opcionesDato);
        return campoUI;
    }

    private CampoUI getCampoUIDesdeCampanyaDato(CampanyaDato campanyaDato, Cliente cliente, List<ClienteDato> listaDatosCliente, List<ClienteItem> listaItemsCliente) throws ParseException {
        CampoUI campoUI = new CampoUI(campanyaDato.getTextoByIdioma(idioma), campanyaDato.getClienteDatoTipo().getClienteDatoTipoTipo().getId(), getNombreCampoDatoExtra(campanyaDato.getId()));

        String clienteValor = buscaValorCampoCliente(campanyaDato, cliente, listaDatosCliente, listaItemsCliente);

        campoUI.setModel("extra");
        campoUI.setRequerido(campanyaDato.getRequerido());
        campoUI.setVinculadoCampanya(campanyaDato.getVinculadoCampanya());
        campoUI.setCampanyaDatoId(campanyaDato.getId());
        campoUI.setVisible(campanyaDato.getVisualizar());
        campoUI.setAyuda(campanyaDato.getTextoAyudaByIdioma(idioma));
        campoUI.setValor(clienteValor);
        if (ParamUtils.isNotNull(campanyaDato.getTipoValidacion()))
            campoUI.setTipoValidacion(campanyaDato.getTipoValidacion().getNombre());

        List<CampanyaDato> relacionados = campanyaDatoService.getCampanyaDatoRelacionados(campanyaDato.getId());

        if (campanyaDato.getClienteDatoTipo().getClienteDatoTipoTipo().getId().equals(TipoDato.SELECCION.getId())) {
            List<OpcionUI> opcionesDato = getOpcionesCampo(campanyaDato);

            for (OpcionUI opcionUI : opcionesDato) {
                UIEntity uiItem = new UIEntity();

                List<Long> mostrar = new ArrayList<Long>();
                List<Long> ocultar = new ArrayList<Long>();

                for (CampanyaDato campanyaDato1 : relacionados) {
                    if (ParamUtils.isNotNull(campanyaDato1.getRespuestaRelacionada()) && campanyaDato1.getRespuestaRelacionada().getId().toString().equals(opcionUI.getValor())) {
                        mostrar.add(campanyaDato1.getId());
                    } else {
                        ocultar.add(campanyaDato1.getId());
                    }
                }
                opcionUI.setMostrar(mostrar);
                opcionUI.setOcultar(ocultar);
            }

            campoUI.setOpciones(opcionesDato);
        }

        if (ParamUtils.isNotNull(campanyaDato.getGrupo())) {
            List<OpcionUI> opcionesDato = getOpcionesItemsCampo(campanyaDato);

            campoUI.setPlaceHolder(traduce("form.campo.clicplaceholder"));
            campoUI.setAyuda(campanyaDato.getGrupo().getDescripcionByIdioma(idioma));

            for (OpcionUI opcionUI : opcionesDato) {
                UIEntity uiItem = new UIEntity();

                List<Long> mostrar = new ArrayList<Long>();
                List<Long> ocultar = new ArrayList<Long>();

                for (CampanyaDato campanyaDato1 : relacionados) {
                    if (ParamUtils.isNotNull(campanyaDato1.getRespuestaRelacionada()) && campanyaDato1.getRespuestaRelacionada().getId().equals(ParamUtils.parseLong(opcionUI.getValor()))) {
                        mostrar.add(campanyaDato1.getId());
                    } else {
                        ocultar.add(campanyaDato1.getId());
                    }
                }
                opcionUI.setMostrar(mostrar);
                opcionUI.setOcultar(ocultar);
            }
            campoUI.setOpciones(opcionesDato);
        }

        if (campanyaDato.getClienteDatoTipo().getClienteDatoTipoTipo().getId().equals(TipoDato.BINARIO.getId())) {
            campoUI.setNombre(getnombreCampoFichero(campanyaDato.getId()));
            campoUI.setModel("extra-fichero");
        }

        List<CampanyaDato> listaOrdenada = campanyaDato.getCampanyaDatos().stream().sorted((p1, p2) -> p1.getOrden().compareTo(p2.getOrden())).collect(Collectors.toList());

        for (CampanyaDato campoDependiente : listaOrdenada) {
            CampoUI campoDependienteUI = getCampoUIDesdeCampanyaDato(campoDependiente, cliente, listaDatosCliente, listaItemsCliente);
            if (ParamUtils.isNotNull(clienteValor)) {
                for (OpcionUI opcionUI : campoUI.getOpciones()) {
                    if (opcionUI.getValor().equals(clienteValor) && opcionUI.getMostrar().contains(campoDependienteUI.getCampanyaDatoId())) {
                        campoDependienteUI.setVisible(true);
                    }
                }
            }
            campoUI.addCampoDependiente(campoDependienteUI);
        }

        return campoUI;
    }

    public String getNombreCampoDatoExtra(Long campanyaDatoId) {
        return NOMBRE_BASE_CAMPO_EXTRA + campanyaDatoId.toString();
    }

    public String getNombreMotivosExtra(Long campanyaDatoId) {
        return NOMBRE_BASE_MOTIVOS + campanyaDatoId.toString();
    }

    public String getnombreCampoFichero(Long campanyaDatoId) {
        return NOMBRE_BASE_CAMPO_FICHERO + campanyaDatoId;
    }

    public Long getCampanyaDatoIdByNombreCampoFichero(String nombreCampo) {
        if (!esCampoDatoExtraFichero(nombreCampo)) {
            return null;
        }

        String campanyaDatoId = nombreCampo.substring(NOMBRE_BASE_CAMPO_FICHERO.length());
        return Long.parseLong(campanyaDatoId);
    }

    private Boolean esCampoDatoExtraFichero(String nombreCampo) {
        return (nombreCampo.indexOf(NOMBRE_BASE_CAMPO_FICHERO) == 0);
    }

    private List<OpcionUI> getOpcionesCampo(CampanyaDato campanyaDato) {
        List<OpcionUI> opcionesDato = new ArrayList<OpcionUI>();
        List<ClienteDatoOpcion> opciones = new ArrayList<ClienteDatoOpcion>();

        if (ParamUtils.isNotNull(campanyaDato.getTamanyo()) && campanyaDato.getTamanyo() > 0) {
            opciones = clienteDatoTipoService.getClienteDatoOpcionesByTipoAndTamanyo(campanyaDato.getClienteDatoTipo(), campanyaDato.getTamanyo());
        } else {
            opciones = clienteDatoTipoService.getClienteDatoOpcionesByTipoId(campanyaDato.getClienteDatoTipo().getId());

        }
        try {
            Collections.sort(opciones, new Comparator<ClienteDatoOpcion>() {
                @Override
                public int compare(ClienteDatoOpcion i1, ClienteDatoOpcion i2) {
                    Long ip = Long.parseLong(i1.getEtiqueta());
                    return ip.compareTo(Long.parseLong(i2.getEtiqueta()));
                }
            });
        } catch (NumberFormatException var2) {
            Collections.sort(opciones, new Comparator<ClienteDatoOpcion>() {
                @Override
                public int compare(ClienteDatoOpcion i1, ClienteDatoOpcion i2) {
                    String ip = i1.getEtiqueta();
                    return ip.compareTo(i2.getEtiqueta());
                }
            });
        }

        for (ClienteDatoOpcion opcion : opciones) {
            opcionesDato.add(new OpcionUI(opcion.getId().toString(), opcion.getEtiqueta()));
        }
        return opcionesDato;
    }

    private List<OpcionUI> getOpcionesItemsCampo(CampanyaDato campanyaDato) {
        List<OpcionUI> opcionesDato = new ArrayList<OpcionUI>();
        List<Item> items = itemService.getItemsByGrupoId(campanyaDato.getGrupo().getId());

        for (Item item : items) {
            opcionesDato.add(new OpcionUI(item.getId().toString(), item.getNombreByIdioma(idioma), item.getDescripcionByIdioma(idioma)));
        }
        return opcionesDato;
    }

    public FormularioUI getById(Long formularioId, Long clienteId, Cliente cliente, String idioma) throws ParseException {
        FormularioUI formularioUI = new FormularioUI();

//        Cliente cliente = clienteService.getClienteByPersonaIdAndServicio()

        CampanyaFormulario campanyaFormulario;
        if (clienteId.equals(-1L)) {
            campanyaFormulario = campanyaFormularioService.getFormularioByFormularioId(formularioId);
        } else {
            campanyaFormulario = campanyaFormularioService.getFormularioByFormularioId(formularioId, clienteId);
        }
        if (campanyaFormulario == null) {
            return null;
        }

        formularioUI.setId(campanyaFormulario.getId());
        formularioUI.setTitulo(campanyaFormulario.getTitulo());
        formularioUI.setAutenticado(campanyaFormulario.getAutenticaFormulario());
        formularioUI.setCampos(getCamposFormularioByFormulario(campanyaFormulario, cliente, idioma));
        formularioUI.setFechaInicioFormulario(campanyaFormulario.getFechaInicio());
        formularioUI.setFechaFinFormulario(campanyaFormulario.getFechaFin());
        formularioUI.setCampanya(campanyaFormulario.getCampanya().getId());
        formularioUI.setCabecera(campanyaFormulario.getCabecera());
        formularioUI.setPlantilla(campanyaFormulario.getPlantilla());

        if (campanyaFormulario.getCampanya().getCampanyaTexto() != null) {
            formularioUI.setTextoLOPD(campanyaFormulario.getCampanya().getCampanyaTexto().getTextoByIdioma(idioma));
        }

        return formularioUI;
    }

    @Transactional
    public void insertarFormularioCliente(Long formularioId, Cliente cliente, UIEntity
            datosCliente, List<UIEntity> datosExtra) throws Exception {


        CampanyaFormulario formulario = campanyaFormularioService.getFormularioById(formularioId);

        if (!ParamUtils.isNotNull(cliente)) {
            cliente = clienteService.addClienteProvisional(datosCliente, formulario.getCampanya().getPrograma().getServicio());
        }

        insertarDatosExtraCliente(cliente, datosExtra, null);
        asociarClienteACampanya(cliente, formulario);

    }

    public Cliente actualizaCliente(Long personaId, Long servicioId, UIEntity uiEntity) {

        ClienteGeneral clienteGeneral = clienteGeneralService.getClienteGeneralByPerId(personaId);
        if (!ParamUtils.isNotNull(clienteGeneral)) {
            clienteGeneral = new ClienteGeneral();
            Persona persona = new Persona();
            persona.setId(personaId);
            clienteGeneral.setPersona(persona);
            clienteGeneral.setIdentificacion(uiEntity.get("identificacion"));

            Tipo tipoIdentificacion = new Tipo();
            tipoIdentificacion.setId(ParamUtils.parseLong(uiEntity.get("tipoIdentificacion")));
            clienteGeneral.setTipoIdentificacion(tipoIdentificacion);
            clienteGeneral.setDefinitivo(Boolean.TRUE);
            clienteGeneral = clienteGeneralService.addClienteGeneral(clienteGeneral);
        } else {
            clienteGeneral.setIdentificacion(uiEntity.get("identificacion"));
            Tipo tipoIdentificacion = new Tipo();
            tipoIdentificacion.setId(ParamUtils.parseLong(uiEntity.get("tipoIdentificacion")));
            clienteGeneral.setTipoIdentificacion(tipoIdentificacion);
            clienteGeneral = clienteGeneralService.update(clienteGeneral);
        }

        Cliente cliente = clienteService.getClienteByPersonaIdAndServicio(personaId, servicioId);

        if (!ParamUtils.isNotNull(cliente)) {
            cliente = new Cliente();
            cliente.setClienteGeneral(clienteGeneral);
            Servicio servicio = new Servicio();
            servicio.setId(servicioId);
            cliente.setServicio(servicio);
            clienteService.addCliente(cliente);
        }

        if (uiEntity.get("nombreOficial") != null) {
            cliente.setNombre(uiEntity.get("nombreOficial"));
        }

        if (uiEntity.get("apellidosOficial") != null) {
            cliente.setApellidos(uiEntity.get("apellidosOficial"));
        }

        if (uiEntity.get("correo") != null) {
            cliente.setCorreo(uiEntity.get("correo"));
        }

        if (uiEntity.get("movil") != null) {
            cliente.setMovil(uiEntity.get("movil"));
        }

        if (uiEntity.get("prefijoTelefono") != null) {
            cliente.setPrefijoTelefono(uiEntity.get("prefijoTelefono"));
        }

        if (uiEntity.get("postal") != null) {
            cliente.setPostal(uiEntity.get("postal"));
        }

        return cliente;
    }

    public Cliente getClienteDesde(UIEntity uiEntity, Servicio servicio) {

        ClienteGeneral clienteGeneral = new ClienteGeneral();
        if (ParamUtils.isNotNull(uiEntity.get("tipoIdentificacion"))) {
            Tipo tipoIdentificacion = new Tipo();
            tipoIdentificacion.setId(ParamUtils.parseLong(uiEntity.get("tipoIdentificacion")));
            clienteGeneral.setTipoIdentificacion(tipoIdentificacion);
        }

        if (ParamUtils.isNotNull(uiEntity.get("identificacion"))) {
            clienteGeneral.setIdentificacion(uiEntity.get("identificacion"));
        }

        clienteGeneral.setDefinitivo(Boolean.TRUE);
        clienteGeneralService.addClienteGeneral(clienteGeneral);

        Cliente cliente = new Cliente();
        cliente.setClienteGeneral(clienteGeneral);

        if (ParamUtils.isNotNull(uiEntity.get("clienteId"))) {
            cliente.setId(ParamUtils.parseLong(uiEntity.get("clienteId")));
        }

        if (ParamUtils.isNotNull(uiEntity.get("nombreOficial"))) {
            cliente.setNombre(uiEntity.get("nombreOficial"));
        }

        if (ParamUtils.isNotNull(uiEntity.get("apellidosOficial"))) {
            cliente.setApellidos(uiEntity.get("apellidosOficial"));
        }

        if (ParamUtils.isNotNull(uiEntity.get("correo"))) {
            cliente.setCorreo(uiEntity.get("correo"));
        }

        if (ParamUtils.isNotNull(uiEntity.get("movil"))) {
            cliente.setMovil(uiEntity.get("movil"));
        }

        if (ParamUtils.isNotNull(uiEntity.get("prefijoTelefono"))) {
            cliente.setPrefijoTelefono(uiEntity.get("prefijoTelefono"));
        }

        if (ParamUtils.isNotNull(uiEntity.get("postal"))) {
            cliente.setPostal(uiEntity.get("postal"));
        }

        cliente.setTipo(TipoCliente.FISICA.getId());
        cliente.setServicio(servicio);

        return cliente;
    }

    @Transactional
    public void asociarClienteACampanya(Cliente cliente, CampanyaFormulario formulario) throws ParseException, SQLException {

        Campanya campanya = campanyaService.getCampanyaById(formulario.getCampanya().getId());

        CampanyaCliente campanyaCliente = campanyaClienteService.getCampanyaClienteByCampanyaAndClienteId(
                formulario.getCampanya().getId(), cliente.getId());

        if (campanyaCliente == null) {

            campanyaCliente = new CampanyaCliente();

            campanyaCliente.setCampanya(campanya);
            campanyaCliente.setCliente(cliente);
            TipoEstadoCampanya tipoAfiliacion = new TipoEstadoCampanya();
            tipoAfiliacion.setId(formulario.getEstadoDestino().getId());
            campanyaCliente.setCampanyaClienteTipo(tipoAfiliacion);
            campanyaClienteService.addCampanyaCliente(campanyaCliente, formulario);
        } else {
            Boolean gestionaCampanya = Boolean.FALSE;
            TipoEstadoCampanya tipoAfiliacion = new TipoEstadoCampanya();
            if (formulario.getEstadoDestino().getId() != campanyaCliente.getCampanyaClienteTipo().getId()) {
                tipoAfiliacion.setId(formulario.getEstadoDestino().getId());
                campanyaCliente.setCampanyaClienteTipo(tipoAfiliacion);
                gestionaCampanya = Boolean.TRUE;
            }
                campanyaClienteService.updateCampanyaCliente(campanyaCliente, formulario, gestionaCampanya);
        }
    }

    @Transactional
    public void insertarDatosExtraCliente(Cliente cliente, List<UIEntity> camposExtra, String formatoFechaOrigen)
            throws Exception {

        HashMap<Long, ClienteDato> campanyaDatoIdClienteDato = new HashMap<Long, ClienteDato>();
        HashMap<Long, Item> campanyaDatoIdItem = new HashMap<Long, Item>();
        HashMap<Long, ClienteItem> campanyaDatoIdClienteItem = new HashMap<Long, ClienteItem>();

        if (ParamUtils.isNotNull(camposExtra)) {
            for (UIEntity campoExtra : camposExtra) {
                Long campanyaDatoId = campoExtra.getLong("campanyaDatoId");
                List<String> valores = campoExtra.getArray("valor");
                Long clienteDatoId = campoExtra.getLong("clienteDatoId");

                for (String valor : valores) {
                    if (ParamUtils.isNotNull(valor)) {
                        CampanyaDato campanyaDato = campanyaDatoService.getCampanyaDatoById(campanyaDatoId);

                        if (ParamUtils.isNotNull(campanyaDato)) {
                            if (campanyaDato.getGrupo() == null) {
                                ClienteDato clienteDato = new ClienteDato();
                                clienteDato.setCliente(cliente);
                                clienteDato.setClienteDatoTipoAcceso(campanyaDato.getTipoAcceso());
                                clienteDato.setClienteDatoTipo(campanyaDato.getClienteDatoTipo());

                                setRelacionesClienteDato(campanyaDatoIdClienteDato, campanyaDatoIdItem, campanyaDato, clienteDato);

                                if (ParamUtils.isNotNull(clienteDatoId)) {
                                    clienteDato.setId(clienteDatoId);
                                    clienteDatoService.updateClienteDato(clienteDato, valor, formatoFechaOrigen);
                                } else
                                    clienteDatoService.addClienteDatoExtra(clienteDato, valor, formatoFechaOrigen);

                                if (campanyaDato.getVinculadoCampanya()) {
                                    clienteDato.setCampanya(campanyaDato.getCampanyaFormulario().getCampanya());
                                    clienteDatoService.update(clienteDato);
                                    entradasService.anyadeDatoExtraEntradas(cliente, campanyaDato, clienteDato);
                                }

                                campanyaDatoIdClienteDato.put(campanyaDato.getId(), clienteDato);
                            } else {
                                try {
                                    Item item = itemService.getItemById(Long.parseLong(valor));
                                    checkItemPerteneceAlGrupo(item, campanyaDato.getGrupo().getId());

                                    ClienteItem clienteItem = new ClienteItem();

                                    if (ParamUtils.isNotNull(clienteDatoId)) {
                                        clienteItem = clienteItemService.updateClienteItemById(clienteDatoId, cliente, item);
                                    } else
                                        clienteItem = clienteItemService.addClienteItem(cliente, item);
                                    campanyaDatoIdItem.put(campanyaDato.getId(), item);
                                    campanyaDatoIdClienteItem.put(campanyaDato.getId(), clienteItem);

                                    setRelacionesClienteDato(campanyaDatoIdClienteDato, campanyaDatoIdClienteItem,
                                            campanyaDato, clienteItem);

                                    clienteItemService.updateClienteItem(clienteItem);

                                    if (campanyaDato.getVinculadoCampanya()) {
                                        clienteItem.setCampanya(campanyaDato.getCampanyaFormulario().getCampanya());
                                        clienteItemService.updateClienteItem(clienteItem);
                                    }


                                } catch (DataIntegrityViolationException e) {
                                }
                            }
                        } else {
                            TipoEstadoCampanya estadoCampanya = tipoEstadoCampanyaService.getTipoEstadoCampanyaById(campanyaDatoId);
                            if (ParamUtils.isNotNull(estadoCampanya)) {
                                TipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivo = new TipoEstadoCampanyaMotivo();
                                tipoEstadoCampanyaMotivo.setId(ParamUtils.parseLong(valor));

                                ClienteEstadoCampanyaMotivo clienteEstadoCampanyaMotivo = new ClienteEstadoCampanyaMotivo();
                                clienteEstadoCampanyaMotivo.setCampanyaEstadoMotivo(tipoEstadoCampanyaMotivo);

                                CampanyaCliente campanyaCliente = campanyaClienteService.getCampanyaClienteByCampanyaAndClienteId(estadoCampanya.getCampanya().getId(), cliente.getId());

                                clienteEstadoCampanyaMotivo.setCampanyaCliente(campanyaCliente);

                                clienteEstadoCampanyaMotivoService.addClienteEstadoMotivo(clienteEstadoCampanyaMotivo, estadoCampanya.getCampanya().getId());
                            }
                        }
                    }
                }
            }
        }
    }

    private void setRelacionesClienteDato(HashMap<Long, ClienteDato> campanyaDatoIdClienteDato,
                                          HashMap<Long, Item> campanyaDatoIdItem, CampanyaDato campanyaDato,
                                          ClienteDato clienteDato) {
        if (campanyaDato.getCampanyaDato() != null && campanyaDatoIdClienteDato.containsKey(campanyaDato.getCampanyaDato().getId())) {
            clienteDato.setClienteDatoId(campanyaDatoIdClienteDato.get(campanyaDato.getCampanyaDato().getId()).getId());
        }

        if (campanyaDato.getCampanyaDato() != null
                && campanyaDatoIdItem.containsKey(campanyaDato.getCampanyaDato().getId())) {
            clienteDato.setItem(campanyaDatoIdItem.get(campanyaDato.getCampanyaDato().getId()));
        }
    }

    private void setRelacionesClienteDato(HashMap<Long, ClienteDato> campanyaDatoIdClienteDato,
                                          HashMap<Long, ClienteItem> campanyaDatoIdClienteItem, CampanyaDato campanyaDato,
                                          ClienteItem clienteItem) {
        if (campanyaDato.getCampanyaDato() != null
                && campanyaDatoIdClienteDato.containsKey(campanyaDato.getCampanyaDato().getId())) {
            clienteItem.setClienteDato(campanyaDatoIdClienteDato.get(campanyaDato.getCampanyaDato()
                    .getId()).getId());
        }

        if (campanyaDato.getCampanyaDato() != null
                && campanyaDatoIdClienteItem.containsKey(campanyaDato.getCampanyaDato().getId())) {

            clienteItem.setClienteItemRelacionado(campanyaDatoIdClienteItem.get(campanyaDato.getCampanyaDato().getId()).getId());
        }
    }

    private void checkValidFileType(String fileName) throws Exception {
        Set<String> allowedExtensions = new HashSet<>(Arrays.asList("doc", "dot", "xls", "rtf",
                "docx", "xlsx", "pdf", "png", "gif", "jpg", "jpeg", "odt", "ods"));
        String fileExtension = FilenameUtils.getExtension(fileName).toLowerCase();
        if (!allowedExtensions.contains(fileExtension)) {
            throw new Exception("Extensió no permesa");
        }
    }

    private void checkItemPerteneceAlGrupo(Item item, Long grupoId) throws Exception {
        if (item == null || !item.getGrupo().getId().equals(grupoId)) {
            throw new Exception("S'ha intentat introduir un item que no pertany al grup");
        }
    }


    public void insertarFicherosExtraCliente(Cliente cliente, FormDataMultiPart datosFormulario,
                                             Long campanyaId) throws Exception {
        for (Map.Entry<String, List<FormDataBodyPart>> campo : datosFormulario.getFields()
                .entrySet()) {

            Long campanyaDatoId = getCampanyaDatoIdByNombreCampoFichero(campo
                    .getKey());
            if (campanyaDatoId != null) {
                CampanyaDato campanyaDato = campanyaDatoService.getCampanyaDatoById(campanyaDatoId);
                if (!campanyaId.equals(campanyaDato.getCampanyaFormulario().getCampanya().getId())) {
                    throw new Exception("No es permet introduir camps aliens a la campanya");
                }

                for (FormDataBodyPart field : campo.getValue()) {
                    ClienteDato clienteDato = new ClienteDato();
                    clienteDato.setCliente(cliente);
                    clienteDato.setClienteDatoTipoAcceso(campanyaDato.getTipoAcceso());
                    clienteDato.setClienteDatoTipo(campanyaDato.getClienteDatoTipo());

                    String fileName = field.getFormDataContentDisposition().getFileName();
                    if (fileName != null && !fileName.isEmpty()) {
                        checkValidFileType(fileName);
                        BodyPartEntity bpe = (BodyPartEntity) field.getEntity();
                        InputStream documento = bpe.getInputStream();
                        clienteDato.setValorFicheroMimetype(field.getMediaType().toString());
                        clienteDato.setValorFicheroNombre(fileName);
                        clienteDato.setValorFicheroBinario(StreamUtils
                                .inputStreamToByteArray(documento));

                        clienteDatoService.insertaClienteDatoFichero(clienteDato);
                    }
                }
            }
        }
    }

    public CampanyaFormulario getFormularioBajaByCampanyaId(Long campanyaId) {

        TipoEstadoCampanya estadoCampanya = tipoEstadoCampanyaService.getTipoEstadoCampanyaByAccion(campanyaId, "ESTADO-BAJA-WEB");
        return campanyaFormularioService.getCampanyaFormulariosByCampanyaAndEstado(campanyaId, estadoCampanya.getId());
    }

    public ResponseMessage anyadirSesionUsuario(Long formularioId, String correo, String idioma) {

        CampanyaFormulario formulario = campanyaFormularioService.getFormularioByFormularioId(formularioId);

        ClienteGrid clienteGrid = clienteService.getClienteParaFormularioByCorreoAndServicio(correo, formulario.getCampanya().getPrograma().getServicio().getId());
        ResponseMessage responseMessage = new ResponseMessage();
        if (ParamUtils.isNotNull(clienteGrid)) {
            //generar session
            Cliente cliente = clienteService.getClienteById(clienteGrid.getClienteId());
            generarSession(formularioId, cliente, correo, idioma);
            responseMessage.setSuccess(true);

            return responseMessage;

        }
        responseMessage.setSuccess(false);
        return responseMessage;
    }

    private void generarSession(Long formularioId, Cliente cliente, String correo, String idioma) {
        String hash = HashUtils.stringToHashLogin();
        sesionService.renewSesion(cliente, hash, correo);
        String direccionWeb = AppInfo.getHost() + "/crm/rest/publicacion/formulario/" + formularioId + "/perfil/hash/" + hash;

        EnvioPlantillaAdmin envioPlantillaAdmin = envioPlantillaAdminService.getEnvioPlantillaAdminByNombre("ENVIO_PARA_HASH_FORMULARIO");
        Envio envio = new Envio(envioPlantillaAdmin, idioma, direccionWeb);
        envioService.insert(envio);
        envioClienteService.addEnvioCliente(cliente, envio, cliente.getCorreo());
    }

    public List<ClienteGrid> getEnlacesPerfil(String hash, Long servicioId) {

        Sesion sesion = sesionService.getSesion(hash);

        if (ParamUtils.isNotNull(sesion) && sesion.getFechaCaducidad().after(new Date())) {
            return clienteService.getClientesParaFormularioByCorreoAndServicio(sesion.getCorreo(), servicioId);
        }
        return null;
    }

    public ResponseMessage actualizaHash(String hash, Long clienteId) {

        ResponseMessage responseMessage = new ResponseMessage();

        Cliente cliente = clienteService.getClienteById(clienteId);
        sesionService.cambiaClienteSesion(cliente, hash);
        responseMessage.setSuccess(true);

        return responseMessage;

    }
}