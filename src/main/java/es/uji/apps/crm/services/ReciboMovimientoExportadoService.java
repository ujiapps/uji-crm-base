package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ReciboMovimientoExportadoDAO;
import es.uji.apps.crm.model.MovimientoRecibo;
import es.uji.apps.crm.model.ReciboMovimientoExportado;
import es.uji.apps.crm.model.ReciboRemesaExportado;

@Service
public class ReciboMovimientoExportadoService {

    @Autowired
    private ReciboMovimientoExportadoDAO reciboMovimientoExportadoDAO;


    public void movimientoExportado(MovimientoRecibo movimientoRecibo, ReciboRemesaExportado remesaExportado) {

        List<ReciboMovimientoExportado> reciboMovimientoExportados = reciboMovimientoExportadoDAO.getReciboMovimientoExportado(movimientoRecibo.getId());

        if (reciboMovimientoExportados.isEmpty())
        {

            ReciboMovimientoExportado reciboMovimientoExportado = new ReciboMovimientoExportado();
            reciboMovimientoExportado.setRemesa(remesaExportado);
            reciboMovimientoExportado.setValido(Boolean.TRUE);
            reciboMovimientoExportado.setReciboMovimiento(movimientoRecibo);

            reciboMovimientoExportadoDAO.insert(reciboMovimientoExportado);
        }
        else
        {
            Long numValidos = reciboMovimientoExportados.stream().filter(r -> r.getValido()).count();
            if (numValidos == 0L)
            {
                ReciboMovimientoExportado reciboMovimientoExportado = new ReciboMovimientoExportado();

                reciboMovimientoExportado.setRemesa(remesaExportado);
                reciboMovimientoExportado.setValido(Boolean.TRUE);
                reciboMovimientoExportado.setReciboMovimiento(movimientoRecibo);

                reciboMovimientoExportadoDAO.insert(reciboMovimientoExportado);
            }
        }
    }

    public ReciboMovimientoExportado update(ReciboMovimientoExportado reciboMovimientoExportado) {
        reciboMovimientoExportado.setValido(Boolean.FALSE);
        return reciboMovimientoExportadoDAO.update(reciboMovimientoExportado);
    }

    public ReciboMovimientoExportado getReciboMovimientoExportadoById(Long reciboMovimientoExportadoId) {
        return reciboMovimientoExportadoDAO.getReciboMovimientoExportadoById(reciboMovimientoExportadoId);
    }
}