package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.auth.UserSessionManager;
import es.uji.apps.crm.model.CampanyaFormulario;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteGrid;
import es.uji.apps.crm.services.CampanyaFormularioService;
import es.uji.apps.crm.services.FormularioService;
import es.uji.apps.crm.services.PublicacionService;
import es.uji.apps.crm.ui.FormularioUI;
import es.uji.apps.crm.utils.AppInfo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.Template;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;

@Component
@Path("formulario")
public class FormularioResource extends CoreBaseService {

    @InjectParam
    private FormularioService formularioService;
    @InjectParam
    private UserSessionManager userSessionManager;
    @InjectParam
    private PublicacionService publicacionService;

    @InjectParam
    private CampanyaFormularioService campanyaFormularioService;
    @Context
    protected HttpServletResponse response;
    private static final int DURACION_UN_ANYO = 31536000;

//    @Value("${uji.crm.domain}")
    private final String domain = "uji.es";

    @GET
    @Path("{formularioId}")
    @Produces(MediaType.TEXT_HTML)
    public Template getFormularioSinAuth(@PathParam("formularioId") Long formularioId,
                                         @CookieParam("uji-lang") String cookieIdioma) throws ParseException {

        String idioma = publicacionService.getIdioma(cookieIdioma);
        formularioService.setIdioma(idioma);
        FormularioUI formularioUI = formularioService.getById(formularioId, -1L, null, idioma);
        return publicacionService.dameTemplate(formularioUI, idioma);

    }

    @GET
    @Path("{formularioId}/inicio")
    @Produces(MediaType.TEXT_HTML)
    public Template inicio(@PathParam("formularioId") Long formularioId, @CookieParam("uji-lang") String cookieIdioma) throws ParseException {
        String idioma = publicacionService.getIdioma(cookieIdioma);
        Template template = AppInfo.buildPagina("crm/base", idioma);
        template.put("contenido", "crm/formulario/acceso_formulario");
        template.put("formulario", "/crm/rest/publicacion/formulario/" + formularioId);

        CampanyaFormulario formulario = campanyaFormularioService.getFormularioByFormularioId(formularioId);
        template.put("cabecera", formulario.getCabecera());
        template.put("plantilla", formulario.getPlantilla());
        template.put("titulo", formulario.getTitulo());
        return template;
    }

    @GET
    @Path("{formularioId}/idioma/{idioma}")
    public Response cambiaCookieIdioma(@PathParam("idioma") String idioma, @Context HttpServletRequest request) throws URISyntaxException {

        Cookie cookie = new Cookie("uji-lang", idioma.toLowerCase());
        cookie.setPath("/");
        cookie.setDomain(domain);
        cookie.setMaxAge(DURACION_UN_ANYO);
        response.addCookie(cookie);
        return Response.temporaryRedirect(new URI(request.getHeader("Referer"))).build();
    }

    @GET
    @Path("{formularioId}/hash/{hash}")
    @Produces(MediaType.TEXT_HTML)
    public Response getFormularioPorHash(@PathParam("formularioId") Long formularioId,
                                         @PathParam("hash") String hash,
                                         @CookieParam("uji-lang") String cookieIdioma) throws URISyntaxException {

        return publicacionService.activarSesionPorHash(hash, "publicacionprincipal/formulario/" + formularioId);
    }

    @GET
    @Path("{formularioId}/perfil/hash/{hash}")
    @Produces(MediaType.TEXT_HTML)
    public Template getFormularioPerfilesPorHash(@PathParam("formularioId") Long formularioId,
                                                 @PathParam("hash") String hash,
                                                 @CookieParam("uji-lang") String cookieIdioma) throws ParseException {

        String idioma = publicacionService.getIdioma(cookieIdioma);
        CampanyaFormulario formulario = campanyaFormularioService.getFormularioByFormularioId(formularioId);

        Template template = AppInfo.buildPagina("crm/base", idioma);

        List<ClienteGrid> enlaces = formularioService.getEnlacesPerfil(hash, formulario.getCampanya().getPrograma().getServicio().getId());
        if (ParamUtils.isNotNull(enlaces)) {
            template.put("contenido", "crm/formulario/perfil");
            template.put("enlaces", enlaces);
            template.put("hash", hash);
            template.put("formulario", "/crm/rest/publicacion/formulario/" + formularioId);
        }
        else{
            template.put("contenido", "crm/formulario/errorhash");
        }

        template.put("cabecera", formulario.getCabecera());
        template.put("plantilla", formulario.getPlantilla());
        template.put("titulo", formulario.getTitulo());
        return template;

    }

    @PUT
    @Path("{formularioId}/perfil/hash/{hash}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaFormularioHash(@PathParam("formularioId") Long formularioId,
                                                       @PathParam("hash") String hash,
                                                       @FormParam("clienteId") Long clienteId,
                                                       @CookieParam("uji-lang") String cookieIdioma) {
        return formularioService.actualizaHash (hash, clienteId);
    }

    @POST
    @Path("{formularioId}/hash")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage solicitaAccesoPorHash(@FormParam("correo") String correo, @PathParam("formularioId") Long formularioId,
                                                 @CookieParam("uji-lang") String cookieIdioma) {
        String idioma = publicacionService.getIdioma(cookieIdioma);
        return formularioService.anyadirSesionUsuario(formularioId, correo, idioma);
    }


    @POST
    @Path("{formularioId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage altaFormulario(@CookieParam("p_hash") String pHash, @PathParam("formularioId") Long formularioId, UIEntity entity)
            throws Exception {
        ResponseMessage response = new ResponseMessage();

        userSessionManager.init(request, pHash, Boolean.FALSE);
        Cliente cliente = userSessionManager.getCliente();

        UIEntity datosCliente = entity.getRelations().get("datosCliente").get(0);
        List<UIEntity> datosExtra = entity.getRelations().get("datosExtra");

        formularioService.insertarFormularioCliente(formularioId, cliente, datosCliente, datosExtra);

        response.setSuccess(true);
        return response;
    }
}
