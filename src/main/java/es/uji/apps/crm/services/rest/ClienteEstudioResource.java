package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.ClienteEstudio;
import es.uji.apps.crm.services.ClienteEstudioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("estudio")
public class ClienteEstudioResource extends CoreBaseService {

    @InjectParam
    private ClienteEstudioService clienteEstudioService;

    @GET
    @Path("persona/{personaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClienteEstudiosByPersona(@PathParam("personaId") Long personaId) {

        List<ClienteEstudio> clienteEstudios = clienteEstudioService.getClienteEstudiosByPersona(personaId);

        return UIEntity.toUI(clienteEstudios);
    }

    private List<UIEntity> modelToUI(List<ClienteEstudio> clienteEstudios) {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        for (ClienteEstudio clienteEstudio : clienteEstudios)
        {
            lista.add(modelToUI(clienteEstudio));
        }
        return lista;

    }

    private UIEntity modelToUI(ClienteEstudio clienteEstudio) {
        UIEntity entity = UIEntity.toUI(clienteEstudio);
        return entity;
    }


}