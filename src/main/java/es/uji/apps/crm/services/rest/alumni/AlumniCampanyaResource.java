package es.uji.apps.crm.services.rest.alumni;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import es.uji.apps.crm.auth.UserSessionManager;
import es.uji.apps.crm.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StreamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.Template;

import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;

public class AlumniCampanyaResource extends CoreBaseService {

    private final static Long CAMPANYAPREMIUM = 1070562L;
    private final static Long CAMPANYABASIC = 3L;
    private final static Long TIPOFOTO = 3996634L;
    private final static Long TIPOARCHIVO = 4L;
    private final static Boolean FICHAZONAPRIVADA = Boolean.TRUE;

    @Context
    ServletContext servletContext;

    @InjectParam
    private UserSessionManager userSessionManager;

    @InjectParam
    private CampanyaTextoService campanyaTextoService;

    @InjectParam
    private AlumniCampanyaService alumniCampanyaService;

    @InjectParam
    private AlumniService alumniService;

    @InjectParam
    private AlumniFormularioService alumniFormularioService;

    @GET
    @Path("premium/alta")
    @Produces(MediaType.TEXT_HTML)
    public Template getPremiumAlta(@CookieParam("p_hash") String pHash,
                                   @CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        Template template = alumniService.getTemplateBaseAlumni(request.getPathInfo(), idioma, "premium/alta");
        template.put("textoLOPD", campanyaTextoService.getCampanyaTextoByCampanyaCodigo("SAUJI").getTextoByIdioma(idioma));
        return template;
    }

    @GET
    @Path("premium/baja")
    @Produces(MediaType.TEXT_HTML)
    public Template getPremiumBaja(@CookieParam("p_hash") String pHash,
                                   @CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        return alumniService.getTemplateBaseAlumni(request.getPathInfo(), idioma, "premium/baja");
    }

    @GET
    @Path("oipep/alta/{campanya}")
    @Produces(MediaType.TEXT_HTML)
    public Template getOIPEPAlta(@CookieParam("p_hash") String pHash,
                                 @CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                 @PathParam("campanya") Long campanya)
            throws ParseException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        Template template = alumniService.getTemplateBaseAlumni(request.getPathInfo(), idioma, "oipep/alta");
        template.put("campanya", campanya);
        template.put("textoLOPD", campanyaTextoService.getCampanyaTextoByCampanyaCodigo("SAUJI").getTextoByIdioma(idioma));
        return template;
    }

    @GET
    @Path("oipep/alta/{campanya}/{hash}")
    @Produces(MediaType.TEXT_HTML)
    public Template getOIPEPAltaFormulario(@PathParam("hash") String pHash,
                                           @CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                           @PathParam("campanya") Long campanya)
            throws ParseException {

       return alumniFormularioService.getFormularioAltaOipep(idioma, pHash, request.getPathInfo());
    }

    @POST
    @Path("premium/alta")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity formularioPremiumAlta(@CookieParam("p_hash") String pHash,
                                          @CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                          @FormDataParam("documentacion") InputStream documentacion,
                                          @FormDataParam("documentacion") FormDataContentDisposition documentacionDetalle,
                                          @FormDataParam("foto") InputStream foto,
                                          @FormDataParam("foto") FormDataContentDisposition fotoDetalle,
                                          @FormDataParam("modalidad") Long modalidad, @FormDataParam("iban") String iban,
                                          @FormDataParam("comoConoces") Long comoConoces,
                                          @FormDataParam("aceptaLOPD") Boolean aceptaLOPD
    ) throws ParseException, IOException, SQLException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);

        alumniService.altaClientePremium(userSessionManager.getCliente(), modalidad, comoConoces, iban, CAMPANYAPREMIUM);

        if (ParamUtils.isNotNull(documentacionDetalle)) {
            alumniService.anyadirArchivo(userSessionManager.getCliente(), TIPOARCHIVO, documentacionDetalle.getFileName(), servletContext.getMimeType(documentacionDetalle.getFileName()), StreamUtils.inputStreamToByteArray(documentacion));
        }
        if (ParamUtils.isNotNull(fotoDetalle)) {
            alumniService.anyadirArchivo(userSessionManager.getCliente(), TIPOFOTO, fotoDetalle.getFileName(), servletContext.getMimeType(fotoDetalle.getFileName()), StreamUtils.inputStreamToByteArray(foto));
        }

        return new UIEntity();
    }

    @POST
    @Path("premium/baja")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity formularioPremiumBaja(@CookieParam("p_hash") String
                                                  pHash, @CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                          @FormDataParam("motivo") Long motivoBaja,
                                          @FormDataParam("explicacion-motivo") String explicacionMotivo
    ) throws ParseException, SQLException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        alumniService.bajaClientePremium(userSessionManager.getCliente(), CAMPANYAPREMIUM, motivoBaja, explicacionMotivo);

        return new UIEntity();
    }

    @POST
    @Path("oipep/alta/{campanya}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity formularioPremiumAlta(@CookieParam("p_hash") String
                                                  pHash, @CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                          @FormDataParam("foto") InputStream foto,
                                          @FormDataParam("foto") FormDataContentDisposition fotoDetalle,
                                          @FormDataParam("modalidad") Long modalidad,
                                          @FormDataParam("campanya") Long campanya,
                                          @FormDataParam("aceptaLOPD") Boolean aceptaLOPD
    ) throws IOException, ParseException, SQLException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        alumniService.altaClienteOipep(userSessionManager.getCliente(), modalidad, campanya);

        if (ParamUtils.isNotNull(fotoDetalle)) {
            alumniService.anyadirArchivo(userSessionManager.getCliente(), TIPOFOTO, fotoDetalle.getFileName(), servletContext.getMimeType(fotoDetalle.getFileName()), StreamUtils.inputStreamToByteArray(foto));
        }

        return new UIEntity();
    }

    @POST
    @Path("oipep/alta/{campanya}/{hash}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response formularioOipepAlta(@PathParam("hash") String hash,
                                        @CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                        @FormDataParam("nombre") String nombre,
                                        @FormDataParam("apellidos") String apellidos,
                                        @FormDataParam("fechaNacimiento") String fechaNacimiento,
                                        @FormDataParam("nacionalidad") String nacionalidad,
                                        @FormDataParam("telefono") String telefono,
                                        @FormDataParam("paisPostal") String paisPostal,
                                        @FormDataParam("provinciaPostal") Long provinciaPostal,
                                        @FormDataParam("poblacionPostal") Long poblacionPostal,
                                        @FormDataParam("codigoPostal") String codigoPostal,
                                        @FormDataParam("postal") String postal,
                                        @FormDataParam("nivelEstudios") Long nivelEstudios,
                                        @FormDataParam("nombreEstudio") String nombreEstudio,
                                        @FormDataParam("estudiosSuperiores") Long estudiosSuperiores,
                                        @FormDataParam("estudioUJINoUJI") Long estudioUJINoUJI,
                                        @FormDataParam("tipoEstudioUJINoUJI") String tipoEstudioUJINoUJI,
                                        @FormDataParam("listaEstudiosGrado") Long listaEstudiosGrado,
                                        @FormDataParam("nombreEstudioNoGrado") String nombreEstudioNoGrado,
                                        @FormDataParam("universidadEstudio") Long universidadEstudio,
                                        @FormDataParam("anyoFinalizacionEstudio") Long anyoFinalizacionEstudio,
                                        @FormDataParam("laboral") Long laboral,
                                        @FormDataParam("sectorEmpresarial") Long sectorEmpresarial,
                                        @FormDataParam("tipoTrabajo") String tipoTrabajo,
                                        @FormDataParam("lugarEstudiante") String lugarEstudiante,
                                        @FormDataParam("perfilLinkedin") String perfilLinkedin,
                                        @FormDataParam("foto") InputStream foto,
                                        @FormDataParam("foto") FormDataContentDisposition fotoDetalle,
                                        @FormDataParam("doc") InputStream doc,
                                        @FormDataParam("doc") FormDataContentDisposition docDetalle,
                                        @PathParam("campanya") Long campanya)
            throws ParseException, SQLException, IOException {

        return alumniFormularioService.guardarDatosFormularioOipep(hash, campanya, nombre, apellidos, UtilsService.fechaParse(fechaNacimiento), nacionalidad, telefono,
                paisPostal, provinciaPostal, poblacionPostal, codigoPostal, postal, nivelEstudios, nombreEstudio, estudiosSuperiores, estudioUJINoUJI, tipoEstudioUJINoUJI,
                listaEstudiosGrado, nombreEstudioNoGrado, universidadEstudio, anyoFinalizacionEstudio, laboral, sectorEmpresarial, tipoTrabajo, lugarEstudiante, perfilLinkedin,
                foto, fotoDetalle, doc, docDetalle);
    }

    @GET
    @Path("basic/baja")
    @Produces(MediaType.TEXT_HTML)
    public Template getBasicBaja(@CookieParam("p_hash") String
                                         pHash, @CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        return alumniService.getTemplateBaseAlumni(request.getPathInfo(), idioma, "basic/baja");
    }

    @POST
    @Path("basic/baja")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity basicBaja(@CookieParam("p_hash") String pHash,
                              @CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                              @FormDataParam("motivo") Long motivoBaja)
            throws ParseException, SQLException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        alumniService.bajaClienteBasic(userSessionManager.getCliente(), CAMPANYABASIC, motivoBaja);
        return new UIEntity();
    }

    @POST
    @Path("basic/alta")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity basicAlta(@CookieParam("p_hash") String pHash,
                              @CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException, SQLException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        alumniService.altaClienteBasic(userSessionManager.getCliente(), CAMPANYABASIC);
        return new UIEntity();
    }

    @GET
    @Path("zonaprivada")
    @Produces(MediaType.TEXT_HTML)
    public Template getAlumniTemplateCampanya(@CookieParam("p_hash") String pHash,
                                              @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        return alumniCampanyaService.getTemplateCampanya(request.getPathInfo(), userSessionManager.getCliente().getId(), idioma);
    }

    @GET
    @Path("zonaprivada/error")
    @Produces(MediaType.TEXT_HTML)
    public Template getAlumniTemplateErrorCampanya(@CookieParam("p_hash") String pHash,
                                                   @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        return alumniCampanyaService.getAlumniTemplateErrorCampanya(request.getPathInfo(), idioma);
    }

    @GET
    @Path("zonaprivada/gethash")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getHashFormularioZonaPrivada(@CookieParam("p_hash") String pHash) {
        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        if (ParamUtils.isNotNull(pHash)) return UIEntity.toUI(pHash);
        String hash = alumniCampanyaService.getHashFormularioZonaPrivada(userSessionManager.getCliente());
        UIEntity entity = new UIEntity();
        entity.put("hash", hash);
        return entity;
    }
}
