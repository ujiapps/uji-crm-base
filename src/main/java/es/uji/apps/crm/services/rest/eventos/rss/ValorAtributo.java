package es.uji.apps.crm.services.rest.eventos.rss;

public class ValorAtributo {
    private String valor;
    private String namespace;

    public ValorAtributo(String valor, String namespace) {
        this.valor = valor;
        this.namespace = namespace;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }
}