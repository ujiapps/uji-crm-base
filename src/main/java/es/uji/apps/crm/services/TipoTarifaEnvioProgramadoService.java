package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.TipoTarifaEnvioProgramadoDAO;
import es.uji.apps.crm.model.TipoTarifaEnvio;
import es.uji.apps.crm.model.TipoTarifaEnvioProgramado;

@Service
public class TipoTarifaEnvioProgramadoService {
    @Autowired
    private TipoTarifaEnvioProgramadoDAO tipoTarifaEnvioProgramadoDAO;

    public List<TipoTarifaEnvioProgramado> getTipoTarifaEnvioProgramadoByTipoTarifa(TipoTarifaEnvio tipoTarifaEnvio) {
        return tipoTarifaEnvioProgramadoDAO.getTipoTarifaEnvioProgramadoByTipoTarifaEnvio(tipoTarifaEnvio);
    }

    public TipoTarifaEnvioProgramado updateTipoTarifaEnvioProgramado(TipoTarifaEnvioProgramado tipoTarifaEnvioProgramado) {
        return tipoTarifaEnvioProgramadoDAO.update(tipoTarifaEnvioProgramado);
    }

    public TipoTarifaEnvioProgramado addTipoTarifaEnvioProgramado(TipoTarifaEnvioProgramado tipoTarifaEnvioProgramado) {
        return tipoTarifaEnvioProgramadoDAO.insert(tipoTarifaEnvioProgramado);
    }

    public void deleteTipoTarifaEnvioProgramado(Long tarifaEnvioId) {
        tipoTarifaEnvioProgramadoDAO.delete(TipoTarifaEnvioProgramado.class, tarifaEnvioId);
    }
}