package es.uji.apps.crm.services;

import oracle.sql.TIMESTAMP;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.dao.AccionDAO;
import es.uji.apps.crm.model.Accion;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.Cliente;

@Service
public class AccionService {
    @InjectParam
    private UtilsService utilsService;

    @Autowired
    private AccionDAO accionDAO;

    public Accion getAccionByReferencia(Cliente cliente, Campanya campanya) {
        return accionDAO.getAccionByReferencia(cliente, campanya);
    }

    public void addAccionBaja(Cliente cliente, Campanya campanya) throws SQLException {

        Accion accion = new Accion();
        accion.setFecha(new Date());
        accion.setAccion("begin acciones.alta_basic(" + cliente.getId() + "," + campanya.getId()
                + "); end;");

        MessageDigest mm = null;
        try
        {
            mm = MessageDigest.getInstance("SHA-1");
            TIMESTAMP fecha = new TIMESTAMP();

            mm.update((fecha.toString() + "sdkskljw").getBytes());
            byte[] mb = mm.digest();
            accion.setHash(String.valueOf(Hex.encodeHex(mb)));

        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace(); // To change body of catch statement use File | Settings | File
            // Templates.
        }

        accion.setReferencia("ALTA#" + cliente.getId() + "#" + campanya.getId());
        accionDAO.insert(accion);
    }
}