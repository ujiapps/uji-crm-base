package es.uji.apps.crm.services.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaFase;
import es.uji.apps.crm.model.Fase;
import es.uji.apps.crm.services.CampanyaFaseService;
import es.uji.apps.crm.services.FaseService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("campanyafase")
public class CampanyaFaseResource extends CoreBaseService {
    @InjectParam
    private CampanyaFaseService campanyaFaseService;
    @InjectParam
    private FaseService faseService;
    @InjectParam
    private UtilsService utilsService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyaFases(@QueryParam("campanyaId") Long campanyaId) {
        List<CampanyaFase> campanyaFases = campanyaFaseService
                .getCampanyaFasesByCampanyaId(campanyaId);
        return modelToUI(campanyaFases);
    }

    private List<UIEntity> modelToUI(List<CampanyaFase> campanyaFases) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (CampanyaFase campanyaFase : campanyaFases)
        {
            listaUI.add(modelToUI(campanyaFase));
        }

        return listaUI;
    }

    private UIEntity modelToUI(CampanyaFase campanyaFase) {
        UIEntity entity = UIEntity.toUI(campanyaFase);
        entity.put("faseNombre", campanyaFase.getFase().getNombre());
        return entity;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanyaFase(UIEntity entity) throws ParseException {
        CampanyaFase campanyaFase = UIToModel(entity);
        campanyaFaseService.addCampanyaFase(campanyaFase);

        return modelToUI(campanyaFase);
    }

    private CampanyaFase UIToModel(UIEntity entity) throws ParseException {
        CampanyaFase campanyaFase = entity.toModel(CampanyaFase.class);

        Fase fase = new Fase();
        fase.setId(Long.parseLong(entity.get("faseId")));
        campanyaFase.setFase(fase);

        Campanya campanya = new Campanya();
        campanya.setId(Long.parseLong(entity.get("campanyaId")));
        campanyaFase.setCampanya(campanya);

        String fechaIni = entity.get("fechaIni");
        if (fechaIni != null && !fechaIni.isEmpty() && fechaIni.length() > 0)
        {
            campanyaFase.setFechaIni(utilsService.fechaParse(fechaIni));
        }

        String fechaFin = entity.get("fechaFin");
        if (fechaFin != null && !fechaFin.isEmpty() && fechaFin.length() > 0)
        {
            campanyaFase.setFechaFin(utilsService.fechaParse(fechaFin));
        }

        return campanyaFase;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateCampanyaFase(@PathParam("id") Long campanyaFaseId, UIEntity entity)
            throws ParseException {
        CampanyaFase campanyaFase = UIToModel(entity);
        campanyaFase.setId(campanyaFaseId);
        campanyaFaseService.updateCampanyaFase(campanyaFase);

        return modelToUI(campanyaFase);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long campanyaFaseId) {
        campanyaFaseService.delete(campanyaFaseId);
    }

}