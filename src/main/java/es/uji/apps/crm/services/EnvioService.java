package es.uji.apps.crm.services;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.crm.exceptions.ErrorEnBorradoDeDocumentoException;
import es.uji.apps.crm.exceptions.ErrorSubiendoDocumentoException;
import es.uji.apps.crm.model.*;
import es.uji.apps.crm.model.domains.TipoCorreoEnvio;
import es.uji.commons.messaging.client.model.FieldValidationException;
import es.uji.commons.rest.StreamUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.EnvioAdjuntoDAO;
import es.uji.apps.crm.dao.EnvioClienteDAO;
import es.uji.apps.crm.dao.EnvioCriterioDAO;
import es.uji.apps.crm.dao.EnvioDAO;

import es.uji.apps.crm.model.domains.TipoClienteDato;
import es.uji.apps.crm.model.domains.TipoEnvio;

import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;
import es.uji.commons.rest.ParamUtils;

@Service
public class EnvioService {

    private UtilsService utilsService;
    private ClienteDatoService clienteDatoService;
    private ClienteService clienteService;
    private MovimientoService movimientoService;
    private AccionService accionService;
    private SesionService sesionService;
    private EnvioDAO envioDAO;
    private EnvioAdjuntoDAO envioAdjuntoDAO;
    private EnvioClienteDAO envioClienteDAO;
    private EnvioCriterioDAO envioCriterioDAO;
    private EntradasService entradasService;
    private EnvioClienteService envioClienteService;
    private ReciboService reciboService;
    private PersonaService personaService;
    private ADEClienteService adeClienteService;


    @Autowired
    public EnvioService(EnvioDAO envioDAO, EnvioAdjuntoDAO envioAdjuntoDAO, EnvioClienteDAO envioClienteDAO, EnvioCriterioDAO envioCriterioDAO, EntradasService entradasService,
                        UtilsService utilsService, ClienteDatoService clienteDatoService, ClienteService clienteService, MovimientoService movimientoService, AccionService accionService,
                        SesionService sesionService, EnvioClienteService envioClienteService, ReciboService reciboService, PersonaService personaService, ADEClienteService adeClienteService) {
        this.envioDAO = envioDAO;
        this.envioAdjuntoDAO = envioAdjuntoDAO;
        this.envioClienteDAO = envioClienteDAO;
        this.envioCriterioDAO = envioCriterioDAO;
        this.entradasService = entradasService;
        this.utilsService = utilsService;
        this.clienteDatoService = clienteDatoService;
        this.movimientoService = movimientoService;
        this.accionService = accionService;
        this.sesionService = sesionService;
        this.clienteService = clienteService;
        this.envioClienteService = envioClienteService;
        this.reciboService = reciboService;
        this.personaService = personaService;
        this.adeClienteService = adeClienteService;
    }

    public List<Envio> getEnvios() {
        return envioDAO.getEnvios();
    }

    public List<Envio> getEnviosGenerico(Long connectedUserId, FiltroEnvio filtroEnvio,
                                         Paginacion paginacion) {
        return envioDAO.getEnviosGenerico(connectedUserId, filtroEnvio, paginacion);
    }

    public Envio getEnvioById(Long envioId) {
        return envioDAO.getEnvioById(envioId);
    }

    public List<EnvioAdjunto> getAdjuntosByEnvioId(Long envioId) {
        return envioDAO.getAdjuntosByEnvioId(envioId);
    }

    public EnvioAdjunto getEnvioAdjuntoById(Long adjuntoId) {
        return envioAdjuntoDAO.getEnvioAdjuntoById(adjuntoId);
    }

    public List<Envio> getEnviosSeguimiento(Long seguimientoId) {
        return envioDAO.getEnviosSeguimiento(seguimientoId);
    }

    public List<Envio> getEnviosByClienteId(Long clienteId) {

        return envioDAO.getEnviosByClienteId(clienteId);
    }

    public void envioPrueba(MultivaluedMap<String, String> params) throws MessageNotSentException {

        MailMessage mensaje = new MailMessage("CRM");

        mensaje.setTitle(params.getFirst("asunto"));
        mensaje.setContentType(MediaType.TEXT_HTML);
        String cuerpo = params.getFirst("cuerpo");

        if (ParamUtils.isNotNull(params.getFirst("base"))) {
            int title = cuerpo.indexOf("</title>");
            String cuerpo_inicio = cuerpo.substring(0, title + 8);
            String cuerpo_final = cuerpo.substring(title + 9);
            cuerpo = cuerpo_inicio + params.getFirst("base") + cuerpo_final;
        }

        mensaje.setContent(cuerpo);

        String responder = (ParamUtils.isNotNull(params.getFirst("responder"))) ? params.getFirst("responder") : "noreply@uji.es";
        mensaje.setReplyTo(responder);
        mensaje.setSender((ParamUtils.isNotNull(params.getFirst("desde"))) ? params.getFirst("desde") : responder + " <" + responder + ">");

        mensaje.addToRecipient(params.getFirst("correoPrueba"));

        MessagingClient cliente = new MessagingClient();
        cliente.send(mensaje);
    }

    public Envio addEnvio(Long connectedUserId, Envio envio) {
        return insert(envio);
    }

    public String cambiaSexo(String cuerpo, Long sexo) {

        Integer i = cuerpo.indexOf("$[sexe,");
        while (i > -1) {

            Integer j = cuerpo.indexOf("{", i);
            Integer k = cuerpo.indexOf("}]", j);

            String datos = cuerpo.substring(j + 1, k);

            String[] listaDatos = new String[10];

            Integer aux = 1;
            while (!datos.isEmpty()) {
                Integer indice = datos.indexOf(",");
                String valor;
                if (indice > -1)
                    valor = datos.substring(0, indice);
                else
                    valor = datos;
                listaDatos[aux] = valor;
                aux++;

                if (indice < 0)
                    datos = "";
                else
                    datos = datos.substring(indice + 1, datos.length());

            }

            if (ParamUtils.isNotNull(sexo)) {
                cuerpo = cuerpo.replace(cuerpo.substring(i, k + 2), listaDatos[Integer.parseInt(sexo.toString())]);
            } else {
                cuerpo = cuerpo.replace(cuerpo.substring(i, k + 2), listaDatos[3]);
            }

            i = cuerpo.indexOf("$[sexe,");

        }
        return cuerpo;
    }

    private String ponerLink(String cuerpo, Cliente cliente, Campanya campanya) {
        Accion accion = accionService.getAccionByReferencia(cliente, campanya);
        if (accion != null) {
            cuerpo = cuerpo.replace("$[alta]",
                    "https://e-ujier.uji.es/pls/www/uji_crm.exec?p_hash=" + accion.getHash());
        }

        return cuerpo;
    }

    public Envio addEnvioAutomatico(CampanyaEnvioAuto campanyaEnvioAuto,
                                    Cliente cliente, String hash, Long dias) throws ParseException {

        Envio envio = crearEnvio(campanyaEnvioAuto, cliente, hash, dias);

        return insert(envio);
    }

    public Envio addEnvioAutomatico(CampanyaEnvioAuto campanyaEnvioAuto,
                                    Cliente cliente, String hash, CampanyaFormulario formulario, Long dias) throws ParseException {

        Envio envio = crearEnvio(campanyaEnvioAuto, cliente, hash, dias);

        if (ParamUtils.isNotNull(formulario.getActo())) {
            ClienteDatoTipo clienteDatoWeb = new ClienteDatoTipo();
            clienteDatoWeb.setId(TipoClienteDato.WEB.getId());

            VwEntradaCrm web = entradasService.getEntradasSolicitadas(cliente, formulario.getActo());

            if (ParamUtils.isNotNull(web) && ParamUtils.isNotNull(web.getWeb())) {
                String cuerpo = envio.getCuerpo();
                cuerpo = cuerpo.replace("$[web]", web.getWeb());
                envio.setCuerpo(cuerpo);
            }
        }

        return insert(envio);
    }

    private String addPre(String texto) {
        return "<span style=\"white-space:pre\">" + texto + "</span>";
    }

    private Envio crearEnvio(CampanyaEnvioAuto campanyaEnvioAuto, Cliente cliente, String hash, Long dias) throws ParseException {

        Envio envio = new Envio();

        Tipo tipo = new Tipo();
        tipo.setId(TipoEnvio.EMAIL.getId());
        envio.setEnvioTipo(tipo);

        envio.setTipoCorreoEnvio(campanyaEnvioAuto.getTipoCorreoEnvio());

        PersonaPasPdi personaEnvio = new PersonaPasPdi();
        personaEnvio.setId(40000L);
        envio.setPersona(personaEnvio);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_YEAR, dias.intValue());
        envio.setFechaEnvioDestinatario(calendar.getTime());

        envio.setFecha(new Date());
        envio.setAsunto(campanyaEnvioAuto.getAsunto());
        envio.setNombre("Envio Automático");
        envio.setDesde(campanyaEnvioAuto.getDesde());
        envio.setResponder(campanyaEnvioAuto.getResponder());

        String cuerpo = campanyaEnvioAuto.getCuerpo();

        if (ParamUtils.isNotNull(campanyaEnvioAuto.getBase())) {
            int title = cuerpo.indexOf("</title>");
            if (title > 0) {
                String cuerpo_inicio = cuerpo.substring(0, title + 8);
                String cuerpo_final = cuerpo.substring(title + 9);
                cuerpo = cuerpo_inicio + campanyaEnvioAuto.getBase() + cuerpo_final;
            }
            else{
                cuerpo = campanyaEnvioAuto.getBase() + cuerpo;
            }
        }
        cuerpo = cuerpo.replace("$[nom]", cliente.getNombreApellidos());
        cuerpo = cuerpo.replace("$[nombre_propio]", cliente.getNombre());
        if (ParamUtils.isNotNull(cliente.getCorreo()))
            cuerpo = cuerpo.replace("$[correo]", cliente.getCorreo());
        else{
            cuerpo = cuerpo.replace("$[correo]", "");
        }
        cuerpo = cuerpo.replace("$[identificacion]", cliente.getClienteGeneral().getIdentificacion());

        if (cuerpo.indexOf("$[recibo]") > 0) {
            cuerpo = ponerLinkPagoRecibo(cuerpo, cliente, campanyaEnvioAuto.getCampanya());
        }

        if (ParamUtils.isNotNull(cliente.getPostal())) {
            cuerpo = cuerpo.replace("$[postal]", this.addPre(ParamUtils.isNotNull(cliente.getPostalCompleto())?cliente.getPostalCompleto():""));
        } else {
            cuerpo = cuerpo.replace("$[postal]", "");
        }


        Movimiento movimiento = movimientoService.getMovimientoByClienteIdAndCampanyaIdAndTipoId(
                cliente.getId(), campanyaEnvioAuto.getCampanya().getId(), campanyaEnvioAuto
                        .getCampanyaEnvioTipo().getId());
        if (movimiento != null) {
            cuerpo = cuerpo.replace("$[data]",
                    utilsService.fechaFormat(movimiento.getFecha(), "dd/MM/yyyy H:m"));
        }
        cuerpo = cambiaSexo(cuerpo, cliente.getSexo());

        cuerpo = ponerLink(cuerpo, cliente, campanyaEnvioAuto.getCampanya());

        if (ParamUtils.isNotNull(hash)) {
            Sesion sesion = sesionService.addSesion(hash, cliente);
            cuerpo = cuerpo.replace("$[hash]", hash);

        }
        envio.setCuerpo(cuerpo);

        return envio;
    }

    private String ponerLinkPagoRecibo(String cuerpo, Cliente cliente, Campanya campanya) {

        Persona persona = personaService.getPersonaByClienteId(cliente.getId());
        Recibo recibo = reciboService.getReciboImpagadoByPersonaAndCampanya(persona, campanya);
        if (ParamUtils.isNotNull(recibo)) {
            return cuerpo.replace("$[recibo]", "https://e-ujier.uji.es/pls/www/!pagar_hash?p_ref=" + recibo.getCodigoPago());
        }
        return cuerpo.replace("$[recibo]", "");
    }

    public Envio insert(Envio envio) {
        envioDAO.insert(envio);
        envio.setId(envio.getId());

        return envio;
    }

    public Envio update(Envio envio) {
        return envioDAO.update(envio);
    }

    public EnvioAdjunto insertaEnvioAdjunto(EnvioAdjunto envioAdjunto) {

        envioAdjuntoDAO.insert(envioAdjunto);
        envioAdjunto.setId(envioAdjunto.getId());
        return envioAdjunto;
    }

    public void deleteEnvioAdjunto(Long envioAdjuntoId) throws MalformedURLException, ErrorEnBorradoDeDocumentoException {
        EnvioAdjunto envioAdjunto = getEnvioAdjuntoById(envioAdjuntoId);
        adeClienteService.deleteDocumento(envioAdjunto.getReferencia());
        envioAdjuntoDAO.delete(EnvioAdjunto.class, envioAdjuntoId);
    }

    public void deleteEnvio(Long envioId) throws MalformedURLException, ErrorEnBorradoDeDocumentoException {

        List<EnvioAdjunto> adjuntos = getAdjuntosByEnvioId(envioId);
        for (EnvioAdjunto envioAdjunto : adjuntos) {
            deleteEnvioAdjunto(envioAdjunto.getId());
        }

        envioClienteService.deleteEnvioClienteByEnvioId(envioId);
        envioDAO.delete(Envio.class, envioId);
    }

    public Envio getEnvioNuevoConSeguimiento(Long connectedUserId, Long tipoId, Long seguimientoId) {
        return envioDAO.getEnvioNuevoConSeguimiento(connectedUserId, tipoId, seguimientoId);
    }

    public Envio getEnvioNuevo(Long connectedUserId, Long tipoId) {
        return envioDAO.getEnvioNuevo(connectedUserId, tipoId);
    }

    public void creaCorreoEntradasNuevas(String web, Cliente cliente, CampanyaActoEnvio campanyaActoEnvio) {

        String correoOficial = envioClienteService.dameCorreoCliente(cliente, TipoCorreoEnvio.OFICIAL.getId());

        if (ParamUtils.isNotNull(correoOficial)) {
            Envio envio = new Envio();

            Tipo tipo = new Tipo();
            tipo.setId(TipoEnvio.EMAIL.getId());
            envio.setEnvioTipo(tipo);

            PersonaPasPdi personaEnvio = new PersonaPasPdi();
            personaEnvio.setId(40000L);
            envio.setPersona(personaEnvio);

            envio.setFechaEnvioDestinatario(new Date());

            envio.setFecha(new Date());
            envio.setAsunto(campanyaActoEnvio.getAsunto());
            envio.setNombre("Envio Automático");
            envio.setDesde(ParamUtils.isNotNull(campanyaActoEnvio.getDesde())?campanyaActoEnvio.getDesde():"graduacions@uji.es <graduacions@uji.es>");
            envio.setResponder(ParamUtils.isNotNull(campanyaActoEnvio.getResponder())?campanyaActoEnvio.getResponder():"graduacions@uji.es");
            envio.setTipoCorreoEnvio(TipoCorreoEnvio.OFICIAL.getId());

            String cuerpo = campanyaActoEnvio.getCuerpo();
            cuerpo = cuerpo.replace("$[nom]", cliente.getNombreApellidos());
            cuerpo = cambiaSexo(cuerpo, cliente.getSexo());
            cuerpo = cuerpo.replace("$[web]", web);

            envio.setCuerpo(cuerpo);
            insert(envio);

            EnvioCliente envioCliente = new EnvioCliente();
            envioCliente.setCliente(cliente);
            envioCliente.setEnvio(envio);
            envioCliente.setPara(correoOficial);

            envioClienteService.insert(envioCliente);
        }
    }

    public void enviaCorreo(Envio envio, Long envioClienteId, String para, Map<String, Object> datosDocumento) throws MessageNotSentException, FieldValidationException {
        MailMessage mensaje = new MailMessage("CRM");

        mensaje.setTitle(envio.getAsunto());
        mensaje.setContentType(MediaType.TEXT_HTML);
        mensaje.setReference(envioClienteId.toString());
        String cuerpo = envio.getCuerpo();
        mensaje.setContent(cuerpo);
        String responder = (ParamUtils.isNotNull(envio.getResponder())) ? envio.getResponder() : "noreply@uji.es";
        mensaje.setReplyTo(responder);
        mensaje.setSender((ParamUtils.isNotNull(envio.getDesde())) ? envio.getDesde(): responder + " <" + responder + ">");
        mensaje.addToRecipient(para);
        if (ParamUtils.isNotNull(datosDocumento))
            mensaje.addAttachment(datosDocumento.get("nombre").toString(), datosDocumento.get("mimetype").toString(),
                    (byte[]) datosDocumento.get("contenido"));

        MessagingClient cliente = new MessagingClient();
        cliente.send(mensaje);
    }

    public Map<String, Object> insertarAdjunto(FormDataMultiPart multiPart, Envio envio) throws IOException, ErrorSubiendoDocumentoException {

        Map<String, Object> datosDocumento = extraeDatosDeMultipart(multiPart);

        if (ParamUtils.isNotNull(datosDocumento)) {
            EnvioAdjunto envioAdjunto = new EnvioAdjunto();
            envioAdjunto.setEnvio(envio);
            envioAdjunto.setNombreFichero(datosDocumento.get("nombre").toString());

            String ref = adeClienteService.addDocumento(datosDocumento.get("nombre").toString(),
                    datosDocumento.get("mimetype").toString(),
                    (byte[]) datosDocumento.get("contenido"));

            envioAdjunto.setReferencia(ref);
            envioAdjunto.setContentType(datosDocumento.get("mimetype").toString());
            insertaEnvioAdjunto(envioAdjunto);
            datosDocumento.put("envioAdjunto", envioAdjunto);
        }
        return datosDocumento;
    }

    private Map<String, Object> extraeDatosDeMultipart(FormDataMultiPart multiPart)
            throws IOException {
        Map<String, Object> datos = new HashMap<>();

        String fileName = "";
        String mimeType = "";
        InputStream documento = null;

        for (BodyPart bodyPart : multiPart.getBodyParts()) {
            mimeType = bodyPart.getHeaders().getFirst("Content-Type");

            if (mimeType != null && !mimeType.isEmpty()) {
                String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                Matcher m = fileNamePattern.matcher(header);
                if (m.matches()) {
                    fileName = m.group(1);
                }
                BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                documento = bpe.getInputStream();
            }

        }

        if (ParamUtils.isNotNull(documento) && ParamUtils.isNotNull(fileName)) {
            datos.put("nombre", fileName);
            datos.put("mimetype", mimeType);
            datos.put("contenido", StreamUtils.inputStreamToByteArray(documento));
            return datos;
        } else return null;

    }

    public Envio duplicarEnvioGenerico(Long envioId, Long connectedUserId) {
        Envio envio = getEnvioById(envioId);

        Envio envioDuplicado = new Envio();
        envioDuplicado.setAsunto(envio.getAsunto());
        envioDuplicado.setFecha(new Date());
        envioDuplicado.setCuerpo(envio.getCuerpo());
        envioDuplicado.setDesde(envio.getDesde());

        PersonaPasPdi user = new PersonaPasPdi();
        user.setId(connectedUserId);
        envioDuplicado.setPersona(user);

        envioDuplicado.setEnvioTipo(envio.getEnvioTipo());
        envioDuplicado.setNombre(envio.getNombre() + " - DUPLICAT");
        envioDuplicado.setResponder(envio.getResponder());
        envioDuplicado.setTipoCorreoEnvio(envio.getTipoCorreoEnvio());
        return envioDAO.insert(envioDuplicado);
    }

    public Envio getEnvioByHash(String hash) {

        return envioDAO.getEnvioByHash(hash);

    }

    public void autoguardado(Long envioId, String cuerpo) {
        Envio envio = getEnvioById(envioId);
        envio.setCuerpo(cuerpo);
        envioDAO.update(envio);
    }

    public void marcarEnviado(Envio envio) {
        envio.setFechaEnvioPlataforma(new Date());
        envio.setFechaEnvioDestinatario(new Date());
        envioDAO.update(envio);
    }
}
