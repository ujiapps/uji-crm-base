package es.uji.apps.crm.services;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import es.uji.apps.crm.model.*;
import org.apache.commons.codec.binary.Hex;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.dao.CampanyaClienteDAO;
import es.uji.apps.crm.dao.SubVinculos;
import es.uji.apps.crm.model.domains.TipoAfiliacionUsuarioCampanya;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Service
public class CampanyaClienteService {

    private final CampanyaService campanyaService;
    private final ClienteService clienteService;
    private final MovimientoService movimientoService;
    private final ClienteDatoService clienteDatoService;
    private final CampanyaEnvioAutoService campanyaEnvioAutoService;
    private final EnvioClienteService envioClienteService;
    private final AccionService accionService;
    private final CampanyaCampanyaService campanyaCampanyaService;
    private final CampanyaEnvioProgramadoService campanyaEnvioProgramadoService;
    private final ClienteTarifaService clienteTarifaService;
    private final CampanyaFormularioService campanyaFormularioService;
    private final CampanyaCartaService campanyaCartaService;
    private final ClienteGeneralService clienteGeneralService;
    private final ClienteEstadoCampanyaMotivoService clienteEstadoCampanyaMotivoService;
    private final TipoTarifaService tipoTarifaService;
    private final CampanyaClienteDAO campanyaClienteDAO;

    public final static Logger log = LoggerFactory.getLogger(CampanyaClienteService.class);

    @InjectParam
    private FormularioService formularioService;

    @Autowired
    public CampanyaClienteService(CampanyaClienteDAO campanyaClienteDAO,
                                  ClienteTarifaService clienteTarifaService,
                                  CampanyaEnvioProgramadoService campanyaEnvioProgramadoService,
                                  CampanyaCampanyaService campanyaCampanyaService,
                                  AccionService accionService,
                                  EnvioClienteService envioClienteService,
                                  CampanyaEnvioAutoService campanyaEnvioAutoService,
                                  ClienteDatoService clienteDatoService,
                                  MovimientoService movimientoService,
                                  ClienteService clienteService,
                                  CampanyaService campanyaService,
                                  CampanyaFormularioService campanyaFormularioService,
                                  CampanyaCartaService campanyaCartaService,
                                  ClienteGeneralService clienteGeneralService,
                                  ClienteEstadoCampanyaMotivoService clienteEstadoCampanyaMotivoService,
                                  TipoTarifaService tipoTarifaService) {

        this.campanyaClienteDAO = campanyaClienteDAO;
        this.clienteTarifaService = clienteTarifaService;
        this.campanyaEnvioProgramadoService = campanyaEnvioProgramadoService;
        this.campanyaCampanyaService = campanyaCampanyaService;
        this.accionService = accionService;
        this.envioClienteService = envioClienteService;
        this.campanyaEnvioAutoService = campanyaEnvioAutoService;
        this.clienteDatoService = clienteDatoService;
        this.movimientoService = movimientoService;
        this.clienteService = clienteService;
        this.campanyaService = campanyaService;
        this.campanyaFormularioService = campanyaFormularioService;
        this.campanyaCartaService = campanyaCartaService;
        this.clienteGeneralService = clienteGeneralService;
        this.clienteEstadoCampanyaMotivoService = clienteEstadoCampanyaMotivoService;
        this.tipoTarifaService = tipoTarifaService;
    }

    public List<CampanyaClienteBusqueda> getCampanyaClientes(FiltroCliente filtro, Paginacion paginacion) {
        return campanyaClienteDAO.getCampanyaClientesByBusqueda(filtro, paginacion);
    }

    public CampanyaCliente addCampanyaCliente(CampanyaCliente campanyaCliente) {

        try {
            campanyaCliente = campanyaClienteDAO.insert(campanyaCliente);
            if (ParamUtils.isNotNull(campanyaCliente)) {
                gestionaCampanyaCliente(campanyaCliente);
            }
            return campanyaCliente;
        } catch (Exception e) {
            log.error("Fallo al insertar campaña cliente", e);
        }
        return null;
    }

    @Transactional
    public CampanyaCliente addCampanyaCliente(CampanyaCliente campanyaCliente, CampanyaFormulario formulario) {

        try {
            campanyaCliente = campanyaClienteDAO.insert(campanyaCliente);
            gestionaCampanyaCliente(campanyaCliente, formulario);
            return campanyaCliente;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Transactional
    public CampanyaCliente updateCampanyaCliente(Long connectedUserId, CampanyaCliente campanyaCliente, Boolean gestionaCampanya) {
        try {
            CampanyaFormulario campanyaFormulario;

            Persona persona = campanyaCliente.getCliente().getClienteGeneral().getPersona();
            if (ParamUtils.isNotNull(persona)) {
                campanyaFormulario = campanyaFormularioService.getFormularioByCampanyaIdAndUsuario(campanyaCliente.getCampanya().getId(), persona.getId());
            } else {
                campanyaFormulario = null; //Añadido para que en el siguiente if entre en el caso de campanyaFormulario y persona sea nulo.
            }

            campanyaClienteDAO.update(campanyaCliente);
            if (gestionaCampanya) {
                if (ParamUtils.isNotNull(campanyaFormulario)) {
                    gestionaCampanyaCliente(campanyaCliente, campanyaFormulario);
                } else {
                    gestionaCampanyaCliente(campanyaCliente);

                }
            }
            return campanyaCliente;
        } catch (Exception e) {
            log.error("updateCampanyaCliente", e);
        }
        return null;
    }

    @Transactional
    public CampanyaCliente updateCampanyaCliente(CampanyaCliente campanyaCliente, Boolean gestionaCampanya) throws ParseException, SQLException {
        campanyaClienteDAO.update(campanyaCliente);
        if (gestionaCampanya) {
            gestionaCampanyaCliente(campanyaCliente);
        }
        return campanyaCliente;
    }

    public CampanyaCliente updateCampanyaCliente(CampanyaCliente campanyaCliente, CampanyaFormulario formulario, Boolean gestionaCampanya) {
        try {
            campanyaClienteDAO.update(campanyaCliente);
            campanyaCliente = getCampanyaClienteById(campanyaCliente.getId());
            if (gestionaCampanya) {
                gestionaCampanyaCliente(campanyaCliente, formulario);
            }
            return campanyaCliente;
        } catch (Exception e) {
            log.error("updateCampanyaCliente", e);
            return null;
        }
    }

    private void gestionaCampanyaCliente(CampanyaCliente campanyaCliente) throws ParseException, SQLException {

        movimientoService.addMovimiento(campanyaCliente, "Movimiento relativo de cliente a campaña");

        gestionaVinculacionCampanyaCliente(campanyaCliente.getId(), campanyaCliente.getCampanyaClienteTipo(), campanyaCliente.getCampanya(), campanyaCliente.getCliente());

        Cliente cliente = clienteService.getClienteById(campanyaCliente.getCliente().getId());
        String hash = getHash();

        realizarEnvioAutomatico(campanyaCliente.getCampanya(), campanyaCliente.getCampanyaClienteTipo(), campanyaCliente.getCliente(), hash);
        anyadirCampanyasVinculadas(campanyaCliente.getCampanya(), campanyaCliente.getCampanyaClienteTipo(), campanyaCliente.getCliente());
    }

    private String getHash() {
        String hash = null;
        MessageDigest mm;
        try {
            mm = MessageDigest.getInstance("SHA-1");
            Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            Long fecha = c.getTimeInMillis();

            mm.update((fecha.toString() + "sdkskljw").getBytes());
            byte[] mb = mm.digest();
            hash = String.valueOf(Hex.encodeHex(mb));

        } catch (NoSuchAlgorithmException e) {
            log.error("getHash", e);
        }
        return hash;
    }

    @Transactional
    private void gestionaCampanyaCliente(CampanyaCliente campanyaCliente, CampanyaFormulario formulario)
            throws ParseException, SQLException {

        movimientoService.addMovimiento(campanyaCliente, "Movimiento relativo de cliente a campaña");

        gestionaVinculacionCampanyaCliente(campanyaCliente.getId(), campanyaCliente.getCampanyaClienteTipo(), campanyaCliente.getCampanya(), campanyaCliente.getCliente());
        gestionaTarifaCampanya(campanyaCliente.getCampanyaClienteTipo(), campanyaCliente.getCampanya(), campanyaCliente.getCliente());

        String hash = getHash();

        realizarEnvioAutomatico(campanyaCliente.getCampanya(), campanyaCliente.getCampanyaClienteTipo(), campanyaCliente.getCliente(), hash, formulario);
        anyadirCampanyasVinculadas(campanyaCliente.getCampanya(), campanyaCliente.getCampanyaClienteTipo(), campanyaCliente.getCliente());
    }

    private void gestionaTarifaCampanya(TipoEstadoCampanya estado, Campanya campanya, Cliente cliente) {

        TipoEstadoCampanya pendiente = campanyaService.getEstadoCampanyaByCampanya(campanya, TipoAfiliacionUsuarioCampanya.PENDIENTE);
        if (ParamUtils.isNotNull(pendiente) && ParamUtils.isNotNull(estado) && estado.getId().equals(pendiente.getId())) {
            List<TipoTarifa> listaTarifasCampanya = tipoTarifaService.getTiposTarifaByCampanyaId(campanya.getId());
            if (listaTarifasCampanya.size() == 1L) {
                clienteTarifaService.addClienteTarifa(listaTarifasCampanya.get(0), cliente);
            }

        }


    }

    private void gestionaVinculacionCampanyaCliente(Long id, TipoEstadoCampanya estado, Campanya campanya, Cliente cliente)
            throws SQLException {

        TipoEstadoCampanya alta = campanyaService.getEstadoCampanyaByCampanya(campanya, TipoAfiliacionUsuarioCampanya.DEFINITIVO);
        if (ParamUtils.isNotNull(alta) && ParamUtils.isNotNull(estado) && estado.getId().equals(alta.getId())) {
            SubVinculos subVinculos = new SubVinculos();
            subVinculos.init();
            subVinculos.clienteCampanya(id, "ALTA");
            return;
        }

        TipoEstadoCampanya baja = campanyaService.getEstadoCampanyaByCampanya(campanya, TipoAfiliacionUsuarioCampanya.BAJA);
        if (ParamUtils.isNotNull(baja) && ParamUtils.isNotNull(estado) && estado.getId().equals(baja.getId())) {
            SubVinculos subVinculos = new SubVinculos();
            subVinculos.init();
            subVinculos.clienteCampanya(id, "BAJA");
            accionService.addAccionBaja(cliente, campanya);
            return;
        }

        TipoEstadoCampanya bajaVinculo = campanyaService.getEstadoCampanyaByCampanya(campanya, TipoAfiliacionUsuarioCampanya.BAJAVINCULO);
        if (ParamUtils.isNotNull(bajaVinculo) && ParamUtils.isNotNull(estado) && estado.getId().equals(bajaVinculo.getId())) {
            SubVinculos subVinculos = new SubVinculos();
            subVinculos.init();
            subVinculos.clienteCampanya(id, "BAJA");
        }
    }

    public void realizarEnvioAutomatico(Campanya campanya, TipoEstadoCampanya estado, Cliente cliente, String hash)
            throws ParseException {
        List<CampanyaEnvioAuto> campanyaEnviosAuto = campanyaEnvioAutoService.getCampanyaEnvioAutoByCampanyaIdAndTipoId(campanya.getId(), estado.getId(), cliente.getId());

        for (CampanyaEnvioAuto campanyaEnvioAuto : campanyaEnviosAuto) {
            List<CampanyaEnvioProgramado> enviosProgramados = campanyaEnvioProgramadoService.getCampanyaEnvioProgramadoByEnvio(campanyaEnvioAuto);
            if (enviosProgramados.isEmpty()) {
                envioClienteService.addEnvioCliente(campanyaEnvioAuto, cliente, hash);
            }
        }

        campanyaCartaService.anyadirCampanyaCartaCliente(campanya.getId(), estado.getId(), cliente);
    }

    @Transactional
    public void realizarEnvioAutomatico(Campanya campanya, TipoEstadoCampanya estado, Cliente cliente, String hash, CampanyaFormulario formulario)
            throws ParseException {
        List<CampanyaEnvioAuto> campanyaEnviosAuto = campanyaEnvioAutoService.getCampanyaEnvioAutoByCampanyaIdAndTipoId(campanya.getId(), estado.getId(), cliente.getId());

        for (CampanyaEnvioAuto campanyaEnvioAuto : campanyaEnviosAuto) {
            List<CampanyaEnvioProgramado> enviosProgramados = campanyaEnvioProgramadoService.getCampanyaEnvioProgramadoByEnvio(campanyaEnvioAuto);
            if (enviosProgramados.isEmpty()) {
                envioClienteService.addEnvioCliente(campanyaEnvioAuto, cliente, hash, formulario);
            }
        }
        campanyaCartaService.anyadirCampanyaCartaCliente(campanya.getId(), estado.getId(), cliente);

    }

    private void anyadirCampanyasVinculadas(Campanya campanya, TipoEstadoCampanya estado, Cliente cliente) throws ParseException, SQLException {

        CampanyaCampanya campanyaVinculada = campanyaCampanyaService.getCampanyaVinculada(campanya, estado);

        if (ParamUtils.isNotNull(campanyaVinculada)) {
            CampanyaCliente campanyaCliente = getCampanyaClienteByCampanyaAndClienteId(campanyaVinculada.getCampanyaDestino().getId(), cliente.getId());

            if (ParamUtils.isNotNull(campanyaCliente)) {
                if (campanyaVinculada.getEstadoDestino() != campanyaCliente.getCampanyaClienteTipo()) {
                    campanyaCliente.setCampanyaClienteTipo(campanyaVinculada.getEstadoDestino());
                    updateCampanyaCliente(campanyaCliente, Boolean.TRUE);
                }
            } else {
                campanyaCliente = new CampanyaCliente();
                campanyaCliente.setCliente(cliente);
                campanyaCliente.setCampanya(campanyaVinculada.getCampanyaDestino());
                campanyaCliente.setCampanyaClienteTipo(campanyaVinculada.getEstadoDestino());

                CampanyaFormulario formulario = campanyaFormularioService.getCampanyaFormulariosByCampanyaAndEstado(campanyaVinculada.getId(), campanyaVinculada.getEstadoDestino().getId());

                campanyaCliente = addCampanyaCliente(campanyaCliente, formulario);
            }
        }

    }

    public void deleteCampanyaCliente(Long campanyaClienteId) {

        SubVinculos subVinculos = new SubVinculos();
        subVinculos.init();
        subVinculos.clienteCampanya(campanyaClienteId, "BAJA");

        clienteDatoService.deleteClienteDatosByCampanyaClienteId(campanyaClienteId);

        CampanyaCliente campanyaCliente = getCampanyaClienteById(campanyaClienteId);

        movimientoService.deleteMovimientosByCampanyaCliente(campanyaCliente.getCampanya(), campanyaCliente.getCliente());
        clienteTarifaService.deleteClienteTarifaByCampanyaAndCliente(campanyaCliente.getCampanya(), campanyaCliente.getCliente());
        clienteEstadoCampanyaMotivoService.deleteClienteEstadoCampanyaMotivoByCampanyacliente(campanyaClienteId);

        campanyaClienteDAO.delete(CampanyaCliente.class, campanyaClienteId);

    }

    public CampanyaCliente getCampanyaClienteById(Long campanyaClienteId) {
        return campanyaClienteDAO.getCampanyaClienteById(campanyaClienteId);
    }

    public CampanyaCliente addCampanyaClienteFormulario(UIEntity clienteGeneralUI, UIEntity clienteUI, List<UIEntity> listaObjDatosExtra, Long campanyaId, String formatoFechaOrigen)
            throws Exception {

        ClienteGeneral clienteGeneral = clienteGeneralService.getClienteGeneralById(ParamUtils.parseLong(clienteGeneralUI.get("id")));
        clienteGeneral.setIdentificacion(clienteGeneralUI.get("identificacion"));

        Tipo tipoIdentificacion = new Tipo();
        tipoIdentificacion.setId(ParamUtils.parseLong(clienteGeneralUI.get("tipoIdentificacion")));
        clienteGeneral.setTipoIdentificacion(tipoIdentificacion);

        clienteGeneralService.update(clienteGeneral);

        Cliente clienteAux = clienteService.getClienteById(ParamUtils.parseLong(clienteUI.get("id")));
        clienteAux.setApellidos(clienteUI.get("apellidos"));
        clienteAux.setCorreo(clienteUI.get("correo"));
        clienteAux.setMovil(clienteUI.get("movil"));
        clienteAux.setPrefijoTelefono(clienteUI.get("prefijoTelefono"));
        clienteAux.setNombre(clienteUI.get("nombre"));
        clienteAux.setPostal(clienteUI.get("postal"));

        Cliente cliente = clienteService.updateCliente(clienteAux);

        formularioService.insertarDatosExtraCliente(cliente, listaObjDatosExtra, formatoFechaOrigen);

        return getCampanyaClienteByCampanyaAndClienteId(campanyaId, cliente.getId());
    }

    public CampanyaCliente getCampanyaClienteByCampanyaAndClienteId(Long campanyaId, Long clienteId) {
        return campanyaClienteDAO.getCampanyaClienteByCampanyaAndclienteId(campanyaId, clienteId);
    }

    public List<CampanyaCliente> getCampanyaClienteByClienteId(Long clienteId) {

        return campanyaClienteDAO.getCampanyaClienteByClienteId(clienteId);
    }

    public CampanyaCliente updateCampanyaCliente(UIEntity entity) throws ParseException, SQLException {
        Long campanyaId = ParamUtils.parseLong(entity.get("campanyaId"));
        Long clienteId = ParamUtils.parseLong(entity.get("clienteId"));

        CampanyaCliente campanyaCliente = getCampanyaClienteByCampanyaAndClienteId(campanyaId, clienteId);
        if (ParamUtils.isNotNull(campanyaCliente)) {
            TipoEstadoCampanya campanyaClienteTipo = new TipoEstadoCampanya();
            campanyaClienteTipo.setId(Long.parseLong(entity.get("campanyaClienteTipoId")));
            campanyaCliente.setCampanyaClienteTipo(campanyaClienteTipo);

            campanyaClienteDAO.update(campanyaCliente);
            gestionaCampanyaCliente(campanyaCliente);
            return campanyaCliente;
        }
        return null;
    }

    public ByteArrayOutputStream getExcelCampanyaCliente(FiltroCliente filtro) {
        try {
            List<CampanyaClienteBusqueda> campanyaClientes = campanyaClienteDAO.getCampanyaClientesByBusqueda(filtro, null);
            if (ParamUtils.isNotNull(campanyaClientes)) {
                WriterExcelService writerExcelService = new WriterExcelService();
                cabeceraClientes(writerExcelService);
                cuerpoClientes(writerExcelService, campanyaClientes);
                writerExcelService.autoSizeColumns(5);
                return writerExcelService.getExcel();
            } else return null;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void cabeceraClientes(WriterExcelService writerExcelService) {
        Integer rownum = 0;
        writerExcelService.addFulla("Resultados búsqueda");

        Row resumen = writerExcelService.getNewRow(rownum);
        writerExcelService.addCell(0, "Dades de clients campanyes", writerExcelService.getEstilNegreta(), resumen);
        writerExcelService.addMergedRegion(rownum, rownum, 0, 6);

        String[] columnas = new String[]{"DNI", "Client", "Correu", "Estat", "Data Estat", "Nombre seguiments"};
        writerExcelService.generaCeldes(writerExcelService.getEstilNegreta(), 2, columnas);
    }

    private void cuerpoClientes(WriterExcelService writerExcelService, List<CampanyaClienteBusqueda> clientes) {
        Integer rownum = 2;

        for (CampanyaClienteBusqueda clienteGrid : clientes) {
            int cellnum = 0;
            Row row = writerExcelService.getNewRow(++rownum);
            writerExcelService.addCell(cellnum++, clienteGrid.getIdentificacion(), null, row);
            writerExcelService.addCell(cellnum++, clienteGrid.getCliente().getNombreApellidos(), null, row);
            writerExcelService.addCell(cellnum++, clienteGrid.getCorreo(), null, row);
            writerExcelService.addCell(cellnum++, clienteGrid.getEstadoCampanya().getNombre(), null, row);
            writerExcelService.addCell(cellnum++, clienteGrid.getFechaEstado(), null, row);
            writerExcelService.addCell(cellnum++, clienteGrid.getNumSeguimientos().toString(), null, row);
        }
    }

    public List<CampanyaCliente> getCampanyaClienteByClienteIdAndCampanyaPadreId(Long clienteId, Long campanyaPadreId) {
        return campanyaClienteDAO.getCampanyaClienteByClienteIdAndCampanyaPadreId(clienteId, campanyaPadreId);
    }
}