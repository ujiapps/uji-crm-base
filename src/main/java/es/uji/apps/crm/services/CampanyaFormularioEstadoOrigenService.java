package es.uji.apps.crm.services;

import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaFormularioEstadoOrigenDAO;
import es.uji.apps.crm.model.CampanyaFormulario;
import es.uji.apps.crm.model.CampanyaFormularioEstadoOrigen;
import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.commons.rest.ParamUtils;

@Service
public class CampanyaFormularioEstadoOrigenService {

    private CampanyaFormularioEstadoOrigenDAO campanyaFormularioEstadoOrigenDAO;

    @Autowired
    public CampanyaFormularioEstadoOrigenService(CampanyaFormularioEstadoOrigenDAO campanyaFormularioEstadoOrigenDAO) {
        this.campanyaFormularioEstadoOrigenDAO = campanyaFormularioEstadoOrigenDAO;
    }


    public List<CampanyaFormularioEstadoOrigen> getCampanyaFormularioEstadosOrigenByFormulario(Long formularioId) {
        return campanyaFormularioEstadoOrigenDAO.getCampanyaFormularioEstadoOrigenByFormulario(formularioId);
    }

    public CampanyaFormularioEstadoOrigen addCampanyaFormularioEstadoOrigen(CampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen) {

        campanyaFormularioEstadoOrigenDAO.insert(campanyaFormularioEstadoOrigen);
        return campanyaFormularioEstadoOrigen;
    }

    public void deleteCampanyaFormularioEstadoOrigen(Long campanyaFormularioEstadoOrigenId) {
        campanyaFormularioEstadoOrigenDAO.delete(CampanyaFormularioEstadoOrigen.class, campanyaFormularioEstadoOrigenId);
    }

    public void insertarCampanyaFormularioEstadoOrigen(CampanyaFormulario campanyaFormulario, JSONObject campanyaFormularioEstadoOrigenJsonObject)
            throws JSONException {
        CampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen = new CampanyaFormularioEstadoOrigen();

        campanyaFormularioEstadoOrigen.setCampanyaFormulario(campanyaFormulario);

        if (ParamUtils.isNotNull(campanyaFormularioEstadoOrigenJsonObject.getString("id")) && campanyaFormularioEstadoOrigenJsonObject.getString("id") != "null")
        {
            TipoEstadoCampanya estadoCampanyaOrigen = new TipoEstadoCampanya();
            estadoCampanyaOrigen.setId(ParamUtils.parseLong(campanyaFormularioEstadoOrigenJsonObject.getString("id")));
            campanyaFormularioEstadoOrigen.setEstadoOrigen(estadoCampanyaOrigen);
        }

        campanyaFormularioEstadoOrigenDAO.insert(campanyaFormularioEstadoOrigen);
    }

    public void insertarCampanyaFormularioEstadoOrigen(CampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen) {

        campanyaFormularioEstadoOrigenDAO.insert(campanyaFormularioEstadoOrigen);
    }

    public void deleteCampanyaFormularioEstadoOrigenByCampanyaFormularioId(CampanyaFormulario campanyaFormulario) {

        campanyaFormularioEstadoOrigenDAO.deleteCampanyaFormularioEstadoOrigenByCampanyaFormularioId(campanyaFormulario);
    }
}