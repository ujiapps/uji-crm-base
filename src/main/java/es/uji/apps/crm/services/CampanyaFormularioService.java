package es.uji.apps.crm.services;

import java.util.Date;
import java.util.List;
import java.util.Set;

import es.uji.apps.crm.model.*;
import es.uji.apps.crm.model.domains.TipoSolicitudCampo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import es.uji.apps.crm.dao.CampanyaFormularioDAO;
import es.uji.apps.crm.model.domains.TipoAfiliacionUsuarioCampanya;

@Service
public class CampanyaFormularioService {

    private CampanyaFormularioDAO campanyaFormularioDAO;
    private CampanyaDatoService campanyaDatoService;
    private CampanyaFormularioEstadoOrigenService campanyaFormularioEstadoOrigenService;
    private TipoEstadoCampanyaService tipoEstadoCampanyaService;
    private CampanyaActoService campanyaActoService;

    @Autowired
    public CampanyaFormularioService(CampanyaFormularioDAO campanyaFormularioDAO, CampanyaDatoService campanyaDatoService, CampanyaActoService campanyaActoService,
                                     CampanyaFormularioEstadoOrigenService campanyaFormularioEstadoOrigenService, TipoEstadoCampanyaService tipoEstadoCampanyaService) {
        this.campanyaFormularioDAO = campanyaFormularioDAO;
        this.campanyaDatoService = campanyaDatoService;
        this.campanyaFormularioEstadoOrigenService = campanyaFormularioEstadoOrigenService;
        this.tipoEstadoCampanyaService = tipoEstadoCampanyaService;
        this.campanyaActoService = campanyaActoService;
    }

    public List<CampanyaFormulario> getCampanyaFormulariosByCampanya(Long campanyaId) {
        Campanya campanya = new Campanya();
        campanya.setId(campanyaId);

        return campanyaFormularioDAO.getCampanyaFormulariosByCampanya(campanya);
    }

    public CampanyaFormulario addCampanyaFormulario(CampanyaFormulario campanyaFormulario) {
        return campanyaFormularioDAO.insert(campanyaFormulario);
    }

    public void updateCampanyaFormulario(CampanyaFormulario campanyaFormulario) {
        campanyaFormularioDAO.update(campanyaFormulario);
    }

    public void deleteCampanyaFormulario(Long campanyaFormularioId) {

        campanyaFormularioDAO.deleteFormularioDatos(campanyaFormularioId);
        campanyaFormularioDAO.deleteFormularioEstados(campanyaFormularioId);

        campanyaFormularioDAO.delete(CampanyaFormulario.class, campanyaFormularioId);
    }


    public CampanyaFormulario getFormularioById(Long formularioId) {
        return campanyaFormularioDAO.getFormularioById(formularioId);
    }

    public CampanyaFormulario getFormularioByCampanyaIdAndUsuario(Long campanyaId, Long connectedUserId) {

        return campanyaFormularioDAO.getFormularioByCampanyaIdAndUsuario(campanyaId, connectedUserId);
    }

    public CampanyaFormulario getCampanyaFormulariosByCampanyaAndEstado(Long campanyaId, Long estadoId) {
        return campanyaFormularioDAO.getCampanyaFormulariosByCampanyaAndEstado(campanyaId, estadoId);
    }

    public CampanyaFormulario duplicarFormulario(CampanyaFormulario campanyaFormulario) {
        List<CampanyaDato> campanyaFormularioDatos = campanyaDatoService.getCampanyaDatosSinDatoAsociadoByFormularioId(campanyaFormulario.getId());
        List<CampanyaFormularioEstadoOrigen> campanyaFormularioEstadosOrigen = campanyaFormularioEstadoOrigenService.getCampanyaFormularioEstadosOrigenByFormulario(campanyaFormulario.getId());

        CampanyaFormulario duplicado = new CampanyaFormulario();
        duplicado.setFechaInicio(campanyaFormulario.getFechaInicio());
        duplicado.setEstadoDestino(campanyaFormulario.getEstadoDestino());
        duplicado.setAutenticaFormulario(campanyaFormulario.getAutenticaFormulario());
        duplicado.setCampanya(campanyaFormulario.getCampanya());
        duplicado.setFechaFin(campanyaFormulario.getFechaFin());
        duplicado.setSolicitarCorreo(campanyaFormulario.getSolicitarCorreo());
        duplicado.setSolicitarIdentificacion(campanyaFormulario.getSolicitarIdentificacion());
        duplicado.setSolicitarMovil(campanyaFormulario.getSolicitarMovil());
        duplicado.setSolicitarNombre(campanyaFormulario.getSolicitarNombre());
        duplicado.setSolicitarPostal(campanyaFormulario.getSolicitarPostal());
        duplicado.setCabecera(campanyaFormulario.getCabecera());
        duplicado.setPlantilla(campanyaFormulario.getPlantilla());
        duplicado.setTitulo(campanyaFormulario.getTitulo() + "DUPLICADO");

        campanyaFormularioDAO.insert(duplicado);

        for (CampanyaDato campanyaDato : campanyaFormularioDatos) {
            duplicarDato(campanyaDato, duplicado, null);
        }

        for (CampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen : campanyaFormularioEstadosOrigen) {
            CampanyaFormularioEstadoOrigen duplicadoEstado = new CampanyaFormularioEstadoOrigen();
            duplicadoEstado.setCampanyaFormulario(duplicado);
            duplicadoEstado.setEstadoOrigen(campanyaFormularioEstadoOrigen.getEstadoOrigen());
            campanyaFormularioEstadoOrigenService.insertarCampanyaFormularioEstadoOrigen(duplicadoEstado);
        }

        return duplicado;

    }

    public CampanyaFormulario getFormularioGenericoByCampanya(Long campanyaId) {
        return campanyaFormularioDAO.getFormularioGenericoByCampanya(campanyaId);

    }

    private void duplicarDato(CampanyaDato campanyaDato, CampanyaFormulario formulario, CampanyaDato campanyaDatoId) {
        CampanyaDato duplicadoCampanyaDato = new CampanyaDato();
        duplicadoCampanyaDato.setCampanyaFormulario(formulario);
        duplicadoCampanyaDato.setCampanyaDato(campanyaDatoId);
        duplicadoCampanyaDato.setClienteDatoTipo(campanyaDato.getClienteDatoTipo());
        duplicadoCampanyaDato.setGrupo(campanyaDato.getGrupo());
        duplicadoCampanyaDato.setItem(campanyaDato.getItem());
        duplicadoCampanyaDato.setOrden(campanyaDato.getOrden());
        duplicadoCampanyaDato.setRequerido(campanyaDato.getRequerido());
        duplicadoCampanyaDato.setTamanyo(campanyaDato.getTamanyo());
        duplicadoCampanyaDato.setTextoCa(campanyaDato.getTextoCa());
        duplicadoCampanyaDato.setTextoEs(campanyaDato.getTextoEs());
        duplicadoCampanyaDato.setTextoUk(campanyaDato.getTextoUk());
        duplicadoCampanyaDato.setTipoAcceso(campanyaDato.getTipoAcceso());
        duplicadoCampanyaDato.setVinculadoCampanya(campanyaDato.getVinculadoCampanya());
        duplicadoCampanyaDato.setVisualizar(campanyaDato.getVisualizar());

        campanyaDatoService.addCampanyaDato(duplicadoCampanyaDato);
        for (CampanyaDato campanyaDatoRef : campanyaDato.getCampanyaDatos()) {
            duplicarDato(campanyaDatoRef, formulario, duplicadoCampanyaDato);
        }
    }

    public void addCampanyaFormulariosGenericaActo(Campanya campanya, TipoAfiliacionUsuarioCampanya estadoDestino) {

        CampanyaActo campanyaActo = campanyaActoService.getCampanyasActoByCampanya(campanya).get(0);

        addCampanyaFormularioGenericaActoAlta(campanya, campanyaActo, estadoDestino);
        addCampanyaFormularioGenericaActoBaja(campanya, campanyaActo);
    }

    private void addCampanyaFormularioGenericaActoAlta(Campanya campanya, CampanyaActo campanyaActo, TipoAfiliacionUsuarioCampanya estadoDestino) {

        CampanyaFormulario campanyaFormulario = new CampanyaFormulario();
        campanyaFormulario.setTitulo("Vull assistir a l’acte de graduació!");

        campanyaFormulario.setAutenticaFormulario(Boolean.TRUE);
        campanyaFormulario.setCampanya(campanya);
        campanyaFormulario.setFechaFin(new Date());
        campanyaFormulario.setFechaInicio(new Date());

        campanyaFormulario.setSolicitarCorreo(TipoSolicitudCampo.NO_SOLICITAR.getId());
        campanyaFormulario.setSolicitarIdentificacion(TipoSolicitudCampo.NO_SOLICITAR.getId());
        campanyaFormulario.setSolicitarMovil(TipoSolicitudCampo.NO_SOLICITAR.getId());
        campanyaFormulario.setSolicitarNombre(TipoSolicitudCampo.NO_SOLICITAR.getId());
        campanyaFormulario.setSolicitarPostal(TipoSolicitudCampo.NO_SOLICITAR.getId());
        campanyaFormulario.setPlantilla("");
        campanyaFormulario.setCabecera("");

        TipoEstadoCampanya tipoEstadoCampanyaDestino = tipoEstadoCampanyaService.getTipoEstadoCampanyaByAccion(campanya.getId(), estadoDestino.getTipo());
        campanyaFormulario.setEstadoDestino(tipoEstadoCampanyaDestino);

        campanyaFormulario.setActo(campanyaActo);

        addCampanyaFormulario(campanyaFormulario);

        CampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen = new CampanyaFormularioEstadoOrigen();
        campanyaFormularioEstadoOrigen.setCampanyaFormulario(campanyaFormulario);
        TipoEstadoCampanya tipoEstadoCampanyaPropuestoOrigen = tipoEstadoCampanyaService.getTipoEstadoCampanyaByAccion(campanya.getId(), TipoAfiliacionUsuarioCampanya.PROPUESTO.getTipo());
        campanyaFormularioEstadoOrigen.setEstadoOrigen(tipoEstadoCampanyaPropuestoOrigen);
        campanyaFormularioEstadoOrigenService.addCampanyaFormularioEstadoOrigen(campanyaFormularioEstadoOrigen);


        campanyaDatoService.addCampanyaDatosGenericaActoAlta(campanyaFormulario);

    }


    private void addCampanyaFormularioGenericaActoBaja(Campanya campanya, CampanyaActo campanyaActo) {

        CampanyaFormulario campanyaFormulario = new CampanyaFormulario();
        campanyaFormulario.setTitulo("No assistiré a l’acte de graduació");

        campanyaFormulario.setAutenticaFormulario(Boolean.TRUE);
        campanyaFormulario.setCampanya(campanya);
        campanyaFormulario.setFechaFin(new Date());
        campanyaFormulario.setFechaInicio(new Date());

        campanyaFormulario.setSolicitarCorreo(TipoSolicitudCampo.NO_SOLICITAR.getId());
        campanyaFormulario.setSolicitarIdentificacion(TipoSolicitudCampo.NO_SOLICITAR.getId());
        campanyaFormulario.setSolicitarMovil(TipoSolicitudCampo.NO_SOLICITAR.getId());
        campanyaFormulario.setSolicitarNombre(TipoSolicitudCampo.NO_SOLICITAR.getId());
        campanyaFormulario.setSolicitarPostal(TipoSolicitudCampo.NO_SOLICITAR.getId());
        campanyaFormulario.setPlantilla("");
        campanyaFormulario.setCabecera("");

        TipoEstadoCampanya tipoEstadoCampanyaBajaDestino = tipoEstadoCampanyaService.getTipoEstadoCampanyaByAccion(campanya.getId(), TipoAfiliacionUsuarioCampanya.BAJAWEB.getTipo());
        campanyaFormulario.setEstadoDestino(tipoEstadoCampanyaBajaDestino);

        campanyaFormulario.setActo(campanyaActo);
        addCampanyaFormulario(campanyaFormulario);

        CampanyaFormularioEstadoOrigen campanyaFormularioEstadoOrigen = new CampanyaFormularioEstadoOrigen();
        campanyaFormularioEstadoOrigen.setCampanyaFormulario(campanyaFormulario);
        campanyaFormularioEstadoOrigen.setEstadoOrigen(null);
        campanyaFormularioEstadoOrigenService.addCampanyaFormularioEstadoOrigen(campanyaFormularioEstadoOrigen);


        campanyaDatoService.addCampanyaDatosGenericaActoBaja(campanyaFormulario);

    }

    public CampanyaFormulario getFormularioByFormularioId(Long formularioId) {
        return campanyaFormularioDAO.getFormularioByFormularioIdSinUsuario(formularioId);
    }

    public CampanyaFormulario getFormularioByFormularioId(Long formularioId, Long clienteId) {
        return campanyaFormularioDAO.getFormularioByFormularioId(formularioId, clienteId);
    }

    public CampanyaFormulario getFormularioByCampanyaIdAndCliente(Long campanyaId, Long clienteId) {
        return campanyaFormularioDAO.getFormularioByCampanyaIdAndCliente(campanyaId, clienteId);
    }
}
