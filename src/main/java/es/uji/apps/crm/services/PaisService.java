package es.uji.apps.crm.services;

import java.util.List;

import es.uji.apps.crm.ui.OpcionUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.PaisDAO;
import es.uji.apps.crm.model.Pais;

@Service
public class PaisService {
    private PaisDAO paisDAO;

    @Autowired
    public PaisService(PaisDAO paisDAO) {
        this.paisDAO = paisDAO;
    }

    public List<Pais> getPaises() {
        return paisDAO.getPaises();
    }

    public List<Pais> getPaisesOrdenadosPorIdioma(String idioma) {
        return paisDAO.getPaisesOrdenadosPorIdioma(idioma);
    }

    public List<OpcionUI> getOpcionPaisesOrdenadosPorIdioma(String idioma) {
        return paisDAO.getOpcionPaisesOrdenadosPorIdioma(idioma);
    }


//    public List<Pais> ordenaPaises(List<Pais> paises, String idioma) {
//        List<Pais> listaOrdenada = new ArrayList<Pais>();
//        Boolean encontrado = Boolean.FALSE;
//        Integer indice = 0;
//        while (!encontrado) {
//            if (paises.get(indice).getId().equals("E")) {
//                listaOrdenada.add(paises.get(indice));
//                encontrado = Boolean.TRUE;
//            }
//            indice++;
//        }
//        return null;
//    }
}