package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.TipoEstadoCampanyaMotivoDAO;
import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.apps.crm.model.TipoEstadoCampanyaMotivo;

@Service
public class TipoEstadoCampanyaMotivoService {
    private TipoEstadoCampanyaMotivoDAO tipoEstadoCampanyaMotivoDAO;

    @Autowired
    public TipoEstadoCampanyaMotivoService(TipoEstadoCampanyaMotivoDAO tipoEstadoCampanyaMotivoDAO) {
        this.tipoEstadoCampanyaMotivoDAO = tipoEstadoCampanyaMotivoDAO;

    }

    public List<TipoEstadoCampanyaMotivo> getTiposEstadoCampanyaByCampanyaId(Long campanyaEstado) {
        return tipoEstadoCampanyaMotivoDAO.getTiposEstadoCampanyaMotivosByEstadoCampanyaId(campanyaEstado);
    }

    public TipoEstadoCampanyaMotivo updateTipoEstadoCampanyaByCampanyaId(TipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivo) {
        return tipoEstadoCampanyaMotivoDAO.update(tipoEstadoCampanyaMotivo);
    }

    public void deleteTipoEstadoCampanyaMotivo(Long tipoEstadoCampanyaMotivoId) {
        tipoEstadoCampanyaMotivoDAO.delete(TipoEstadoCampanyaMotivo.class, tipoEstadoCampanyaMotivoId);
    }

    public void deleteTiposEstadoCampanyaMotivo(Long tipoEstadoCampanyaId) {
        tipoEstadoCampanyaMotivoDAO.delete(TipoEstadoCampanyaMotivo.class, "tipoEstadoCampanya=" + tipoEstadoCampanyaId);
    }

    public TipoEstadoCampanyaMotivo getTipoEstadoCampanyaById(Long campanyaEstadoMotivoId) {
        return tipoEstadoCampanyaMotivoDAO.getTipoEstadoCampanyaById(campanyaEstadoMotivoId);
    }

    public void addTiposEstadoCampanyaMotivoGenericaActo(TipoEstadoCampanya tipoEstadoCampanyaBaja) {

        TipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivoFecha = new TipoEstadoCampanyaMotivo();
        tipoEstadoCampanyaMotivoFecha.setTipoEstadoCampanya(tipoEstadoCampanyaBaja);
        tipoEstadoCampanyaMotivoFecha.setNombreCa("Considere que la data i/o hora no són adequades");
        tipoEstadoCampanyaMotivoFecha.setNombreEs("Considero que la fecha y/o la hora no son adecuadas");
        tipoEstadoCampanyaMotivoFecha.setNombreUk("-");
        addTipoEstadoCampanyaByCampanyaId(tipoEstadoCampanyaMotivoFecha);

        TipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivoResidencia = new TipoEstadoCampanyaMotivo();
        tipoEstadoCampanyaMotivoResidencia.setTipoEstadoCampanya(tipoEstadoCampanyaBaja);
        tipoEstadoCampanyaMotivoResidencia.setNombreCa("Estic residint fora de Castelló");
        tipoEstadoCampanyaMotivoResidencia.setNombreEs("Estoy residiendo fuera de Castellón");
        tipoEstadoCampanyaMotivoResidencia.setNombreUk("-");
        addTipoEstadoCampanyaByCampanyaId(tipoEstadoCampanyaMotivoResidencia);

        TipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivoTrabajo = new TipoEstadoCampanyaMotivo();
        tipoEstadoCampanyaMotivoTrabajo.setTipoEstadoCampanya(tipoEstadoCampanyaBaja);
        tipoEstadoCampanyaMotivoTrabajo.setNombreCa("Estic treballant el dia que es realitza l'acte de graduació");
        tipoEstadoCampanyaMotivoTrabajo.setNombreEs("Estoy trabajando el día que se realiza el acto de graduación");
        tipoEstadoCampanyaMotivoTrabajo.setNombreUk("-");
        addTipoEstadoCampanyaByCampanyaId(tipoEstadoCampanyaMotivoTrabajo);

        TipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivoActo = new TipoEstadoCampanyaMotivo();
        tipoEstadoCampanyaMotivoActo.setTipoEstadoCampanya(tipoEstadoCampanyaBaja);
        tipoEstadoCampanyaMotivoActo.setNombreCa("He participat en altre acte de graduació");
        tipoEstadoCampanyaMotivoActo.setNombreEs("He participado en otro acto de graduación");
        tipoEstadoCampanyaMotivoActo.setNombreUk("-");
        addTipoEstadoCampanyaByCampanyaId(tipoEstadoCampanyaMotivoActo);

        TipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivoContacto = new TipoEstadoCampanyaMotivo();
        tipoEstadoCampanyaMotivoContacto.setTipoEstadoCampanya(tipoEstadoCampanyaBaja);
        tipoEstadoCampanyaMotivoContacto.setNombreCa("No tinc contacte amb la resta de companys i companyes");
        tipoEstadoCampanyaMotivoContacto.setNombreEs("No tengo contacto con el resto de compañeros y compañeras");
        tipoEstadoCampanyaMotivoContacto.setNombreUk("-");
        addTipoEstadoCampanyaByCampanyaId(tipoEstadoCampanyaMotivoContacto);

        TipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivoUniversidad = new TipoEstadoCampanyaMotivo();
        tipoEstadoCampanyaMotivoUniversidad.setTipoEstadoCampanya(tipoEstadoCampanyaBaja);
        tipoEstadoCampanyaMotivoUniversidad.setNombreCa("No vull participar en activitats organitzades per la Universitat");
        tipoEstadoCampanyaMotivoUniversidad.setNombreEs("No quiero participar en actividades organizadas por la Universitat");
        tipoEstadoCampanyaMotivoUniversidad.setNombreUk("-");
        addTipoEstadoCampanyaByCampanyaId(tipoEstadoCampanyaMotivoUniversidad);

    }

    public TipoEstadoCampanyaMotivo addTipoEstadoCampanyaByCampanyaId(TipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivo) {
        return tipoEstadoCampanyaMotivoDAO.insert(tipoEstadoCampanyaMotivo);
    }


}