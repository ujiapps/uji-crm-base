package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Programa;
import es.uji.apps.crm.model.ProgramaCurso;
import es.uji.apps.crm.model.Ubicacion;
import es.uji.apps.crm.services.ProgramaCursoService;
import es.uji.apps.crm.services.UbicacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("programacurso")
public class ProgramaCursoResource extends CoreBaseService {
    @InjectParam
    private ProgramaCursoService programaCursoService;
    @InjectParam
    private UbicacionService ubicacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getProgramaPerfilesByProgramaId(@QueryParam("programaId") Long programaId) {
        List<ProgramaCurso> lista = programaCursoService.getProgramaCursosByProgramaId(programaId);
        return modelToUI(lista);
    }

    private List<UIEntity> modelToUI(List<ProgramaCurso> programaCursos) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (ProgramaCurso programaCurso : programaCursos)
        {
            listaUI.add(modelToUI(programaCurso));
        }
        return listaUI;
    }

    private UIEntity modelToUI(ProgramaCurso programaCurso) {
        UIEntity entity = UIEntity.toUI(programaCurso);

        entity.put("uestNombre", programaCurso.getUest().getNombre());
        return entity;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addProgramaCurso(UIEntity entity) {
        ProgramaCurso programaCurso = UIToModel(entity);
        programaCursoService.addProgramaCurso(programaCurso);
        return modelToUI(programaCurso);
    }

    private ProgramaCurso UIToModel(UIEntity entity) {
        ProgramaCurso programaCurso = entity.toModel(ProgramaCurso.class);

        Programa programa = new Programa();
        programa.setId(Long.parseLong(entity.get("programaId")));
        programaCurso.setPrograma(programa);

        Ubicacion ubicacion = ubicacionService.getUbicacion(Long.parseLong(entity.get("uestId")));
        programaCurso.setUest(ubicacion);
        return programaCurso;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateProgramaCurso(@PathParam("id") Long programaCursoId, UIEntity entity) {
        ProgramaCurso programaCurso = UIToModel(entity);
        programaCursoService.updateProgramaCurso(programaCurso);
        return modelToUI(programaCurso);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long programaCursoId) {
        programaCursoService.deleteProgramaCurso(programaCursoId);
    }

}