package es.uji.apps.crm.services;

import es.uji.apps.crm.auth.UserSessionManager;
import es.uji.apps.crm.model.*;
import es.uji.apps.crm.model.datos.Personal;
import es.uji.apps.crm.ui.ItemUI;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.web.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public class AlumniPersonalesService {

    private final static Long CAMPANYAPREMIUM = 1070562L;
    private final static Long CUENTA = 17L;

    private final UserSessionManager userSessionManager;
    private final ClienteDatoService clienteDatoService;
    private final ClienteItemService clienteItemService;
    private final ItemService itemService;
    private final ProvinciaService provinciaService;
    private final AlumniService alumniService;
    private final PaisService paisService;

    @Autowired
    public AlumniPersonalesService(UserSessionManager userSessionManager,
                                   ClienteDatoService clienteDatoService,
                                   ClienteItemService clienteItemService,
                                   ItemService itemService,
                                   ProvinciaService provinciaService,
                                   AlumniService alumniService,
                                   PaisService paisService) {
        this.userSessionManager = userSessionManager;
        this.clienteDatoService = clienteDatoService;
        this.clienteItemService = clienteItemService;
        this.itemService = itemService;
        this.provinciaService = provinciaService;
        this.alumniService = alumniService;
        this.paisService = paisService;
    }

    public Personal getAlumniPersonal(ClienteGeneral clienteGeneral, Cliente cliente) {
        List<ClienteDato> listaDatosCliente = clienteDatoService.getAllDatosByClienteId(cliente.getId());
        List<ClienteItem> listaItemsCliente = clienteItemService.getAllItemsByClienteId(cliente.getId());
        return new Personal(clienteGeneral, cliente, listaDatosCliente, listaItemsCliente);
    }

    public Template getTemplatePersonales(String path, String idioma) throws ParseException {
        Template template = alumniService.getTemplateBaseAlumni(path, idioma, "personales");

        List<Item> paisNacimiento = itemService.getItemsByGrupId(3276215L);
        template.put("paisNacimiento", ItemUI.toUI(paisNacimiento, idioma));
        template.put("paises", paisService.getPaisesOrdenadosPorIdioma(idioma));
        template.put("provincias", provinciaService.getProvincias(idioma));

        alumniService.introducePremiumEstadoEnPlantillaZonaPrivada(template);

        ClienteDato cuentaBancaria = clienteDatoService.getClienteDatoByClienteAndTipoIdVinculadoCampanya(userSessionManager.getCliente(), CUENTA, CAMPANYAPREMIUM);
        if (ParamUtils.isNotNull(cuentaBancaria)) {
            try {
                template.put("cuentaBancaria", cuentaBancaria.getValorSegunTipo());
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        } else {
            template.put("cuentaBancaria", "");
        }

        return template;
    }
}
