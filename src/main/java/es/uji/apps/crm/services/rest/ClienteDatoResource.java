package es.uji.apps.crm.services.rest;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.LazyInitializationException;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteDato;
import es.uji.apps.crm.model.ClienteDatoOpcion;
import es.uji.apps.crm.model.ClienteDatoTipo;
import es.uji.apps.crm.model.ClienteItem;
import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.model.domains.TipoAccesoDatoUsuario;
import es.uji.apps.crm.model.domains.TipoDato;
import es.uji.apps.crm.services.ClienteDatoOpcionService;
import es.uji.apps.crm.services.ClienteDatoService;
import es.uji.apps.crm.services.ClienteDatoTipoService;
import es.uji.apps.crm.services.ClienteItemService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StreamUtils;
import es.uji.commons.rest.UIEntity;

@Path("clientedato")
public class ClienteDatoResource extends CoreBaseService {
    private static final String VALOR_PARAM = "valor";
    @InjectParam
    private ClienteDatoService consultaClienteDato;
    @InjectParam
    private ClienteDatoTipoService consultaClienteDatoTipo;
    @InjectParam
    private ClienteDatoOpcionService consultaClienteDatoOpcion;
    @InjectParam
    private ClienteItemService clienteItemService;
    @InjectParam
    private UtilsService utilsService;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getClienteById(@PathParam("id") Long clienteDatoId) {
        ClienteDato clienteDato = consultaClienteDato.getClienteDatoById(clienteDatoId);
        UIEntity entity = modelToUI(clienteDato);

        String tipo = clienteDato.getClienteDatoTipo().getClienteDatoTipoTipo().getNombre();

        switch (tipo.substring(0, 1)) {
            case "N":
                entity.put("valor", clienteDato.getValorNumero());
                break;
            case "T":
                entity.put("valor", clienteDato.getValorTexto());
                break;
            case "F":
                entity.put("valor", utilsService.fechaFormat(clienteDato.getValorFecha()));
                break;
            case "S":
                entity.put("valor",
                        consultaClienteDatoOpcion
                                .getClienteDatoOpcionById(clienteDato.getValorNumero()).getEtiqueta());
                entity.put("valorSelect", clienteDato.getValorNumero());
                break;
            default:
                entity.put("valor", clienteDato.getValorFicheroNombre());
                break;
        }

        return entity;
    }

    private UIEntity modelToUI(ClienteDato clienteDato) {

        UIEntity entity = UIEntity.toUI(clienteDato);

        entity.put("tipoDatoId", clienteDato.getClienteDatoTipo().getId());

        ClienteDatoTipo clienteDatoTipo = clienteDato.getClienteDatoTipo();
        if (ParamUtils.isNotNull(clienteDatoTipo)) {
            entity.put("tipoDatoNombre", clienteDatoTipo.getNombre());
        }
        entity.put("tipoDatoTipoNombre", clienteDato.getClienteDatoTipo().getClienteDatoTipoTipo()
                .getNombre());
        entity.put("tipoDatoTipoId", clienteDato.getClienteDatoTipo().getClienteDatoTipoTipo()
                .getId());
        entity.put("tipoAccesoId", clienteDato.getClienteDatoTipoAcceso().getId());
        Tipo tipoAcceso = clienteDato.getClienteDatoTipoAcceso();
        if (ParamUtils.isNotNull(tipoAcceso)) {
            entity.put("tipoAccesoNombre", tipoAcceso.getNombre());
        }
        entity.put("clienteId", clienteDato.getCliente().getId());

        if (ParamUtils.isNotNull(clienteDato.getCampanya())) {
            entity.put("campanyaId", clienteDato.getCampanya().getId());
        }

        try {
            if (clienteDato.getClienteDatoTipo() != null
                    && clienteDato.getClienteDatoTipo().getClientesDatosOpciones() != null) {
                for (ClienteDatoOpcion clienteDatoOpcion : clienteDato.getClienteDatoTipo()
                        .getClientesDatosOpciones()) {
                    UIEntity uiOpcion = new UIEntity();
                    uiOpcion.put("id", clienteDatoOpcion.getId());
                    uiOpcion.put("nombre", clienteDatoOpcion.getEtiqueta());
                    uiOpcion.put("valor", clienteDatoOpcion.getValor());

                    entity.put("datosOpciones", uiOpcion);
                }
            }
        } catch (LazyInitializationException e) {
        }

        String tipo = clienteDato.getClienteDatoTipo().getClienteDatoTipoTipo().getNombre();

        if (tipo.substring(0, 1).equals(TipoDato.NUMERO.getNombre())) {
            entity.put("valor", clienteDato.getValorNumero());
        } else if (tipo.substring(0, 1).equals(TipoDato.TEXTO.getNombre())) {
            entity.put("valor", clienteDato.getValorTexto());
        } else if (tipo.substring(0, 1).equals(TipoDato.FECHA.getNombre())) {
            Date fecha = clienteDato.getValorFecha();
            entity.put("valor", (fecha != null) ? utilsService.fechaFormat(fecha) : null);
        } else if (tipo.substring(0, 1).equals(TipoDato.SELECCION.getNombre())) {
            entity.put("valor", clienteDato.getValorNumero());
        } else {
            entity.put("valor", clienteDato.getValorFicheroNombre());
        }
        return entity;
    }

    @GET
    @Path("tipo/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getClienteDatoTipoById(@PathParam("id") Long tipoId) {
        ClienteDatoTipo clienteDatoTipo = consultaClienteDatoTipo.getClienteDatoTipoById(tipoId);
        UIEntity entity = UIEntity.toUI(clienteDatoTipo);

        entity.put("clienteDatoTipoTipoNombre", clienteDatoTipo.getClienteDatoTipoTipo()
                .getNombre());

        return entity;
    }

    @GET
    @Path("tipo/{id}/opciones/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClienteDatoOpcionesByTipoId(@PathParam("id") Long tipoId) {
        List<ClienteDatoOpcion> clienteDatoOpcion = consultaClienteDatoTipo
                .getClienteDatoOpcionesByTipoId(tipoId);

        return UIEntity.toUI(clienteDatoOpcion);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addClienteDatoExtra(@FormParam("valor") String valor, @FormParam("clienteId") Long clienteId,
                                        @FormParam("tipoDatoId") Long tipoDatoId, @FormParam("tipoAccesoId") Long tipoAccesoId, @FormParam("comentarios") String comentarios,
                                        @FormParam("clienteDatoId") Long clienteDatoId, @FormParam("itemId") Long itemId, @FormParam("clienteItemId") Long clienteItemId, @FormParam("campanyaId") Long campanyaId,
                                        @FormParam("id") Long id)
            throws ParseException {

        ParamUtils.checkNotNull(valor, clienteId, tipoDatoId);

        if (ParamUtils.isNotNull(clienteItemId)) {
            ClienteItem clienteItem = clienteItemService.getClienteItemById(clienteItemId);
            itemId = clienteItem.getItem().getId();
        }

        if (ParamUtils.isNotNull(id)) { //Fichero
            return modelToUI(consultaClienteDato.updateClienteDato(id,  comentarios, tipoAccesoId));
        } else
            return modelToUI(consultaClienteDato.addClienteDatoExtra(clienteId, tipoDatoId, tipoAccesoId, comentarios, valor, clienteDatoId, itemId, campanyaId));

    }

    @POST
    @Path("fichero/")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity addClienteFichero(FormDataMultiPart multiPart) throws IOException {

        ClienteDato clienteDato = new ClienteDato();

        String clienteId = multiPart.getField("clienteId").getValue();
        String tipoId = multiPart.getField("tipoId").getValue();

        Cliente cliente = new Cliente();
        cliente.setId(ParamUtils.parseLong(clienteId));
        clienteDato.setCliente(cliente);

        ClienteDatoTipo clienteDatoTipo = new ClienteDatoTipo();
        clienteDatoTipo.setId(ParamUtils.parseLong(tipoId));
        clienteDato.setClienteDatoTipo(clienteDatoTipo);

        Tipo tipo = new Tipo();
        tipo.setId(TipoAccesoDatoUsuario.INTERNO.getId());
        clienteDato.setClienteDatoTipoAcceso(tipo);

        for (BodyPart bodyPart : multiPart.getBodyParts()) {
            String mimeType = bodyPart.getHeaders().getFirst("Content-Type");
            if (mimeType != null && !mimeType.isEmpty()) {
                String fileName = "";
                String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                Matcher m = fileNamePattern.matcher(header);
                if (m.matches()) {
                    fileName = m.group(1);
                }
                BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                InputStream documento = bpe.getInputStream();
                clienteDato.setValorFicheroMimetype(mimeType);
                clienteDato.setValorFicheroNombre(fileName);
                clienteDato.setValorFicheroBinario(StreamUtils.inputStreamToByteArray(documento));
                break;
            }
        }

        clienteDato = consultaClienteDato.insertaClienteDatoFichero(clienteDato);
        return modelToUI(clienteDato);
    }

    @POST
    @Path("fichero/{clienteId}/tipo/{tipoArchivoId}/raw")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity addClienteFicheroRaw(@PathParam("clienteId") Long clienteId, @PathParam("tipoArchivoId") Long tipoArchivoId, FormDataMultiPart multiPart) throws IOException {

        ClienteDato clienteDato = new ClienteDato();

//        String tipoId = multiPart.getField("tipoId").getValue();

        Cliente cliente = new Cliente();
        cliente.setId(clienteId);
        clienteDato.setCliente(cliente);

        ClienteDatoTipo clienteDatoTipo = new ClienteDatoTipo();
        clienteDatoTipo.setId(tipoArchivoId);
        clienteDato.setClienteDatoTipo(clienteDatoTipo);

        Tipo tipo = new Tipo();
        tipo.setId(TipoAccesoDatoUsuario.INTERNO.getId());
        clienteDato.setClienteDatoTipoAcceso(tipo);

        for (BodyPart bodyPart : multiPart.getBodyParts()) {
            String mimeType = bodyPart.getHeaders().getFirst("Content-Type");
            if (mimeType != null && !mimeType.isEmpty()) {
                String fileName = "";
                String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                Matcher m = fileNamePattern.matcher(header);
                if (m.matches()) {
                    fileName = m.group(1);
                }
                BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                InputStream documento = bpe.getInputStream();
                clienteDato.setValorFicheroMimetype(mimeType);
                clienteDato.setValorFicheroNombre(fileName);
                clienteDato.setValorFicheroBinario(StreamUtils.inputStreamToByteArray(documento));
                break;
            }
        }

        clienteDato = consultaClienteDato.insertaClienteDatoFichero(clienteDato);
        return UIEntity.toUI(clienteDato);
    }


    @POST
    @Path("entradasacto")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insertaEntradasActo(@FormParam("actoId") Long acto, @FormParam("nuevas") Long entradas) {

        consultaClienteDato.addClienteDatoExtraEntradas(acto, entradas);
        return null;
    }

    @POST
    @Path("entradasacto/cliente")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insertaEntradasActoCliente(@FormParam("actoId") Long acto, @FormParam("nuevas") Long entradas, @FormParam("clienteId") Long cliente) {

        consultaClienteDato.addClienteDatoExtraEntradasCliente(acto, entradas, cliente);
        return null;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateClienteDatoExtra(@PathParam("id") Long clienteDatoId,
                                           @FormParam("valor") String valor,
                                           @FormParam("comentarios") String comentarios,
                                           @FormParam("tipoAcceso") Long tipoAcceso) throws ParseException {

        ClienteDato clienteDato = consultaClienteDato.updateClienteDato(clienteDatoId, valor, comentarios, tipoAcceso);
        return modelToUI(clienteDato);
    }

    @PUT
    @Path("{id}/cliente/{clienteId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateClienteDatoExtraCliente(@PathParam("id") Long clienteDatoId,
                                                  @PathParam("clienteId") Long clienteId) {
        ClienteDato clienteDato = consultaClienteDato.updateClienteDatoClienteId(clienteDatoId,
                clienteId);
        return modelToUI(clienteDato);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteDatoExtra(@PathParam("id") Long datoId) {
        consultaClienteDato.delete(datoId);
    }

    private List<UIEntity> modelToUI(List<ClienteDato> clienteDatos) {
        List<UIEntity> listUIEntity = new ArrayList<>();

        for (ClienteDato clienteDato : clienteDatos) {
            UIEntity entity = modelToUI(clienteDato);
            listUIEntity.add(entity);
        }
        return listUIEntity;
    }

    private ClienteDato UIToModel(UIEntity entity) {
        ClienteDato clienteDato = entity.toModel(ClienteDato.class);

        Cliente cliente = new Cliente();
        cliente.setId(ParamUtils.parseLong(entity.get("clienteId")));
        clienteDato.setCliente(cliente);

        ClienteDatoTipo clienteDatoTipo = consultaClienteDato.getClienteDatoTipoById(ParamUtils
                .parseLong(entity.get("tipoDatoId")));

        clienteDato.setClienteDatoTipo(clienteDatoTipo);

        Tipo tipo = new Tipo();

        String tipoAccesoId = entity.get("tipoAccesoId");

        if (tipoAccesoId == null) {
            tipo.setId(TipoAccesoDatoUsuario.INTERNO.getId());
        } else {
            tipo.setId(ParamUtils.parseLong(tipoAccesoId));
        }
        clienteDato.setClienteDatoTipoAcceso(tipo);

        return clienteDato;
    }
}