package es.uji.apps.crm.services;

import es.uji.apps.crm.model.*;
import es.uji.apps.crm.model.domains.TipoAccesoDatoUsuario;
import es.uji.apps.crm.ui.ItemUI;
import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import es.uji.commons.web.template.Template;

import java.text.ParseException;
import java.util.List;


@Service
public class AlumniLaboralesService {

    private final static Long GRUPOLABORAL = 807418L;
    private final static Long EMPRESA = 7L;
    private final static Long CARGO = 8L;
    private final static Long TIPO_NOMBRE_ESTUDIO_ACTUAL = 6460937L;
    private final static Long PERFILLINKEDIN = 1645952L;

    private final ItemService itemService;
    private final ClienteItemService clienteItemService;
    private final ClienteDatoService clienteDatoService;
    private final AlumniService alumniService;
    private final ClienteDato clienteDato;
    private final Cliente cliente;

    @Autowired
    public AlumniLaboralesService(ItemService itemService,
                                  ClienteItemService clienteItemService,
                                  ClienteDatoService clienteDatoService, AlumniService alumniService, ClienteDato clienteDato, Cliente cliente) {
        this.itemService = itemService;
        this.clienteItemService = clienteItemService;
        this.clienteDatoService = clienteDatoService;
        this.alumniService = alumniService;
        this.clienteDato = clienteDato;
        this.cliente = cliente;
    }

    public Template getTemplateLaboral(String path, String idioma) throws ParseException {

        Template template = alumniService.getTemplateBaseAlumni(path, idioma, "laboral");

        List<Item> datosLaborales = itemService.getItemsByGrupId(GRUPOLABORAL);
        template.put("datosLaborales", ItemUI.toUI(datosLaborales, idioma));

        return template;
    }

    public void actualizaLaborales(Cliente cliente,
                                   Item estadoLaboral,
                                   Long sectorEmpresarialId,
                                   String empresa,
                                   String cargo,
                                   String nombreEstudioActual,
                                   String linkedin)
            throws ParseException {

        eliminaDatosLaborales(cliente, estadoLaboral.getGrupo());

        ClienteItem clienteItemEstadoLaboral = clienteItemService.addClienteItemSinDuplicados(cliente, estadoLaboral, null, null, null);
        if (ParamUtils.isNotNull(sectorEmpresarialId)) {
            Item sectorEmpresarial = itemService.getItemById(sectorEmpresarialId);
            clienteItemService.addClienteItemSinDuplicados(cliente, sectorEmpresarial, clienteItemEstadoLaboral.getId(), null, null);
            clienteDatoService.addClienteDatoExtra(cliente.getId(), EMPRESA, null, null, empresa, null, estadoLaboral.getId(), null);
            clienteDatoService.addClienteDatoExtra(cliente.getId(), CARGO, null, null, cargo, null, estadoLaboral.getId(), null);
        }
        if (ParamUtils.isNotNull(nombreEstudioActual)) {
            clienteDatoService.addClienteDatoExtra(cliente.getId(), TIPO_NOMBRE_ESTUDIO_ACTUAL, null, null, nombreEstudioActual, null, estadoLaboral.getId(), null);
        }
        if (ParamUtils.isNotNull(linkedin))
            clienteDatoService.addClienteDatoExtra(cliente.getId(), PERFILLINKEDIN, TipoAccesoDatoUsuario.INTERNO.getId(), null, linkedin, null, null, null);
    }

    private void eliminaDatosLaborales(Cliente cliente, Grupo grupo) {
        List<Item> datosLaborales = itemService.getItemsByGrupId(grupo.getId());

        datosLaborales.forEach(item -> {
            ClienteItem clienteItem = clienteItemService.getClienteItemByItemAndCliente(cliente.getId(), item.getId());
            if (ParamUtils.isNotNull(clienteItem))
                clienteItemService.deleteClienteItem(clienteItem.getId());
            List <ClienteDato> clienteDatos = clienteDatoService.getClienteDatoByClienteAndItemId(cliente, item.getId());
            clienteDatos.forEach(clienteDato1 -> {
                clienteDatoService.delete(clienteDato1.getId());
            });
        });
    }
}
