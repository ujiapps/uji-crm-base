package es.uji.apps.crm.services;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.CampanyaCartaDAO;
import es.uji.apps.crm.model.CampanyaCarta;
import es.uji.apps.crm.model.Cliente;

@Service
public class CampanyaCartaService {

    private CampanyaCartaClienteService campanyaCartaClienteService;
    private CampanyaCartaDAO campanyaCartaDAO;

    @Autowired
    public CampanyaCartaService(CampanyaCartaDAO campanyaCartaDAO, CampanyaCartaClienteService campanyaCartaClienteService) {

        this.campanyaCartaDAO = campanyaCartaDAO;
        this.campanyaCartaClienteService = campanyaCartaClienteService;
    }

    public CampanyaCarta getById(Long campanyaCartaId) {
        List<CampanyaCarta> lista = campanyaCartaDAO.get(CampanyaCarta.class, campanyaCartaId);
        if (lista.size() > 0)
        {
            return lista.get(0);
        }
        return null;
    }

    public List<CampanyaCarta> getCampanyaCartasByCampanyaId(Long campanyaId) {
        return campanyaCartaDAO.getCampanyaCartasByCampanyaId(campanyaId);
    }

    public CampanyaCarta addCampanyaCarta(CampanyaCarta campanyaCarta) {
        return campanyaCartaDAO.insert(campanyaCarta);
    }

    public void updateCampanyaCarta(CampanyaCarta campanyaCarta) {
        campanyaCartaDAO.update(campanyaCarta);
    }

    public void delete(Long campanyaCartaId) {
        campanyaCartaDAO.delete(CampanyaCarta.class, campanyaCartaId);
    }

    public void anyadirCampanyaCartaCliente(Long campanya, Long estado, Cliente cliente) throws ParseException {

        List<CampanyaCarta> campanyaCartas = getCampanyaCartasByCampanyaIdAndTipoId(campanya, estado);
        for (CampanyaCarta campanyaCarta : campanyaCartas)
        {
            campanyaCartaClienteService.addCampanyaCartaCliente(campanyaCarta, cliente, campanya);
        }
    }

    public List<CampanyaCarta> getCampanyaCartasByCampanyaIdAndTipoId(Long campanyaId, Long estadoId) {
        return campanyaCartaDAO.getCampanyaCartasByCampanyaIdAndTipoId(campanyaId, estadoId);
    }

    public void autoguardadoCampanyaCarta(Long cartaId, String cuerpo) {
        CampanyaCarta campanyaCarta = getById(cartaId);
        campanyaCarta.setCuerpo(cuerpo);
        campanyaCartaDAO.update(campanyaCarta);
    }
}