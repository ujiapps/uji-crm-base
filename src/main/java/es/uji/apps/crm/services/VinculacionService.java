package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.VinculacionDAO;
import es.uji.apps.crm.model.Vinculacion;

@Service
public class VinculacionService {
    private VinculacionDAO vinculacionDAO;

    @Autowired
    public VinculacionService(VinculacionDAO vinculacionDAO) {
        this.vinculacionDAO = vinculacionDAO;
    }

    public List<Vinculacion> getVinculacionesByCliente1Id(Long clienteId) {
        return vinculacionDAO.getVinculacionesByCliente1Id(clienteId);
    }

    public List<Vinculacion> getVinculacionesInversasByClienteId(Long clienteId) {
        return vinculacionDAO.getVinculacionesInversasByClienteId(clienteId);
    }

    public Vinculacion addVinculacion(Vinculacion vinculacion) {
        vinculacionDAO.insert(vinculacion);
        vinculacion.setId(vinculacion.getId());
        return vinculacion;
    }

    public void delete(Long vinculacionId) {
        vinculacionDAO.delete(Vinculacion.class, vinculacionId);

    }
}