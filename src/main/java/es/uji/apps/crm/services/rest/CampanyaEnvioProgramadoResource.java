package es.uji.apps.crm.services.rest;

import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.CampanyaEnvioAuto;
import es.uji.apps.crm.model.CampanyaEnvioProgramado;
import es.uji.apps.crm.services.CampanyaEnvioProgramadoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("campanyaenvioprogramado")
public class CampanyaEnvioProgramadoResource extends CoreBaseService {

    @InjectParam
    private CampanyaEnvioProgramadoService campanyaEnvioProgramadoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyasEnvioByCampanya(@QueryParam("envioId") Long envioId) {
        CampanyaEnvioAuto envio = new CampanyaEnvioAuto();
        envio.setId(envioId);
        return modelToUI(campanyaEnvioProgramadoService.getCampanyaEnvioProgramadoByEnvio(envio));
    }

    private List<UIEntity> modelToUI(List<CampanyaEnvioProgramado> campanyaEnvioProgramados) {
        return campanyaEnvioProgramados.stream().map((CampanyaEnvioProgramado t) -> modelToUI(t)).collect(Collectors.toList());
    }

    private UIEntity modelToUI(CampanyaEnvioProgramado campanyaEnvioProgramado) {
        return UIEntity.toUI(campanyaEnvioProgramado);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanyaEnvioProgramado(UIEntity entity) throws ParseException {
        CampanyaEnvioProgramado campanyaEnvioProgramado = UIToModel(entity);
        campanyaEnvioProgramadoService.addCampanyaEnvioProgramado(campanyaEnvioProgramado);
        return modelToUI(campanyaEnvioProgramado);
    }

    private CampanyaEnvioProgramado UIToModel(UIEntity entity) throws ParseException {

        CampanyaEnvioProgramado campanyaEnvioProgramado = entity.toModel(CampanyaEnvioProgramado.class);

        CampanyaEnvioAuto campanyaEnvio = new CampanyaEnvioAuto();
        campanyaEnvio.setId(ParamUtils.parseLong(entity.get("envioId")));
        campanyaEnvioProgramado.setEnvio(campanyaEnvio);

        return campanyaEnvioProgramado;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateCampanyaEnvioProgramado(UIEntity entity) throws ParseException {

        CampanyaEnvioProgramado campanyaEnvioProgramado = UIToModel(entity);
        campanyaEnvioProgramadoService.updateCampanyaEnvioProgramado(campanyaEnvioProgramado);
        return modelToUI(campanyaEnvioProgramado);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long envioProgramadoId) {
        campanyaEnvioProgramadoService.deleteCampanyaEnvioProgramado(envioProgramadoId);
    }
}