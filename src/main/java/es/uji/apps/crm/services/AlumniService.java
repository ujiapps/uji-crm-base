package es.uji.apps.crm.services;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.auth.UserSessionManager;
import es.uji.apps.crm.model.*;
import es.uji.apps.crm.model.domains.TipoAfiliacionUsuarioCampanya;
import es.uji.apps.crm.model.domains.TipoCategoriaFormulario;
import es.uji.apps.crm.ui.*;
import es.uji.apps.crm.utils.AppInfo;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;
import es.uji.commons.web.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.crm.dao.ActualizaPersona;
import es.uji.apps.crm.model.domains.TipoAccesoDatoUsuario;
import es.uji.apps.crm.model.domains.TipoClienteDato;
import es.uji.commons.rest.ParamUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Service
public class AlumniService {

    @InjectParam
    private UserSessionManager userSessionManager;
    //    @Value("${uji.crm.domain}")
    private final String domain = "uji.es";

    private final static Long TELEFONO_ALTERNATIVO_ID = 3L;

    private final static Long IBAN = 17L;
    private final static String CODIGOTRATAMIENTO = "SAUJI";
    private final static Long TIPOESTUDIOOTROS = 2L;
    private final static Long TIPOESTUDIOGRADO = 1L;
    private final static Long TIPOESTUDIOPOST = 3L;
    private final static Long TIPOESTUDIODOC = 4L;
    private final static Long CAMPANYABASIC = 3L;
    private final static Long CAMPANYASOIPEP = 4104036L;
    private final static Long CAMPANYAPREMIUM = 1070562L;
    private final static Long CUENTA = 17L;

    private final ClienteGeneralService clienteGeneralService;
    private final ClienteService clienteService;
    private final ClienteDatoService clienteDatoService;
    private final ClienteItemService clienteItemService;
    private final ItemService itemService;
    private final ActualizaPersona actualizaPersona;
    private final CampanyaClienteService campanyaClienteService;
    private final TipoEstadoCampanyaService tipoEstadoCampanyaService;
    private final AceptaTratamientoService aceptaTratamientoService;
    private final ClienteEstadoCampanyaMotivoService clienteEstadoCampanyaMotivoService;
    private final ClienteTitulacionesNoUJIService clienteTitulacionesNoUJIService;
    private final MovimientoService movimientoService;
    private final ClienteEstudioService clienteEstudioService;
    private final TipoEstudioService tipoEstudioService;
    private final UniversidadService universidadService;
    private final CampanyaService campanyaService;
    private final CuentasCorporativasService cuentasCorporativasService;
    private final PaisService paisService;
    private final ProvinciaService provinciaService;
    private final TratamientoRGPDService tratamientoRGPDService;
    private final TipoService tipoService;
    private final BannerService bannerService;

    @Autowired
    public AlumniService(ClienteService clienteService,
                         ClienteGeneralService clienteGeneralService,
                         ClienteDatoService clienteDatoService,
                         ClienteItemService clienteItemService,
                         ItemService itemService,
                         ActualizaPersona actualizaPersona,
                         TipoEstadoCampanyaService tipoEstadoCampanyaService,
                         CampanyaClienteService campanyaClienteService,
                         AceptaTratamientoService aceptaTratamientoService,
                         ClienteEstadoCampanyaMotivoService clienteEstadoCampanyaMotivoService,
                         ClienteTitulacionesNoUJIService clienteTitulacionesNoUJIService,
                         MovimientoService movimientoService,
                         ClienteEstudioService clienteEstudioService,
                         TipoEstudioService tipoEstudioService,
                         UniversidadService universidadService,
                         CampanyaService campanyaService,
                         CuentasCorporativasService cuentasCorporativasService,
                         PaisService paisService,
                         ProvinciaService provinciaService,
                         TratamientoRGPDService tratamientoRGPDService,
                         TipoService tipoService,
                         BannerService bannerService) {
        this.clienteGeneralService = clienteGeneralService;
        this.clienteService = clienteService;
        this.clienteDatoService = clienteDatoService;
        this.clienteItemService = clienteItemService;
        this.itemService = itemService;
        this.actualizaPersona = actualizaPersona;
        this.tipoEstadoCampanyaService = tipoEstadoCampanyaService;
        this.campanyaClienteService = campanyaClienteService;
        this.aceptaTratamientoService = aceptaTratamientoService;
        this.clienteEstadoCampanyaMotivoService = clienteEstadoCampanyaMotivoService;
        this.clienteTitulacionesNoUJIService = clienteTitulacionesNoUJIService;
        this.movimientoService = movimientoService;
        this.clienteEstudioService = clienteEstudioService;
        this.tipoEstudioService = tipoEstudioService;
        this.universidadService = universidadService;
        this.campanyaService = campanyaService;
        this.cuentasCorporativasService = cuentasCorporativasService;
        this.paisService = paisService;
        this.provinciaService = provinciaService;
        this.tratamientoRGPDService = tratamientoRGPDService;
        this.tipoService = tipoService;
        this.bannerService = bannerService;
    }

    @Transactional
    public void actualizaPersonal(ClienteGeneral clienteGeneral, Cliente cliente, Long connectedUserId, Long tipoIdentificacionId, String identificacion,
                                  String apellidos, String nombre, Long sexo, Date fechaNacimiento, Long paisNacimientoId,
                                  Long provinciaNacimientoId, Long poblacionNacimientoId,
                                  String prefijoMovil, Long movil, Long telefonoAlternativo, String email, String paisContactoId,
                                  Long provinciaContactoId, Long poblacionContactoId, String codigoPostal, String direccion)
            throws ParseException {

        if (ParamUtils.isNotNull(tipoIdentificacionId)) {
            Tipo tipoIdentificacion = new Tipo();
            tipoIdentificacion.setId(tipoIdentificacionId);
            clienteGeneral.setTipoIdentificacion(tipoIdentificacion);
        }
        if (ParamUtils.isNotNull(identificacion)) {
            clienteGeneral.setIdentificacion(identificacion);
        }
        clienteGeneralService.update(clienteGeneral);

        cliente.setApellidos(apellidos);
        cliente.setNombre(nombre);
        clienteService.updateCliente(cliente);

        if (ParamUtils.isNotNull(sexo)) {
            cliente.setSexo(sexo);
        }
        if (ParamUtils.isNotNull(fechaNacimiento)) {
            cliente.setFechaNacimiento(fechaNacimiento);
        }

        Item paisNacimiento = itemService.getItemById(paisNacimientoId);
        ClienteItem clientePaisNacimiento = clienteItemService.addClienteItemSinDuplicados(cliente, paisNacimiento, null, null, null);
        if (ParamUtils.isNotNull(provinciaNacimientoId)) {
            clienteItemService.addClienteItem(cliente.getId(), provinciaNacimientoId, clientePaisNacimiento.getId(), null, null, null, null);
        }
        if (ParamUtils.isNotNull(poblacionNacimientoId)) {
            clienteItemService.addClienteItem(cliente.getId(), poblacionNacimientoId, clientePaisNacimiento.getId(), null, null, null, null);
        }

        cliente.setCodigoPostal(codigoPostal);
        cliente.setPostal(direccion);

        if (ParamUtils.isNotNull(paisContactoId)) {
            cliente.setPaisPostal(new Pais(paisContactoId));
        }

        if (ParamUtils.isNotNull(provinciaContactoId)) {
            cliente.setProvinciaPostal(new Provincia(provinciaContactoId));
        }

        if (ParamUtils.isNotNull(poblacionContactoId)) {
            cliente.setPoblacionPostal(new Poblacion(poblacionContactoId));
        }

        if (ParamUtils.isNotNull(email)) {
            cliente.setCorreo(email);
        }
        if (ParamUtils.isNotNull(movil)) {
            cliente.setMovil(movil.toString());
        }

        cliente.setPrefijoTelefono(prefijoMovil);

        clienteService.updateCliente(cliente);

        if (ParamUtils.isNotNull(telefonoAlternativo)) {
            actualizaDatoExtra(cliente, TELEFONO_ALTERNATIVO_ID, telefonoAlternativo.toString());
        }

        //Si el movil no es nulo actualizar suscriptor.
        if (ParamUtils.isNotNull(cliente.getMovil()) && connectedUserId != -1L) {
            actualizaPersona.guardaMovilSuscriptor(connectedUserId, cliente.getMovil());
        }

        if (ParamUtils.isNotNull(cliente.getCorreo()) && connectedUserId != -1L) {
            actualizaPersona.guardaCorreoSuscriptor(connectedUserId, cliente.getCorreo());
        }
    }

    private void actualizaDatoExtra(Cliente cliente, Long tipoDatoId, String valor) throws ParseException {
        ClienteDato clienteDato = clienteDatoService.getClienteDatoByClienteAndTipoId(cliente, tipoDatoId);
        if (ParamUtils.isNotNull(clienteDato)) {
            clienteDatoService.updateClienteDato(clienteDato, valor, null);
        } else {
            clienteDato = new ClienteDato();
            clienteDato.setCliente(cliente);
            ClienteDatoTipo clienteDatoTipo = clienteDatoService.getClienteDatoTipoById(tipoDatoId);
            clienteDato.setClienteDatoTipo(clienteDatoTipo);

            Tipo tipo = new Tipo();
            tipo.setId(TipoAccesoDatoUsuario.INTERNO.getId());
            clienteDato.setClienteDatoTipoAcceso(tipo);
            clienteDatoService.addClienteDatoExtra(clienteDato, valor, null);

        }
    }

    public void anyadirEstudio(Cliente cliente, String tipoEstudio, Long ambitoEstudiosMediosId, Long estudiosNoUniversitariosId, Long gradoId, Long ambitoPostGradoId, Long ambitoDoctoradoId, String textoEstudio, Long anyoEstudio, Long universidadId) {
        switch (tipoEstudio) {
            case "ELEM":
                clienteTitulacionesNoUJIService.addClienteTitulacion(cliente, null, null, null, textoEstudio, 4L);
                break;
            case "MIT":
                clienteTitulacionesNoUJIService.addClienteTitulacion(cliente, ambitoEstudiosMediosId, null, null, textoEstudio, 5L);
                break;
            case "NO":
                clienteTitulacionesNoUJIService.addClienteTitulacion(cliente, estudiosNoUniversitariosId, null, null, textoEstudio, 6L);
                break;
            case "GRADO":
                clienteTitulacionesNoUJIService.addClienteTitulacion(cliente, gradoId, anyoEstudio.toString(), universidadId, textoEstudio, 1L);
                break;
            case "POST":
                clienteTitulacionesNoUJIService.addClienteTitulacion(cliente, ambitoPostGradoId, anyoEstudio.toString(), universidadId, textoEstudio, 2L);
                break;
            case "DOC":
                clienteTitulacionesNoUJIService.addClienteTitulacion(cliente, ambitoDoctoradoId, anyoEstudio.toString(), universidadId, textoEstudio, 3L);
                break;
        }
    }

    public void actualizaSuscriptores(Long itemId, Cliente cliente, Boolean checked) {
        ClienteItem clienteItem = clienteItemService.getClienteItemByItemAndCliente(cliente.getId(), itemId);
        if (ParamUtils.isNotNull(clienteItem)) {
            clienteItem.setCorreo(checked);
            clienteItemService.updateClienteItem(clienteItem);
        } else {
            clienteItemService.addClienteItem(cliente.getId(), itemId, null, null, null, checked, Boolean.FALSE);
        }

    }

    @Transactional
    public void altaClientePremium(Cliente cliente, Long modalidad, Long comoConoces, String iban, Long campanya)
            throws ParseException, SQLException {

        Long GRUPOCONOCES = 5115980L;

        altaClienteCampanya(cliente, modalidad, campanya, TipoAfiliacionUsuarioCampanya.ALTAWEB.getTipo());
        List<ClienteItem> itemsCliente = clienteItemService.getAllItemsByClienteId(cliente.getId());
        itemsCliente.stream()
                .filter(item -> ParamUtils.isNotNull(item.getCampanya()) && item.getCampanya().getId().equals(campanya))
                .filter(item -> item.getItem().getGrupo().getId().equals(GRUPOCONOCES))
                .collect(Collectors.toList())
                .forEach(clienteItem -> clienteItemService.deleteClienteItem(clienteItem.getId()));

        clienteItemService.addClienteItem(cliente.getId(), comoConoces, null, null, campanya, Boolean.TRUE, Boolean.TRUE);

        ClienteDato clienteDatoIban = clienteDatoService.getClienteDatoByClienteAndTipoIdVinculadoCampanya(cliente, IBAN, CAMPANYAPREMIUM);
        if (ParamUtils.isNotNull(clienteDatoIban)) {
            clienteDatoIban.setValorSegunTipo(ParamUtils.isNotNull(iban) ? iban : "", null);
            clienteDatoService.update(clienteDatoIban);
            return;
        }
        if (ParamUtils.isNotNull(iban)) {
            clienteDatoService.addClienteDatoExtra(cliente.getId(), IBAN, TipoAccesoDatoUsuario.INTERNO.getId(), null, iban, null, null, campanya);
        }
    }

    private void altaClienteCampanya(Cliente cliente, Long modalidad, Long campanyaId, String accionAlta)
            throws ParseException, SQLException {
        AceptaTratamiento aceptaTratamiento = aceptaTratamientoService.getAceptaTratamientoByClienteAndCodigo(cliente, CODIGOTRATAMIENTO);
        if (!ParamUtils.isNotNull(aceptaTratamiento)) {
            aceptaTratamientoService.addAceptaTratamiento(cliente, CODIGOTRATAMIENTO);
        }

        TipoEstadoCampanya tipoEstadoCampanya = tipoEstadoCampanyaService.getTipoEstadoCampanyaByAccion(campanyaId, accionAlta);

        CampanyaCliente campanyaCliente = campanyaClienteService.getCampanyaClienteByCampanyaAndClienteId(campanyaId, cliente.getId());

        if (ParamUtils.isNotNull(campanyaCliente)) {
            campanyaCliente.setCampanyaClienteTipo(tipoEstadoCampanya);
            campanyaClienteService.updateCampanyaCliente(campanyaCliente, Boolean.TRUE);
        } else {
            CampanyaCliente campanyaClienteNuevo = new CampanyaCliente();
            Campanya campanya = new Campanya();
            campanya.setId(campanyaId);
            campanyaClienteNuevo.setCampanya(campanya);
            campanyaClienteNuevo.setCliente(cliente);
            campanyaClienteNuevo.setCampanyaClienteTipo(tipoEstadoCampanya);

            campanyaClienteService.addCampanyaCliente(campanyaClienteNuevo);

        }
        if (ParamUtils.isNotNull(modalidad)) {
            clienteItemService.addClienteItem(cliente.getId(), modalidad, null, null, campanyaId, Boolean.TRUE, Boolean.TRUE);
        }
    }

    public void altaClienteOipep(Cliente cliente, Long modalidad, Long campanya)
            throws ParseException, SQLException {

        altaClienteCampanya(cliente, modalidad, campanya, TipoAfiliacionUsuarioCampanya.ALTAWEB.getTipo());

    }

    public void anyadirArchivo(Cliente cliente, Long tipoDato, String nombre, String mimeType,
                               byte[] documentacionFile) {

        ClienteDato clienteDato = new ClienteDato();

        clienteDato.setCliente(cliente);
        ClienteDatoTipo clienteDatoTipo = new ClienteDatoTipo();
        clienteDatoTipo.setId(tipoDato);
        clienteDato.setClienteDatoTipo(clienteDatoTipo);
        Tipo tipoAcceso = new Tipo();
        tipoAcceso.setId(TipoAccesoDatoUsuario.INTERNO.getId());
        clienteDato.setClienteDatoTipoAcceso(tipoAcceso);
        clienteDato.setValorFicheroMimetype(mimeType);
        clienteDato.setValorFicheroNombre(nombre);
        clienteDato.setValorFicheroBinario(documentacionFile);

        clienteDatoService.insertaClienteDatoFichero(clienteDato);

    }

    public void altaClienteBasic(Cliente cliente, Long campanyabasic)
            throws ParseException, SQLException {
        altaClienteCampanya(cliente, null, campanyabasic, TipoAfiliacionUsuarioCampanya.DEFINITIVO.getTipo());

    }

    public void bajaClienteBasic(Cliente cliente, Long campanyabasic, Long motivoBaja)
            throws ParseException, SQLException {

        TipoEstadoCampanya tipoEstadoCampanya = tipoEstadoCampanyaService.getTipoEstadoCampanyaByAccion(campanyabasic, TipoAfiliacionUsuarioCampanya.BAJAWEB.getTipo());

        if (ParamUtils.isNotNull(tipoEstadoCampanya)) {
            CampanyaCliente campanyaCliente = campanyaClienteService.getCampanyaClienteByCampanyaAndClienteId(campanyabasic, cliente.getId());

            if (ParamUtils.isNotNull(campanyaCliente)) {
                campanyaCliente.setCampanyaClienteTipo(tipoEstadoCampanya);
                campanyaClienteService.updateCampanyaCliente(campanyaCliente, Boolean.TRUE);
                if (ParamUtils.isNotNull(motivoBaja)) {
                    ClienteEstadoCampanyaMotivo clienteEstadoCampanyaMotivo = new ClienteEstadoCampanyaMotivo();
                    clienteEstadoCampanyaMotivo.setCampanyaCliente(campanyaCliente);
                    TipoEstadoCampanyaMotivo tipoEstadoCampanyaMotivo = new TipoEstadoCampanyaMotivo();
                    tipoEstadoCampanyaMotivo.setId(motivoBaja);
                    clienteEstadoCampanyaMotivo.setCampanyaEstadoMotivo(tipoEstadoCampanyaMotivo);
                    clienteEstadoCampanyaMotivoService.addClienteEstadoMotivo(clienteEstadoCampanyaMotivo, campanyabasic);

                }
            }
        }
    }

    public void bajaClientePremium(Cliente cliente, Long campanyaPremium, Long motivoBaja, String explicacionMotivo)
            throws ParseException, SQLException {
        TipoEstadoCampanya tipoEstadoCampanya = tipoEstadoCampanyaService.getTipoEstadoCampanyaByAccion(campanyaPremium, TipoAfiliacionUsuarioCampanya.BAJAWEB.getTipo());

        if (ParamUtils.isNotNull(tipoEstadoCampanya)) {
            CampanyaCliente campanyaCliente = campanyaClienteService.getCampanyaClienteByCampanyaAndClienteId(campanyaPremium, cliente.getId());

            if (ParamUtils.isNotNull(campanyaCliente)) {
                campanyaCliente.setCampanyaClienteTipo(tipoEstadoCampanya);
                campanyaClienteService.updateCampanyaCliente(campanyaCliente, Boolean.TRUE);
                if (ParamUtils.isNotNull(motivoBaja)) {
                    clienteItemService.addClienteItem(cliente.getId(), motivoBaja, null, null, campanyaPremium, Boolean.TRUE, Boolean.TRUE);
                    clienteDatoService.addClienteDatoExtra(cliente.getId(), TipoClienteDato.CAMPOLIBRE.getId(), TipoAccesoDatoUsuario.INTERNO.getId(), null, explicacionMotivo, null, motivoBaja, campanyaPremium);
                }
            }
        }
    }

    public Template getTemplateAlumni(Template template, String contenido, String idioma) throws ParseException {

        List<CuentaCorportativa> cuentasCorportativas = new ArrayList<>();
        List<ClienteEstudio> estudiosPersona = new ArrayList<>();

        if (userSessionManager.getConnectedUserId() != -1L) {
            estudiosPersona = clienteEstudioService.getClienteEstudiosByPersona(userSessionManager.getConnectedUserId());
            cuentasCorportativas = cuentasCorporativasService.getCuentasCorporativasActivasByUserId(userSessionManager.getConnectedUserId());
            String tratamiento = tratamientoRGPDService.estaRegistradoTratamientoPLSQL("U004", userSessionManager.getConnectedUserId());
            if (ParamUtils.isNotNull(tratamiento)) {
                if (tratamiento.length() > 1) {
                    tratamiento = "0";
                }
            } else {
                tratamiento = "-1";
            }
            template.put("tratamientoUsuario", tratamiento);
        } else {
            template.put("tratamientoUsuario", -1);
        }

        List<Item> paisNacimiento = itemService.getItemsByGrupId(3276215L);
        List<TipoEstudio> ambitoEstudios = tipoEstudioService.getTipoEstudioByTipo(TIPOESTUDIOOTROS);
        List<TipoEstudio> grados = tipoEstudioService.getTipoEstudioByTipo(TIPOESTUDIOGRADO);
        List<UniversidadUI> universidades = universidadService.getUniversidades(idioma);
        List<TipoEstudio> estudiosPostgrado = tipoEstudioService.getTipoEstudioByTipo(TIPOESTUDIOPOST);
        List<TipoEstudio> estudiosDoctorado = tipoEstudioService.getTipoEstudioByTipo(TIPOESTUDIODOC);
        List<Item> datosLaborales = itemService.getItemsByGrupId(807418L);

        Map<String, List<SuscriptoresUI>> suscriptores = itemService.getItemsVisiblesByProgramaId(1L)
                .stream().map(item -> new SuscriptoresUI(item, idioma)).collect(Collectors.groupingBy(SuscriptoresUI::getGrupo));


        template.put("contenido", contenido);
        template.put("suscriptores", suscriptores);
        if (ParamUtils.isNotNull(userSessionManager.getClienteGeneral().getPersona())) {
            template.put("persona", true);
        } else {
            template.put("persona", false);
        }

        CampanyaCliente campanyaClienteBasic = campanyaClienteService.getCampanyaClienteByCampanyaAndClienteId(CAMPANYABASIC, userSessionManager.getCliente().getId());

        Boolean basic = Boolean.FALSE;
        Boolean oipep = Boolean.FALSE;
        Boolean optaBasic = Boolean.FALSE;

        if (ParamUtils.isNotNull(campanyaClienteBasic)) {
            optaBasic = Boolean.TRUE;
            if (ParamUtils.isNotNull(campanyaClienteBasic.getCampanyaClienteTipo().getAccion()) && campanyaClienteBasic.getCampanyaClienteTipo().getAccion().equals(TipoAfiliacionUsuarioCampanya.DEFINITIVO.getTipo())) {
                basic = Boolean.TRUE;
                template.put("fechaBasicAlta", movimientoService.getFechaAlta(userSessionManager.getCliente().getId(), CAMPANYABASIC));
            }
        }

        List<Campanya> campanyaOIPEP = campanyaService.getCampanyasByClienteIdAndCampanyaPadreId(userSessionManager.getCliente().getId(), CAMPANYASOIPEP);

        for (Campanya campanya : campanyaOIPEP) {
            for (CampanyaCliente campanyaCliente : campanya.getCampanyasClientes()) {
                if (ParamUtils.isNotNull(campanyaCliente.getCampanyaClienteTipo().getAccion()) && campanyaCliente.getCampanyaClienteTipo().getAccion().equals(TipoAfiliacionUsuarioCampanya.PROPUESTO.getTipo())) {
                    oipep = Boolean.TRUE;
                    template.put("campanyaOipep", campanya.getId());

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date());
                    calendar.add(Calendar.DAY_OF_YEAR, -1);
                    Date fechaInicioActiva = calendar.getTime();
                    if (ParamUtils.isNotNull(campanya.getFechaInicioActiva())) {
                        fechaInicioActiva = campanya.getFechaInicioActiva();
                    }


                    calendar.add(Calendar.DAY_OF_YEAR, 2);
                    Date fechaFinActiva = calendar.getTime();
                    if (ParamUtils.isNotNull(campanya.getFechaFinActiva())) {
                        fechaFinActiva = campanya.getFechaFinActiva();
                    }

                    Date hoy = new Date();
                    if (hoy.after(fechaInicioActiva) && hoy.before(fechaFinActiva)) {
                        template.put("campanyaOipepActiva", true);
                    }
                }
            }
        }
        template.put("oipep", oipep);
        template.put("basic", basic);
        template.put("optaBasic", optaBasic);

        template.put("paisNacimiento", ItemUI.toUI(paisNacimiento, idioma));
        template.put("paises", paisService.getPaisesOrdenadosPorIdioma(idioma));
        template.put("provincias", provinciaService.getProvincias(idioma));
        template.put("ambitoEstudios", TipoEstudioUI.toUI(ambitoEstudios, idioma));
        template.put("grados", TipoEstudioUI.toUI(grados, idioma));
        template.put("universidades", universidades);
        template.put("estudiosPostgrado", TipoEstudioUI.toUI(estudiosPostgrado, idioma));
        template.put("estudiosDoctorado", TipoEstudioUI.toUI(estudiosDoctorado, idioma));
        template.put("datosLaborales", ItemUI.toUI(datosLaborales, idioma));
        template.put("tratamientoRPGD", tratamientoRGPDService.visualizaTratamientoPLSQL("U004", "SNN", idioma));
        template.put("movil", userSessionManager.getCliente().getMovil());
        template.put("cuentasCorporativas", cuentasCorportativas);
        template.put("estudiosPersona", ClienteEstudioUI.toUI(estudiosPersona, idioma));
        template.put("today", Calendar.getInstance());

        ClienteDato cuentaBancaria = clienteDatoService.getClienteDatoByClienteAndTipoIdVinculadoCampanya(userSessionManager.getCliente(), CUENTA, CAMPANYAPREMIUM);
        if (ParamUtils.isNotNull(cuentaBancaria) && ParamUtils.isNotNull(cuentaBancaria.getValorSegunTipo())) {
            template.put("cuentaBancaria", cuentaBancaria.getValorSegunTipo());
        } else {
            template.put("cuentaBancaria", "");
        }

        return template;
    }

    public void introducePremiumEstadoEnPlantillaZonaPrivada(Template template) {
        String premiumEstado;
        CampanyaCliente campanyaClientePremium = campanyaClienteService.getCampanyaClienteByCampanyaAndClienteId(CAMPANYAPREMIUM, userSessionManager.getCliente().getId());

        premiumEstado = "BAJA";
        if (ParamUtils.isNotNull(campanyaClientePremium) && ParamUtils.isNotNull(campanyaClientePremium.getCampanyaClienteTipo().getAccion())) {
            if (campanyaClientePremium.getCampanyaClienteTipo().getAccion().equals(TipoAfiliacionUsuarioCampanya.DEFINITIVO.getTipo())) {
                premiumEstado = "ALTA";
                template.put("fechaPremiumAlta", movimientoService.getFechaAlta(userSessionManager.getCliente().getId(), CAMPANYAPREMIUM));
            } else {
                if (campanyaClientePremium.getCampanyaClienteTipo().getAccion().equals(TipoAfiliacionUsuarioCampanya.ALTAWEB.getTipo()) || campanyaClientePremium.getCampanyaClienteTipo().getAccion().equals(TipoAfiliacionUsuarioCampanya.PENDIENTE.getTipo())) {
                    premiumEstado = "PENDIENTE-ALTA";
                } else {
                    if (campanyaClientePremium.getCampanyaClienteTipo().getAccion().equals(TipoAfiliacionUsuarioCampanya.BAJA.getTipo())) {
                        template.put("fechaPremiumBaja", movimientoService.getFechaAlta(userSessionManager.getCliente().getId(), CAMPANYAPREMIUM));
                    }
                }
            }
        }
        template.put("premium", premiumEstado);
    }

    public Template getTemplateBaseAlumni(String path, String idioma, String contenido) throws ParseException {

        String pathPlantilla = "crm/" + path.split("/")[1] + "/";

        Template template = AppInfo.buildPagina(pathPlantilla + "base", idioma);
        template.put("imagenesBanner", bannerService.getAllBanners());
        template.put("banner", pathPlantilla + "banner");
        template.put("contenido", pathPlantilla + contenido);
        template.put("urlBase", "/crm/rest" + path + "?");
        return template;
    }

    public Template getZonaPrivadaAlumni(String path, String idioma) throws
            ParseException {
        if (ParamUtils.isNotNull(userSessionManager.getClienteGeneral())) {
            Template template = getTemplateBaseAlumni(path, idioma, "datos");
            introducePremiumEstadoEnPlantillaZonaPrivada(template);
            if (path.split("/")[1].equals("alumni")) {
                path = "crm/" + path.split("/")[1] + "/";
                return getTemplateAlumni(template, path + "datos", idioma);
            }
            else {
                return template;
            }
        } else {
            return getTemplateAviso(idioma, "form.errorusuario.noexiste", "/crm/rest/" + path.split("/")[1] + "/login/", path);
        }
    }

    public Boolean enviarFormularioContacto(Long tipoIdentificacionId,
                                            String identificacion,
                                            String nombre,
                                            String apellidos,
                                            String correo,
                                            String telefono,
                                            Long categoria,
                                            String comentario) {
        try {
            MailMessage mensaje = new MailMessage("CRM");

            mensaje.setTitle("Formulario Contacto Usuario ALUMNI");
            mensaje.setContentType(MediaType.TEXT_HTML);

            Tipo tipoIdentificacion = tipoService.getTiposById(tipoIdentificacionId);
            String cuerpo = "Tipo Identificacion: " + tipoIdentificacion.getNombre() + "<br />";
            cuerpo = cuerpo + "Identificacion: " + identificacion + "<br />";
            cuerpo = cuerpo + "Nombre: " + nombre + "<br />";
            cuerpo = cuerpo + "Apellidos: " + apellidos + "<br />";
            cuerpo = cuerpo + "Correo: " + correo + "<br />";
            cuerpo = cuerpo + "Telefono: " + telefono + "<br />";
            cuerpo = cuerpo + "Categoria: " + TipoCategoriaFormulario.getTextoById(categoria) + "<br />";
            cuerpo = cuerpo + "Comentario: " + comentario + "<br />";

            mensaje.setContent(cuerpo);
            mensaje.setReplyTo("alumnisauji@uji.es");
            mensaje.setSender("Programa AlumniSAUJI <alumnisauji@uji.es>");
            mensaje.addToRecipient("alumnisauji@uji.es");

            MessagingClient cliente = new MessagingClient();
            cliente.send(mensaje);

            return Boolean.TRUE;
        } catch (Exception e) {
            return Boolean.FALSE;
        }
    }

    public Template getTemplateAviso(String idioma, String aviso, String hrefOrigen, String path) throws ParseException {

        Template template = getTemplateBaseAlumni(path, idioma, "aviso");
        template.put("aviso", aviso);
//        template.put("urlBase", path + "?");
        template.put("hrefOrigen", hrefOrigen);
        return template;
    }

    public Response deslogeaUsuarioZonaPrivada(HttpServletRequest request, HttpServletResponse response, javax.ws.rs.core.Cookie cookie) throws URISyntaxException {

        String path = request.getPathInfo();
        request.getSession().removeAttribute(User.SESSION_USER);

        if (ParamUtils.isNotNull(cookie)) {
            eliminaCookieDeSesion(response);
        }
        return getRedireccionDeslogeado(request, path);
    }

    private void eliminaCookieDeSesion(HttpServletResponse response) {
        Cookie newCookie = new Cookie("p_hash", null);
        newCookie.setPath("/");
        newCookie.setDomain(domain);
        newCookie.setMaxAge(0);
        newCookie.setValue(null);
        response.addCookie(newCookie);
    }

    private Response getRedireccionDeslogeado(HttpServletRequest request, String path) throws URISyntaxException {
        URI url = new URI(AppInfo.getHost() + "/crm/rest/alumni2024/login/");
        if (path.split("/")[1].equals("alumni")) {
            url = new URI(AppInfo.getHost() + "/crm/rest/alumni/inici/");
        }

        if (ParamUtils.isNotNull(AccessManager.getConnectedUserId(request))) {
            url = new URI("saml/logout");
        }
        return Response.seeOther(url).build();

    }
}