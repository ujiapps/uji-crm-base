package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ItemItemDAO;
import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.ItemItem;

@Service
public class ItemItemService {
    private ItemItemDAO itemItemDAO;

    @Autowired
    public ItemItemService(ItemItemDAO itemItemDAO) {
        this.itemItemDAO = itemItemDAO;
    }

    public List<ItemItem> getItemItem(Item item) {
        return itemItemDAO.getItemsItemsByItemId(item);

    }

    public List<ItemItem> getItemsItemsByItemId(Long itemId) {
        Item item = new Item();
        item.setId(itemId);
        return itemItemDAO.getItemsItemsByItemId(item);
    }

    public void deleteItemItem(Long itemItemId) {
        itemItemDAO.delete(ItemItem.class, itemItemId);
    }

    public ItemItem addItemItem(ItemItem itemItem) {
        itemItemDAO.insert(itemItem);
        itemItem.setId(itemItem.getId());
        return itemItem;
    }

    public List<ItemItem> getItemsItemsOrigenByItemId(Long itemId) {
        Item item = new Item();
        item.setId(itemId);
        return itemItemDAO.getItemsItemsOrigenByItemId(item);
    }

    //
    // public List<Item> getItems(Long connectedUserId, Long grupoId)
    // {
    // if (grupoId != null)
    // {
    // return itemDAO.getItemsByGrupoId(connectedUserId, grupoId);
    // }
    //
    // return itemDAO.getItems(connectedUserId);
    // }
    //
    // public List<Item> getItemsByClienteIdFaltan(Long connectedUserId, Long clienteId, Long
    // grupoId)
    // {
    // return itemDAO.getItemsByClienteIdFaltan(connectedUserId, clienteId, grupoId);
    // }
    //
    // public Item getItemById(Long itemId)
    // {
    // return itemDAO.getItemById(itemId);
    // }
    //
    // public List<Item> getItemsByCampanyaId(Long campanyaId)
    // {
    // return itemDAO.getItemsByCampanyaId(campanyaId);
    // }
    //
    // public List<Item> getItemsByEnvioId(Long envioId)
    // {
    // return itemDAO.getItemsByEnvioId(envioId);
    // }
    //
    // public List<Item> getItemsAsociadosByCampanyaId(Long campanyaId)
    // {
    // return itemDAO.getItemsAsociadosByCampanyaId(campanyaId);
    // }
    //
    // public Item addItem(Item item)
    // {
    // itemDAO.insert(item);
    // item.setId(item.getId());
    // return item;
    // }
    //
    // public void updateItem(Item item)
    // {
    // itemDAO.update(item);
    // }
    //
    // public void deleteItem(Long itemId)
    // {
    // itemDAO.delete(Item.class, itemId);
    // }
    //
    // public List<Item> getItemsByrupoId(Long grupoId)
    // {
    // return itemDAO.getItemsByGrupoId(grupoId);
    //
    // }
}