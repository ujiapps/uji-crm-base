package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.PersonaPasPdiDAO;
import es.uji.apps.crm.model.PersonaPasPdi;

@Service
public class PersonaPasPdiService {
    private PersonaPasPdiDAO personaPasPdiDAO;

    @Autowired
    public PersonaPasPdiService(PersonaPasPdiDAO personaPasPdiDAO) {
        this.personaPasPdiDAO = personaPasPdiDAO;
    }

    public PersonaPasPdi getPersonaPasPdiById(Long personaId) {
        return personaPasPdiDAO.getPersonaById(personaId);
    }

    public List<PersonaPasPdi> getPersonasByServicioId(Long servicioId) {
        return personaPasPdiDAO.getPersonasByServicioId(servicioId);
    }

}