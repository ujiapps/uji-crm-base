package es.uji.apps.crm.services.rest.eventos.rss;

import java.util.List;

import es.uji.apps.crm.services.rest.eventos.Metadato;

public class ContenidoRSS {
    private Long contenidoId;
    private String titulo;
    private String fecha;
    private String resumen;
    private String duracion;
    private String contenido;
    private String hora;
    private String horaApertura;
    private List<Metadato> metadatos;
    private List<Enclosure> enclosures;
    private String esHtml;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public List<Metadato> getMetadatos() {
        return metadatos;
    }

    public void setMetadatos(List<Metadato> metadatos) {
        this.metadatos = metadatos;
    }

    public List<Enclosure> getEnclosures() {
        return enclosures;
    }

    public void setEnclosures(List<Enclosure> enclosures) {
        this.enclosures = enclosures;
    }

    public Long getContenidoId() {
        return contenidoId;
    }

    public void setContenidoId(Long contenidoId) {
        this.contenidoId = contenidoId;
    }

    public String getEsHtml() {
        return esHtml;
    }

    public void setEsHtml(String esHtml) {
        this.esHtml = esHtml;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getHoraApertura() {
        return horaApertura;
    }

    public void setHoraApertura(String horaApertura) {
        this.horaApertura = horaApertura;
    }
}