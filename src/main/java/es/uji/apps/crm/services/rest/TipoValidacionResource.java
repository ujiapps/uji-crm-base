package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.services.TipoValidacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("tipovalidacion")
public class TipoValidacionResource extends CoreBaseService {
    @InjectParam
    private TipoValidacionService tipoValidacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposValidacion() {
        return UIEntity.toUI(tipoValidacionService.getTiposValidacion());
    }

}