package es.uji.apps.crm.services.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.CampanyaCartaCliente;
import es.uji.apps.crm.services.CampanyaCartaClienteService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("clientecarta")
public class ClienteCartaResource extends CoreBaseService {

    @InjectParam
    private CampanyaCartaClienteService campanyaCartaClienteService;

    @GET
    @Path("{clienteCartaId}/pdf")
    @Produces(MediaType.TEXT_HTML + ";charset=utf-8")
    public String generarCartaPdf(@PathParam("clienteCartaId") Long clienteCartaId) {

        CampanyaCartaCliente clienteCarta = campanyaCartaClienteService.getClienteCartaById(clienteCartaId);
        return clienteCarta.getClienteCarta();
    }

    @GET
    @Path("cliente")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClienteCartasByClienteId(@QueryParam("clienteId") Long clienteId, @QueryParam("campanyaId") Long campanyaId) {

        List<CampanyaCartaCliente> campanyaCartasCliente = campanyaCartaClienteService.getClienteCartasByClienteId(clienteId, campanyaId);

        return modelToUI(campanyaCartasCliente);
    }

    @PUT
    @Path("cliente/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity updateClienteCarta(@PathParam("id") Long clienteCartaId, @FormParam("contenidoCarta") String carta) {

        CampanyaCartaCliente campanyaCartaCliente = campanyaCartaClienteService.getClienteCartaById(clienteCartaId);
        campanyaCartaCliente.setClienteCarta(carta);
        return modelToUI(campanyaCartaClienteService.updateClienteCarta(campanyaCartaCliente));
    }

    @POST
    @Path("cliente")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity addClienteCarta(@FormParam("clienteId") Long clienteId, @FormParam("campanyaId") Long campanyaId,
                                    @FormParam("contenidoCarta") String carta) throws ParseException {
        return modelToUI(campanyaCartaClienteService.addClienteCarta(clienteId, campanyaId, carta));
    }

    @POST
    @Path("cliente/todas")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public List<UIEntity> addClienteCartasTodas(@FormParam("clienteId") Long clienteId, @FormParam("campanyaId") Long campanyaId) throws ParseException {
        return modelToUI(campanyaCartaClienteService.addClienteCartasTodas(clienteId, campanyaId));
    }

    @DELETE
    @Path("cliente/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateClienteCarta(@PathParam("id") Long clienteCartaId) {
        campanyaCartaClienteService.deleteClienteCarta(clienteCartaId);
    }

    private List<UIEntity> modelToUI(List<CampanyaCartaCliente> campanyaCartasCliente) {
        List<UIEntity> lista = new ArrayList<>();

        for (CampanyaCartaCliente campanyaCartaCliente : campanyaCartasCliente) {
            lista.add(modelToUI(campanyaCartaCliente));
        }
        return lista;

    }

    private UIEntity modelToUI(CampanyaCartaCliente campanyaCartaCliente) {
        return UIEntity.toUI(campanyaCartaCliente);
    }


}