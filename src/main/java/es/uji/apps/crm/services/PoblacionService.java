package es.uji.apps.crm.services;

import java.util.List;

import es.uji.apps.crm.ui.OpcionUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.PoblacionDAO;
import es.uji.apps.crm.model.Poblacion;

@Service
public class PoblacionService {
    private PoblacionDAO poblacionDAO;

    @Autowired
    public PoblacionService(PoblacionDAO poblacionDAO) {
        this.poblacionDAO = poblacionDAO;
    }


    public List<Poblacion> getPoblacionesByProvinciaId(Long provinciaId, String idioma) {
        return poblacionDAO.getPoblacionesByProvinciaId(provinciaId, idioma);
    }

    public List<Poblacion> getPoblaciones(String idioma) {
        return poblacionDAO.getPoblaciones(idioma);
    }

    public List<OpcionUI> getOpcionPoblacionesByProvinciaId(Long provinciaId, String idioma) {
        return poblacionDAO.getOpcionPoblacionesByProvinciaId(provinciaId, idioma);    }
}