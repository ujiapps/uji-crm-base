package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Perfil;
import es.uji.apps.crm.services.PerfilService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("perfil")
public class PerfilResource extends CoreBaseService {
    @InjectParam
    private PerfilService perfilService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPerfiles() {
        return UIEntity.toUI(perfilService.getPerfiles());
    }

    @GET
    @Path("programa/faltan")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPerfilesFaltanByProgramaId(@QueryParam("programaId") Long programaId) {
        List<Perfil> lista = perfilService.getPerfilesFaltanByProgramaId(programaId);
        return UIEntity.toUI(lista);
    }

}