package es.uji.apps.crm.services.rest.alumni;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.auth.UserSessionManager;
import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.datos.Personal;
import es.uji.apps.crm.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class AlumniPersonalesResource extends CoreBaseService {
    private final static Boolean FICHAZONAPRIVADA = Boolean.TRUE;

    @InjectParam
    private AlumniService alumniService;
    @InjectParam
    private UserSessionManager userSessionManager;
    @InjectParam
    private ItemService itemService;
    @InjectParam
    private AlumniPersonalesService alumniPersonalesService;


    @GET
    @Path("zonaprivada")
    @Produces(MediaType.TEXT_HTML)
    public Template getTemplateAlumniPersonales(@CookieParam("p_hash") String pHash, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);

        if (ParamUtils.isNotNull(userSessionManager.getClienteGeneral())) {
            return alumniPersonalesService.getTemplatePersonales(request.getPathInfo(), idioma);
        } else {
            return alumniService.getTemplateBaseAlumni(request.getPathInfo(), idioma, "errorusuario");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getAlumniPersonales(@CookieParam("p_hash") String pHash,
                                        @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        Personal personal = alumniPersonalesService.getAlumniPersonal(userSessionManager.getClienteGeneral(), userSessionManager.getCliente());
        return UIEntity.toUI(personal);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity postAlumniPersonales(@CookieParam("p_hash") String pHash,
                                         @CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                         @FormParam("tipoIdentificacionId") Long tipoIdentificacionId,
                                         @FormParam("identificacion") String identificacion,
                                         @FormParam("nombre") String nombre,
                                         @FormParam("apellidos") String apellidos,
                                         @FormParam("sexoId") Long sexoId,
                                         @FormParam("fechaNacimiento") String fechaNacimiento,
                                         @FormParam("paisNacimientoId") Long paisNacimientoId,
                                         @FormParam("provinciaNacimientoId") Long provinciaNacimientoId,
                                         @FormParam("poblacionNacimientoId") Long poblacionNacimientoId,
                                         @FormParam("prefijoMovil") String prefijoMovil,
                                         @FormParam("movil") Long movil,
                                         @FormParam("telefonoAlternativo") Long telefonoAlternativo,
                                         @FormParam("email") String email,
                                         @FormParam("paisContactoId") String paisContactoId,
                                         @FormParam("provinciaContactoId") Long provinciaContactoId,
                                         @FormParam("poblacionContactoId") Long poblacionContactoId,
                                         @FormParam("codigoPostal") String codigoPostal,
                                         @FormParam("direccion") String direccion)
            throws ParseException {

        userSessionManager.init(request, pHash, FICHAZONAPRIVADA);
        Date fecha = null;
        if (ParamUtils.isNotNull(fechaNacimiento)) {
            fecha = UtilsService.fechaParse(fechaNacimiento, "yyyy-MM-dd");
        }

        alumniService.actualizaPersonal(userSessionManager.getClienteGeneral(), userSessionManager.getCliente(), userSessionManager.getConnectedUserId(), tipoIdentificacionId,
                identificacion, apellidos, nombre, sexoId, fecha, paisNacimientoId, provinciaNacimientoId, poblacionNacimientoId, prefijoMovil, movil,
                telefonoAlternativo, email, paisContactoId, provinciaContactoId, poblacionContactoId, codigoPostal, direccion);
        return new UIEntity();
    }

    //TODO Mover esto a su resource correspondiente.
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("preinscripcion")
    public List<UIEntity> getItemsPreInscripcion(@QueryParam("grupoId") @DefaultValue("-1") Long grupoId) {
        List<Item> lista = itemService.getItemsByGrupId(grupoId);
        return UIEntity.toUI(lista);
    }


}
