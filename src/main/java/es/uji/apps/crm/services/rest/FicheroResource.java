package es.uji.apps.crm.services.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.crm.model.CampanyaClienteArchivo;
import es.uji.apps.crm.model.Paginacion;
import es.uji.apps.crm.services.CampanyaClienteArchivoService;
import es.uji.apps.crm.services.FicheroService;
import es.uji.apps.crm.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Component
@Path("fichero")
public class FicheroResource extends CoreBaseService {


    @InjectParam
    private FicheroService ficheroService;

    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private CampanyaClienteArchivoService campanyaClienteArchivoService;


    @GET
    @Path("persona")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getListaPersonasSinAnyadirACampanya(@QueryParam("campanya") Long campanyaId, @QueryParam("page") @DefaultValue("1") Long page, @QueryParam("start") @DefaultValue("0") Long start,
                                                               @QueryParam("limit") @DefaultValue("25") Long limit) {

        Paginacion paginacion = new Paginacion(start, limit);

        List<CampanyaClienteArchivo> lista = campanyaClienteArchivoService.getListaPersonasSinAnyadirACampanya(campanyaId, paginacion);


        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
        responseMessage.setData(modelToUI(lista));
        return responseMessage;
    }

    private List<UIEntity> modelToUI(List<CampanyaClienteArchivo> campanyaClientesArchivo) {
        List<UIEntity> res = new ArrayList<UIEntity>();
        for (CampanyaClienteArchivo campanyaClienteArchivo : campanyaClientesArchivo) {
            res.add(modelToUI(campanyaClienteArchivo));
        }
        return res;
    }

    private UIEntity modelToUI(CampanyaClienteArchivo campanyaClienteArchivo) {
        UIEntity entity = UIEntity.toUI(campanyaClienteArchivo);
        if (ParamUtils.isNotNull(campanyaClienteArchivo.getPersona())) {
            entity.put("identificacion", campanyaClienteArchivo.getPersona().getIdentificacion());
            entity.put("apellidosNombre", campanyaClienteArchivo.getPersona().getApellidosNombre());
        } else {
            entity.put("identificacion", campanyaClienteArchivo.getIdentificacion());
            entity.put("apellidosNombre", campanyaClienteArchivo.getApellidosNombre());
        }
        return entity;
    }

    @POST
    @Path("persona/campanya/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getFicheroPersonas(FormDataMultiPart form, @PathParam("id") Long campanyaId)
            throws IOException {


        String fichero = extraeDatosDeMultipart(form).get("contenido").toString();
        fichero = fichero.replace("\"", "");

        return ficheroService.addFicheroPersonas(fichero, campanyaId);
    }

    @DELETE
    @Path("persona/campanya/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteFicheroPersonas(@PathParam("id") Long campanyaId) {
        ficheroService.deleteFicheroPersonas(campanyaId);
    }

    @DELETE
    @Path("persona/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteFicheroPersona(@PathParam("id") Long personaFicheroId) {
        ficheroService.deleteFicheroPersona(personaFicheroId);
    }

    private Map<String, Object> extraeDatosDeMultipart(FormDataMultiPart multiPart)
            throws IOException {
        Map<String, Object> datos = new HashMap<>();

        String fileName = "";
        String mimeType = "";
        InputStream documento = null;

        for (BodyPart bodyPart : multiPart.getBodyParts()) {
            mimeType = bodyPart.getHeaders().getFirst("Content-Type");
            if (ParamUtils.isNotNull(mimeType)) {
                if (mimeType != null && !mimeType.isEmpty()) {
                    String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                    Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                    Matcher m = fileNamePattern.matcher(header);
                    if (m.matches()) {
                        fileName = m.group(1);
                    }
                    BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                    documento = bpe.getInputStream();
                }
            }
        }

        if (ParamUtils.isNotNull(documento)) {
            String myString = IOUtils.toString(documento, "UTF-8");
            datos.put("nombre", fileName);
            datos.put("mimetype", mimeType);
            datos.put("contenido", myString);
        }

        return datos;
    }

    @POST
    @Path("persona")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public void subirFicheroDevolucionDatos(@FormParam("correoPersonal") Boolean correoPersonal,
                                            @FormParam("nacimiento") Boolean nacimiento,
                                            @FormParam("uji") Boolean uji,
                                            @FormParam("direccionPostal") Boolean direccionPostal,
                                            @FormParam("nacionalidad") Boolean nacionalidad,
                                            @FormParam("campanya") Long campanyaId,
                                            @FormParam("estado") Long estado,
                                            @FormParam("zonaPrivada") Boolean zonaPrivada) {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        campanyaClienteArchivoService.anyadePersonasACampanya(connectedUserId, campanyaId, nacimiento, uji, direccionPostal, nacionalidad, estado, correoPersonal, zonaPrivada);
    }

}