package es.uji.apps.crm.services.rest.eventos.rss;

import java.util.HashMap;
import java.util.Map;

public class FeedRSS {
    private Map<String, ContenidoRSS> contenidosRSS = new HashMap<>();

    public void put(String key, ContenidoRSS contenidoRSS) {
        ContenidoRSS contenidoRSSGuardado = get(key);

        if (noEsHTMLYExisteContenidoHtml(contenidoRSS, contenidoRSSGuardado))
        {
            return;
        }

        contenidosRSS.put(key, contenidoRSS);
    }

    private boolean noEsHTMLYExisteContenidoHtml(ContenidoRSS contenidoRSS,
                                                 ContenidoRSS contenidoRSSGuardado) {
        return "N".equals(contenidoRSS.getEsHtml()) && contenidoRSSGuardado != null
                && "S".equals(contenidoRSSGuardado.getEsHtml());
    }

    public ContenidoRSS get(String key) {
        return contenidosRSS.get(key);
    }

    public java.util.Set<Map.Entry<String, ContenidoRSS>> entrySet() {
        return contenidosRSS.entrySet();
    }

    public int size() {
        return contenidosRSS.size();
    }
}