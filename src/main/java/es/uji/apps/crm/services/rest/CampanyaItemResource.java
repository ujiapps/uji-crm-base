package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaItem;
import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.services.CampanyaItemService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("campanyaitem")
public class CampanyaItemResource extends CoreBaseService {
    @InjectParam
    private CampanyaItemService campanyaItemService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyaItems(@QueryParam("campanyaId") Long campanyaId) {

        List<CampanyaItem> lista = campanyaItemService.getCampanyaItemsByCampanyaId(campanyaId);
        return modelToUI(lista);
    }

    private List<UIEntity> modelToUI(List<CampanyaItem> campanyaItems) {
        List<UIEntity> listaUI = new ArrayList<>();

        for (CampanyaItem campanyaItem : campanyaItems)
        {
            listaUI.add(modelToUI(campanyaItem));
        }
        return listaUI;
    }

    private UIEntity modelToUI(CampanyaItem campanyaItem) {
        UIEntity entity = UIEntity.toUI(campanyaItem);

        entity.put("itemNombre", campanyaItem.getItem().getNombreCa());
        entity.put("grupoNombre", campanyaItem.getItem().getGrupo().getNombreCa());
        entity.put("programaNombre", campanyaItem.getItem().getGrupo().getPrograma().getNombreCa());

        return entity;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getCampanyaItemById(@PathParam("id") Long campanyaItemId) {
        CampanyaItem campanyaItem = campanyaItemService.getCampanyaItemById(campanyaItemId);
        return modelToUI(campanyaItem);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanyaItem(UIEntity entity) {
        CampanyaItem campanyaItem = UIToModel(entity);
        campanyaItemService.addCampanyaItem(campanyaItem);
        return UIEntity.toUI(campanyaItem);
    }

    private CampanyaItem UIToModel(UIEntity entity) {
        CampanyaItem campanyaItem = entity.toModel(CampanyaItem.class);

        Campanya campanya = new Campanya();
        campanya.setId(Long.parseLong(entity.get("campanyaId")));
        campanyaItem.setCampanya(campanya);

        Item item = new Item();
        item.setId(Long.parseLong(entity.get("itemId")));
        campanyaItem.setItem(item);

        return campanyaItem;
    }

    @POST
    @Path("campanya/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public List<UIEntity> addCampanyaItemArray(@PathParam("id") Long campanyaId,
                                               @FormParam("items") List<Long> items) {
        List<CampanyaItem> campanyaItems = new ArrayList<>();

        for (Long itemId : items)
        {
            CampanyaItem campanyaItem = campanyaItemService
                    .addCampanyaItemArray(campanyaId, itemId);
            if (campanyaItem != null) {
                campanyaItems.add(campanyaItem);
            }
        }
        if (campanyaItems.size() > 0) {
            return UIEntity.toUI(campanyaItems);
        } else {
            return new ArrayList<>() ;
        }
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateCampanyaItem(@PathParam("id") Long campanyaItemId, UIEntity entity) {
        CampanyaItem campanyaItem = UIToModel(entity);
        campanyaItem.setId(campanyaItemId);
        campanyaItemService.updateCampanyaItem(campanyaItem);
        return modelToUI(campanyaItem);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long campanyaItemId) {
        campanyaItemService.deleteCampanyaItem(campanyaItemId);
    }
}