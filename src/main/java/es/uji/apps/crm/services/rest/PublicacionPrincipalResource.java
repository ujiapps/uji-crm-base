package es.uji.apps.crm.services.rest;
import java.text.ParseException;
import java.util.List;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.auth.UserSessionManager;
import es.uji.apps.crm.model.CampanyaFormulario;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.services.*;
import es.uji.apps.crm.ui.FormularioUI;
import es.uji.apps.crm.utils.AppInfo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.Template;


@Path("publicacionprincipal")
public class PublicacionPrincipalResource extends CoreBaseService {

    @InjectParam
    PublicacionService publicacionService;
    @InjectParam
    private FormularioService formularioService;
    @InjectParam
    private UserSessionManager userSessionManager;

    private final static Boolean FICHAZONAPUBLICA = Boolean.FALSE;

    @GET
    @Path("{campanyaId}")
    @Produces(MediaType.TEXT_HTML)
    public Template getFormulario(@CookieParam("p_hash") String pHash, @PathParam("campanyaId") Long campanyaId, @CookieParam("uji-lang") String cookieIdioma) throws ParseException {

        userSessionManager.init(request, pHash, FICHAZONAPUBLICA);

        Long clienteId = (ParamUtils.isNotNull(userSessionManager.getCliente())) ? userSessionManager.getCliente().getId() : -1L;
        String idioma = publicacionService.getIdioma(cookieIdioma);

        CampanyaFormulario formulario = formularioService.getByCampanyaId(campanyaId, clienteId);

        return getTemplate(clienteId, formulario, idioma);
    }

    @GET
    @Path("{campanyaId}/baja")
    @Produces(MediaType.TEXT_HTML)
    public Template getFormularioBaja(@CookieParam("p_hash") String pHash, @PathParam("campanyaId") Long campanyaId, @CookieParam("uji-lang") String cookieIdioma) throws ParseException {

        userSessionManager.init(request, pHash, FICHAZONAPUBLICA);
        String idioma = publicacionService.getIdioma(cookieIdioma);

        Long clienteId = (ParamUtils.isNotNull(userSessionManager.getCliente())) ? userSessionManager.getCliente().getId() : -1L;

        if (clienteId.equals(-1L)) {
            String templateUrl = "crm/main";
            Template template = AppInfo.buildPagina(templateUrl, idioma);
            template.put("contenido", "/crm/formulario-no-encontrado");
            return template;
        }

        CampanyaFormulario formulario = formularioService.getFormularioBajaByCampanyaId(campanyaId);
        return getTemplate(clienteId, formulario, idioma);

    }

    private Template getTemplate(Long clienteId, CampanyaFormulario formulario, String idioma) throws ParseException {
        if (!ParamUtils.isNotNull(formulario)) {
            String templateUrl = "crm/main";
            Template template = AppInfo.buildPagina(templateUrl, idioma);
            template.put("contenido", "/crm/formulario-no-encontrado");
            return template;
        }

        formularioService.setIdioma(idioma);
        FormularioUI formularioUI = formularioService.getById(formulario.getId(), clienteId, userSessionManager.getCliente(), idioma);
        return publicacionService.dameTemplate(formularioUI, idioma);
    }

    @GET
    @Path("formulario/{formularioId}")
    @Produces(MediaType.TEXT_HTML)

    public Template getFormularioAutenticado(@CookieParam("p_hash") String pHash, @PathParam("formularioId") Long formularioId, @CookieParam("uji-lang") String cookieIdioma) throws ParseException{
        userSessionManager.init(request, pHash, FICHAZONAPUBLICA);
        Long clienteId = (ParamUtils.isNotNull(userSessionManager.getCliente())) ? userSessionManager.getCliente().getId() : -1L;

        Long connectedUserId = userSessionManager.getConnectedUserId();

        String idioma = publicacionService.getIdioma(cookieIdioma);
        formularioService.setIdioma(idioma);
        FormularioUI formularioUI;
        if (ParamUtils.isNotNull(connectedUserId) && connectedUserId != -1L) {
            formularioUI = formularioService.getById(formularioId, clienteId, userSessionManager.getCliente(), idioma);
        } else {
            formularioUI = formularioService.getById(formularioId, -1L, null, idioma);
        }
        return publicacionService.dameTemplate(formularioUI, idioma);

    }


    @POST
    @Path("formulario/{formularioId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage actualizaFormularioAutenticado(@CookieParam("p_hash") String pHash, @PathParam("formularioId") Long formularioId, @CookieParam("uji-lang") String cookieIdioma, UIEntity entity) throws Exception {
        ResponseMessage response = new ResponseMessage();

        userSessionManager.init(request, pHash, Boolean.FALSE);
        Cliente cliente = userSessionManager.getCliente();

        UIEntity datosCliente = entity.getRelations().get("datosCliente").get(0);
        List<UIEntity> datosExtra = entity.getRelations().get("datosExtra");

        formularioService.insertarFormularioCliente(formularioId, cliente, datosCliente, datosExtra);

        response.setSuccess(true);
        return response;

    }
}