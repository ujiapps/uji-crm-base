package es.uji.apps.crm.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ClienteViewDAO;
import es.uji.apps.crm.model.ClienteView;

@Service
public class ClienteViewService {

    private ClienteViewDAO clienteViewDAO;

    @Autowired
    public ClienteViewService(ClienteViewDAO clienteViewDAO) {
        this.clienteViewDAO = clienteViewDAO;
    }

    public ClienteView getClienteViewById(Long clienteId) {
        return clienteViewDAO.getClienteViewById(clienteId);
    }
}