package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.ClienteDatoTipo;
import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.services.ClienteDatoOpcionService;
import es.uji.apps.crm.services.ClienteDatoTipoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("clientedatotipo")
public class ClienteDatoTipoResource extends CoreBaseService {
    @InjectParam
    private ClienteDatoTipoService clienteDatoTipoService;

    @InjectParam
    private ClienteDatoOpcionService clienteDatoOpcionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClienteDatoTipos() {
        List<ClienteDatoTipo> lista = clienteDatoTipoService.getClientesDatosTipos();
        return modelToUI(lista);
    }

    private List<UIEntity> modelToUI(List<ClienteDatoTipo> clienteDatoTipos) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (ClienteDatoTipo clienteDatoTipo : clienteDatoTipos)
        {
            listaUI.add(modelToUI(clienteDatoTipo));
        }
        return listaUI;
    }

    private UIEntity modelToUI(ClienteDatoTipo clienteDatoTipo) {
        UIEntity entity = UIEntity.toUI(clienteDatoTipo);
        entity.put("clienteDatoTipoTipoNombre", clienteDatoTipo.getClienteDatoTipoTipo().getNombre());
        return entity;
    }

    @GET
    @Path("clientes")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClienteDatoTiposClientes() {
        return modelToUI(clienteDatoTipoService.getClientesDatosTiposClientes());
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getClienteDatoTipoById(@PathParam("id") Long clienteDatoTipoId) {
        return modelToUI(clienteDatoTipoService.getClienteDatoTipoById(clienteDatoTipoId));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addClienteDatoTipo(UIEntity entity) {
        ClienteDatoTipo clienteDatoTipo = UIToModel(entity);
        clienteDatoTipoService.addClienteDatoTipo(clienteDatoTipo);
        return modelToUI(clienteDatoTipo);
    }

    private ClienteDatoTipo UIToModel(UIEntity entity) {
        ClienteDatoTipo clienteDatoTipo = entity.toModel(ClienteDatoTipo.class);

        Tipo tipo = new Tipo();
        tipo.setId(Long.parseLong(entity.get("clienteDatoTipoTipoId")));
        clienteDatoTipo.setClienteDatoTipoTipo(tipo);

        return clienteDatoTipo;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateClienteDatoTipo(@PathParam("id") Long clienteDatoTipoId, UIEntity entity)
            throws Exception {
        ClienteDatoTipo clienteDatoTipo = UIToModel(entity);
        clienteDatoTipo.setId(clienteDatoTipoId);
        clienteDatoTipoService.updateClienteDatoTipo(clienteDatoTipo);
        return modelToUI(clienteDatoTipo);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long clienteDatoTipoId) {
        clienteDatoTipoService.deleteClienteDatoTipo(clienteDatoTipoId);
    }

}