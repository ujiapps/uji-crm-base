package es.uji.apps.crm.services.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.TipoTarifa;
import es.uji.apps.crm.model.TipoTarifaEnvio;
import es.uji.apps.crm.services.TipoTarifaEnvioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("tipotarifaenvio")
public class TipoTarifaEnvioResource extends CoreBaseService {
    @InjectParam
    private TipoTarifaEnvioService tipoTarifaEnvioService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTipoTarifasEnvioByTipoTarifa(@QueryParam("tipoTarifaId") Long tipoTarifaId) {
        TipoTarifa tipoTarifa = new TipoTarifa();
        tipoTarifa.setId(tipoTarifaId);
        return modelToUI(tipoTarifaEnvioService.getTipoTarifaEnvioByTipoTarifa(tipoTarifa));
    }

    private List<UIEntity> modelToUI(List<TipoTarifaEnvio> tipoTarifaEnvios) {
        return tipoTarifaEnvios.stream().map((TipoTarifaEnvio t) -> modelToUI(t)).collect(Collectors.toList());
    }

    private UIEntity modelToUI(TipoTarifaEnvio tipoTarifaEnvio) {
        return UIEntity.toUI(tipoTarifaEnvio);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addTipoTarifaEnvio(UIEntity entity) {
        TipoTarifaEnvio tipoTarifaEnvio = UIToModel(entity);
        tipoTarifaEnvioService.addTipoTarifaEnvio(tipoTarifaEnvio);
        return modelToUI(tipoTarifaEnvio);
    }

    private TipoTarifaEnvio UIToModel(UIEntity entity) {
        TipoTarifaEnvio tipoTarifaEnvio = entity.toModel(TipoTarifaEnvio.class);

        TipoTarifa tipoTarifa = new TipoTarifa();
        tipoTarifa.setId(ParamUtils.parseLong(entity.get("tipoTarifaId")));

        tipoTarifaEnvio.setTipoTarifa(tipoTarifa);

        return tipoTarifaEnvio;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateTarifa(UIEntity entity) {

        TipoTarifaEnvio tipoTarifaEnvio = UIToModel(entity);
        tipoTarifaEnvioService.updateTarifa(tipoTarifaEnvio);
        return modelToUI(tipoTarifaEnvio);
    }

    @PUT
    @Path("autoguardado")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void autoguardadoTarifaEnvio(@FormParam("tarifaEnvioId") Long id, @FormParam("cuerpo") String cuerpo) {
        tipoTarifaEnvioService.autoguardadoTarifaEnvio(id, cuerpo);
    }

    @PUT
    @Path("{id}/cuerpo")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity updateTarifaEnvioCuerpo(@PathParam("id") Long tipoTarifaEnvioId, @FormParam("cuerpo") String cuerpo) {
        return modelToUI(tipoTarifaEnvioService.updateTarifaEnvioCuerpo(tipoTarifaEnvioId, cuerpo));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long tarifaEnvioId) {

        tipoTarifaEnvioService.deleteTipoTarifaEnvio(tarifaEnvioId);
    }
}