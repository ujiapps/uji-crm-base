package es.uji.apps.crm.services.rest;

import java.io.StringWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.LazyInitializationException;
import org.springframework.transaction.annotation.Transactional;

import com.opencsv.CSVWriter;
import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.dao.ActualizaPersona;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteDato;
import es.uji.apps.crm.model.ClienteDatoOpcion;
import es.uji.apps.crm.model.ClienteGeneral;
import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.Persona;
import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.model.domains.TipoDato;
import es.uji.apps.crm.model.domains.TipoEstadoConfirmacionDato;
import es.uji.apps.crm.services.ClienteDatoOpcionService;
import es.uji.apps.crm.services.ClienteDatoService;
import es.uji.apps.crm.services.ClienteDatoTipoService;
import es.uji.apps.crm.services.ClienteGeneralService;
import es.uji.apps.crm.services.ClienteService;
import es.uji.apps.crm.services.GrupoService;
import es.uji.apps.crm.services.ItemService;
import es.uji.apps.crm.services.PersonaService;
import es.uji.apps.crm.services.TipoService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import static java.lang.Boolean.FALSE;

@Path("cliente")
public class ClienteResource extends CoreBaseService {
    @InjectParam
    private ClienteService clienteService;
    @InjectParam
    private ClienteDatoService consultaClienteDato;
    @InjectParam
    private ClienteDatoOpcionService consultaClienteDatoOpcion;
    @InjectParam
    private UtilsService utilsService;
    @InjectParam
    private PersonaService personaService;
    @InjectParam
    private ClienteDatoTipoService clienteDatoTipoService;
    @InjectParam
    private GrupoService grupoService;
    @InjectParam
    private ItemService itemService;
    @InjectParam
    private TipoService tipoService;
    @InjectParam
    private ClienteGeneralService clienteGeneralService;
    @InjectParam
    private ActualizaPersona actualizaPersona;


    @Path("{id}/fotos")
    public ClienteFotoResource getPlatformItem(
            @InjectParam ClienteFotoResource clienteFotoResource) {
        return clienteFotoResource;
    }

    @GET
    @Path("general")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClientesByClienteGeneralId(@QueryParam("clienteGeneralId") Long clienteGeneralId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<UIEntity> clientes = clienteService.getClientesByClienteGeneralId(connectedUserId, clienteGeneralId);
        return clientes;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getClienteById(@PathParam("id") Long clienteId) {
        return modelToUIConPersona(clienteService.getClienteById(clienteId));
    }

    private UIEntity modelToUIConPersona(Cliente cliente) {
        UIEntity entity = modelToUI(cliente);
        if (ParamUtils.isNotNull(cliente.getClienteGeneral().getPersona()))
        {
            entity.put("personaUjiNombre", cliente.getClienteGeneral().getPersona().getApellidosNombre());
        }
        return entity;
    }

    private UIEntity modelToUI(Cliente cliente) {
        UIEntity entity = UIEntity.toUI(cliente);

        if (ParamUtils.isNotNull(cliente.getClienteGeneral().getPersona()))
        {
            Persona persona = personaService.getPersonaByClienteId(cliente.getId());
            entity.put("personaId", persona.getId());
            entity.put("personaNombre", persona.getApellidosNombre());
        }

        if (!ParamUtils.isNotNull(cliente.getCorreoEstado()))
        {
            entity.put("correoEstadoId", TipoEstadoConfirmacionDato.VALIDO.getId());
        }

        if (!ParamUtils.isNotNull(cliente.getMovilEstado()))
        {
            entity.put("movilEstadoId", TipoEstadoConfirmacionDato.VALIDO.getId());
        }

        if (!ParamUtils.isNotNull(cliente.getPostalEstado()))
        {
            entity.put("postalEstadoId", TipoEstadoConfirmacionDato.VALIDO.getId());
        }

        entity.put("servicioNombre", cliente.getServicio().getNombre());

        return entity;
    }

    @GET
    @Path("envio/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClienteByEnvioId(@PathParam("id") Long envioId) {
        return modelToUI(clienteService.getClientesByEnvioId(envioId));
    }

    private List<UIEntity> modelToUI(List<Cliente> clientes) {

        List<UIEntity> lista = new ArrayList<>();

        for (Cliente cliente : clientes)
        {
            UIEntity entity = modelToUI(cliente);
            lista.add(entity);
        }

        return lista;
    }

    @GET
    @Path("item/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClientes(@PathParam("id") Long itemId) {
        List<Cliente> lista = clienteService.getClientesByItemId(itemId);
        return modelToUI(lista);
    }

    @GET
    @Path("{id}/datosextra/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDatosExtraByClienteId(@PathParam("id") Long clienteId) {
        List<ClienteDato> clienteDatos = clienteService.getClientesDatosLista(clienteId);
        return modelToUIDato(clienteDatos);
    }

    private List<UIEntity> modelToUIDato(List<ClienteDato> clienteDatos) {
        List<UIEntity> listUIEntity = new ArrayList<>();

        for (ClienteDato clienteDato : clienteDatos)
        {
            UIEntity entity = modelToUIDato(clienteDato);
            listUIEntity.add(entity);
        }
        return listUIEntity;
    }

    private UIEntity modelToUIDato(ClienteDato clienteDato) {

        UIEntity entity = UIEntity.toUI(clienteDato);

        entity.put("tipoDatoId", clienteDato.getClienteDatoTipo().getId());

        entity.put("tipoDatoNombre", clienteDato.getClienteDatoTipo().getNombre());
        entity.put("tipoDatoTipoNombre", clienteDato.getClienteDatoTipo().getClienteDatoTipoTipo()
                .getNombre());
        entity.put("tipoDatoTipoId", clienteDato.getClienteDatoTipo().getClienteDatoTipoTipo()
                .getId());
        entity.put("tipoAccesoId", clienteDato.getClienteDatoTipoAcceso().getId());

        Tipo tipoAcceso = tipoService.getTiposById(clienteDato.getClienteDatoTipoAcceso().getId());

        entity.put("tipoAccesoNombre", tipoAcceso.getNombre());
        entity.put("clienteId", clienteDato.getCliente().getId());

        if (ParamUtils.isNotNull(clienteDato.getCampanya()))
        {
            entity.put("campanyaId", clienteDato.getCampanya().getId());
        }

        try
        {
            if (clienteDato.getClienteDatoTipo() != null
                    && clienteDato.getClienteDatoTipo().getClientesDatosOpciones() != null)
            {
                for (ClienteDatoOpcion clienteDatoOpcion : clienteDato.getClienteDatoTipo()
                        .getClientesDatosOpciones())
                {
                    UIEntity uiOpcion = new UIEntity();
                    uiOpcion.put("id", clienteDatoOpcion.getId());
                    uiOpcion.put("nombre", clienteDatoOpcion.getEtiqueta());
                    uiOpcion.put("valor", clienteDatoOpcion.getValor());

                    entity.put("datosOpciones", uiOpcion);
                }
            }
        }
        catch (LazyInitializationException e)
        {
        }

        String tipo = clienteDato.getClienteDatoTipo().getClienteDatoTipoTipo().getNombre();

        if (tipo.substring(0, 1).equals(TipoDato.NUMERO.getNombre()))
        {
            entity.put("valor", clienteDato.getValorNumero());
        }
        else if (tipo.substring(0, 1).equals(TipoDato.TEXTO.getNombre()))
        {
            entity.put("valor", clienteDato.getValorTexto());
        }
        else if (tipo.substring(0, 1).equals(TipoDato.FECHA.getNombre()))
        {
            Date fecha = clienteDato.getValorFecha();
            entity.put("valor", (fecha != null) ? utilsService.fechaFormat(fecha) : null);
        }
        else if (tipo.substring(0, 1).equals(TipoDato.SELECCION.getNombre()))
        {
            entity.put("valor", clienteDato.getValorNumero());
        }
        else
        {
            entity.put("valor", clienteDato.getValorFicheroNombre());
        }
        return entity;
    }

    @GET
    @Path("{id}/persona")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getPersonaByClienteId(@PathParam("id") Long clienteId) {
        //throws RegistroNoEncontradoException {
        return UIEntity.toUI(personaService.getPersonaByClienteId(clienteId));
    }

    @GET
    @Path("fichero/{id}/raw")
    public Response getClienteDatoFichero(@PathParam("id") Long clienteDatoId) {
        ClienteDato clienteDato = consultaClienteDato.getClienteDatoById(clienteDatoId);

        String nombreFichero;
        String contentType;

        byte[] documento = clienteDato.getValorFicheroBinario();

        if (ParamUtils.isNotNull(documento))
        {
            nombreFichero = clienteDato.getValorFicheroNombre();
            contentType = clienteDato.getValorFicheroMimetype();

            return Response.ok(documento)
                    .header("Content-Disposition", "attachment; filename = " + nombreFichero)
                    .header("Content-Length", documento.length).type(contentType).build();
        }

        return Response.noContent().build();
    }

    @POST
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity addCliente(@FormParam("id") Long persona, @FormParam("tipo") String tipoUsuario, @FormParam("tipoIdentificacion") Long tipoIdentificacion,
                               @FormParam("identificacion") String identificacion, @FormParam("nombre") String nombre, @FormParam("apellidos") String apellidos,
                               @FormParam("correo") String correo, @FormParam("movil") String movil, @FormParam("paisPostalId") String paisPostalId,
                               @FormParam("provinciaPostalId") Long provinciaPostalId, @FormParam("servicio") Long servicioId, @FormParam("poblacionPostalId") Long poblacionPostalId,
                               @FormParam("postal") String postal, @FormParam("codigoPostal") String codigoPostal, @FormParam("fechaNacimiento") String fechaNacimiento, @FormParam("nacionalidadId") String nacionalidadId,
                               @FormParam("sexoId") Long sexo, @FormParam("provinciaPostalNombre") String provinciaPostalNombre, @FormParam("poblacionPostalNombre") String poblacionPostalNombre)
            throws ParseException {

        ClienteGeneral clienteGeneral;
        Boolean fichaZonaPrivada = FALSE;

        clienteGeneral = clienteGeneralService.getClienteGeneralByIdentificacionSinJoinPersona(identificacion);
        if (!ParamUtils.isNotNull(clienteGeneral.getId()))
        {
            clienteGeneral = clienteGeneralService.UIToModel(null, tipoIdentificacion, identificacion, persona);
            clienteGeneral.setDefinitivo(Boolean.TRUE);
            clienteGeneralService.addClienteGeneral(clienteGeneral);
            fichaZonaPrivada = Boolean.TRUE;
        }
        Cliente cliente = clienteService.UIToModel(null, tipoUsuario, nombre, apellidos, correo, null, null, movil, null, paisPostalId,
                provinciaPostalId, poblacionPostalId, postal, codigoPostal, null, utilsService.fechaParse(fechaNacimiento, "dd/MM/yyyy"), sexo, servicioId,
                fichaZonaPrivada, nacionalidadId, provinciaPostalNombre, poblacionPostalNombre, Boolean.TRUE, Boolean.TRUE);
        clienteService.addCliente(cliente);

        cliente.setClienteGeneral(clienteGeneral);
        clienteService.updateCliente(cliente);

        if (ParamUtils.isNotNull(clienteGeneral.getPersona()) && ParamUtils.isNotNull(cliente.getMovil()))
        {
            actualizaPersona.guardaMovilSuscriptor(clienteGeneral.getPersona().getId(), cliente.getMovil().substring(0, 9));
        }

        if (ParamUtils.isNotNull(clienteGeneral.getPersona()) && ParamUtils.isNotNull(cliente.getCorreo()))
        {
            actualizaPersona.guardaCorreoSuscriptor(clienteGeneral.getPersona().getId(), cliente.getCorreo());
        }

        return modelToUI(cliente);
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity updateCliente(@FormParam("clienteId") Long clienteId,
                                  @FormParam("clienteGeneralId") Long clienteGeneralId,
                                  @FormParam("persona") Long persona,
                                  @FormParam("tipo") String tipoUsuario,
                                  @FormParam("tipoIdentificacion") Long tipoIdentificacion,
                                  @FormParam("identificacion") String identificacion,
                                  @FormParam("nombre") String nombre,
                                  @FormParam("apellidos") String apellidos,
                                  @FormParam("correo") String correo,
                                  @FormParam("correoEstadoId") Long correoEstadoId,
                                  @FormParam("prefijoTelefono") String prefijoTelefono,
                                  @FormParam("movil") String movil,
                                  @FormParam("movilEstadoId") Long movilEstadoId,
                                  @FormParam("paisPostalId") String paisPostalId,
                                  @FormParam("provinciaPostalId") Long provinciaPostalId,
                                  @FormParam("poblacionPostalId") Long poblacionPostalId,
                                  @FormParam("postal") String postal,
                                  @FormParam("codigoPostal") String codigoPostal,
                                  @FormParam("postalEstadoId") Long postalEstadoId,
                                  @FormParam("fechaNacimiento") String fechaNacimiento,
                                  @FormParam("sexo") Long sexo,
                                  @FormParam("servicioId") Long servicioId,
                                  @FormParam("fichaZonaPrivada") Boolean fichaZonaPrivada,
                                  @FormParam("nacionalidadId") String nacionalidadId,
                                  @FormParam("provinciaPostalNombre") String provinciaPostalNombre,
                                  @FormParam("poblacionPostalNombre") String poblacionPostalNombre,
                                  @FormParam("recibePostal") @DefaultValue("false") Boolean recibePostal,
                                  @FormParam("recibeCorreo") @DefaultValue("false") Boolean recibeCorreo)
            throws ParseException {

        ClienteGeneral clienteGeneral = clienteGeneralService.UIToModel(clienteGeneralId, tipoIdentificacion, identificacion, persona);
        Date fechaNacimientoDate = null;

        if (ParamUtils.isNotNull(fechaNacimiento))
        {
            fechaNacimientoDate = utilsService.fechaParse(fechaNacimiento, "dd/MM/yyyy");
        }

        Cliente cliente = clienteService.UIToModel(clienteId, tipoUsuario, nombre, apellidos, correo, correoEstadoId, prefijoTelefono, movil, movilEstadoId, paisPostalId,
                provinciaPostalId, poblacionPostalId, postal, codigoPostal, postalEstadoId, fechaNacimientoDate, sexo, servicioId, fichaZonaPrivada, nacionalidadId,
                provinciaPostalNombre, poblacionPostalNombre, recibePostal, recibeCorreo);

        clienteGeneralService.update(clienteGeneral);
        cliente.setClienteGeneral(clienteGeneral);
        clienteService.updateCliente(cliente);

        if (ParamUtils.isNotNull(clienteGeneral.getPersona()) && ParamUtils.isNotNull(cliente.getMovil()))
        {
            actualizaPersona.guardaMovilSuscriptor(clienteGeneral.getPersona().getId(), cliente.getMovil().substring(0, 9));
        }

        if (ParamUtils.isNotNull(clienteGeneral.getPersona()) && ParamUtils.isNotNull(cliente.getCorreo()))
        {
            actualizaPersona.guardaCorreoSuscriptor(clienteGeneral.getPersona().getId(), cliente.getCorreo());
        }
        return modelToUI(cliente);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long clienteId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        clienteService.delete(connectedUserId, clienteId);
    }

    @DELETE
    @Path("datosextra/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteDatoExtra(@PathParam("id") Long datoId) {
        consultaClienteDato.delete(datoId);
    }

    private String entityToCSV(List<Cliente> lista, List<Long> checkedDatosExtra,
                               List<Long> checkedGrupos) {

        final char DELIMITER = ';';

        StringWriter writer = new StringWriter();
        CSVWriter csvWriter;

        try
        {
            csvWriter = new CSVWriter(writer, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");

            List<String[]> records = new ArrayList<>();
            List<String> record = new ArrayList<>(Arrays.asList("APELLIDOS, NOMBRE",
                    "IDENTIFICACION", "CORREO", "MOVIL", "POSTAL", "TIPO"));

            for (Long checked : checkedDatosExtra)
            {
                record.add(clienteDatoTipoService.getClienteDatoTipoById(checked).getNombre());
            }

            for (Long checked : checkedGrupos)
            {
                record.add(grupoService.getGrupoById(checked).getNombreCa());
            }

            String[] recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);

            for (Cliente cliente : lista)
            {
                String movil = null;
                if (cliente.getMovil() != null)
                {
                    movil = cliente.getMovil();
                }

                List<String> recordCliente = new ArrayList<>(Arrays.asList(
                        cliente.getApellidosNombre(), cliente.getClienteGeneral().getIdentificacion(),
                        cliente.getCorreo(), movil, cliente.getPostal(),
                        cliente.getTipo()));

                for (Long checked : checkedDatosExtra)
                {
                    ClienteDato clienteDato = consultaClienteDato.getClienteDatoByClienteAndTipoId(
                            cliente, checked);
                    if (clienteDato != null)
                    {
                        String valor = clienteDato.getValorSegunTipo();
                        recordCliente.add(valor);
                    }
                    else
                    {
                        recordCliente.add("");
                    }

                }

                for (Long checked : checkedGrupos)
                {

                    List<Item> listaItems = itemService.getItemsByGrupoIdAndClienteId(checked,
                            cliente.getId());

                    if (!listaItems.isEmpty())
                    {
                        recordCliente.add(pasaListToString(listaItems));
                    }
                    else
                    {
                        recordCliente.add("");
                    }

                }

                recordArray = new String[recordCliente.size()];
                recordCliente.toArray(recordArray);
                records.add(recordArray);
            }
            csvWriter.writeAll(records);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return writer.toString();
    }

    private String pasaListToString(List<Item> lista) {

        StringBuilder valores = new StringBuilder();
        for (Item item : lista)
        {
            if (valores.length() == 0)
            {
                valores = new StringBuilder(item.getNombreCa());
            }
            else
            {
                valores.append(", ").append(item.getNombreCa());
            }
        }
        return valores.toString();
    }
}