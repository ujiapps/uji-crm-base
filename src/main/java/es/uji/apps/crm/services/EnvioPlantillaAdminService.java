package es.uji.apps.crm.services;

import es.uji.apps.crm.dao.EnvioPlantillaAdminDAO;
import es.uji.apps.crm.model.EnvioPlantillaAdmin;
import org.springframework.stereotype.Service;

@Service
public class EnvioPlantillaAdminService {
    private EnvioPlantillaAdminDAO envioPlantillaAdminDAO;

    public EnvioPlantillaAdminService(EnvioPlantillaAdminDAO envioPlantillaAdminDAO) {
        this.envioPlantillaAdminDAO = envioPlantillaAdminDAO;
    }

    public EnvioPlantillaAdmin getEnvioPlantillaAdminByNombre(String accesoFormularioPremium) {
        return envioPlantillaAdminDAO.getEnvioPlantillaAdminByNombre(accesoFormularioPremium);
    }
}
