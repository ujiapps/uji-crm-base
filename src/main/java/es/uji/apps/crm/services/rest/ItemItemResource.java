package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.ItemItem;
import es.uji.apps.crm.services.ItemItemService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("itemitem")
public class ItemItemResource extends CoreBaseService {
    @InjectParam
    private ItemItemService itemItemService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItemsItems() {
        return null;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getItemItemById(@PathParam("id") Long itemItemId) {
        return null;
    }

    @GET
    @Path("item/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItemsItemsByItemId(@PathParam("id") Long itemId) {
        return modelToUI(itemItemService.getItemsItemsByItemId(itemId));
    }

    private List<UIEntity> modelToUI(List<ItemItem> itemItems) {
        List<UIEntity> lista = new ArrayList<>();

        for (ItemItem itemItem : itemItems)
        {
            lista.add(modelToUI(itemItem));
        }
        return lista;

    }

    private UIEntity modelToUI(ItemItem itemItem) {
        UIEntity entity = UIEntity.toUI(itemItem);

        entity.put("itemOrigen", itemItem.getItemOrigen().getNombreCa());
        entity.put("itemDestino", itemItem.getItemDestino().getNombreCa());
        return entity;
    }

    @GET
    @Path("item/origen/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getItemsItemsOrigenByItemId(@PathParam("id") Long itemId) {
        return modelToUI(itemItemService.getItemsItemsOrigenByItemId(itemId));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addGrupo(UIEntity entity) {
        ItemItem itemItem = UIToModel(entity);
        itemItemService.addItemItem(itemItem);
        return modelToUI(itemItem);
    }

    private ItemItem UIToModel(UIEntity entity) {
        ItemItem itemItem = entity.toModel(ItemItem.class);

        Item itemOrigen = new Item();
        itemOrigen.setId(ParamUtils.parseLong(entity.get("itemIdOrigen")));
        itemItem.setItemOrigen(itemOrigen);

        Item itemDestino = new Item();
        itemDestino.setId(ParamUtils.parseLong(entity.get("itemIdDestino")));
        itemItem.setItemDestino(itemDestino);

        return itemItem;
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long itemItemId) {
        itemItemService.deleteItemItem(itemItemId);
    }
}