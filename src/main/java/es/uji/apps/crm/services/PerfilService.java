package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.PerfilDAO;
import es.uji.apps.crm.model.Perfil;

@Service
public class PerfilService {
    private PerfilDAO perfilDAO;

    @Autowired
    public PerfilService(PerfilDAO perfilDAO) {
        this.perfilDAO = perfilDAO;
    }

    public List<Perfil> getPerfiles() {
        return perfilDAO.getPerfiles();
    }

    public List<Perfil> getPerfilesFaltanByProgramaId(Long programaId) {
        return perfilDAO.getPerfilesFaltanByProgramaId(programaId);
    }
}