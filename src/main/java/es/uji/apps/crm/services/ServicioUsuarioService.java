package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ServicioUsuarioDAO;
import es.uji.apps.crm.model.PersonaPasPdi;
import es.uji.apps.crm.model.Servicio;
import es.uji.apps.crm.model.ServicioUsuario;

@Service
public class ServicioUsuarioService {
    private ServicioUsuarioDAO servicioUsuarioDAO;

    @Autowired
    public ServicioUsuarioService(ServicioUsuarioDAO servicioUsuarioDAO) {
        this.servicioUsuarioDAO = servicioUsuarioDAO;
    }

    public List<ServicioUsuario> getServiciosUsuarios() {
        return servicioUsuarioDAO.getServicioUSuario();
    }

    public List<ServicioUsuario> getServiciosUsuariosByServicioId(Long servicioId) {
        return servicioUsuarioDAO.getServiciosUsuarioById(servicioId);
    }

    public ServicioUsuario insertaNuevoServicioUsuario(Long personaId, Long servicioId) {
        ServicioUsuario servicioUsuario = new ServicioUsuario();

        PersonaPasPdi personaPasPdi = new PersonaPasPdi();
        personaPasPdi.setId(personaId);
        servicioUsuario.setPersonaPasPdi(personaPasPdi);

        Servicio servicio = new Servicio();
        servicio.setId(servicioId);
        servicioUsuario.setServicio(servicio);

        servicioUsuarioDAO.insert(servicioUsuario);
        servicioUsuario.setId(servicioUsuario.getId());

        return servicioUsuario;
    }

    public void delete(Long servicioUsuarioId) {
        servicioUsuarioDAO.delete(ServicioUsuario.class, servicioUsuarioId);

    }

}