package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.services.CampanyaActoEnvioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

public class CampanyaActoEnvioResource extends CoreBaseService {

    @PathParam("id")
    private Long campanyaActoId;

    @InjectParam
    private CampanyaActoEnvioService campanyaActoEnvioService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getCampanyasActosByCampanya(@PathParam("id") Long campanyaActoId) {

        return UIEntity.toUI(campanyaActoEnvioService.getCampanyasActoEnvioByCampanyaActo(campanyaActoId));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity insertCampanyaActoEnvio(@FormParam("campanyaActoId") Long campanyaActoId, @FormParam("asunto") String asunto,
                                            @FormParam("cuerpo") String cuerpo, @FormParam("desde") String desde,
                                            @FormParam("responder") String responder){
        return UIEntity.toUI(campanyaActoEnvioService.addCampanyaActoEnvio(campanyaActoId, asunto, cuerpo, desde, responder));
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity insertCampanyaActoEnvio(@FormParam("asunto") String asunto, @FormParam("cuerpo") String cuerpo,
                                            @FormParam("desde") String desde, @FormParam("responder") String responder,
                                            @PathParam("id") Long campanyaActoEnvioId){
        return UIEntity.toUI(campanyaActoEnvioService.updateCampanyaActoEnvio(campanyaActoEnvioId, asunto, cuerpo, desde, responder));
    }

}