package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Poblacion;
import es.uji.apps.crm.services.PoblacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("poblacion")
public class PoblacionResource extends CoreBaseService {
    @InjectParam
    private PoblacionService poblacionService;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPoblacionesByProvinciaId(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @PathParam("id") Long provinciaId) {
        return modelToUI(poblacionService.getPoblacionesByProvinciaId(provinciaId, idioma), idioma);
    }

    private List<UIEntity> modelToUI(List<Poblacion> poblaciones, String idioma) {

        List<UIEntity> lista = new ArrayList<>();
        for (Poblacion poblacion : poblaciones)
        {
            lista.add(modelToUI(poblacion, idioma));
        }

        return lista;
    }

    private UIEntity modelToUI(Poblacion poblacion, String idioma) {
        UIEntity entity = UIEntity.toUI(poblacion);

        if (idioma.equalsIgnoreCase("ca"))
        {
            entity.put("nombre", poblacion.getNombreCA());
        }
        if (idioma.equalsIgnoreCase("en") || idioma.equalsIgnoreCase("uk"))
        {
            entity.put("nombre", poblacion.getNombreEN());
        }
        if (idioma.equalsIgnoreCase("es"))
        {
            entity.put("nombre", poblacion.getNombreES());
        }
        return entity;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPoblaciones(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        return modelToUI(poblacionService.getPoblaciones(idioma), idioma);
    }


}