package es.uji.apps.crm.services;

import java.util.List;

import es.uji.apps.crm.dao.ClienteItemDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteDato;
import es.uji.apps.crm.model.ClienteItem;
import es.uji.apps.crm.model.Item;
import es.uji.apps.crm.model.ItemItem;
import es.uji.apps.crm.model.domains.TipoItem;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Service
public class ClienteItemService {

    private ItemItemService itemItemService;
    private ItemService itemService;
    private ClienteDatoService clienteDatoService;
    private ClienteItemDAO clienteItemDAO;


    @Autowired
    public ClienteItemService(ClienteItemDAO clienteItemDAO, ItemItemService itemItemService,
                              ItemService itemService, ClienteDatoService clienteDatoService) {
        this.clienteItemDAO = clienteItemDAO;
        this.itemItemService = itemItemService;
        this.itemService = itemService;
        this.clienteDatoService = clienteDatoService;

    }

    public ClienteItem getItemsClientes(Long itemId, Long clienteId) {
        return clienteItemDAO.getItemsClientes(itemId, clienteId);

    }

    public List<ClienteItem> getItemsByGrupoIdAndClienteId(Long grupoId, Long clienteId) {
        return clienteItemDAO.getItemsByGrupoIdAndClienteId(grupoId, clienteId);
    }

    public List<ClienteItem> getItemsByClienteId(Long clienteId) {
        return clienteItemDAO.getItemsByClienteId(clienteId);
    }

    public List<ClienteItem> getAllItemsByClienteId(Long clienteId) {
        return clienteItemDAO.getAllItemsByClienteId(clienteId);
    }

    public void updateClienteItem(ClienteItem clienteItem) {
        clienteItemDAO.update(clienteItem);
    }

    public ClienteItem addClienteItem(Long clienteId, Long itemId, Long clienteItemRelacionado, Long clienteDato, Long campanyaId, Boolean correo, Boolean postal) {

        ClienteItem clienteItem = new ClienteItem();

        try
        {

            Cliente cliente = new Cliente();
            cliente.setId(clienteId);
            clienteItem.setCliente(cliente);

            Item item = itemService.getItemById(itemId);
            clienteItem.setItem(item);

            if (ParamUtils.isNotNull(clienteItemRelacionado))
            {
                clienteItem.setClienteItemRelacionado(clienteItemRelacionado);
            }

            if (ParamUtils.isNotNull(clienteDato))
            {
                clienteItem.setClienteDato(clienteDato);
            }

            if (ParamUtils.isNotNull(campanyaId))
            {
                Campanya campanya = new Campanya();
                campanya.setId(campanyaId);
                clienteItem.setCampanya(campanya);
            }

            if (ParamUtils.isNotNull(correo))
            {
                clienteItem.setCorreo(correo);
            }
            else
            {
                clienteItem.setCorreo(valorItem(item.getCorreo().getId()));
            }

            if (ParamUtils.isNotNull(postal))
            {
                clienteItem.setPostal(postal);
            }
            else
            {
                clienteItem.setPostal(valorItem(item.getCorreoPostal().getId()));
            }

            clienteItemDAO.insert(clienteItem);

            List<ItemItem> itemItems = itemItemService.getItemItem(item);
            for (ItemItem itemItem : itemItems)
            {
                addClienteItem(clienteItem.getCliente(), itemItem.getItemDestino());
            }

        }
        catch (DataIntegrityViolationException e)
        {
        }
        return clienteItem;
    }

    public ClienteItem addClienteItemSinDuplicados(Cliente cliente, Item item, Long clienteItemRelacionado, Long clienteDato, Long campanyaId) {

        ClienteItem clienteItem = new ClienteItem();

        List<ClienteItem> clienteItems = getItemsByGrupoIdAndClienteId(item.getGrupo().getId(), cliente.getId());

        if (clienteItems.size() > 0)
        {
            clienteItem = clienteItems.get(0);
            clienteItem.setItem(item);
            updateClienteItem(clienteItem);
        }
        else
        {
            clienteItem.setCliente(cliente);

            clienteItem.setItem(item);

            if (ParamUtils.isNotNull(clienteItemRelacionado))
            {
                clienteItem.setClienteItemRelacionado(clienteItemRelacionado);
            }

            if (ParamUtils.isNotNull(clienteDato))
            {
                clienteItem.setClienteDato(clienteDato);
            }

            if (ParamUtils.isNotNull(campanyaId))
            {
                Campanya campanya = new Campanya();
                campanya.setId(campanyaId);
                clienteItem.setCampanya(campanya);
            }

            clienteItem.setCorreo(valorItem(item.getCorreo().getId()));
            clienteItem.setPostal(valorItem(item.getCorreoPostal().getId()));

            clienteItemDAO.insert(clienteItem);


        }

        List<ItemItem> itemItems = itemItemService.getItemItem(item);
        for (ItemItem itemItem : itemItems)
        {
            addClienteItem(clienteItem.getCliente(), itemItem.getItemDestino());
        }
        return clienteItem;
    }

    public ClienteItem addClienteItem(Cliente cliente, Item item) {


        try
        {
            ClienteItem clienteItem = new ClienteItem();
            clienteItem.setCliente(cliente);
            clienteItem.setItem(item);
            clienteItem.setCorreo(valorItem(item.getCorreo().getId()));
            clienteItem.setPostal(valorItem(item.getCorreoPostal().getId()));

            clienteItemDAO.insert(clienteItem);

            List<ItemItem> itemItems = itemItemService.getItemItem(clienteItem.getItem());
            for (ItemItem itemItem : itemItems)
            {
                addClienteItem(clienteItem.getCliente(), itemItem.getItemDestino());
            }
            return clienteItem;

        }
        catch (DataIntegrityViolationException e)
        {
        }
        return null;
    }

    public void deleteClienteItem(Long clienteItemId) {
        clienteItemDAO.delete(ClienteItem.class, clienteItemId);
    }

    public Long getClientesItemByItemId(Long itemId) {

        return clienteItemDAO.getClientesItemByItemId(itemId);
    }

    public Long getCountClientesItem(Long tipoEnvioId, Long itemId) {
        return clienteItemDAO.getCountClientesItem(tipoEnvioId, itemId);
    }

    public ClienteItem updateClienteByItemId(Long clienteItemId, Long clienteId) {
        ClienteItem clienteItem = clienteItemDAO.getItemsClientesById(clienteItemId);

        List<ClienteDato> datosExtra = clienteDatoService.getClienteDatoByClienteAndItemId(
                clienteItem.getCliente(), clienteItem.getItem().getId());

        Cliente cliente = new Cliente();
        cliente.setId(clienteId);

        if (datosExtra != null)
        {
            for (ClienteDato clienteDato : datosExtra)
            {
                clienteDato.setCliente(cliente);
                clienteDatoService.update(clienteDato);
            }
        }

        clienteItem.setCliente(cliente);
        clienteItemDAO.update(clienteItem);
        return clienteItem;

    }

    public ClienteItem updateClienteItemById(Long clienteItemId, Cliente cliente, Item item) {

        ClienteItem clienteItem = new ClienteItem();
        clienteItem.setId(clienteItemId);
        clienteItem.setCliente(cliente);
        clienteItem.setItem(item);
        clienteItem.setCorreo(valorItem(item.getCorreo().getId()));
        clienteItem.setPostal(valorItem(item.getCorreoPostal().getId()));

        clienteItemDAO.update(clienteItem);
        return clienteItem;
    }

    public Boolean valorItem(Long tipoItemId) {
        return (tipoItemId.equals(TipoItem.MODIFICABLE_DEFECTO.getId()) || tipoItemId
                .equals(TipoItem.NO_MODIFICABLE_DEFECTO.getId()));
    }

    public List<ClienteItem> getItemRelacionadosByClienteIdAndItemId(Long clienteId, Long itemId) {
        return clienteItemDAO.getItemRelacionadosByClienteIdAndItemId(clienteId, itemId);
    }

    public ClienteItem getClienteItemByItemAndCliente(Long clienteId, Long itemId) {
        return clienteItemDAO.getItemsClientes(itemId, clienteId);
    }

    public ClienteItem getClienteItemCliente(Long clienteItemPaisNacimiento, Long clienteId) {
        return clienteItemDAO.getClienteItemCliente(clienteItemPaisNacimiento, clienteId);
    }

    public List<ClienteItem> getItemsVisiblesByClienteIdAndProgramaId(Long clienteId, Long programaId) {
        return clienteItemDAO.getItemsVisiblesByClienteIdAndProgramaId(clienteId, programaId);
    }

    public ClienteItem updateClienteItem(UIEntity entity) {

        ClienteItem clienteItem = UIToModel(entity);
        clienteItemDAO.update(clienteItem);
        return clienteItem;
    }

    private ClienteItem UIToModel(UIEntity entity) {

        ClienteItem clienteItem = entity.toModel(ClienteItem.class);

        Cliente cliente = new Cliente();
        cliente.setId(ParamUtils.parseLong(entity.get("clienteId")));
        clienteItem.setCliente(cliente);

        Item item = itemService.getItemById(ParamUtils.parseLong(entity.get("itemId")));
        clienteItem.setItem(item);

        return clienteItem;
    }

    public ClienteItem updateClienteItem(Long clienteItemId, Integer correo, Integer postal) {

        ClienteItem clienteItem = getClienteItemById(clienteItemId);

        clienteItem.setCorreo(correo != 0);
        clienteItem.setPostal(postal != 0);

        clienteItemDAO.update(clienteItem);

        return clienteItem;
    }

    public ClienteItem getClienteItemById(Long clienteItemId) {
        return clienteItemDAO.getItemsClientesById(clienteItemId);
    }

    public List<ClienteItem> getItemsByGrupoIdAndClienteIdAndCampanyaId(Long grupoId, Long clienteId, Long campanyaId) {
        return clienteItemDAO.getItemsByGrupoIdAndClienteIdAndCampanyaId(grupoId, clienteId, campanyaId);
    }
}