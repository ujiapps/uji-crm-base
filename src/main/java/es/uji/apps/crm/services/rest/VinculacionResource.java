package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.model.Vinculacion;
import es.uji.apps.crm.services.VinculacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("vinculacion")
public class VinculacionResource extends CoreBaseService {
    @InjectParam
    private VinculacionService vinculacionService;

    @GET
    @Path("cliente/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getVinculacionesBycliente1Id(@PathParam("id") Long clienteId) {
        List<Vinculacion> vinculaciones = vinculacionService
                .getVinculacionesByCliente1Id(clienteId);
        return modelToUI(vinculaciones);
    }

    private List<UIEntity> modelToUI(List<Vinculacion> vinculaciones) {
        List<UIEntity> listaEntity = new ArrayList<UIEntity>();

        for (Vinculacion vinculacion : vinculaciones)
        {
            listaEntity.add(modelToUI(vinculacion));
        }
        return listaEntity;
    }

    private UIEntity modelToUI(Vinculacion vinculacion) {
        UIEntity entity = UIEntity.toUI(vinculacion);
        entity.put("cliente1Nombre", vinculacion.getCliente1().getNombreApellidos());
        entity.put("cliente2Nombre", vinculacion.getCliente2().getNombreApellidos());
        entity.put("tipoNombre", vinculacion.getTipo().getNombre());
        return entity;
    }

    @GET
    @Path("inversa/cliente/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getVinculacionesInversasBycliente1Id(@PathParam("id") Long clienteId) {
        List<Vinculacion> vinculaciones = vinculacionService
                .getVinculacionesInversasByClienteId(clienteId);
        return modelToUI(vinculaciones);
    }

    @POST
    @Path("cliente/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addVinculacion(@PathParam("id") Long clienteId, UIEntity entity) {
        ParamUtils.checkNotNull(clienteId, entity.get("cliente2Id"), entity.get("tipoId"));

        Vinculacion vinculacion = UIToModel(clienteId, entity);
        vinculacionService.addVinculacion(vinculacion);
        return modelToUI(vinculacion);
    }

    private Vinculacion UIToModel(Long clienteId, UIEntity entity) {
        Vinculacion vinculacion = entity.toModel(Vinculacion.class);

        Cliente cliente1 = new Cliente();
        cliente1.setId(clienteId);
        vinculacion.setCliente1(cliente1);

        Cliente cliente2 = new Cliente();
        cliente2.setId(Long.parseLong(entity.get("cliente2Id")));
        vinculacion.setCliente2(cliente2);

        Tipo tipo = new Tipo();
        tipo.setId(Long.parseLong(entity.get("tipoId")));
        vinculacion.setTipo(tipo);

        return vinculacion;
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long vinculacionId) {
        vinculacionService.delete(vinculacionId);
    }
}