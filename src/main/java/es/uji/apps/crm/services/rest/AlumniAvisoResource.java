package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.services.AlumniService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;

public class AlumniAvisoResource extends CoreBaseService {

    @Path("aviso")
    public AlumniAvisoResource getPlatformItem(
            @InjectParam AlumniAvisoResource alumniAvisoResource) {
        return alumniAvisoResource;
    }

    @InjectParam
    private AlumniService alumniService;

    @GET
    @Path("alta/correcto")
    @Produces(MediaType.TEXT_HTML)
    public Template getTemplateFormularioOk(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {
        return alumniService.getTemplateAviso(idioma, "aviso.formulario.premium.ok", "/crm/rest/alumni2024/login/", request.getPathInfo());
    }

    @GET
    @Path("alta/incorrecto")
    @Produces(MediaType.TEXT_HTML)
    public Template getTemplateFormularioKo(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {
        return alumniService.getTemplateAviso(idioma, "aviso.formulario.premium.ko", "/crm/rest/alumni2024/login/", request.getPathInfo());
    }
}