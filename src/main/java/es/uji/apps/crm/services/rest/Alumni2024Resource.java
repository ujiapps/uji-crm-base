package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.services.rest.alumni.AlumniHashResource;
import es.uji.apps.crm.services.rest.alumni.AlumniSSOResource;

import javax.ws.rs.*;

@Path("alumni2024")
public class Alumni2024Resource {

    @Path("login")
    public AlumniLoginResource getPlatformItem(
            @InjectParam AlumniLoginResource alumniLoginResource) {
        return alumniLoginResource;
    }

    @Path("premium")
    public AlumniPremiumResource getPlatformItem(
            @InjectParam AlumniPremiumResource alumniPremiumResource) {
        return alumniPremiumResource;
    }

    @Path("hash")
    public AlumniHashResource getPlatformItem(
            @InjectParam AlumniHashResource alumniHashResource) {
        return alumniHashResource;
    }

    @Path("sso")
    public AlumniSSOResource getPlatformItem(
            @InjectParam AlumniSSOResource alumniSSOResource) {
        return alumniSSOResource;
    }

    @Path("contacto")
    public AlumniContactoResource getPlatformItem(
            @InjectParam AlumniContactoResource alumniContactoResource) {
        return alumniContactoResource;
    }
}
