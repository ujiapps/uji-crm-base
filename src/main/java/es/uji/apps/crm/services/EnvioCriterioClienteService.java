package es.uji.apps.crm.services;

import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.EnvioCriterioClienteDAO;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.Envio;
import es.uji.apps.crm.model.EnvioCriterioCliente;

@Service
public class EnvioCriterioClienteService {

    private EnvioCriterioClienteDAO envioCriterioClienteDAO;

    @Autowired
    public EnvioCriterioClienteService(EnvioCriterioClienteDAO envioCriterioClienteDAO) {
        this.envioCriterioClienteDAO = envioCriterioClienteDAO;
    }

    public EnvioCriterioCliente  addEnvioCriterioClienteByEnvioId(Long envioId, Long clienteId) {

        envioCriterioClienteDAO.deleteEnvioCriterioCliente(envioId, clienteId, null);

        EnvioCriterioCliente envioCriterioCliente = new EnvioCriterioCliente();

        Cliente cliente = new Cliente();
        cliente.setId(clienteId);
        envioCriterioCliente.setCliente(cliente);

        Envio envio = new Envio();
        envio.setId(envioId);
        envioCriterioCliente.setEnvio(envio);

        envioCriterioCliente.setAccion(1L);

        return envioCriterioClienteDAO.insert(envioCriterioCliente);
    }

    public EnvioCriterioCliente deleteEnvioCriterioClienteByEnvioId(Long envioId, Long clienteId, String correo) {

        envioCriterioClienteDAO.deleteEnvioCriterioCliente(envioId, clienteId, correo);

        EnvioCriterioCliente envioCriterioCliente = new EnvioCriterioCliente();

        Envio envio = new Envio();
        envio.setId(envioId);
        envioCriterioCliente.setEnvio(envio);

        if (ParamUtils.isNotNull(clienteId)){
            Cliente cliente = new Cliente();
            cliente.setId(clienteId);
            envioCriterioCliente.setCliente(cliente);
        }

        envioCriterioCliente.setCorreo(correo);
        envioCriterioCliente.setAccion(0L);

        return envioCriterioClienteDAO.insert(envioCriterioCliente);
    }
}