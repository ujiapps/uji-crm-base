package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Provincia;
import es.uji.apps.crm.services.ProvinciaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("provincia")
public class ProvinciaResource extends CoreBaseService {
    @InjectParam
    private ProvinciaService provinciaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getProvincias(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) {

        return modelToUI(provinciaService.getProvincias(idioma), idioma);
    }

    private List<UIEntity> modelToUI(List<Provincia> provincias, String idioma) {

        List<UIEntity> lista = new ArrayList<>();
        for (Provincia provincia : provincias)
        {
            lista.add(modelToUI(provincia, idioma));
        }

        return lista;
    }


    private UIEntity modelToUI(Provincia provincia, String idioma) {
        UIEntity entity = UIEntity.toUI(provincia);

        if (idioma.equalsIgnoreCase("ca"))
        {
            entity.put("nombre", provincia.getNombreCA());
        }
        if (idioma.equalsIgnoreCase("en") || idioma.equalsIgnoreCase("uk"))
        {
            entity.put("nombre", provincia.getNombreEN());
        }
        if (idioma.equalsIgnoreCase("es"))
        {
            entity.put("nombre", provincia.getNombreES());
        }
        return entity;
    }


}