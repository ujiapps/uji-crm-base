package es.uji.apps.crm.services.rest;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteTarifa;
import es.uji.apps.crm.model.TipoTarifa;
import es.uji.apps.crm.services.ClienteTarifaService;
import es.uji.apps.crm.services.TipoTarifaService;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("clientetarifa")
public class ClienteTarifaResource extends CoreBaseService {

    @InjectParam
    private ClienteTarifaService clienteTarifaService;
    @InjectParam
    private UtilsService utilsService;
    @InjectParam
    private TipoTarifaService tipoTarifaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClienteTarifaByClienteIdAndCampanyaId(@QueryParam("clienteId") Long cliente, @QueryParam("campanyaId") Long campanya) {
        return UIEntity.toUI(clienteTarifaService.getClienteTarifaByClienteIdAndCampanyaId(cliente, campanya));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addClienteTarifa(UIEntity entity) throws ParseException {
        ClienteTarifa clienteTarifa = UIToModel(entity);
        clienteTarifaService.addClienteTarifa(clienteTarifa);
        return UIEntity.toUI(clienteTarifa);

    }

    private ClienteTarifa UIToModel(UIEntity entity) throws ParseException {
        ClienteTarifa clienteTarifa = entity.toModel(ClienteTarifa.class);

        Cliente cliente = new Cliente();
        cliente.setId(Long.parseLong(entity.get("clienteId")));
        clienteTarifa.setCliente(cliente);

        TipoTarifa tipoTarifa = tipoTarifaService.getTipoTarifaById(entity.getLong("tipoTarifaId"));
//        tipoTarifa.setId(ParamUtils.parseLong(entity.get()));
        clienteTarifa.setTipoTarifa(tipoTarifa);

        if (ParamUtils.isNotNull(entity.get("fechaInicioTarifa")))
        {
            Date fecha = utilsService.fechaParse(entity.get("fechaInicioTarifa").substring(0, 10));
            clienteTarifa.setFechaInicioTarifa(fecha);
        }

        if (ParamUtils.isNotNull(entity.get("fechaCaducidad")))
        {
            Date fecha = utilsService.fechaParse(entity.get("fechaCaducidad").substring(0, 10));
            clienteTarifa.setFechaCaducidad(fecha);
        }

        return clienteTarifa;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateClienteTarifa(UIEntity entity) throws ParseException {
        ClienteTarifa clienteTarifa = UIToModel(entity);
        clienteTarifaService.updateClienteTarifa(clienteTarifa);
        return UIEntity.toUI(clienteTarifa);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long clienteTarifaId) {
        clienteTarifaService.delete(clienteTarifaId);
    }

//    private UIEntity modelToUI(ClienteTarifa clienteTarifa) {
//        UIEntity entity = UIEntity.toUI(clienteTarifa);
////        entity.put("tipoTarifaNombre", clienteTarifa.getTipoTarifa().getNombre());
//        return entity;
//    }
//
//    private List<UIEntity> modelToUI(List<ClienteTarifa> clienteTarifas) {
//        List<UIEntity> clienteTarifasUI = new ArrayList<UIEntity>();
//        for (ClienteTarifa clienteTarifa : clienteTarifas) {
//            clienteTarifasUI.add(modelToUI(clienteTarifa));
//        }
//        return clienteTarifasUI;
//    }
}