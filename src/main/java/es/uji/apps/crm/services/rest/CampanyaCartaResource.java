package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaCarta;
import es.uji.apps.crm.model.TipoEstadoCampanya;
import es.uji.apps.crm.services.CampanyaCartaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("campanyacarta")
public class CampanyaCartaResource extends CoreBaseService {
    @InjectParam
    private CampanyaCartaService campanyaCartaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyaCartaByCampanyaId(@QueryParam("campanyaId") Long campanyaId) {
        List<CampanyaCarta> campanyaCarta = campanyaCartaService
                .getCampanyaCartasByCampanyaId(campanyaId);

        return modelToUI(campanyaCarta);
    }

    private List<UIEntity> modelToUI(List<CampanyaCarta> campanyaCartas) {

        List<UIEntity> entityCampanyasCarta = new ArrayList<>();

        for (CampanyaCarta campanyaCarta : campanyaCartas)
        {
            entityCampanyasCarta.add(modelToUI(campanyaCarta));
        }

        return entityCampanyasCarta;
    }

    private UIEntity modelToUI(CampanyaCarta campanyaCarta) {
        UIEntity entity = UIEntity.toUI(campanyaCarta);

        entity.put("campanyaCartaTipoNombre", campanyaCarta.getCampanyaCartaTipo().getNombre());

        return entity;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanyaCarta(UIEntity entity) {

        CampanyaCarta campanyaCarta = UIToModel(entity);
        campanyaCartaService.addCampanyaCarta(campanyaCarta);

        return modelToUI(campanyaCarta);
    }

    private CampanyaCarta UIToModel(UIEntity entity) {
        CampanyaCarta campanyaCarta = entity.toModel(CampanyaCarta.class);

        Campanya campanya = new Campanya();
        campanya.setId(Long.parseLong(entity.get("campanyaId")));
        campanyaCarta.setCampanya(campanya);

        TipoEstadoCampanya tipoEnvio = new TipoEstadoCampanya();
        tipoEnvio.setId(Long.parseLong(entity.get("campanyaCartaTipoId")));
        campanyaCarta.setCampanyaCartaTipo(tipoEnvio);

        return campanyaCarta;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanyaCarta(@PathParam("id") Long CartaId, UIEntity entity) {
        CampanyaCarta campanyaCarta = UIToModel(entity);
//        campanyaCarta.setId(CartaId);
        campanyaCartaService.updateCampanyaCarta(campanyaCarta);

        return modelToUI(campanyaCarta);
    }

    @PUT
    @Path("autoguardado")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void autoguardadoCampanyaCarta(@FormParam("cartaId") Long cartaId, @FormParam("cuerpo") String cuerpo) {
        campanyaCartaService.autoguardadoCampanyaCarta(cartaId, cuerpo);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long campanyaCartaId) {
        campanyaCartaService.delete(campanyaCartaId);
    }
}