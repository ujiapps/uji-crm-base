package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Servicio;
import es.uji.apps.crm.services.ServicioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("servicio")
public class ServicioResource extends CoreBaseService {
    @InjectParam
    private ServicioService servicioService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getServicios() {
        return UIEntity.toUI(servicioService.getServicios());
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getServicioById(@PathParam("id") Long servicioId) {
        return UIEntity.toUI(servicioService.getServicioById(servicioId));
    }

    @GET
    @Path("usuario")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getServiciosByUsuario() {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(servicioService.getServiciosByUsuario(connectedUserId));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addServicio(UIEntity entity) {
        Servicio servicio = entity.toModel(Servicio.class);
        servicioService.addServicio(servicio);
        return UIEntity.toUI(servicio);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateServicio(@PathParam("id") Long servicioId, UIEntity entity) {
        Servicio servicio = entity.toModel(Servicio.class);
        servicio.setId(servicioId);
        servicioService.updateServicio(servicio);
        return UIEntity.toUI(servicio);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long servicioId) {
        servicioService.delete(servicioId);
    }

}