package es.uji.apps.crm.services.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.ClienteTitulacionesNoUJI;
import es.uji.apps.crm.services.ClienteTitulacionesNoUJIService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("estudionouji")
public class ClienteEstudiosNoUJIResource extends CoreBaseService {

    @InjectParam
    private ClienteTitulacionesNoUJIService clienteTitulacionesNoUJIService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getClienteEstudiosNoUJIByCliente(@QueryParam("clienteId") Long clienteId) {

        List<ClienteTitulacionesNoUJI> clienteEstudios = clienteTitulacionesNoUJIService.getClienteEstudiosNoUJIByPersona(clienteId);

        return modelToUI(clienteEstudios, clienteId);
    }

    private List<UIEntity> modelToUI(List<ClienteTitulacionesNoUJI> clienteTitulacionesNoUJI, Long clienteId) {
        return clienteTitulacionesNoUJI.stream().map((ClienteTitulacionesNoUJI t) -> modelToUI(t, clienteId)).collect(Collectors.toList());
    }

    private UIEntity modelToUI(ClienteTitulacionesNoUJI clienteTitulacionesNoUJI, Long clienteId) {
        UIEntity entity = UIEntity.toUI(clienteTitulacionesNoUJI);
        entity.put("clienteId", clienteId);
        return entity;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addClienteEstudiosNoUJI(UIEntity entity) {

        return modelToUI(clienteTitulacionesNoUJIService.addClienteEstudiosNoUJI(
                ParamUtils.parseLong(entity.get("clienteId")),
                entity.get("nombre"),
                ParamUtils.parseLong(entity.get("universidadId")),
                ParamUtils.parseLong(entity.get("clasificacionId")),
                entity.get("anyoFinalizacion"),
                ParamUtils.parseLong(entity.get("tipoEstudio"))), ParamUtils.parseLong(entity.get("clienteId")));
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateClienteEstudiosNoUJI(UIEntity entity) {

        return modelToUI(clienteTitulacionesNoUJIService.updateClienteEstudiosNoUJI(
                ParamUtils.parseLong(entity.get("id")),
                ParamUtils.parseLong(entity.get("clienteId")),
                entity.get("nombre"),
                ParamUtils.parseLong(entity.get("universidadId")),
                ParamUtils.parseLong(entity.get("clasificacionId")),
                entity.get("anyoFinalizacion"),
                ParamUtils.parseLong(entity.get("tipoEstudio"))), ParamUtils.parseLong(entity.get("clienteId")));
    }

    @DELETE
    @Path("{id}")
    public void deleteClienteEstudioNoUJI(@PathParam("id") Long clienteEstudioId) {
        clienteTitulacionesNoUJIService.deleteAlumniAcademicosNoUJI(clienteEstudioId);
    }

}