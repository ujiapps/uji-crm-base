package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.EstudioUJIDAO;
import es.uji.apps.crm.model.EstudioUJI;

@Service
public class EstudioUJIService {
    private EstudioUJIDAO estudioUJIDAO;

    @Autowired
    public EstudioUJIService(EstudioUJIDAO estudioUJIDAO) {
        this.estudioUJIDAO = estudioUJIDAO;
    }

    public List<EstudioUJI> getEstudiosUJI(Long clasificacionId) {
        return estudioUJIDAO.getEstudiosUJI(clasificacionId);
    }
}