package es.uji.apps.crm.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.crm.services.rest.alumni.AlumniHashResource;
import es.uji.apps.crm.services.rest.alumni.AlumniSSOResource;
import javax.ws.rs.*;

@Path("alumni")
public class AlumniResource {

    @Path("inici")
    public AlumniInicioResource getPlatformItem(
            @InjectParam AlumniInicioResource alumniInicioResource) {
        return alumniInicioResource;
    }

    @Path("hash")
    public AlumniHashResource getPlatformItem(
            @InjectParam AlumniHashResource alumniHashResource) {
        return alumniHashResource;
    }

    @Path("sso")
    public AlumniSSOResource getPlatformItem(
            @InjectParam AlumniSSOResource alumniSSOResource) {
        return alumniSSOResource;
    }
}
