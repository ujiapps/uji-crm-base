package es.uji.apps.crm.services;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.CuentaCorportativa;
import es.uji.apps.crm.model.Persona;
import es.uji.apps.crm.ui.SuscriptoresUI;
import es.uji.commons.web.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.text.ParseException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.LinkedHashMap;

@Service
public class AlumniEspacioService {

    private final CuentasCorporativasService cuentasCorporativasService;
    private final ItemService itemService;
    private final PersonaService personaService;
    private final AlumniService alumniService;

    @Autowired
    public AlumniEspacioService(CuentasCorporativasService cuentasCorporativasService,
                                ItemService itemService,
                                PersonaService personaService,
                                AlumniService alumniService){
        this.cuentasCorporativasService = cuentasCorporativasService;
        this.itemService = itemService;
        this.personaService = personaService;
        this.alumniService = alumniService;
    }

    public Template getAlumniTemplateEspacio(Cliente cliente, String path, String idioma) throws ParseException {
        Persona persona = personaService.getPersonaByClienteId(cliente.getId());
        List<CuentaCorportativa> cuentasCorportativas = cuentasCorporativasService.getCuentasCorporativasActivasByUserId(persona.getId());

        Template template = alumniService.getTemplateBaseAlumni(path, idioma, "espacio");
        template.put("cuentasCorporativas", cuentasCorportativas);
        template.put("movil", cliente.getMovil());
        Map<String, List<SuscriptoresUI>> suscriptores = itemService.getItemsVisiblesByProgramaId(1L)
                .stream().map(item -> new SuscriptoresUI(item, idioma))
                .sorted(Comparator.comparing(SuscriptoresUI::getOrdenGrupo)
                        .thenComparing(SuscriptoresUI::getOrdenSuscriptor))
                .collect(Collectors.groupingBy(SuscriptoresUI::getGrupo, LinkedHashMap::new, Collectors.toList()));

        template.put("suscriptores", suscriptores);
        return template;
    }
}
