package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.exceptions.BusquedaVaciaException;
import es.uji.apps.crm.model.Movimiento;
import es.uji.apps.crm.services.MovimientoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("movimiento")
public class MovimientoResource extends CoreBaseService {
    @InjectParam
    private MovimientoService movimientoService;

    @GET
    @Path("cliente/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getMovimientosByClienteId(@PathParam("id") Long clienteId)
            throws BusquedaVaciaException {
        List<Movimiento> movimientos = movimientoService.getMovimientosByClienteId(clienteId);
        return modelToUI(movimientos);
    }

    private List<UIEntity> modelToUI(List<Movimiento> movimientos) {

        List<UIEntity> lista = new ArrayList<UIEntity>();

        for (Movimiento movimiento : movimientos)
        {
            UIEntity entity = modelToUI(movimiento);
            lista.add(entity);
        }

        return lista;
    }

    private UIEntity modelToUI(Movimiento movimiento) {
        UIEntity entity = UIEntity.toUI(movimiento);

        if (ParamUtils.isNotNull(movimiento.getCampanya()))
        {
            entity.put("campanyaNombre", movimiento.getCampanya().getNombre());
        }
        entity.put("tipoNombre", movimiento.getTipo().getNombre());

        return entity;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getMovimientosByClienteIdAndCampanyaId(
            @QueryParam("campanyaId") Long campanyaId, @QueryParam("clienteId") Long clienteId)
            throws BusquedaVaciaException {
        List<Movimiento> movimientos = movimientoService.getMovimientosByClienteIdAndCampanyaId(
                campanyaId, clienteId);
        return modelToUI(movimientos);
    }

}