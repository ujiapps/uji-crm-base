package es.uji.apps.crm.services;

import java.util.Date;
import java.util.List;

import es.uji.commons.rest.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.MovimientoDAO;
import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.CampanyaCliente;
import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.Movimiento;
import es.uji.apps.crm.model.Tipo;

@Service
public class MovimientoService {
    @Autowired
    private MovimientoDAO movimientoDAO;

    public Movimiento addMovimiento(CampanyaCliente campanyaCliente, String descripcion) {
        if (ParamUtils.isNotNull(campanyaCliente.getCampanyaClienteTipo())) {
            Movimiento movimiento = new Movimiento();
            movimiento.setFecha(new Date());

            Tipo tipo = new Tipo();
            tipo.setId(campanyaCliente.getCampanyaClienteTipo().getId());

            movimiento.setTipo(tipo);
            movimiento.setCliente(campanyaCliente.getCliente());
            movimiento.setDescripcion(descripcion);
            movimiento.setCampanya(campanyaCliente.getCampanya());

            return addMovimiento(movimiento);
        }
        return null;
    }

    public Movimiento addMovimiento(Movimiento movimiento) {
        movimientoDAO.insert(movimiento);
        movimiento.setId(movimiento.getId());
        return movimiento;
    }

    public Movimiento getMovimientoByClienteIdAndCampanyaIdAndTipoId(Long clienteId,
                                                                     Long campanyaId, Long campanyaEnvioTipoId) {

        return movimientoDAO.getMovimientoByClienteIdAndCampanyaIdAndTipoId(clienteId, campanyaId,
                campanyaEnvioTipoId);
    }

    public List<Movimiento> getMovimientosByClienteId(Long clienteId) {
        return movimientoDAO.getMovimientosByClienteId(clienteId);
    }

    public List<Movimiento> getMovimientosByClienteIdAndCampanyaId(Long campanyaId, Long clienteId) {
        return movimientoDAO.getMovimientoByClienteIdAndCampanyaId(campanyaId, clienteId);
    }

    public void delete(Long movimientoId) {
        movimientoDAO.delete(Movimiento.class, movimientoId);

    }

    public void deleteMovimientosByCampanyaCliente(Campanya campanya, Cliente cliente) {
        movimientoDAO.deleteMovimientosByCampanyaCliente(campanya, cliente);
    }

    public Date getFechaAlta(Long id, Long campanyaId) {
        return movimientoDAO.getFechaAlta(id, campanyaId);
    }

    public Date getFechaBaja(Long id, Long campanyaId) {
        return movimientoDAO.getFechaBaja(id, campanyaId);
    }
}