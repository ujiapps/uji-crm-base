package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Grupo;
import es.uji.apps.crm.model.Programa;
import es.uji.apps.crm.model.TipoAcceso;
import es.uji.apps.crm.services.GrupoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("grupo")
public class GrupoResource extends CoreBaseService {
    @InjectParam
    private GrupoService grupoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGrupos(@QueryParam("programaId") Long programaId, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Grupo> lista = grupoService.getGrupos(connectedUserId, programaId);
        return modelToUI(lista, idioma);
    }

    private List<UIEntity> modelToUI(List<Grupo> grupos, String idioma) {
        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (Grupo grupo : grupos)
        {
            listaUI.add(modelToUI(grupo, idioma));
        }
        return listaUI;
    }

    private UIEntity modelToUI(Grupo grupo, String idioma) {
        UIEntity entity = UIEntity.toUI(grupo);

        entity.put("visible", grupo.getVisible());
        entity.put("programaNombre", grupo.getPrograma().getNombreCa());
        entity.put("tipoAccesoNombre", grupo.getTipoAcceso().getNombre());
        entity.put("nombre", grupo.getNombre(idioma));
        return entity;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getGrupoById(@PathParam("id") Long grupoId, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        return modelToUI(grupoService.getGrupoById(grupoId), idioma);
    }

    @GET
    @Path("cliente/{clienteId}/faltan")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGruposFaltanByClienteId(@PathParam("clienteId") Long clienteId, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Grupo> lista = grupoService.getGruposFaltanByClienteId(connectedUserId, clienteId);
        return modelToUI(lista, idioma);
    }

    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGruposTodosByUsuarioConectado(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return modelToUI(grupoService.getGruposTodosByUsuarioConectado(connectedUserId), idioma);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addGrupo(UIEntity entity, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        Grupo grupo = UIToModel(entity);
        grupoService.addGrupo(grupo);
        return modelToUI(grupo, idioma);
    }

    private Grupo UIToModel(UIEntity entity) {
        Grupo grupo = entity.toModel(Grupo.class);

        TipoAcceso tipoAcceso = new TipoAcceso();
        tipoAcceso.setId(Long.parseLong(entity.get("tipoAccesoId")));
        grupo.setTipoAcceso(tipoAcceso);

        Programa programa = new Programa();
        programa.setId(Long.parseLong(entity.get("programaId")));
        grupo.setPrograma(programa);

        return grupo;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateGrupo(@PathParam("id") Long grupoId, UIEntity entity, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        Grupo grupo = UIToModel(entity);
        grupo.setId(grupoId);
        grupoService.updateGrupo(grupo);
        return modelToUI(grupo, idioma);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") Long grupoId) {
        grupoService.deleteGrupo(grupoId);
    }

}