package es.uji.apps.crm.services;

import java.util.List;

import es.uji.apps.crm.ui.OpcionUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.ItemDAO;
import es.uji.apps.crm.model.Item;

@Service
public class ItemService {
    private ItemDAO itemDAO;

    @Autowired
    public ItemService(ItemDAO itemDAO) {
        this.itemDAO = itemDAO;
    }

    public List<Item> getItemsByGrupoIdAndClienteId(Long grupoId, Long clienteId) {

        return itemDAO.getItemsByGrupoIdAndClienteId(grupoId, clienteId);
    }

    public List<Item> getItems(Long connectedUserId, Long grupoId) {
        if (grupoId != null)
        {
            return itemDAO.getItemsByGrupoId(connectedUserId, grupoId);
        }

        return itemDAO.getItems(connectedUserId);
    }

    public List<Item> getItemsVisibles(Long connectedUserId, Long grupoId) {
        if (grupoId != null)
        {
            return itemDAO.getItemsVisiblesByGrupoId(connectedUserId, grupoId);
        }

        return itemDAO.getItemsVisibles(connectedUserId);
    }

    public List<Item> getItemsByClienteIdFaltan(Long connectedUserId, Long clienteId, Long grupoId) {
        return itemDAO.getItemsByClienteIdFaltan(connectedUserId, clienteId, grupoId);
    }

    public Item getItemById(Long itemId) {
        return itemDAO.getItemById(itemId);
    }


    public List<Item> getItemsByEnvioId(Long envioId) {
        return itemDAO.getItemsByEnvioId(envioId);
    }

    public List<Item> getItemsAsociadosByCampanyaId(Long campanyaId) {
        return itemDAO.getItemsAsociadosByCampanyaId(campanyaId);
    }

    public Item addItem(Item item) {
        itemDAO.insert(item);
        item.setId(item.getId());
        return item;
    }

    public void updateItem(Item item) {
        itemDAO.update(item);
    }

    public void deleteItem(Long itemId) {
        itemDAO.delete(Item.class, itemId);
    }

    public List<Item> getItemsByGrupoId(Long grupoId) {
        return itemDAO.getItemsByGrupoId(grupoId);
    }

    /**
     * Funcion que solo devuelve los campos id y nombre del item
     *
     * @param grupoId
     * @return
     */
    public List<Item> getItemsByGrupId(Long grupoId) {
        return itemDAO.getItemsByGrupId(grupoId);
    }
    public List<OpcionUI> getItemsByGrupoOrderByIdioma(Long grupoId, String idioma) {
        return itemDAO.getItemsByGrupoOrderByIdioma(grupoId, idioma);
    }

    public List<Item> getItemsByDatoCampanya(Long connectedUserId, Long datoCampanya) {

        return itemDAO.getItemsByDatoCampanya(datoCampanya);
    }

    public List<Item> getItemsVisiblesByProgramaId(Long programaId) {
        return itemDAO.getItemsVisiblesByProgramaId(programaId);
    }

    public List<Item> getItemsVisiblesByGrupId(Long grupoId) {
        return itemDAO.getItemsVisiblesByGrupId(grupoId);
    }

    public List<Item> getItemsByEstudioId(Long estudioId) {
        return null;
    }
}