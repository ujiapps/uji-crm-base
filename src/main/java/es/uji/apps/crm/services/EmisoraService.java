package es.uji.apps.crm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.crm.dao.EmisoraDAO;
import es.uji.apps.crm.model.Emisora;

@Service
public class EmisoraService {

    private EmisoraDAO emisoraDAO;

    @Autowired
    public EmisoraService(EmisoraDAO emisoraDAO) {
        this.emisoraDAO = emisoraDAO;
    }

    public List<Emisora> getEmisoras() {
        return emisoraDAO.getEmisoras();
    }

}