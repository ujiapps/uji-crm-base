package es.uji.apps.crm.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.ClasificacionEstudio;
import es.uji.apps.crm.services.ClasificacionEstudioUJIService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("clasificacionestudio")
public class ClasificacionEstudioUJIResource extends CoreBaseService {
    @InjectParam
    private ClasificacionEstudioUJIService clasificacionEstudioUJIService;

    @Path("{clasificacionId}/estudioUJI")
    public EstudioUJIResource getPlatformItem(
            @InjectParam EstudioUJIResource estudioUJIResource) {
        return estudioUJIResource;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getClasificacionesEstudios(@QueryParam("sort") String ordenacion) throws JSONException {
        String columnaOrden;
        String tipoOrden;

        if (ParamUtils.isNotNull(ordenacion))
        {
            JSONArray array = new JSONArray(ordenacion);
            JSONObject obj = array.getJSONObject(0);

            columnaOrden = (String) obj.get("property");
            tipoOrden = (String) obj.get("direction");
        }
        else
        {
            columnaOrden = "nombre";
            tipoOrden = "ASC";
        }

        List<ClasificacionEstudio> lista = clasificacionEstudioUJIService.getClasificacionesEstudios(columnaOrden, tipoOrden);
        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setData(UIEntity.toUI(lista));
        return responseMessage;
    }

//    @Path("grado")
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public ResponseMessage getClasificacionEstudiosGrado(@QueryParam("start") @DefaultValue("0") Long start,
//                                                         @QueryParam("limit") @DefaultValue("25") Long limit,
//                                                         @QueryParam("sort") String ordenacion) throws JSONException
//    {
//        Paginacion paginacion = new Paginacion(start, limit);
//        String columnaOrden;
//        String tipoOrden;
//
//        if (ParamUtils.isNotNull(ordenacion))
//        {
//            JSONArray array = new JSONArray(ordenacion);
//            JSONObject obj = array.getJSONObject(0);
//
//            columnaOrden = (String) obj.get("property");
//            tipoOrden = (String) obj.get("direction");
//        }
//        else
//        {
//            columnaOrden = "nombre";
//            tipoOrden = "ASC";
//        }
//
//        List<ClasificacionEstudio> lista = clasificacionEstudioUJIService.getClasificacionEstudios("G", paginacion, columnaOrden, tipoOrden);
//        ResponseMessage responseMessage = new ResponseMessage(true);
//        responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
//        responseMessage.setData(UIEntity.toUI(lista));
//        return responseMessage;
//    }
//
//    @Path("postgrado")
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public ResponseMessage getClasificacionEstudiosPostGrado(@QueryParam("start") @DefaultValue("0") Long start,
//                                                             @QueryParam("limit") @DefaultValue("25") Long limit,
//                                                             @QueryParam("sort") String columnaOrden,
//                                                             @QueryParam("dir") String tipoOrden)
//    {
//        Paginacion paginacion = new Paginacion(start, limit);
//        List<ClasificacionEstudio> lista = clasificacionEstudioUJIService.getClasificacionEstudios("M", paginacion, columnaOrden, tipoOrden);
//        ResponseMessage responseMessage = new ResponseMessage(true);
//        responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
//        responseMessage.setData(UIEntity.toUI(lista));
//        return responseMessage;
//    }
//
//    @Path("doctorado")
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public ResponseMessage getClasificacionEstudiosDoctorado(@QueryParam("start") @DefaultValue("0") Long start,
//                                                             @QueryParam("limit") @DefaultValue("25") Long limit,
//                                                             @QueryParam("sort") String columnaOrden,
//                                                             @QueryParam("dir") String tipoOrden)
//    {
//        Paginacion paginacion = new Paginacion(start, limit);
//        List<ClasificacionEstudio> lista = clasificacionEstudioUJIService.getClasificacionEstudios("D", paginacion, columnaOrden, tipoOrden);
//        ResponseMessage responseMessage = new ResponseMessage(true);
//        responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
//        responseMessage.setData(UIEntity.toUI(lista));
//        return responseMessage;
//    }
}