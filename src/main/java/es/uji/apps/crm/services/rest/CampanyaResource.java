package es.uji.apps.crm.services.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Campanya;
import es.uji.apps.crm.model.Programa;
import es.uji.apps.crm.services.CampanyaService;
import es.uji.apps.crm.services.ProgramaService;
import es.uji.apps.crm.services.SubVinculoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("campanya")
public class CampanyaResource extends CoreBaseService {
    @InjectParam
    private CampanyaService campanyaService;
    @InjectParam
    private ProgramaService programaService;
    @InjectParam
    private SubVinculoService subVinculoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyas() {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Campanya> lista = campanyaService.getCampanyasAdmin(connectedUserId);
        return modelToUI(lista);
    }

    private List<UIEntity> modelToUI(List<Campanya> campanyas) {
        List<UIEntity> listaUI = new ArrayList<>();

        for (Campanya campanya : campanyas)
        {
            listaUI.add(modelToUI(campanya));
        }
        return listaUI;
    }

    private UIEntity modelToUI(Campanya campanya) {
        UIEntity entity = UIEntity.toUI(campanya);

        entity.put("programaNombre", campanya.getPrograma().getNombreCa());

//        Campanya campanyaRelacionada = campanya.getCampanya();
//        if (ParamUtils.isNotNull(campanyaRelacionada)) {
//            entity.put("campanyaNombre", campanyaRelacionada.getNombre());
//        }
//
//        Long subVinculo = campanya.getSubVinculo();
//        if (subVinculo != null && subVinculo != 0) {
//
//            entity.put("subVinculoNombre",
//                    subVinculoService.getSubVinculoNombreById(campanya.getSubVinculo()));
//        }
        entity.put("fideliza", campanya.getFideliza());
//        entity.put("autenticaFormulario", campanya.getAutenticaFormulario());


        return entity;
    }

    @GET
    @Path("user")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyasUser() {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Campanya> lista = campanyaService.getCampanyasUser(connectedUserId);
        return modelToUI(lista);
    }

    @GET
    @Path("user/tarifas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyasUserConTarifa() {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Campanya> lista = campanyaService.getCampanyasUserConTarifa(connectedUserId);
        return modelToUI(lista);
    }

    @GET
    @Path("servicio/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyasByServicio(@PathParam("id") Long servicioId) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Campanya> lista = campanyaService.getCampanyasByServicio(connectedUserId, servicioId);
        return modelToUI(lista);
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getCampanyaById(@PathParam("id") Long campanyaId) {
        Campanya campanya = campanyaService.getCampanyaById(campanyaId);
        return modelToUI(campanya);
    }

    @GET
    @Path("cliente/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyaByClienteId(@PathParam("id") Long clienteId) {
        List<Campanya> campanyas = campanyaService.getCampanyasByClienteId(clienteId);
        return modelToUI(campanyas);
    }

    @GET
    @Path("user/tree/root/admin/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyasTreeAdmin() {
        return getCampanyasTreeAdmin(0L);
    }

    @GET
    @Path("user/tree/root/admin/{node}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyasTreeAdmin(@PathParam("node") @DefaultValue("0") Long node) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Campanya> lista = campanyaService.getCampanyasAdminTree(node, connectedUserId);
        return lista.stream().map(a -> {
            UIEntity ui = UIEntity.toUI(a);
            ui.put("text", a.getNombre());
            ui.put("programa", a.getPrograma().getNombreCa());
            ui.put("descripcion", a.getDescripcion());
            ui.put("leaf", !campanyaService.tieneHijos(a.getId()));

            if (ParamUtils.isNotNull(a.getCampanya()))
            {
                ui.put("parentId", a.getCampanya().getId());
            }
            else
            {
                ui.put("parentId", 0);
            }
            return ui;
        }).collect(Collectors.toList());
    }

    @GET
    @Path("user/tree/root/{node}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCampanyasTree(@PathParam("node") @DefaultValue("0") Long node) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Campanya> lista = campanyaService.getCampanyasTree(node, connectedUserId);
        return lista.stream().map(a -> {
            UIEntity ui = UIEntity.toUI(a);
            ui.put("text", a.getNombre());
            ui.put("leaf", !campanyaService.tieneHijos(a.getId()));

            if (ParamUtils.isNotNull(a.getCampanya()))
            {
                ui.put("parentId", a.getCampanya().getId());
            }
            else
            {
                ui.put("parentId", 0);
            }
            return ui;
        }).collect(Collectors.toList());
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addCampanya(UIEntity entity) {
        Campanya campanya = UIToModel(entity);
        campanyaService.addCampanya(campanya);
        return modelToUI(campanya);
    }

    private Campanya UIToModel(UIEntity entity) {
        Campanya campanya = entity.toModel(Campanya.class);

        String campanyaId = entity.get("campanyaId");
        if (campanyaId != null)
        {
            Campanya campanyaPadre = new Campanya();
            campanyaPadre.setId(Long.parseLong(campanyaId));
            campanya.setCampanya(campanyaPadre);
        }

        Programa programa = new Programa();
        programa.setId(Long.parseLong(entity.get("programaId")));
        campanya.setPrograma(programa);

        if (ParamUtils.isNotNull(entity.get("fechaInicioActiva")))
        {
            campanya.setFechaInicioActiva(entity.getDate("fechaInicioActiva"));
        }

        if (ParamUtils.isNotNull(entity.get("fechaFinActiva")))
        {
            campanya.setFechaFinActiva(entity.getDate("fechaFinActiva"));
        }
        return campanya;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("generica/acto")
    public UIEntity addCampanyaGenericaActo() {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(campanyaService.addCampanyaGenericaActo(connectedUserId));

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("generica/actoconpago")
    public UIEntity addCampanyaGenericaActoConPago() {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(campanyaService.addCampanyaGenericaActoConPago(connectedUserId));

    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateCampanya(@PathParam("id") Long campanyaId, UIEntity entity) {
        Campanya campanya = UIToModel(entity);
        campanya.setId(campanyaId);
        campanyaService.updateCampanya(campanya);
        return modelToUI(campanya);
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteCampanya(@PathParam("id") Long campanyaId) {
        campanyaService.deleteCampanya(campanyaId);
    }

    @Path("{campanyaId}/grupo")
    public CampanyaGrupoSubResource getCampanyaGrupoSubResource(
            @InjectParam CampanyaGrupoSubResource campanyaGrupoSubResource) {
        return campanyaGrupoSubResource;
    }

    @Path("{campanyaId}/item")
    public CampanyaItemSubResource getCampanyaItemSubResource(
            @InjectParam CampanyaItemSubResource campanyaItemSubResource) {
        return campanyaItemSubResource;
    }

}