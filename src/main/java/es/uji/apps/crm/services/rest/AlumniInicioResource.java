package es.uji.apps.crm.services.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.crm.model.Tipo;
import es.uji.apps.crm.model.domains.TipoColumna;
import es.uji.apps.crm.services.AlumniInicioService;
import es.uji.apps.crm.services.CampanyaTextoService;
import es.uji.apps.crm.services.TipoService;
import es.uji.apps.crm.utils.AppInfo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.web.template.Template;
import org.springframework.beans.factory.annotation.Value;

public class AlumniInicioResource extends CoreBaseService {

    private static final int DURACION_UN_ANYO = 31536000;
    @Context
    protected HttpServletResponse response;
    @InjectParam
    private CampanyaTextoService campanyaTextoService;
    @InjectParam
    private AlumniInicioService alumniInicioService;
    @InjectParam
    private TipoService tipoService;

//    @Value("${uji.crm.domain}")
    private String domain = "uji.es";

    private final String plantilla = "crm/alumni/";
    private final String plantillaBase = plantilla + "base";

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template getInicio(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @QueryParam("error_hash") @DefaultValue("0") Integer validHash)
            throws ParseException {
        Template template = AppInfo.buildPagina(plantilla + "base", idioma);
        template.put("contenido", plantilla + "login");
        template.put("validHash", validHash);
        template.put("tratamientoRPGD", campanyaTextoService.getCampanyaTextoByCampanyaCodigo("SAUJI").getTextoByIdioma(idioma));
        return template;
    }

    @GET
    @Path("avisook")
    @Produces(MediaType.TEXT_HTML)
    public Template getAvisoOk(@CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException {
        Template template = AppInfo.buildPagina(plantillaBase, idioma);
        template.put("contenido", plantilla + "avisook");
        return template;
    }

    @GET
    @Path("avisoko")
    @Produces(MediaType.TEXT_HTML)
    public Template getAvisoKo(@CookieParam("uji-lang") @DefaultValue("ca") String idioma)
            throws ParseException {
        Template template = AppInfo.buildPagina(plantillaBase, idioma);
        template.put("contenido", plantilla + "avisoko");
        return template;
    }

    @GET
    @Path("hash/{p_hash}")
    @Produces(MediaType.TEXT_HTML)
    public Response getCookieParam(@PathParam("p_hash") String pHash) throws URISyntaxException {
        return alumniInicioService.activarSesionPorHash(pHash, "alumni", Boolean.FALSE);
    }

    @POST
    @Path("hash")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage addSessionUser(@FormParam("correo") String correo, @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {

        //Dar de alta un hash en crm_session y Enviar correo hash
        return alumniInicioService.anyadirSesionUsuario(correo, idioma, request);

    }

    @GET
    @Path("formulario")
    @Produces(MediaType.TEXT_HTML)
    public Template getFormulario(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException {
        Template template = AppInfo.buildPagina(plantillaBase , idioma);
        template.put("contenido", plantilla + "formulario");
        List<Tipo> tiposIdentificacion = tipoService.getTipos(TipoColumna.IDENTIFICACION.getValue());
        template.put("tiposIdentificacion", tiposIdentificacion);
        return template;
    }


    @POST
    @Path("formulario")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage enviarFormulario(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                     @FormParam("tipoIdentificacion") Long tipoIdentificacionId,
                                     @FormParam("identificacion") String identificacion,
                                     @FormParam("nombre") String nombre,
                                     @FormParam("apellidos") String apellidos,
                                     @FormParam("correo") String correo,
                                     @FormParam("fechaNacimiento") String fechaNacimiento,
                                     @FormParam("telefono") String telefono,
                                     @FormParam("nacionalidad") String nacionalidad,
                                     @FormParam("categoria") Long categoria) {

        return alumniInicioService.enviarFormularioContacto(tipoIdentificacionId, identificacion, nombre, apellidos, correo, fechaNacimiento, telefono, categoria, nacionalidad);

    }

    @GET
    @Path("idioma/{idioma}")
    public Response cambiaCookieIdioma(@PathParam("idioma") String idioma, @Context HttpServletRequest request)
            throws URISyntaxException {

        Cookie cookie = new Cookie("uji-lang", idioma.toLowerCase());
        cookie.setPath("/");
        cookie.setDomain(domain);
        cookie.setMaxAge(DURACION_UN_ANYO);
        response.addCookie(cookie);
        return Response.temporaryRedirect(new URI(request.getHeader("Referer"))).build();


    }


}