package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CRM_EXT_PROVINCIAS")
public class Provincia implements Serializable {
    @Id
    private Long id;

    @Column(name = "NOMBRE_CA")
    private String nombreCA;

    @Column(name = "NOMBRE_CA_LIMPIO")
    private String nombreCALimpio;

    @Column(name = "NOMBRE_ES")
    private String nombreES;

    @Column(name = "NOMBRE_ES_LIMPIO")
    private String nombreESLimpio;

    @Column(name = "NOMBRE_EN")
    private String nombreEN;

    @Column(name = "NOMBRE_EN_LIMPIO")
    private String nombreENLimpio;

    private Integer orden;

    @OneToMany(mappedBy = "provincia")
    private Set<Poblacion> poblaciones;

    public Provincia(Long provinciaPostal) {
        this.id = provinciaPostal;
    }

    public Provincia() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreCA() {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA) {
        this.nombreCA = nombreCA;
    }

    public String getNombreES() {
        return nombreES;
    }

    public void setNombreES(String nombreES) {
        this.nombreES = nombreES;
    }

    public String getNombreEN() {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN) {
        this.nombreEN = nombreEN;
    }

    public Set<Poblacion> getPoblaciones() {
        return poblaciones;
    }

    public void setPoblaciones(Set<Poblacion> poblaciones) {
        this.poblaciones = poblaciones;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }
}