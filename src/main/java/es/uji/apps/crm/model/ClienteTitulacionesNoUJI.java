package es.uji.apps.crm.model;

import es.uji.commons.rest.ParamUtils;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CRM_CLIENTES_EST_NO_UJI")
public class ClienteTitulacionesNoUJI implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "TIPO_ESTUDIO_ID")
    private Long tipoEstudio;

    @Column(name = "ANYO_FINALIZACION")
    private String anyoFinalizacion;

    @Column(name = "NOMBRE")
    private String nombre;

    @ManyToOne
    @JoinColumn(name = "CLASIFICACION_ID")
    private TipoEstudio clasificacion;

    @ManyToOne
    @JoinColumn(name = "UNIVERSIDAD_ID")
    private Universidad universidad;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CLIENTE_ID")
    private Cliente cliente;

    @ManyToOne
    @JoinColumn(name = "CLIENTE_DATO_ID")
    private ClienteDato clienteDatoEstudio;

    public ClienteTitulacionesNoUJI() {
    }

    public ClienteTitulacionesNoUJI(Cliente cliente, Long clasificacionId, String anyoEstudio, Long universidadId, String textoEstudio, Long tipoEstudioId, ClienteDato clienteDatoEstudio) {
        this.cliente = cliente;
        this.nombre = textoEstudio;
        this.anyoFinalizacion = anyoEstudio;
        this.tipoEstudio = tipoEstudioId;
        if (ParamUtils.isNotNull(clasificacionId)) {
            this.clasificacion = new TipoEstudio(clasificacionId);
        }
        if (ParamUtils.isNotNull(universidadId)) {
            this.universidad = new Universidad(universidadId);
        }
        if (ParamUtils.isNotNull(clienteDatoEstudio)) {
            this.clienteDatoEstudio = clienteDatoEstudio;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTipoEstudio() {
        return tipoEstudio;
    }

    public void setTipoEstudio(Long tipoEstudio) {
        this.tipoEstudio = tipoEstudio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoEstudio getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(TipoEstudio clasificacion) {
        this.clasificacion = clasificacion;
    }

    public Universidad getUniversidad() {
        return universidad;
    }

    public void setUniversidad(Universidad universidad) {
        this.universidad = universidad;
    }

    public String getAnyoFinalizacion() {
        return anyoFinalizacion;
    }

    public void setAnyoFinalizacion(String anyoFinalizacion) {
        this.anyoFinalizacion = anyoFinalizacion;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ClienteDato getClienteDatoEstudio() {
        return clienteDatoEstudio;
    }

    public void setClienteDatoEstudio(ClienteDato clienteDatoEstudio) {
        this.clienteDatoEstudio = clienteDatoEstudio;
    }
}