package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import es.uji.commons.rest.annotations.DataTag;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_TARIFAS_TIPOS")
public class TipoTarifa implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @DataTag
    private String nombre;
    //    @Column(name = "DIAS_CADUCA")
//    private Long diasCaducidad;
    @Column(name = "MESES_CADUCA")
    private Long mesesCaduca;
    //    @Column(name = "DIAS_VIGENCIA")
//    private Long diasVigencia;
    @Column(name = "MESES_VIGENCIA")
    private Long mesesVigencia;
    @Column(name = "defecto")
    private boolean defecto;

    @ManyToOne
    @JoinColumn(name = "LINEA_FACTURACION_ID")
    private LineaFacturacion lineaFacturacion;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private Campanya campanya;

    @OneToMany(mappedBy = "tipoTarifa")
    private Set<ClienteTarifa> clientesTarifa;

    @OneToMany(mappedBy = "tipoTarifa")
    private Set<TipoTarifaEnvio> tipoTarifaEnvios;

    public TipoTarifa() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

//    public Long getDiasCaducidad() {
//        return diasCaducidad;
//    }
//
//    public void setDiasCaducidad(Long diasCaducidad) {
//        this.diasCaducidad = diasCaducidad;
//    }
//
//    public Long getDiasVigencia() {
//        return diasVigencia;
//    }
//
//    public void setDiasVigencia(Long diasVigencia) {
//        this.diasVigencia = diasVigencia;
//    }

    public Campanya getCampanya() {
        return campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

    public Set<ClienteTarifa> getClientesTarifa() {
        return clientesTarifa;
    }

    public void setClientesTarifa(Set<ClienteTarifa> clientesTarifa) {
        this.clientesTarifa = clientesTarifa;
    }

    public boolean getDefecto() {
        return defecto;
    }

    public Set<TipoTarifaEnvio> getTipoTarifaEnvios() {
        return tipoTarifaEnvios;
    }

    public void setTipoTarifaEnvios(Set<TipoTarifaEnvio> tipoTarifaEnvios) {
        this.tipoTarifaEnvios = tipoTarifaEnvios;
    }

    public boolean isDefecto() {
        return defecto;
    }

    public void setDefecto(boolean defecto) {
        this.defecto = defecto;
    }

    public LineaFacturacion getLineaFacturacion() {
        return lineaFacturacion;
    }

    public void setLineaFacturacion(LineaFacturacion lineaFacturacion) {
        this.lineaFacturacion = lineaFacturacion;
    }

    public Long getMesesVigencia() {
        return mesesVigencia;
    }

    public void setMesesVigencia(Long mesesVigencia) {
        this.mesesVigencia = mesesVigencia;
    }

    public Long getMesesCaduca() {
        return mesesCaduca;
    }

    public void setMesesCaduca(Long mesesCaduca) {
        this.mesesCaduca = mesesCaduca;
    }
}