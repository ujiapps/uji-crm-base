package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_PROGRAMAS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_LINEAS_FACTURACION")

public class LineaFacturacion implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "DESCRIPCION")
    private String descripcion;

    // bi-directional many-to-one association to Servicio
    @ManyToOne
    @JoinColumn(name = "servicio_id")
    private Servicio servicio;

    @ManyToOne
    @JoinColumn(name = "emisora_id")
    private Emisora emisora;

    @ManyToOne
    @JoinColumn(name = "formato_id")
    private ReciboFormato reciboFormato;

    @Column(name = "TIPO_RECIBO")
    private String tipoRecibo;

    @OneToMany(mappedBy = "lineaFacturacion")
    private Set<Campanya> campanyas;

    @OneToMany(mappedBy = "lineaFacturacion")
    private Set<TipoTarifa> tiposTarifa;

    public LineaFacturacion() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public Emisora getEmisora() {
        return emisora;
    }

    public void setEmisora(Emisora emisora) {
        this.emisora = emisora;
    }

    public Set<Campanya> getCampanyas() {
        return campanyas;
    }

    public void setCampanyas(Set<Campanya> campanyas) {
        this.campanyas = campanyas;
    }

    public ReciboFormato getReciboFormato() {
        return reciboFormato;
    }

    public void setReciboFormato(ReciboFormato reciboFormato) {
        this.reciboFormato = reciboFormato;
    }

    public String getTipoRecibo() {
        return tipoRecibo;
    }

    public void setTipoRecibo(String tipoRecibo) {
        this.tipoRecibo = tipoRecibo;
    }

    public Set<TipoTarifa> getTiposTarifa() {
        return tiposTarifa;
    }

    public void setTiposTarifa(Set<TipoTarifa> tiposTarifa) {
        this.tiposTarifa = tiposTarifa;
    }
}