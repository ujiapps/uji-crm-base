package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "CRM_VW_CLIENTES_ESTUDIOS_UJI")
public class ClienteTitulacionesUJI implements Serializable {
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CLIENTE_ID")
    private Cliente cliente;

    @Id
    @ManyToOne
    @JoinColumn(name = "EST_ID")
    private ClasificacionEstudio clasificacionEstudio;

    @Column(name = "TIPO")
    private String tipoEstudio;

    @Column(name = "CURSO_ACA_FIN")
    private String anyoFinalizacion;

    @Column(name = "ESTUDIO_NOMBRE")
    private String nombre;

    @Column(name = "TIPO_BECA")
    private String tipoBeca;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_TITULO")
    private Date fechaTitulo;

    public ClienteTitulacionesUJI() {
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ClasificacionEstudio getClasificacionEstudio() {
        return clasificacionEstudio;
    }

    public void setClasificacionEstudio(ClasificacionEstudio clasificacionEstudio) {
        this.clasificacionEstudio = clasificacionEstudio;
    }

    public String getTipoEstudio() {
        return tipoEstudio;
    }

    public void setTipoEstudio(String tipoEstudio) {
        this.tipoEstudio = tipoEstudio;
    }

    public String getAnyoFinalizacion() {
        return anyoFinalizacion;
    }

    public void setAnyoFinalizacion(String anyoFinalizacion) {
        this.anyoFinalizacion = anyoFinalizacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoBeca() {
        return tipoBeca;
    }

    public void setTipoBeca(String tipoBeca) {
        this.tipoBeca = tipoBeca;
    }

    public Date getFechaTitulo() {
        return fechaTitulo;
    }

    public void setFechaTitulo(Date fechaTitulo) {
        this.fechaTitulo = fechaTitulo;
    }
}