package es.uji.apps.crm.model.domains;

public enum TipoClienteDato {
    IBAN(17L, "IBAN"),
    WEB(25L, "WEB"),
    CAMPOLIBRE(3890136L, "CAMPOLIBRE"),
    ANYOFINALIZACIONESTUDIOS(19L, "ANYOFINALIZACIONESTUDIOS"),
    NUMENTRADAS(2494144L, "NUMENTRADAS");

    private final Long id;
    private final String nombre;

    TipoClienteDato(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

}