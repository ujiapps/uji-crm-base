package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CAMPANYAS database table.
 */

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_PROGRAMAS_CURSOS")
public class ProgramaCurso implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "uest_id")
    private Ubicacion uest;

    @ManyToOne
    @JoinColumn(name = "PROGRAMA_ID")
    private Programa programa;

    public ProgramaCurso() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public Ubicacion getUest() {
        return uest;
    }

    public void setUest(Ubicacion uest) {
        this.uest = uest;
    }

}