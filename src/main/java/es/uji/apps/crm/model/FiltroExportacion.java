package es.uji.apps.crm.model;

public class FiltroExportacion {

    private Long operador;
    private FiltroFechas fecha;


    public FiltroExportacion() {
    }

    public Long getOperador() {
        return operador;
    }

    public void setOperador(Long operador) {
        this.operador = operador;
    }

    public FiltroFechas getFecha() {
        return fecha;
    }

    public void setFecha(FiltroFechas fecha) {
        this.fecha = fecha;
    }
}