package es.uji.apps.crm.model;

import es.uji.apps.crm.model.domains.TipoOperador;

public class FiltroCampanya {

    private TipoOperador operador;
    private Long campanya;
    private Long campanyaTipoEstado;


    public FiltroCampanya() {
    }

    public TipoOperador getOperador() {
        return operador;
    }

    public void setOperador(Long operador) {

        if (operador.equals(1L))
        {
            this.operador = TipoOperador.Y;
        }
        if (operador.equals(2L))
        {
            this.operador = TipoOperador.O;
        }

        if (operador.equals(3L))
        {
            this.operador = TipoOperador.YNO;
        }
        if (operador.equals(4L))
        {
            this.operador = TipoOperador.ONO;
        }
    }

    public Long getCampanya() {
        return campanya;
    }

    public void setCampanya(Long campanya) {
        this.campanya = campanya;
    }

    public Long getCampanyaTipoEstado() {
        return campanyaTipoEstado;
    }

    public void setCampanyaTipoEstado(Long campanyaTipoEstado) {
        this.campanyaTipoEstado = campanyaTipoEstado;
    }

}