package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;


@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_RGPD_ACEPTA_TRATAMIENTO")

public class AceptaTratamiento implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CODIGO_TRATAMIENTO")
    private String codigoTratamiento;
    @ManyToOne
    @JoinColumn(name = "CLIENTE_GENERAL_ID")
    private ClienteGeneral clienteGeneral;
    private Date fecha;

    public AceptaTratamiento() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigoTratamiento() {
        return codigoTratamiento;
    }

    public void setCodigoTratamiento(String codigoTratamiento) {
        this.codigoTratamiento = codigoTratamiento;
    }

    public ClienteGeneral getClienteGeneral() {
        return clienteGeneral;
    }

    public void setClienteGeneral(ClienteGeneral clienteGeneral) {
        this.clienteGeneral = clienteGeneral;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}