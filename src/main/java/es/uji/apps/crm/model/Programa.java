package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import es.uji.commons.rest.ParamUtils;

/**
 * The persistent class for the CRM_PROGRAMAS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_PROGRAMAS")

public class Programa implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NOMBRE_CA")
    private String nombreCa;

    @Column(name = "NOMBRE_ES")
    private String nombreEs;

    @Column(name = "NOMBRE_UK")
    private String nombreUk;

    private Integer orden;

    private Boolean visible;

    @Column(name = "PLANTILLA_WEB")
    private String plantilla;

    @Column(name = "vinculo_id")
    private Long vinculoId;

    // bi-directional many-to-one association to ProgramaPerfil
    @OneToMany(mappedBy = "programa")
    private Set<ProgramaPerfil> programasPerfiles;

    // bi-directional many-to-one association to Campanya
    @OneToMany(mappedBy = "programa")
    private Set<Campanya> campanyas;

    @OneToMany(mappedBy = "programa")
    private Set<Envio> envios;

    // bi-directional many-to-one association to Grupo
    @OneToMany(mappedBy = "programa")
    private Set<Grupo> grupos;

    // bi-directional many-to-one association to Servicio
    @ManyToOne
    private Servicio servicio;

    // bi-directional many-to-one association to TipoAcceso
    @ManyToOne
    @JoinColumn(name = "TIPO_ACCESO_ID")
    private TipoAcceso tipoAcceso;

    // bi-directional many-to-one association to ProgramaUsuario
    @OneToMany(mappedBy = "programa")
    private Set<ProgramaUsuario> programasUsuarios;

    // bi-directional many-to-one association to Item
    @OneToMany(mappedBy = "programa")
    private Set<PersonaPrograma> personaProgramas;

    public Programa() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOrden() {
        return this.orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public TipoAcceso getTipoAcceso() {
        return this.tipoAcceso;
    }

    public void setTipoAcceso(TipoAcceso tipoAcceso) {
        this.tipoAcceso = tipoAcceso;
    }

    public Boolean getVisible() {
        return this.visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Set<ProgramaPerfil> getProgramasPerfiles() {
        return this.programasPerfiles;
    }

    public void setProgramasPerfiles(Set<ProgramaPerfil> programasPerfiles) {
        this.programasPerfiles = programasPerfiles;
    }

    public Set<Campanya> getCampanyas() {
        return this.campanyas;
    }

    public void setCampanyas(Set<Campanya> campanyas) {
        this.campanyas = campanyas;
    }

    public Set<Grupo> getGrupos() {
        return this.grupos;
    }

    public void setGrupos(Set<Grupo> grupos) {
        this.grupos = grupos;
    }

    public Servicio getServicio() {
        return this.servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public Set<ProgramaUsuario> getProgramasUsuarios() {
        return this.programasUsuarios;
    }

    public void setProgramasUsuarios(Set<ProgramaUsuario> programasUsuarios) {
        this.programasUsuarios = programasUsuarios;
    }

    public Set<PersonaPrograma> getPersonaProgramas() {
        return personaProgramas;
    }

    public void setPersonaProgramas(Set<PersonaPrograma> personaProgramas) {
        this.personaProgramas = personaProgramas;
    }

    public String getNombre(String idioma) {
        if (idioma == null)
        {
            idioma = "CA";
        }
        switch (idioma.toUpperCase())
        {
            case "ES":
                if (ParamUtils.isNotNull(getNombreEs()))
                {
                    return getNombreEs();
                }
                else
                {
                    return getNombreCa();
                }
            case "UK":
            case "EN":
                if (ParamUtils.isNotNull(getNombreUk()))
                {
                    return getNombreUk();
                }
                else
                {
                    return getNombreCa();
                }
            default:
                return getNombreCa();
        }
    }

    public String getNombreCa() {
        return this.nombreCa;
    }

    public void setNombreCa(String nombreCa) {
        this.nombreCa = nombreCa;
    }

    public String getNombreEs() {
        return this.nombreEs;
    }

    public void setNombreEs(String nombreEs) {
        this.nombreEs = nombreEs;
    }

    public String getNombreUk() {
        return this.nombreUk;
    }

    public void setNombreUk(String nombreUk) {
        this.nombreUk = nombreUk;
    }

    public Long getVinculoId() {
        return vinculoId;
    }

    public void setVinculoId(Long vinculoId) {
        this.vinculoId = vinculoId;
    }

    public Set<Envio> getEnvios() {
        return envios;
    }

    public void setEnvios(Set<Envio> envios) {
        this.envios = envios;
    }

    public String getPlantilla() {
        return plantilla;
    }

    public void setPlantilla(String plantilla) {
        this.plantilla = plantilla;
    }
}