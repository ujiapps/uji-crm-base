package es.uji.apps.crm.model;

import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * The persistent class for the CRM_EXT_PERSONAS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_EXT_PERSONAS_SIMPLE")
public class PersonaSimple implements Serializable {
    @Id
    private Long id;

    private String identificacion;

    private String nombre;

    private String apellido1;

    private String apellido2;

    public PersonaSimple() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidosNombre() {
        return this.apellido1 + ((ParamUtils.isNotNull(this.apellido2)) ? " " + this.apellido2 : "") + ", " + this.nombre;
    }
}