package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_FASES database table.
 */

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_EXT_UBIC_ESTRUCTURALES")
public class Ubicacion implements Serializable {
    @Id
    private Long id;
    private String nombre;

    @OneToMany(mappedBy = "uest")
    private Set<ProgramaPersonaCurso> ubicaciones;

    @OneToMany(mappedBy = "tipo")
    private Set<CursoInscripcion> tipos;

    @OneToMany(mappedBy = "uest")
    private Set<ProgramaCurso> programaCursos;

    public Ubicacion() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }
}