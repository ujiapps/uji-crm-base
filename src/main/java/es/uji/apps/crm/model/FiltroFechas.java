package es.uji.apps.crm.model;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class FiltroFechas {
    private Long tipoComparacion;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaMax;

    private Long tipoFecha;
    private Long mes;
    private Long mesMax;
    private Long anyo;
    private Long anyoMax;
    private Long comportamiento;
    private Long operador;


    public FiltroFechas() {
    }

    public Long getTipoComparacion() {
        return tipoComparacion;
    }

    public void setTipoComparacion(Long tipoComparacion) {
        this.tipoComparacion = tipoComparacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaMax() {
        return fechaMax;
    }

    public void setFechaMax(Date fechaMax) {
        this.fechaMax = fechaMax;
    }

    public Long getTipoFecha() {
        return tipoFecha;
    }

    public void setTipoFecha(Long tipoFecha) {
        this.tipoFecha = tipoFecha;
    }

    public Long getMes() {
        return mes;
    }

    public void setMes(Long mes) {
        this.mes = mes;
    }

    public Long getAnyo() {
        return anyo;
    }

    public void setAnyo(Long anyo) {
        this.anyo = anyo;
    }

    public Long getMesMax() {
        return mesMax;
    }

    public void setMesMax(Long mesMax) {
        this.mesMax = mesMax;
    }

    public Long getAnyoMax() {
        return anyoMax;
    }

    public void setAnyoMax(Long anyoMax) {
        this.anyoMax = anyoMax;
    }

    public Long getComportamiento() {
        return comportamiento;
    }

    public void setComportamiento(Long comportamiento) {
        this.comportamiento = comportamiento;
    }

    public Long getOperador() {
        return operador;
    }

    public void setOperador(Long operador) {
        this.operador = operador;
    }
}