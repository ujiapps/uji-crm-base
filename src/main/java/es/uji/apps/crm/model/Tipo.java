package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "crm_vw_tipos")
public class Tipo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    @Column(name = "TABLA_COLUMNA")
    private String tablaColumna;

    // bi-directional many-to-one association to Clientes
    @OneToMany(mappedBy = "correoEstado")
    private Set<Cliente> correoEstado;

    // bi-directional many-to-one association to Clientes
    @OneToMany(mappedBy = "movilEstado")
    private Set<Cliente> movilEstado;

    // bi-directional many-to-one association to Clientes
    @OneToMany(mappedBy = "postalEstado")
    private Set<Cliente> postalEstado;

    @OneToMany(mappedBy = "tipoIdentificacion")
    private Set<ClienteGeneral> tipoIdentificacion;

    @OneToMany(mappedBy = "campanyaClienteEstado")
    private Set<CampanyaClienteAnyadirAuto> campanyaClienteEstado;

    // bi-directional many-to-one association to clientesDatos
    @OneToMany(mappedBy = "clienteDatoTipoAcceso")
    private Set<ClienteDato> clienteDatoTipoAcceso;

    // bi-directional many-to-one association to clientesDatos
    @OneToMany(mappedBy = "clienteDatoTipoTipo")
    private Set<ClienteDatoTipo> clienteDatoTipoTipo;

    // bi-directional many-to-one association to Items
    @OneToMany(mappedBy = "correo")
    private Set<Item> correo;

    // bi-directional many-to-one association to Items
    @OneToMany(mappedBy = "sms")
    private Set<Item> sms;

    // bi-directional many-to-one association to Items
    @OneToMany(mappedBy = "aplicacionMovil")
    private Set<Item> aplicacionMovil;

    // bi-directional many-to-one association to Items
    @OneToMany(mappedBy = "correoPostal")
    private Set<Item> correoPostal;

    @OneToMany(mappedBy = "tipo")
    private Set<Vinculacion> vinculacionTipo;

    @OneToMany(mappedBy = "envioTipo")
    private Set<Envio> envioTipo;

    @OneToMany(mappedBy = "campanyaEnvioTipo")
    private Set<CampanyaEnvioAdmin> campanyaEnvioTipoAdmin;

    // bi-directional many-to-one association to Campanya Datos
    @OneToMany(mappedBy = "tipoAcceso")
    private Set<CampanyaDato> campanyaDatoTipoAcceso;

    @OneToMany(mappedBy = "tipo")
    private Set<Movimiento> movimientos;

    @OneToMany(mappedBy = "tipo")
    private Set<Sesion> sesiones;

    @OneToMany(mappedBy = "sexo")
    private Set<Cliente> clientesSexo;

    @OneToMany(mappedBy = "tipoEnvio")
    private Set<CampanyaEnvioAuto> campanyaEnviosAuto;

    @OneToMany(mappedBy = "estadoCorreo")
    private Set<EnvioClienteVista> enviosClienteVista;

    public Tipo() {
    }

    public Tipo(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTablaColumna() {
        return tablaColumna;
    }

    public void setTablaColumna(String tablaColumna) {
        this.tablaColumna = tablaColumna;
    }

    public Set<Cliente> getCorreoEstado() {
        return correoEstado;
    }

    public void setCorreoEstado(Set<Cliente> correoEstado) {
        this.correoEstado = correoEstado;
    }

    public Set<Cliente> getMovilEstado() {
        return movilEstado;
    }

    public void setMovilEstado(Set<Cliente> movilEstado) {
        this.movilEstado = movilEstado;
    }

    public Set<Cliente> getPostalEstado() {
        return postalEstado;
    }

    public void setPostalEstado(Set<Cliente> postalEstado) {
        this.postalEstado = postalEstado;
    }

    public Set<ClienteDatoTipo> getClienteDatoTipoTipo() {
        return clienteDatoTipoTipo;
    }

    public void setClienteDatoTipoTipo(Set<ClienteDatoTipo> clienteDatoTipoTipo) {
        this.clienteDatoTipoTipo = clienteDatoTipoTipo;
    }

    public Set<ClienteDato> getClienteDatoTipoAcceso() {
        return clienteDatoTipoAcceso;
    }

    public void setClienteDatoTipoAcceso(Set<ClienteDato> clienteDatoTipoAcceso) {
        this.clienteDatoTipoAcceso = clienteDatoTipoAcceso;
    }

    public Set<Vinculacion> getVinculacionTipo() {
        return vinculacionTipo;
    }

    public void setVinculacionTipo(Set<Vinculacion> vinculacionTipo) {
        this.vinculacionTipo = vinculacionTipo;
    }

    public Set<Item> getCorreo() {
        return correo;
    }

    public void setCorreo(Set<Item> correo) {
        this.correo = correo;
    }

    public Set<Item> getSms() {
        return sms;
    }

    public void setSms(Set<Item> sms) {
        this.sms = sms;
    }

    public Set<Item> getAplicacionMovil() {
        return aplicacionMovil;
    }

    public void setAplicacionMovil(Set<Item> aplicacionMovil) {
        this.aplicacionMovil = aplicacionMovil;
    }

    public Set<Item> getCorreoPostal() {
        return correoPostal;
    }

    public void setCorreoPostal(Set<Item> correoPostal) {
        this.correoPostal = correoPostal;
    }

    public Set<Envio> getEnvioTipo() {
        return envioTipo;
    }

    public void setEnvioTipo(Set<Envio> envioTipo) {
        this.envioTipo = envioTipo;
    }

    public Set<CampanyaDato> getCampanyaDatoTipoAcceso() {
        return campanyaDatoTipoAcceso;
    }

    public void setCampanyaDatoTipoAcceso(Set<CampanyaDato> campanyaDatoTipoAcceso) {
        this.campanyaDatoTipoAcceso = campanyaDatoTipoAcceso;
    }

    public Set<Movimiento> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(Set<Movimiento> movimientos) {
        this.movimientos = movimientos;
    }

    public Set<CampanyaClienteAnyadirAuto> getCampanyaClienteEstado() {
        return campanyaClienteEstado;
    }

    public void setCampanyaClienteEstado(Set<CampanyaClienteAnyadirAuto> campanyaClienteEstado) {
        this.campanyaClienteEstado = campanyaClienteEstado;
    }

    public Set<CampanyaEnvioAdmin> getCampanyaEnvioTipoAdmin() {
        return campanyaEnvioTipoAdmin;
    }

    public void setCampanyaEnvioTipoAdmin(Set<CampanyaEnvioAdmin> campanyaEnvioTipoAdmin) {
        this.campanyaEnvioTipoAdmin = campanyaEnvioTipoAdmin;
    }

    public Set<Sesion> getSesiones() {
        return sesiones;
    }

    public void setSesiones(Set<Sesion> sesiones) {
        this.sesiones = sesiones;
    }

    public Set<ClienteGeneral> getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(Set<ClienteGeneral> tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public Set<Cliente> getClientesSexo() {
        return clientesSexo;
    }

    public void setClientesSexo(Set<Cliente> clientesSexo) {
        this.clientesSexo = clientesSexo;
    }

    public Set<CampanyaEnvioAuto> getCampanyaEnviosAuto() {
        return campanyaEnviosAuto;
    }

    public void setCampanyaEnviosAuto(Set<CampanyaEnvioAuto> campanyaEnviosAuto) {
        this.campanyaEnviosAuto = campanyaEnviosAuto;
    }

    public Set<EnvioClienteVista> getEnviosClienteVista() {
        return enviosClienteVista;
    }

    public void setEnviosClienteVista(Set<EnvioClienteVista> enviosClienteVista) {
        this.enviosClienteVista = enviosClienteVista;
    }public boolean isNumericoOSelector() {
        return this.isNumerico() || this.isSelector();
    }

    public boolean isNumerico() {
        return this.getNombre().substring(0, 1).equals("N");
    }

    public boolean isSelector() {
        return this.getNombre().substring(0, 1).equals("S");
    }

    public boolean isTexto() {
        return this.getNombre().substring(0, 1).equals("T");
    }

    public boolean isFecha() {
        return this.getNombre().substring(0, 1).equals("F");
    }
}
