package es.uji.apps.crm.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * The persistent class for the CRM_TIPOS_ACCESO database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "crm_tipos")
public class TipoModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;

    @Column(name = "TABLA_COLUMNA")
    private String tablaColumna;

    public TipoModel() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTablaColumna() {
        return tablaColumna;
    }

    public void setTablaColumna(String tablaColumna) {
        this.tablaColumna = tablaColumna;
    }
}
