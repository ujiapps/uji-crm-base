package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_CAMPANYAS")
public class CampanyaCampanya implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // bi-directional many-to-one association to Cliente
    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID_ORIGEN")
    private Campanya campanyaOrigen;

    // bi-directional many-to-one association to Item
    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID_DESTINO")
    private Campanya campanyaDestino;

    @ManyToOne
    @JoinColumn(name = "ESTADO_ID_ORIGEN")
    private TipoEstadoCampanya estadoOrigen;

    @ManyToOne
    @JoinColumn(name = "ESTADO_ID_DESTINO")
    private TipoEstadoCampanya estadoDestino;

    public CampanyaCampanya() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Campanya getCampanyaOrigen() {
        return campanyaOrigen;
    }

    public void setCampanyaOrigen(Campanya campanyaOrigen) {
        this.campanyaOrigen = campanyaOrigen;
    }

    public Campanya getCampanyaDestino() {
        return campanyaDestino;
    }

    public void setCampanyaDestino(Campanya campanyaDestino) {
        this.campanyaDestino = campanyaDestino;
    }

    public TipoEstadoCampanya getEstadoDestino() {
        return estadoDestino;
    }

    public void setEstadoDestino(TipoEstadoCampanya estadoDestino) {
        this.estadoDestino = estadoDestino;
    }

    public TipoEstadoCampanya getEstadoOrigen() {
        return estadoOrigen;
    }

    public void setEstadoOrigen(TipoEstadoCampanya estadoOrigen) {
        this.estadoOrigen = estadoOrigen;
    }
}