package es.uji.apps.crm.model.domains;

public enum TipoAccesoProgramaPerfil {
    LECTURA("LECTURA"), ESCRITURA("ESCRITURA");

    private final String nombre;

    TipoAccesoProgramaPerfil(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

}