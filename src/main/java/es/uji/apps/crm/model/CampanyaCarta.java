package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;


/**
 * The persistent class for the CRM_CAMPANYAS_ENVIOS_AUTO database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_CARTAS")
public class CampanyaCarta implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Lob
    @Column(name = "CUERPO")
    private String cuerpo;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private Campanya campanya;

    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private TipoEstadoCampanya campanyaCartaTipo;

    @OneToMany(mappedBy = "campanyaCarta")
    private Set<CartaImagen> cartaImagenes;


    public CampanyaCarta() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public Campanya getCampanya() {
        return campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

    public TipoEstadoCampanya getCampanyaCartaTipo() {
        return campanyaCartaTipo;
    }

    public void setCampanyaCartaTipo(TipoEstadoCampanya campanyaCartaTipo) {
        this.campanyaCartaTipo = campanyaCartaTipo;
    }

    public Set<CartaImagen> getCartaImagenes() {
        return cartaImagenes;
    }

    public void setCartaImagenes(Set<CartaImagen> cartaImagenes) {
        this.cartaImagenes = cartaImagenes;
    }


}