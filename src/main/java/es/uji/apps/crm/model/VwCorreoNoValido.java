package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

@Table(name = "CRM_EXT_CORREOS_NO_VALIDOS")
@Entity
@Component
@SuppressWarnings("serial")

public class VwCorreoNoValido implements Serializable {

    @Id
    private Long id;
    private String nombre;

    @Column(name = "NUMERO_INTENTOS")
    private Long numeroIntentos;

    private String estado;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_PRIMER_INTENTO")
    private Date fechaPrimerIntento;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_ENVIO")
    private Date fechaEnvio;

    @OneToOne(mappedBy = "correoNoValido")
    private CorreoNoValidoControl correoNoValidoControl;

    public VwCorreoNoValido() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getNumeroIntentos() {
        return numeroIntentos;
    }

    public void setNumeroIntentos(Long numeroIntentos) {
        this.numeroIntentos = numeroIntentos;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaPrimerIntento() {
        return fechaPrimerIntento;
    }

    public void setFechaPrimerIntento(Date fechaPrimerIntento) {
        this.fechaPrimerIntento = fechaPrimerIntento;
    }

    public CorreoNoValidoControl getCorreoNoValidoControl() {
        return correoNoValidoControl;
    }

    public void setCorreoNoValidoControl(CorreoNoValidoControl correoNoValidoControl) {
        this.correoNoValidoControl = correoNoValidoControl;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }
}