package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CLIENTES_DATOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_CLIENTE_EST_MOT")
public class ClienteEstadoCampanyaMotivo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ESTADO_MOTIVO_ID")
    private TipoEstadoCampanyaMotivo campanyaEstadoMotivo;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_CLIENTE_ID")
    private CampanyaCliente campanyaCliente;

    @Column(name = "COMENTARIOS")
    private String comentarios;

    public ClienteEstadoCampanyaMotivo() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoEstadoCampanyaMotivo getCampanyaEstadoMotivo() {
        return campanyaEstadoMotivo;
    }

    public void setCampanyaEstadoMotivo(TipoEstadoCampanyaMotivo campanyaEstadoMotivo) {
        this.campanyaEstadoMotivo = campanyaEstadoMotivo;
    }

    public CampanyaCliente getCampanyaCliente() {
        return campanyaCliente;
    }

    public void setCampanyaCliente(CampanyaCliente campanyaCliente) {
        this.campanyaCliente = campanyaCliente;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }
}