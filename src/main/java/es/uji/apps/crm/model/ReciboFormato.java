package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_EXT_PERSONAS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_RECIBOS_FORMATO")
public class ReciboFormato implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    private String formato;

    @OneToMany(mappedBy = "reciboFormato")
    private Set<LineaFacturacion> lineasFacturacion;

    public ReciboFormato() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public Set<LineaFacturacion> getLineasFacturacion() {
        return lineasFacturacion;
    }

    public void setLineasFacturacion(Set<LineaFacturacion> lineasFacturacion) {
        this.lineasFacturacion = lineasFacturacion;
    }
}