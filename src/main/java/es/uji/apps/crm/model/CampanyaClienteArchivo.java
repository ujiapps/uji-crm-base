package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name = "CRM_CAMPANYAS_CLIENTES_ARCHIVO")
public class CampanyaClienteArchivo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID", referencedColumnName = "ID")
    private Campanya campanya;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
//    @Column (name = "PERSONA_ID")
    private PersonaSimple persona;

    private boolean anyadir;

    private String correo;

    @Column(name = "CREADA_FICHA")
    private Boolean creadaFicha;

    @Column(name = "FECHA_ANYADIDO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAnyadido;

    @Column(name = "ADMIN_ID")
    private Long admin;

    private String nombre;
    private String apellido1;
    private String apellido2;
    private String identificacion;

    public CampanyaClienteArchivo() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Campanya getCampanya() {
        return campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

    public PersonaSimple getPersona() {
        return persona;
    }

    public void setPersona(PersonaSimple persona) {
        this.persona = persona;
    }

    public boolean getAnyadir() {
        return anyadir;
    }

    public boolean isAnyadir() {
        return anyadir;
    }

    public void setAnyadir(boolean anyadir) {
        this.anyadir = anyadir;
    }

    public Boolean getCreadaFicha() {
        return creadaFicha;
    }

    public void setCreadaFicha(Boolean creadaFicha) {
        this.creadaFicha = creadaFicha;
    }

    public Date getFechaAnyadido() {
        return fechaAnyadido;
    }

    public void setFechaAnyadido(Date fechaAnyadido) {
        this.fechaAnyadido = fechaAnyadido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Long getAdmin() {
        return admin;
    }

    public void setAdmin(Long admin) {
        this.admin = admin;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getApellidosNombre() {
        return this.apellido1 + " " + this.apellido2 + ", " + this.nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }
}