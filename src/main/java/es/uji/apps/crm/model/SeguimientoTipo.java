package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_SEGUIMIENTOS_TIPOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_SEGUIMIENTOS_TIPOS")
public class SeguimientoTipo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    // bi-directional many-to-one association to CampanyaSeguimiento
    @OneToMany(mappedBy = "seguimientoTipo")
    private Set<CampanyaSeguimiento> campanyasSeguimientos;

    // bi-directional many-to-one association to Seguimiento
    @OneToMany(mappedBy = "seguimientoTipo")
    private Set<Seguimiento> seguimientos;

    public SeguimientoTipo() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<CampanyaSeguimiento> getCampanyasSeguimientos() {
        return this.campanyasSeguimientos;
    }

    public void setCampanyasSeguimientos(Set<CampanyaSeguimiento> campanyasSeguimientos) {
        this.campanyasSeguimientos = campanyasSeguimientos;
    }

    public Set<Seguimiento> getSeguimientos() {
        return this.seguimientos;
    }

    public void setSeguimientos(Set<Seguimiento> seguimientos) {
        this.seguimientos = seguimientos;
    }

}