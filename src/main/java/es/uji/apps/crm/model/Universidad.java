package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CRM_EXT_UNIVERSIDADES")
public class Universidad implements Serializable {
    @Id
    private Long id;

    @Column(name = "NOMBRE_GENERICO")
    private String nombreEs;

    @Column(name = "NOMBRE_VAL")
    private String nombreCa;

    @Column(name = "NOMBRE_UK")
    private String nombreUk;

    public Universidad() {
    }

    public Universidad(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEs() {
        return nombreEs;
    }

    public void setNombreEs(String nombreEs) {
        this.nombreEs = nombreEs;
    }

    public String getNombreCa() {
        return nombreCa;
    }

    public void setNombreCa(String nombreCa) {
        this.nombreCa = nombreCa;
    }

    public String getNombreUk() {
        return nombreUk;
    }

    public void setNombreUk(String nombreUk) {
        this.nombreUk = nombreUk;
    }
}