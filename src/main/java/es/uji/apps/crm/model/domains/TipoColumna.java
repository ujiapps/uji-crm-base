package es.uji.apps.crm.model.domains;

public enum TipoColumna {
    ESTADOENVIO("clientes.estadosEnvios"), MOVIMIENTO("movimientos.tipoDato"), ACCESO("clientesDatos.accesos"), IDENTIFICACION("clientes.tipoIdentificacion"),
    ESTADOCUOTA("clientesCuotas.estado"), COMPARACIONENVIOS("enviosCriterios.tipoComparacionEnvios"), ENVIO("envios.tipoDato"), VINCULACION("vinculaciones.tipoDato"),
    CAMPANYACLIENTE("campanyasClientes.tipoDato"), ENVIOCAMPANYAAUTO("campanyasEnviosAuto.tipoCorreo"), RECEPCIONINFO("items.recepcionInfo"), COMPARACION("enviosCriterios.tipoComparacion");
    private final String value;

    TipoColumna(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
