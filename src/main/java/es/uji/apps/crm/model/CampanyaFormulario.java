package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CAMPANYAS database table.
 */

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_FORMULARIOS")
public class CampanyaFormulario implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String titulo;

    @Column(name = "PLANTILLA")
    private String plantilla;
    private String cabecera;
    @Column(name = "SOLICITAR_IDENTIFICACION")
    private Long solicitarIdentificacion;
    @Column(name = "SOLICITAR_NOMBRE")
    private Long solicitarNombre;
    @Column(name = "SOLICITAR_CORREO")
    private Long solicitarCorreo;
    @Column(name = "SOLICITAR_MOVIL")
    private Long solicitarMovil;

    @Column(name = "SOLICITAR_POSTAL")
    private Long solicitarPostal;

    @Column(name = "AUTENTICA_FORMULARIO")
    private Boolean autenticaFormulario;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private Campanya campanya;

    @ManyToOne
    @JoinColumn(name = "ACTO_ID")
    private CampanyaActo acto;

    @ManyToOne
    @JoinColumn(name = "ESTADO_DESTINO_ID")
    private TipoEstadoCampanya estadoDestino;

    @OneToMany(mappedBy = "campanyaFormulario")
    private Set<CampanyaFormularioEstadoOrigen> campanyaFormularioEstadosOrigen;

//    @ManyToOne
//    @JoinColumn(name = "ESTADO_ORIGEN_ID")
//    private TipoEstadoCampanya estadoOrigen;

    @OneToMany(mappedBy = "campanyaFormulario")
    private Set<CampanyaDato> campanyaDatos;


    public CampanyaFormulario() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getSolicitarIdentificacion() {
        return solicitarIdentificacion;
    }

    public void setSolicitarIdentificacion(Long solicitarIdentificacion) {
        this.solicitarIdentificacion = solicitarIdentificacion;
    }

    public Long getSolicitarNombre() {
        return solicitarNombre;
    }

    public void setSolicitarNombre(Long solicitarNombre) {
        this.solicitarNombre = solicitarNombre;
    }

    public Long getSolicitarCorreo() {
        return solicitarCorreo;
    }

    public void setSolicitarCorreo(Long solicitarCorreo) {
        this.solicitarCorreo = solicitarCorreo;
    }

    public Long getSolicitarMovil() {
        return solicitarMovil;
    }

    public void setSolicitarMovil(Long solicitarMovil) {
        this.solicitarMovil = solicitarMovil;
    }

    public Long getSolicitarPostal() {
        return solicitarPostal;
    }

    public void setSolicitarPostal(Long solicitarPostal) {
        this.solicitarPostal = solicitarPostal;
    }


    public Set<CampanyaDato> getCampanyaDatos() {
        return this.campanyaDatos;
    }

    public void setCampanyaDatos(Set<CampanyaDato> campanyaDatos) {
        this.campanyaDatos = campanyaDatos;
    }


    public Boolean getAutenticaFormulario() {
        return autenticaFormulario;
    }

    public Boolean isAutenticaFormulario() {
        return autenticaFormulario;
    }

    public void setAutenticaFormulario(Boolean autenticaFormulario) {
        this.autenticaFormulario = autenticaFormulario;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Campanya getCampanya() {
        return campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

    public TipoEstadoCampanya getEstadoDestino() {
        return estadoDestino;
    }

    public void setEstadoDestino(TipoEstadoCampanya estadoDestino) {
        this.estadoDestino = estadoDestino;
    }
//
//    public TipoEstadoCampanya getEstadoOrigen() {
//        return estadoOrigen;
//    }
//
//    public void setEstadoOrigen(TipoEstadoCampanya estadoOrigen) {
//        this.estadoOrigen = estadoOrigen;
//    }


    public Set<CampanyaFormularioEstadoOrigen> getCampanyaFormularioEstadosOrigen() {
        return campanyaFormularioEstadosOrigen;
    }

    public void setCampanyaFormularioEstadosOrigen(Set<CampanyaFormularioEstadoOrigen> campanyaFormularioEstadosOrigen) {
        this.campanyaFormularioEstadosOrigen = campanyaFormularioEstadosOrigen;
    }

    public CampanyaActo getActo() {
        return acto;
    }

    public void setActo(CampanyaActo acto) {
        this.acto = acto;
    }

    public String getCabecera() {
        return cabecera;
    }

    public void setCabecera(String cabecera) {
        this.cabecera = cabecera;
    }

    public String getPlantilla() {
        return plantilla;
    }

    public void setPlantilla(String plantilla) {
        this.plantilla = plantilla;
    }
}