package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CAMPANYAS database table.
 */

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS")
public class Campanya implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String descripcion;
    private String nombre;

    @Column(name = "VINCULACION_ID")
    private Long subVinculo;

    @Column(name = "FIDELIZA")
    private Boolean fideliza;

    @Column(name = "CORREO_AVISA_COBRO")
    private String correoAvisaCobro;

    @Column(name = "FECHA_INICIO_ACTIVA")
    private Date fechaInicioActiva;

    @Column(name = "FECHA_FIN_ACTIVA")
    private Date fechaFinActiva;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_TEXTO_ID")
    private CampanyaTexto campanyaTexto;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private Campanya campanya;

    @ManyToOne
    @JoinColumn(name = "LINEA_FACTURACION_ID")
    private LineaFacturacion lineaFacturacion;


    @OneToMany(mappedBy = "campanya")
    private Set<Campanya> campanyas;

    @OneToMany(mappedBy = "campanya")
    private Set<CampanyaClienteAnyadirAuto> campanyasClienteAnyadirAuto;

    @ManyToOne
    @JoinColumn(name = "PROGRAMA_ID")
    private Programa programa;

    @OneToMany(mappedBy = "campanya")
    private Set<CampanyaCliente> campanyasClientes;

    @OneToMany(mappedBy = "campanya")
    private Set<CampanyaFase> campanyasFases;

    @OneToMany(mappedBy = "campanya")
    private Set<CampanyaItem> campanyasItems;

    @OneToMany(mappedBy = "campanya")
    private Set<Seguimiento> seguimientos;

    @OneToMany(mappedBy = "campanya")
    private Set<CampanyaEnvioAuto> campanyaEnviosAuto;

    @OneToMany(mappedBy = "campanya")
    private Set<CampanyaEnvioAdmin> campanyaEnviosAdmin;

    @OneToMany(mappedBy = "campanya")
    private Set<CampanyaCarta> campanyaCartas;

    @OneToMany(mappedBy = "campanyaOrigen")
    private Set<CampanyaCampanya> campanyasOrigen;

    @OneToMany(mappedBy = "campanyaDestino")
    private Set<CampanyaCampanya> campanyasDestino;

    @OneToMany(mappedBy = "campanya")
    private Set<TipoTarifa> campanyasTarifas;

    @OneToMany(mappedBy = "campanya")
    private Set<ClienteDato> campanyasDatosClientes;

    @OneToMany(mappedBy = "campanya")
    private Set<CampanyaActo> campanyaActos;

    @OneToMany(mappedBy = "campanya")
    private Set<TipoEstadoCampanya> campanyaEstados;

    @OneToMany(mappedBy = "campanya")
    private Set<CampanyaFormulario> campanyaFormularios;

    @OneToMany(mappedBy = "campanya")
    private Set<CampanyaClienteArchivo> campanyaClientesArchivo;

    @OneToMany(mappedBy = "campanya")
    private Set<Movimiento> movimientos;

    @OneToMany(mappedBy = "campanya")
    private Set<ClienteItem> campanyaClienteItems;

    @OneToMany(mappedBy = "campanya")
    private Set<CampanyaCartaCliente> cartasCliente;

    public Campanya() {
    }

    public Campanya(Long campanyaId) {
        this.id = campanyaId;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Campanya getCampanya() {
        return this.campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

    public Set<Campanya> getCampanyas() {
        return this.campanyas;
    }

    public void setCampanyas(Set<Campanya> campanyas) {
        this.campanyas = campanyas;
    }

    public Programa getPrograma() {
        return this.programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public Set<CampanyaCliente> getCampanyasClientes() {
        return this.campanyasClientes;
    }

    public void setCampanyasClientes(Set<CampanyaCliente> campanyasClientes) {
        this.campanyasClientes = campanyasClientes;
    }

    public Set<CampanyaFase> getCampanyasFases() {
        return this.campanyasFases;
    }

    public void setCampanyasFases(Set<CampanyaFase> campanyasFases) {
        this.campanyasFases = campanyasFases;
    }

    public Set<CampanyaItem> getCampanyasItems() {
        return campanyasItems;
    }

    public void setCampanyasItems(Set<CampanyaItem> campanyasItems) {
        this.campanyasItems = campanyasItems;
    }

    public Set<Seguimiento> getSeguimientos() {
        return seguimientos;
    }

    public void setSeguimientos(Set<Seguimiento> seguimientos) {
        this.seguimientos = seguimientos;
    }

    public Set<CampanyaEnvioAuto> getCampanyaEnviosAuto() {
        return campanyaEnviosAuto;
    }

    public void setCampanyaEnviosAuto(Set<CampanyaEnvioAuto> campanyaEnviosAuto) {
        this.campanyaEnviosAuto = campanyaEnviosAuto;
    }

    public Long getSubVinculo() {
        return subVinculo;
    }

    public void setSubVinculo(Long subVinculo) {
        this.subVinculo = subVinculo;
    }

    public Set<Movimiento> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(Set<Movimiento> movimientos) {
        this.movimientos = movimientos;
    }

    public CampanyaTexto getCampanyaTexto() {
        return campanyaTexto;
    }

    public void setCampanyaTexto(CampanyaTexto campanyaTexto) {
        this.campanyaTexto = campanyaTexto;
    }

    public Set<CampanyaClienteAnyadirAuto> getCampanyasClienteAnyadirAuto() {
        return campanyasClienteAnyadirAuto;
    }

    public void setCampanyasClienteAnyadirAuto(
            Set<CampanyaClienteAnyadirAuto> campanyasClienteAnyadirAuto) {
        this.campanyasClienteAnyadirAuto = campanyasClienteAnyadirAuto;
    }

    public Set<CampanyaEnvioAdmin> getCampanyaEnviosAdmin() {
        return campanyaEnviosAdmin;
    }

    public void setCampanyaEnviosAdmin(Set<CampanyaEnvioAdmin> campanyaEnviosAdmin) {
        this.campanyaEnviosAdmin = campanyaEnviosAdmin;
    }

    public Set<CampanyaCarta> getCampanyaCartas() {
        return campanyaCartas;
    }

    public void setCampanyaCartas(Set<CampanyaCarta> campanyaCartas) {
        this.campanyaCartas = campanyaCartas;
    }

    public Set<CampanyaCampanya> getCampanyasOrigen() {
        return campanyasOrigen;
    }

    public void setCampanyasOrigen(Set<CampanyaCampanya> campanyasOrigen) {
        this.campanyasOrigen = campanyasOrigen;
    }

    public Set<CampanyaCampanya> getCampanyasDestino() {
        return campanyasDestino;
    }

    public void setCampanyasDestino(Set<CampanyaCampanya> campanyasDestino) {
        this.campanyasDestino = campanyasDestino;
    }

    public Set<TipoTarifa> getCampanyasTarifas() {
        return campanyasTarifas;
    }

    public void setCampanyasTarifas(Set<TipoTarifa> campanyasTarifas) {
        this.campanyasTarifas = campanyasTarifas;
    }

    public Boolean getFideliza() {
        return fideliza;
    }

    public void setFideliza(Boolean fideliza) {
        this.fideliza = fideliza;
    }

    public Set<ClienteDato> getCampanyasDatosClientes() {
        return campanyasDatosClientes;
    }

    public void setCampanyasDatosClientes(Set<ClienteDato> campanyasDatosClientes) {
        this.campanyasDatosClientes = campanyasDatosClientes;
    }

    public Set<CampanyaActo> getCampanyaActos() {
        return campanyaActos;
    }

    public void setCampanyaActos(Set<CampanyaActo> campanyaActos) {
        this.campanyaActos = campanyaActos;
    }

    public LineaFacturacion getLineaFacturacion() {
        return lineaFacturacion;
    }

    public void setLineaFacturacion(LineaFacturacion lineaFacturacion) {
        this.lineaFacturacion = lineaFacturacion;
    }

    public Set<TipoEstadoCampanya> getCampanyaEstados() {
        return campanyaEstados;
    }

    public void setCampanyaEstados(Set<TipoEstadoCampanya> campanyaEstados) {
        this.campanyaEstados = campanyaEstados;
    }

    public Set<ClienteItem> getCampanyaClienteItems() {
        return campanyaClienteItems;
    }

    public void setCampanyaClienteItems(Set<ClienteItem> campanyaClienteItems) {
        this.campanyaClienteItems = campanyaClienteItems;
    }

    public Set<CampanyaFormulario> getCampanyaFormularios() {
        return campanyaFormularios;
    }

    public void setCampanyaFormularios(Set<CampanyaFormulario> campanyaFormularios) {
        this.campanyaFormularios = campanyaFormularios;
    }

    public Set<CampanyaClienteArchivo> getCampanyaClientesArchivo() {
        return campanyaClientesArchivo;
    }

    public void setCampanyaClientesArchivo(Set<CampanyaClienteArchivo> campanyaClientesArchivo) {
        this.campanyaClientesArchivo = campanyaClientesArchivo;
    }

    public String getCorreoAvisaCobro() {
        return correoAvisaCobro;
    }

    public void setCorreoAvisaCobro(String correoAvisaCobro) {
        this.correoAvisaCobro = correoAvisaCobro;
    }

    public Date getFechaInicioActiva() {
        return fechaInicioActiva;
    }

    public void setFechaInicioActiva(Date fechaInicioActiva) {
        this.fechaInicioActiva = fechaInicioActiva;
    }

    public Date getFechaFinActiva() {
        return fechaFinActiva;
    }

    public void setFechaFinActiva(Date fechaFinActiva) {
        this.fechaFinActiva = fechaFinActiva;
    }

    public Set<CampanyaCartaCliente> getCartasCliente() {
        return cartasCliente;
    }

    public void setCartasCliente(Set<CampanyaCartaCliente> cartasCliente) {
        this.cartasCliente = cartasCliente;
    }
}