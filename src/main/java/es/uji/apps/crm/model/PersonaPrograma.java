package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_V_PERSONAS_PROGRAMAS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_V_PERSONAS_PROGRAMAS")
public class PersonaPrograma implements Serializable {
    @Id
    private Long id;

    @ManyToOne
    private PersonaPasPdi persona;

    @ManyToOne
    private Programa programa;

    @Column(name = "TIPO_ACCESO")
    private String tipoAcceso;

    public PersonaPrograma() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonaPasPdi getPersona() {
        return persona;
    }

    public void setPersona(PersonaPasPdi persona) {
        this.persona = persona;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public String getTipoAcceso() {
        return tipoAcceso;
    }

    public void setAccesoTipo(String tipo) {
        this.tipoAcceso = tipo;
    }
}