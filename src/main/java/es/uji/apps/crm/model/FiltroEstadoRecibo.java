package es.uji.apps.crm.model;

import es.uji.apps.crm.model.domains.TipoOperador;

public class FiltroEstadoRecibo {

    private TipoOperador operador;
    private Long tipoEstadoRecibo;

    public FiltroEstadoRecibo() {
    }


    public Long getTipoEstadoRecibo() {
        return tipoEstadoRecibo;
    }

    public void setTipoEstadoRecibo(Long tipoEstadoRecibo) {
        this.tipoEstadoRecibo = tipoEstadoRecibo;
    }

    public TipoOperador getOperador() {
        return operador;
    }

    public void setOperador(Long operador) {

        if (operador.equals(1L))
        {
            this.operador = TipoOperador.Y;
        }
        if (operador.equals(2L))
        {
            this.operador = TipoOperador.O;
        }
        if (operador.equals(3L))
        {
            this.operador = TipoOperador.YNO;
        }
        if (operador.equals(4L))
        {
            this.operador = TipoOperador.ONO;
        }
    }
}