package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_ENVIOS_ADJUNTOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_ENVIOS_ADJUNTOS")
public class EnvioAdjunto implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "REFERENCIA")
    private String referencia;

    @Column(name = "CONTENT_TYPE")
    private String contentType;

    @Column(name = "NOMBRE_FICHERO")
    private String nombreFichero;

    // bi-directional many-to-one association to Envio
    @ManyToOne
    private Envio envio;

    public EnvioAdjunto() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getContentType() {
        return this.contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getNombreFichero() {
        return this.nombreFichero;
    }

    public void setNombreFichero(String nombreFichero) {
        this.nombreFichero = nombreFichero;
    }

    public Envio getEnvio() {
        return this.envio;
    }

    public void setEnvio(Envio envio) {
        this.envio = envio;
    }

}