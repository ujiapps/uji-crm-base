package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_ENVIOS_MODELO")
public class CampanyaEnvioModelo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String asunto;

    private String desde;

    @Column(name = "responder_a")
    private String responder;

    private String cuerpo;

    @Column(name = "tipo_correo_envio")
    private Long tipoCorreoEnvio;

    @Column(name = "base")
    private String base;

    @Column(name = "TIPO_MODELO")
    private String tipoModelo;

    public CampanyaEnvioModelo() {
    }

    public Long getId() {
        return this.id;
    }

    public String getAsunto() {
        return asunto;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public String getDesde() {
        return desde;
    }

    public String getResponder() {
        return responder;
    }

    public Long getTipoCorreoEnvio() {
        return tipoCorreoEnvio;
    }

    public String getBase() {
        return base;
    }

    public String getTipoModelo() {
        return tipoModelo;
    }
}