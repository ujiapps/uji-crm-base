package es.uji.apps.crm.model.domains;

public enum TipoEnvio {
    APP_MOVIL(29L, "Aplicaciò mòbil"), SMS(28L, "SMS"), EMAIL(27L, "eMail"), POSTAL(30L, "Postal");

    private final Long id;
    private final String nombre;

    TipoEnvio(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

}