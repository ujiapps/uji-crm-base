package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;


/**
 * The persistent class for the CRM_CAMPANYAS_ENVIOS_AUTO database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_ENVIOS_AUTO")
public class CampanyaEnvioAuto implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String asunto;

    private String desde;

    @Column(name = "responder_a")
    private String responder;

    private String cuerpo;

    @Column(name = "tipo_correo_envio")
    private Long tipoCorreoEnvio;

    @Column(name = "base")
    private String base;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private Campanya campanya;

    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private TipoEstadoCampanya campanyaEnvioTipo;

    @ManyToOne
    @JoinColumn(name = "TIPO_ENVIO_ID")
    private Tipo tipoEnvio;

    @OneToMany(mappedBy = "envio")
    private Set<CampanyaEnvioProgramado> enviosProgramados;

    public CampanyaEnvioAuto() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public Campanya getCampanya() {
        return campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

    public TipoEstadoCampanya getCampanyaEnvioTipo() {
        return campanyaEnvioTipo;
    }

    public void setCampanyaEnvioTipo(TipoEstadoCampanya campanyaEnvioTipo) {
        this.campanyaEnvioTipo = campanyaEnvioTipo;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getResponder() {
        return responder;
    }

    public void setResponder(String responder) {
        this.responder = responder;
    }

    public Set<CampanyaEnvioProgramado> getEnviosProgramados() {
        return enviosProgramados;
    }

    public void setEnviosProgramados(Set<CampanyaEnvioProgramado> enviosProgramados) {
        this.enviosProgramados = enviosProgramados;
    }

    public Long getTipoCorreoEnvio() {
        return tipoCorreoEnvio;
    }

    public void setTipoCorreoEnvio(Long tipoCorreoEnvio) {
        this.tipoCorreoEnvio = tipoCorreoEnvio;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Tipo getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(Tipo tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }
}
