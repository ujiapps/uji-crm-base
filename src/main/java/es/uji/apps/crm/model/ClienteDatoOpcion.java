package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.*;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CLIENTES_DATOS_OPCIONES database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CLIENTES_DATOS_OPCIONES")
public class ClienteDatoOpcion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String etiqueta;

    @Column(name = "etiqueta_es")
    private String etiquetaES;

    @Column(name = "etiqueta_en")
    private String etiquetaEN;

    private Long valor;

    private String referencia;

    private Long orden;

    // bi-directional many-to-one association to clienteDatoTipo
    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private ClienteDatoTipo clienteDatoTipo;

    public ClienteDatoOpcion() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEtiqueta() {
        return this.etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public Long getValor() {
        return this.valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }

    public ClienteDatoTipo getClientesDatosTipo() {
        return this.clienteDatoTipo;
    }

    public ClienteDatoTipo getClienteDatoTipo() {
        return clienteDatoTipo;
    }

    public void setClienteDatoTipo(ClienteDatoTipo clienteDatoTipo) {
        this.clienteDatoTipo = clienteDatoTipo;
    }

    public String getEtiquetaES() {
        return etiquetaES;
    }

    public void setEtiquetaES(String etiquetaES) {
        this.etiquetaES = etiquetaES;
    }

    public String getEtiquetaEN() {
        return etiquetaEN;
    }

    public void setEtiquetaEN(String etiquetaEN) {
        this.etiquetaEN = etiquetaEN;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public Long getOrden() {
        return orden;
    }

    public void setOrden(Long orden) {
        this.orden = orden;
    }
}