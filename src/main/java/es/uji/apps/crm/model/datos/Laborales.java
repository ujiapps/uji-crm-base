package es.uji.apps.crm.model.datos;

import java.text.ParseException;
import java.util.List;

import es.uji.apps.crm.model.ClienteDato;
import es.uji.apps.crm.model.ClienteItem;
import es.uji.commons.rest.ParamUtils;

public class Laborales {

    private static final Long TIPO_ESTADO_LABORAL_ID = 996675L;
    private static final Long TIPO_SECTOR_EMPRESARIAL_ID = 807418L;
    private static final Long TIPO_CARGO_EMPRESA_ID = 8L;
    private static final Long TIPO_EMPRESA_ID = 7L;
    private static final Long TIPO_NOMBRE_ESTUDIO_ACTUAL = 6460937L;

    private Long estadoLaboral;
    private String nombreEstudioActual;
    private Long sectorEmpresarial;
    private String empresa;
    private String cargo;
    private String linkedin;

    public Laborales(List<ClienteDato> datos, List<ClienteItem> items, ClienteDato linkedin) throws ParseException {
        getDatosLaborales(datos, items, linkedin);

    }

    private void getDatosLaborales(List<ClienteDato> datos, List<ClienteItem> items, ClienteDato linkedin) throws ParseException {

        datos.forEach(dato -> {
            if (dato.getClienteDatoTipo().getId().equals(TIPO_EMPRESA_ID))
            {
                this.empresa = dato.getValorTexto();
            }
            if (dato.getClienteDatoTipo().getId().equals(TIPO_CARGO_EMPRESA_ID))
            {
                this.cargo = dato.getValorTexto();
            }
            if (dato.getClienteDatoTipo().getId().equals(TIPO_NOMBRE_ESTUDIO_ACTUAL))
            {
                this.nombreEstudioActual = dato.getValorTexto();
            }
        });

        items.forEach(item -> {
            if (item.getItem().getGrupo().getId().equals(TIPO_ESTADO_LABORAL_ID))
            {
                this.estadoLaboral = item.getItem().getId();
            }

            if (item.getItem().getGrupo().getId().equals(TIPO_SECTOR_EMPRESARIAL_ID))
            {
                this.sectorEmpresarial = item.getItem().getId();
            }
        });

        this.linkedin = ParamUtils.isNotNull(linkedin) ? linkedin.getValorSegunTipo() : "";
    }

    public Long getEstadoLaboral() {
        return estadoLaboral;
    }

    public void setEstadoLaboral(Long estadoLaboral) {
        this.estadoLaboral = estadoLaboral;
    }

    public String getNombreEstudioActual() {
        return nombreEstudioActual;
    }

    public void setNombreEstudioActual(String nombreEstudioActual) {
        this.nombreEstudioActual = nombreEstudioActual;
    }

    public Long getSectorEmpresarial() {
        return sectorEmpresarial;
    }

    public void setSectorEmpresarial(Long sectorEmpresarial) {
        this.sectorEmpresarial = sectorEmpresarial;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }
}