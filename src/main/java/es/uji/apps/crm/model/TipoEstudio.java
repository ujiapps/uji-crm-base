package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import es.uji.apps.crm.model.domains.TipoEstudioNoUJI;

@Entity
@Table(name = "CRM_TIPOS_ESTUDIOS")
public class TipoEstudio implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NOMBRE_ES")
    private String nombreEs;

    @Column(name = "NOMBRE_CA")
    private String nombreCa;

    @Column(name = "NOMBRE_UK")
    private String nombreUk;

    @Column(name = "TIPO_ID")
    private Long tipo;

    @OneToMany(mappedBy = "clasificacion")
    private Set<ClienteTitulacionesNoUJI> clientesTitulacionesNoUJI;

    @OneToMany(mappedBy = "estudioNoUJI")
    private Set<EnvioCriterioEstudioNoUJI> criterioEstudioNoUJIS;

    @Transient
    private Long tieneItems;

    public TipoEstudio() {
    }

    public TipoEstudio(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEs() {
        return nombreEs;
    }

    public void setNombreEs(String nombreEs) {
        this.nombreEs = nombreEs;
    }

    public String getNombreCa() {
        return nombreCa;
    }

    public void setNombreCa(String nombreCa) {
        this.nombreCa = nombreCa;
    }

    public String getNombreUk() {
        return nombreUk;
    }

    public void setNombreUk(String nombreUk) {
        this.nombreUk = nombreUk;
    }

    public Set<ClienteTitulacionesNoUJI> getClientesTitulacionesNoUJI() {
        return clientesTitulacionesNoUJI;
    }

    public void setClientesTitulacionesNoUJI(Set<ClienteTitulacionesNoUJI> clientesTitulacionesNoUJI) {
        this.clientesTitulacionesNoUJI = clientesTitulacionesNoUJI;
    }

    public Long getTieneItems() {
        return tieneItems;
    }

    public void setTieneItems(Long tieneItems) {
        this.tieneItems = tieneItems;
    }

    public Set<EnvioCriterioEstudioNoUJI> getCriterioEstudioNoUJIS() {
        return criterioEstudioNoUJIS;
    }

    public void setCriterioEstudioNoUJIS(Set<EnvioCriterioEstudioNoUJI> criterioEstudioNoUJIS) {
        this.criterioEstudioNoUJIS = criterioEstudioNoUJIS;
    }

    public String getTipoDescripcion() {

        if (this.getTipo() == 1)
        {
            return TipoEstudioNoUJI.GRADO.getDescripcion();
        }
        else if (this.getTipo() == 2)
        {
            return TipoEstudioNoUJI.POSTGRADO.getDescripcion();
        }
        else if (this.getTipo() == 3)
        {
            return TipoEstudioNoUJI.DOCTORADO.getDescripcion();
        }
        else if (this.getTipo() == 4)
        {
            return TipoEstudioNoUJI.ELEMENTAL.getDescripcion();
        }
        else if (this.getTipo() == 5)
        {
            return TipoEstudioNoUJI.MEDIOS.getDescripcion();
        }
        else if (this.getTipo() == 6)
        {
            return TipoEstudioNoUJI.SUPERIORESNOUNI.getDescripcion();
        }

        return "";
    }

    public Long getTipo() {
        return tipo;
    }

    public void setTipo(Long tipo) {
        this.tipo = tipo;
    }
}