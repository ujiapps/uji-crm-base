package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_EXT_PERSONAS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_EXT_MOVIMIENTOS_RECIBOS")
public class MovimientoRecibo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "TIPO_NOMBRE")
    private String tipoMovimiento;

    @Column(name = "TIPO_CONCILIA")
    private String tipoConcilia;

    @Column(name = "SIGNO_CONCILIA")
    private String signo;

    private String descripcion;

    @Column(name = "REMESA_ID")
    private Long remesa;

    @Column(name = "remesa")
    private Long remesaExportacion;

    @Column(name = "fecha_exportacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaExportacion;

    @Column(name = "id_exportado")
    private Long exportadoId;

    @ManyToOne
    @JoinColumn(name = "RECIBO_ID")
    private Recibo recibo;

    @OneToMany(mappedBy = "reciboMovimiento")
    private Set<ReciboMovimientoExportado> exportados;

    public MovimientoRecibo() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Recibo getRecibo() {
        return recibo;
    }

    public void setRecibo(Recibo recibo) {
        this.recibo = recibo;
    }

    public Long getRemesa() {
        return remesa;
    }

    public void setRemesa(Long remesa) {
        this.remesa = remesa;
    }

    public Set<ReciboMovimientoExportado> getExportados() {
        return exportados;
    }

    public void setExportados(Set<ReciboMovimientoExportado> exportados) {
        this.exportados = exportados;
    }

    public Long getRemesaExportacion() {
        return remesaExportacion;
    }

    public void setRemesaExportacion(Long remesaExportacion) {
        this.remesaExportacion = remesaExportacion;
    }

    public Date getFechaExportacion() {
        return fechaExportacion;
    }

    public void setFechaExportacion(Date fechaExportacion) {
        this.fechaExportacion = fechaExportacion;
    }

    public Long getExportadoId() {
        return exportadoId;
    }

    public void setExportadoId(Long exportadoId) {
        this.exportadoId = exportadoId;
    }

    public String getTipoConcilia() {
        return tipoConcilia;
    }

    public void setTipoConcilia(String tipoConcilia) {
        this.tipoConcilia = tipoConcilia;
    }

    public String getSigno() {
        return signo;
    }

    public void setSigno(String signo) {
        this.signo = signo;
    }
}