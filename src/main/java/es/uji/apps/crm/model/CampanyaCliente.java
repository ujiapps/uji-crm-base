package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CAMPANYAS_CLIENTES database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_CLIENTES")
public class CampanyaCliente implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private TipoEstadoCampanya campanyaClienteTipo;

    // bi-directional many-to-one association to Campanya
    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private Campanya campanya;

    //     bi-directional many-to-one association to Cliente
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CLIENTE_ID")
    private Cliente cliente;

    @Column(name = "ESTADO")
    private String comentariosEstado;

    public CampanyaCliente() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoEstadoCampanya getCampanyaClienteTipo() {
        return campanyaClienteTipo;
    }

    public void setCampanyaClienteTipo(TipoEstadoCampanya campanyaClienteTipo) {
        this.campanyaClienteTipo = campanyaClienteTipo;
    }

    public Campanya getCampanya() {
        return this.campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getComentariosEstado() {
        return comentariosEstado;
    }

    public void setComentariosEstado(String comentariosEstado) {
        this.comentariosEstado = comentariosEstado;
    }
}