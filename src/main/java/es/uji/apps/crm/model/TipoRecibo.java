package es.uji.apps.crm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CRM_EXT_TIPOS_RECIBOS", schema = "UJI_CRM")
public class TipoRecibo {


    @Id
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "EMISORA_ID")
    private Long emisora;

    @Column(name = "TIPO_PREINS")
    private String tipoRecibo;

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public Long getEmisora() {
        return emisora;
    }

    public String getTipoRecibo() {
        return tipoRecibo;
    }
}