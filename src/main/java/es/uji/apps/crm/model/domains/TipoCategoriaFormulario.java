package es.uji.apps.crm.model.domains;

public enum TipoCategoriaFormulario {
    PREMIUM(1L, "AlumniSAUJI Premium"),
    BASIC(2L, "Alumni Bàsic"),
    OTROS(3L, "Ja sóc membre però no puc accedir amb les meues dades"),
    OIPEP(4L, "AlumniSAUJI Premium com a supervisor/a de pràctiques curriculars");

    private final Long id;
    private final String texto;

    TipoCategoriaFormulario(Long id, String texto) {
        this.id = id;
        this.texto = texto;
    }

    public static String getTextoById(Long id) {
        if (id.equals(1L))
        {
            return PREMIUM.getTexto();
        }
        if (id.equals(2L))
        {
            return BASIC.getTexto();
        }
        if (id.equals(3L))
        {
            return OTROS.getTexto();
        }
        if (id.equals(4L))
        {
            return OIPEP.getTexto();
        }
        return "No existe Id";
    }

    public String getTexto() {
        return texto;
    }

    public Long getId() {
        return id;
    }

}