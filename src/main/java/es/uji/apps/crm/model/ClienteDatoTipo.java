package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CLIENTES_DATOS_TIPOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CLIENTES_DATOS_TIPOS")
public class ClienteDatoTipo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    private Integer tamanyo;

    private Integer orden;

    private Boolean duplicar;

    private Boolean modificar;

    // bi-directional many-to-one association to CampanyaDato
    @OneToMany(mappedBy = "clienteDatoTipo")
    private Set<CampanyaDato> campanyasDatos;

    // bi-directional many-to-one association to ClienteDato
    @OneToMany(mappedBy = "clienteDatoTipo")
    private Set<ClienteDato> clientesDatos;

    // bi-directional many-to-one association to ClienteDatoOpcion
    @OneToMany(mappedBy = "clienteDatoTipo")
    private Set<ClienteDatoOpcion> clientesDatosOpciones;

    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private Tipo clienteDatoTipoTipo;

    public ClienteDatoTipo() {
    }

    public ClienteDatoTipo(Long clienteDatoTipoId) {
        this.id = clienteDatoTipoId;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getTamanyo() {
        return this.tamanyo;
    }

    public void setTamanyo(Integer tamanyo) {
        this.tamanyo = tamanyo;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Set<ClienteDatoOpcion> getClientesDatosOpciones() {
        return this.clientesDatosOpciones;
    }

    public void setClientesDatosOpciones(Set<ClienteDatoOpcion> clientesDatosOpciones) {
        this.clientesDatosOpciones = clientesDatosOpciones;
    }

    public Set<CampanyaDato> getCampanyasDatos() {
        return this.campanyasDatos;
    }

    public void setCampanyasDatos(Set<CampanyaDato> campanyasDatos) {
        this.campanyasDatos = campanyasDatos;
    }

    public Set<ClienteDato> getClientesDatos() {
        return this.clientesDatos;
    }

    public void setClientesDatos(Set<ClienteDato> clientesDatos) {
        this.clientesDatos = clientesDatos;
    }

    public Tipo getClienteDatoTipoTipo() {
        return clienteDatoTipoTipo;
    }

    public void setClienteDatoTipoTipo(Tipo clienteDatoTipoTipo) {
        this.clienteDatoTipoTipo = clienteDatoTipoTipo;
    }

    public Boolean getDuplicar() {
        return duplicar;
    }

    public void setDuplicar(Boolean duplicar) {
        this.duplicar = duplicar;
    }

    public Boolean isDuplicar() {
        return duplicar;
    }

    public Boolean getModificar() {
        return modificar;
    }

    public void setModificar(Boolean modificar) {
        this.modificar = modificar;
    }

    public Boolean isModificar() {
        return modificar;
    }

    public boolean isNumericoOSelector() {
        return this.isNumerico() || this.isSelector();
    }

    public boolean isNumerico() {
        return this.getClienteDatoTipoTipo().isNumerico();
    }

    public boolean isSelector() {
        return this.getClienteDatoTipoTipo().isSelector();
    }

    public boolean isTexto() {
        return this.getClienteDatoTipoTipo().isTexto();
    }

    public boolean isFecha() {
        return this.getClienteDatoTipoTipo().isFecha();
    }
}