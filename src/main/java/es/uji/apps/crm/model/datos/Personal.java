package es.uji.apps.crm.model.datos;

import java.util.Date;
import java.util.List;

import es.uji.apps.crm.model.Cliente;
import es.uji.apps.crm.model.ClienteDato;
import es.uji.apps.crm.model.ClienteGeneral;
import es.uji.apps.crm.model.ClienteItem;
import es.uji.commons.rest.ParamUtils;

public class Personal {

    private final static Long SEXO_ID = 1L;
    private final static Long FECHA_NACIMIENTO_ID = 2L;
    private final static Long GRUPO_PAIS_NACIMIENTO_ID = 3276215L;
    private final static Long GRUPO_PROVINCIA_NACIMIENTO_ID = 3746139L;
    private final static Long GRUPO_POBLACION_CS_NACIMIENTO_ID = 3879484L;
    private final static Long GRUPO_POBLACION_VAL_NACIMIENTO_ID = 3880026L;
    private final static Long GRUPO_POBLACION_ALI_NACIMIENTO_ID = 3890089L;
    private final static Long TELEFONO_ALTERNATIVO_ID = 3L;

    private String prefijoMovil;
    private String movil;
    private Long telefonoAlternativo;
    private String email;
    private String paisContactoId;
    private Long provinciaContactoId;
    private Long poblacionContactoId;
    private String direccion;
    private String codigoPostal;
    private Long tipoIdentificacion;
    private String identificacion;
    private String nombreOficial;
    private String apellidosOficial;
    private Long sexo;
    private Date fechaNacimiento;
    private Long paisNacimiento;
    private Long provinciaNacimiento;
    private Long poblacionNacimiento;

    public Personal(ClienteGeneral clienteGeneral, Cliente cliente, List<ClienteDato> datos, List<ClienteItem> items) {
        this.tipoIdentificacion = clienteGeneral.getTipoIdentificacion().getId();
        this.identificacion = clienteGeneral.getIdentificacion();
        this.nombreOficial = cliente.getNombre();
        this.apellidosOficial = cliente.getApellidos();
        this.sexo = (ParamUtils.isNotNull(cliente.getSexo())) ? cliente.getSexo() : null;
        this.fechaNacimiento = cliente.getFechaNacimiento();
        getDatosPersonales(datos, items);

        this.prefijoMovil = cliente.getPrefijoTelefono();
        this.movil = cliente.getMovil();
        this.email = cliente.getCorreo();
        this.paisContactoId = (ParamUtils.isNotNull(cliente.getPaisPostal())) ? cliente.getPaisPostal().getId() : null;
        this.provinciaContactoId = (ParamUtils.isNotNull(cliente.getProvinciaPostal())) ? cliente.getProvinciaPostal().getId() : null;
        this.poblacionContactoId = (ParamUtils.isNotNull(cliente.getPoblacionPostal())) ? cliente.getPoblacionPostal().getId() : null;
        this.codigoPostal = cliente.getCodigoPostal();
        this.direccion = cliente.getPostal();


        getDatosContacto(datos);
    }


    private void getDatosContacto(List<ClienteDato> datos) {
        datos.stream().forEach(dato -> {
            if (dato.getClienteDatoTipo().getId().equals(TELEFONO_ALTERNATIVO_ID))
            {
                this.telefonoAlternativo = dato.getValorNumero();
            }
        });
    }

    private void getDatosPersonales(List<ClienteDato> datos, List<ClienteItem> items) {

        items.stream().forEach(item -> {

            if (item.getItem().getGrupo().getId().equals(GRUPO_PAIS_NACIMIENTO_ID))
            {
                this.paisNacimiento = item.getItem().getId();
            }
            if (item.getItem().getGrupo().getId().equals(GRUPO_PROVINCIA_NACIMIENTO_ID))
            {
                this.provinciaNacimiento = item.getItem().getId();
            }
            if (item.getItem().getGrupo().getId().equals(GRUPO_POBLACION_CS_NACIMIENTO_ID) || item.getItem().getGrupo().getId().equals(GRUPO_POBLACION_VAL_NACIMIENTO_ID) || item.getItem().getGrupo().getId().equals(GRUPO_POBLACION_ALI_NACIMIENTO_ID))
            {
                this.poblacionNacimiento = item.getItem().getId();
            }
        });
    }

    public Long getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(Long tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombreOficial() {
        return nombreOficial;
    }

    public void setNombreOficial(String nombreOficial) {
        this.nombreOficial = nombreOficial;
    }

    public String getApellidosOficial() {
        return apellidosOficial;
    }

    public void setApellidosOficial(String apellidosOficial) {
        this.apellidosOficial = apellidosOficial;
    }

    public Long getSexo() {
        return sexo;
    }

    public void setSexo(Long sexo) {
        this.sexo = sexo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Long getPaisNacimiento() {
        return paisNacimiento;
    }

    public void setPaisNacimiento(Long paisNacimiento) {
        this.paisNacimiento = paisNacimiento;
    }

    public Long getProvinciaNacimiento() {
        return provinciaNacimiento;
    }

    public void setProvinciaNacimiento(Long provinciaNacimiento) {
        this.provinciaNacimiento = provinciaNacimiento;
    }

    public Long getPoblacionNacimiento() {
        return poblacionNacimiento;
    }

    public void setPoblacionNacimiento(Long poblacionNacimiento) {
        this.poblacionNacimiento = poblacionNacimiento;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPaisContactoId() {
        return paisContactoId;
    }

    public void setPaisContactoId(String paisContactoId) {
        this.paisContactoId = paisContactoId;
    }

    public Long getProvinciaContactoId() {
        return provinciaContactoId;
    }

    public void setProvinciaContactoId(Long provinciaContactoId) {
        this.provinciaContactoId = provinciaContactoId;
    }

    public Long getPoblacionContactoId() {
        return poblacionContactoId;
    }

    public void setPoblacionContactoId(Long poblacionContactoId) {
        this.poblacionContactoId = poblacionContactoId;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getPrefijoMovil() {
        return prefijoMovil;
    }

    public void setPrefijoMovil(String prefijoMovil) {
        this.prefijoMovil = prefijoMovil;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public Long getTelefonoAlternativo() {
        return telefonoAlternativo;
    }

    public void setTelefonoAlternativo(Long telefonoAlternativo) {
        this.telefonoAlternativo = telefonoAlternativo;
    }
}