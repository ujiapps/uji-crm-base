package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "CRM_ENVIOS_PLANTILLAS")
public class EnvioPlantilla implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;

    @Column(name="GUARDADO_AUTOMATICO")
    private Long guardadoAutomatico;

    @Lob
    private String plantilla;

    public EnvioPlantilla() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPlantilla() {
        return plantilla;
    }

    public void setPlantilla(String plantilla) {
        this.plantilla = plantilla;
    }

    public Long getGuardadoAutomatico() {
        return guardadoAutomatico;
    }

    public void setGuardadoAutomatico(Long guardadoAutomatico) {
        this.guardadoAutomatico = guardadoAutomatico;
    }

    //    public Set<Envio> getEnvioPlantillas() {
//        return envioPlantillas;
//    }
//
//    public void setEnvioPlantillas(Set<Envio> envioPlantillas) {
//        this.envioPlantillas = envioPlantillas;
//    }
}