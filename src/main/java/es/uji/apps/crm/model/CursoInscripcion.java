package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_EXT_CURSOS_INSCRITOS database table.
 */

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_EXT_CURSOS_INSCRITOS")
public class CursoInscripcion implements Serializable {
    @Id
    private Long id;
    private String nombre;
//    private Long tipo;

    @Column(name = "TIPO_COMPARTE")
    private Long tipoComparte;

    @Column(name = "TIPO_NOMBRE")
    private String tipoNombre;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INSCRIPCION")
    private Date fechaInscripcion;

    @Column(name = "ANULA_INSCRIPCION")
    private Long anulaInscripcion;

    private Boolean asistencia;
    private Boolean aprovechamiento;
    private Boolean espera;
    private Boolean confirmacion;
    private Long calificacion;
    private Long importe;

    @ManyToOne
    @JoinColumn(name = "tipo")
    private Ubicacion tipo;

    @ManyToOne
    @JoinColumn(name = "persona_id")
    private Persona persona;

    public CursoInscripcion() {
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public Ubicacion getTipo() {
        return tipo;
    }

    public String getTipoNombre() {
        return tipoNombre;
    }

    public Date getFechaInscripcion() {
        return fechaInscripcion;
    }

    public Long getAnulaInscripcion() {
        return anulaInscripcion;
    }

    public Boolean getAsistencia() {
        return asistencia;
    }

    public Boolean getAprovechamiento() {
        return aprovechamiento;
    }

    public Boolean getEspera() {
        return espera;
    }

    public Boolean getConfirmacion() {
        return confirmacion;
    }

    public Long getCalificacion() {
        return calificacion;
    }

    public Long getImporte() {
        return importe;
    }

    public Persona getPersona() {
        return persona;
    }
}