package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_ENVIOS_PROG")
public class CampanyaEnvioProgramado implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "envio_id")
    private CampanyaEnvioAuto envio;

    @Column(name = "dias")
    private Long dias;

    @Column(name = "meses")
    private Long meses;

    @Column(name = "anyos")
    private Long anyos;

    @Column(name="puntual")
    private Boolean puntual;

    @Column(name="periodica")
    private Boolean periodica;

    public CampanyaEnvioProgramado() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDias() {
        return dias;
    }

    public void setDias(Long dias) {
        this.dias = dias;
    }

    public Long getMeses() {
        return meses;
    }

    public void setMeses(Long meses) {
        this.meses = meses;
    }

    public Long getAnyos() {
        return anyos;
    }

    public void setAnyos(Long anyos) {
        this.anyos = anyos;
    }

    public Boolean isPuntual() {
        return puntual;
    }

    public void setPuntual(Boolean puntual) {
        this.puntual = puntual;
    }

    public Boolean isPeriodica() {
        return periodica;
    }

    public void setPeriodica(Boolean periodica) {
        this.periodica = periodica;
    }

    public CampanyaEnvioAuto getEnvio() {
        return envio;
    }

    public void setEnvio(CampanyaEnvioAuto envio) {
        this.envio = envio;
    }
}