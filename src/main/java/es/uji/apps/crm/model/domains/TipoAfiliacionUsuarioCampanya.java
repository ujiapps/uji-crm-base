package es.uji.apps.crm.model.domains;

public enum TipoAfiliacionUsuarioCampanya {
    DEFINITIVO("ESTADO-ALTA"),
    BAJA("ESTADO-BAJA"),
    BAJAVINCULO("BAJA-VINCULO"),
    BAJAWEB("ESTADO-BAJA-WEB"),
    ALTAWEB("ESTADO-ALTA-WEB"),
    WEB("WEB"),
    PROPUESTO("ESTADO-PROPUESTO"),
    PENDIENTE("ESTADO-PENDIENTE");

    private final String tipo;

    TipoAfiliacionUsuarioCampanya(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }
}
