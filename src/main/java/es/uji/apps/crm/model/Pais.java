package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CRM_EXT_PAISES")
public class Pais implements Serializable {
    @Id
    private String id;

    @Column(name = "NOMBRE_CA")
    private String nombreCA;

    @Column(name = "NOMBRE_CA_LIMPIO")
    private String nombreCALimpio;

    @Column(name = "NOMBRE_ES")
    private String nombreES;

    @Column(name = "NOMBRE_ES_LIMPIO")
    private String nombreESLimpio;

    @Column(name = "NOMBRE_EN")
    private String nombreEN;

    @Column(name = "NOMBRE_EN_LIMPIO")
    private String nombreENLimpio;

    private Integer orden;

    @OneToMany(mappedBy = "nacionalidad")
    private Set<Persona> nacionalidadesPersona;

    @OneToMany(mappedBy = "nacionalidad")
    private Set<Cliente> nacionalidadesCliente;

    @OneToMany(mappedBy = "paisPostal")
    private Set<Persona> paisesPostales;

    public Pais(String nacionalidad) {
        this.id = nacionalidad;
    }

    public Pais() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreCA() {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA) {
        this.nombreCA = nombreCA;
    }

    public String getNombreES() {
        return nombreES;
    }

    public void setNombreES(String nombreES) {
        this.nombreES = nombreES;
    }

    public String getNombreEN() {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN) {
        this.nombreEN = nombreEN;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Set<Persona> getNacionalidadesPersona() {
        return nacionalidadesPersona;
    }

    public void setNacionalidadesPersona(Set<Persona> nacionalidadesPersona) {
        this.nacionalidadesPersona = nacionalidadesPersona;
    }

    public Set<Cliente> getNacionalidadesCliente() {
        return nacionalidadesCliente;
    }

    public void setNacionalidadesCliente(Set<Cliente> nacionalidadesCliente) {
        this.nacionalidadesCliente = nacionalidadesCliente;
    }

    public Set<Persona> getPaisesPostales() {
        return paisesPostales;
    }

    public void setPaisesPostales(Set<Persona> paisesPostales) {
        this.paisesPostales = paisesPostales;
    }
}