package es.uji.apps.crm.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_ENVIOS_ACTO")
public class CampanyaActoEnvio implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String asunto;

    private String desde;

    @Column(name = "responder_a")
    private String responder;

    private String cuerpo;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ACTO_ID")
    private CampanyaActo campanyaActo;

    public CampanyaActoEnvio() {
    }

    public Long getId() {
        return this.id;
    }

    public String getAsunto() {
        return asunto;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public String getDesde() {
        return desde;
    }

    public String getResponder() {
        return responder;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public void setResponder(String responder) {
        this.responder = responder;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public CampanyaActo getCampanyaActo() {
        return campanyaActo;
    }

    public void setCampanyaActo(CampanyaActo campanyaActo) {
        this.campanyaActo = campanyaActo;
    }
}