package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CAMPANYAS_DATOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_DATOS")
public class CampanyaDato implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "texto_ca")
    private String textoCa;

    @Column(name = "texto_es")
    private String textoEs;

    @Column(name = "texto_uk")
    private String textoUk;

    private Integer orden;

    private Boolean requerido;

    private Long tamanyo;

    private String etiqueta;

    @Column(name = "texto_ayuda_es")
    private String textoAyudaEs;
    @Column(name = "texto_ayuda_ca")
    private String textoAyudaCa;
    @Column(name = "texto_ayuda_uk")
    private String textoAyudaUk;


    private Boolean visualizar;

    @Column(name = "vinculado_campanya")
    private Boolean vinculadoCampanya;

    @Column(name = "vinculado_acto")
    private Boolean vinculadoActo;

    @ManyToOne
    @JoinColumn(name = "FORMULARIO_ID")
    private CampanyaFormulario campanyaFormulario;

//    // bi-directional many-to-one association to Campanya
//    @ManyToOne
//    private Campanya campanya;

    // bi-directional many-to-one association to ClienteDatoTipo
    @ManyToOne
    @JoinColumn(name = "DATO_ID")
    private ClienteDatoTipo clienteDatoTipo;

    // bi-directional many-to-one association to Grupo
    @ManyToOne
    @JoinColumn(name = "GRUPO_ID")
    private Grupo grupo;

    // bi-directional many-to-one association to Item
    @ManyToOne
    private Item item;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_DATO_ID")
    private CampanyaDato campanyaDato;

    @OneToMany(mappedBy = "campanyaDato")
    private Set<CampanyaDato> campanyaDatos;

    // bi-directional many-to-one association to Tipo
    @ManyToOne
    @JoinColumn(name = "TIPO_ACCESO_ID")
    private Tipo tipoAcceso;

    @ManyToOne
    @JoinColumn(name = "PREGUNTA_RELACIONADA_ID")
    private CampanyaDato preguntaRelacionada;

    @ManyToOne
    @JoinColumn(name = "RESPUESTA_RELACIONADA_ID")
    private Item respuestaRelacionada;

    @ManyToOne
    @JoinColumn(name = "TIPO_VALIDACION_ID")
    private TipoValidacion tipoValidacion;

    public CampanyaDato() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOrden() {
        return this.orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

//    public Campanya getCampanya() {
//        return this.campanya;
//    }
//
//    public void setCampanya(Campanya campanya) {
//        this.campanya = campanya;
//    }

    public ClienteDatoTipo getClienteDatoTipo() {
        return this.clienteDatoTipo;
    }

    public void setClienteDatoTipo(ClienteDatoTipo clienteDatoTipo) {
        this.clienteDatoTipo = clienteDatoTipo;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Boolean getRequerido() {
        return requerido;
    }

    public void setRequerido(Boolean requerido) {
        this.requerido = requerido;
    }

    public Tipo getTipoAcceso() {
        return tipoAcceso;
    }

    public void setTipoAcceso(Tipo tipoAcceso) {
        this.tipoAcceso = tipoAcceso;
    }

    public CampanyaDato getCampanyaDato() {
        return campanyaDato;
    }

    public void setCampanyaDato(CampanyaDato campanyaDato) {
        this.campanyaDato = campanyaDato;
    }

    public Set<CampanyaDato> getCampanyasDatos() {
        return campanyaDatos;
    }

    public Set<CampanyaDato> getCampanyaDatos() {
        return campanyaDatos;
    }

    public void setCampanyaDatos(Set<CampanyaDato> campanyaDatos) {
        this.campanyaDatos = campanyaDatos;
    }

    public Boolean getVinculadoCampanya() {
        return vinculadoCampanya;
    }

    public void setVinculadoCampanya(Boolean vinculadoCampanya) {
        this.vinculadoCampanya = vinculadoCampanya;
    }

    public Long getTamanyo() {
        return tamanyo;
    }

    public void setTamanyo(Long tamanyo) {
        this.tamanyo = tamanyo;
    }

    public CampanyaFormulario getCampanyaFormulario() {
        return campanyaFormulario;
    }

    public void setCampanyaFormulario(CampanyaFormulario campanyaFormulario) {
        this.campanyaFormulario = campanyaFormulario;
    }

    public Boolean getVinculadoActo() {
        return vinculadoActo;
    }

    public void setVinculadoActo(Boolean vinculadoActo) {
        this.vinculadoActo = vinculadoActo;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getTextoAyudaByIdioma(String idioma) {

        if (idioma == null)
        {
            idioma = "CA";
        }
        switch (idioma.toUpperCase())
        {
            case "ES":
                return getTextoAyudaEs();
            case "UK":
            case "EN":
                return getTextoAyudaUk();
            default:
                return getTextoAyudaCa();
        }
    }

    public String getTextoAyudaEs() {
        return textoAyudaEs;
    }

    public void setTextoAyudaEs(String textoAyudaEs) {
        this.textoAyudaEs = textoAyudaEs;
    }

    public String getTextoAyudaCa() {
        return textoAyudaCa;
    }

    public void setTextoAyudaCa(String textoAyudaCa) {
        this.textoAyudaCa = textoAyudaCa;
    }

    public String getTextoAyudaUk() {
        return textoAyudaUk;
    }

    public void setTextoAyudaUk(String textoAyudaUk) {
        this.textoAyudaUk = textoAyudaUk;
    }

    public String getTextoByIdioma(String idioma) {
        if (idioma == null)
        {
            idioma = "CA";
        }
        switch (idioma.toUpperCase())
        {
            case "ES":
                return getTextoEs();
            case "UK":
            case "EN":
                return getTextoUk();
            default:
                return getTextoCa();
        }
    }

    public String getTextoCa() {
        return textoCa;
    }

    public void setTextoCa(String textoCa) {
        this.textoCa = textoCa;
    }

    public String getTextoEs() {
        return textoEs;
    }

    public void setTextoEs(String textoEs) {
        this.textoEs = textoEs;
    }

    public String getTextoUk() {
        return textoUk;
    }

    public void setTextoUk(String textoUk) {
        this.textoUk = textoUk;
    }

    public Boolean getVisualizar() {
        return visualizar;
    }

    public void setVisualizar(Boolean visualizar) {
        this.visualizar = visualizar;
    }

    public CampanyaDato getPreguntaRelacionada() {
        return preguntaRelacionada;
    }

    public void setPreguntaRelacionada(CampanyaDato preguntaRelacionada) {
        this.preguntaRelacionada = preguntaRelacionada;
    }

    public Item getRespuestaRelacionada() {
        return respuestaRelacionada;
    }

    public void setRespuestaRelacionada(Item respuestaRelacionada) {
        this.respuestaRelacionada = respuestaRelacionada;
    }

    public TipoValidacion getTipoValidacion() {
        return tipoValidacion;
    }

    public void setTipoValidacion(TipoValidacion tipoValidacion) {
        this.tipoValidacion = tipoValidacion;
    }

}