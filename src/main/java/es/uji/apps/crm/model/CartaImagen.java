package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CAMPANYAS database table.
 */

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_ENVIOS_IMAGENES")
public class CartaImagen implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CARTA_ID")
    private CampanyaCarta campanyaCarta;

    private String nombre;
    private String referencia;

    public CartaImagen() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public CampanyaCarta getCampanyaCarta() {
        return campanyaCarta;
    }

    public void setCampanyaCarta(CampanyaCarta campanyaCarta) {
        this.campanyaCarta = campanyaCarta;
    }
}