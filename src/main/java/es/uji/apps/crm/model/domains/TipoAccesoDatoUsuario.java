package es.uji.apps.crm.model.domains;

public enum TipoAccesoDatoUsuario {
    LECTURA(5L, "Lectura"), ESCRITURA(4L, "Escritura"), INTERNO(9L, "Intern");

    private final Long id;
    private final String nombre;

    TipoAccesoDatoUsuario(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

}