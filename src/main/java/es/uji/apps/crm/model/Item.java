package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import es.uji.commons.rest.ParamUtils;

/**
 * The persistent class for the CRM_ITEMS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_ITEMS")
public class Item implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "APLICACION_MOVIL_ID")
    private Tipo aplicacionMovil;

    @ManyToOne
    @JoinColumn(name = "CORREO_ID")
    private Tipo correo;

    @ManyToOne
    @JoinColumn(name = "CORREO_POSTAL_ID")
    private Tipo correoPostal;

    @Column(name = "NOMBRE_CA")
    private String nombreCa;

    @Column(name = "NOMBRE_ES")
    private String nombreEs;

    @Column(name = "NOMBRE_UK")
    private String nombreUk;

    @Column(name = "DESCRIPCION_CA")
    private String descripcionCa;

    @Column(name = "DESCRIPCION_ES")
    private String descripcionEs;

    @Column(name = "DESCRIPCION_UK")
    private String descripcionUk;

    private Integer orden;

    private Integer referencia;

    @ManyToOne
    @JoinColumn(name = "SMS_ID")
    private Tipo sms;

    private Boolean visible;

    // bi-directional many-to-one association to TipoAcceso
    @ManyToOne
    @JoinColumn(name = "TIPO_ACCESO_ID")
    private TipoAcceso tipoAcceso;

    // bi-directional many-to-one association to ClienteItem
    @OneToMany(mappedBy = "item")
    private Set<ClienteItem> clientesItems;

    @OneToMany(mappedBy = "item")
    private Set<ClienteCirculo> clienteCirculos;

    // bi-directional many-to-one association to EnvioItem
    @OneToMany(mappedBy = "item")
    private Set<EnvioItem> enviosItems;

    // bi-directional many-to-one association to CampanyaItems
    @OneToMany(mappedBy = "item")
    private Set<CampanyaItem> campanyaItems;

    @OneToMany(mappedBy = "itemOrigen")
    private Set<ItemItem> itemItemsOrigen;

    @OneToMany(mappedBy = "itemDestino")
    private Set<ItemItem> enviosItemsDestino;

    @OneToMany(mappedBy = "item")
    private Set<ClienteDato> clienteDatos;

    // bi-directional many-to-one association to Grupo
    @ManyToOne
    private Grupo grupo;

    @OneToMany(mappedBy = "item")
    private Set<ItemClasificacion> itemsClasificacion;

    public Item() {
    }

    public Item(Long itemId) {
        this.id = itemId;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOrden() {
        return this.orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Integer getReferencia() {
        return this.referencia;
    }

    public void setReferencia(Integer referencia) {
        this.referencia = referencia;
    }

    public Boolean getVisible() {
        return this.visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Boolean isVisible() {
        return this.visible;
    }

    public Set<ClienteItem> getClientesItems() {
        return this.clientesItems;
    }

    public void setClientesItems(Set<ClienteItem> clientesItems) {
        this.clientesItems = clientesItems;
    }

    public TipoAcceso getTipoAcceso() {
        return this.tipoAcceso;
    }

    public void setTipoAcceso(TipoAcceso tipoAcceso) {
        this.tipoAcceso = tipoAcceso;
    }

    public Set<EnvioItem> getEnviosItems() {
        return this.enviosItems;
    }

    public void setEnviosItems(Set<EnvioItem> enviosItems) {
        this.enviosItems = enviosItems;
    }

    public Grupo getGrupo() {
        return this.grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public Tipo getAplicacionMovil() {
        return aplicacionMovil;
    }

    public void setAplicacionMovil(Tipo aplicacionMovil) {
        this.aplicacionMovil = aplicacionMovil;
    }

    public Tipo getCorreo() {
        return correo;
    }

    public void setCorreo(Tipo correo) {
        this.correo = correo;
    }

    public Tipo getCorreoPostal() {
        return correoPostal;
    }

    public void setCorreoPostal(Tipo correoPostal) {
        this.correoPostal = correoPostal;
    }

    public Tipo getSms() {
        return sms;
    }

    public void setSms(Tipo sms) {
        this.sms = sms;
    }

    public Set<ItemItem> getItemItems() {
        return itemItemsOrigen;
    }

    public void setItemItems(Set<ItemItem> itemItems) {
        this.itemItemsOrigen = itemItems;
    }

    public Set<ItemItem> getEnviosItemsDestino() {
        return enviosItemsDestino;
    }

    public void setEnviosItemsDestino(Set<ItemItem> enviosItemsDestino) {
        this.enviosItemsDestino = enviosItemsDestino;
    }

    public Set<CampanyaItem> getCampanyaItems() {
        return campanyaItems;
    }

    public void setCampanyaItems(Set<CampanyaItem> campanyaItems) {
        campanyaItems = campanyaItems;
    }

    public String getNombreByIdioma(String idioma) {
        if (idioma == null)
        {
            idioma = "CA";
        }
        switch (idioma.toUpperCase())
        {
            case "ES":
                if (ParamUtils.isNotNull(getNombreEs()))
                {
                    return getNombreEs();
                }
                else
                {
                    return getNombreCa();
                }
            case "UK":
            case "EN":
                if (ParamUtils.isNotNull(getNombreUk()))
                {
                    return getNombreUk();
                }
                else
                {
                    return getNombreCa();
                }
            default:
                return getNombreCa();
        }
    }

    public String getNombreCa() {
        return this.nombreCa;
    }

    public void setNombreCa(String nombreCa) {
        this.nombreCa = nombreCa;
    }

    public String getNombreEs() {
        return this.nombreEs;
    }

    public void setNombreEs(String nombreEs) {
        this.nombreEs = nombreEs;
    }

    public String getNombreUk() {
        return this.nombreUk;
    }

    public void setNombreUk(String nombreUk) {
        this.nombreUk = nombreUk;
    }

    public String getDescripcionByIdioma(String idioma) {
        if (idioma == null)
        {
            idioma = "CA";
        }
        switch (idioma.toUpperCase())
        {
            case "ES":
                return getDescripcionEs();
            case "UK":
            case "EN":
                return getDescripcionUk();
            default:
                return getDescripcionCa();
        }
    }

    public String getDescripcionCa() {
        return descripcionCa;
    }

    public void setDescripcionCa(String descripcionCa) {
        this.descripcionCa = descripcionCa;
    }

    public String getDescripcionEs() {
        return descripcionEs;
    }

    public void setDescripcionEs(String descripcionEs) {
        this.descripcionEs = descripcionEs;
    }

    public String getDescripcionUk() {
        return descripcionUk;
    }

    public void setDescripcionUk(String descripcionUk) {
        this.descripcionUk = descripcionUk;
    }

    public Set<ItemItem> getItemItemsOrigen() {
        return itemItemsOrigen;
    }

    public void setItemItemsOrigen(Set<ItemItem> itemItemsOrigen) {
        this.itemItemsOrigen = itemItemsOrigen;
    }

    public Set<ClienteDato> getClienteDatos() {
        return clienteDatos;
    }

    public void setClienteDatos(Set<ClienteDato> clienteDatos) {
        this.clienteDatos = clienteDatos;
    }

    public Set<ItemClasificacion> getItemsClasificacion() {
        return itemsClasificacion;
    }

    public void setItemsClasificacion(Set<ItemClasificacion> itemsClasificacion) {
        this.itemsClasificacion = itemsClasificacion;
    }

    public Set<ClienteCirculo> getClienteCirculos() {
        return clienteCirculos;
    }

    public void setClienteCirculos(Set<ClienteCirculo> clienteCirculos) {
        this.clienteCirculos = clienteCirculos;
    }
}