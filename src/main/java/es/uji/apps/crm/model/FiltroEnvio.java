package es.uji.apps.crm.model;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class FiltroEnvio {
    private String busqueda;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaDesde;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHasta;

    private Long tipoEnvio;
    private Long persona;
    private String email;
    private Long estado;


    public FiltroEnvio() {
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Long getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(Long tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }

    public Long getPersona() {
        return persona;
    }

    public void setPersona(Long persona) {
        this.persona = persona;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }
}