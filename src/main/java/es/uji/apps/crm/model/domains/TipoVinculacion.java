package es.uji.apps.crm.model.domains;

public enum TipoVinculacion {
    GERENTE(19L, "Gerente"), SECRETARIO(23L, "Secretario"), TRABAJADOR(24L, "Trabajador");

    private final Long id;
    private final String nombre;

    TipoVinculacion(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

}