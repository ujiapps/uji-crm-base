package es.uji.apps.crm.model;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import es.uji.apps.crm.model.domains.TipoAccesoDatoUsuario;
import es.uji.apps.crm.services.UtilsService;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CLIENTES_DATOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CLIENTES_DATOS")
public class ClienteDato implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "VALOR_TEXTO")
    private String valorTexto;

    @Column(name = "VALOR_NUMERO")
    private Long valorNumero;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "VALOR_FECHA")
    private Date valorFecha;

    private String observaciones;

    @Column(name = "FECHA_ULT_MOD")
    private Date fechaUltimaModificacion;

    @Column(name = "VALOR_FICHERO_BINARIO")
    @Lob()
    private byte[] valorFicheroBinario;

    @Column(name = "VALOR_FICHERO_NOMBRE")
    private String valorFicheroNombre;

    @Column(name = "VALOR_FICHERO_MIMETYPE")
    private String valorFicheroMimetype;

    // bi-directional many-to-one association to Cliente
    @ManyToOne
    @JoinColumn(name = "CLIENTE_ID")
    private Cliente cliente;

    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    private Item item;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private Campanya campanya;

    //    @ManyToOne
    @Column(name = "CLIENTE_DATO_ID")
//    private ClienteDato clienteDato;
    private Long clienteDatoId;

//    @OneToMany(mappedBy = "clienteDato")
//    private Set<ClienteDato> clienteDatos;

//    @OneToMany(mappedBy = "clienteDato")
//    private Set<ClienteItem> clienteItemDatos;

    // bi-directional many-to-one association to ClienteDatoTipo
    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private ClienteDatoTipo clienteDatoTipo;

    @ManyToOne
    @JoinColumn(name = "TIPO_ACCESO_ID")
    private Tipo clienteDatoTipoAcceso;

    @ManyToOne
    @JoinColumn(name = "ACTO_ID")
    private CampanyaActo campanyaActo;

    public ClienteDato() {
    }

    public ClienteDato(Long clienteId, ClienteDatoTipo clienteDatoTipo, String valor, String format, Long tipoAccesoId, Long clienteDatoId, Long itemId, Long campanyaId, String observaciones) throws ParseException {

        this.setCliente(new Cliente(clienteId));
        this.setClienteDatoTipo(clienteDatoTipo);

        if (ParamUtils.isNotNull(clienteDatoId)) {
            this.setClienteDatoId(clienteDatoId);
        }

        if (ParamUtils.isNotNull(itemId)) {
            this.setItem(new Item(itemId));
        }

        this.setClienteDatoTipoAcceso(new Tipo(tipoAccesoId == null ? TipoAccesoDatoUsuario.INTERNO.getId() : tipoAccesoId));
        this.setObservaciones(observaciones);

        if (ParamUtils.isNotNull(campanyaId)) {
            this.setCampanya(new Campanya(campanyaId));
        }

        this.setValorSegunTipo(valor, format);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValorTexto() {
        return valorTexto;
    }

    public void setValorTexto(String valorTexto) {
        this.valorTexto = valorTexto;
    }

    public Long getValorNumero() {
        return valorNumero;
    }

    public void setValorNumero(Long valorNumero) {
        this.valorNumero = valorNumero;
    }

    public Date getValorFecha() {
        return valorFecha;
    }

    public void setValorFecha(Date valorFecha) {
        this.valorFecha = valorFecha;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public byte[] getValorFicheroBinario() {
        return valorFicheroBinario;
    }

    public void setValorFicheroBinario(byte[] valorFicheroBinario) {
        this.valorFicheroBinario = valorFicheroBinario;
    }

    public String getValorFicheroNombre() {
        return valorFicheroNombre;
    }

    public void setValorFicheroNombre(String valorFicheroNombre) {
        this.valorFicheroNombre = valorFicheroNombre;
    }

    public String getValorFicheroMimetype() {
        return valorFicheroMimetype;
    }

    public void setValorFicheroMimetype(String valorFicheroMimetype) {
        this.valorFicheroMimetype = valorFicheroMimetype;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ClienteDatoTipo getClienteDatoTipo() {
        return this.clienteDatoTipo;
    }

    public void setClienteDatoTipo(ClienteDatoTipo clienteDatoTipo) {
        this.clienteDatoTipo = clienteDatoTipo;
    }

    public Tipo getClienteDatoTipoAcceso() {
        return clienteDatoTipoAcceso;
    }

    public void setClienteDatoTipoAcceso(Tipo clienteDatoTipoAcceso) {
        this.clienteDatoTipoAcceso = clienteDatoTipoAcceso;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Campanya getCampanya() {
        return campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

    public CampanyaActo getCampanyaActo() {
        return campanyaActo;
    }

    public void setCampanyaActo(CampanyaActo campanyaActo) {
        this.campanyaActo = campanyaActo;
    }

    public Long getClienteDatoId() {
        return clienteDatoId;
    }

    public void setClienteDatoId(Long clienteDatoId) {
        this.clienteDatoId = clienteDatoId;
    }

    public Date getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    public void setValorSegunTipo(String valor, String format) throws ParseException {

//        if (valor.isEmpty()) {
//            return;
//        }
        if (this.getClienteDatoTipo().isNumericoOSelector()) {
            this.setValorNumero(ParamUtils.parseLong(valor));
            return;
        }
        if (this.getClienteDatoTipo().isTexto()) {
            this.setValorTexto(valor);
            return;
        }
        if (this.getClienteDatoTipo().isFecha()) {
            this.setValorFecha(UtilsService.fechaParse(valor.substring(0, 10), (!ParamUtils.isNotNull(format)) ? "yyyy-MM-dd" : format));
            return;
        }
        this.setValorFicheroNombre(valor);
    }

    public String getValorSegunTipo () throws ParseException {
        ClienteDatoTipo clienteDatoTipo = this.getClienteDatoTipo();

        if (clienteDatoTipo.isNumericoOSelector()) {
            return String.valueOf(this.getValorNumero());
        }
        if (clienteDatoTipo.isTexto()) {
            return this.getValorTexto();
        }
        if (clienteDatoTipo.isFecha()) {
            return String.valueOf(this.getValorFecha());
        }
        return this.getValorFicheroNombre();
    }
}