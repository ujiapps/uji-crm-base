package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;


@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_ENVIOS_CRITERIOS")
public class EnvioCriterio implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "TIPO_SELECCION_CAMPANYA")
    private Long tipoSeleccionCampanya;

    @Column(name = "MES_NACIMIENTO")
    private String mesNacimiento;

    @Column(name = "ANYO_NACIMIENTO_DESDE")
    private Long anyoNacimientoDesde;

    @Column(name = "ANYO_NACIMIENTO_HASTA")
    private Long anyoNacimientoHasta;

    @Column(name = "COMBINADO_CAMPANYAS")
    private Long combinadoCampanyas;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private Campanya campanya;

//    @ManyToOne
//    @JoinColumn(name = "ESTADO_CAMPANYA_ID")
//    private TipoEstadoCampanya tipoEstadoCampanya;

    @ManyToOne
    @JoinColumn(name = "ENVIO_ID")
    private Envio envio;

    @OneToMany(mappedBy = "envioCriterio")
    private Set<EnvioCriterioCirculo> envioCriterioCirculos;

    @OneToMany(mappedBy = "envioCriterio")
    private Set<EnvioCriterioCampanyaEstado> envioCriterioCampanyaEstados;

    @OneToMany(mappedBy = "envioCriterio")
    private Set<EnvioCriterioEstudioUJIBeca> envioCriterioEstudioUJIBecas;

    public EnvioCriterio() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Campanya getCampanya() {
        return campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

//    public TipoEstadoCampanya getTipoEstadoCampanya() {
//        return tipoEstadoCampanya;
//    }
//
//    public void setTipoEstadoCampanya(TipoEstadoCampanya tipoEstadoCampanya) {
//        this.tipoEstadoCampanya = tipoEstadoCampanya;
//    }

    public Envio getEnvio() {
        return envio;
    }

    public void setEnvio(Envio envio) {
        this.envio = envio;
    }

    public String getMesNacimiento() {
        return mesNacimiento;
    }

    public void setMesNacimiento(String mesNacimiento) {
        this.mesNacimiento = mesNacimiento;
    }

    public Long getCombinadoCampanyas() {
        return combinadoCampanyas;
    }

    public void setCombinadoCampanyas(Long combinadoCampanyas) {
        this.combinadoCampanyas = combinadoCampanyas;
    }

    public Long getTipoSeleccionCampanya() {
        return tipoSeleccionCampanya;
    }

    public void setTipoSeleccionCampanya(Long tipoSeleccionCampanya) {
        this.tipoSeleccionCampanya = tipoSeleccionCampanya;
    }

    public Long getAnyoNacimientoDesde() {
        return anyoNacimientoDesde;
    }

    public void setAnyoNacimientoDesde(Long anyoNacimientoDesde) {
        this.anyoNacimientoDesde = anyoNacimientoDesde;
    }

    public Long getAnyoNacimientoHasta() {
        return anyoNacimientoHasta;
    }

    public void setAnyoNacimientoHasta(Long anyoNacimientoHasta) {
        this.anyoNacimientoHasta = anyoNacimientoHasta;
    }

    public Set<EnvioCriterioCirculo> getEnvioCriterioCirculos() {
        return envioCriterioCirculos;
    }

    public void setEnvioCriterioCirculos(Set<EnvioCriterioCirculo> envioCriterioCirculos) {
        this.envioCriterioCirculos = envioCriterioCirculos;
    }

    public Set<EnvioCriterioCampanyaEstado> getEnvioCriterioCampanyaEstados() {
        return envioCriterioCampanyaEstados;
    }

    public void setEnvioCriterioCampanyaEstados(Set<EnvioCriterioCampanyaEstado> envioCriterioCampanyaEstados) {
        this.envioCriterioCampanyaEstados = envioCriterioCampanyaEstados;
    }

    public Set<EnvioCriterioEstudioUJIBeca> getEnvioCriterioEstudioUJIBecas() {
        return envioCriterioEstudioUJIBecas;
    }

    public void setEnvioCriterioEstudioUJIBecas(Set<EnvioCriterioEstudioUJIBeca> envioCriterioEstudioUJIBecas) {
        this.envioCriterioEstudioUJIBecas = envioCriterioEstudioUJIBecas;
    }
}