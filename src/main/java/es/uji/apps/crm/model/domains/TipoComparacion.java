package es.uji.apps.crm.model.domains;

public enum TipoComparacion {
    MAYORQUE(58L, "Mayor que"), MENORQUE(59L, "Menor que"), IGUAL(60L, "Igual"), MAYOROIGUAL(61L, "Mayor o igual"), MENOROIGUAL(62L, "Menor o igual"), ENTRE(64L, "Entre");
    private final Long id;
    private final String nombre;

    TipoComparacion(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}