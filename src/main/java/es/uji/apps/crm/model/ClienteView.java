package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name = "CRM_VW_CLIENTES")

public class ClienteView implements Serializable {
    @Id
    @Column(name = "CLIENTE_ID")
    private Long id;

    private String identificacion;

    @Column(name = "TIPO_IDENTIFICACION_ID")
    private Long tipoIdentificacion;

    private String correo;

//    @Column(name = "correo_oficial")
//    private String correoOficial;

    @Column(name = "NOMBRE_OFICIAL")
    private String nombre;

    @Column(name = "APELLIDOS_OFICIAL")
    private String apellidos;

    @Column(name = "CODIGO_POSTAL")
    private String codigoPostal;
    private String postal;

    @Column(name = "FICHA_ZONA_PRIVADA")
    private Boolean fichaZonaPrivada;

    @Column(name = "RECIBE_CORREO")
    private Boolean recibeCorreo;

    @Column(name = "RECIBE_POSTAL")
    private Boolean recibePostal;

    private String movil;

    @Column(name = "prefijo_telefono")
    private String prefijoTelefono;

    private String tipo;

    @Column(name = "BUSQUEDA")
    private String busqueda;

    @Column(name = "FECHA_NACIMIENTO")
    private Date fechaNacimiento;

    private Long sexo;

    @Column(name = "NACIONALIDAD_ID")
    private String nacionalidadId;

    @Column(name = "PAIS_ID")
    private String paisPostalId;

    @Column(name = "PROVINCIA_ID")
    private Long provinciaPostalId;

    @Column(name = "PROVINCIA_NOMBRE")
    private String provinciaPostalNombre;

    @Column(name = "POBLACION_ID")
    private Long poblacionPostalId;

    @Column(name = "POBLACION_NOMBRE")
    private String poblacionPostalNombre;

    @Column(name = "CORREO_ESTADO_ID")
    private Long correoEstadoId;

    @Column(name = "MOVIL_ESTADO_ID")
    private Long movilEstadoId;

    @Column(name = "POSTAL_ESTADO_ID")
    private Long postalEstadoId;

    @Column(name = "ID")
    private Long clienteGeneral;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "PERSONA_NOMBRE")
    private String personaNombre;

    @Column(name = "SERVICIO_ID")
    private Long servicioId;

    @Column(name = "SERVICIO_NOMBRE")
    private String servicioNombre;

    @OneToOne
    @JoinColumn(name = "CLIENTE_ID")
    private Cliente cliente;

    @Column(name = "DEFINITIVO")
    private Boolean definitivo;

    public ClienteView() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getPostal() {
        return this.postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public Long getCorreoEstadoId() {
        return correoEstadoId;
    }

    public void setCorreoEstadoId(Long correoEstadoId) {
        this.correoEstadoId = correoEstadoId;
    }

    public Long getMovilEstadoId() {
        return movilEstadoId;
    }

    public void setMovilEstadoId(Long movilEstadoId) {
        this.movilEstadoId = movilEstadoId;
    }

    public Long getPostalEstadoId() {
        return postalEstadoId;
    }

    public Long getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(Long tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public void setPostalEstadoId(Long postalEstadoId) {
        this.postalEstadoId = postalEstadoId;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public String getNombreApellidos() {
        String nombre = (this.nombre != null) ? this.nombre + " " : "";
        String apellidos = (this.apellidos != null) ? this.apellidos : "";
        return nombre + apellidos;
    }

    public String getApellidosNombre() {
        String nombre = (this.nombre != null) ? this.nombre : "";
        String apellidos = (this.apellidos != null) ? this.apellidos : "";

        return (apellidos != "") ? apellidos + ", " + nombre : nombre;
    }

    public String getPrefijoTelefono() {
        return prefijoTelefono;
    }

    public void setPrefijoTelefono(String prefijoTelefono) {
        this.prefijoTelefono = prefijoTelefono;
    }
//
//    public String getCorreoOficial() {
//        return
//    }

//    public void setCorreoOficial(String correoOficial) {
//        this.correoOficial = correoOficial;
//    }

    public Long getClienteGeneral() {
        return clienteGeneral;
    }

    public void setClienteGeneral(Long clienteGeneral) {
        this.clienteGeneral = clienteGeneral;
    }

    public Long getServicioId() {
        return servicioId;
    }

    public void setServicioId(Long servicioId) {
        this.servicioId = servicioId;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }


    public Long getSexo() {
        return sexo;
    }

    public void setSexo(Long sexo) {
        this.sexo = sexo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

//    public String getPostalCompleto() {
//
//        String poblacion = "";
//        String provincia = "";
//        String pais = "";
//
//
//        if (ParamUtils.isNotNull(this.getPoblacionPostal())) poblacion = this.getPoblacionPostal().getNombreES();
//        if (ParamUtils.isNotNull(this.getProvinciaPostal())) provincia = this.getProvinciaPostal().getNombreES();
//        if (ParamUtils.isNotNull(this.getPaisPostal())) pais = this.getPaisPostal().getNombreES();
//
//
//        return this.getNombreApellidos() + "\n" + this.getPostal() + "\n" + this.getCodigoPostal() + " " + poblacion + ", " + provincia + "\n" + pais;
//    }

    public Boolean getFichaZonaPrivada() {
        return fichaZonaPrivada;
    }

    public void setFichaZonaPrivada(Boolean fichaZonaPrivada) {
        this.fichaZonaPrivada = fichaZonaPrivada;
    }

    public Boolean isFichaZonaPrivada() {
        return this.fichaZonaPrivada;
    }

    public String getNacionalidadId() {
        return nacionalidadId;
    }

    public void setNacionalidadId(String nacionalidadId) {
        this.nacionalidadId = nacionalidadId;
    }

    public String getPaisPostalId() {
        return paisPostalId;
    }

    public void setPaisPostalId(String paisPostalId) {
        this.paisPostalId = paisPostalId;
    }

    public Long getProvinciaPostalId() {
        return provinciaPostalId;
    }

    public void setProvinciaPostalId(Long provinciaPostalId) {
        this.provinciaPostalId = provinciaPostalId;
    }

    public Long getPoblacionPostalId() {
        return poblacionPostalId;
    }

    public void setPoblacionPostalId(Long poblacionPostalId) {
        this.poblacionPostalId = poblacionPostalId;
    }

    public String getProvinciaPostalNombre() {
        return provinciaPostalNombre;
    }

    public void setProvinciaPostalNombre(String provinciaPostalNombre) {
        this.provinciaPostalNombre = provinciaPostalNombre;
    }

    public String getPoblacionPostalNombre() {
        return poblacionPostalNombre;
    }

    public void setPoblacionPostalNombre(String poblacionPostalNombre) {
        this.poblacionPostalNombre = poblacionPostalNombre;
    }

    public String getServicioNombre() {
        return servicioNombre;
    }

    public void setServicioNombre(String servicioNombre) {
        this.servicioNombre = servicioNombre;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public String getPersonaNombre() {
        return personaNombre;
    }

    public void setPersonaNombre(String personaNombre) {
        this.personaNombre = personaNombre;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Boolean getDefinitivo() {
        return definitivo;
    }
    public Boolean isDefinitivo() {
        return definitivo;
    }

    public void setDefinitivo(Boolean definitivo) {
        this.definitivo = definitivo;
    }

    public Boolean getRecibeCorreo() {
        return recibeCorreo;
    }

    public Boolean isRecibeCorreo() {
        return recibeCorreo;
    }

    public void setRecibeCorreo(Boolean recibeCorreo) {
        this.recibeCorreo = recibeCorreo;
    }

    public Boolean getRecibePostal() {
        return recibePostal;
    }

    public Boolean isRecibePostal() {
        return recibePostal;
    }

    public void setRecibePostal(Boolean recibePostal) {
        this.recibePostal = recibePostal;
    }
}