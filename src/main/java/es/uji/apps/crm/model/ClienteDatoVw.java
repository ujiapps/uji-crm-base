package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_VW_CLIENTES_DATOS database view.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_VW_CLIENTES_DATOS")
public class ClienteDatoVw implements Serializable {

    @Id
    private Long id;

    @Column(name = "TIPO_DATO")
    private String tipoDato;

    @Column(name = "NOMBRE_DATO_CA")
    private String nombreDatoCa;

    @Column(name = "NOMBRE_DATO_ES")
    private String nombreDatoEs;

    @Column(name = "NOMBRE_DATO_UK")
    private String nombreDatoUk;

    @Column(name = "VALOR_CA")
    private String valorCa;

    @Column(name = "VALOR_ES")
    private String valorEs;

    @Column(name = "VALOR_UK")
    private String valorUk;

    @Column(name = "CLIENTE_ITEM_ID")
    private Long clienteItemId;

    @Column(name = "CAMPANYA_ID")
    private Long campanyaId;

    @Column(name = "CLIENTE_DATO_ID")
    private Long clienteDatoId;

    @Column(name = "OBSERVACIONES")
    private String observaciones;

    @Column(name = "FECHA_ULT_MOD")
    private Date fechaUltimaModificacion;


    // bi-directional many-to-one association to Cliente
    @ManyToOne
    @JoinColumn(name = "CLIENTE_ID")
    private Cliente cliente;

    public ClienteDatoVw() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreDatoCa() {
        return nombreDatoCa;
    }

    public void setNombreDatoCa(String nombreDatoCa) {
        this.nombreDatoCa = nombreDatoCa;
    }

    public String getNombreDatoEs() {
        return nombreDatoEs;
    }

    public void setNombreDatoEs(String nombreDatoEs) {
        this.nombreDatoEs = nombreDatoEs;
    }

    public String getNombreDatoUk() {
        return nombreDatoUk;
    }

    public void setNombreDatoUk(String nombreDatoUk) {
        this.nombreDatoUk = nombreDatoUk;
    }

    public String getValorCa() {
        return valorCa;
    }

    public void setValorCa(String valorCa) {
        this.valorCa = valorCa;
    }

    public String getValorEs() {
        return valorEs;
    }

    public void setValorEs(String valorEs) {
        this.valorEs = valorEs;
    }

    public String getValorUk() {
        return valorUk;
    }

    public void setValorUk(String valorUk) {
        this.valorUk = valorUk;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getTipoDato() {
        return tipoDato;
    }

    public void setTipoDato(String tipoDato) {
        this.tipoDato = tipoDato;
    }

    public Long getClienteItemId() {
        return clienteItemId;
    }

    public void setClienteItemId(Long clienteItemId) {
        this.clienteItemId = clienteItemId;
    }

    public Long getCampanyaId() {
        return campanyaId;
    }

    public void setCampanyaId(Long campanyaId) {
        this.campanyaId = campanyaId;
    }

    public Long getClienteDatoId() {
        return clienteDatoId;
    }

    public void setClienteDatoId(Long clienteDatoId) {
        this.clienteDatoId = clienteDatoId;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }
}