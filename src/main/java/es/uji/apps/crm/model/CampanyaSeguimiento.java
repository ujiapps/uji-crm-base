package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CAMPANYAS_SEGUIMIENTOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_SEGUIMIENTOS")
public class CampanyaSeguimiento implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String descripcion;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    private String nombre;


    // bi-directional many-to-one association to Envio
    @OneToMany(mappedBy = "seguimiento")
    private Set<Envio> envios;

    // bi-directional many-to-one association to CampanyaFase
    @ManyToOne
    @JoinColumn(name = "FASE_CAMPANYA_ID")
    private CampanyaFase campanyaFase;

    // bi-directional many-to-one association to SeguimientoTipo
    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private SeguimientoTipo seguimientoTipo;

    // bi-directional many-to-one association to SeguimientoFichero
    @OneToMany(mappedBy = "campanyaSeguimiento")
    private Set<SeguimientoFichero> seguimientoFicheros;

    public CampanyaSeguimiento() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public CampanyaFase getCampanyaFase() {
        return this.campanyaFase;
    }

    public void setCampanyaFase(CampanyaFase campanyaFase) {
        this.campanyaFase = campanyaFase;
    }

    public SeguimientoTipo getSeguimientoTipo() {
        return this.seguimientoTipo;
    }

    public void setSeguimientoTipo(SeguimientoTipo seguimientoTipo) {
        this.seguimientoTipo = seguimientoTipo;
    }

    public Set<Envio> getEnvios() {
        return this.envios;
    }

    public void setEnvios(Set<Envio> envios) {
        this.envios = envios;
    }

    public Set<SeguimientoFichero> getSeguimientoFicheros() {
        return seguimientoFicheros;
    }

    public void setSeguimientoFicheros(Set<SeguimientoFichero> seguimientoFicheros) {
        this.seguimientoFicheros = seguimientoFicheros;
    }

}