package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_TARIFAS_TIPOS_ENVIOS_PROG")
public class TipoTarifaEnvioProgramado implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "tarifa_tipo_envio_id")
    private TipoTarifaEnvio tarifaTipoEnvio;

    @Column(name = "tipo_fecha")
    private Long tipoFecha;

    @Column(name = "dias")
    private Long dias;

    @Column(name = "cuando_realizar_envio")
    private Long cuandoRealizarEnvio;

    public TipoTarifaEnvioProgramado() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoTarifaEnvio getTarifaTipoEnvio() {
        return tarifaTipoEnvio;
    }

    public void setTarifaTipoEnvio(TipoTarifaEnvio tarifaTipoEnvio) {
        this.tarifaTipoEnvio = tarifaTipoEnvio;
    }


    public Long getDias() {
        return dias;
    }

    public void setDias(Long dias) {
        this.dias = dias;
    }

    public Long getTipoFecha() {
        return tipoFecha;
    }

    public void setTipoFecha(Long tipoFecha) {
        this.tipoFecha = tipoFecha;
    }

    public Long getCuandoRealizarEnvio() {
        return cuandoRealizarEnvio;
    }

    public void setCuandoRealizarEnvio(Long cuandoRealizarEnvio) {
        this.cuandoRealizarEnvio = cuandoRealizarEnvio;
    }


}