package es.uji.apps.crm.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class FiltroCliente {
    private String busquedaGenerica;
    private String etiqueta;
    private List<Long> items;
    private Long itemsOpcion;
    private Long campanyaId;
    private Long campanyaClienteTipoId;
    private Long clienteId;
    private String tipoCliente;
    private ClienteDatoTipo tipoDato;
    private String valorDato;
    private String correo;
    private String postal;
    private String movil;
    private Long campanyaTipoEstado;
    private Long programa;
    private Long grupo;
    private Long item;
    private String nacionalidad;
    private String codigoPostal;
    private String paisDomicilio;
    private Long provinciaDomicilio;
    private Long poblacionDomiclio;
    private Long estudioUJI;
    private List<Long> estudioUJIBeca;
    private Long estudioNoUJI;
    // private Long seguimientoTipo;
    private List<Long> seguimientoTipos;
    private Long seguimientoTipoOpcion;
    private Long tipoComparacion;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaMax;

    private Long tipoFecha;
    private Long mes;
    private Long mesMax;
    private Long anyo;
    private Long anyoMax;

    private String columnaOrdenacion;
    private String tipoOrdenacion;

    public FiltroCliente() {
    }

    public String getBusquedaGenerica() {
        return busquedaGenerica;
    }

    public void setBusquedaGenerica(String busquedaGenerica) {
        this.busquedaGenerica = busquedaGenerica;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public List<Long> getItems() {
        return items;
    }

    public void setItems(List<Long> items) {
        this.items = items;
    }

    public void setItems(String itemSeparadosPorPuntoyComa) {
        this.items = parseStringAsList(itemSeparadosPorPuntoyComa);
    }

    List<Long> parseStringAsList(String cadenaSeparadaPorComas) {
        List<Long> elementosList = new ArrayList<Long>();

        if (cadenaSeparadaPorComas == null)
        {
            return elementosList;
        }

        String[] elementos = cadenaSeparadaPorComas.split(";");

        for (String elemento : elementos)
        {
            elemento = elemento.trim();
            if (!elemento.isEmpty())
            {
                elementosList.add(Long.parseLong(elemento));
            }
        }
        return elementosList;
    }

    public Long getCampanyaId() {
        return campanyaId;
    }

    public void setCampanyaId(Long campanyaId) {
        this.campanyaId = campanyaId;
    }

    public Long getCampanyaClienteTipoId() {
        return campanyaClienteTipoId;
    }

    public void setCampanyaClienteTipoId(Long campanyaClienteTipoId) {
        this.campanyaClienteTipoId = campanyaClienteTipoId;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public ClienteDatoTipo getTipoDato() {
        return tipoDato;
    }

    public void setTipoDato(ClienteDatoTipo tipoDato) {
        this.tipoDato = tipoDato;
    }

    public String getValorDato() {
        return valorDato;
    }

    public void setValorDato(String valorDato) {
        this.valorDato = valorDato;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public Long getCampanyaTipoEstado() {
        return campanyaTipoEstado;
    }

    public void setCampanyaTipoEstado(Long campanyaTipoEstado) {
        this.campanyaTipoEstado = campanyaTipoEstado;
    }

    public Long getPrograma() {
        return programa;
    }

    public void setPrograma(Long programa) {
        this.programa = programa;
    }

    public Long getGrupo() {
        return grupo;
    }

    public void setGrupo(Long grupo) {
        this.grupo = grupo;
    }

    public Long getItem() {
        return item;
    }

    public void setItem(Long item) {
        this.item = item;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public Long getSeguimientoTipoOpcion() {
        return seguimientoTipoOpcion;
    }

    public void setSeguimientoTipoOpcion(Long seguimientoTipoOpcion) {
        this.seguimientoTipoOpcion = seguimientoTipoOpcion;
    }

    public List<Long> getSeguimientoTipos() {
        return seguimientoTipos;
    }

    public void setSeguimientoTipos(List<Long> seguimientoTipos) {
        this.seguimientoTipos = seguimientoTipos;
    }

    public void setSeguimientoTipos(String seguimientoTiposSeparadosPorPuntoyComa) {
        this.seguimientoTipos = parseStringAsList(seguimientoTiposSeparadosPorPuntoyComa);
    }

    public Long getItemsOpcion() {
        return itemsOpcion;
    }

    public void setItemsOpcion(Long itemsOpcion) {
        this.itemsOpcion = itemsOpcion;
    }

    public Long getTipoComparacion() {
        return tipoComparacion;
    }

    public void setTipoComparacion(Long tipoComparacion) {
        this.tipoComparacion = tipoComparacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaMax() {
        return fechaMax;
    }

    public void setFechaMax(Date fechaMax) {
        this.fechaMax = fechaMax;
    }

    public Long getTipoFecha() {
        return tipoFecha;
    }

    public void setTipoFecha(Long tipoFecha) {
        this.tipoFecha = tipoFecha;
    }

    public Long getMes() {
        return mes;
    }

    public void setMes(Long mes) {
        this.mes = mes;
    }

    public Long getAnyo() {
        return anyo;
    }

    public void setAnyo(Long anyo) {
        this.anyo = anyo;
    }

    public Long getMesMax() {
        return mesMax;
    }

    public void setMesMax(Long mesMax) {
        this.mesMax = mesMax;
    }

    public Long getAnyoMax() {
        return anyoMax;
    }

    public void setAnyoMax(Long anyoMax) {
        this.anyoMax = anyoMax;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getPaisDomicilio() {
        return paisDomicilio;
    }

    public void setPaisDomicilio(String paisDomicilio) {
        this.paisDomicilio = paisDomicilio;
    }

    public Long getProvinciaDomicilio() {
        return provinciaDomicilio;
    }

    public void setProvinciaDomicilio(Long provinciaDomicilio) {
        this.provinciaDomicilio = provinciaDomicilio;
    }

    public Long getPoblacionDomiclio() {
        return poblacionDomiclio;
    }

    public void setPoblacionDomiclio(Long poblacionDomiclio) {
        this.poblacionDomiclio = poblacionDomiclio;
    }

    public Long getEstudioUJI() {
        return estudioUJI;
    }

    public void setEstudioUJI(Long estudioUJI) {
        this.estudioUJI = estudioUJI;
    }

    public List<Long> getEstudioUJIBeca() {
        return estudioUJIBeca;
    }

    public void setEstudioUJIBeca(List<Long> estudioUJIBeca) {
        this.estudioUJIBeca = estudioUJIBeca;
    }

    public Long getEstudioNoUJI() {
        return estudioNoUJI;
    }

    public void setEstudioNoUJI(Long estudioNoUJI) {
        this.estudioNoUJI = estudioNoUJI;
    }

    public String getColumnaOrdenacion() {
        return columnaOrdenacion;
    }

    public void setColumnaOrdenacion(String columnaOrdenacion) {
        this.columnaOrdenacion = columnaOrdenacion;
    }

    public String getTipoOrdenacion() {
        return tipoOrdenacion;
    }

    public void setTipoOrdenacion(String tipoOrdenacion) {
        this.tipoOrdenacion = tipoOrdenacion;
    }
}