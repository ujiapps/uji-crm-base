package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import es.uji.commons.rest.ParamUtils;

/**
 * The persistent class for the CRM_GRUPOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_GRUPOS")
public class Grupo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String asunto;

    private String desde;

    private String firma;

    @Column(name = "NOMBRE_CA")
    private String nombreCa;

    @Column(name = "NOMBRE_ES")
    private String nombreEs;

    @Column(name = "NOMBRE_UK")
    private String nombreUk;

    @Column(name = "DESCRIPCION_CA")
    private String descripcionCa;

    @Column(name = "DESCRIPCION_ES")
    private String descripcionEs;

    @Column(name = "DESCRIPCION_UK")
    private String descripcionUk;

    private Integer orden;

    @Column(name = "RESPONDER_A")
    private String responderA;

    private Boolean visible;

    // bi-directional many-to-one association to Programa
    @ManyToOne
    private Programa programa;

    // bi-directional many-to-one association to TipoAcceso
    @ManyToOne
    @JoinColumn(name = "TIPO_ACCESO_ID")
    private TipoAcceso tipoAcceso;

    // bi-directional many-to-one association to Item
    @OneToMany(mappedBy = "grupo")
    private Set<Item> items;


    @OneToMany(mappedBy = "grupo")
    private Set<CampanyaDato> campanyaDatos;

    public Grupo() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAsunto() {
        return this.asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getDesde() {
        return this.desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getFirma() {
        return this.firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public Integer getOrden() {
        return this.orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public String getResponderA() {
        return this.responderA;
    }

    public void setResponderA(String responderA) {
        this.responderA = responderA;
    }

    public Boolean getVisible() {
        return this.visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public TipoAcceso getTipoAcceso() {
        return this.tipoAcceso;
    }

    public void setTipoAcceso(TipoAcceso tipoAcceso) {
        this.tipoAcceso = tipoAcceso;
    }

    public Programa getPrograma() {
        return this.programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public Set<Item> getItems() {
        return this.items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public Set<CampanyaDato> getCampanyaDatos() {
        return campanyaDatos;
    }

    public void setCampanyaDatos(Set<CampanyaDato> campanyaDatos) {
        this.campanyaDatos = campanyaDatos;
    }

    public String getNombre(String idioma) {
        if (idioma == null)
        {
            idioma = "CA";
        }
        switch (idioma.toUpperCase())
        {
            case "ES":
                if (ParamUtils.isNotNull(getNombreEs()))
                {
                    return getNombreEs();
                }
                else
                {
                    return getNombreCa();
                }
            case "UK":
            case "EN":
                if (ParamUtils.isNotNull(getNombreUk()))
                {
                    return getNombreUk();
                }
                else
                {
                    return getNombreCa();
                }
            default:
                return getNombreCa();
        }
    }

    public String getNombreCa() {
        return this.nombreCa;
    }

    public void setNombreCa(String nombreCa) {
        this.nombreCa = nombreCa;
    }

    public String getNombreEs() {
        return this.nombreEs;
    }

    public void setNombreEs(String nombreEs) {
        this.nombreEs = nombreEs;
    }

    public String getNombreUk() {
        return this.nombreUk;
    }

    public void setNombreUk(String nombreUk) {
        this.nombreUk = nombreUk;
    }

    public String getDescripcionByIdioma(String idioma) {
        if (idioma == null)
        {
            idioma = "CA";
        }
        switch (idioma.toUpperCase())
        {
            case "ES":
                return getDescripcionEs();
            case "UK":
            case "EN":
                return getDescripcionUk();
            default:
                return getDescripcionCa();
        }
    }

    public String getDescripcionCa() {
        return descripcionCa;
    }

    public void setDescripcionCa(String descripcionCa) {
        this.descripcionCa = descripcionCa;
    }

    public String getDescripcionEs() {
        return descripcionEs;
    }

    public void setDescripcionEs(String descripcionEs) {
        this.descripcionEs = descripcionEs;
    }

    public String getDescripcionUk() {
        return descripcionUk;
    }

    public void setDescripcionUk(String descripcionUk) {
        this.descripcionUk = descripcionUk;
    }

}