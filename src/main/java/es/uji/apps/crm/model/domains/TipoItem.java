package es.uji.apps.crm.model.domains;

public enum TipoItem {
    NO_ACTIU(14L, "No actiu"), MODIFICABLE(15L, "Modificable"), NO_MODIFICABLE(16L,
            "No modificable"), MODIFICABLE_DEFECTO(17L, "Modificable + Defecto"), NO_MODIFICABLE_DEFECTO(
            18L, "No Modificable + Defecto");
    private final Long id;
    private final String nombre;

    TipoItem(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

}