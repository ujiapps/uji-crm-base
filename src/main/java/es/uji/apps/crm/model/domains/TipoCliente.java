package es.uji.apps.crm.model.domains;

public enum TipoCliente {
    FISICA("F", "Persona fisica"), JURIDICA("J", "Empresa");

    private final String id;
    private final String nombre;

    TipoCliente(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

}