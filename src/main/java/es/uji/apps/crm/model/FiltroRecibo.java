package es.uji.apps.crm.model;

import java.util.List;

public class FiltroRecibo {

    private List<FiltroFechas> filtroFechas;
    private List<FiltroCampanya> filtroCampanyas;
    private List<FiltroEstadoRecibo> filtroEstadoRecibos;
    private List<FiltroExportacion> filtroExportaciones;

    public FiltroRecibo() {
    }

    public List<FiltroCampanya> getFiltroCampanyas() {
        return filtroCampanyas;
    }

    public void setFiltroCampanyas(List<FiltroCampanya> filtroCampanyas) {
        this.filtroCampanyas = filtroCampanyas;
    }

    public List<FiltroFechas> getFiltroFechas() {
        return filtroFechas;
    }

    public void setFiltroFechas(List<FiltroFechas> filtroFechas) {
        this.filtroFechas = filtroFechas;
    }

    public List<FiltroEstadoRecibo> getFiltroEstadoRecibos() {
        return filtroEstadoRecibos;
    }

    public void setFiltroEstadoRecibos(List<FiltroEstadoRecibo> filtroEstadoRecibos) {
        this.filtroEstadoRecibos = filtroEstadoRecibos;
    }

    public List<FiltroExportacion> getFiltroExportaciones() {
        return filtroExportaciones;
    }

    public void setFiltroExportaciones(List<FiltroExportacion> filtroExportaciones) {
        this.filtroExportaciones = filtroExportaciones;
    }
}