package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CAMPANYAS database table.
 */

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_V_PER_PROGRAMAS_CURSOS")
public class ProgramaPersonaCurso implements Serializable {
    @Id
    private Long id;

    @ManyToOne
    @JoinColumn(name = "uest_id")
    private Ubicacion uest;

    @Column(name = "PERSONA_ID")
    private Long persona;


}