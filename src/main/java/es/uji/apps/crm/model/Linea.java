package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_EXT_PERSONAS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_EXT_LINEAS")
public class Linea implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @Column(name = "IMPORTE_NETO")
    private Integer importeNeto;

    @Column(name = "IMPORTE_BRUTO")
    private Integer importeBruto;

    private String origen;

    @Column(name = "REFERENCIA1_ID")
    private Integer referencia1;

    @Column(name = "REFERENCIA2_ID")
    private Integer referencia2;

    private String observaciones;

    private String concepto;

    @Column(name = "LINEA_FINANCIACION")
    private String lineaFinanciacion;

    @Column(name = "DESCUENTO_TXT")
    private String descuento;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private Persona persona;

    @ManyToOne
    @JoinColumn(name = "RECIBO_ID")
    private Recibo recibo;

    public Linea() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getImporteNeto() {
        return importeNeto;
    }

    public void setImporteNeto(Integer importeNeto) {
        this.importeNeto = importeNeto;
    }

    public Integer getImporteBruto() {
        return importeBruto;
    }

    public void setImporteBruto(Integer importeBruto) {
        this.importeBruto = importeBruto;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Integer getReferencia1() {
        return referencia1;
    }

    public void setReferencia1(Integer referencia1) {
        this.referencia1 = referencia1;
    }

    public Integer getReferencia2() {
        return referencia2;
    }

    public void setReferencia2(Integer referencia2) {
        this.referencia2 = referencia2;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getLineaFinanciacion() {
        return lineaFinanciacion;
    }

    public void setLineaFinanciacion(String lineaFinanciacion) {
        this.lineaFinanciacion = lineaFinanciacion;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Recibo getRecibo() {
        return recibo;
    }

    public void setRecibo(Recibo recibo) {
        this.recibo = recibo;
    }


}