package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_SERVICIOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_SERVICIOS")
public class Servicio implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    // bi-directional many-to-one association to Programa
    @OneToMany(mappedBy = "servicio")
    private Set<Programa> programas;

    // bi-directional many-to-one association to ServicioUsuario
    @OneToMany(mappedBy = "servicio")
    private Set<ServicioUsuario> serviciosUsuario;

    @OneToMany(mappedBy = "servicio")
    private Set<LineaFacturacion> lineasFacturacion;

    @OneToMany(mappedBy = "servicio")
    private Set<Cliente> clientes;

    public Servicio() {
    }

    public Servicio(Long servicio) {
        this.id = servicio;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Programa> getProgramas() {
        return this.programas;
    }

    public void setProgramas(Set<Programa> programas) {
        this.programas = programas;
    }

    public Set<ServicioUsuario> getServiciosUsuario() {
        return serviciosUsuario;
    }

    public void setServiciosUsuario(Set<ServicioUsuario> serviciosUsuario) {
        this.serviciosUsuario = serviciosUsuario;
    }

    public Set<LineaFacturacion> getLineasFacturacion() {
        return lineasFacturacion;
    }

    public void setLineasFacturacion(Set<LineaFacturacion> lineasFacturacion) {
        this.lineasFacturacion = lineasFacturacion;
    }

    public Set<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(Set<Cliente> clientes) {
        this.clientes = clientes;
    }
}