package es.uji.apps.crm.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name = "CRM_CORREOS_NO_VALIDOS_CONTROL")
public class CorreoNoValidoControl {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    @JoinColumn(name = "CORREO_NO_VALIDO_ID")
    private VwCorreoNoValido correoNoValido;

    private Boolean modificado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public VwCorreoNoValido getCorreoNoValido() {
        return correoNoValido;
    }

    public void setCorreoNoValido(VwCorreoNoValido correoNoValido) {
        this.correoNoValido = correoNoValido;
    }
}