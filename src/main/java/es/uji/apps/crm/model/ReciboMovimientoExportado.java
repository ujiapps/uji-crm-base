package es.uji.apps.crm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CRM_RECIBOS_MOVIMIENTOS_EXPORT", schema = "UJI_CRM")
public class ReciboMovimientoExportado {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RECIBO_MOVIMIENTO_ID")
    private MovimientoRecibo reciboMovimiento;


    @ManyToOne
    @JoinColumn(name = "REMESA_ID")
    private ReciboRemesaExportado remesa;

    @Column(name = "VALIDO")
    private Boolean valido;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MovimientoRecibo getReciboMovimiento() {
        return reciboMovimiento;
    }

    public void setReciboMovimiento(MovimientoRecibo reciboMovimiento) {
        this.reciboMovimiento = reciboMovimiento;
    }

    public ReciboRemesaExportado getRemesa() {
        return remesa;
    }

    public void setRemesa(ReciboRemesaExportado remesa) {
        this.remesa = remesa;
    }

    public Boolean getValido() {
        return valido;
    }

    public void setValido(Boolean valido) {
        this.valido = valido;
    }


}