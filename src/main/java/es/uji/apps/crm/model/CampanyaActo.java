package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CAMPANYAS_ACTOS database table.
 */

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_ACTOS")
public class CampanyaActo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "IMAGEN_NOMBRE")
    private String imagenNombre;
    @Column(name = "IMAGEN_REFERENCIA")
    private String imagenReferencia;
    @Column(name = "IMAGEN_TIPO")
    private String imagenTipo;

    @Column(name = "IMAGEN_PUBLI_NOMBRE")
    private String imagenPubliNombre;
    @Column(name = "IMAGEN_PUBLI_REFERENCIA")
    private String imagenPubliReferencia;
    @Column(name = "IMAGEN_PUBLI_TIPO")
    private String imagenPubliTipo;

    private String titulo;
    private String resumen;
    private Long duracion;
    private String contenido;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    private String hora;
    @Column(name = "HORA_APERTURA")
    private String horaApertura;

    @Column(name = "RSS_ACTIVO")
    private Boolean rssActivo;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private Campanya campanya;

    public CampanyaActo() {
    }

    public CampanyaActo(Campanya campanya) {
        this.setCampanya(campanya);
        this.setContenido("MODIFICAR -- Contenido acto");
        this.setDuracion(90L);
        this.setFecha(new Date());
        this.setHora("19:30");
        this.setHoraApertura("19:00");
        this.setResumen("MODIFICAR -- Resumen acto");
        this.setRssActivo(Boolean.FALSE);
        this.setTitulo("MODIFICAR -- Titulo acto");

        this.setImagenPubliNombre("footer.jpeg");
        this.setImagenPubliReferencia("P7KO5SEGKSQRCQQB0IFU1NUMEZ53E66X");
        this.setImagenPubliTipo("image/jpg");

        this.setImagenNombre("Logo.png");
        this.setImagenReferencia("VVEMSFGQF899X72NATKTGC3EX7OSJ06Y");
        this.setImagenTipo("image/png");
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImagenNombre() {
        return imagenNombre;
    }

    public void setImagenNombre(String imagenNombre) {
        this.imagenNombre = imagenNombre;
    }

    public String getImagenReferencia() {
        return imagenReferencia;
    }

    public void setImagenReferencia(String imagenReferencia) {
        this.imagenReferencia = imagenReferencia;
    }

    public String getImagenTipo() {
        return imagenTipo;
    }

    public void setImagenTipo(String imagenTipo) {
        this.imagenTipo = imagenTipo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public Long getDuracion() {
        return duracion;
    }

    public void setDuracion(Long duracion) {
        this.duracion = duracion;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public Boolean getRssActivo() {
        return rssActivo;
    }

    public void setRssActivo(Boolean rssActivo) {
        this.rssActivo = rssActivo;
    }

    public Campanya getCampanya() {
        return campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

    public String getHoraApertura() {
        return horaApertura;
    }

    public void setHoraApertura(String horaApertura) {
        this.horaApertura = horaApertura;
    }

    public String getImagenPubliNombre() {
        return imagenPubliNombre;
    }

    public void setImagenPubliNombre(String imagenPubliNombre) {
        this.imagenPubliNombre = imagenPubliNombre;
    }

    public String getImagenPubliReferencia() {
        return imagenPubliReferencia;
    }

    public void setImagenPubliReferencia(String imagenPubliReferencia) {
        this.imagenPubliReferencia = imagenPubliReferencia;
    }

    public String getImagenPubliTipo() {
        return imagenPubliTipo;
    }

    public void setImagenPubliTipo(String imagenPubliTipo) {
        this.imagenPubliTipo = imagenPubliTipo;
    }
}