package es.uji.apps.crm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import java.util.Date;

@SuppressWarnings("serial")
@Component
@Table(name = "CRM_VW_CAMP_CLIENTE_BUSQUEDA", schema = "UJI_CRM")
@Entity
public class CampanyaClienteBusqueda {

    @Column(name = "CAMPANYA_CLIENTE_ID")
    @Id
    private Long Id;

    private String estado;
    @Column(name = "APELLIDOS_OFICIAL")
    private String apellidosOficial;
    private String busqueda;
    private String correo;
    private String identificacion;
    private Long movil;
    @Column(name = "NOMBRE_OFICIAL")
    private String nombreOficial;
    @Column(name = "APELLIDOS_NOMBRE")
    private String apellidosNombre;
    private String postal;

    @Column(name = "DEFINITIVO")
    private Boolean definitivo;

    @Column(name = "CAMPANYA_DESCRIPCION")
    private String campanyaDescripcion;
    @Column(name = "CAMPANYA_NOMBRE")
    private String campanyaNombre;
    private String etiquetas;
    @Column(name = "CLIENTE_TIPO")
    private String clienteTipo;
    @Column(name="FECHA_ESTADO")
    private Date fechaEstado;

    @Column(name = "NUM_SEGUIMIENTOS")
    private Long numSeguimientos;

    @ManyToOne
    @JoinColumn(name = "CLIENTE_ID")
    private Cliente cliente;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private Campanya campanya;

    @ManyToOne
    @JoinColumn(name = "ESTADO_CAMPANYA_ID")
    private TipoEstadoCampanya estadoCampanya;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private Persona persona;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_PADRE_ID")
    private Campanya campanyaPadre;

    @ManyToOne
    @JoinColumn(name = "PROGRAMA_ID")
    private Programa programa;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getApellidosOficial() {
        return apellidosOficial;
    }

    public void setApellidosOficial(String apellidosOficial) {
        this.apellidosOficial = apellidosOficial;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Long getMovil() {
        return movil;
    }

    public void setMovil(Long movil) {
        this.movil = movil;
    }

    public String getNombreOficial() {
        return nombreOficial;
    }

    public void setNombreOficial(String nombreOficial) {
        this.nombreOficial = nombreOficial;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getCampanyaDescripcion() {
        return campanyaDescripcion;
    }

    public void setCampanyaDescripcion(String campanyaDescripcion) {
        this.campanyaDescripcion = campanyaDescripcion;
    }

    public String getCampanyaNombre() {
        return campanyaNombre;
    }

    public void setCampanyaNombre(String campanyaNombre) {
        this.campanyaNombre = campanyaNombre;
    }

    public String getEtiquetas() {
        return etiquetas;
    }

    public void setEtiquetas(String etiquetas) {
        this.etiquetas = etiquetas;
    }

    public Long getNumSeguimientos() {
        return numSeguimientos;
    }

    public void setNumSeguimientos(Long numSeguimientos) {
        this.numSeguimientos = numSeguimientos;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Campanya getCampanya() {
        return campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

    public TipoEstadoCampanya getEstadoCampanya() {
        return estadoCampanya;
    }

    public void setEstadoCampanya(TipoEstadoCampanya estadoCampanya) {
        this.estadoCampanya = estadoCampanya;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getClienteTipo() {
        return clienteTipo;
    }

    public void setClienteTipo(String clienteTipo) {
        this.clienteTipo = clienteTipo;
    }

    public Campanya getCampanyaPadre() {
        return campanyaPadre;
    }

    public void setCampanyaPadre(Campanya campanyaPadre) {
        this.campanyaPadre = campanyaPadre;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public Date getFechaEstado() {
        return fechaEstado;
    }

    public void setFechaEstado(Date fechaEstado) {
        this.fechaEstado = fechaEstado;
    }

    public String getApellidosNombre() {
        return apellidosNombre;
    }

    public void setApellidosNombre(String apellidosNombre) {
        this.apellidosNombre = apellidosNombre;
    }

    public Boolean getDefinitivo() {
        return definitivo;
    }
    public Boolean isDefinitivo() {
        return definitivo;
    }

    public void setDefinitivo(Boolean definitivo) {
        this.definitivo = definitivo;
    }
}