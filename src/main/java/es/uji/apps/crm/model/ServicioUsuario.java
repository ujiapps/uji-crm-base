package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_SERVICIOS_USUARIOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_SERVICIOS_USUARIOS")
public class ServicioUsuario implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // bi-directional many-to-one association to Persona
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaPasPdi personaPasPdi;

    // bi-directional many-to-one association to Servicio
    @ManyToOne
    private Servicio servicio;

//    private static ServicioUsuarioDAO servicioUsuarioDAO;

//    @Autowired
//    public void setServicioUsuarioDAO(ServicioUsuarioDAO servicioUsuarioDAO)
//    {
//        ServicioUsuario.servicioUsuarioDAO = servicioUsuarioDAO;
//    }

    public ServicioUsuario() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Servicio getServicio() {
        return this.servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public PersonaPasPdi getPersonaPasPdi() {
        return personaPasPdi;
    }

    public void setPersonaPasPdi(PersonaPasPdi personaPasPdi) {
        this.personaPasPdi = personaPasPdi;
    }
}