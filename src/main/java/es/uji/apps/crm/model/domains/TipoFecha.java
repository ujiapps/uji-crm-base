package es.uji.apps.crm.model.domains;

public enum TipoFecha {
    MENSUAL(1L, "Mes"), ANUAL(2L, "Año"), COMPLETA(3L, "Completa");
    private final Long id;
    private final String nombre;

    TipoFecha(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}