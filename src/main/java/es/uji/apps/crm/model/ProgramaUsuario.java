package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_PROGRAMAS_USUARIOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_PROGRAMAS_USUARIOS")
public class ProgramaUsuario implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String tipo;

    // bi-directional many-to-one association to PersonaPasPdi
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaPasPdi personaPasPdi;

    // bi-directional many-to-one association to Programa
    @ManyToOne
    @JoinColumn(name = "PROGRAMA_ID")
    private Programa programa;

    // private static ProgramaUsuarioDAO programaUsuarioDAO;

    // @Autowired
    // public void setProgramaUsuarioDAO(ProgramaUsuarioDAO programaUsuarioDAO)
    // {
    // ProgramaUsuario.programaUsuarioDAO = programaUsuarioDAO;
    // }

    public ProgramaUsuario() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public PersonaPasPdi getPersonaPasPdi() {
        return this.personaPasPdi;
    }

    public void setPersonaPasPdi(PersonaPasPdi personaPasPdi) {
        this.personaPasPdi = personaPasPdi;
    }

    public Programa getPrograma() {
        return this.programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }
}