package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_ETIQUETAS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_ETIQUETAS")
public class Etiqueta implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    @Column(insertable = false, updatable = false)
    private String busqueda;

    // bi-directional many-to-one association to ClienteEtiqueta
    @OneToMany(mappedBy = "etiqueta")
    private Set<ClienteEtiqueta> clientesEtiquetas;

    // bi-directional many-to-one association to Etiqueta
    @ManyToOne
    private Etiqueta etiqueta;

    // bi-directional many-to-one association to Etiqueta
    @OneToMany(mappedBy = "etiqueta")
    private Set<Etiqueta> etiquetas;

    public Etiqueta() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public Set<ClienteEtiqueta> getClientesEtiquetas() {
        return this.clientesEtiquetas;
    }

    public void setClientesEtiquetas(Set<ClienteEtiqueta> clientesEtiquetas) {
        this.clientesEtiquetas = clientesEtiquetas;
    }

    public Etiqueta getEtiqueta() {
        return this.etiqueta;
    }

    public void setEtiqueta(Etiqueta etiqueta) {
        this.etiqueta = etiqueta;
    }

    public Set<Etiqueta> getEtiquetas() {
        return this.etiquetas;
    }

    public void setEtiquetas(Set<Etiqueta> etiquetas) {
        this.etiquetas = etiquetas;
    }

}