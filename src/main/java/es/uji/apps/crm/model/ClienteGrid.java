package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CLIENTES database table.
 */

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_VW_CLIENTES_GRID")

public class ClienteGrid implements Serializable {

    @Id
    private Long id;

    @Id
    @Column(name = "CLIENTE_ID")
    private Long clienteId;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    private String correo;

    @Column(name = "CORREO_OFICIAL")
    private String correoOficial;

    private String identificacion;

    @Column(name = "TIPO_IDENTIFICACION")
    private Long tipoIdentificacion;

    @Column(name = "PERSONA_UJI_NOMBRE")
    private String personaUjiNombre;

    private String busqueda;

    @Column(name = "NOMBRE_COMPLETO")
    private String nombreCompleto;

    private String movil;

    @Column(name = "SERVICIO_ID")
    private Long servicio;

    @Column(name = "DEFINITIVO")
    private Boolean definitivo;

    @Column(name="TIPO")
    private String tipo;

    public ClienteGrid() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public Long getServicio() {
        return servicio;
    }

    public void setServicio(Long servicio) {
        this.servicio = servicio;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Long getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(Long tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getPersonaUjiNombre() {
        return personaUjiNombre;
    }

    public void setPersonaUjiNombre(String personaUjiNombre) {
        this.personaUjiNombre = personaUjiNombre;
    }

    public Boolean getDefinitivo() {
        return definitivo;
    }
    public Boolean isDefinitivo() {
        return definitivo;
    }

    public void setDefinitivo(Boolean definitivo) {
        this.definitivo = definitivo;
    }

    public String getCorreoOficial() {
        return correoOficial;
    }

    public void setCorreoOficial(String correoOficial) {
        this.correoOficial = correoOficial;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}