package es.uji.apps.crm.model.domains;

public enum TipoSolicitudCampo {
    NO_SOLICITAR(0L, "No sol.licitar"), OPCIONAL(1L, "Opcional"), OBLIGATORIO_NO_MODIFICABLE(2L, "Obligatori no modificable"), OBLIGATORIO_MODIFICABLE(3L, "Obligatori modificable");

    private final Long id;
    private final String nombre;

    TipoSolicitudCampo(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

}