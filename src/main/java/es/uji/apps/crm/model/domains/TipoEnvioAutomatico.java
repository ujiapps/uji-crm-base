package es.uji.apps.crm.model.domains;

public enum TipoEnvioAutomatico {
    ALTA(32L, "Benvinguda"), BAIXA(33L, "Baixa");

    private final Long id;
    private final String nombre;

    TipoEnvioAutomatico(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

}