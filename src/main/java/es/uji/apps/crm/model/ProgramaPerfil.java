package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_PROGRAMAS_PERFILES database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_PROGRAMAS_PERFILES")
public class ProgramaPerfil implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "TIPO_ACCESO")
    private String tipoAcceso;

    // bi-directional many-to-one association to Perfil
    @ManyToOne
    private Perfil perfil;

    // bi-directional many-to-one association to Programa
    @ManyToOne
    private Programa programa;

    public ProgramaPerfil() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoAcceso() {
        return this.tipoAcceso;
    }

    public void setTipoAcceso(String tipoAcceso) {
        this.tipoAcceso = tipoAcceso;
    }

    public Perfil getPerfil() {
        return this.perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public Programa getPrograma() {
        return this.programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

}