package es.uji.apps.crm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name = "CRM_ITEMS_CLASIFICACION")
public class ItemClasificacion {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    //    @ManyToOne
//    @JoinColumn(name = "CLASIFICACION_ID")
    @Column(name = "CLASIFICACION_ID")
    private Long clasificacionEstudio;

    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    private Item item;

    public ItemClasificacion() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClasificacionEstudio() {
        return clasificacionEstudio;
    }

    public void setClasificacionEstudio(Long clasificacionEstudio) {
        this.clasificacionEstudio = clasificacionEstudio;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}