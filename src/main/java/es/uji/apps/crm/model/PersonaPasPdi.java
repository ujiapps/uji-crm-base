package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_PERSONAS_PASPDI database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_EXT_PERSONAS_PASPDI")
public class PersonaPasPdi implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;

    // bi-directional many-to-one association to ServicioUsuario
    @OneToMany(mappedBy = "personaPasPdi")
    private Set<ServicioUsuario> serviciosUsuario;

    // bi-directional many-to-one association to ProgramaUsuario
    @OneToMany(mappedBy = "personaPasPdi")
    private Set<ProgramaUsuario> programasUsuarios;

    // bi-directional many-to-one association to Envios
    @OneToMany(mappedBy = "persona")
    private Set<Envio> envios;

    public PersonaPasPdi() {
    }

    public PersonaPasPdi(Long personaId) {
        this.id = personaId;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<ServicioUsuario> getServiciosUsuario() {
        return serviciosUsuario;
    }

    public void setServicioUsuario(Set<ServicioUsuario> serviciosUsuario) {
        this.serviciosUsuario = serviciosUsuario;
    }

    public Set<ProgramaUsuario> getProgramasUsuarios() {
        return this.programasUsuarios;
    }

    public void setProgramasUsuarios(Set<ProgramaUsuario> programasUsuarios) {
        this.programasUsuarios = programasUsuarios;
    }

}