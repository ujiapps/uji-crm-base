package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_EXT_PERFILES database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_EXT_PERFILES")
public class Perfil implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    // bi-directional many-to-one association to ProgramaPerfil
    @OneToMany(mappedBy = "perfil")
    private Set<ProgramaPerfil> programasPerfiles;

    public Perfil() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<ProgramaPerfil> getProgramasPerfiles() {
        return this.programasPerfiles;
    }

    public void setProgramasPerfiles(Set<ProgramaPerfil> programasPerfiles) {
        this.programasPerfiles = programasPerfiles;
    }

}