package es.uji.apps.crm.model.domains;

public enum TipoDato {
    NUMERO(6L, "N", "Numérico"), TEXTO(8L, "T", "Texto"), FECHA(7L, "F", "Fecha"), SELECCION(20L,
            "S", "Select"), BINARIO(10L, "B", "Binario"), TELEFONO(12L, "NT", "Teléfono"), EMAIL(
            11L, "TM", "Email"), TEXTO_LARGO(31L, "TL", "Texto largo"), CABECERA(42L, "TCA",
            "Cabecera"), ENUNCIADO(73L, "TEN", "Enunciado"), BOOLEANO(43L, "NBOOL", "Booleano"), ITEMS_SELECCION_MULTIPLE(44L, "ISM",
            "Listado de items selección múltiple"), ITEMS_SELECCION_UNICA_CHECKS(45L, "ISUC",
            "Listado de items selección única con checks"), ITEMS_SELECCION_UNICA_DESPL(46L,
            "ISUD", "Listado de items selección única desplegable"), ITEMS_SELECCION_MULTIPLE_CHECKS(
            47L, "ISMC", "Listado de items seleccion múltiple con checks"), TEXTO_LOPD(83L, "TLOPD", "Texto LOPD");
    private final Long id;
    private final String nombre;
    private final String descripcion;

    TipoDato(Long id, String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return descripcion;
    }

}
