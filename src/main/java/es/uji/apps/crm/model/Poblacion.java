package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CRM_EXT_PUEBLOS")
public class Poblacion implements Serializable {
    @Id
    private Long id;

    @Column(name = "PUEBLO_ID")
    private Long pueblo;

    @Column(name = "NOMBRE_CA")
    private String nombreCA;

    @Column(name = "NOMBRE_CA_LIMPIO")
    private String nombreCALimpio;

    @Column(name = "NOMBRE_ES")
    private String nombreES;

    @Column(name = "NOMBRE_ES_LIMPIO")
    private String nombreESLimpio;

    @Column(name = "NOMBRE_EN")
    private String nombreEN;

    @Column(name = "NOMBRE_EN_LIMPIO")
    private String nombreENLimpio;

    @ManyToOne
    @JoinColumn(name = "PROVINCIA_ID")
    private Provincia provincia;

    public Poblacion() {
    }

    public Poblacion(Long poblacionPostal) {
        this.id = poblacionPostal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    public String getNombreCA() {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA) {
        this.nombreCA = nombreCA;
    }

    public String getNombreES() {
        return nombreES;
    }

    public void setNombreES(String nombreES) {
        this.nombreES = nombreES;
    }

    public String getNombreEN() {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN) {
        this.nombreEN = nombreEN;
    }

    public Long getPueblo() {
        return pueblo;
    }

    public void setPueblo(Long pueblo) {
        this.pueblo = pueblo;
    }
}