package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_EXT_FOTOS")
public class PersonaFoto implements Serializable {
    @Id
    @Column(name = "CLIENTE_ID")
    private Long clienteId;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "NOMBRE_FICHERO")
    private String nombreFichero;

    @Column(name = "BINARIO")
    private byte[] binario;

    @Column(name = "MIME_TYPE")
    private String mimeType;

    public PersonaFoto() {
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public String getNombreFichero() {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero) {
        this.nombreFichero = nombreFichero;
    }

    public byte[] getBinario() {
        return binario;
    }

    public void setBinario(byte[] binario) {
        this.binario = binario;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }
}