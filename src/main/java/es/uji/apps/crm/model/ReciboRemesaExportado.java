package es.uji.apps.crm.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CRM_RECIBOS_REMESAS_EXPORT", schema = "UJI_CRM")
public class ReciboRemesaExportado {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "remesas_sequence")
    @SequenceGenerator(
            name = "remesas_sequence",
            sequenceName = "remesas_sequence",
            allocationSize = 1
    )
    @Column(name = "ID")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha")
    private Date fecha;

    @OneToMany(mappedBy = "remesa")
    private Set<ReciboMovimientoExportado> reciboMovimientosExportados;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Set<ReciboMovimientoExportado> getReciboMovimientosExportados() {
        return reciboMovimientosExportados;
    }

    public void setReciboMovimientosExportados(Set<ReciboMovimientoExportado> reciboMovimientosExportados) {
        this.reciboMovimientosExportados = reciboMovimientosExportados;
    }
}