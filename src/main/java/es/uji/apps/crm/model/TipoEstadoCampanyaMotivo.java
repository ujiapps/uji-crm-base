package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_TIPOS_ACCESO database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_ESTADOS_MOTIVOS")
public class TipoEstadoCampanyaMotivo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nombre_es")
    private String nombreEs;
    @Column(name = "nombre_uk")
    private String nombreUk;
    @Column(name = "nombre_ca")
    private String nombreCa;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ESTADO_ID")
    private TipoEstadoCampanya tipoEstadoCampanya;

    @OneToMany(mappedBy = "campanyaEstadoMotivo")
    private Set<ClienteEstadoCampanyaMotivo> clienteEstadoCampanyaMotivos;

    public TipoEstadoCampanyaMotivo() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoEstadoCampanya getTipoEstadoCampanya() {
        return tipoEstadoCampanya;
    }

    public void setTipoEstadoCampanya(TipoEstadoCampanya tipoEstadoCampanya) {
        this.tipoEstadoCampanya = tipoEstadoCampanya;
    }

    public Set<ClienteEstadoCampanyaMotivo> getClienteEstadoCampanyaMotivos() {
        return clienteEstadoCampanyaMotivos;
    }

    public void setClienteEstadoCampanyaMotivos(Set<ClienteEstadoCampanyaMotivo> clienteEstadoCampanyaMotivos) {
        this.clienteEstadoCampanyaMotivos = clienteEstadoCampanyaMotivos;
    }

    public String getTextoByIdioma(String idioma) {
        if (idioma == null)
        {
            idioma = "CA";
        }
        switch (idioma.toUpperCase())
        {
            case "ES":
                return getNombreEs();
            case "UK":
            case "EN":
                return getNombreUk();
            default:
                return getNombreCa();
        }
    }

    public String getNombreEs() {
        return nombreEs;
    }

    public void setNombreEs(String nombreEs) {
        this.nombreEs = nombreEs;
    }

    public String getNombreUk() {
        return nombreUk;
    }

    public void setNombreUk(String nombreUk) {
        this.nombreUk = nombreUk;
    }

    public String getNombreCa() {
        return nombreCa;
    }

    public void setNombreCa(String nombreCa) {
        this.nombreCa = nombreCa;
    }
}