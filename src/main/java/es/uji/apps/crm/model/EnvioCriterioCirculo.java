package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;


@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_ENVIOS_CRITERIOS_CIRCULOS")
public class EnvioCriterioCirculo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ENVIO_CRITERIO_ID")
    private EnvioCriterio envioCriterio;

    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    private Item circulo;

    public EnvioCriterioCirculo() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EnvioCriterio getEnvioCriterio() {
        return envioCriterio;
    }

    public void setEnvioCriterio(EnvioCriterio envioCriterio) {
        this.envioCriterio = envioCriterio;
    }

    public Item getCirculo() {
        return circulo;
    }

    public void setCirculo(Item circulo) {
        this.circulo = circulo;
    }
}