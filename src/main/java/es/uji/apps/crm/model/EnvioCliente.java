package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_ENVIOS_CLIENTES database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_ENVIOS_CLIENTES")
public class EnvioCliente implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String para;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_ENVIO")
    private Date fechaEnvio;

    // bi-directional many-to-one association to Cliente
    @ManyToOne
    @JoinColumn(name = "CLIENTE_ID")
    private Cliente cliente;

    // bi-directional many-to-one association to Envio
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ENVIO_ID")
    private Envio envio;

    @OneToOne(mappedBy = "envioCliente")
    private EnvioMensajeria envioMensajeria;

    public EnvioCliente() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Envio getEnvio() {
        return envio;
    }

    public void setEnvio(Envio envio) {
        this.envio = envio;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getPara() {
        return para;
    }

    public void setPara(String para) {
        this.para = para;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public EnvioMensajeria getEnvioMensajeria() {
        return envioMensajeria;
    }

    public void setEnvioMensajeria(EnvioMensajeria envioMensajeria) {
        this.envioMensajeria = envioMensajeria;
    }
}