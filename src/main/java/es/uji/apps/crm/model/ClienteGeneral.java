package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CLIENTES_GENERAL")
public class ClienteGeneral implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String identificacion;

    // bi-directional many-to-one association to Persona
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    private Persona persona;

    @ManyToOne
    @JoinColumn(name = "TIPO_IDENTIFICACION_ID")
    private Tipo tipoIdentificacion;

    private Boolean definitivo;

    @OneToMany(mappedBy = "clienteGeneral")
    private Set<Cliente> clientes;

    public ClienteGeneral() {
    }

    public ClienteGeneral(String identificacion, Tipo tipoIdentificacion, Boolean definitivo) {
        this.identificacion = identificacion;
        this.tipoIdentificacion = tipoIdentificacion;
        this.definitivo = definitivo;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Tipo getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(Tipo tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public Set<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(Set<Cliente> clientes) {
        this.clientes = clientes;
    }

    public Boolean isDefinitivo() {
        return definitivo;
    }

    public void setDefinitivo(Boolean definitivo) {
        this.definitivo = definitivo;
    }
}