package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CAMPANYAS database table.
 */

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_FORM_EST_ORIGEN")
public class CampanyaFormularioEstadoOrigen implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "FORMULARIO_ID")
    private CampanyaFormulario campanyaFormulario;

    @ManyToOne
    @JoinColumn(name = "ESTADO_ORIGEN_ID")
    private TipoEstadoCampanya estadoOrigen;


    public CampanyaFormularioEstadoOrigen() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public TipoEstadoCampanya getEstadoOrigen() {
        return estadoOrigen;
    }

    public void setEstadoOrigen(TipoEstadoCampanya estadoOrigen) {
        this.estadoOrigen = estadoOrigen;
    }

    public CampanyaFormulario getCampanyaFormulario() {
        return campanyaFormulario;
    }

    public void setCampanyaFormulario(CampanyaFormulario campanyaFormulario) {
        this.campanyaFormulario = campanyaFormulario;
    }
}