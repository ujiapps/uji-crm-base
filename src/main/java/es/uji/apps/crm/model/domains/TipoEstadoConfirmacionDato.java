package es.uji.apps.crm.model.domains;

public enum TipoEstadoConfirmacionDato {
    PENDIENTE(1L, "Pendent"), VALIDO(2L, "Vàlid"), NO_VALIDO(3L, "No Vàlid");

    private final Long id;
    private final String nombre;

    TipoEstadoConfirmacionDato(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

}