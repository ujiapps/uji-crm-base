package es.uji.apps.crm.model.domains;

public enum TipoOperador {
    Y(1L), O(2L), YNO(3L), ONO(4L);

    private final Long id;

    TipoOperador(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}