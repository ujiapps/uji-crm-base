package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA. User: veriton1 Date: 14/04/14 Time: 13:37 To change this template use
 * File | Settings | File Templates.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_EXT_VINCULACIONES")
public class SubVinculo implements Serializable {
    @Id
    private Long id;

    @Column(name = "vin_id")
    private Long vinculoId;

    @Column(name = "sub_vin_id")
    private Long subVinculoId;

    @Column(name = "vin_nombre")
    private String vinculoNombre;

    @Column(name = "sub_vin_nombre")
    private String subVinculoNombre;
    @OneToMany(mappedBy = "subVinculo")
    private Set<VwVinculacionPersona> vinculacionesPersona;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<VwVinculacionPersona> getVinculacionesPersona() {
        return vinculacionesPersona;
    }

    public void setVinculacionesPersona(Set<VwVinculacionPersona> vinculacionesPersona) {
        this.vinculacionesPersona = vinculacionesPersona;
    }

    public String getVinculoNombre() {
        return vinculoNombre;
    }

    public void setVinculoNombre(String vinculoNombre) {
        this.vinculoNombre = vinculoNombre;
    }

    public String getSubVinculoNombre() {
        return subVinculoNombre;
    }

    public void setSubVinculoNombre(String subVinculoNombre) {
        this.subVinculoNombre = subVinculoNombre;
    }

    public Long getVinculoId() {
        return vinculoId;
    }

    public void setVinculoId(Long vinculoId) {
        this.vinculoId = vinculoId;
    }

    public Long getSubVinculoId() {
        return subVinculoId;
    }

    public void setSubVinculoId(Long subVinculoId) {
        this.subVinculoId = subVinculoId;
    }
}