package es.uji.apps.crm.model.domains;

public enum TipoOperacionEnvioCriterio {
    ANYADIR(1L), RESTRINGIR(2L), ELIMINAR(3L);

    private final Long id;

    TipoOperacionEnvioCriterio(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}