package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CAMPANYAS_DATOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_ITEMS")
public class CampanyaItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // bi-directional many-to-one association to Campanya
    @ManyToOne
    @JoinColumn(name = "CAMPANYAS_ID")
    private Campanya campanya;

    // bi-directional many-to-one association to ClienteDatoTipo
    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    private Item item;

    public CampanyaItem() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Campanya getCampanya() {
        return this.campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}