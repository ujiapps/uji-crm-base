package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import es.uji.apps.crm.model.domains.TipoEnvio;
import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_ENVIOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_ENVIOS")
public class Envio implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String asunto;

    private String desde;

    @Column(name = "tipo_correo_envio")
    private Long tipoCorreoEnvio;

    @Column(name = "responder_a")
    private String responder;

    @Lob
    private String cuerpo;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_ENVIO_PLATAFORMA")
    private Date fechaEnvioPlataforma;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_ENVIO_DESTINATARIO")
    private Date fechaEnvioDestinatario;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_FIN_VIGENCIA")
    private Date fechaFinVigencia;

    private String nombre;

    @Column(insertable = false, updatable = false)
    private String busqueda;

    // bi-directional many-to-one association to PersonaPasPdi
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private PersonaPasPdi persona;

    // bi-directional many-to-one association to Campanya
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SEGUIMIENTO_ID")
    private CampanyaSeguimiento seguimiento;

    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private Tipo envioTipo;

    @ManyToOne
    @JoinColumn(name = "PROGRAMA_ID")
    private Programa programa;

    // bi-directional many-to-one association to EnvioAdjunto
    @OneToMany(mappedBy = "envio")
    private Set<EnvioAdjunto> enviosAdjuntos;

    // bi-directional many-to-one association to EnvioCliente
    @OneToMany(mappedBy = "envio")
    private Set<EnvioCliente> envioClientes;

    @OneToMany(mappedBy = "envio")
    private Set<Sesion> envioSesiones;

    @OneToMany(mappedBy = "envio")
    private Set<EnvioClienteVista> envioClientesVista;

    @OneToMany(mappedBy = "envio")
    private Set<EnvioImagen> envioImagenes;

    @OneToMany(mappedBy = "envio")
    private Set<EnvioCriterioCliente> envioCriterioClientes;

    public Envio() {
    }

    public Envio(EnvioPlantillaAdmin envioPlantillaAdmin, String idioma, String direccionWeb) {
        this.setNombre(envioPlantillaAdmin.getNombre());

        if (idioma.equalsIgnoreCase("ca")) {
            this.setAsunto(envioPlantillaAdmin.getAsuntoCA());
            this.setCuerpo(envioPlantillaAdmin.getCuerpoCA().replace("$[direccionWeb]", direccionWeb));
        } else {
            if (idioma.equalsIgnoreCase("uk") || idioma.equalsIgnoreCase("en")) {
                this.setAsunto(envioPlantillaAdmin.getAsuntoUK());
                this.setCuerpo(envioPlantillaAdmin.getCuerpoUK().replace("$[direccionWeb]", direccionWeb));
            } else {
                this.setAsunto(envioPlantillaAdmin.getAsuntoES());
                this.setCuerpo(envioPlantillaAdmin.getCuerpoES().replace("$[direccionWeb]", direccionWeb));
            }
        }

        this.setDesde(envioPlantillaAdmin.getDesde());

        this.setFecha(new Date());
        this.setFechaEnvioDestinatario(new Date());
        this.setPersona(new PersonaPasPdi(40000L));
        this.setResponder(envioPlantillaAdmin.getResponder());
        this.setTipoCorreoEnvio(0L);

        this.setEnvioTipo(new Tipo(TipoEnvio.EMAIL.getId()));

    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAsunto() {
        return this.asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCuerpo() {
        return this.cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaFinVigencia() {
        return this.fechaFinVigencia;
    }

    public void setFechaFinVigencia(Date fechaFinVigencia) {
        this.fechaFinVigencia = fechaFinVigencia;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public PersonaPasPdi getPersona() {
        return this.persona;
    }

    public void setPersona(PersonaPasPdi persona) {
        this.persona = persona;
    }

    public CampanyaSeguimiento getSeguimiento() {
        return this.seguimiento;
    }

    public void setSeguimiento(CampanyaSeguimiento seguimiento) {
        this.seguimiento = seguimiento;
    }

    public Set<EnvioAdjunto> getEnviosAdjuntos() {
        return this.enviosAdjuntos;
    }

    public void setEnviosAdjuntos(Set<EnvioAdjunto> enviosAdjuntos) {
        this.enviosAdjuntos = enviosAdjuntos;
    }

    public Tipo getEnvioTipo() {
        return envioTipo;
    }

    public void setEnvioTipo(Tipo envioTipo) {
        this.envioTipo = envioTipo;
    }

    public Date getFechaEnvioPlataforma() {
        return fechaEnvioPlataforma;
    }

    public void setFechaEnvioPlataforma(Date fechaEnvioPlataforma) {
        this.fechaEnvioPlataforma = fechaEnvioPlataforma;
    }

    public Date getFechaEnvioDestinatario() {
        return fechaEnvioDestinatario;
    }

    public void setFechaEnvioDestinatario(Date fechaEnvioDestinatario) {
        this.fechaEnvioDestinatario = fechaEnvioDestinatario;
    }

    public Set<EnvioCliente> getEnvioClientes() {
        return envioClientes;
    }

    public void setEnvioClientes(Set<EnvioCliente> envioClientes) {
        this.envioClientes = envioClientes;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getResponder() {
        return responder;
    }

    public void setResponder(String responder) {
        this.responder = responder;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public Set<EnvioImagen> getEnvioImagenes() {
        return envioImagenes;
    }

    public void setEnvioImagenes(Set<EnvioImagen> envioImagenes) {
        this.envioImagenes = envioImagenes;
    }

    public Long getTipoCorreoEnvio() {
        return tipoCorreoEnvio;
    }

    public void setTipoCorreoEnvio(Long tipoCorreoEnvio) {
        this.tipoCorreoEnvio = tipoCorreoEnvio;
    }

    public Set<EnvioClienteVista> getEnvioClientesVista() {
        return envioClientesVista;
    }

    public void setEnvioClientesVista(Set<EnvioClienteVista> envioClientesVista) {
        this.envioClientesVista = envioClientesVista;
    }

    public Set<EnvioCriterioCliente> getEnvioCriterioClientes() {
        return envioCriterioClientes;
    }

    public void setEnvioCriterioClientes(Set<EnvioCriterioCliente> envioCriterioClientes) {
        this.envioCriterioClientes = envioCriterioClientes;
    }

    public Set<Sesion> getEnvioSesiones() {
        return envioSesiones;
    }

    public void setEnvioSesiones(Set<Sesion> envioSesiones) {
        this.envioSesiones = envioSesiones;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }
}