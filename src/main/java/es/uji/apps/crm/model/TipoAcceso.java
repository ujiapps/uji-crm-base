package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_TIPOS_ACCESO database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_TIPOS_ACCESO")
public class TipoAcceso implements Serializable {
    public final static Long ACCESO_PUBLICO = 1L;
    public final static Long ACCESO_PRIVADO = 2L;
    public final static Long ACCESO_POR_PERFIL = 3L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    // bi-directional many-to-one association to Grupo
    @OneToMany(mappedBy = "tipoAcceso")
    private Set<Grupo> Grupos;

    // bi-directional many-to-one association to Programa
    @OneToMany(mappedBy = "tipoAcceso")
    private Set<Programa> Programas;

    // bi-directional many-to-one association to CrmItem
    @OneToMany(mappedBy = "tipoAcceso")
    private Set<Item> Items;

    public TipoAcceso() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Grupo> getGrupos() {
        return this.Grupos;
    }

    public void setGrupos(Set<Grupo> Grupos) {
        this.Grupos = Grupos;
    }

    public Set<Programa> getProgramas() {
        return this.Programas;
    }

    public void setProgramas(Set<Programa> Programas) {
        this.Programas = Programas;
    }

    public Set<Item> getItems() {
        return this.Items;
    }

    public void setItems(Set<Item> Items) {
        this.Items = Items;
    }

}