package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.springframework.stereotype.Component;

@Table(name = "CRM_VW_ENTRADAS_ACTOS")
@Entity
@Component
@SuppressWarnings("serial")

public class VwEntradaCrm implements Serializable {

    @Id
    private String persona;
    private String correo;

    private Long entradas;
    private Long sobrantes;
    private Long nuevas;
    private String web;
    private Boolean dadas;
    @Column(name = "NUEVAS_ID")
    private Long nuevasId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_MOV")
    private Date fechaMovimiento;
//    @Column(name = "ENTRADAS_ID")
//    private Long entradasId;

    @Id
    @ManyToOne
    @JoinColumn(name = "CLIENTE_ID")
    private Cliente cliente;

    @Id
    @ManyToOne
    @JoinColumn(name = "ACTO")
    private CampanyaActo acto;

    public VwEntradaCrm() {
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public Long getEntradas() {
        return entradas;
    }

    public void setEntradas(Long entradas) {
        this.entradas = entradas;
    }

    public Long getSobrantes() {
        return sobrantes;
    }

    public void setSobrantes(Long sobrantes) {
        this.sobrantes = sobrantes;
    }

    public CampanyaActo getActo() {
        return acto;
    }

    public void setActo(CampanyaActo acto) {
        this.acto = acto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Long getNuevas() {
        return nuevas;
    }

    public void setNuevas(Long nuevas) {
        this.nuevas = nuevas;
    }

    public Boolean getDadas() {
        return dadas;
    }

    public void setDadas(Boolean dadas) {
        this.dadas = dadas;
    }

    public Long getNuevasId() {
        return nuevasId;
    }

    public void setNuevasId(Long nuevasId) {
        this.nuevasId = nuevasId;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Date getFechaMovimiento() {
        return fechaMovimiento;
    }

    public void setFechaMovimiento(Date fechaMovimiento) {
        this.fechaMovimiento = fechaMovimiento;
    }

    //    public Long getEntradasId() {
//        return entradasId;
//    }
//
//    public void setEntradasId(Long entradasId) {
//        this.entradasId = entradasId;
//    }
}