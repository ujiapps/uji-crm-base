package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_EXT_EMISORAS")
public class Emisora implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @Column(name = "NOMBRE")
    private String nombre;

    @OneToMany(mappedBy = "emisora")
    private Set<LineaFacturacion> lineasFacturacion;

    public Emisora() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<LineaFacturacion> getLineasFacturacion() {
        return lineasFacturacion;
    }

    public void setLineasFacturacion(Set<LineaFacturacion> lineasFacturacion) {
        this.lineasFacturacion = lineasFacturacion;
    }
}