package es.uji.apps.crm.model;

public class Paginacion
{
    private Long start;
    private Long limit;
    private String ordenarPor;
    private String direccion;
    private Long totalCount;

    public Paginacion(Long start, Long limit)
    {
        this(start, limit, null, null);
    }

    public Paginacion(Long start, Long limit, String ordenarPor) {
        this(start, limit, ordenarPor, "ASC");
    }
    public Paginacion(Long start, Long limit, String ordenarPor, String direccion)
    {
        this.start = start;
        this.limit = limit;
        this.ordenarPor = ordenarPor;
        this.direccion = direccion;
    }

    public Long getStart()
    {
        return start;
    }

    public void setStart(Long start)
    {
        this.start = start;
    }

    public Long getLimit()
    {
        return limit;
    }

    public void setLimit(Long limit)
    {
        this.limit = limit;
    }

    public String getOrdenarPor()
    {
        return ordenarPor;
    }

    public void setOrdenarPor(String ordenarPor)
    {
        this.ordenarPor = ordenarPor;
    }

    public String getDireccion()
    {
        return direccion;
    }

    public void setDireccion(String direccion)
    {
        this.direccion = direccion;
    }

    public Long getTotalCount()
    {
        return totalCount;
    }

    public void setTotalCount(Long totalCount)
    {
        this.totalCount = totalCount;
    }
}