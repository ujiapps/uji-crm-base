package es.uji.apps.crm.model.domains;

public enum TipoOrigenEnvioCriterio {
    GRUPOS(2L, "Grupos"), CAMPANYAS(1L, "Campanyes"), ETIQUETAS(3L, "Etiquetes"), ELIMINAR(4L, "Eliminar"), DATOEXTRA(5L, "Dato Extra"), MOVIMIENTO(6L, "Movimientos"),
    CLIENTE(7L, "Cliente"), DATOSGENERALES(8L, "Datos Generales"), CONSULTASQL(9L, "ConsultaSQL");

    private final Long id;
    private final String nombre;

    TipoOrigenEnvioCriterio(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

}
