package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import es.uji.apps.crm.model.domains.TipoEstadoConfirmacionDato;
import org.springframework.stereotype.Component;

import es.uji.commons.rest.ParamUtils;

@Entity
@Component
@Table(name = "CRM_CLIENTES")

public class Cliente implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String correo;

    @Column(name = "NOMBRE_OFICIAL")
    private String nombre;

    @Column(name = "APELLIDOS_OFICIAL")
    private String apellidos;

    @Column(name = "CODIGO_POSTAL")
    private String codigoPostal;
    private String postal;

    @Column(name = "FICHA_ZONA_PRIVADA")
    private Boolean fichaZonaPrivada;

    @Column(name = "RECIBE_CORREO")
    private Boolean recibeCorreo;

    @Column(name = "RECIBE_POSTAL")
    private Boolean recibePostal;

    private String movil;

    @Column(name = "prefijo_telefono")
    private String prefijoTelefono;

    private String tipo;

    private Long sexo;

    @Column(name = "BUSQUEDA", updatable = false, insertable = false)
    private String busqueda;

    @Column(name = "FECHA_NACIMIENTO")
    private Date fechaNacimiento;

    @Column(name = "DEFINITIVO")
    private Boolean definitivo;

    @ManyToOne
    @JoinColumn(name = "CLIENTE_GENERAL_ID")
    private ClienteGeneral clienteGeneral;

    @ManyToOne
    @JoinColumn(name = "SERVICIO_ID")
    private Servicio servicio;

    @ManyToOne
    @JoinColumn(name = "NACIONALIDAD_ID")
    private Pais nacionalidad;

    @ManyToOne
    @JoinColumn(name = "PAIS_ID")
    private Pais paisPostal;

    @ManyToOne
    @JoinColumn(name = "PROVINCIA_ID")
    private Provincia provinciaPostal;

    @Column(name = "PROVINCIA_NOMBRE")
    private String provinciaPostalNombre;

    @ManyToOne
    @JoinColumn(name = "POBLACION_ID")
    private Poblacion poblacionPostal;

    @Column(name = "POBLACION_NOMBRE")
    private String poblacionPostalNombre;

    // bi-directional many-to-one association to Tipo
    @ManyToOne
    @JoinColumn(name = "CORREO_ESTADO_ID")
    private Tipo correoEstado;

    // bi-directional many-to-one association to Tipo
    @ManyToOne
    @JoinColumn(name = "MOVIL_ESTADO_ID")
    private Tipo movilEstado;

    // bi-directional many-to-one association to Tipo
    @ManyToOne
    @JoinColumn(name = "POSTAL_ESTADO_ID")
    private Tipo postalEstado;

    // bi-directional many-to-one association to CampanyaCliente
    @OneToMany(mappedBy = "cliente")
    private Set<CampanyaCliente> campanyasClientes;

    @OneToMany(mappedBy = "cliente")
    private Set<ClienteCirculo> clienteCirculos;

    @OneToMany(mappedBy = "cliente")
    private Set<Sesion> sesionesCliente;

    @OneToMany(mappedBy = "cliente")
    private Set<ClienteTarifa> tarifas;

    @OneToMany(mappedBy = "cliente")
    private Set<ClienteCuota> clienteCuotas;

    @OneToMany(mappedBy = "cliente")
    private Set<EnvioCriterioCliente> envioCriterioClientes;

    @OneToMany(mappedBy = "cliente")
    private Set<ClienteTitulacionesNoUJI> titulacionesNoUJI;

    @OneToMany(mappedBy = "cliente")
    private Set<ClienteTitulacionesUJI> titulacionesUJI;

    // bi-directional many-to-one association to ClienteDato
    @OneToMany(mappedBy = "cliente")
    private Set<ClienteDato> clientesDatos;

    // bi-directional many-to-one association to ClienteEtiqueta
    @OneToMany(mappedBy = "cliente")
    private Set<ClienteEtiqueta> clientesEtiquetas;

    // bi-directional many-to-one association to ClienteItem
    @OneToMany(mappedBy = "cliente")
    private Set<ClienteItem> clientesItems;

    // bi-directional many-to-one association to EnvioCliente
    @OneToMany(mappedBy = "cliente")
    private Set<EnvioCliente> envioClientes;

    // bi-directional many-to-one association to Seguimiento
    @OneToMany(mappedBy = "cliente")
    private Set<Seguimiento> seguimientos;

    // bi-directional many-to-one association to Vinculacion
    @OneToMany(mappedBy = "cliente1")
    private Set<Vinculacion> vinculaciones1;

    // bi-directional many-to-one association to Vinculacion
    @OneToMany(mappedBy = "cliente2")
    private Set<Vinculacion> vinculaciones2;

    @OneToMany(mappedBy = "cliente")
    private Set<Movimiento> movimientos;

    public Cliente() {
    }

    public Cliente(Long clienteId) {
        this.id = clienteId;
    }

    public Cliente(ClienteGeneral clienteGeneral,
                   String nombre,
                   String apellidos,
                   String correo,
                   String movil,
                   String tipo,
                   String postal,
                   String codigoPostal,
                   String prefijoTelefono,
                   String paisPostal,
                   Long provinciaPostal,
                   Long poblacionPostal,
                   String provinciaPostalNombre,
                   String poblacionPostalNombre,
                   Long postalEstado,
                   Long movilEstado,
                   Long correoEstado,
                   Boolean recibeCorreo,
                   Boolean recibePostal,
                   Boolean definitivo,
                   Boolean fichaZonaPrivada,
                   String nacionalidad,
                   Long sexo,
                   Date fechaNacimiento,
                   Long servicio) {
        this.clienteGeneral = clienteGeneral;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.correo = correo;
        this.movil = movil;
        this.tipo = tipo;
        this.postal = postal;
        this.codigoPostal = codigoPostal;
        this.prefijoTelefono = prefijoTelefono;
        this.paisPostal = ParamUtils.isNotNull(paisPostal)?new Pais(paisPostal):null;
        this.provinciaPostal = ParamUtils.isNotNull(provinciaPostal)?new Provincia(provinciaPostal):null;
        this.poblacionPostal = ParamUtils.isNotNull(poblacionPostal)?new Poblacion(poblacionPostal):null;
        this.provinciaPostalNombre = provinciaPostalNombre;
        this.poblacionPostalNombre = poblacionPostalNombre;
        this.postalEstado = ParamUtils.isNotNull(postalEstado)?new Tipo(postalEstado):null;
        this.movilEstado = ParamUtils.isNotNull(movilEstado)?new Tipo(movilEstado):null;
        this.correoEstado = ParamUtils.isNotNull(correoEstado)?new Tipo(correoEstado):null;
        this.recibeCorreo = recibeCorreo;
        this.recibePostal = recibePostal;
        this.definitivo = definitivo;
        this.fichaZonaPrivada = fichaZonaPrivada;
        this.nacionalidad = ParamUtils.isNotNull(nacionalidad)?new Pais(nacionalidad):null;
        this.sexo = sexo;
        this.fechaNacimiento = fechaNacimiento;
        this.servicio = ParamUtils.isNotNull(servicio)?new Servicio(servicio):null;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Set<CampanyaCliente> getCampanyasClientes() {
        return this.campanyasClientes;
    }

    public void setCampanyasClientes(Set<CampanyaCliente> campanyasClientes) {
        this.campanyasClientes = campanyasClientes;
    }

    public Set<ClienteDato> getClientesDatos() {
        return this.clientesDatos;
    }

    public void setClientesDatos(Set<ClienteDato> clientesDatos) {
        this.clientesDatos = clientesDatos;
    }

    public Set<ClienteEtiqueta> getClientesEtiquetas() {
        return this.clientesEtiquetas;
    }

    public void setClientesEtiquetas(Set<ClienteEtiqueta> clientesEtiquetas) {
        this.clientesEtiquetas = clientesEtiquetas;
    }

    public Set<ClienteItem> getClientesItems() {
        return this.clientesItems;
    }

    public void setClientesItems(Set<ClienteItem> clientesItems) {
        this.clientesItems = clientesItems;
    }

    public Set<Seguimiento> getSeguimientos() {
        return this.seguimientos;
    }

    public void setSeguimientos(Set<Seguimiento> seguimientos) {
        this.seguimientos = seguimientos;
    }

    public Set<Vinculacion> getVinculaciones1() {
        return this.vinculaciones1;
    }

    public void setVinculaciones1(Set<Vinculacion> vinculaciones1) {
        this.vinculaciones1 = vinculaciones1;
    }

    public Set<Vinculacion> getVinculaciones2() {
        return this.vinculaciones2;
    }

    public void setVinculaciones2(Set<Vinculacion> vinculaciones2) {
        this.vinculaciones2 = vinculaciones2;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public Tipo getCorreoEstado() {
        return correoEstado;
    }

    public void setCorreoEstado(Tipo correoEstado) {
        this.correoEstado = correoEstado;
    }

    public Tipo getMovilEstado() {
        return movilEstado;
    }

    public void setMovilEstado(Tipo movilEstado) {
        this.movilEstado = movilEstado;
    }

    public Tipo getPostalEstado() {
        return postalEstado;
    }

    public void setPostalEstado(Tipo postalEstado) {
        this.postalEstado = postalEstado;
    }

    public Set<EnvioCliente> getEnvioClientes() {
        return envioClientes;
    }

    public void setEnvioClientes(Set<EnvioCliente> envioClientes) {
        this.envioClientes = envioClientes;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public String getApellidosNombre() {
        String nombre = (this.nombre != null) ? this.nombre : "";
        String apellidos = (this.apellidos != null) ? this.apellidos : "";

        return (apellidos != "") ? apellidos + ", " + nombre : nombre;
    }

    public Set<Movimiento> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(Set<Movimiento> movimientos) {
        this.movimientos = movimientos;
    }

    public Set<Sesion> getSesionesCliente() {
        return sesionesCliente;
    }

    public void setSesionesCliente(Set<Sesion> sesionesCliente) {
        this.sesionesCliente = sesionesCliente;
    }

    public Set<ClienteTarifa> getTarifas() {
        return tarifas;
    }

    public void setTarifas(Set<ClienteTarifa> tarifas) {
        this.tarifas = tarifas;
    }

    public Set<ClienteCuota> getClienteCuotas() {
        return clienteCuotas;
    }

    public void setClienteCuotas(Set<ClienteCuota> clienteCuotas) {
        this.clienteCuotas = clienteCuotas;
    }

    public String getPrefijoTelefono() {
        return prefijoTelefono;
    }

    public void setPrefijoTelefono(String prefijoTelefono) {
        this.prefijoTelefono = prefijoTelefono;
    }

    public ClienteGeneral getClienteGeneral() {
        return clienteGeneral;
    }

    public void setClienteGeneral(ClienteGeneral clienteGeneral) {
        this.clienteGeneral = clienteGeneral;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public Set<ClienteTitulacionesNoUJI> getTitulacionesNoUJI() {
        return titulacionesNoUJI;
    }

    public void setTitulacionesNoUJI(Set<ClienteTitulacionesNoUJI> titulacionesNoUJI) {
        this.titulacionesNoUJI = titulacionesNoUJI;
    }

    public Long getSexo() {
        return sexo;
    }

    public void setSexo(Long sexo) {
        this.sexo = sexo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getPostalCompleto() {

        String poblacion = (ParamUtils.isNotNull(this.getPoblacionPostal())) ? this.getPoblacionPostal().getNombreES() : "";
        String provincia = (ParamUtils.isNotNull(this.getProvinciaPostal())) ? this.getProvinciaPostal().getNombreES() : "";
        String pais = (ParamUtils.isNotNull(this.getPaisPostal())) ? this.getPaisPostal().getNombreES() : "";
        String codigoPostal = (ParamUtils.isNotNull(this.getCodigoPostal())) ? this.getCodigoPostal() : "";

        return this.getNombreApellidos() + "\n" + this.getPostal() + "\n" + codigoPostal + " " + poblacion + ", " + provincia + "\n" + pais;
    }

    public String getPostal() {
        return this.postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getNombreApellidos() {
        String nombre = (ParamUtils.isNotNull(this.nombre)) ? this.nombre + " " : "";
        String apellidos = (ParamUtils.isNotNull(this.apellidos)) ? this.apellidos : "";
        return nombre + apellidos;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public Pais getPaisPostal() {
        return paisPostal;
    }

    public void setPaisPostal(Pais paisPostal) {
        this.paisPostal = paisPostal;
    }

    public Provincia getProvinciaPostal() {
        return provinciaPostal;
    }

    public void setProvinciaPostal(Provincia provinciaPostal) {
        this.provinciaPostal = provinciaPostal;
    }

    public Poblacion getPoblacionPostal() {
        return poblacionPostal;
    }

    public void setPoblacionPostal(Poblacion poblacionPostal) {
        this.poblacionPostal = poblacionPostal;
    }

    public Boolean getFichaZonaPrivada() {
        return fichaZonaPrivada;
    }

    public void setFichaZonaPrivada(Boolean fichaZonaPrivada) {
        this.fichaZonaPrivada = fichaZonaPrivada;
    }

    public Boolean isFichaZonaPrivada() {
        return this.fichaZonaPrivada;
    }

    public Pais getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(Pais nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getProvinciaPostalNombre() {
        return provinciaPostalNombre;
    }

    public void setProvinciaPostalNombre(String provinciaPostalNombre) {
        this.provinciaPostalNombre = provinciaPostalNombre;
    }

    public String getPoblacionPostalNombre() {
        return poblacionPostalNombre;
    }

    public void setPoblacionPostalNombre(String poblacionPostalNombre) {
        this.poblacionPostalNombre = poblacionPostalNombre;
    }

    public Set<ClienteCirculo> getClienteCirculos() {
        return clienteCirculos;
    }

    public void setClienteCirculos(Set<ClienteCirculo> clienteCirculos) {
        this.clienteCirculos = clienteCirculos;
    }

    public Set<ClienteTitulacionesUJI> getTitulacionesUJI() {
        return titulacionesUJI;
    }

    public void setTitulacionesUJI(Set<ClienteTitulacionesUJI> titulacionesUJI) {
        this.titulacionesUJI = titulacionesUJI;
    }

    public Set<EnvioCriterioCliente> getEnvioCriterioClientes() {
        return envioCriterioClientes;
    }

    public void setEnvioCriterioClientes(Set<EnvioCriterioCliente> envioCriterioClientes) {
        this.envioCriterioClientes = envioCriterioClientes;
    }

    public Boolean getRecibeCorreo() {
        return recibeCorreo;
    }

    public Boolean isRecibeCorreo() {
        return recibeCorreo;
    }

    public void setRecibeCorreo(Boolean recibeCorreo) {
        this.recibeCorreo = recibeCorreo;
    }

    public Boolean getRecibePostal() {
        return recibePostal;
    }

    public Boolean isRecibePostal() {
        return recibePostal;
    }

    public void setRecibePostal(Boolean recibePostal) {
        this.recibePostal = recibePostal;
    }

    public Boolean getDefinitivo() {
        return definitivo;
    }

    public Boolean isDefinitivo() {
        return definitivo;
    }

    public void setDefinitivo(Boolean definitivo) {
        this.definitivo = definitivo;
    }

    public void updateDatosFormularioPremium(String nombre,
                                             String apellidos,
                                             Date fechaNacimiento,
                                             String nacionalidad,
                                             String telefono,
                                             String paisPostal,
                                             Long provinciaPostal,
                                             Long poblacionPostal,
                                             String codigoPostal,
                                             String postal) {

        this.setNombre(nombre);
        this.setApellidos(apellidos);
        this.setFechaNacimiento(fechaNacimiento);
        this.setCorreoEstado(new Tipo(TipoEstadoConfirmacionDato.VALIDO.getId()));
        this.setNacionalidad(new Pais(nacionalidad));
        this.setMovil(telefono);
        this.setMovilEstado(new Tipo(TipoEstadoConfirmacionDato.PENDIENTE.getId()));
        this.setPaisPostal(new Pais(paisPostal));
        this.setProvinciaPostal(null);

        if (ParamUtils.isNotNull(provinciaPostal)) {
            this.setProvinciaPostal(new Provincia(provinciaPostal));
        }

        this.setPoblacionPostal(null);
        if (ParamUtils.isNotNull(poblacionPostal)) {
            this.setPoblacionPostal(new Poblacion(poblacionPostal));
        }
        this.setCodigoPostal(codigoPostal);
        this.setPostal(postal);

        this.setPostalEstado(new Tipo(TipoEstadoConfirmacionDato.PENDIENTE.getId()));
    }
}