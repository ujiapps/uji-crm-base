package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_EXT_PERSONAS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "crm_vmc_correos_mensajeria")
public class EnvioMensajeria implements Serializable {
    @Id
    private Long id;

    @Column(name = "ESTADO")
    private String estado;

    @OneToOne
    @JoinColumn(name = "REFERENCIA")
    private EnvioCliente envioCliente;

    public EnvioMensajeria() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public EnvioCliente getEnvioCliente() {
        return envioCliente;
    }

    public void setEnvioCliente(EnvioCliente envioCliente) {
        this.envioCliente = envioCliente;
    }
}