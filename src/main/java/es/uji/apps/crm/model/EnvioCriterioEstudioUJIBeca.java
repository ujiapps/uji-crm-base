package es.uji.apps.crm.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name = "CRM_ENVIOS_CRITERIOS_EST_BECA")
public class EnvioCriterioEstudioUJIBeca {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ENVIO_CRITERIO_ID")
    private EnvioCriterio envioCriterio;

    @ManyToOne
    @JoinColumn(name = "TIPO_BECA_ID")
    private TipoBeca tipoBeca;

    public EnvioCriterioEstudioUJIBeca() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EnvioCriterio getEnvioCriterio() {
        return envioCriterio;
    }

    public void setEnvioCriterio(EnvioCriterio envioCriterio) {
        this.envioCriterio = envioCriterio;
    }

    public TipoBeca getTipoBeca() {
        return tipoBeca;
    }

    public void setTipoBeca(TipoBeca tipoBeca) {
        this.tipoBeca = tipoBeca;
    }
}