package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_CLI_ANYADIR_AUTO")
public class CampanyaClienteAnyadirAuto implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CLIENTES_IDS")
    @Lob
    private String clientes;

    @ManyToOne
    @JoinColumn(name = "ESTADO_ID")
    private Tipo campanyaClienteEstado;

    // bi-directional many-to-one association to Campanya
    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private Campanya campanya;

    public CampanyaClienteAnyadirAuto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientes() {
        return clientes;
    }

    public void setClientes(String clientes) {
        this.clientes = clientes;
    }

    public Tipo getCampanyaClienteEstado() {
        return campanyaClienteEstado;
    }

    public void setCampanyaClienteEstado(Tipo campanyaClienteEstado) {
        this.campanyaClienteEstado = campanyaClienteEstado;
    }

    public Campanya getCampanya() {
        return campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }
}