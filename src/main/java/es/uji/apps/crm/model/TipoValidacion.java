package es.uji.apps.crm.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CRM_TIPOS_VALIDACION", schema = "UJI_CRM")
public class TipoValidacion {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "VALIDACION")
    private String validacion;

    @OneToMany(mappedBy = "tipoValidacion")
    private Set<CampanyaDato> campanyaDatos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<CampanyaDato> getCampanyaDatos() {
        return campanyaDatos;
    }

    public void setCampanyaDatos(Set<CampanyaDato> campanyaDatos) {
        this.campanyaDatos = campanyaDatos;
    }

    public String getValidacion() {
        return validacion;
    }

    public void setValidacion(String validacion) {
        this.validacion = validacion;
    }
}