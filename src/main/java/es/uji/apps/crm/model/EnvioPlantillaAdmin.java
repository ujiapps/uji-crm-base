package es.uji.apps.crm.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_ENVIOS_PLANTILLA_ADM")

public class EnvioPlantillaAdmin implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    @Column(name = "ASUNTO_ES")
    private String asuntoES;

    @Column(name = "ASUNTO_CA")
    private String asuntoCA;

    @Column(name = "ASUNTO_UK")
    private String asuntoUK;

    @Column(name = "CUERPO_ES")
    private String cuerpoES;

    @Column(name = "CUERPO_CA")
    private String cuerpoCA;

    @Column(name = "CUERPO_UK")
    private String cuerpoUK;

    private String responder;

    private String desde;

    public EnvioPlantillaAdmin() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAsuntoES() {
        return asuntoES;
    }

    public void setAsuntoES(String asuntoES) {
        this.asuntoES = asuntoES;
    }

    public String getAsuntoCA() {
        return asuntoCA;
    }

    public void setAsuntoCA(String asuntoCA) {
        this.asuntoCA = asuntoCA;
    }

    public String getAsuntoUK() {
        return asuntoUK;
    }

    public void setAsuntoUK(String asuntoUK) {
        this.asuntoUK = asuntoUK;
    }

    public String getCuerpoES() {
        return cuerpoES;
    }

    public void setCuerpoES(String cuerpoES) {
        this.cuerpoES = cuerpoES;
    }

    public String getCuerpoCA() {
        return cuerpoCA;
    }

    public void setCuerpoCA(String cuerpoCA) {
        this.cuerpoCA = cuerpoCA;
    }

    public String getCuerpoUK() {
        return cuerpoUK;
    }

    public void setCuerpoUK(String cuerpoUK) {
        this.cuerpoUK = cuerpoUK;
    }

    public String getResponder() {
        return responder;
    }

    public void setResponder(String responder) {
        this.responder = responder;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }
}