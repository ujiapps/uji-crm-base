package es.uji.apps.crm.model.domains;

public enum TipoEstadoCuota {
    VALIDO(83L, "Vàlida"), NOVALIDO(84L, "Incorrecta");

    private final Long id;
    private final String nombre;

    TipoEstadoCuota(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

}