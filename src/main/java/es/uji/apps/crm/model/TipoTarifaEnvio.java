package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_TARIFAS_TIPOS_ENVIOS")
public class TipoTarifaEnvio implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "asunto")
    private String asunto;

    @Column(name = "cuerpo")
    private String cuerpo;

    @Column(name = "desde")
    private String desde;

    @Column(name = "responder_a")
    private String responderA;

    @Column(name = "tipo_correo_envio")
    private Long tipoCorreoEnvio;

    @ManyToOne
    @JoinColumn(name = "TIPO_TARIFA_ID")
    private TipoTarifa tipoTarifa;

    @OneToMany(mappedBy = "tarifaTipoEnvio")
    private Set<TipoTarifaEnvioProgramado> tipoTarifaEnvioProgramados;

    public TipoTarifaEnvio() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getResponderA() {
        return responderA;
    }

    public void setResponderA(String responderA) {
        this.responderA = responderA;
    }

    public TipoTarifa getTipoTarifa() {
        return tipoTarifa;
    }

    public void setTipoTarifa(TipoTarifa tipoTarifa) {
        this.tipoTarifa = tipoTarifa;
    }

    public Set<TipoTarifaEnvioProgramado> getTipoTarifaEnvioProgramados() {
        return tipoTarifaEnvioProgramados;
    }

    public void setTipoTarifaEnvioProgramados(Set<TipoTarifaEnvioProgramado> tipoTarifaEnvioProgramados) {
        this.tipoTarifaEnvioProgramados = tipoTarifaEnvioProgramados;
    }

    public Long getTipoCorreoEnvio() {
        return tipoCorreoEnvio;
    }

    public void setTipoCorreoEnvio(Long tipoCorreoEnvio) {
        this.tipoCorreoEnvio = tipoCorreoEnvio;
    }


}