package es.uji.apps.crm.model.domains;

public enum TipoAccionTarifaEnvioProgramado {
    ANTES(1L, "Abans"), DESPUES(2L, "Despues");

    private final Long id;
    private final String nombre;

    TipoAccionTarifaEnvioProgramado(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

}