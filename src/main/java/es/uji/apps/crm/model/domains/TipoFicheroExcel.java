package es.uji.apps.crm.model.domains;

public enum TipoFicheroExcel
{
    XLS, XLSX;
}