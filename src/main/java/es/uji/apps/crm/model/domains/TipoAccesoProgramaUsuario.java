package es.uji.apps.crm.model.domains;

public enum TipoAccesoProgramaUsuario {
    ADMIN("ADMIN"), USUARIO("USER"), ACTOS("ACTOS");

    private final String nombre;

    TipoAccesoProgramaUsuario(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

}