package es.uji.apps.crm.model.domains;

public enum TipoFechaTarifaEnvioProgramado {
    FECHAINICIORECIBO(1L), FECHAFINRECIBO(2L), FECHAFINTARIFA(3L);

    private final Long id;

    TipoFechaTarifaEnvioProgramado(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

}
