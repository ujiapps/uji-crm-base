package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CAMPANYAS database table.
 */

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_ENVIOS_IMAGENES")
public class EnvioImagen implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ENVIO_ID")
    private Envio envio;

    @ManyToOne
    @JoinColumn(name = "TARIFA_ENVIO_ID")
    private TipoTarifaEnvio tarifaEnvio;

    @ManyToOne
    @JoinColumn(name = "CARTA_ID")
    private CampanyaCarta carta;

    private String nombre;
    private String referencia;

    public EnvioImagen() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Envio getEnvio() {
        return envio;
    }

    public void setEnvio(Envio envio) {
        this.envio = envio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public TipoTarifaEnvio getTarifaEnvio() {
        return tarifaEnvio;
    }

    public void setTarifaEnvio(TipoTarifaEnvio tarifaEnvio) {
        this.tarifaEnvio = tarifaEnvio;
    }

    public CampanyaCarta getCarta() {
        return carta;
    }

    public void setCarta(CampanyaCarta carta) {
        this.carta = carta;
    }
}