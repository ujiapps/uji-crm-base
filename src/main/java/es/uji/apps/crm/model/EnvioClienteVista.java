package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.xpath.operations.Bool;
import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name = "CRM_VW_ENVIOS_CLIENTES")
public class EnvioClienteVista implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "CLIENTE_ID")
    private Long clienteId;

    @Column(name = "IDENTIFICACION")
    private String identificacion;
    @Column(name = "NOMBRE")
    private String nombre;
    @Id
    @Column(name = "CORREO")
    private String correo;

    @ManyToOne
    @JoinColumn(name = "ENVIO_ID")
    private Envio envio;

    @ManyToOne
    @JoinColumn(name = "ESTADO_CORREO_ID")
    private Tipo estadoCorreo;

    @Column(name="RECIBE_CORREO")
    private Boolean recibeCorreo;

    public EnvioClienteVista() {
    }

    public Long getId() {
        return id;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public Envio getEnvio() {
        return envio;
    }

    public Tipo getEstadoCorreo() {
        return estadoCorreo;
    }

    public Boolean getRecibeCorreo() {
        return recibeCorreo;
    }

    public Boolean isRecibeCorreo() {
        return recibeCorreo;
    }

    public Long getClienteId() {
        return clienteId;
    }
}