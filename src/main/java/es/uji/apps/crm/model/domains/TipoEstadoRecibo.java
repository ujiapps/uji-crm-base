package es.uji.apps.crm.model.domains;

public class TipoEstadoRecibo {

    public static final Long PENDIENTEPAGO = 1L;
    public static final Long MOROSO = 2L;
    public static final Long PENDIENTEENVIO = 3L;
    public static final Long PAGADO = 4L;

//    private String id;
//
//    TipoEstadoRecibo(String i) {
//
//    }
//
//    public String getId() {
//        return id;
//    }
}
