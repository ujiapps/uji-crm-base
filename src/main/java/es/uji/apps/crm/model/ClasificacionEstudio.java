package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_VW_CLASIFICACION_ESTUDIOS")
public class ClasificacionEstudio implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ESTUDIO_NOMBRE")
    private String nombre;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "TIT_ID")
    private Long estudioId;

    @Column(name = "TIENEITEMS")
    private Long tieneItems;
//
//    @ManyToOne
//    @JoinColumn(name="TIT_ID")
//    private EstudioUJI estudioUJI;

    @OneToMany(mappedBy = "clasificacionEstudio")
    private Set<ClienteTitulacionesUJI> clientesTitulacionesUJI;

//    @OneToMany(mappedBy = "clasificacionEstudio")
//    private Set<ItemClasificacion> itemsClasificacion;

    public ClasificacionEstudio() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<ClienteTitulacionesUJI> getClientesTitulacionesUJI() {
        return clientesTitulacionesUJI;
    }

    public void setClientesTitulacionesUJI(Set<ClienteTitulacionesUJI> clientesTitulacionesUJI) {
        this.clientesTitulacionesUJI = clientesTitulacionesUJI;
    }

    public Long getTieneItems() {
        return tieneItems;
    }

    public void setTieneItems(Long tieneItems) {
        this.tieneItems = tieneItems;
    }

    public Long getEstudioId() {
        return estudioId;
    }

    public void setEstudioId(Long estudioId) {
        this.estudioId = estudioId;
    }

    public String getTipoNombre() {

        switch (this.getTipo())
        {
            case "G":
                return "Grau";
            case "120C":
                return "+120 Crédits";
            case "O":
                return "Intercanvi";
            case "D":
                return "Doctorat";
            case "MC":
                return "Curs d\'especialització o de expert";
            case "MO":
                return "Master Oficial";
            case "MP":
                return "Master Propi";
            default:
                return "";
        }
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    //    public Set<ItemClasificacion> getItemsClasificacion() {
//        return itemsClasificacion;
//    }
//
//    public void setItemsClasificacion(Set<ItemClasificacion> itemsClasificacion) {
//        this.itemsClasificacion = itemsClasificacion;
//    }

    //    public EstudioUJI getEstudioUJI()
//    {
//        return estudioUJI;
//    }
//
//    public void setEstudioUJI(EstudioUJI estudioUJI)
//    {
//        this.estudioUJI = estudioUJI;
//    }
}