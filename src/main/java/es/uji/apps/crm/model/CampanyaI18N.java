package es.uji.apps.crm.model;

public class CampanyaI18N {
    String lang;
    String text;

    public CampanyaI18N(String lang, String text) {
        this.lang = lang;
        this.text = text;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}