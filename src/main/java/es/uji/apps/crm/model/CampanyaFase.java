package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CAMPANYAS_FASES database table.
 */

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_FASES")

public class CampanyaFase implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INI")
    private Date fechaIni;

    // bi-directional many-to-one association to Campanya
    @ManyToOne
    private Campanya campanya;

    // bi-directional many-to-one association to Fase
    @ManyToOne
    private Fase fase;

    // bi-directional many-to-one association to CampanyaSeguimiento
    @OneToMany(mappedBy = "campanyaFase")
    private Set<CampanyaSeguimiento> campanyasSeguimientos;

//    private static CampanyaFaseDAO campanyaFaseDAO;

//    @Autowired
//    public void setCampanyaFaseDAO(CampanyaFaseDAO campanyaFaseDAO)
//    {
//        CampanyaFase.campanyaFaseDAO = campanyaFaseDAO;
//    }

    public CampanyaFase() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaIni() {
        return this.fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Campanya getCampanya() {
        return this.campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

    public Fase getFase() {
        return this.fase;
    }

    public void setFase(Fase fase) {
        this.fase = fase;
    }

    public Set<CampanyaSeguimiento> getCampanyasSeguimientos() {
        return this.campanyasSeguimientos;
    }

    public void setCampanyasSeguimientos(Set<CampanyaSeguimiento> campanyasSeguimientos) {
        this.campanyasSeguimientos = campanyasSeguimientos;
    }

}