package es.uji.apps.crm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name = "CRM_ENVIOS_CRITERIO_EST_NO_UJI")
public class EnvioCriterioEstudioNoUJI {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ANYO_FINALIZACION_INICIO")
    private Long anyoFinalizacionEstudioNoUJIInicio;

    @Column(name = "ANYO_FINALIZACION_FIN")
    private Long anyoFinalizacionEstudioNoUJIFin;

    @ManyToOne
    @JoinColumn(name = "ENVIO_CRITERIO_ID")
    private EnvioCriterio envioCriterio;

    @ManyToOne
    @JoinColumn(name = "ESTUDIO_ID")
    private TipoEstudio estudioNoUJI;

    public EnvioCriterioEstudioNoUJI() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EnvioCriterio getEnvioCriterio() {
        return envioCriterio;
    }

    public void setEnvioCriterio(EnvioCriterio envioCriterio) {
        this.envioCriterio = envioCriterio;
    }

    public TipoEstudio getEstudioNoUJI() {
        return estudioNoUJI;
    }

    public void setEstudioNoUJI(TipoEstudio estudioNoUJI) {
        this.estudioNoUJI = estudioNoUJI;
    }

    public Long getAnyoFinalizacionEstudioNoUJIInicio() {
        return anyoFinalizacionEstudioNoUJIInicio;
    }

    public void setAnyoFinalizacionEstudioNoUJIInicio(Long anyoFinalizacionEstudioNoUJIInicio) {
        this.anyoFinalizacionEstudioNoUJIInicio = anyoFinalizacionEstudioNoUJIInicio;
    }

    public Long getAnyoFinalizacionEstudioNoUJIFin() {
        return anyoFinalizacionEstudioNoUJIFin;
    }

    public void setAnyoFinalizacionEstudioNoUJIFin(Long anyoFinalizacionEstudioNoUJIFin) {
        this.anyoFinalizacionEstudioNoUJIFin = anyoFinalizacionEstudioNoUJIFin;
    }
}