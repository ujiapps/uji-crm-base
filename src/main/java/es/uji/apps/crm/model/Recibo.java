package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_EXT_RECIBOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_EXT_RECIBOS")
public class Recibo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CODIFICACION")
    private String codificacion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_CREACION")
    private Date fechaCreacion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_PAGO")
    private Date fechaPago;

    @Column(name = "IMPORTE_NETO")
    private Integer importeNeto;

    @Column(name = "TIPO_RECIBO_ID")
    private Integer tipoRecibo;

    @Column(name = "TIPO_RECIBO_NOMBRE")
    private String tipoReciboNombre;

    @Column(name = "TIPO_COBRO_ID")
    private Integer tipoCobro;

    @Column(name = "TIPO_COBRO_NOMBRE")
    private String tipoCobroNombre;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_PAGO_LIMITE")
    private Date fechaPagoLimite;

    private String observaciones;

    private String descripcion;

    private String idioma;

    private Boolean moroso;

    @Column(name = "REFERENCIA_ID")
    private Long referencia;

    @Column(name = "TPV_CORREO")
    private String correo;

    @Column(name = "CUENTA_BANCARIA_ABONO")
    private String ibanAbono;

    @Column(name = "CTA_CLIENTE")
    private String ibanDomiciliacion;

    private String pie;

    private String destinatario;

    private Long emisora_id;

    private Long exportado;

    @Column(name = "CODIGO_PAGO")
    private String codigoPago;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private Persona persona;

    @OneToMany(mappedBy = "recibo")
    private Set<Linea> lineas;

    @OneToMany(mappedBy = "recibo")
    private Set<MovimientoRecibo> movimientosRecibo;

    public Recibo() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Integer getImporteNeto() {
        return importeNeto;
    }

    public void setImporteNeto(Integer importeNeto) {
        this.importeNeto = importeNeto;
    }

    public String getTipoReciboNombre() {
        return tipoReciboNombre;
    }

    public void setTipoReciboNombre(String tipoReciboNombre) {
        this.tipoReciboNombre = tipoReciboNombre;
    }

    public Integer getTipoCobro() {
        return tipoCobro;
    }

    public void setTipoCobro(Integer tipoCobro) {
        this.tipoCobro = tipoCobro;
    }

    public Date getFechaPagoLimite() {
        return fechaPagoLimite;
    }

    public void setFechaPagoLimite(Date fechaPagoLimite) {
        this.fechaPagoLimite = fechaPagoLimite;
    }

    public Integer getTipoRecibo() {
        return tipoRecibo;
    }

    public void setTipoRecibo(Integer tipoRecibo) {
        this.tipoRecibo = tipoRecibo;
    }

    public String getTipoCobroNombre() {
        return tipoCobroNombre;
    }

    public void setTipoCobroNombre(String tipoCobroNombre) {
        this.tipoCobroNombre = tipoCobroNombre;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getIbanAbono() {
        return ibanAbono;
    }

    public void setIbanAbono(String ibanAbono) {
        this.ibanAbono = ibanAbono;
    }

    public String getIbanDomiciliacion() {
        return ibanDomiciliacion;
    }

    public void setIbanDomiciliacion(String ibanDomiciliacion) {
        this.ibanDomiciliacion = ibanDomiciliacion;
    }

    public String getPie() {
        return pie;
    }

    public void setPie(String pie) {
        this.pie = pie;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public Set<Linea> getLineas() {
        return lineas;
    }

    public void setLineas(Set<Linea> lineas) {
        this.lineas = lineas;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Set<MovimientoRecibo> getMovimientosRecibo() {
        return movimientosRecibo;
    }

    public void setMovimientosRecibo(Set<MovimientoRecibo> movimientosRecibo) {
        this.movimientosRecibo = movimientosRecibo;
    }

    public Long getEmisora_id() {
        return emisora_id;
    }

    public void setEmisora_id(Long emisora_id) {
        this.emisora_id = emisora_id;
    }

    public Boolean getMoroso() {
        return moroso;
    }

    public void setMoroso(Boolean moroso) {
        this.moroso = moroso;
    }

    public Long getExportado() {
        return exportado;
    }

    public void setExportado(Long exportado) {
        this.exportado = exportado;
    }

    public String getCodificacion() {
        return codificacion;
    }

    public void setCodificacion(String codificacion) {
        this.codificacion = codificacion;
    }

    public String getCodigoPago() {
        return codigoPago;
    }

    public void setCodigoPago(String codigoPago) {
        this.codigoPago = codigoPago;
    }

    public Long getReferencia() {
        return referencia;
    }

    public void setReferencia(Long referencia) {
        this.referencia = referencia;
    }
}