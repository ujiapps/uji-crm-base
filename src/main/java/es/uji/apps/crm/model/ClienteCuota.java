package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CLIENTES_CUOTAS")
public class ClienteCuota implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;

    private Integer importe;

    @Column(name = "recibo_crm_id")
    private Long reciboCRM;

    @Column(name = "ESTADO")
    private Long estado;

    @Column(name = "recibo_id")
    private Long reciboId;

    private String codificacion;

//    @ManyToOne
//    @JoinColumn(name = "TARIFA_ID")
//    private Tarifa tarifa;

    @ManyToOne
    @JoinColumn(name = "CLIENTE_ID")
    private Cliente cliente;

    @ManyToOne
    @JoinColumn(name = "CLIENTE_TARIFA_ID")
    private ClienteTarifa clienteTarifa;


    public ClienteCuota() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

//    public Tarifa getTarifa() {
//        return tarifa;
//    }
//
//    public void setTarifa(Tarifa tarifa) {
//        this.tarifa = tarifa;
//    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Integer getImporte() {
        return importe;
    }

    public void setImporte(Integer importe) {
        this.importe = importe;
    }

    public Long getReciboCRM() {
        return reciboCRM;
    }

    public void setReciboCRM(Long reciboCRM) {
        this.reciboCRM = reciboCRM;
    }

    public String getCodificacion() {
        return codificacion;
    }

    public void setCodificacion(String codificacion) {
        this.codificacion = codificacion;
    }

    public Long getReciboId() {
        return reciboId;
    }

    public void setReciboId(Long reciboId) {
        this.reciboId = reciboId;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    public ClienteTarifa getClienteTarifa() {
        return clienteTarifa;
    }

    public void setClienteTarifa(ClienteTarifa clienteTarifa) {
        this.clienteTarifa = clienteTarifa;
    }
}