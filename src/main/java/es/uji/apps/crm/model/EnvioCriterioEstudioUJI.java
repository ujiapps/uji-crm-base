package es.uji.apps.crm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name = "CRM_ENVIOS_CRITERIOS_EST_UJI")
public class EnvioCriterioEstudioUJI {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ANYO_FINALIZACION_INICIO")
    private Long anyoFinalizacionEstudioUJIInicio;

    @Column(name = "ANYO_FINALIZACION_FIN")
    private Long anyoFinalizacionEstudioUJIFin;

    @ManyToOne
    @JoinColumn(name = "ENVIO_CRITERIO_ID")
    private EnvioCriterio envioCriterio;

    @ManyToOne
    @JoinColumn(name = "CLASIFICACION_ID")
    private ClasificacionEstudio estudioUJI;

    public EnvioCriterioEstudioUJI() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EnvioCriterio getEnvioCriterio() {
        return envioCriterio;
    }

    public void setEnvioCriterio(EnvioCriterio envioCriterio) {
        this.envioCriterio = envioCriterio;
    }

    public ClasificacionEstudio getEstudioUJI() {
        return estudioUJI;
    }

    public void setEstudioUJI(ClasificacionEstudio estudioUJI) {
        this.estudioUJI = estudioUJI;
    }

    public Long getAnyoFinalizacionEstudioUJIInicio() {
        return anyoFinalizacionEstudioUJIInicio;
    }

    public void setAnyoFinalizacionEstudioUJIInicio(Long anyoFinalizacionEstudioUJIInicio) {
        this.anyoFinalizacionEstudioUJIInicio = anyoFinalizacionEstudioUJIInicio;
    }

    public Long getAnyoFinalizacionEstudioUJIFin() {
        return anyoFinalizacionEstudioUJIFin;
    }

    public void setAnyoFinalizacionEstudioUJIFin(Long anyoFinalizacionEstudioUJIFin) {
        this.anyoFinalizacionEstudioUJIFin = anyoFinalizacionEstudioUJIFin;
    }
}