package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_SEGUIMIENTOS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_SEGUIMIENTOS")
public class Seguimiento implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String descripcion;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    private String nombre;

    // bi-directional many-to-one association to Cliente
    @ManyToOne
    @JoinColumn(name = "CLIENTE_ID")
    private Cliente cliente;

    // bi-directional many-to-one association to SeguimientoTipo
    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private SeguimientoTipo seguimientoTipo;

    // bi-directional many-to-one association to Campanya
    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private Campanya campanya;

    // bi-directional many-to-one association to SeguimientoFichero
    @OneToMany(mappedBy = "seguimiento")
    private Set<SeguimientoFichero> seguimientoFicheros;

    public Seguimiento() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public SeguimientoTipo getSeguimientoTipo() {
        return this.seguimientoTipo;
    }

    public void setSeguimientoTipo(SeguimientoTipo seguimientoTipo) {
        this.seguimientoTipo = seguimientoTipo;
    }

    public Set<SeguimientoFichero> getSeguimientoFicheros() {
        return seguimientoFicheros;
    }

    public void setSeguimientoFicheros(Set<SeguimientoFichero> seguimientoFicheros) {
        this.seguimientoFicheros = seguimientoFicheros;
    }

    public Campanya getCampanya() {
        return campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

}
