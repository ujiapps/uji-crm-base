package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_FASES database table.
 */

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_FASES")

public class Fase implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer activa;

    private String nombre;

    // bi-directional many-to-one association to CampanyaFase
    @OneToMany(mappedBy = "fase")
    private Set<CampanyaFase> campanyasFases;

//    private static FaseDAO faseDAO;

//    @Autowired
//    public void setFaseDAO(FaseDAO faseDAO)
//    {
//        Fase.faseDAO = faseDAO;
//    }

    public Fase() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getActiva() {
        return this.activa;
    }

    public void setActiva(Integer activa) {
        this.activa = activa;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<CampanyaFase> getCampanyasFases() {
        return this.campanyasFases;
    }

    public void setCampanyasFases(Set<CampanyaFase> campanyasFases) {
        this.campanyasFases = campanyasFases;
    }

}