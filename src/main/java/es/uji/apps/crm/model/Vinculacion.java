package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_VINCULACIONES database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_VINCULACIONES")
public class Vinculacion implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // bi-directional many-to-one association to Cliente
    @ManyToOne
    private Cliente cliente1;

    // bi-directional many-to-one association to Cliente
    @ManyToOne
    private Cliente cliente2;

    // bi-directional many-to-one association to Tipo
    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private Tipo tipo;

    public Vinculacion() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cliente getCliente1() {
        return this.cliente1;
    }

    public void setCliente1(Cliente cliente1) {
        this.cliente1 = cliente1;
    }

    public Cliente getCliente2() {
        return this.cliente2;
    }

    public void setCliente2(Cliente cliente2) {
        this.cliente2 = cliente2;
    }

    public Tipo getTipo() {
        return this.tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

}