package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_EXT_PERSONAS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_EXT_PERSONAS")
public class Persona implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String identificacion;

    private String nombre;

    private String apellidos;

    private String correo;
    @Column(name = "CORREO_OFICIAL")
    private String correoOficial;


    private String movil;

    private String postal;

    private String cuenta;

    private String busqueda;

    @ManyToOne
    @JoinColumn(name = "SEXO")
    private Tipo sexo;

    @Column(name = "fecha_nacimiento")
    private Date fechaNacimiento;

    @Column(name = "codigo_postal")
    private String codigoPostal;

    @ManyToOne
    @JoinColumn(name = "NACIONALIDAD_ID")
    private Pais nacionalidad;

    @ManyToOne
    @JoinColumn(name = "PAIS_POSTAL_ID")
    private Pais paisPostal;

    @ManyToOne
    @JoinColumn(name = "PROVINCIA_POSTAL_ID")
    private Provincia provinciaPostal;

    @ManyToOne
    @JoinColumn(name = "POBLACION_POSTAL_ID")
    private Poblacion poblacionPostal;

//    private String nacionalidad;

    // bi-directional many-to-one association to Cliente
    @OneToMany(mappedBy = "persona")
    private Set<ClienteGeneral> clientes;

    @OneToMany(mappedBy = "persona")
    private Set<VwVinculacionPersona> vinculacionesPersona;

    @OneToMany(mappedBy = "persona")
    private Set<CursoInscripcion> cursoInscripciones;

    @OneToMany(mappedBy = "persona")
    private Set<CuentaCorportativa> cuentasCorporativas;

    @OneToMany(mappedBy = "persona")
    private Set<Recibo> recibos;

    @OneToMany(mappedBy = "persona")
    private Set<Linea> lineas;

    @OneToMany(mappedBy = "persona")
    private Set<CampanyaClienteArchivo> campanyaClientesArchivo;

    public Persona() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public Tipo getSexo() {
        return sexo;
    }

    public void setSexo(Tipo sexo) {
        this.sexo = sexo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public Pais getPaisPostal() {
        return paisPostal;
    }

    public void setPaisPostal(Pais paisPostal) {
        this.paisPostal = paisPostal;
    }

    public Provincia getProvinciaPostal() {
        return provinciaPostal;
    }

    public void setProvinciaPostal(Provincia provinciaPostal) {
        this.provinciaPostal = provinciaPostal;
    }

    public Poblacion getPoblacionPostal() {
        return poblacionPostal;
    }

    public void setPoblacionPostal(Poblacion poblacionPostal) {
        this.poblacionPostal = poblacionPostal;
    }

    public Set<ClienteGeneral> getClientes() {
        return clientes;
    }

    public void setClientes(Set<ClienteGeneral> clientes) {
        this.clientes = clientes;
    }

    public Set<VwVinculacionPersona> getVinculacionesPersona() {
        return vinculacionesPersona;
    }

    public void setVinculacionesPersona(Set<VwVinculacionPersona> vinculacionesPersona) {
        this.vinculacionesPersona = vinculacionesPersona;
    }

    public Set<CursoInscripcion> getCursoInscripciones() {
        return cursoInscripciones;
    }

    public void setCursoInscripciones(Set<CursoInscripcion> cursoInscripciones) {
        this.cursoInscripciones = cursoInscripciones;
    }

    public Set<CuentaCorportativa> getCuentasCorporativas() {
        return cuentasCorporativas;
    }

    public void setCuentasCorporativas(Set<CuentaCorportativa> cuentasCorporativas) {
        this.cuentasCorporativas = cuentasCorporativas;
    }

    public Set<Recibo> getRecibos() {
        return recibos;
    }

    public void setRecibos(Set<Recibo> recibos) {
        this.recibos = recibos;
    }

    public Set<Linea> getLineas() {
        return lineas;
    }

    public void setLineas(Set<Linea> lineas) {
        this.lineas = lineas;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public String getApellidosNombre() {
        return this.apellidos + ", " + this.nombre;
    }

    public String getCorreoOficial() {
        return correoOficial;
    }

    public void setCorreoOficial(String correoOficial) {
        this.correoOficial = correoOficial;
    }

    public Pais getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(Pais nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Set<CampanyaClienteArchivo> getCampanyaClientesArchivo() {
        return campanyaClientesArchivo;
    }

    public void setCampanyaClientesArchivo(Set<CampanyaClienteArchivo> campanyaClientesArchivo) {
        this.campanyaClientesArchivo = campanyaClientesArchivo;
    }
}