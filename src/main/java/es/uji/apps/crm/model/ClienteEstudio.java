package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CRM_EXT_PERSONAS_TITULACIONES")
public class ClienteEstudio implements Serializable {
    @Id
    private Long id;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "PERSONA_ID")
    private Long persona;

    @Column(name = "CURSO_ACA_INI")
    private Long cursoAcaIni;

    @Column(name = "CURSO_ACA_FIN")
    private Long cursoAcaFin;

    @Column(name = "NOMBRE_ES")
    private String estudio;

    @Column(name = "NOMBRE_CA")
    private String estudioCa;

    @Column(name = "NOMBRE_UK")
    private String estudioUk;

    @Column(name = "FECHA_TITULO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaTitulo;

    @Column(name = "EST_ID")
    private Long itemEstudio;

    @Column(name = "TIT_ID")
    private Long estudioId;

    @Column(name = "TIPO_AGRUPACION")
    private String tipoEstudio;

    @Column(name = "TIPO_BECA_ID")
    private Long tipoBecaId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersona() {
        return persona;
    }

    public void setPersona(Long persona) {
        this.persona = persona;
    }

    public Long getCursoAcaIni() {
        return cursoAcaIni;
    }

    public void setCursoAcaIni(Long cursoAcaIni) {
        this.cursoAcaIni = cursoAcaIni;
    }

    public Long getCursoAcaFin() {
        return cursoAcaFin;
    }

    public void setCursoAcaFin(Long cursoAcaFin) {
        this.cursoAcaFin = cursoAcaFin;
    }

    public String getEstudio() {
        return estudio;
    }

    public void setEstudio(String estudio) {
        this.estudio = estudio;
    }

    public Date getFechaTitulo() {
        return fechaTitulo;
    }

    public void setFechaTitulo(Date fechaTitulo) {
        this.fechaTitulo = fechaTitulo;
    }

    public String getEstudioCa() {
        return estudioCa;
    }

    public void setEstudioCa(String estudioCa) {
        this.estudioCa = estudioCa;
    }

    public String getEstudioUk() {
        return estudioUk;
    }

    public void setEstudioUk(String estudioUk) {
        this.estudioUk = estudioUk;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getItemEstudio() {
        return itemEstudio;
    }

    public void setItemEstudio(Long itemEstudio) {
        this.itemEstudio = itemEstudio;
    }

    public Long getEstudioId() {
        return estudioId;
    }

    public void setEstudioId(Long estudioId) {
        this.estudioId = estudioId;
    }

    public String getTipoEstudio() {
        return tipoEstudio;
    }

    public void setTipoEstudio(String tipoEstudio) {
        this.tipoEstudio = tipoEstudio;
    }

    public Long getTipoBecaId() {
        return tipoBecaId;
    }

    public void setTipoBecaId(Long tipoBecaId) {
        this.tipoBecaId = tipoBecaId;
    }
}