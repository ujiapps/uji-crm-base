package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA. User: veriton1 Date: 6/03/14 Time: 15:41 To change this template use
 * File | Settings | File Templates.
 */
@Table(name = "CRM_EXT_VINCULOS_PERSONAS")
@Entity
@Component
@SuppressWarnings("serial")
public class VwVinculacionPersona implements Serializable {

    @Id
    private Long id;

    @Column(name = "sub_vinculo_nombre")
    private String subVinculoNombre;

    @Column(name = "vinculo_nombre")
    private String vinculoNombre;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha_alta;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha_baja;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private Persona persona;

    @ManyToOne
    @JoinColumn(name = "SUBVINCULO_ID")
    private SubVinculo subVinculo;

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public SubVinculo getSubVinculo() {
        return subVinculo;
    }

    public void setSubVinculo(SubVinculo subVinculo) {
        this.subVinculo = subVinculo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha_alta() {
        return fecha_alta;
    }

    public void setFecha_alta(Date fecha_alta) {
        this.fecha_alta = fecha_alta;
    }

    public Date getFecha_baja() {
        return fecha_baja;
    }

    public void setFecha_baja(Date fecha_baja) {
        this.fecha_baja = fecha_baja;
    }

    public String getSubVinculoNombre() {
        return subVinculoNombre;
    }

    public void setSubVinculoNombre(String subVinculoNombre) {
        this.subVinculoNombre = subVinculoNombre;
    }

    public String getVinculoNombre() {
        return vinculoNombre;
    }

    public void setVinculoNombre(String vinculoNombre) {
        this.vinculoNombre = vinculoNombre;
    }
}