package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CRM_PREINSCRITOS_PREMIUM_ITEMS")
public class PreinscripcionItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "PREINSCRIPCION_ID")
    private Long preinscripcionId;

    @Column(name = "ITEM_ID")
    private Long itemId;

    @Column(name = "GRUPO_ID")
    private Long grupoId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPreinscripcionId() {
        return preinscripcionId;
    }

    public void setPreinscripcionId(Long preinscripcionId) {
        this.preinscripcionId = preinscripcionId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Long grupoId) {
        this.grupoId = grupoId;
    }
}