package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CAMPANYAS_TEXTOS database table.
 */

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_TEXTOS")
public class CampanyaTexto implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "texto_ca")
    private String textoCa;

    @Column(name = "texto_es")
    private String textoEs;

    @Column(name = "texto_uk")
    private String textoUk;

    private String codigo;

    @OneToMany(mappedBy = "campanyaTexto")
    private Set<Campanya> campanyas;


    public CampanyaTexto() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Campanya> getCampanyas() {
        return campanyas;
    }

    public void setCampanyas(Set<Campanya> campanyas) {
        this.campanyas = campanyas;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTextoByIdioma(String idioma) {
        if (idioma == null)
        {
            idioma = "CA";
        }
        switch (idioma.toUpperCase())
        {
            case "ES":
                return getTextoEs();
            case "UK":
            case "EN":
                return getTextoUk();
            default:
                return getTextoCa();
        }
    }

    public String getTextoCa() {
        return textoCa;
    }

    public void setTextoCa(String textoCa) {
        this.textoCa = textoCa;
    }

    public String getTextoEs() {
        return textoEs;
    }

    public void setTextoEs(String textoEs) {
        this.textoEs = textoEs;
    }

    public String getTextoUk() {
        return textoUk;
    }

    public void setTextoUk(String textoUk) {
        this.textoUk = textoUk;
    }
}