package es.uji.apps.crm.model.domains;

public enum TipoCorreoEnvio {
    PERSONAL(0L, "Correu Personal"), OFICIAL(1L, "Correu Oficial");

    private final Long id;
    private final String nombre;

    TipoCorreoEnvio(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}
