package es.uji.apps.crm.model.datos;

import java.util.List;

import es.uji.apps.crm.model.ClienteItem;

public class Academicos {

    private final static Long TIPO_NIVEL_ESTUDIOS_ID = 3229080L;

    private Long nivelEstudios;

    public Academicos(List<ClienteItem> items) {
        getDatosAcademicos(items);

    }

    private void getDatosAcademicos(List<ClienteItem> items) {
        items.stream().forEach(item -> {
            if (item.getItem().getGrupo().getId().equals(TIPO_NIVEL_ESTUDIOS_ID))
            {
                this.nivelEstudios = item.getItem().getId();
            }
        });
    }

    public Long getNivelEstudios() {
        return nivelEstudios;
    }

    public void setNivelEstudios(Long nivelEstudios) {
        this.nivelEstudios = nivelEstudios;
    }
}