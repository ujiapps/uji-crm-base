package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_EXT_ESTUDIOS_UJI")
public class EstudioUJI implements Serializable {
    @Id
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "TIPO_ESTUDIO")
    private String tipoEstudio;

    @Column(name = "CLASIFICACION_ID")
    private Long clasificacion;

    public EstudioUJI() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoEstudio() {
        return tipoEstudio;
    }

    public void setTipoEstudio(String tipoEstudio) {
        this.tipoEstudio = tipoEstudio;
    }

    public Long getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(Long clasificacion) {
        this.clasificacion = clasificacion;
    }
}