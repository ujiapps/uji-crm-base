package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_VW_CAMPANYA_CLIENTES_ITEMS")
public class CampanyaClienteItem implements Serializable {
    @Id
    private Long id;

    private Boolean correo;

    private Boolean postal;

    // bi-directional many-to-one association to Cliente
    @ManyToOne
    @JoinColumn(name = "CLIENTE_ID")
    private Cliente cliente;

    // bi-directional many-to-one association to Item
    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    private Item item;

    public CampanyaClienteItem() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getCorreo() {
        return this.correo;
    }

    public void setCorreo(Boolean correo) {
        this.correo = correo;
    }

    public Boolean isCorreo() {
        return this.correo;
    }

    public Boolean getPostal() {
        return postal;
    }

    public void setPostal(Boolean postal) {
        this.postal = postal;
    }

    public Boolean isPostal() {
        return this.postal;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Item getItem() {
        return this.item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}