package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CLIENTES_ITEMS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_ITEMS_ITEMS")
public class ItemItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // bi-directional many-to-one association to Cliente
    @ManyToOne
    @JoinColumn(name = "ITEM_ID_ORIGEN")
    private Item itemOrigen;

    // bi-directional many-to-one association to Item
    @ManyToOne
    @JoinColumn(name = "ITEM_ID_DESTINO")
    private Item itemDestino;

    public ItemItem() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Item getItemDestino() {
        return itemDestino;
    }

    public void setItemDestino(Item itemDestino) {
        this.itemDestino = itemDestino;
    }

    public Item getItemOrigen() {
        return itemOrigen;
    }

    public void setItemOrigen(Item itemOrigen) {
        this.itemOrigen = itemOrigen;
    }

}