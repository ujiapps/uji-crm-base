package es.uji.apps.crm.model.domains;

public enum TipoEstudioNoUJI {
    ELEMENTAL(4L, "ELEM", "Elementales"), MEDIOS(5L, "MIT", "Mitjans"), SUPERIORESNOUNI(6L, "NO", "Superiors no Universitaris"),
    GRADO(1L, "GRADO", "grau"), POSTGRADO(2L, "POST", "PostGrau"), DOCTORADO(3L, "DOC", "Doctorat");

    private final Long id;
    private final String nombre;
    private final String descripcion;


    TipoEstudioNoUJI(Long id, String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return nombre;
    }
}