package es.uji.apps.crm.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_TIPOS_ACCESO database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CAMPANYAS_ESTADOS")
public class TipoEstadoCampanya implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    @Column(name = "accion")
    private String accion;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private Campanya campanya;

    @OneToMany(mappedBy = "tipoEstadoCampanya")
    private Set<TipoEstadoCampanyaMotivo> tipoEstadoCampanyaMotivos;

    // bi-directional many-to-one association to CampanyasClientes
    @OneToMany(mappedBy = "campanyaClienteTipo")
    private Set<CampanyaCliente> campanyaClienteTipo;

    @OneToMany(mappedBy = "campanyaCartaTipo")
    private Set<CampanyaCarta> campanyaCartaTipo;

    @OneToMany(mappedBy = "campanyaEnvioTipo")
    private Set<CampanyaEnvioAuto> campanyaEnvioTipo;

    @OneToMany(mappedBy = "estadoDestino")
    private Set<CampanyaCampanya> campanyaEstadosDestino;

    @OneToMany(mappedBy = "estadoOrigen")
    private Set<CampanyaCampanya> campanyaEstadosOrigen;

    @OneToMany(mappedBy = "estadoDestino")
    private Set<CampanyaFormulario> campanyaFormularioDestinos;

    @OneToMany(mappedBy = "estadoOrigen")
    private Set<CampanyaFormularioEstadoOrigen> campanyaFormularioOrigenes;

    @OneToMany(mappedBy = "campanyaEstado")
    private Set<EnvioCriterioCampanyaEstado> envioCriterioCampanyaEstados;

    public TipoEstadoCampanya() {
    }

    public TipoEstadoCampanya(Campanya campanya, String accion, String nombre) {
        this.campanya = campanya;
        this.accion = accion;
        this.nombre = nombre;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public Campanya getCampanya() {
        return campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

    public Set<TipoEstadoCampanyaMotivo> getTipoEstadoCampanyaMotivos() {
        return tipoEstadoCampanyaMotivos;
    }

    public void setTipoEstadoCampanyaMotivos(Set<TipoEstadoCampanyaMotivo> tipoEstadoCampanyaMotivos) {
        this.tipoEstadoCampanyaMotivos = tipoEstadoCampanyaMotivos;
    }

    public Set<CampanyaCliente> getCampanyaClienteTipo() {
        return campanyaClienteTipo;
    }

    public void setCampanyaClienteTipo(Set<CampanyaCliente> campanyaClienteTipo) {
        this.campanyaClienteTipo = campanyaClienteTipo;
    }

    public Set<CampanyaCarta> getCampanyaCartaTipo() {
        return campanyaCartaTipo;
    }

    public void setCampanyaCartaTipo(Set<CampanyaCarta> campanyaCartaTipo) {
        this.campanyaCartaTipo = campanyaCartaTipo;
    }

    public Set<CampanyaEnvioAuto> getCampanyaEnvioTipo() {
        return campanyaEnvioTipo;
    }

    public void setCampanyaEnvioTipo(Set<CampanyaEnvioAuto> campanyaEnvioTipo) {
        this.campanyaEnvioTipo = campanyaEnvioTipo;
    }

    public Set<CampanyaCampanya> getCampanyaEstadosDestino() {
        return campanyaEstadosDestino;
    }

    public void setCampanyaEstadosDestino(Set<CampanyaCampanya> campanyaEstadosDestino) {
        this.campanyaEstadosDestino = campanyaEstadosDestino;
    }

    public Set<CampanyaCampanya> getCampanyaEstadosOrigen() {
        return campanyaEstadosOrigen;
    }

    public void setCampanyaEstadosOrigen(Set<CampanyaCampanya> campanyaEstadosOrigen) {
        this.campanyaEstadosOrigen = campanyaEstadosOrigen;
    }

    public Set<CampanyaFormulario> getCampanyaFormularioDestinos() {
        return campanyaFormularioDestinos;
    }

    public void setCampanyaFormularioDestinos(Set<CampanyaFormulario> campanyaFormularioDestinos) {
        this.campanyaFormularioDestinos = campanyaFormularioDestinos;
    }

    public Set<CampanyaFormularioEstadoOrigen> getCampanyaFormularioOrigenes() {
        return campanyaFormularioOrigenes;
    }

    public void setCampanyaFormularioOrigenes(Set<CampanyaFormularioEstadoOrigen> campanyaFormularioOrigenes) {
        this.campanyaFormularioOrigenes = campanyaFormularioOrigenes;
    }

    public Set<EnvioCriterioCampanyaEstado> getEnvioCriterioCampanyaEstados() {
        return envioCriterioCampanyaEstados;
    }

    public void setEnvioCriterioCampanyaEstados(Set<EnvioCriterioCampanyaEstado> envioCriterioCampanyaEstados) {
        this.envioCriterioCampanyaEstados = envioCriterioCampanyaEstados;
    }
}