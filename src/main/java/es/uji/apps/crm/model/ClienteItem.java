package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_CLIENTES_ITEMS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_CLIENTES_ITEMS")
public class ClienteItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

//    private Boolean bt;

    private Boolean correo;

    private Boolean postal;

//    private Boolean sms;

    // bi-directional many-to-one association to Cliente
    @ManyToOne(fetch = FetchType.LAZY)
    private Cliente cliente;

    // bi-directional many-to-one association to Item
    @ManyToOne(fetch = FetchType.LAZY)
    private Item item;

    @Column(name = "CLIENTE_ITEM_ID")
    private Long clienteItemRelacionado;

    @Column(name = "CLIENTE_DATO_ID")
    private Long clienteDato;

    @ManyToOne
    @JoinColumn(name = "CAMPANYA_ID")
    private Campanya campanya;

    public ClienteItem() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    public Boolean getBt() {
//        return this.bt;
//    }
//
//    public void setBt(Boolean bt) {
//        this.bt = bt;
//    }
//    public Boolean isBt() {
//        return this.bt;
//    }

    public Boolean getCorreo() {
        return this.correo;
    }

    public void setCorreo(Boolean correo) {
        this.correo = correo;
    }

    public Boolean isCorreo() {
        return this.correo;
    }


    public Boolean getPostal() {
        return this.postal;
    }

    public void setPostal(Boolean postal) {
        this.postal = postal;
    }

    public Boolean isPostal() {
        return this.postal;
    }
//    public Boolean getSms() {
//        return this.sms;
//    }
//
//    public void setSms(Boolean sms) {
//        this.sms = sms;
//    }
//    public Boolean isSms() {
//        return this.sms;
//    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Item getItem() {
        return this.item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Campanya getCampanya() {
        return campanya;
    }

    public void setCampanya(Campanya campanya) {
        this.campanya = campanya;
    }

    public Long getClienteItemRelacionado() {
        return clienteItemRelacionado;
    }

    public void setClienteItemRelacionado(Long clienteItemRelacionado) {
        this.clienteItemRelacionado = clienteItemRelacionado;
    }

    public Long getClienteDato() {
        return clienteDato;
    }

    public void setClienteDato(Long clienteDato) {
        this.clienteDato = clienteDato;
    }
}