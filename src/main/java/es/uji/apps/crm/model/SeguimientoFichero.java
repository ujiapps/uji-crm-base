package es.uji.apps.crm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the CRM_SEGUIMIENTOS_FICHEROS database table.
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "CRM_SEGUIMIENTOS_FICHEROS")
public class SeguimientoFichero implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

//    @Lob
//    private byte[] binario;

    @Column(name = "CONTENT_TYPE")
    private String contentType;

    @Column(name = "NOMBRE_FICHERO")
    private String nombreFichero;

    @Column(name = "REFERENCIA")
    private String referencia;

    // bi-directional many-to-one association to CampanyaSeguimiento
    @ManyToOne
    @JoinColumn(name = "CAMPANYA_SEGUIMIENTO_ID")
    private CampanyaSeguimiento campanyaSeguimiento;

    // bi-directional many-to-one association to Seguimiento
    @ManyToOne
    @JoinColumn(name = "SEGUIMIENTO_ID")
    private Seguimiento seguimiento;

    public SeguimientoFichero() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    public byte[] getBinario()
//    {
//        return this.binario;
//    }
//
//    public void setBinario(byte[] binario)
//    {
//        this.binario = binario;
//    }

    public String getContentType() {
        return this.contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getNombreFichero() {
        return this.nombreFichero;
    }

    public void setNombreFichero(String nombreFichero) {
        this.nombreFichero = nombreFichero;
    }

    public CampanyaSeguimiento getCampanyaSeguimiento() {
        return campanyaSeguimiento;
    }

    public void setCampanyaSeguimiento(CampanyaSeguimiento campanyaSeguimiento) {
        this.campanyaSeguimiento = campanyaSeguimiento;
    }

    public Seguimiento getSeguimiento() {
        return seguimiento;
    }

    public void setSeguimiento(Seguimiento seguimiento) {
        this.seguimiento = seguimiento;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
}