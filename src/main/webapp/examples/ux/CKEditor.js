Ext.define('Ext.ux.CKEditor', {
    extend: 'Ext.form.field.TextArea',
    alias: 'widget.ckeditor',

    constructor: function () {
        this.callParent(arguments);
        this.addEvents("instanceReady");
    },

    initComponent: function () {
        this.callParent(arguments);
        this.on("afterrender", function () {

            Ext.apply(this.CKConfig, {
                height: this.getHeight(),
                width: this.getWidth()
            });

            this.editor = CKEDITOR.replace(this.inputEl.id, this.CKConfig);
            this.editor.name = this.name;
            this.editor.config.toolbar = 'MyToolbarSet';
            this.editor.config.allowedContent = true;
            this.editor.config.extraAllowedContent = 'script[*]; style[*]; link[*]; meta[*]; html[*]; head[*]; title[*];' +
            'aside(*)[*]{*}; address(*)[*]{*}; article(*)[*]{*}; body(*)[*]{*}; footer(*)[*]{*}; header(*)[*]{*}; hgroup(*)[*]{*}; nav(*)[*]{*}; section(*)[*]{*}; h1(*)[*]{*}; h2(*)[*]{*}; h3(*)[*]{*}; h4(*)[*]{*}; h5(*)[*]{*}; h6(*)[*]{*};' +
            'blockquote(*)[*]{*}; dd(*)[*]{*}; div(*)[*]{*}; dl(*)[*]{*}; dt(*)[*]{*}; figcaption(*)[*]{*}; figure(*)[*]{*}; hr(*)[*]{*}; li(*)[*]{*}; main(*)[*]{*}; ol(*)[*]{*}; p(*)[*]{*}; pre(*)[*]{*}; ul(*)[*]{*};' +
            'a(*)[*]{*}; abbr(*)[*]{*}; b(*)[*]{*}; bdi(*)[*]{*}; bdo(*)[*]{*}; br(*)[*]{*}; cite(*)[*]{*}; code(*)[*]{*}; data(*)[*]{*}; dfn(*)[*]{*}; em(*)[*]{*}; i(*)[*]{*}; kbd(*)[*]{*}; ' +
            'mark(*)[*]{*}; q(*)[*]{*}; rp(*)[*]{*}; rt(*)[*]{*}; rtc(*)[*]{*}; ruby(*)[*]{*}; s(*)[*]{*}; samp(*)[*]{*}; small(*)[*]{*}; span(*)[*]{*}; strong(*)[*]{*}; sub(*)[*]{*}; sup(*)[*]{*}; time(*)[*]{*}; u(*)[*]{*}; var(*)[*]{*}; wbr(*)[*]{*};' +
            'area(*)[*]{*}; audio(*)[*]{*}; map(*)[*]{*}; track(*)[*]{*}; video(*)[*]{*};' +
            'embed(*)[*]{*}; iframe(*)[*]{*}; img(*)[*]{*}; object(*)[*]{*}; param(*)[*]{*}; source(*)[*]{*};' +
            'del(*)[*]{*}; ins(*)[*]{*};' +
            'details(*)[*]{*}; dialog(*)[*]{*}; menu(*)[*]{*}; menuitem(*)[*]{*}; summary(*)[*]{*};' +
            'button(*)[*]{*}; datalist(*)[*]{*}; fieldset(*)[*]{*}; form(*)[*]{*}; input(*)[*]{*}; keygen(*)[*]{*}; label(*)[*]{*}; legend(*)[*]{*}; meter(*)[*]{*}; optgroup(*)[*]{*}; option(*)[*]{*}; output(*)[*]{*}; progress(*)[*]{*}; select(*)[*]{*}; textarea(*)[*]{*};' +
            'caption(*)[*]{*}; col(*)[*]{*}; colgroup(*)[*]{*}; table(*)[*]{*}; tbody(*)[*]{*}; td(*)[*]{*}; tfoot(*)[*]{*}; th(*)[*]{*}; thead(*)[*]{*}; tr(*)[*]{*};';
            this.editor.config.disallowedContent = '*[dir]; base[*];';
            this.editor.config.pasteFilter = 'strong[*]; b[*]; span[*]; cite[*]; h1[*]; h2[*]; h3[*]; h4[*]; h5[*]; h6[*]; p[*]; br[*]; a[*]; dl[*]; dt[*]; dd[*]; div[*]; sub[*]; sup[*]; caption[*]; col[*]; colgroup[*]; table[*]; tbody[*]; td[*]; tfoot[*]; th[*]; thead[*]; tr[*]; li[*]; ol[*]; ul[*];';
            this.editor.config.extraPlugins = 'footnote,accessibilitydialog';
            this.editor.config.toolbar_MyToolbarSet = [
                {
                    name: 'document',
                    items: ['Source', '-', 'Preview']
                },
                {
                    name: 'clipboard',
                    items: ['Cut', 'Copy', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']
                },
                {
                    name: 'editing',
                    items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt']
                }, '/',
                {
                    name: 'insert',
                    items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar', 'Iframe']
                },
                {
                    name: 'UJI',
                    items: ['Footnote', 'AccessibilityDialog']
                },
                {
                    name: 'styles',
                    items: ['Styles', 'Format', 'FontSize', 'Font']
                }, '/',
                {
                    name: 'basicstyles',
                    items: [
                        'Bold', 'Italic', 'Subscript', 'Superscript', '-', 'RemoveFormat']
                },
                {
                    name: 'colors',
                    items: ['TextColor', 'BGColor']

                },
                {
                    name: 'tools',
                    items: ['Maximize', 'ShowBlocks']
                }, '/',
                {
                    name: 'paragraph',
                    items: [
                        'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
                        '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',
                        '-', 'BidiLtr', 'BidiRtl'
                    ]
                }
                ,
                {
                    name: 'links',
                    items: ['Link', 'Unlink', 'Anchor']
                }
            ];
            //this.editor.config.toolbar_MyToolbarSet = [['Source'], '-', ['Cut', 'Copy', 'PasteText', 'PasteFromWord'],
            //    ['Undo', 'Redo'], '-', //['Find', 'Replace'], '-',
            //    ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'], '/',
            //    ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'Image', 'Link', 'Unlink'], '-',
            //    ['NumberedList', 'BulletedList'], '-', ['Maximize']];//,

            this.editor.on("instanceReady", function () {
                this.fireEvent("instanceReady",
                    this,
                    this.editor);

            }, this);

        }, this);
    },

    onRender: function (ct, position) {
        if (!this.el) {
            this.defaultAutoCreate = {
                tag: 'textarea',
                autocomplete: 'off'
            };
        }
        this.callParent(arguments)
    },

    setValue: function (value) {
        this.callParent(arguments);
        if (this.editor) {
            this.editor.setData(value);
        }
    },

    getValue: function () {
        if (this.editor) {
            return this.editor.getData();
        } else {
            return ''
        }
    },

    setUrl: function (id) {
        this.editor.config.filebrowserBrowseUrl = 'rest' + id;
    }
});