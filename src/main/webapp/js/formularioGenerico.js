$(document).ready(function () {

    var _formularioGenerico = new FormularioGenerico();

});

$(document).on('click', '#form-crm a.add-block', function (e) {
    e.preventDefault();
    var row = $(this).closest('.row');
    var duplicado = row.clone();
    $('input[type="text"], input[type="number"], input[type="email"], textarea', duplicado).val('');
    duplicado.insertAfter(row).hide().slideDown();
});

$(document).on('click', '#form-crm a.remove-block', function (e) {
    e.preventDefault();
    var fieldset = $(this).closest('fieldset');
    if ($('[data-fieldsetdatoid="' + fieldset.attr('data-fieldsetdatoid') + '"]').length > 1) {
        $(this).closest('.row').slideUp(function () {
            $(this).remove();
        });
    }
});


FormularioGenerico = function () {

    $('.submitForm').click(this.onSubmitForm.bind(this));
    $('#form-crm').validate();
    $('#tipoIdentificacion').change(this.onChangeTipoIdentificacion.bind(this));

    var me = this;
    $('input[type=radio]').on('change', function () {
        me.mostrarCampos($(this).data('mostrar'));
        me.ocultarCampos($(this).data('ocultar'));
    });

    $('select').on('change', function () {
        var dataMostrar = $(this).find(':selected').data('mostrar');
        var dataOcultar = $(this).find(':selected').data('ocultar');
        if (dataMostrar) {
            me.mostrarCampos(dataMostrar);
        }
        if (dataOcultar)
            me.ocultarCampos(dataOcultar);
    });
}


FormularioGenerico.prototype.ocultarCampos = function (ocultar) {
    ocultar.forEach(function (e) {
        $('[data-campanyadatoid=' + e + ']').attr("disabled", true);
        $('[data-campanyadatoid=' + e + ']').closest('.campo-input').hide();
        var dataOcultar = $('[data-campanyadatoid=' + e + ']').find(':selected').data('mostrar');
        if (dataOcultar) {
            ocultarCampos(dataOcultar);
        }
        $('[data-campanyadatoid=' + e + ']').val('').trigger("chosen:updated");
    });
};


FormularioGenerico.prototype.mostrarCampos = function (mostrar) {
    mostrar.forEach(function (e) {
        $('[data-campanyadatoid=' + e + ']').attr("disabled", false);
        $('[data-campanyadatoid=' + e + ']').closest('.campo-input').show();
        $('select[data-campanyadatoid=' + e + ']').trigger("chosen:updated");
    });
};

FormularioGenerico.prototype.getCampanyaId = function () {
    var segment_array = window.location.pathname.split('/');
    return segment_array[segment_array.length - 1];
};

FormularioGenerico.prototype.onChangeTipoIdentificacion = function () {
    var selec = $('#tipoIdentificacion :selected').text();
    if (selec == 'DNI') {
        $('#identificacion').rules('add', 'nifES');
        $('#identificacion').rules('remove', 'cifES nieES');
    } else if (selec == 'CIF') {
        $('#identificacion').rules('add', 'cifES');
        $('#identificacion').rules('remove', 'nifES nieES');
    } else if (selec == 'NIE') {
        $('#identificacion').rules('add', 'nieES');
        $('#identificacion').rules('remove', 'cifES nifES');
    } else {
        $('#identificacion').rules('remove', 'nifES cifES nieES');
    }
}

FormularioGenerico.prototype.onSubmitForm = function (f) {
    f.preventDefault();
    var me = this, form = $("#form-crm");
    if (form.valid()) {
        form.parent().addClass("loading-mask");
        me._enviaForm();
    }
};

FormularioGenerico.prototype.getFormularioId = function () {
    return $("#formularioId").text();
};

FormularioGenerico.prototype._enviaForm = function () {
    var form = $("#form-crm"),
        formSerializer = FormSerializer.create({}),
        datosForm = formSerializer.serialize(form),
        urlForm = '/crm/rest/publicacion/formulario/' + this.getFormularioId();

    $.ajax({

        contentType: 'application/json',
        data: JSON.stringify(datosForm),
        dataType: 'json',
        async: true,
        processData: false,
        type: 'POST',
        url: urlForm,
        success: function (response) {
            alert("Formulario enviado");
            window.location.href = "https://www.uji.es";
        },
        error: function () {
            $("#form-crm").removeClass("loading-mask");

            alert("No es pot enviar el formulari");
            // event.preventDefault();
        }
    });
};