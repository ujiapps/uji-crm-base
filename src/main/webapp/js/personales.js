$(document).ready(function () {
    var _formPerso = new Personales();
    $('#tipoIdentificacion').change(function () {
        var selec = $('#tipoIdentificacion :selected').text();
        if (selec == 'DNI') {
            $('#identificacion').rules('add', 'nifES');
            $('#identificacion').rules('remove', 'cifES nieES');
        } else if (selec == 'CIF') {
            $('#identificacion').rules('add', 'cifES');
            $('#identificacion').rules('remove', 'nifES nieES');
        } else if (selec == 'NIE') {
            $('#identificacion').rules('add', 'nieES');
            $('#identificacion').rules('remove', 'cifES nifES');
        } else {
            $('#identificacion').rules('remove', 'nifES cifES nieES');

        }
    });
    $('.tabs a').click(function (e) {
        if ($('#personalesTab').hasClass('active') && _formPerso.dataChange) {
            $.alert(i18n.get_string('guardarcambios'), i18n.get_string('aviso'));
            e.preventDefault();
            e.stopImmediatePropagation();
        }
    });

    $(".guardar-personales").click(function () {
        _formPerso.guardarTipo = 1;
        $("#form-personales").submit();
    });

    $(".siguiente-personales").click(function () {
        _formPerso.guardarTipo = 2;
        $("#form-personales").submit();
    });

    $("#form-personales").on("submit", function (event) {
        if ($("#form-personales").valid()) {
            // process the form
            $("#form-personales").addClass("loading-mask");
            $.ajax({
                type: 'POST',
                url: 'personales',
                data: $("#form-personales").serialize(),
                // dataType: 'json',
                encode: true
            }).done(function (data) {
                //$.alert("Dades actualizades", "Actualizació");
                $('#form-personales').data('changed', false);
                _formPerso.dataChange = false;
                $("#form-personales").removeClass("loading-mask");
                //Llamar funcion que abre pesteña academicos
                if (_formPerso.guardarTipo == 2)
                    $('[href="#academicos"]').click();
            }).fail(function () {
                $("#form-personales").removeClass("loading-mask");
            });

            event.preventDefault();
        }
    });

    $("#form-personales").validate({
        ignore: '.ignore'
    });

    // $(".proteccion a").each(function (elem) {
    //     elem.setAttribute("target", "_blank");
    // }) ;


});

/**
 * Objeto que controla el formulario de datos personales
 * @constructor
 */
Personales = function () {

    var dataChanged = false;
    // Nacimiento
    this._paisNacimiento = $('#paisNacimiento > select');
    this._provinciaNacimiento = $('#provinciaNacimiento > select');
    this._poblacionNacimiento = $('#poblacionNacimiento > select');
    this._paisNacimiento.change(this.onPaisNacimientoSelected.bind(this));
    this._provinciaNacimiento.change(this.onProvinciaNacimientoSelected.bind(this));

    //Contacto
    this._paisContacto = $('#paisContacto > select');
    this._provinciaContacto = $('#provinciaContacto > select');
    this._poblacionContacto = $('#poblacionContacto > select');
    this._paisContacto.change(this.onPaisContactoSelected.bind(this));
    this._provinciaContacto.change(this.onProvinciaContactoSelected.bind(this));

    this.getData();

};


Personales.prototype.getData = function () {
    var me = this;
    $("#form-personales").addClass("loading-mask");
    $.ajax({
        url: 'personales',
        success: function (respuesta) {
            me.data = respuesta.data;
            me.loadForm();
        },
        error: function () {
            $("#form-personales").removeClass("loading-mask");
        }
    });
};
Personales.prototype.loadForm = function () {

    var ref = this;

    $('#tipoIdentificacion').val(this.data.tipoIdentificacion);
    $('#identificacion').val(this.data.identificacion);
    $('#nombre').val(this.data.nombreOficial);
    $('#apellidos').val(this.data.apellidosOficial);
    $('#sexo').val(this.data.sexo);
    $('#prefijoMovil').val(this.data.prefijoMovil);
    $('#movil').val(this.data.movil);
    $('#telefonoAlternativo').val(this.data.telefonoAlternativo);
    $('#email').val(this.data.email);
    $('#codigoPostal').val(this.data.codigoPostal);

    $('[name="paisContactoId"]').val(this.data.paisContactoId).trigger('change');
    $('[name="provinciaContactoId"]').val(this.data.provinciaContactoId).trigger('change');
    $('[name="poblacionContactoId"]').val(this.data.poblacionContactoId);

    $('#direccion').val(this.data.direccion);
    $('#fechaNacimiento').val(this.parseFechatoString(this.data.fechaNacimiento));
    $('[name="paisNacimientoId"]').val(this.data.paisNacimiento).trigger('change');
    $("#form-personales").removeClass("loading-mask");

    $("#form-personales :input").change(function () {
        ref.dataChange = true;
        $(this).closest('#form-personales').data('changed', true);
    });


};
Personales.prototype.parseFechatoString = function (fecha) {
    if (fecha) {
        var parsedFecha = fecha.split("/");
        var f = new Date(Date.UTC(parsedFecha[2].split(" ")[0], parseInt(parsedFecha[1], 10) - 1, parsedFecha[0], 0, 0, 0, 0));
        return f.toISOString().substring(0, 10);
    }
};

Personales.prototype._disableField = function (element) {
    var select = element.find('select');
    element.addClass('hidden');
    select.val('');
    select.prop('disabled', true);
};
Personales.prototype._enableField = function (element) {
    var select = element.find('select');
    element.removeClass('hidden');
    select.prop('disabled', false);
};

Personales.prototype.onPaisNacimientoSelected = function () {
    var me = this;
    if (me._paisNacimiento.val() == 3276243) {
        this._enableField($('#provinciaNacimiento'));
        this.updateSelectsNacimiento(3746139, me._provinciaNacimiento, "provincia");
    } else {
        this._disableField($('#provinciaNacimiento'));
        this._disableField($('#poblacionNacimiento'));
    }

};

Personales.prototype.onProvinciaNacimientoSelected = function () {
    var me = this;
    if (me._provinciaNacimiento.val() == 3746151) {
        //Castellon
        this._enableField($('#poblacionNacimiento'));
        this.updateSelectsNacimiento(3879484, me._poblacionNacimiento, "poblacion");
    } else if (me._provinciaNacimiento.val() == 3746185) {
        //Valencia
        this._enableField($('#poblacionNacimiento'));
        this.updateSelectsNacimiento(3880026, me._poblacionNacimiento, "poblacion");
    } else {
        this._disableField($('#poblacionNacimiento'));
    }
};

Personales.prototype.updateSelectsNacimiento = function (grupoId, elem, tipo) {
    var me = this;
    $("#form-personales").parent().addClass("loading-mask");
    $.ajax({
        url: 'personales/preinscripcion?grupoId=' + grupoId,
    }).done(function (value) {
        var options = '<option value=""></option>';
        for (var i = 0; i < value.data.length; i++) {
            options += '<option value="' + value.data[i].id + '">' + value.data[i].nombreCa + '</option>';
        }
        elem.html(options);
        //Primera carga
        if (tipo == "provincia" && me.data.provinciaNacimiento) {
            $('[name="provinciaNacimientoId"]').val(me.data.provinciaNacimiento).trigger('change');
            me.data.provinciaNacimiento = '';
            me.dataChange = false;
        }
        if (tipo == "poblacion") {
            $('[name="poblacionNacimientoId"]').val(me.data.poblacionNacimiento);
            me.data.poblacionNacimiento = '';
            me.dataChange = false;
        }

        $("#form-personales").parent().removeClass("loading-mask");
    });
};

Personales.prototype.onPaisContactoSelected = function () {
    var me = this;
    if (me._paisContacto.val() == "E") {
        this._enableField($('#provinciaContacto'));
    } else {
        this._disableField($('#provinciaContacto'));
        this._disableField($('#poblacionContacto'));
    }

};

Personales.prototype.onProvinciaContactoSelected = function () {
    var me = this;
    if (me._provinciaContacto.val()) {
        this._enableField($('#poblacionContacto'));
        this.updateSelectsContacto(me._provinciaContacto.val(), me._poblacionContacto, "poblacion");
    } else {
        this._disableField($('#poblacionContacto'));
    }
};

Personales.prototype.updateSelectsContacto = function (grupoId, elem, tipo) {
    var me = this;
    $("#form-contacto").parent().addClass("loading-mask");
    $.ajax({
        url: 'poblacion/' + grupoId,
    }).done(function (value) {
        var options = '<option value=""></option>';
        for (var i = 0; i < value.data.length; i++) {
            if (APP_CONFIG.lang == 'en') {
                options += '<option value="' + value.data[i].id + '">' + value.data[i].nombreEN + '</option>';
            } else if (APP_CONFIG.lang == 'es') {
                options += '<option value="' + value.data[i].id + '">' + value.data[i].nombreES + '</option>';
            } else {
                options += '<option value="' + value.data[i].id + '">' + value.data[i].nombreCA + '</option>';
            }
        }
        elem.html(options);
        //Primera carga
        if (tipo == "provincia" && me.data.provinciaContactoId) {
            $('[name="provinciaContactoId"]').val(me.data.provinciaContactoId).trigger('change');
            me.data.provinciaContactoId = '';
            me.dataChange = false;
        }
        if (tipo == "poblacion") {
            $('[name="poblacionContactoId"]').val(me.data.poblacionContactoId);
            me.data.poblacionContactoId = '';
            me.dataChange = false;
        }
    });
};


