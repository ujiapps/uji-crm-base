$(document).ready(function () {
    var _formLabo = new Laborales();

    $('.tabs a').click(function (e) {
        if ($('#laboralTab').hasClass('active') && _formLabo.dataChange) {
            $.alert(i18n.get_string('guardarcambios'), i18n.get_string('aviso'));
            e.preventDefault();
            e.stopImmediatePropagation();
        }
    });


    $(".guardar-laboral").click(function () {
        _formLabo.guardarTipo = 1;
        $("#form-laborales").submit();
    });

    $(".siguiente-laboral").click(function () {
        _formLabo.guardarTipo = 2;
        $("#form-laborales").submit();
    });

    $("#form-laborales").on("submit", function (event) {
        if ($("#form-laborales").valid()) {
            $("#form-laborales").addClass("loading-mask");
            $.ajax({
                type: 'POST',
                url: 'laborales',
                data: $("#form-laborales").serialize(),
                // dataType: 'json',
                encode: true
            }).done(function (data) {
                _formLabo.dataChange = false;
                $("#form-laborales").removeClass("loading-mask");
                if (_formLabo.guardarTipo == 2)
                    $('[href="#espacioAlumni"]').removeClass("hidden");
                $('[href="#espacioAlumni"]').click();
            }).fail(function () {
                $("#form-laborales").removeClass("loading-mask");
            });
        }
        event.preventDefault();
    });
    $("#form-laborales").validate({
        ignore: '.ignore'
    });
});

/**
 * Objeto que controla el formulario de datos Laborales
 * @constructor
 */
Laborales = function () {

    var dataChanged = false;
    this._estadoLaboral = $('#estadoLaboral');
    this._estadoLaboral.change(this.onEstadoLaboralSelected.bind(this));
    this.getDataLabo();

};

Laborales.prototype.getDataLabo = function () {
    var me = this;
    $("#form-laborales").addClass("loading-mask");
    $.ajax({
        url: 'laborales',
        success: function (respuesta) {
            me.data = respuesta.data;
            me.loadForm();
        },
        error: function () {
            $("#form-laborales").removeClass("loading-mask");
        }
    });
};

Laborales.prototype.loadForm = function () {
    var ref = this;
    $('#estadoLaboral').val(this.data.estadoLaboral).trigger('change');
    $('#sectorEmpresarial').val(this.data.sectorEmpresarial);
    $('#empresa').val(this.data.empresa);
    $('#cargo').val(this.data.cargo);
    $('#nombreEstudioActual').val(this.data.nombreEstudioActual);
    $("#form-laborales").removeClass("loading-mask");
    $("#form-laborales > .datos").removeClass("hidden");
    $("#form-laborales :input").change(function () {
        ref.dataChange = true;
    });

};

Laborales.prototype._disableField = function (element) {
    var select = element.find('select');
    element.addClass('hidden');
    select.val('');
    select.prop('disabled', true);
};
Laborales.prototype._enableField = function (element) {
    var select = element.find('select');
    element.removeClass('hidden');
    select.prop('disabled', false);
};

Laborales.prototype.onEstadoLaboralSelected = function () {
    var me = this;
    if (me._estadoLaboral.val() == 996676) {
        $('label[for=sectorEmpresarial],#sectorEmpresarial').removeClass('hidden');
        $('#sectorEmpresarial').prop('disabled', false);
        $('label[for=empresa],#empresa').removeClass('hidden');
        $('#empresa').prop('disabled', false);
        $('label[for=cargo],#cargo').removeClass('hidden');
        $('#cargo').prop('disabled', false);
        $('label[for=nombreEstudioActual],#nombreEstudioActual').addClass('hidden');
        $('#nombreEstudioActual').prop('disabled', true);
    } else if (me._estadoLaboral.val() == 1638182) {
        $('label[for=sectorEmpresarial],#sectorEmpresarial').addClass('hidden');
        $('#sectorEmpresarial').prop('disabled', true);
        $('label[for=empresa],#empresa').addClass('hidden');
        $('#empresa').prop('disabled', true);
        $('label[for=cargo],#cargo').addClass('hidden');
        $('#cargo').prop('disabled', true);
        $('label[for=nombreEstudioActual],#nombreEstudioActual').removeClass('hidden');
        $('#nombreEstudioActual').prop('disabled', false);
    } else {
        $('label[for=sectorEmpresarial],#sectorEmpresarial').addClass('hidden');
        $('#sectorEmpresarial').prop('disabled', true);
        $('label[for=empresa],#empresa').addClass('hidden');
        $('#empresa').prop('disabled', true);
        $('label[for=cargo],#cargo').addClass('hidden');
        $('#cargo').prop('disabled', true);
        $('label[for=nombreEstudioActual],#nombreEstudioActual').addClass('hidden');
        $('#nombreEstudioActual').prop('disabled', true);
    }
}