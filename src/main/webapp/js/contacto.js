$(document).ready(function () {
    var _contacto = new Contacto();
    // $('#tipoIdentificacion').change(function () {
    //     var selec = $('#tipoIdentificacion :selected').text();
    //     if (selec == 'DNI') {
    //         $('#identificacion').rules('add', 'nifES');
    //         $('#identificacion').rules('remove', 'cifES nieES');
    //     } else if (selec == 'CIF') {
    //         $('#identificacion').rules('add', 'cifES');
    //         $('#identificacion').rules('remove', 'nifES nieES');
    //     } else if (selec == 'NIE') {
    //         $('#identificacion').rules('add', 'nieES');
    //         $('#identificacion').rules('remove', 'cifES nifES');
    //     } else {
    //         $('#identificacion').rules('remove', 'nifES cifES nieES');
    //
    //     }
    // });

});

Contacto = function () {

    this._contacto = $("#formularioContacto");
    this._contacto.submit(this.onClickContacto.bind(this));

    // this._contacto.validate({
    //     ignore: '.ignore'
    // });
};

// Contacto.prototype.comprobarFecha = function(e) {
//     let hoy = new Date()
//     let mayoriaEdad = 18 * 365 * 24 * 60 * 60 * 1000
//     let dateInp = new Date(e.val())
//
//     return (hoy - dateInp >= mayoriaEdad);
// };


Contacto.prototype.onClickContacto = function (event) {
    this._contacto.addClass("loading-mask");
    var ref = this;
    // let fechaNacimiento = $('#fechaNacimiento');

    // if (!this.comprobarFecha(fechaNacimiento)){
    //     alert(i18n.get_string('fechaNacimientoIncorrecta'));
    // }

    if ($("#contactoAcceso").valid() ){ //&& this.comprobarFecha(fechaNacimiento)) {
        $.ajax({
            url: '/crm/rest/alumni/inici/contacto',
            type: "POST",
            dataType: 'json',
            data: $("#contactoAcceso").serialize(),
            success: function (result) {
                if (result.success) {
                    $("<div></div>").dialog({
                        resizable: false,
                        title: "Informació",
                        buttons: [{
                            text: "Ok",
                            icon: "ui-icon-heart",
                            click: function () {
                                $(this).dialog("close");
                                window.location='https://www.uji.es/serveis/alumnisauji/';
                            }
                        }],
                        modal: true
                    }).text(i18n.get_string('enviorealizado'));
                    ref._contacto.removeClass("loading-mask");
                } else {
                    $("<div></div>").dialog({
                        resizable: false,
                        title: "Informació",
                        modal: true,
                        buttons: [{
                            text: "Ok",
                            icon: "ui-icon-heart",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }]
                    }).text(i18n.get_string('envioerror'));
                    ref._contacto.removeClass("loading-mask");
                }
            }
        });
        event.preventDefault();
    }
    else ref._contacto.removeClass("loading-mask");

    event.preventDefault();
};