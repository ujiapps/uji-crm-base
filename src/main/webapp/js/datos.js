$(document).ready(function () {

    var dialog = $("#tratamientoVentana").dialog({
        autoOpen: true,
        width: 'auto',
        modal: true,
        position: ['center', 120],
        buttons: [
            {
                text: "Ok",
                click: function () {
                    $.ajax({
                        url: 'tratamiento',
                        type: "POST",
                        dataType: 'json',
                        callback: function (result) {
                            window.location.href = '/crm/rest/alumni/'
                        },
                        error: function (xhr, resp, text) {
                        }
                    });
                    $(this).dialog("close");
                }
            }
        ]
    });

    var _formDatos = new Datos();

    $('.proteccion a').attr('target', '_blank');


});

Datos = function () {

    $('input[name=desconectar]').click(this.logout.bind(this));
};


Datos.prototype.logout = function () {

    $.ajax({
        type: 'GET',
        url: 'logout',
        encode: true
        // success: function (result) {
            // window.location.href = '/crm/rest/alumni/inici/';
            // console.log("logout");
        // }
    }).done(function (data) {

    })
};


