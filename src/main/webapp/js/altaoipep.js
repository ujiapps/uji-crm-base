
$(document).ready(function () {

    var _altaOIPEP = new AltaOIPEP();
});

/**
 * Objeto que controla el formulario de datos academicos
 * @constructor
 */
AltaOIPEP = function () {

    var me = this;

    this.uploadCrop = $('#upload-foto').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport: {
            width: 150,
            height: 200
        },
        boundary: {
            width: 200,
            height: 250
        }
    });

    $('#foto').on('change', function () {
        me.readFile(this);
    });
    $('#guardarAlta').click(this.onClickGuardar.bind(this));
    $('.modalidad input[type=radio]').change(this.onChangeCheckDomicilio);
};

AltaOIPEP.prototype.readFile = function (input) {
    var me = this;
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.upload-foto').addClass('ready');
            me.uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function () {
            });

        }
        reader.readAsDataURL(input.files[0]);
    }
};

AltaOIPEP.prototype.onChangeCheckDomicilio = function () {
    if ($(this).is(':checked') && $(this).attr('id') == 'checkDomicilio') {
        $('.foto-div').removeClass("hidden");
        $('input[name="foto"]').attr('required', 'required')
    } else {
        $('.foto-div').addClass("hidden");
        $('input[name="foto"]').removeAttr('required');
    }

};

AltaOIPEP.prototype.onClickGuardar = function () {

    var me = this;
    $("#form-altaoipep").parent().addClass("loading-mask");
    if ($("#form-altaoipep").valid()) {
        var formData = new FormData();
        var modalidad= $('.modalidad input[type=radio]:checked').val();
        formData.append('modalidad', modalidad);
        formData.append('aceptaLOPD', $('input[name="aceptaLOPD"]').is(':checked'));
        formData.append('campanya', $('[name="campanyaId"]').text());
        // formData.append('campanya', )

        if (modalidad == 14613872) {
            this.uploadCrop.croppie('result', {
                type: 'blob',
                size: 'viewport'
            }).then(function (resp) {
                formData.append('foto', resp, 'foto.png');
                me._enviaForm(formData);
            });
        } else {
            me._enviaForm(formData);

        }

    }else{
        $("#form-altaoipep").parent().removeClass("loading-mask");
    }

};
AltaOIPEP.prototype._enviaForm = function (formData) {
    $.ajax({
        type: 'post',
        data: formData,
        // cache: false,
        contentType: false,
        processData: false,
        success: function (respuesta) {
            $("#form-altaoipep").parent().removeClass("loading-mask");
            $("<div></div>").dialog({
                resizable: false,
                title: i18n.get_string('informacion'),
                modal: true
            }).text(i18n.get_string("solicitudaltacorrecta"));
            setTimeout(function(){
                window.location=window.location.pathname.split('/campanyas/oipep/alta')[0]+'/#campanyes';
            }, 3000);


        },
        error: function () {
            $("#form-altaoipep").parent().removeClass("loading-mask");

        }
    });
};




