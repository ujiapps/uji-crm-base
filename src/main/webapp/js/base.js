$(document).ready(function () {
    var _base = new Base();
});

Base = function () {

    //Contacto
    this._pais = $('.pais > select');
    this._provincia = $('.provincia > select');
    this._poblacion = $('.poblacion > select');
    this._pais.change(this.onPaisSelected.bind(this));
    this._provincia.change(this.onProvinciaSelected.bind(this));
};

// Personales.prototype.loadForm = function () {
//
//     var ref = this;
//
//     $('#tipoIdentificacion').val(this.data.tipoIdentificacion);
//     $('#identificacion').val(this.data.identificacion);
//     $('#nombre').val(this.data.nombreOficial);
//     $('#apellidos').val(this.data.apellidosOficial);
//     $('#sexo').val(this.data.sexo);
//     $('#prefijoMovil').val(this.data.prefijoMovil);
//     $('#movil').val(this.data.movil);
//     $('#telefonoAlternativo').val(this.data.telefonoAlternativo);
//     $('#email').val(this.data.email);
//     $('#codigoPostal').val(this.data.codigoPostal);
//
//     $('[name="paisContactoId"]').val(this.data.paisContactoId).trigger('change');
//     $('[name="provinciaContactoId"]').val(this.data.provinciaContactoId).trigger('change');
//     $('[name="poblacionContactoId"]').val(this.data.poblacionContactoId);
//
//     $('#direccion').val(this.data.direccion);
//     $('#fechaNacimiento').val(this.parseFechatoString(this.data.fechaNacimiento));
//     $('[name="paisNacimientoId"]').val(this.data.paisNacimiento).trigger('change');
//     $("#form-personales").removeClass("loading-mask");
//
//     $("#form-personales :input").change(function () {
//         ref.dataChange = true;
//         $(this).closest('#form-personales').data('changed', true);
//     });
// };
// Personales.prototype.parseFechatoString = function (fecha) {
//     if (fecha) {
//         var parsedFecha = fecha.split("/");
//         var f = new Date(Date.UTC(parsedFecha[2].split(" ")[0], parseInt(parsedFecha[1], 10) - 1, parsedFecha[0], 0, 0, 0, 0));
//         return f.toISOString().substring(0, 10);
//     }
// };

Base.prototype._disableField = function (element) {
    var select = element.find('select');
    // element.addClass('hidden');
    select.val('');
    select.prop('disabled', true);
};
Base.prototype._enableField = function (element) {
    var select = element.find('select');
    // element.removeClass('hidden');
    select.prop('disabled', false);
};

Base.prototype.onPaisSelected = function () {
    var me = this;
    if (me._pais.val() == "E") {
        this._enableField($('.provincia'));
    } else {
        this._disableField($('.provincia'));
        this._disableField($('.poblacion'));
    }

};

Base.prototype.onProvinciaSelected = function () {
    var me = this;
    if (me._provincia.val()) {
        this._enableField($('.poblacion'));
        this.updateSelects(me._provincia.val(), me._poblacion, "poblacion");
    } else {
        this._disableField($('.poblacion'));
    }
};

Base.prototype.updateSelects = function (grupoId, elem, tipo) {
    var me = this;
    $(".form").parent().addClass("loading-mask");
    $.ajax({
        url: '/crm/rest/poblacion/' + grupoId,
    }).done(function (value) {
        var options = '<option value=""></option>';
        for (var i = 0; i < value.data.length; i++) {
            options += '<option value="' + value.data[i].id + '">' + value.data[i].nombre + '</option>';
            // if (APP_CONFIG.lang == 'en') {
            //     options += '<option value="' + value.data[i].id + '">' + value.data[i].nombreEN + '</option>';
            // } else if (APP_CONFIG.lang == 'es') {
            //     options += '<option value="' + value.data[i].id + '">' + value.data[i].nombreES + '</option>';
            // } else {
            //     options += '<option value="' + value.data[i].id + '">' + value.data[i].nombreCA + '</option>';
            // }
        }
        elem.html(options);
        $(".form").parent().removeClass("loading-mask");
        //Primera carga
        // if (tipo == "provincia" && me.data.provinciaContactoId) {
        //     $('[name="provinciaContactoId"]').val(me.data.provinciaContactoId).trigger('change');
        //     me.data.provinciaContactoId = '';
        //     me.dataChange = false;
        // }
        // if (tipo == "poblacion") {
        //     $('[name="poblacionContactoId"]').val(me.data.poblacionContactoId);
        //     me.data.poblacionContactoId = '';
        //     me.dataChange = false;
        // }
    });
};


