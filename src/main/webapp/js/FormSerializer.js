var FormSerializer = function(config)
{
    this.config = config;
};

FormSerializer.create = function(config)
{
    return new FormSerializer(config);
};

FormSerializer.prototype =
{
    serialize : function(form)
    {
        var ref = this;
        var datosForm =
        {
            datosCliente : {},
            datosExtra : []
        };

        $('*[data-model="cliente"]', form).each(function()
        {
            var input = $(this);
            datosForm.datosCliente[input.attr('name')] = input.val();
        });

        $('*[data-model="extra"]', form).each(function()
        {
            var input = $(this),
                value = ref.getInputValue(input);

            if (value)
            {
                var datoExtra =
                {
                    campanyaDatoId : input.attr('data-campanyadatoid'),
                    clienteDatoId : input.attr('data-clientedatoid'),
                    valor : value
                };
                datosForm.datosExtra.push(datoExtra);
            }
        });

        return datosForm;
    },

    getInputValue : function(input)
    {
        if (input.is(':checkbox') || input.is(':radio'))
        {
            return input.is(':checked') ? input.val() : null;
        }

        return input.val();
    }

};