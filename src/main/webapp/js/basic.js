$(document).ready(function () {

    var _basic = new Basic();
});

/**
 * Objeto que controla el formulario de datos academicos
 * @constructor
 */
Basic = function () {

    var me = this;

    $('#guardarBaja').click(this.onClickGuardar.bind(this));
};

Basic.prototype.onClickGuardar = function () {

    var me = this;
    $("#form-bajaBasic").parent().addClass("loading-mask");
    if ($("#form-bajaBasic").valid()) {
        var formData = new FormData();
        var motivo = $('.motivo input[type=radio]:checked').val();
        formData.append('motivo', motivo);
        me._enviaForm(formData);
    } else {
        $("#form-bajaBasic").parent().removeClass("loading-mask");
    }

};
Basic.prototype._enviaForm = function (formData) {
    $.ajax({
        url: 'baja',
        type: 'post',
        data: formData,
        contentType: false,
        processData: false,
        success: function (respuesta) {
            $("#form-bajaBasic").parent().removeClass("loading-mask");
            $("<div></div>").dialog({
                resizable: false,
                title: i18n.get_string('informacion'),
                modal: true
            }).text(i18n.get_string("solicitudbajacorrecta"));
            setTimeout(function () {
                window.location=window.location.pathname.split('/campanyas/basic/baja')[0]+'/#campanyes';

            }, 3000);


        },
        error: function () {
            $("#form-bajaBasic").parent().removeClass("loading-mask");

        }
    });
};




