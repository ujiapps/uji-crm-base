$(document).ready(function () {
    var _formAca = new Academicos();

    $("#form-academicos").submit(function (event) {
        if ($("#form-academicos").valid()) {
            _formAca.addEstudio();
        }
        event.preventDefault();
    });
    $("#form-academicos").validate({
        ignore: '.ignore'
    });

    $(".siguiente-academicos").click(function () {
        $('[href="#laboral"]').click();
    });
});

/**
 * Objeto que controla el formulario de datos academicos
 * @constructor
 */



Academicos = function () {
    this._tipoEstudio = $('#tipoEstudio');
    this._tipoEstudio.change(this.onTipoEstudioSelected.bind(this));

    $('#anyadirEstudio').click(this.showWindowAdd.bind(this));
    this.loadAcademicos();
    this.loadEstudiosNouji();
};
Academicos.prototype.loadAcademicos = function () {
    $.ajax({
        url: 'academicos',
        success: function (respuesta) {
        },
        error: function () {
        }
    });
};

Academicos.prototype.loadEstudiosNouji = function () {
    var ref = this;
    $("#estudios-no-uji").addClass("loading-mask");
    $.ajax({
        url: 'academicos/estudios',
        success: function (respuesta) {
            $('#estudios-no-uji').html(respuesta);
            $('.eliminarEstudio').click(ref.eliminarEstudio);
            $("#estudios-no-uji").removeClass("loading-mask");
        },
        error: function () {
            $("#estudios-no-uji").removeClass("loading-mask");
        }
    });
};

Academicos.prototype.showWindowAdd = function () {
    $("#dialogAnyadirEstudio").dialog({
        width: 450,
        modal: true,
        position: ['center', 20]
    });

};

Academicos.prototype.eliminarEstudio = function () {
    $("#estudios-no-uji").addClass("loading-mask");
    var estudioId =  $(this).attr('data-itemid');
    $.ajax({
        type: 'DELETE',
        url: 'academicos/estudios/'+estudioId,
        encode: true,
        success: function (respuesta) {
            $("#estudio-"+estudioId).remove();
            $("#estudios-no-uji").removeClass("loading-mask");
        },
        error: function () {
            $("#estudios-no-uji").removeClass("loading-mask");
        }
    });
};


Academicos.prototype.onTipoEstudioSelected = function () {
    var tipoEstudio = $('#tipoEstudio').val();
    $('#ambitoEstudios').addClass("hidden");
    $('#ambitoEstudios > select').prop('disabled', true);
    $('#secciongrados').addClass("hidden");
    $('#secciongrados > select').prop('disabled', true);
    $('#seccionpostgrado').addClass("hidden");
    $('#seccionpostgrado > select').prop('disabled', true);
    $('#secciondoctorado').addClass("hidden");
    $('#secciondoctorado > select').prop('disabled', true);
    $('#seccionUniversidad').addClass("hidden");
    $('#seccionUniversidad > select').prop('disabled', true);
    $('#seccionNombre').addClass("hidden");
    $('#seccionNombre > input').prop('disabled', true);
    $('#seccionAnyoFin').addClass("hidden");
    $('#seccionNombre > input').prop('disabled', true);

    if (tipoEstudio == 'ELEM') {
        $('#seccionNombre').removeClass("hidden");
        $('#seccionNombre > input').prop('disabled', false);
    }

    if (tipoEstudio == 'MIT') {
        $('#ambitoEstudios').removeClass("hidden");
        $('#ambitoEstudios > select').prop('disabled', false);
        $('#seccionNombre').removeClass("hidden");
        $('#seccionNombre > input').prop('disabled', false);
    }

    if (tipoEstudio == 'NO') {
        $('#ambitoEstudios').removeClass("hidden");
        $('#ambitoEstudios > select').prop('disabled', false);
        $('#seccionNombre').removeClass("hidden");
        $('#seccionNombre > input').prop('disabled', false);
    }
    if (tipoEstudio == 'GRADO') {
        $('#secciongrados').removeClass("hidden");
        $('#secciongrados > select').prop('disabled', false);
        $('#seccionUniversidad').removeClass("hidden");
        $('#seccionUniversidad > select').prop('disabled', false);
        $('#seccionAnyoFin').removeClass("hidden");
    }
    if (tipoEstudio == 'POST') {
        $('#seccionpostgrado').removeClass("hidden");
        $('#seccionpostgrado > select').prop('disabled', false);
        $('#seccionUniversidad').removeClass("hidden");
        $('#seccionUniversidad > select').prop('disabled', false);
        $('#seccionNombre').removeClass("hidden");
        $('#seccionNombre > input').prop('disabled', false);
        $('#seccionAnyoFin').removeClass("hidden");
    }
    if (tipoEstudio == 'DOC') {
        $('#secciondoctorado').removeClass("hidden");
        $('#secciondoctorado > select').prop('disabled', false);
        $('#seccionUniversidad').removeClass("hidden");
        $('#seccionUniversidad > select').prop('disabled', false);
        $('#seccionNombre').removeClass("hidden");
        $('#seccionNombre > input').prop('disabled', false);
        $('#seccionAnyoFin').removeClass("hidden");
    }

};
Academicos.prototype.addEstudio = function () {
    var me = this;
    $("#form-academicos").addClass("loading-mask");
    $.ajax({
        type: 'POST',
        url: 'academicos/estudios',
        data: $("#form-academicos").serialize(),
        encode: true,
        success: function (respuesta) {
            $("#form-academicos").removeClass("loading-mask");
            $("#dialogAnyadirEstudio").dialog('close');
            me.loadEstudiosNouji();

        },
        error: function () {
            $("#form-academicos").removeClass("loading-mask");
        }
    });
};