$(document).ready(function () {
    var _login = new Login();
    $('#tipoIdentificacion').change(function () {
        var selec = $('#tipoIdentificacion :selected').text();
        if (selec == 'DNI') {
            $('#identificacion').rules('add', 'nifES');
            $('#identificacion').rules('remove', 'cifES nieES');
        } else if (selec == 'CIF') {
            $('#identificacion').rules('add', 'cifES');
            $('#identificacion').rules('remove', 'nifES nieES');
        } else if (selec == 'NIE') {
            $('#identificacion').rules('add', 'nieES');
            $('#identificacion').rules('remove', 'cifES nifES');
        } else {
            $('#identificacion').rules('remove', 'nifES cifES nieES');

        }
    });

});

Login = function () {
    this._loginUsuarioCorreo = $("#formLoginCorreo");
    this._loginUsuarioSSO = $('input[name="enviarUsuarioSSO"]');
    this._loginUsuarioNuevo = $("#formLoginNuevo");

    this._loginUsuarioCorreo.submit(this.onClickLoginUsuarioCorreo.bind(this));
    this._loginUsuarioSSO.click(this.onClickLoginSSO);
    this._loginUsuarioNuevo.submit(this.onClickUsuarioNuevo.bind(this));

    $(".activarAccesoNoAutenticado").click(this.activarAccesoNoAutenticado);
    $(".activarAccesoAutenticado").click(this.activarAccesoAutenticado);
    $(".activarAccesoHash").click(this.activarAccesoHash);


    // formLoginNuevo
    this._loginUsuarioNuevo.validate({
        ignore: '.ignore'
    });
};

Login.prototype.activarAccesoNoAutenticado = function(){
    $('.accesoNoAutenticado').removeClass("hidden");
    $('.accesoAutenticado').addClass("hidden");
    $('.accesoHash').addClass("hidden");
};

Login.prototype.activarAccesoAutenticado = function(){
    $('.accesoNoAutenticado').addClass("hidden");
    $('.accesoAutenticado').removeClass("hidden");
    $('.accesoHash').addClass("hidden");
};

Login.prototype.activarAccesoHash = function(){
    $('.accesoNoAutenticado').addClass("hidden");
    $('.accesoAutenticado').addClass("hidden");
    $('.accesoHash').removeClass("hidden");
};

Login.prototype.onClickLoginUsuarioCorreo = function (event) {
    if ($("#formLoginCorreo").valid()) {
        event.preventDefault();
        $.ajax({
            url: '/crm/rest/alumni/inici/hash',
            type: "POST",
            dataType: 'json',
            data: $("#formLoginCorreo").serialize(),
            success: function (result) {
                if (result.success) {
                    $('#usuarioClaveMensaje').show();
                    $('#usuarioCorreoError').hide();
                } else {
                    $('#usuarioCorreoError').show();
                    $('#usuarioClaveMensaje').hide();

                }
            },
            error: function (xhr, resp, text) {
            }
        });
        event.preventDefault();
    }
    event.preventDefault();
};

Login.prototype.onClickLoginSSO = function (event) {
    var url = window.location.origin + '/crm/rest/alumni/sso/&lang='+APP_CONFIG.lang ;
    window.location.href = 'https://xmlrpc.uji.es/lsmSSO-83/lsmanage.php?Url=' + url;
};

Login.prototype.onClickUsuarioNuevo = function (event) {
    if ($("#formLoginNuevo").valid()) {
        $.ajax({
            url: '/crm/rest/alumni/inici/nuevo',
            type: "POST",
            dataType: 'json',
            data: $("#formLoginNuevo").serialize(),
            success: function (result) {
                if (result.success) {
                    $('#nuevoUsuario').show();
                } else {
                    $('#nuevoUsuarioError').show();
                }
            },
            error: function (xhr, resp, text) {
            }
        });
        event.preventDefault();
    }
    event.preventDefault();
};