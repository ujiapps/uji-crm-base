
$(document).ready(function () {
    var _altaPremium = new AltaPremium();
});

/**
 * Objeto que controla el formulario de datos academicos
 * @constructor
 */
AltaPremium = function () {

    var me = this;

    this.uploadCrop = $('#upload-foto').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport: {
            width: 150,
            height: 200
        },
        boundary: {
            width: 200,
            height: 250
        }
    });

    $('#foto').on('change', function () {
        me.readFile(this);
    });
    $('#guardarAlta').click(this.onClickGuardar.bind(this));
    $('.modalidad input[type=radio]').change(this.onChangeCheckDomicilio);
};

AltaPremium.prototype.readFile = function (input) {
    var me = this;
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.upload-foto').addClass('ready');
            me.uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function () {
            });

        }
        reader.readAsDataURL(input.files[0]);
    }
};

AltaPremium.prototype.onChangeCheckDomicilio = function () {
    if ($(this).is(':checked') && $(this).attr('id') == 'checkSubirFotografia') {
        $('.foto-div').removeClass("hidden");
        $('input[name="foto"]').attr('required', 'required')
    } else {
        $('.foto-div').addClass("hidden");
        $('input[name="foto"]').removeAttr('required');
    }

};

AltaPremium.prototype.onClickGuardar = function () {

    var me = this;
    $("#form-altapremium").parent().addClass("loading-mask");
    if ($("#form-altapremium").valid()) {
        var formData = new FormData();
        var modalidad= $('.modalidad input[type=radio]:checked').val();
        formData.append('modalidad', modalidad);
        formData.append('iban', $('input[name="iban"]').val());
        formData.append('aceptaLOPD', $('input[name="aceptaLOPD"]').is(':checked'));
        if($('input[type=file][name="documentacion"]')[0].files[0]) {
            formData.append('documentacion', $('input[type=file][name="documentacion"]')[0].files[0]);
        }
        formData.append('comoConoces', $("#comoConoces option:selected").val());

        if (modalidad == 14613872) {
            this.uploadCrop.croppie('result', {
                type: 'blob',
                size: 'viewport'
            }).then(function (resp) {
                formData.append('foto', resp, 'foto.png');
                me._enviaForm(formData);
            });
        } else {
            me._enviaForm(formData);

        }

    }else{
        $("#form-altapremium").parent().removeClass("loading-mask");
    }

};
AltaPremium.prototype._enviaForm = function (formData) {
    $.ajax({
        url: 'alta',
        type: 'post',
        data: formData,
        // cache: false,
        contentType: false,
        processData: false,
        success: function (respuesta) {
            $("#form-altapremium").parent().removeClass("loading-mask");
            $("<div></div>").dialog({
                resizable: false,
                title: i18n.get_string('informacion'),
                modal: true
            }).text(i18n.get_string("solicitudaltacorrecta"));
            setTimeout(function(){
                window.location=window.location.pathname.split('/campanyas/premium/alta')[0]+'/#campanyes';
            }, 3000);


        },
        error: function () {
            $("#form-altapremium").parent().removeClass("loading-mask");

        }
    });
};


