
$(document).ready(function () {
    var _bajaPremium = new BajaPremium();
});

/**
 * Objeto que controla el formulario de datos academicos
 * @constructor
 */

BajaPremium = function (){
    $('#guardarBaja').click(this.onClickGuardarBaja.bind(this));
    $('.motivo input[type=radio]').change(this.onChangeCheck);

};

BajaPremium.prototype.onClickGuardarBaja = function () {

    var me = this;
    $("#form-bajaPremium").parent().addClass("loading-mask");
    if ($("#form-bajaPremium").valid()) {
        var formData = new FormData();
        var motivo= $('.motivo input[type=radio]:checked').val();
        formData.append('motivo', motivo);
        var explicacionMotivo = $('#explicacion-motivo').val();
        formData.append('explicacion-motivo', explicacionMotivo);
        me._enviaForm(formData);
    }else{
        $("#form-bajaPremium").parent().removeClass("loading-mask");
    }

};

BajaPremium.prototype._enviaForm = function (formData) {
    $.ajax({
        url: 'baja',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        success: function (respuesta) {
            $("#form-bajaPremium").parent().removeClass("loading-mask");
            $("<div></div>").dialog({
                resizable: false,
                title: i18n.get_string('informacion'),
                modal: true
            }).text(i18n.get_string('redirigir'));
            setTimeout(function(){
                window.location=window.location.pathname.split('/campanyas/premium/baja')[0]+'/#campanyes';
            }, 3000);


        },
        error: function () {
            $("#form-bajaPremium").parent().removeClass("loading-mask");

        }
    });
};

BajaPremium.prototype.onChangeCheck = function () {
    var check= $('.motivo input[type=radio]:checked').val();
    $('#div-4739613').addClass("hidden");
    $('#campo-extra-4739613').attr('disabled', 'disabled');

    $('#div-3916810').addClass("hidden");
    $('#campo-extra-3916810').attr('disabled', 'disabled');

    $('#div-4739615').addClass("hidden");
    $('#campo-extra-4739615').attr('disabled', 'disabled');

    $('#div-4739621').addClass("hidden");
    $('#campo-extra-4739621').attr('disabled', 'disabled');

    $('#div-4739622').addClass("hidden");
    $('#campo-extra-4739622').attr('disabled', 'disabled');

    $('#div-4739623').addClass("hidden");
    $('#campo-extra-4739623').attr('disabled', 'disabled');

    $('#div-4832298').addClass("hidden");
    $('#campo-extra-4832298').attr('disabled', 'disabled');

    $('#div-4975736').addClass("hidden");
    $('#campo-extra-4975736').attr('disabled', 'disabled');

    if (check == 3412500) {
        $('#div-4739613').removeClass("hidden");
        $('#campo-extra-4739613').removeAttr('disabled');
    }
    if (check == 3412501){
        $('#div-4739615').removeClass("hidden");
        $('#campo-extra-4739615').removeAttr('disabled');
    }
    if (check == 3412502){
        $('#div-4739621').removeClass("hidden");
        $('#campo-extra-4739621').removeAttr('disabled');

    }
    if (check == 3412503){
        $('#div-4739622').removeClass("hidden");
        $('#campo-extra-4739622').removeAttr('disabled');

    }
    if (check == 4752705){

        $('#div-4832298').removeClass("hidden");
        $('#campo-extra-4832298').removeAttr('disabled');

    }
    if (check == 4975589){
        $('#div-4975736').removeClass("hidden");
        $('#campo-extra-4975736').removeAttr('disabled');

    }
    if (check == 3412506){
        $('#div-3916810').removeClass("hidden");
        $('#campo-extra-3916810').removeAttr('disabled');

    }
    if (check == 3412505){
        $('#div-4739623').removeClass("hidden");
        $('#campo-extra-4739623').removeAttr('disabled');
    }
    $('#div-explicacion-motivo').removeClass("hidden");
    $('#explicacion-motivo').removeAttr('disabled');
};