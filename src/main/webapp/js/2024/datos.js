$(document).ready(function () {

    var dialog = $("#tratamientoVentana").dialog({
        autoOpen: true,
        width: 'auto',
        modal: true,
        position: ['center', 120],
        buttons: [
            {
                text: "Ok",
                click: function () {
                    $.ajax({
                        url: 'tratamiento',
                        type: "POST",
                        dataType: 'json',
                        callback: function (result) {
                            // window.location.href = '/crm/rest/alumni2024/'
                        },
                        error: function (xhr, resp, text) {
                        }
                    });
                    $(this).dialog("close");
                }
            }
        ]
    });

    var _formDatos = new Datos();

    $('.proteccion a').attr('target', '_blank');


});

Datos = function () {

    $('[name=m-desconectar]').click(this.logout.bind(this));
};


Datos.prototype.logout = function () {

    $.ajax({
        type: 'GET',
        url: 'logout/',
        encode: true
    }).done(function (data) {
        $("<div></div>").dialog({
            resizable: false,
            title: i18n.get_string('informacion'),
            modal: true
        }).text(i18n.get_string('redirigir'));
        // setTimeout(function () {
        //     window.location = data;
        // }, 3000);
        // console.log(data);
        // window.location.href = data;
    })
};


