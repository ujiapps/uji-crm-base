$(document).ready(function () {
    var _formEspacio = new Espacio();
    $(".cuentas-accordion-button").click(function(){
        $(".contenido-accordion").toggleClass("hide");
    });
});

Espacio = function () {

    $('button[name=solicitarCorporativo]').click(this.onClickSolicitarCorporativo.bind(this));
    $(":checkbox").change(function () {
        var me = this;
        $("#espacioAlumniTab").addClass("loading-mask");
        $.ajax({
            type: 'POST',
            url: 'suscripciones',
            data: {checked: $(me).prop("checked"), itemId: $(this).data("itemid"), tipo: $(this).data("tipoitem")},
            encode: true
        }).done(function () {
            $("#espacioAlumniTab").removeClass("loading-mask");

        }).fail(function () {
            $("#espacioAlumniTab").removeClass("loading-mask");
        });
    });
    $("#boton-menu").click(this.onClickBotonMenu.bind(this));

    this.getSuscriptores();

};

Espacio.prototype.obtenerRaizFija = function(url) {
    const partes = url.split('/');
    return partes.slice(0, 5).join('/') + "/";
}

Espacio.prototype.onClickBotonMenu = function () {
    window.location.href = this.obtenerRaizFija(window.location.pathname);
}

Espacio.prototype.getSuscriptores = function () {
    $("#espacioAlumniTab").addClass("loading-mask");
    $.ajax({
        url: 'suscripciones',
        success: function (respuesta) {
            var data = respuesta.data;
            data.forEach(function (item) {
                $("#correo-" + item.itemId).prop("checked", (item.correo === "true") ? true : false);
            });
            $("#espacioAlumniTab").removeClass("loading-mask");
        },
        error: function () {
            $("#espacioAlumniTab").removeClass("loading-mask");
        }
    });
};

Espacio.prototype.datosValidos = function () {
    var personales = $('#form-personales');
    var laborales = $('#form-laborales');
    if (!personales.valid()) {
        $('[href="#personales"]').click();
        return false;
    } else if (!laborales.valid()) {
        $('[href="#laborales"]').click();
        return false;
    } else {
        return true;
    }
};

Espacio.prototype.onClickSolicitarCorporativo = function () {
    $("#solicitarCorporativo").addClass("loading-mask");
    $.ajax({
        type: 'POST',
        url: 'corporativo',
        data: {alta: true},
        encode: true
    }).done(function (data) {
        let solicitarCorporativo = $("#solicitarCorporativo");
        solicitarCorporativo.text(" S'ha generat l'usuari: " + data.data.usuario + ". El compte corporatiu estarà actiu en 24 hores.");
        solicitarCorporativo.removeClass("loading-mask");
        solicitarCorporativo[0].style.pointerEvents = "none";
        solicitarCorporativo[0].style.color = "#0A0A0A";
        solicitarCorporativo[0].style.backgroundColor = "#fff";
        solicitarCorporativo[0].style.fontWeight = "bold";
    }).fail(function () {
        $("#solicitarCorporativo").removeClass("loading-mask");
    });

};

