$(document).ready(function () {
    var _formularioAltaOipep = new FormularioAltaOipep();

    $('#nivelEstudios').change(function () {
        let selec = $('#nivelEstudios :selected');
        let divNombreEstudio = $('#divNombreEstudio');
        let nombreEstudio = $('#nombreEstudio');
        let divEstudioUJINoUJI = $('#divEstudioUJINoUJI');
        let estudioUJINoUJI = $('#estudioUJINoUJI');
        let divEstudiosSuperiores = $('#divEstudiosSuperiores');
        let estudiosSuperiores = $('#estudiosSuperiores');

        $('#divTipoEstudioUJINoUJI').addClass('hide');
        $('#tipoEstudioUJINoUJI').attr("disabled", true);

        divNombreEstudio.addClass('hide');
        nombreEstudio.attr("disabled", true);

        divEstudioUJINoUJI.addClass('hide');
        estudioUJINoUJI.attr("disabled", true);

        divEstudiosSuperiores.addClass('hide');
        estudiosSuperiores.attr("disabled", true);

        if (selec.attr('data-referencia') === 'ELEM' || selec.attr('data-referencia') === 'MIT') {
            divNombreEstudio.removeClass('hide');
            nombreEstudio.attr("disabled", false);
        } else if (selec.attr("data-referencia") === 'NO') {
            divEstudiosSuperiores.removeClass('hide');
            estudiosSuperiores.attr("disabled", false);
        } else if (selec.attr("data-referencia") === 'SUP') {
            divEstudioUJINoUJI.removeClass('hide');
            estudioUJINoUJI.attr("disabled", false);
        }
    });

    $('#laboral').change(function () {
        var selec = $('#laboral :selected')[0].value;
        let divEstudiante = $('#divEstudiante');
        let lugarEstudiante = $('#lugarEstudiante');
        let divLaboralActivo = $('#divLaboralActivo');
        let sectorEmpresarial = $('#sectorEmpresarial');
        let tipoTrabajo = $('#tipoTrabajo');

        //Poner divs y elementos en estado inicial
        divEstudiante.addClass('hide');
        lugarEstudiante.attr("disabled", true);

        divLaboralActivo.addClass('hide');
        sectorEmpresarial.attr("disabled", true);
        tipoTrabajo.attr("disabled", true);

        if (selec === '996676') { // Activa divs y elementos para opción laboral activo
            divLaboralActivo.removeClass('hide');
            sectorEmpresarial.attr("disabled", false);
            tipoTrabajo.attr("disabled", false);
        } else if (selec === '1638182') { // Activa divs y elementos para opción laboral estudiante
            divEstudiante.removeClass('hide');
            lugarEstudiante.attr("disabled", false);
        }
    });

    $('#estudioUJINoUJI').change(function () {
        var selec = $('#estudioUJINoUJI :selected');
        $('#divTipoEstudioUJINoUJI').addClass('hide');
        $('#tipoEstudioUJINoUJI').attr("disabled", true);
        $('#universidadEstudio').attr("disabled", true);
        $('#anyoFinalizacionEstudio').attr("disabled", true);
        $('#listaEstudiosGrado').attr("disabled", true);
        $('#nombreEstudioNoGrado').attr("disabled", true);

        if (selec.attr('data-referencia') === 'NOUJI') {
            $('#divTipoEstudioUJINoUJI').removeClass('hide');
            $('#tipoEstudioUJINoUJI').attr("disabled", false);
        }
    });

    $('#tipoEstudioUJINoUJI').change(function () {
        var selec = $('#tipoEstudioUJINoUJI :selected')[0].value;
        $('#universidadEstudio').attr("disabled", false);
        $('#anyoFinalizacionEstudio').attr("disabled", false);
        $('#listaEstudiosGrado').attr("disabled", true);
        $('#nombreEstudioNoGrado').attr("disabled", true);
        $('#divNombreEstudioNoGrado').addClass('hide');
        $('#divListaEstudiosGrado').addClass('hide');

        if (selec === 'GRADO') {
            $('#divListaEstudiosGrado').removeClass('hide');
            $('#listaEstudiosGrado').attr("disabled", false);
        } else if (selec === 'POST') {
            $('#nombreEstudioNoGrado').attr("disabled", false);
            $('#divNombreEstudioNoGrado').removeClass('hide');
        } else if (selec === 'DOC') {
            $('#nombreEstudioNoGrado').attr("disabled", false);
            $('#divNombreEstudioNoGrado').removeClass('hide');
        } else {
            $('#nombreEstudioNoGrado').attr("disabled", true);
            $('#universidadEstudio').attr("disabled", true);
            $('#anyoFinalizacionEstudio').attr("disabled", true);
            $('#divNombreEstudioNoGrado').removeClass('hide');
        }
    });
});


FormularioAltaOipep = function () {

    this._formularioAltaOipep = $("#formularioalta");
    this._formularioAltaOipep.submit(this.onClickFormulario.bind(this));

    this._formularioAltaOipep.validate({
        ignore: '.ignore'
    });

    $('#enviar').click(this.onClickFormulario.bind(this));

    var me = this;

    me.uploadCrop = $('#upload-foto').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport: {
            width: 150,
            height: 200
        },
        boundary: {
            width: 200,
            height: 250
        }
    });

    $('#foto').on('change', function () {
        $('#upload-foto').removeClass('hide');
        me.readFile(this);
    });
};

FormularioAltaOipep.prototype.readFile = function (input) {
    var me = this;
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.upload-foto').addClass('ready');
            me.uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function () {
            });

        }
        reader.readAsDataURL(input.files[0]);
    }
};

FormularioAltaOipep.prototype.compruebaIBANEspanya = function (iban) {
    return (iban.substring(0, 2) === "ES");
}

FormularioAltaOipep.prototype.onClickFormulario = function (event) {
    event.preventDefault();
    let form = this._formularioAltaOipep;
    let formData = new FormData(form[0]);
    let me = this;

    form.addClass("loading-mask");

    if (!form.valid()) {
        form.removeClass("loading-mask");
        return;
    }

    if (formData.get("foto").name === "" ){
        me._enviaForm(formData);
        return;
    }

    this.uploadCrop.croppie('result', {
        type: 'blob',
        size: 'viewport'
    }).then(function (resp) {
        formData.delete("foto");
        formData.append('foto', resp, 'foto.png');

        me._enviaForm(formData);
    });
};

FormularioAltaOipep.prototype.obtenerRaizFija = function(url) {
    const partes = url.split('/');
    return partes.slice(0, 5).join('/') + "/";
}

FormularioAltaOipep.prototype._enviaForm = function (formData) {
    let form = this._formularioAltaOipep;
    let me = this;

    $.ajax({
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'text',
        success: function () {
            $("<div></div>").dialog({
                resizable: false,
                title: i18n.get_string('informacion'),
                modal: true,
                closeText: 'X'
            }).text(i18n.get_string('solicitudaltacorrecta'));
            setTimeout(function(){
                form.parent().removeClass("loading-mask");
                window.location.href = me.obtenerRaizFija(window.location.pathname) + "campanyas/zonaprivada/";
            })
        },
        error: function () {
            $("<div></div>").dialog({
                resizable: false,
                title: i18n.get_string('informacion'),
                modal: true,
                closeText: 'X'
            }).text(i18n.get_string('envioerror'));
            setTimeout(function(){
                form.parent().removeClass("loading-mask");
                window.location.href = me.obtenerRaizFija(window.location.pathname) + "campanyas/zonaprivada/";
            })

        }
    });
};