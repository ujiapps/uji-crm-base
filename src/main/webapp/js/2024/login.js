$(document).ready(function () {
    var _login = new Login();
});

Login = function () {
    this._loginUsuarioCorreo = $("#botonUsuarioCorreo");
    this._loginUsuarioCorreo.click(this.onClickLoginUsuarioCorreo.bind(this));
};

Login.prototype.onClickLoginUsuarioCorreo = function (event) {
    const inputCorreo = document.getElementById("usuarioCorreo")
    if (inputCorreo.checkValidity()) {
        $.ajax({
            url: '/crm/rest/alumni2024/login/hash/',
            type: "POST",
            dataType: 'json',
            data: {correo: inputCorreo.value},
            success: function (result) {
                if (result.success) {
                    window.location.href = window.location.origin + '/crm/rest/alumni2024/login/avisook';
                } else {
                    window.location.href = window.location.origin + '/crm/rest/alumni2024/login/avisoko';
                }
            },
            error: function (xhr, resp, text) {
            }
        });
    }
    else {
        inputCorreo.setAttribute("style", "border-color:red;");
        const aviso = document.getElementById("usuarioCorreoVacio");
        document.getElementById("usuarioCorreoVacio").classList.remove("hidden");
    }
};