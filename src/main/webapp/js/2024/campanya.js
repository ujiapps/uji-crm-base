$(document).ready(function () {
    var _formCampa = new Campanya();
});

/**
 * Objeto que controla el formulario de datos academicos
 * @constructor
 */
Campanya = function () {

    $('input[name=altaPremium]').click(this.onClickAltaPremium.bind(this));
    $('input[name=bajaPremium]').click(this.onClickBajaPremium.bind(this));
    $('input[name=bajaBasic]').click(this.onClickBajaBasic.bind(this));
    $('input[name=altaBasic]').click(this.onClickAltaBasic.bind(this));
    $('input[name=altaOIPEP]').click(this.onClickAltaOIPEP.bind(this));
    $("#boton-menu").click(this.onClickBotonMenu.bind(this));
};

Campanya.prototype.obtenerRaizFija = function (url) {
    const partes = url.split('/');
    return partes.slice(0, 5).join('/') + "/";
}

Campanya.prototype.onClickBotonMenu = function () {
    window.location.href = this.obtenerRaizFija(window.location.pathname);
}

Campanya.prototype.onClickAltaPremium = function () {
    let me = this;
    $("#campanyas").addClass("loading-mask");
    $.ajax({
        type: 'GET',
        url: me.obtenerRaizFija(window.location.pathname) + 'campanyas/zonaprivada/gethash/',
        success: function (data) {
            // Redirigir después de procesar los datos
            window.location.href = me.obtenerRaizFija(window.location.pathname) + 'premium/alta/' + data.data.hash;
        },
        error: function (xhr, status, error) {
            console.error('Error:', error);
        }
    });
};

Campanya.prototype.onClickAltaOIPEP = function () {
    let me = this;
    $("#campanyas").addClass("loading-mask");
    $.ajax({
        type: 'GET',
        url: me.obtenerRaizFija(window.location.pathname) + 'campanyas/zonaprivada/gethash/',
        success: function (data) {
            // Redirigir después de procesar los datos
            var campanyaId = $('[name=altaOIPEP]').data('campanyaid');
            window.location.href = me.obtenerRaizFija(window.location.pathname) + 'campanyas/oipep/alta/' + campanyaId + '/' + data.data.hash;
        },
        error: function (xhr, status, error) {
            console.error('Error:', error);
        }
    });
};

Campanya.prototype.onClickBajaPremium = function () {
    window.location = this.obtenerRaizFija(window.location.pathname) + 'campanyas/premium/baja/';
};

Campanya.prototype.onClickAltaBasic = function () {
    $("#campanyas").addClass("loading-mask");
    let me = this;
    $.ajax({
        type: 'POST',
        url: me.obtenerRaizFija(window.location.pathname) + 'campanyas/basic/alta/'
    }).done(function () {
        window.location.href = me.obtenerRaizFija(window.location.pathname) + 'campanyas/zonaprivada/';
    }).fail(function () {
        window.location.href = me.obtenerRaizFija(window.location.pathname) + 'campanyas/zonaprivada/error/';
    });
};

Campanya.prototype.onClickBajaBasic = function () {
    window.location = this.obtenerRaizFija(window.location.pathname) + 'campanyas/basic/baja/';
};

