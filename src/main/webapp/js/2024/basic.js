$(document).ready(function () {

    var _basic = new Basic();
});

Basic = function () {

    var me = this;

    $('#guardarBaja').click(this.onClickGuardar.bind(this));
    $('#boton-menu').click(this.onClickMenu.bind(this));
};
Basic.prototype.onClickMenu = function () {
    $("#form-bajaBasic").parent().addClass("loading-mask");
    window.location=window.location.pathname.split('/basic/baja')[0] + '/zonaprivada/';
}

Basic.prototype.onClickGuardar = function () {

    var me = this;
    $("#form-bajaBasic").parent().addClass("loading-mask");
    if ($("#form-bajaBasic").valid()) {
        var formData = new FormData();
        var motivo = $('.motivo input[type=radio]:checked').val();
        formData.append('motivo', motivo);
        me._enviaForm(formData);
    } else {
        $("#form-bajaBasic").parent().removeClass("loading-mask");
    }

};
Basic.prototype._enviaForm = function (formData) {

    $.ajax({
        url: 'baja',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        success: function (respuesta) {
            $("#form-bajaBasic").parent().removeClass("loading-mask");
            $("<div></div>").dialog({
                resizable: false,
                title: i18n.get_string('informacion'),
                modal: true
            }).text(i18n.get_string("solicitudbajacorrecta"));
            setTimeout(function () {
                window.location=window.location.pathname.split('/basic/baja')[0] + '/zonaprivada/';

            }, 3000);


        },
        error: function () {
            $("#form-bajaBasic").parent().removeClass("loading-mask");

        }
    });
};




