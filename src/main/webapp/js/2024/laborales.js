$(document).ready(function () {
    var _formLabo = new Laborales();
    let formLaborales = $("#form-laborales");

    $(".guardar-laboral").click(function () {
        formLaborales.submit();
    });

    formLaborales.on("submit", function (event) {

        const partes = window.location.pathname.split('/');

        if (formLaborales.valid()) {
            formLaborales.addClass("loading-mask");
            $.ajax({
                type: 'POST',
                url: partes.slice(0, 5).join('/') + "/laborales/",
                data: formLaborales.serialize(),
                encode: true
            }).done(function () {
                $("<div></div>").dialog({
                    resizable: false,
                    title: i18n.get_string('informacion'),
                    modal: true,
                    closeText: 'X'
                }).text(i18n.get_string('datosactualizados'));
                formLaborales.removeClass("loading-mask");

            }).fail(function () {
                $("<div></div>").dialog({
                    resizable: false,
                    title: i18n.get_string('informacion'),
                    modal: true,
                    closeText: 'X'
                }).text(i18n.get_string('rellenadatosincorrectos'));
                formLaborales.removeClass("loading-mask");
            });
        }
        event.preventDefault();
    });
    formLaborales.validate({
        ignore: '.ignore'
    });

});

/**
 * Objeto que controla el formulario de datos Laborales
 * @constructor
 */
Laborales = function () {
    $("#boton-menu").click(this.onClickBotonMenu.bind(this));

    this._estadoLaboral = $('#estadoLaboral');
    this._estadoLaboral.change(this.onEstadoLaboralSelected.bind(this));
    this.getDataLabo();

};

Laborales.prototype.obtenerRaizFija = function (url) {
    const partes = url.split('/');
    return partes.slice(0, 5).join('/') + "/";
}


Laborales.prototype.onClickBotonMenu = function () {
    window.location.href = this.obtenerRaizFija(window.location.pathname);
}

Laborales.prototype.getDataLabo = function () {
    var me = this;

    $("#form-laborales").addClass("loading-mask");
    $.ajax({
        url: me.obtenerRaizFija(window.location.pathname) + 'laborales',
        success: function (respuesta) {
            me.data = respuesta.data;
            me.loadForm();
        },
        error: function () {
            $("#form-laborales").removeClass("loading-mask");
        }
    });
};

Laborales.prototype.loadForm = function () {
    $('#estadoLaboral').val(this.data.estadoLaboral).trigger('change');
    $('#sectorEmpresarial').val(this.data.sectorEmpresarial);
    $('#empresa').val(this.data.empresa);
    $('#cargo').val(this.data.cargo);
    $('#nombreEstudioActual').val(this.data.nombreEstudioActual);
    $("#form-laborales").removeClass("loading-mask");
    $("#form-laborales > .datos").removeClass("hide").addClass("visible");
    $("#linkedin").val(this.data.linkedin);
};

Laborales.prototype._disableField = function (element) {
    var select = element.find('select');
    element.addClass('hide').removeClass("visible");
    select.val('');
    select.prop('disabled', true);
};
Laborales.prototype._enableField = function (element) {
    var select = element.find('select');
    element.removeClass('hide').addClass("visible");
    select.prop('disabled', false);
};

Laborales.prototype.onEstadoLaboralSelected = function () {
    var me = this;
    if (me._estadoLaboral.val() == 996676) {
        $('label[for=sectorEmpresarial],#sectorEmpresarial').removeClass('hide').addClass("visible");
        $('#sectorEmpresarial').prop('disabled', false);
        $('label[for=empresa],#empresa').removeClass('hide').addClass("visible");
        $('#empresa').prop('disabled', false);
        $('label[for=cargo],#cargo').removeClass('hide').addClass("visible");
        $('#cargo').prop('disabled', false);
        $('#divNombreEstudioActual').addClass('hide').removeClass("visible");
        $('#nombreEstudioActual').prop('disabled', true);
    } else if (me._estadoLaboral.val() == 1638182) {
        $('label[for=sectorEmpresarial],#sectorEmpresarial').addClass('hide').removeClass("visible");
        $('#sectorEmpresarial').prop('disabled', true);
        $('label[for=empresa],#empresa').addClass('hide').removeClass("visible");
        $('#empresa').prop('disabled', true);
        $('label[for=cargo],#cargo').addClass('hide').removeClass("visible");
        $('#cargo').prop('disabled', true);
        $('#divNombreEstudioActual').removeClass('hide').addClass("visible");
        $('#nombreEstudioActual').prop('disabled', false);
    } else {
        $('label[for=sectorEmpresarial],#sectorEmpresarial').addClass('hide').removeClass("visible");
        $('#sectorEmpresarial').prop('disabled', true);
        $('label[for=empresa],#empresa').addClass('hide').removeClass("visible");
        $('#empresa').prop('disabled', true);
        $('label[for=cargo],#cargo').addClass('hide').removeClass("visible");
        $('#cargo').prop('disabled', true);
        $('#divNombreEstudioActual').addClass('hide').removeClass("visible")
        $('#nombreEstudioActual').prop('disabled', true);
    }
}


