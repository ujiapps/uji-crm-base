$(document).ready(function () {
    var _formAca = new Academicos();

    $("#form-academicos").submit(function (event) {
        if ($("#form-academicos").valid()) {
            _formAca.addEstudio();
        }
        event.preventDefault();
    });
    $("#form-academicos").validate({
        ignore: '.ignore'
    });
});

/**
 * Objeto que controla el formulario de datos academicos
 * @constructor
 */
Academicos = function () {
    let me = this;

    $("#boton-menu").click(this.onClickBotonMenu.bind(this));

    this.loadAcademicos();
    this.loadEstudiosNouji();
    $("#anyadirEstudio").on("click", function () {
        me.showFormularioAltaEstudios();
    });
    this._tipoEstudio = $('#tipoEstudio');
    this._tipoEstudio.change(this.onTipoEstudioSelected.bind(this));
};

Academicos.prototype.obtenerRaizFija = function(url) {
    const partes = url.split('/');
    return partes.slice(0, 5).join('/') + "/";
}

Academicos.prototype.onClickBotonMenu = function () {
    window.location.href = this.obtenerRaizFija(window.location.pathname);
}

Academicos.prototype.showFormularioAltaEstudios = function() {
    let containerAcceptance = $("#formulario-alta-estudios");
    containerAcceptance.foundation("open");
}

Academicos.prototype.loadAcademicos = function () {

    $.ajax({
        url: this.obtenerRaizFija(window.location.pathname) + "academicos",
        success: function (respuesta) {
        },
        error: function () {
        }
    });
};

Academicos.prototype.loadEstudiosNouji = function () {
    var ref = this;
    $("#estudios-no-uji").addClass("loading-mask");
    $.ajax({
        url: ref.obtenerRaizFija(window.location.pathname) + 'academicos/estudios',
        success: function (respuesta) {
            $('#estudios-no-uji').html(respuesta);
            $('.eliminarEstudio').click(ref.eliminarEstudio);
            $("#estudios-no-uji").removeClass("loading-mask");
        },
        error: function () {
            $("#estudios-no-uji").removeClass("loading-mask");
        }
    });
};

Academicos.prototype.eliminarEstudio = function () {
    $("#estudios-no-uji").addClass("loading-mask");
    var estudioId = $(this).attr('data-itemid');
    $.ajax({
        type: 'DELETE',
        url: 'estudios/' + estudioId,
        encode: true,
        success: function (respuesta) {
            $("#estudio-" + estudioId).remove();
            $("#estudios-no-uji").removeClass("loading-mask");
        },
        error: function () {
            $("#estudios-no-uji").removeClass("loading-mask");
        }
    });
};

Academicos.prototype.addEstudio = function () {
    var me = this;
    $("#form-academicos").addClass("loading-mask");
    $.ajax({
        type: 'POST',
        url: 'estudios',
        data: $("#form-academicos").serialize(),
        encode: true,
        success: function (respuesta) {
            $("#form-academicos").removeClass("loading-mask");
            window.location.href = window.location.pathname;
        },
        error: function () {
            $("#form-academicos").removeClass("loading-mask");
        }
    });
};

Academicos.prototype.onTipoEstudioSelected = function () {
    var tipoEstudio = $('#tipoEstudio').val();
    $('#ambitoEstudios').removeClass("visible").removeClass("visible").addClass("hide")
    $('#ambitoEstudios > select').prop('disabled', true);
    $('#estudiosSuperiores').removeClass("visible").removeClass("visible").addClass("hide")
    $('#estudiosSuperiores > select').prop('disabled', true);
    $('#secciongrados').removeClass("visible").addClass("hide")
    $('#secciongrados > select').prop('disabled', true);
    $('#seccionpostgrado').removeClass("visible").addClass("hide")
    $('#seccionpostgrado > select').prop('disabled', true);
    $('#secciondoctorado').removeClass("visible").addClass("hide")
    $('#secciondoctorado > select').prop('disabled', true);
    $('#seccionUniversidad').removeClass("visible").addClass("hide")
    $('#seccionUniversidad > select').prop('disabled', true);
    $('#seccionNombre').removeClass("visible").addClass("hide")
    $('#seccionNombre > input').prop('disabled', true);
    $('#seccionAnyoFin').removeClass("visible").addClass("hide")
    $('#seccionNombre > input').prop('disabled', true);

    if (tipoEstudio == 'ELEM') {
        $('#seccionNombre').removeClass("hide").addClass("visible");
        $('#seccionNombre > input').prop('disabled', false);
    }

    if (tipoEstudio == 'MIT') {
        $('#ambitoEstudios').removeClass("hide").addClass("visible");
        $('#ambitoEstudios > select').prop('disabled', false);
        $('#seccionNombre').removeClass("hide").addClass("visible");
        $('#seccionNombre > input').prop('disabled', false);
    }

    if (tipoEstudio == 'NO') {
        $('#estudiosSuperiores').removeClass("hide").addClass("visible");
        $('#estudiosSuperiores > select').prop('disabled', false);
        $('#seccionNombre').removeClass("hide").addClass("visible");
        $('#seccionNombre > input').prop('disabled', false);
    }
    if (tipoEstudio == 'GRADO') {
        $('#secciongrados').removeClass("hide").addClass("visible");
        $('#secciongrados > select').prop('disabled', false);
        $('#seccionUniversidad').removeClass("hide").addClass("visible");
        $('#seccionUniversidad > select').prop('disabled', false);
        $('#seccionAnyoFin').removeClass("hide").addClass("visible");
    }
    if (tipoEstudio == 'POST') {
        $('#seccionpostgrado').removeClass("hide").addClass("visible");
        $('#seccionpostgrado > select').prop('disabled', false);
        $('#seccionUniversidad').removeClass("hide").addClass("visible");
        $('#seccionUniversidad > select').prop('disabled', false);
        $('#seccionNombre').removeClass("hide").addClass("visible");
        $('#seccionNombre > input').prop('disabled', false);
        $('#seccionAnyoFin').removeClass("hide").addClass("visible").addClass("visible");
    }
    if (tipoEstudio == 'DOC') {
        $('#secciondoctorado').removeClass("hide").addClass("visible");
        $('#secciondoctorado > select').prop('disabled', false);
        $('#seccionUniversidad').removeClass("hide").addClass("visible");
        $('#seccionUniversidad > select').prop('disabled', false);
        $('#seccionNombre').removeClass("hide").addClass("visible");
        $('#seccionNombre > input').prop('disabled', false);
        $('#seccionAnyoFin').removeClass("hide").addClass("visible");
    }
};




