$(document).ready(function () {
    var _formCampa = new Campanya();
});

/**
 * Objeto que controla el formulario de datos academicos
 * @constructor
 */
Campanya = function () {

    $('input[name=altaPremium]').click(this.onClickAltaPremium.bind(this));
    $('input[name=bajaPremium]').click(this.onClickBajaPremium.bind(this));
    $('input[name=finalizaAltaPremium]').click(this.onClickFinalizaAltaPremium.bind(this));
    $('input[name=bajaBasic]').click(this.onClickBajaBasic.bind(this));
    $('input[name=altaBasic]').click(this.onClickAltaBasic.bind(this));
    $('input[name=altaOIPEP]').click(this.onClickAltaOIPEP.bind(this));
    $('input[name=finalizaAltaOIPEP]').click(this.onClickFinalizaAltaOIPEP);
};


Campanya.prototype.datosValidos = function () {
    var personales = $('#form-personales');
    var laborales = $('#form-laborales');
    if (!personales.valid()) {
        $('[href="#personales"]').click();
        return false;
    } else if (!laborales.valid()) {
        $('[href="#laborales"]').click();
        return false;
    } else {
        return true;
    }
};

Campanya.prototype.rellenaFormulario = function () {
    // $("#pagina").removeClass('pagina');
    $(".contenido").addClass('pagina-formulario');
    $('#titulosTabs').addClass('hidden');

    $(".siguiente").show();
    $(".guarda").hide();
    $(".usuarioCorporativo").addClass('hidden');

    $('[href="#personales"]').click();
}
;

Campanya.prototype.onClickAltaPremium = function () {

    this.rellenaFormulario();
    $(".siguiente-espacio").click(this.onClickFinalizaAltaPremium.bind(this));
};

Campanya.prototype.onClickAltaOIPEP = function () {
    this.rellenaFormulario();
    $(".siguiente-espacio").click(this.onClickFinalizaAltaOIPEP.bind(this));
};

Campanya.prototype.onClickFinalizaAltaPremium = function () {
    if (this.datosValidos()) {
        window.location = 'campanyas/premium/alta';
    }
    else {
        $.alert(i18n.get_string('rellenadatosincorrectos'), i18n.get_string('datosincorrectos'));
    }
};

Campanya.prototype.onClickFinalizaAltaOIPEP = function () {
    var campanyaId = $('[name=altaOIPEP]').data('campanyaid');
    window.location = 'campanyas/oipep/alta/' + campanyaId;
};

Campanya.prototype.onClickBajaPremium = function () {
    window.location = 'campanyas/premium/baja';
};

Campanya.prototype.onClickAltaBasic = function () {
    $("#campanyesTab").addClass("loading-mask");
    $.ajax({
        type: 'POST',
        url: 'campanyas/basic/alta'
    }).done(function (data) {
        $(".baja-basic").removeClass("hidden");
        $(".alta-basic").addClass("hidden");
        $("#campanyesTab").removeClass("loading-mask");
    }).fail(function () {
        $("#campanyesTab").removeClass("loading-mask");
    });
};

Campanya.prototype.onClickBajaBasic = function () {
    window.location = 'campanyas/basic/baja';
};

