$(document).ready(function () {
    var _acceso = new Acceso();

    $('#tipoIdentificacion').change(function () {

        var selec = $('#tipoIdentificacion :selected').text();
        if (selec == 'DNI') {

            $('#identificacion').rules('add', 'nifES');
            $('#identificacion').rules('remove', 'cifES nieES');
        } else if (selec == 'CIF') {
            $('#identificacion').rules('add', 'cifES');
            $('#identificacion').rules('remove', 'nifES nieES');
        } else if (selec == 'NIE') {
            $('#identificacion').rules('add', 'nieES');
            $('#identificacion').rules('remove', 'cifES nifES');
        } else {
            $('#identificacion').rules('remove', 'nifES cifES nieES');

        }
    });

});

Acceso = function () {

    this._acceso = $("#accesoFormularioPremium");
    this._acceso.submit(this.onClickAcceso.bind(this));

    this._acceso.validate({
        ignore: '.ignore'
    });

};

Acceso.prototype.comprobarFecha = function (e) {
    let hoy = new Date()
    let mayoriaEdad = 18 * 365 * 24 * 60 * 60 * 1000
    let dateInp = new Date(e.val())

    return (hoy - dateInp >= mayoriaEdad);
};


Acceso.prototype.onClickAcceso = function (event) {
    let fechaNacimiento = $('#fechaNacimiento');
    let errorFechaNacimiento = $('#fechaNacimiento-error');
    errorFechaNacimiento.remove();

    if (!this.comprobarFecha(fechaNacimiento)) {
        event.preventDefault();
        fechaNacimiento[0].classList.remove('valid');
        fechaNacimiento[0].parentElement.insertAdjacentHTML('beforeend', '<p id="fechaNacimiento-error" class="error" for="fechaNacimiento">Fecha incorrecta</p>');
    }
};