$(document).ready(function () {
    var _formBajaMensajeria = new BajaMensajeria();
});

BajaMensajeria = function () {

    $(":checkbox").change(function () {
        var me = this;
        $("#bajaMensajeria").addClass("loading-mask");
        $.ajax({
            type: 'POST',
            url: '/crm/rest/baja',
            data: {checked: $(me).prop("checked"), itemId: $(this).data("itemid"), hash: $(this).data("hash")},
            encode: true
        }).done(function (data) {
            $("#bajaMensajeria").removeClass("loading-mask");
        }).fail(function () {
            $("#bajaMensajeria").removeClass("loading-mask");
        });
    });
};



