$(document).ready(function () {
    var _formEspacio = new Espacio();
});

Espacio = function () {

    $('div[name=solicitarCorporativo]').click(this.onClickSolicitarCorporativo.bind(this));
    $(".siguiente-espacio").click(this.onClickFinalizaAltaPremium.bind(this));
    $(":checkbox").change(function () {
        var me = this;
        $("#espacioAlumniTab").addClass("loading-mask");
        $.ajax({
            type: 'POST',
            url: 'espacio/suscripciones',
            data: {checked: $(me).prop("checked"), itemId: $(this).data("itemid"), tipo: $(this).data("tipoitem")},
            encode: true
        }).done(function (data) {
            $("#espacioAlumniTab").removeClass("loading-mask");
        }).fail(function () {
            $("#espacioAlumniTab").removeClass("loading-mask");
        });
    });

    this.getSuscriptores();

};


Espacio.prototype.getSuscriptores = function () {
    $("#espacioAlumniTab").addClass("loading-mask");
    $.ajax({
        url: 'espacio/suscripciones',
        success: function (respuesta) {
            var data = respuesta.data;
            data.forEach(function (item) {
                $("#correo-" + item.itemId).prop("checked", (item.correo === "true") ? true : false);
            });
            $("#espacioAlumniTab").removeClass("loading-mask");
        },
        error: function () {
            $("#espacioAlumniTab").removeClass("loading-mask");
        }
    });
};

Espacio.prototype.datosValidos = function () {
    var personales = $('#form-personales');
    var laborales = $('#form-laborales');
    if (!personales.valid()) {
        $('[href="#personales"]').click();
        return false;
    } else if (!laborales.valid()) {
        $('[href="#laborales"]').click();
        return false;
    } else {
        return true;
    }
};

Espacio.prototype.onClickSolicitarCorporativo = function () {
    $("#solicitarCorporativo").addClass("loading-mask");
    $.ajax({
        type: 'POST',
        url: 'espacio/corporativo',
        data: {alta: true},
        encode: true
    }).done(function (data) {
        $("#solicitarCorporativo").text(" S'ha generat l'usuari: " + data.data.usuario + ". El compte corporatiu estarà actiu en 24 hores.");
        $("#solicitarCorporativo").removeClass("loading-mask");
    }).fail(function () {
        $("#solicitarCorporativo").removeClass("loading-mask");
    });

};

Espacio.prototype.onClickFinalizaAltaPremium = function () {
    if (this.datosValidos()) {
        window.location = 'campanyas/premium/alta';
    }
    else {
        $.alert(i18n.get_string('rellenadatosincorrectos'), i18n.get_string('datosincorrectos'));
    }
};