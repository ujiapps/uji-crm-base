$(document).ready(function () {
    var _formulario = new Formulario();
    $('#tipoIdentificacion').change(function () {
        var selec = $('#tipoIdentificacion :selected').text();
        if (selec == 'DNI') {
            $('#identificacion').rules('add', 'nifES');
            $('#identificacion').rules('remove', 'cifES nieES');
        } else if (selec == 'CIF') {
            $('#identificacion').rules('add', 'cifES');
            $('#identificacion').rules('remove', 'nifES nieES');
        } else if (selec == 'NIE') {
            $('#identificacion').rules('add', 'nieES');
            $('#identificacion').rules('remove', 'cifES nifES');
        } else {
            $('#identificacion').rules('remove', 'nifES cifES nieES');

        }
    });

});

Formulario = function () {

    this._formulario = $("#formularioAcceso");
    this._formulario.submit(this.onClickFormulario.bind(this));

    this._formulario.validate({
        ignore: '.ignore'
    });
};

Formulario.prototype.comprobarFecha = function(e) {
    let hoy = new Date()
    let mayoriaEdad = 18 * 365 * 24 * 60 * 60 * 1000
    let dateInp = new Date(e.val())

    return (hoy - dateInp >= mayoriaEdad);
};


Formulario.prototype.onClickFormulario = function (event) {
    this._formulario.addClass("loading-mask");
    var ref = this;
    let fechaNacimiento = $('#fechaNacimiento');

    if (!this.comprobarFecha(fechaNacimiento)){
        alert(i18n.get_string('fechaNacimientoIncorrecta'));
    }

    if ($("#formularioAcceso").valid() && this.comprobarFecha(fechaNacimiento)) {
        $.ajax({
            url: '/crm/rest/alumni/inici/formulario',
            type: "POST",
            dataType: 'json',
            data: $("#formularioAcceso").serialize(),
            success: function (result) {
                console.log(result);
                if (result.success) {
                    $("<div></div>").dialog({
                        resizable: false,
                        title: "Informació",
                        buttons: [{
                            text: "Ok",
                            icon: "ui-icon-heart",
                            click: function () {
                                $(this).dialog("close");
                                window.location='https://www.uji.es/serveis/alumnisauji/';
                            }
                        }],
                        modal: true
                    }).text(i18n.get_string('enviorealizado'));
                    ref._formulario.removeClass("loading-mask");
                } else {
                    $("<div></div>").dialog({
                        resizable: false,
                        title: "Informació",
                        modal: true,
                        buttons: [{
                            text: "Ok",
                            icon: "ui-icon-heart",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }]
                    }).text(i18n.get_string('envioerror'));
                    ref._formulario.removeClass("loading-mask");
                }
            }
        });
        event.preventDefault();
    }
    else ref._formulario.removeClass("loading-mask");

    event.preventDefault();
};