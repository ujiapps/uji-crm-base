$(document).ready(function () {
    var _inicio = new Inicio();
});

$(document).on('click', '#form-crm a.add-block', function (e) {
    e.preventDefault();
    var row = $(this).closest('.row');
    var duplicado = row.clone();
    $('input[type="text"], input[type="number"], input[type="email"], textarea', duplicado).val('');
    duplicado.insertAfter(row).hide().slideDown();
});

$(document).on('click', '#form-crm a.remove-block', function (e) {
    e.preventDefault();
    var fieldset = $(this).closest('fieldset');
    if ($('[data-fieldsetdatoid="' + fieldset.attr('data-fieldsetdatoid') + '"]').length > 1) {
        $(this).closest('.row').slideUp(function () {
            $(this).remove();
        });
    }
});

Inicio = function () {
    this._inicioUsuarioCorreo = $("#formInicioCorreo");

    $(".enviarUsuarioCorreo").click(this.onClickInicioUsuarioCorreo);
    $(".redirectFormulario").click(this.onClickRedirectFormularioBlanco);
    $(".accesoPerfil").click(this.onClickAccesoPerfil);
    $(".submitForm").click(this.submitForm.bind(this));

    // $('#form-crm').validate();
    $('#tipoIdentificacion').change(this.onChangeTipoIdentificacion.bind(this));

    var me = this;
    $('input[type=radio]').on('change', function () {
        me.mostrarCampos($(this).data('mostrar'));
        me.ocultarCampos($(this).data('ocultar'));
    });

    $('select').on('change', function () {
        var dataMostrar = $(this).find(':selected').data('mostrar');
        var dataOcultar = $(this).find(':selected').data('ocultar');
        if (dataMostrar) {
            me.mostrarCampos(dataMostrar);
        }
        if (dataOcultar)
            me.ocultarCampos(dataOcultar);
    });


};

Inicio.prototype.ocultarCampos = function (ocultar) {
    var me = this;
    ocultar.forEach(function (e) {
        $('[data-campanyadatoid=' + e + ']').attr("disabled", true);
        $('[data-campanyadatoid=' + e + ']').closest('.campo-input').hide();
        var dataOcultar = $('[data-campanyadatoid=' + e + ']').find(':selected').data('mostrar');
        if (dataOcultar) {
            me.ocultarCampos(dataOcultar);
        }
        $('[data-campanyadatoid=' + e + ']').val('').trigger("chosen:updated");
    });
};


Inicio.prototype.mostrarCampos = function (mostrar) {
    mostrar.forEach(function (e) {
        $('[data-campanyadatoid=' + e + ']').attr("disabled", false);
        $('[data-campanyadatoid=' + e + ']').closest('.campo-input').show();
        $('select[data-campanyadatoid=' + e + ']').trigger("chosen:updated");
    });
};


Inicio.prototype.onChangeTipoIdentificacion = function () {
    var selec = $('#tipoIdentificacion :selected').text();
    if (selec == 'DNI') {
        $('#identificacion').rules('add', 'nifES');
        $('#identificacion').rules('remove', 'cifES nieES');
    } else if (selec == 'CIF') {
        $('#identificacion').rules('add', 'cifES');
        $('#identificacion').rules('remove', 'nifES nieES');
    } else if (selec == 'NIE') {
        $('#identificacion').rules('add', 'nieES');
        $('#identificacion').rules('remove', 'cifES nifES');
    } else {
        $('#identificacion').rules('remove', 'nifES cifES nieES');
    }
}

Inicio.prototype.getFormularioId = function () {
    return $("#formularioId").text();
};

Inicio.prototype.submitForm = function (event){
    var form = $("#form-crm"),
        formSerializer = FormSerializer.create({}),
        urlForm = '/crm/rest/publicacionprincipal/formulario/' + this.getFormularioId(),
        datosForm = formSerializer.serialize(form);
    $('#spinner').show();
    event.preventDefault();
    $.ajax({

        contentType: 'application/json',
        data: JSON.stringify(datosForm),
        dataType: 'json',
        async: true,
        processData: false,
        url: urlForm,
        type: 'POST',

        success: function (response) {
            alert("Formulario enviado");
            $('#spinner').hide();
            window.location.href = "https://www.uji.es";
        },
        error: function () {
            $("#form-crm").removeClass("loading-mask");
            $('#spinner').hide();
            alert("No es pot enviar el formulari");
        }
    });
};

Inicio.prototype.onClickInicioUsuarioCorreo = function () {

    $('#spinner').show();
    $.ajax({
        url: $(this).data("formulario") + '/hash',
        type: "POST",
        dataType: 'json',
        data: $("#formInicioCorreo").serialize(),
        success: function (result) {
            if (result.success) {
                $('#usuarioClaveMensaje').removeClass("hide");
                $('#usuarioCorreoError').addClass("hide");
            } else {
                $('#usuarioCorreoError').removeClass("hide");
                $('#usuarioClaveMensaje').addClass("hide");
            }
            $('#spinner').hide();
        },
        error: function (xhr, resp, text) {
            $('#spinner').hide();
        }
    });
};

Inicio.prototype.onClickAccesoPerfil = function () {
    let formulario = $(this).data("formulario");
    let hash = $(this).data("hash");

    $('#spinner').show();
    $.ajax({
        type: "PUT",
        dataType: 'json',
        data: {clienteId: $(this).data("clienteid")},
        success: function (result) {
            window.location = formulario + '/hash/' + hash;
            $('#spinner').hide();
        },
        error: function (xhr, resp, text) {
            $('#spinner').hide();
        }
    });

}

Inicio.prototype.onClickRedirectFormularioBlanco = function () {
    window.location = $(this).data("formulario");
}