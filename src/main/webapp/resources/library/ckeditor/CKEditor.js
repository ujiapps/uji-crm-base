/***************************************************************************************************
 * CKEditor Extension
 **************************************************************************************************/
Ext.define('Ext.form.CKEditor', {
    extend : 'Ext.form.field.TextArea',
    alias : 'widget.ckeditor',
    version : "0.0.2",
    firstSetData : false,
    defaultListenerScope : true,

    listeners : {
        instanceReady : 'instanceReady',
        resize : 'resize',
        boxready : 'onBoxReady'
    },

    editorId : null,

    editor : null,
    config : {},

    constructor : function()
    {
        this.callParent(arguments);
    },

    initComponent : function()
    {
        this.callParent(arguments);

    },
    instanceReady : function(ev)
    {
        // Set read only to false to avoid issue when created into or as a child of a disabled component.
        this.setReadOnly(false);
    },

    onRender : function(ct, position)
    {
        var ref = this;

        if (!this.el)
        {
            this.defaultAutoCreate = {
                tag : "textarea",
                autocomplete : "off"
            };
        }

        Ext.apply(this.CKConfig, {
            height : this.height,
            width : this.width,
            baseFloatZIndex: 190001
        });

        this.callParent(arguments);

        this.editor = CKEDITOR.replace(this.inputEl.id, this.config.CKConfig);
        this.editorId = this.inputEl.id;

        this.editor.on('instanceReady', function(evt, editor)
        {
            ref.instanceReady();
            ref.resetDirty();
            ref.updateLayout();
        });

        this.editor.on("resize", function()
        {
            ref.updateLayout();
        })
    },

    prepareFirstSetData : function(firstSetData)
    {
        this.firstSetData = firstSetData;

        if (this.firstSetData)
        {
            this.resetDirty();
        }
    },

    setValue : function(value, firstSetData)
    {
        var ref = this;

        this.callParent(arguments);

        if (!Ext.isEmpty(this.editor))
        {
            if (typeof firstSetData !== 'undefined')
            {
                this.firstSetData = firstSetData;
            }

            if (this.firstSetData)
            {
                if (this.editor.status === 'ready')
                {
                    ref.setValueAndResetDirty(value);
                    return
                }

                this.editor.on('instanceReady', function(evt)
                {
                    ref.setValueAndResetDirty(value);
                });

                return;
            }

            this.editor.setData(value);
        }
    },

    setValueAndResetDirty : function(value)
    {
        var ref = this;

        this.editor.setData(value, {
            callback : function()
            {
                ref.resetDirty();
                ref.firstSetData = false;
            }
        })
    },

    getValue : function()
    {
        if (this.editor)
        {
            return this.editor.getData();
        } else
        {
            return ''
        }
    },

    getRawValue : function()
    {
        if (!Ext.isEmpty(this.editor))
        {
            this.value = this.editor.getData();
            return this.value;
        }
        return this.rawValue;
    },

    destroy : function()
    {
        // delete instance
        if (!Ext.isEmpty(this.editor))
        {
            this.editor.destroy();
            delete CKEDITOR.instances[this.editorId];
        }

        this.callParent(arguments);
    },

    reset : function()
    {
        var ref = this;
        this.callParent(arguments);

        if (CKEDITOR.instances[this.editorId])
        {
            CKEDITOR.instances[this.editorId].setData(' ', {
                callback : function()
                {
                    ref.resetDirty();
                }
            })
        }
    },

    resetDirty : function()
    {
        this.originalValue = this.getValue();
    },

    onBoxReady : function(win, width, height, eOpts)
    {
        // used to hook into the resize method
    },

    setReadOnly : function(option)
    {
        this.CKConfig.readOnly = option;

        if (CKEDITOR.instances[this.editorId])
        {
            CKEDITOR.instances[this.editorId].setReadOnly(option);
        }
    },

    hide : function()
    {
        this.callParent(arguments);

        if (!Ext.isEmpty(this.editor))
        {
            this.editor.container.hide();
        }

    },

    show : function()
    {
        this.callParent(arguments);

        if (!Ext.isEmpty(this.editor))
        {
            this.editor.container.show();
        }
    },

    resize: function (win, width, height) {
        var eid = this.editorId;
        var me = this;
        if (!Ext.isEmpty(this.editor)) {
            if (CKEDITOR.instances[this.editorId].container) {
                CKEDITOR.instances[this.editorId].resize(width, height);
            }
            else {
                //TODO revisar.
                me.setLoading(true);
                new Ext.util.DelayedTask(function () {
                    CKEDITOR.instances[eid].resize(width, height);
                    me.setLoading(false);
                }).delay(1000);
            }
        }
    },

});