Ext.Loader.setConfig(
    {
        enabled: true,
        paths: {
            'Ext.ux': '/crm/examples/ux',
            //'Ext.ux': window.location.protocol+'//static.uji.es/js/extjs/extjs-4.2.0/examples/ux',
            'Ext.ux.uji': '/crm/Ext/ux/uji',
            'Ext.ux.form.field': '/crm/Ext/ux/form/field'
        }
    });

Ext.require('Ext.data.proxy.Rest');
Ext.require('Ext.data.reader.Json');
Ext.require('Ext.data.writer.Json');
Ext.require('Ext.data.TreeStore');
Ext.require('Ext.container.Viewport');
Ext.require('Ext.layout.container.Border');
Ext.require('Ext.tab.Panel');
Ext.require('Ext.tree.Panel');
Ext.require('Ext.ux.TabCloseMenu');
Ext.require('Ext.ux.uji.TabPanel');
Ext.require('Ext.ux.uji.form.LookupComboBox');
Ext.require('Ext.ux.uji.form.LookupWindow');
Ext.require('Ext.ux.uji.form.model.Lookup');
Ext.require('Ext.ux.uji.ApplicationViewport');
Ext.require('Ext.form.field.Trigger');
Ext.require('Ext.form.FieldSet');
Ext.require('Ext.form.field.Text');
Ext.require('Ext.form.field.File');
Ext.require('Ext.form.field.Display');
Ext.require('Ext.form.Label');
Ext.require('Ext.toolbar.Toolbar');
Ext.require('Ext.toolbar.Spacer');
Ext.require('Ext.form.field.Checkbox');
Ext.require('Ext.grid.BooleanColumn');
Ext.require('Ext.grid.column.Date');
Ext.require('Ext.form.field.Radio');
Ext.require('Ext.form.field.Hidden');
Ext.require('Ext.form.RadioGroup');
Ext.require('Ext.layout.container.Table');
Ext.require('Ext.layout.container.Form');
Ext.require('Ext.layout.container.Column');
Ext.require('Ext.grid.Panel');
Ext.require('Ext.grid.plugin.RowEditing');
Ext.require('Ext.Date');
Ext.require('Ext.ux.LiveSearchGridPanel');
Ext.require('Ext.ux.grid.FiltersFeature');
Ext.require('Ext.panel.Table');
Ext.require('Ext.toolbar.Paging');
Ext.require('Ext.ux.form.field.BoxSelect');

Ext.application(
    {
        name: 'CRM',
        appFolder: 'app',
        autoCreateViewport: false,

        views: ['dashboard.PanelDashboard', 'servicios.PanelServicios', 'ApplicationViewport', 'programas.PanelProgramas', 'clientes.PanelClientes', 'campanyas.PanelCampanyas', 'grupos.PanelGrupos',
            'envios.PanelEnvios', 'seguimientos.PanelCampanyaSeguimientos', 'busqueda.FiltrosBusqueda', 'listados.PanelListados', 'estudios.PanelEstudios'],
        controllers: ['ControllerDashboards', 'ControllerPrincipal', 'ControllerPanelServicios', 'ControllerPanelProgramas', 'ControllerPanelClientes', 'ControllerPanelCampanyas',
            'ControllerPanelGrupos', 'ControllerPanelItems', 'ControllerPanelClientesDatos', 'ControllerPanelClientesVinculaciones', 'ControllerPanelSeguimientos', 'ControllerPanelClientesSeguimientos',
            'ControllerPanelEnvios', 'ControllerPanelEnviosSeguimientos', 'ControllerPanelClientesEtiquetas', 'ControllerVentanaClienteCampanya', 'ControllerPanelClientesCampanyas',
            'ControllerPanelEtiquetas', 'ControllerPanelClientesDatosTipos', 'ControllerPanelCampanyaTextos', 'ControllerPanelTiposVinculacion', 'ControllerPanelTiposSeguimientos',
            'ControllerPanelPagos', 'ControllerPanelClientesPagos', 'ControllerPanelListados', 'ControllerPanelCampanyasFormularios', 'ControllerPanelCampanyasCartas', 'ControllerPanelClientesCartas',
            'ControllerPanelClientesEstudios', 'ControllerPanelEstudios', 'ControllerPanelClientesItems'],
        models: ['DatosFormularioCampanya', 'DatosExtraFormulario', 'Cliente'],
        stores: ['StoreTiposFecha', 'StoreAnyos', 'StoreMeses', 'StoreOperadoresLogicos', 'StoreGrupos', 'StoreClasificacionesEstudioNoUJI'
            , 'StoreSeguimientoTipos', 'StoreProvincias', 'StoreItems', 'StoreProgramas', 'StoreCampanyaActos', 'StoreEnviosImagenes', 'StorePoblaciones',
            'StorePaises', 'StoreEnviosAdjuntos', 'StoreClasificacionesEstudio', 'StoreTiposEstadoCampanyaMotivos', 'StoreTiposDatosSelect', 'StoreTiposEstadoCampanya'],
        launch: function () {
            var viewport = Ext.create('CRM.view.ApplicationViewport',
                {
                    codigoAplicacion: 'CRM',
                    tituloAplicacion: 'CRM',
                    dashboard: true
                });
            viewport.addNewTab('CRM.view.dashboard.PanelDashboard');
        }

    });

Ext.onReady(function () {
    Ext.override(Ext.grid.plugin.RowEditing,
        {
            cancelEdit: function () {
                if (this.editing) {
                    this.getEditor().cancelEdit();
                    this.callParent(arguments);

                    if (this.context.record.phantom && this.context.record.store) {
                        this.context.record.store.remove(this.context.record);
                    }

                    this.fireEvent('canceledit', this.context);
                }
            }
        });
});
