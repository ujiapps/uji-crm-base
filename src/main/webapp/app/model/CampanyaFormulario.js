Ext.define('CRM.model.CampanyaFormulario',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'campanyaId',
                type: 'int'
            },
            {
                name: 'titulo',
                type: 'string'
            },
            {
                name: 'plantilla',
                type: 'string'
            },
            {
                name: 'cabecera',
                type: 'string'
            },
            {
                name: 'accionDestino',
                type: 'string'
            },
            {
                name: 'solicitarIdentificacion',
                type: 'int'
            },
            {
                name: 'solicitarNombre',
                type: 'int'
            },
            {
                name: 'solicitarCorreo',
                type: 'int'
            },
            {
                name: 'solicitarMovil',
                type: 'int'
            },
            {
                name: 'solicitarPostal',
                type: 'int'
            },
            {
                name: 'autenticaFormulario',
                type: 'boolean'
            },
            {
                name: 'fechaInicio',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s',
                useNull: true
            },
            {
                name: 'fechaFin',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s',
                useNull: true
            },
            {
                name: 'actoId',
                type: 'int',
                useNull: true
            },
            {
                name: 'estadoDestinoId',
                type: 'int'
                //},
                //{
                //    name: 'estadoOrigenId',
                //    type: 'int',
                //    useNull: true
            }]

    });