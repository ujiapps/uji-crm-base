Ext.define('CRM.model.CampanyaItem',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'campanyaId',
        type : 'int'
    },
    {
        name : 'itemId',
        type : 'int'
    },
    {
        name : 'itemNombre',
        type : 'string'
    },
    {
        name : 'grupoNombre',
        type : 'string'
    },
    {
        name : 'programaNombre',
        type : 'string'
    } ]
});