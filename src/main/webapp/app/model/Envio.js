Ext.define('CRM.model.Envio',
    {
        extend: 'Ext.data.Model',
        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true

            },
            {
                name: 'nombre',
                type: 'string'
            },
            {
                name: 'cuerpo',
                type: 'string'
            },
            {
                name: 'asunto',
                type: 'string'
            },
            {
                name: 'responder',
                type: 'string'
            },
            {
                name: 'desde',
                type: 'string'
            },
            {
                name: 'personaId',
                type: 'int',
                useNull: true
            },
            {
                name: 'seguimientoId',
                type: 'int',
                useNull: true
            },
            {
                name: 'envioTipoId',
                type: 'int'
            },
            {
                name: 'envioTipoNombre',
                type: 'string'
            },
            {
                name: 'fechaEnvioDestinatario',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'fechaFinVigencia',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'fechaEnvioPlataforma',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'horaEnvioDestinatario',
                type: 'string'
            },
            {
                name: 'horaFinVigencia',
                type: 'string'
            },
            {
                name: 'fecha',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'enviadoa',
                type: 'string'
            },
            {
                name: 'tipoCorreoEnvio',
                type: 'int',
                useNull: true
            }, {
                name: 'programaId',
                type: 'int',
                useNull: true
            // }, {
            //     name: 'envioPieId',
            //     type: 'int',
            //     useNull: true
            // },{
            //     name: 'envioCabeceraId',
            //     type: 'int',
            //     useNull: true
            }]
    });