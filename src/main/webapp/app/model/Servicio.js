Ext.define('CRM.model.Servicio',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'nombre',
        type : 'string'
    } ],

    validations : [
    {
        type : 'length',
        field : 'nombre',
        min : 1
    } ]

});