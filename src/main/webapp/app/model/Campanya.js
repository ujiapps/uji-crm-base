Ext.define('CRM.model.Campanya', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'nombre',
        type: 'string'
    }, {
        name: 'descripcion',
        type: 'string'
    }, {
        name: 'programaId',
        type: 'int'
    }, {
        name: 'programaNombre',
        type: 'string'
    }, {
        name: 'campanyaId',
        type: 'int',
        useNull: true
    }, {
        name: 'campanyaNombre',
        type: 'string'
    }, {
        name: 'subVinculo',
        type: 'int'
    }, {
        name: 'subVinculoNombre',
        type: 'string'
    }, {
        name: 'campanyaTextoId',
        type: 'int',
        useNull: true
    }, {
        name: 'fideliza',
        type: 'boolean'
    }, {
        name: 'lineaFacturacionId',
        type: 'int',
        useNull: true
    }, {
        name: 'correoAvisaCobro',
        type: 'string'
    }, {
        name: 'fechaInicioActiva',
        type: 'date',
        dateFormat: 'd/m/Y H:i:s'
    }, {
        name: 'fechaFinActiva',
        type: 'date',
        dateFormat: 'd/m/Y H:i:s'
    }],

    validations: [{
        type: 'length',
        field: 'nombre',
        min: 1
    }]
});