Ext.define('CRM.model.ClienteDatoExtra',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'tipoDatoNombre',
                type: 'string'
            },
            {
                name: 'tipoDatoId',
                type: 'int'
            },
            {
                name: 'tipoDatoTipoId',
                type: 'int'
            },
            {
                name: 'tipoDatoTipoNombre',
                type: 'string'
            },
            {
                name: 'valor',
                type: 'string'
            },
            {
                name: 'valorSelect',
                type: 'int'
            },
            {
                name: 'observaciones',
                type: 'string'
            },
            {
                name: 'tipoAccesoId',
                type: 'int',
                useNull: true
            },
            {
                name: 'tipoAccesoNombre',
                type: 'string'
            },
            {
                name: 'clienteId',
                type: 'int'
            },
            {
                name: 'itemId',
                type: 'int',
                useNull: true
            },
            {
                name: 'clienteDatoId',
                type: 'int',
                useNull: true
            },
            {
                name: 'campanyaId',
                type: 'int',
                useNull: true
            },

            'datosOpciones',

            {
                name: 'valorFecha',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            }],

        validations: [
            {
                type: 'presence',
                field: 'tipoDatoId'
            },
            {
                type: 'presence',
                field: 'clienteId'
            }]
    });