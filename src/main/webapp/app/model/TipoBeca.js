Ext.define('CRM.model.TipoBeca',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int'
    },
    {
        name : 'nombre',
        type : 'string'
    } ]
});