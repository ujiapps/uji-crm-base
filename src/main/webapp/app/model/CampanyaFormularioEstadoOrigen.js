Ext.define('CRM.model.CampanyaFormularioEstadoOrigen',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'campanyaFormularioId',
                type: 'int'
            },
            {
                name: 'estadoOrigenId',
                type: 'int',
                useNull: true
            }]

    });