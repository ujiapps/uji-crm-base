Ext.define('CRM.model.TipoTarifaEnvio',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'tipoTarifaId',
                type: 'int',
                useNull: true
            },
            {
                name: 'asunto',
                type: 'string'
            },
            {
                name: 'cuerpo',
                type: 'string',
                useNull: true
            },
            {
                name: 'desde',
                type: 'string'
            },
            {
                name: 'responderA',
                type: 'string'
            },
            {
                name: 'tipoCorreoEnvio',
                type: 'int',
                useNull: true
            }]
    });