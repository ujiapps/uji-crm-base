Ext.define('CRM.model.TipoValidacion',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'nombre',
                type: 'string'
            }]
    });
