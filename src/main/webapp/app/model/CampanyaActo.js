Ext.define('CRM.model.CampanyaActo',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'campanyaId',
                type: 'int'
            },
            {
                name: 'rssActivo',
                type: 'boolean'
            },
            {
                name: 'imagenNombre',
                type: 'string'
            },
            {
                name: 'titulo',
                type: 'string'
            },
            {
                name: 'resumen',
                type: 'string'
            },
            {
                name: 'duracion',
                type: 'int'
            },
            {
                name: 'contenido',
                type: 'string'
            },
            {
                name: 'fecha',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'hora',
                type: 'string'
            },
            {
                name: 'horaApertura',
                type: 'string'
            }]

    });
