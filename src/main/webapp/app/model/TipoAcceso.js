Ext.define('CRM.model.TipoAcceso',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int'
    },
    {
        name : 'nombre',
        type : 'string'
    } ]
});
