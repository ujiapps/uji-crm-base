Ext.define('CRM.model.CampanyaEnvio',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'campanyaId',
                type: 'int'
            },
            {
                name: 'asunto',
                type: 'string'
            },
            {
                name: 'cuerpo',
                type: 'string'
            },
            {
                name: 'desde',
                type: 'string'
            },
            {
                name: 'responder',
                type: 'string'
            },
            {
                name: 'base',
                type: 'string'
            },
            {
                name: 'campanyaEnvioTipoId',
                type: 'int'
            },
            {
                name: 'campanyaEnvioTipoNombre',
                type: 'string'
            },
            {
                name: 'tipoCorreoEnvio',
                type: 'int',
                useNull: true
            }]
    });