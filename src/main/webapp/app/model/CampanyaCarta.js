Ext.define('CRM.model.CampanyaCarta',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'campanyaId',
        type : 'int'
    },
    {
        name : 'cuerpo',
        type : 'string'
    },
    {
        name : 'campanyaCartaTipoId',
        type : 'int'
    //},
    //{
    //    name : 'campanyaCartaTipoNombre',
    //    type : 'string'
    } ]
});