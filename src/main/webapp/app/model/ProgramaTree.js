Ext.define('CRM.model.ProgramaTree',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int'
    },
    {
        name : 'text',
        type : 'string'
    },
    {
        name : 'leaf',
        type : 'boolean'
    } ]

});