Ext.define('CRM.model.Fase',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'nombre',
        type : 'string'
    },
    {
        name : 'activa',
        type : 'int'
    } ],
    validations : [
    {
        type : 'length',
        field : 'nombre',
        min : 1
    } ]
});