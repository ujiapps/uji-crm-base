Ext.define('CRM.model.EnvioNoValido',
    {
        extend: 'Ext.data.Model',

        fields: [

            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'fechaPrimerIntento',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            }, {
                name: 'fechaEnvio',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'estado',
                type: 'string'
            },
            {
                name: 'numeroIntentos',
                type: 'int'
            },
            {
                name: 'nombre',
                type: 'string'
            },
            {
                name: 'modificado',
                type: 'boolean'
            }]
    });