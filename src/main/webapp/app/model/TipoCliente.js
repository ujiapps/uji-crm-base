Ext.define('CRM.model.TipoCliente',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'string'
    },
    {
        name : 'nombre',
        type : 'string'
    } ]
});