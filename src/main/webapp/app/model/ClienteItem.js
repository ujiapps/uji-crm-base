Ext.define('CRM.model.ClienteItem', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'nombre',
        type: 'string'
    }, {
        name: 'tipo',
        type: 'string'
    }, {
        name: 'clienteId',
        type: 'int',
        useNull: true
    }, {
        name: 'itemId',
        type: 'int',
        useNull: true
    }, {
        name: 'grupoId',
        type: 'int',
        useNull: true
    }, {
        name: 'grupoNombre',
        type: 'string'
    }, {
        name: 'correo',
        type: 'boolean'
    }, {
        name: 'postal',
        type: 'boolean'
    }, {
        name: 'clienteItemRelacionado',
        type: 'int',
        useNull: true
    }, {
        name: 'clienteDato',
        type: 'int',
        useNull: true
    }, {
        name: 'campanyaId',
        type: 'int',
        useNull: true
    }]
});
