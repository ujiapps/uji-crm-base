Ext.define('CRM.model.EnvioItem',
{
    extend : 'Ext.data.Model',
    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'envioId',
        type : 'int'
    },
    {
        name : 'itemId',
        type : 'int'
    } ]
});