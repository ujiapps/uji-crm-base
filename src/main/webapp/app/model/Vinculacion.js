Ext.define('CRM.model.Vinculacion',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'cliente1Id',
        type : 'int'
    },
    {
        name : 'cliente2Id',
        type : 'int'
    },
    {
        name : 'tipoId',
        type : 'int'
    },
    {
        name : 'cliente1Nombre',
        type : 'string'
    },
    {
        name : 'cliente1Etiqueta',
        type : 'string'
    },
    {
        name : 'cliente2Nombre',
        type : 'string'
    },
    {
        name : 'cliente2Etiqueta',
        type : 'string'
    },
    {
        name : 'tipoNombre',
        type : 'string'
    } ]
});