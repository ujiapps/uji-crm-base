Ext.define('CRM.model.LineaFacturacion',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'nombre',
                type: 'string'
            },
            {
                name: 'descripcion',
                type: 'string'
            },
            {
                name: 'servicioId',
                type: 'int'
            },
            {
                name: 'emisoraId',
                type: 'int',
                useNull: true
            },
            {
                name: 'reciboFormatoId',
                type: 'int',
                useNull: true
            }
        ],

        validations: [
            {
                type: 'length',
                field: 'nombre',
                min: 1
            }]

    });