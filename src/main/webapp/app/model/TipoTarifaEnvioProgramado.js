Ext.define('CRM.model.TipoTarifaEnvioProgramado',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'tarifaTipoEnvioId',
                type: 'int',
                useNull: true
            },
            {
                name: 'tipoFecha',
                type: 'int'
            },
            {
                name: 'dias',
                type: 'int'
            },
            {
                name: 'cuandoRealizarEnvio',
                type: 'int'
            }]

    });