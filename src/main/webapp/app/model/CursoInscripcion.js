Ext.define('CRM.model.CursoInscripcion',
{
    extend : 'Ext.data.Model',

    fields : [

    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'nombre',
        type : 'string'
    },
    {
        name : 'tipoNombre',
        type : 'string'
    },
    {
        name : 'importe',
        type : 'float'
    },
    {
        name : 'confirmacion',
        type : 'boolean'
    },
    {
        name : 'aprovechamiento',
        type : 'boolean'
    },
    {
        name : 'anulaInscripcion',
        type : 'boolean'
    },
    {
        name : 'calificacion',
        type : 'int'
    },
    {
        name : 'espera',
        type : 'boolean'
    },
    {
        name : 'asistencia',
        type : 'boolean'
    },
    {
        name : 'fechaInscripcion',
        type : 'date'
    } ]
});