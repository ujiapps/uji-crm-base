Ext.define('CRM.model.CampanyaAdminTree', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'text',
        type: 'string'
    }, {
        name: 'leaf',
        type: 'boolean'
    }, {
        name: 'parentId',
        type: 'int'
    },{
        name: 'programa',
        type: 'string'
    },{
        name: 'programaId',
        type: 'int'
    },{
        name: 'descripcion',
        type: 'string'
    }]
});