Ext.define('CRM.model.CampanyaFase',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'campanyaId', 'faseId', 'faseNombre',
    {
        name : 'fechaIni',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'fechaFin',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    } ],
    validations : [
    {
        type : 'length',
        field : 'faseId',
        min : 1
    } ]
});