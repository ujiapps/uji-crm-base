Ext.define('CRM.model.EnvioClienteVista', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'envioId',
        type: 'int'
    }, {
        name: 'nombre',
        type: 'string'
    }, {
        name: 'identificacion',
        type: 'string'
    }, {
        name: 'correo',
        type: 'string'
    }]
});