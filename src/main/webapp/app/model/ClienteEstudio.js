Ext.define('CRM.model.ClienteEstudio', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'personaId',
            type: 'int'
        },
        {
            name: 'estudio',
            type: 'string'
        },
        {
            name: 'cursoAcaIni',
            type: 'int',
            useNull: true
        },
        {
            name: 'cursoAcaFin',
            type: 'int',
            useNull: true
        },
        {
            name: 'fechaTitulo',
            type: 'date',
            dateFormat: 'd/m/Y H:i:s',
            useNull: true
        }]
});