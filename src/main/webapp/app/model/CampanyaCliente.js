Ext.define('CRM.model.CampanyaCliente', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'campanyaId',
        type: 'int'
    }, {
        name: 'campanyaNombre',
        type: 'string'
    }, {
        name: 'identificacion',
        type: 'string'
    }, {
        name: 'clienteId',
        type: 'int'
    }, {
        name: 'clienteNombre',
        type: 'string'
    }, {
        name: 'estado',
        type: 'string'
    }, {
        name: 'campanyaClienteTipoId',
        type: 'int'
    }, {
        name: 'campanyaClienteTipoNombre',
        type: 'string'
    }, {
        name: 'campanyaClienteMotivoNombre',
        type: 'string'
    }, {
        name: 'campanyaClienteMotivoId',
        type: 'int'
    }, {
        name: 'numSeguimientos',
        type: 'int'
    }, {
        name: 'definitivo',
        type: 'boolean'
    }, {
        name: 'clienteGeneralId',
        type: 'int'
    },{
        name: 'correo',
        type: 'string'
    }]

});