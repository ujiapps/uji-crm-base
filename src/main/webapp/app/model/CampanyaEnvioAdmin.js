Ext.define('CRM.model.CampanyaEnvioAdmin',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'campanyaId',
        type : 'int'
    },
    {
        name : 'asunto',
        type : 'string'
    },
    {
        name : 'cuerpo',
        type : 'string'
    },
    {
        name : 'campanyaEnvioTipoId',
        type : 'int'
    },
    {
        name : 'campanyaEnvioTipoNombre',
        type : 'string'
    },
    {
        name : 'emails',
        type : 'string'
    } ]
});