Ext.define('CRM.model.DatosFormularioCampanya',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'clienteGeneral',
                foreign: true,
                useNull: true
            },
            {
                name: 'cliente',
                foreign: true,
                useNull: true
            },
            {
                name: 'estadoCampanya',
                foreign: true,
                useNull: true
            },
            {
                name: 'datosExtraFormulario',
                foreign: true,
                useNull: true
            }]
    });