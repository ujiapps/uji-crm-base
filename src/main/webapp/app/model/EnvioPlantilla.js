Ext.define('CRM.model.EnvioPlantilla', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'nombre',
        type: 'string'
    }, {
        name: 'plantilla',
        type: 'string'
    }]
});