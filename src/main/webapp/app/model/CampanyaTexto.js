Ext.define('CRM.model.CampanyaTexto', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'nombre',
        type: 'string'
    }, {
        name: 'textoCa',
        type: 'string'
    }, {
        name: 'textoEs',
        type: 'string'
    }, {
        name: 'textoUk',
        type: 'string'
    }, {
        name: 'codigo',
        type: 'string'
    }]

});