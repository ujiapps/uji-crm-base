Ext.define('CRM.model.Programa', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'tipoAccesoId',
        type: 'int'
    }, {
        name: 'tipoAccesoNombre',
        type: 'string'
    }, {
        name: 'servicioId',
        type: 'int'
    }, {
        name: 'servicioNombre',
        type: 'string'
    }, {
        name: 'nombreCa',
        type: 'string'
    }, {
        name: 'nombre',
        type: 'string'
    }, {
        name: 'nombreEs',
        type: 'string'
    }, {
        name: 'nombreUk',
        type: 'string'
    }, {
        name: 'plantilla',
        type: 'string'
    }, {
        name: 'orden',
        type: 'int'
    }, {
        name: 'visible',
        type: 'boolean'
    }],

    validations: [{
        type: 'length',
        field: 'nombreCa',
        min: 1
    }]

});