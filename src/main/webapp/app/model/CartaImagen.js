Ext.define('CRM.model.CartaImagen',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'cartaId',
                type: 'int'
            },
            {
                name: 'nombre',
                type: 'string'
            },
            {
                name: 'referencia',
                type: 'string'
            }]
    });