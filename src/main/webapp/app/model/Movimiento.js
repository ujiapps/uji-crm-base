Ext.define('CRM.model.Movimiento',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int'
    },
    {
        name : 'fecha',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'descripcion',
        type : 'string'
    },
    {
        name : 'clienteId',
        type : 'int'
    },
    {
        name : 'tipoId',
        type : 'int'
    },
    {
        name : 'tipoNombre',
        type : 'string'
    },
    {
        name : 'campanyaId',
        type : 'int'
    },
    {
        name : 'campanyaNombre',
        type : 'string'
    } ]

});
