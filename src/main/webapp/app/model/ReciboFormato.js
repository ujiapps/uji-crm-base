Ext.define('CRM.model.ReciboFormato',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'formato',
        type : 'string'
    } ]
});