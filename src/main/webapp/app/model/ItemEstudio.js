Ext.define('CRM.model.ItemEstudio', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'id',
            type: 'int',
            useNull: true
        },
        {
            name: 'itemId',
            type: 'int',
            useNull: true
        },
        {
            name: 'grupoId',
            type: 'int',
            useNull: true
        }, {
            name: 'clasificacionId',
            type: 'int',
            useNull: true
        }, {
            name: 'itemNombre',
            type: 'string',
            useNull: true
        }]

});
