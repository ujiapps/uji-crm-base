Ext.define('CRM.model.VinculoUji',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'vinculoNombre',
                type: 'string'
            },
            {
                name: 'subVinculoNombre',
                type: 'string'
            },
            {
                name: 'fecha_alta',
                dateFormat: 'd/m/Y H:i:s',
                type: 'date'
            },
            {
                name: 'fecha_baja',
                dateFormat: 'd/m/Y H:i:s',
                type: 'date'
            }]
    });