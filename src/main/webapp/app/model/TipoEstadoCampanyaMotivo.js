Ext.define('CRM.model.TipoEstadoCampanyaMotivo',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'tipoEstadoCampanyaId',
                type: 'int'
            },
            {
                name: 'nombreEs',
                type: 'string'

            },
            {
                name: 'nombreCa',
                type: 'string'
            },
            {
                name: 'nombreUk',
                type: 'string'
            }]
    });