Ext.define('CRM.model.ClasificacionEstudio', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'nombre',
        type: 'string'
    }, {
        name: 'tipo',
        type: 'string'
    }, {
        name: 'tieneItems',
        type: 'int'
    }]
});