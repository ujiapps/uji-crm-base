Ext.define('CRM.model.ClienteEstadoCampanyaMotivo',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'campanyaEstadoMotivoId',
                type: 'int'
            },
            {
                name: 'campanyaClienteId',
                type: 'int',
                useNull: true
            },
            {
                name: 'campanyaId',
                type: 'int'
            },
            {
                name: 'clienteId',
                type: 'int'
            },
            {
                name: 'comentarios',
                type: 'string'
            }]
    });