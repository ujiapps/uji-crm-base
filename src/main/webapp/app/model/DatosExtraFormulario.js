Ext.define('CRM.model.DatosExtraFormulario',
    {
        extend: 'Ext.data.Model',

        fields: [

            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'campanyaDatoId',
                type: 'int'
            },
            {
                name: 'clienteDatoId',
                type: 'int',
                useNull: true
            },
            {
                name: 'valor',
                type: 'string'
            },
            {
                name: 'padre',
                type: 'int',
                useNull: true
            }]
    });