Ext.define('CRM.model.enums.TiposSolicitudCampo',
{
    statics :
    {
        NO_SOLICITAR : 0,
        OPCIONAL : 1,
        OBLIGATORIO : 2
    }
});