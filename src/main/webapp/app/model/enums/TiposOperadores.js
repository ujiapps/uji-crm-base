Ext.define('CRM.model.enums.TiposOperadores',
    {

        statics: {
            Y: 1,
            O: 2,
            YNO: 3,
            ONO: 4
        }
    });