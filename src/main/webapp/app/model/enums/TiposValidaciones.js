Ext.define('CRM.model.enums.TiposValidaciones',
    {
        statics: {
            IBAN: 1,
            IDENTIFICACION: 2,
            TELEFONO: 3
        }
    });