Ext.define('CRM.model.enums.TiposDato',
    {
        statics: {
            NUMERICO: {
                id: 6,
                nombre: 'N'
            },
            TEXTO: {
                id: 8,
                nombre: 'T'
            },
            TEXTO_LARGO: {
                id: 31,
                nombre: 'TL'
            },
            FECHA: {
                id: 7,
                nombre: 'F'
            },
            SELECCION: {
                id: 20,
                nombre: 'S'
            },
            BINARIO: {
                id: 10,
                nombre: 'B'
            },
            TELEFONO: {
                id: 12,
                nombre: 'NT'
            },
            EMAIL: {
                id: 11,
                nombre: 'TM'
            },
            BOOLEAN: {
                id: 43,
                nombre: 'NBOOL'
            },
            CABECERA: {
                id: 42,
                nombre: "TCA"
            },

            ENUNCIADO: {
                id: 73,
                nombre: "TEN"
            },
            ITEMS_SELECCION_MULTIPLE: {
                id: 44,
                nombre: "ISM"
            },
            ITEMS_SELECCION_UNICA_RADIO: {
                id: 45,
                nombre: "ISUC"
            },
            ITEMS_SELECCION_UNICA: {
                id: 46,
                nombre: "ISUD"
            },
            ITEMS_SELECCION_MULTIPLE_CHECKS: {
                id: 47,
                nombre: "ISMC"
            },
            TEXTO_LOPD: {
                id: 83,
                nombre: "Texto LOPD"
            }
        }
    });