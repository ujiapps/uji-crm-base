Ext.define('CRM.model.enums.TiposCorreo',
    {
        statics: {
            PERSONAL: {
                id: 1,
                nombre: 'Correu Personal'
            },
            OFICIAL: {
                id: 2,
                nombre: 'Correu Oficial'
            }
        }
    });