Ext.define('CRM.model.enums.TiposCliente',
{
    statics :
    {
        FISICA :
        {
            id : 'F',
            nombre : 'Persona física'
        },
        JURIDICA :
        {
            id : 'J',
            nombre : 'Empresa'
        }
    }
});