Ext.define('CRM.model.enums.TiposAccesoProgramaPerfil',
{
    statics :
    {
        LECTURA : 'LECTURA',
        ESCRITURA : 'ESCRITURA'
    }
});