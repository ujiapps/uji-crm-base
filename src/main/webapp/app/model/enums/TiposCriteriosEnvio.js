Ext.define('CRM.model.enums.TiposCriteriosEnvio', {
    statics: {
        CAMPANYAS: 1,
        GRUPOS: 2,
        ETIQUETAS: 3,
        ELIMINAR: 4,
        DATOEXTRA: 5,
        MOVIMIENTO: 6,
        CLIENTE: 7,
        DATOSGENERALES: 8,
        CONSULTASQL: 9,
        RESIDENCIA: 10,
        FECHANACIMIENTO: 11,
        PAIS: 12,
        PROVINCIA: 13,
        POBLACION: 14,
        CODIGOPOSTAL: 15
    }
});