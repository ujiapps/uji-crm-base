Ext.define('CRM.model.enums.TiposProgramaUsuario',
{
    statics :
    {
        ADMIN : 'ADMIN',
        USUARIO : 'USER',
        ACTOS: 'ACTOS'
    }
});