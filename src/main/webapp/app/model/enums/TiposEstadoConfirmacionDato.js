Ext.define('CRM.model.enums.TiposEstadoConfirmacionDato',
{
    statics :
    {
        PENDIENTE : 1,
        VALIDO : 2,
        NO_VALIDO : 3
    }
});