Ext.define('CRM.model.enums.TiposAccesoDatoUsuario',
{
    statics :
    {
        LECTURA : 5,
        ESCRITURA : 4,
        INTERNO : 6
    }
});