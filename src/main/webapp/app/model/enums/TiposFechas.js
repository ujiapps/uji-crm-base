Ext.define('CRM.model.enums.TiposFechas',
{
    statics :
    {
        MENSUAL : 1,
        ANUAL : 2,
        COMPLETA : 3
    }
});