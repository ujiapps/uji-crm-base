Ext.define('CRM.model.enums.TiposComparaciones',
    {

        statics: {
            MAYORQUE: 58,
            MENORQUE: 59,
            IGUAL: 60,
            MAYOROIGUAL: 61,
            MENOROIGUAL: 62,
            ENTRE: 64
        }
    });