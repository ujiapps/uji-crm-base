Ext.define('CRM.model.EnvioImagen',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'envioId',
                type: 'int'
            },
            {
                name: 'nombre',
                type: 'string'
            },
            {
                name: 'referencia',
                type: 'string'
            }]
    });