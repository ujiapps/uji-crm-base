Ext.define('CRM.model.Persona',
    {
        extend: 'Ext.data.Model',

        fields: [{
            name: 'id',
            type: 'int',
            useNull: true
        }, {
            name: 'nombre',
            type: 'string'
        }, {
            name: 'apellidosNombre',
            type: 'string'
        },{
            name: 'apellidos',
            type: 'string'
        }, {
            name: 'identificacion',
            type: 'string'
        }, {
            name: 'correo',
            type: 'string'
        }, {
            name: 'correoOficial',
            type: 'string'
        }, {
            name: 'movil',
            type: 'string'
        }, {
            name: 'postal',
            type: 'string'
        }, {
            name: 'paisPostalId',
            type: 'string',
            useNull: true
        }, {
            name: 'provinciaPostalId',
            type: 'int',
            useNull: true
        }, {
            name: 'poblacionPostalId',
            type: 'int',
            useNull: true
        }, {
            name: 'codigoPostal',
            type: 'string',
            useNull: true
        }, {
            name: 'sexoId',
            type: 'int'
        }, {
            name: 'nacionalidadId',
            type: 'string'
        }, {
            name: 'fechaNacimiento',
            type: 'date',
            dateFormat: 'd/m/Y H:i:s'
        }]
    });