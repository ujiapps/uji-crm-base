Ext.define('CRM.model.Emisora',
    {
        extend: 'Ext.data.Model',

        fields: [

            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'nombre',
                type: 'string'
            }]
    });