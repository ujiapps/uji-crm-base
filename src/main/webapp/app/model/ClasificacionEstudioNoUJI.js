Ext.define('CRM.model.ClasificacionEstudioNoUJI', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'nombreCa',
        type: 'string'
    }, {
        name: 'nombreEs',
        type: 'string'
    },{
        name: 'nombreUk',
        type: 'string'
    },{
        type: 'int',
        name: 'tipo',
        useNull: true
    },{
        type: 'int',
        name: 'tieneItems'
    }]
});