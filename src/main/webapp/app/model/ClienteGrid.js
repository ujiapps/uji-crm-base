Ext.define('CRM.model.ClienteGrid', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'clienteId',
        type: 'int'
    }, {
        name: 'personaId',
        type: 'int',
        useNull: true
    }, {
        name: 'nombreCompleto',
        type: 'string'
    }, {
        name: 'identificacion',
        type: 'string'
    }, {
        name: 'tipoIdentificacion',
        type: 'int'
    }, {
        name: 'correo',
        type: 'string'
    }, {
        name: 'movil',
        type: 'string',
        useNull: true

    }, {
        name: 'personaUjiNombre',
        type: 'string',
        useNull: true

    }, {
        name: 'definitivo',
        type: 'boolean'
    }]

});