Ext.define('CRM.model.ClienteDato', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'tipoDato',
        type: 'string'
    }, {
        name: 'nombreDatoCa',
        type: 'string'
    }, {
        name: 'nombreDatoEs',
        type: 'string'
    }, {
        name: 'nombreDatoUk',
        type: 'string'
    }, {
        name: 'valorCa',
        type: 'string'
    }, {
        name: 'valorEs',
        type: 'string'
    }, {
        name: 'valorUk',
        type: 'string'
    }, {
        name: 'clienteId',
        type: 'int'
    }, {
        name: 'observaciones',
        type: 'string'
    }, {
        name: 'fechaUltimaModificacion',
        type: 'date',
        dateFormat: 'd/m/Y H:i:s'
    }]
});