Ext.define('CRM.model.EnvioCliente',
    {
        extend: 'Ext.data.Model',
        idProperty: 'id',
        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'envioId',
                type: 'int'
            },
            {
                name: 'clienteId',
                type: 'int'
            },
            {
                name: 'clienteNombre',
                type: 'string'
            },
            {
                name: 'clienteEtiqueta',
                type: 'string'
            },
            {
                name: 'para',
                type: 'string'
            },
            {
                name: 'responder',
                type: 'string'
            },
            {
                name: 'desde',
                type: 'string'
            },
            {
                name: 'totalCount',
                type: 'int'
            },
            {
                name: 'fechaEnvio',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'nombre',
                type: 'string'
            },
            {
                name: 'asunto',
                type: 'string'
            },
            {
                name: 'cuerpo',
                type: 'string'
            },
            {
                name: 'envioTipoNombre',
                type: 'string'
            },
            {
                name: 'estado',
                type: 'string'
            }]
    });