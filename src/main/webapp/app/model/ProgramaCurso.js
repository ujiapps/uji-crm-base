Ext.define('CRM.model.ProgramaCurso',
{
    extend : 'Ext.data.Model',
    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'uestId',
        type : 'int'
    },
    {
        name : 'uestNombre',
        type : 'string'
    },
    {
        name : 'programaId',
        type : 'int'
    } ]
});