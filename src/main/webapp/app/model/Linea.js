Ext.define('CRM.model.Linea',
    {
        extend: 'Ext.data.Model',

        fields: [

            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'importeNeto',
                type: 'int'
            },
            {
                name: 'importeBruto',
                type: 'int'
            },
            {
                name: 'observaciones',
                type: 'string'
            },
            {
                name: 'concepto',
                type: 'string'
            },
            {
                name: 'origen',
                type: 'string'
            },
            {
                name: 'referencia1',
                type: 'int'
            },
            {
                name: 'referencia2',
                type: 'int'
            },
            {
                name: 'lineaFinanciacion',
                type: 'string'
            },
            {
                name: 'descuento',
                type: 'string'
            },
            {
                name: 'recibo',
                type: 'int'
            }]
    });