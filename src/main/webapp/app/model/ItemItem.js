Ext.define('CRM.model.ItemItem',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'itemIdDestino',
        type : 'int'
    },
    {
        name : 'itemIdOrigen',
        type : 'int'
    },
    {
        name : 'itemOrigen',
        type : 'string'
    },
    {
        name : 'itemDestino',
        type : 'string'
    } ]

});
