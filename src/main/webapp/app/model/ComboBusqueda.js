Ext.define('CRM.model.ComboBusqueda',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'etiqueta',
        type : 'string'
    } ]
});