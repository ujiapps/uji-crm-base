Ext.define('CRM.model.ClienteGeneral',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'identificacion',
                type: 'string'
            },
            {
                name: 'tipoIdentificacion',
                type: 'int',
                useNull: true
            },
            {
                name: 'personaId',
                type: 'int',
                useNull: true
            }]

    });