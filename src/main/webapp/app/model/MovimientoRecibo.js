Ext.define('CRM.model.MovimientoRecibo',
    {
        extend: 'Ext.data.Model',

        fields: [

            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'fecha',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'descripcion',
                type: 'string'
            },
            {
                name: 'tipoMovimiento',
                type: 'string'
            },
            {
                name: 'recibo',
                type: 'int'
            },
            {
                name: 'fechaExportacion',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'remesaExportacion',
                type: 'int',
                useNull: true
            },
            {
                name: 'exportadoId',
                type: 'int',
                useNull: true
            }]
    });