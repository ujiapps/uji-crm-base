Ext.define('CRM.model.ProgramaUsuario',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'personaPasPdiId',
        type : 'id'
    },
    {
        name : 'programaId',
        type : 'int'
    },
    {
        name : 'tipo',
        type : 'string'
    },
    {
        name : 'nombrePersona',
        type : 'string'
    } ],
    validations : [
    {
        type : 'length',
        field : 'tipo',
        min : 1
    } ]
});