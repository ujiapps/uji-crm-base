Ext.define('CRM.model.ClienteEtiqueta',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'etiquetaId',
        type : 'int'
    },
    {
        name : 'clienteId',
        type : 'int'
    },
    {
        name : 'etiquetaNombre',
        type : 'string'
    } ]
});