Ext.define('CRM.model.TipoIdioma',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'string'
    },
    {
        name : 'nombre',
        type : 'string'
    } ]
});