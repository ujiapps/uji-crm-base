Ext.define('CRM.model.EstudioUJI', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'nombre',
        type: 'string'
    }, {
        name: 'tipoEstudio',
        type: 'string'
    }]
});