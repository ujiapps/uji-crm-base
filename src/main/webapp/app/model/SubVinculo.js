Ext.define('CRM.model.SubVinculo',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'vinculoId',
                type: 'int'
            },
            {
                name: 'subVinculoId',
                type: 'int'
            },
            {
                name: 'subVinculoNombre',
                type: 'string'
            }]
    });