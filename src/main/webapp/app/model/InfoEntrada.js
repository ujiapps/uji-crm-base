Ext.define('CRM.model.InfoEntrada',
    {
        extend: 'Ext.data.Model',

        fields: [

            {
                name: 'email',
                type: 'string'
            },
            {
                name: 'compras',
                type: 'int'
            },
            {
                name: 'presentadas',
                type: 'int'
            }]
    });