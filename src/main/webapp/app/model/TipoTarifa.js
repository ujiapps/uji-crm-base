Ext.define('CRM.model.TipoTarifa', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'nombre',
        type: 'string'
    }, {
        name: 'campanyaId',
        type: 'int',
        useNull: true
    }, {
        name: 'campanyaNombre',
        type: 'string'
    // }, {
    //     name: 'diasCaducidad',
    //     type: 'int'
    // }, {
    //     name: 'diasVigencia',
    //     type: 'int'
    }, {
        name: 'mesesVigencia',
        type: 'int'
    }, {
        name: 'mesesCaduca',
        type: 'int'
    },{
        name: 'defecto',
        type: 'boolean'
    }, {
        name: 'lineaFacturacionId',
        type: 'int',
        useNull: true
    }]

});