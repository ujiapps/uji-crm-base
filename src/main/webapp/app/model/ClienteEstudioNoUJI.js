Ext.define('CRM.model.ClienteEstudioNoUJI',
    {
        extend: 'Ext.data.Model',
        idProperty: 'id',
        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'clienteId',
                type: 'int'
            },
            {
                name: 'nombre',
                type: 'string',
                useNull: true
            },
            {
                name: 'universidadId',
                type: 'int' ,
                useNull: true
            },
            {
                name: 'clasificacionId',
                type: 'int',
                useNull: true
            },
            {
                name: 'anyoFinalizacion',
                type: 'string',
                useNull: true
            },
            {
                name: 'tipoEstudio',
                type: 'int'
            },]
    });