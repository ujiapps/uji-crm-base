Ext.define('CRM.model.ClienteEstudioUJI', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'clienteId',
        type: 'int'
    }, {
        name: 'nombre',
        type: 'string',
        useNull: true
    }, {
        name: 'anyoFinalizacion',
        type: 'string',
        useNull: true
    }, {
        name: 'tipoEstudio',
        type: 'string'
    }, {
        name: 'tipoBeca',
        type: 'string'
    }]
});