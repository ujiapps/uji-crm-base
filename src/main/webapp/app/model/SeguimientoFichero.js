Ext.define('CRM.model.SeguimientoFichero',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'campanyaSeguimientoId',
                type: 'int'
            },
            {
                name: 'nombreFichero',
                type: 'string'
            },
            {
                name: 'referencia',
                type: 'string'
            },
            {
                name: 'contentType',
                type: 'string'
            }]
    });