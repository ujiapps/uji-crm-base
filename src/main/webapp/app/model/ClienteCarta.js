Ext.define('CRM.model.ClienteCarta',
    {
        extend: 'Ext.data.Model',
        idProperty: 'id',
        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'clienteId',
                type: 'int'
            },
            {
                name: 'fechaCreacion',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'clienteCarta',
                type: 'string'
            }]
    });