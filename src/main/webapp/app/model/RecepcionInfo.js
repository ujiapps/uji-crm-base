Ext.define('CRM.model.RecepcionInfo',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'nombre',
        type : 'string'
    } ]
});