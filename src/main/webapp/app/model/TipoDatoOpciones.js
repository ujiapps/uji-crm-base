Ext.define('CRM.model.TipoDatoOpciones',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int'
    },
    {
        name : 'etiqueta',
        type : 'string'
    } ]
});