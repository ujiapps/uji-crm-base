Ext.define('CRM.model.EnvioCriterio',
    {
        extend: 'Ext.data.Model',
        idProperty: 'id',
        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'envioId',
                type: 'int'
            },
            {
                name: 'tipo',
                type: 'int'
            },
            {
                name: 'origen',
                type: 'string'
            },
            {
                name: 'referencia1Id',
                type: 'int'
            },
            {
                name: 'referencia2Id',
                type: 'int'
            },
            {
                name: 'nombre',
                type: 'string'
            },
            {
                name: 'count',
                type: 'int'
            },
            {
                name: 'tipoComparacion',
                type: 'int'
            },
            {
                name: 'tipoComparacionCadena',
                type: 'string'
            },
            {
                name: 'tipoFecha',
                type: 'int'
            },
            {
                name: 'valor',
                type: 'string'
            }]
    });