Ext.define('CRM.model.CampanyaEnvioProgramado',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'envioId',
                type: 'int',
                useNull: true
            },
            {
                name: 'dias',
                type: 'int'
            }]

    });