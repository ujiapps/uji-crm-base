Ext.define('CRM.model.ProgramaPerfil',
{
    extend : 'Ext.data.Model',
    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'programaId',
        type : 'int'
    },
    {
        name : 'perfilId',
        type : 'int'
    },
    {
        name : 'tipoAcceso',
        type : 'string'
    },
    {
        name : 'perfilNombre',
        type : 'string'
    } ],
    validations : [
    {
        type : 'length',
        field : 'perfilId',
        min : 1
    },
    {
        type : 'length',
        field : 'programaId',
        min : 1
    } ]
});