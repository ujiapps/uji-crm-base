Ext.define('CRM.model.CampanyaSeguimiento',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'seguimientoTipoId',
        type : 'int',
        useNull : true
    },
    {
        name : 'campanyaFaseId',
        type : 'int'
    },
    {
        name : 'fecha',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'nombre',
        type : 'string'
    },
    {
        name : 'descripcion',
        type : 'string'
    },
    {
        name : 'seguimientoTipoNombre',
        type : 'string'
    } ]

});