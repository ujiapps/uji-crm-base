Ext.define('CRM.model.EnvioAdjunto',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'envioId',
                type: 'int'
            },
            {
                name: 'nombreFichero',
                type: 'string'
            },
            {
                name: 'referencia',
                type: 'string'
            },
            {
                name: 'tipoFichero',
                type: 'string'
            }]
    });