Ext.define('CRM.model.TipoAccesoProgramaPerfil',
{
    extend : 'Ext.data.Model',

    statics :
    {
        LECTURA : 'LECTURA',
        ESCRITURA : 'ESCRITURA'
    },

    fields : [
    {
        name : 'nombre',
        type : 'string'
    } ]
});