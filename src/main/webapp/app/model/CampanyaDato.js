Ext.define('CRM.model.CampanyaDato',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'campanyaFormularioId',
                type: 'int'
            },
            {
                name: 'clienteDatoTipoId',
                type: 'int'
            },
            {
                name: 'orden',
                type: 'int'
            },
            {
                name: 'clienteDatoTipoNombre',
                type: 'string'
            },
            {
                name: 'clienteDatoTipoTamanyo',
                type: 'int'
            },
            {
                name: 'clienteDatoTipoTipoId',
                type: 'int'
            },
            {
                name: 'textoCa',
                type: 'string'
            },
            {
                name: 'textoEs',
                type: 'string'
            },
            {
                name: 'textoUk',
                type: 'string'
            },
            {
                name: 'textoAyudaCa',
                type: 'string'
            },
            {
                name: 'textoAyudaEs',
                type: 'string'
            },
            {
                name: 'textoAyudaUk',
                type: 'string'
            },
            {
                name: 'grupoId',
                type: 'int',
                useNull: true
            },
            {
                name: 'grupoNombre',
                type: 'string'
            },
            {
                name: 'itemId',
                type: 'int',
                useNull: true
            },
            {
                name: 'itemNombre',
                type: 'string'
            },
            {
                name: 'requerido',
                type: 'boolean',
                defaultValue: true
            },
            {
                name: 'tipoAccesoId',
                type: 'int'
            },
            {
                name: 'accesoNombre',
                type: 'string'
            },
            {
                name: 'campanyaDatoExtra',
                type: 'string'
            },
            {
                name: 'vinculadoCampanya',
                type: 'boolean',
                defaultValue: false
            },
            {
                name: 'campanyaDatoExtraId',
                type: 'int',
                useNull: true
            },
            {
                name: 'tamanyo',
                type: 'int',
                useNull: true
            },
            {
                name: 'etiqueta',
                type: 'string'
            },
            {
                name: 'visualizar',
                type: 'boolean',
                defaultValue: true
            },
            {
                name: 'tipoValidacionId',
                type: 'int',
                useNull: true
            },
            {
                name: 'validacionValue',
                type: 'string',
                useNull: true
            },
            {
                name: 'preguntaRelacionadaId',
                type: 'int',
                useNull: true
            },
            {
                name: 'respuestaRelacionadaId',
                type: 'int',
                useNull: true
            },
            'itemsGrupo', 'opciones']

    });