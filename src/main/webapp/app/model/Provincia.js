Ext.define('CRM.model.Provincia',
    {
        extend: 'Ext.data.Model',

        fields: [{
            name: 'id',
            type: 'int',
            useNull: true
        }, {
            name: 'nombre',
            type: 'string'
        }, {
            name: 'orden',
            type: 'int'
        }]
    });