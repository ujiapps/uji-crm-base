Ext.define('CRM.model.Tarifa',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'tipoTarifaId',
        type : 'int',
        useNull : true
    },
    {
        name : 'fechaInicio',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'fechaFin',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'importe',
        type : 'int'
    } ]

});