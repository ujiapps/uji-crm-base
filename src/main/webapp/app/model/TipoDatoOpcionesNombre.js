Ext.define('CRM.model.TipoDatoOpcionesNombre',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int'
    },
    {
        name : 'nombre',
        type : 'string'
    } ]
});