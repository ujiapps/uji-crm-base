Ext.define('CRM.model.ClienteDatoOpcion',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'clienteDatoTipoId',
        type : 'int',
        useNull : true
    },
    {
        name : 'etiqueta',
        type : 'string'
    },
    {
        name : 'valor',
        type : 'int'
    } ]

});