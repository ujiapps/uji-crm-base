Ext.define('CRM.model.CampanyaCampanya',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'campanyaDestinoId',
                type: 'int'
            },
            {
                name: 'estadoDestinoId',
                type: 'int'
            },
            {
                name: 'campanyaOrigenId',
                type: 'int'
            },
            {
                name: 'estadoOrigenId',
                type: 'int'
            },
            //{
            //    name : 'campanyaOrigen',
            //    type : 'string'
            //},
            {
                name: 'estadoOrigen',
                type: 'string'
            },
            //{
            //    name : 'campanyaDestino',
            //    type : 'string'
            //},
            {
                name: 'estadoDestino',
                type: 'string'
            }]
        // ]

    });
