Ext.define('CRM.model.Item', {
    extend: 'Ext.data.Model',

    statics: {
        RECEPCION_NO_ACTIVO: 14,
        RECEPCION_MODIFICABLE: 15,
        RECEPCION_NO_MODIFICABLE: 16,
        RECEPCION_MODIFICABLE_Y_POR_DEFECTO: 17,
        RECEPCION_NO_MODIFICABLE_Y_POR_DEFECTO: 18
    },

    fields: [
        {name: 'id', type: 'int', useNull: true},
        {name: 'grupoId', type: 'int', useNull: true},
        {name: 'grupoNombre', type: 'string'},
        {name: 'tipoAccesoId', type: 'int', useNull: true},
        {name: 'tipoAccesoNombre', type: 'string'},
        {name: 'referencia', type: 'int'},
        {name: 'nombreCa', type: 'string'},
        {name: 'nombre', type: 'string'},
        {name: 'nombreEs', type: 'string'},
        {name: 'nombreUk', type: 'string'},
        {name: 'descripcionCa', type: 'string'},
        {name: 'descripcionEs', type: 'string'},
        {name: 'descripcionUk', type: 'string'},
        {name: 'visible', type: 'boolean', defaultValue: true},
        {name: 'orden', type: 'int'},
        {name: 'correoId', type: 'int'},
        {name: 'smsId', type: 'int'},
        {name: 'aplicacionMovilId', type: 'int'},
        {name: 'correoPostalId', type: 'int'},
        'mostrar',
        'ocultar']

});
