Ext.define('CRM.model.ClienteCuota',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'fechaInicio',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'fechaFin',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'clienteId',
                type: 'int'
            },
            {
                name: 'clienteTarifaId',
                type: 'int'
            },
            {
                name: 'importe',
                type: 'int'
            },
            {
                name: 'reciboCRM',
                type: 'int'
            },
            {
                name: 'reciboId',
                type: 'int',
                useNull: true
            },
            {
                name: 'codificacion',
                type: 'string'
            },
            {
                name: 'estado',
                type: 'int'
            }]

    });