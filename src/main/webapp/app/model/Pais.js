Ext.define('CRM.model.Pais',
    {
        extend: 'Ext.data.Model',

        fields: [{
            name: 'id',
            type: 'string',
            useNull: true
        }, {
            name: 'nombre',
            type: 'string'
        }, {
            name: 'orden',
            type: 'int'
        }]
    });