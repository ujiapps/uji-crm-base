Ext.define('CRM.model.ProgramaEnviosTree',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int'
    },
    {
        name : 'text',
        type : 'string'
    },
    {
        name : 'checked',
        type : 'boolean'
    },
//    {
//        name : 'qtip',
//        mapping : 'text'
//    },
    {
        name : 'leaf',
        type : 'boolean'
    } ]

});