Ext.define('CRM.model.CuentaCorporativa', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'nombre',
        type: 'string'
    }, {
        name: 'inicio',
        type: 'date',
        dateFormat: 'd/m/Y H:i:s'
    }, {
        name: 'fin',
        type: 'date',
        dateFormat: 'd/m/Y H:i:s'
    }, {
        name: 'principal',
        type: 'boolean'
    }]
});