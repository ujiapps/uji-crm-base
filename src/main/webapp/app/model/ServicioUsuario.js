Ext.define('CRM.model.ServicioUsuario',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'personaId',
        type : 'int'
    },
    {
        name : 'servicioId',
        type : 'int'
    },
    {
        name : 'nombrePersona',
        type : 'string'
    } ]
});