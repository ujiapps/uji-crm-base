Ext.define('CRM.model.Recibo',
    {
        extend: 'Ext.data.Model',

        fields: [

            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'codificacion',
                type: 'string'
            },
            {
                name: 'fechaCreacion',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'fechaPago',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'importeNeto',
                type: 'int'
            },
            {
                name: 'tipoReciboNombre',
                type: 'string'
            },
            {
                name: 'tipoCobroNombre',
                type: 'string'
            },
            {
                name: 'fechaPagoLimite',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'persona',
                type: 'int'
            },
            {
                name: 'observaciones',
                type: 'string'
            },
            {
                name: 'descripcion',
                type: 'string'
            },
            {
                name: 'idioma',
                type: 'string'
            },
            {
                name: 'correo',
                type: 'string'
            },
            {
                name: 'ibanAbono',
                type: 'string'
            },
            {
                name: 'ibanDomiciliacion',
                type: 'string'
            },
            {
                name: 'pie',
                type: 'string'
            },
            {
                name: 'destinatario',
                type: 'string'
            },
            {
                name: 'exportado',
                type: 'int'
            },
            {
                name: 'moroso',
                type: 'boolean'
            }]
    });