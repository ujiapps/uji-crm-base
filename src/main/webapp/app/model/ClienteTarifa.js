Ext.define('CRM.model.ClienteTarifa',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },
            {
                name: 'fechaCaducidad',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'fechaInicioTarifa',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'fechaUltimaModificacion',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'clienteId',
                type: 'int'
            },
            {
                name: 'tipoTarifaId',
                type: 'int'
            },
            {
                name: '_tipoTarifa',
                type: 'auto'
            }]

    });