Ext.define('CRM.model.ClienteDatoTipo', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'nombre',
        type: 'string'
    }, {
        name: 'tamanyo',
        type: 'int',
        useNull: true
    }, {
        name: 'orden',
        type: 'int',
        useNull: true
    }, {
        name: 'clienteDatoTipoTipoId',
        type: 'int',
        useNull: true
    }, {
        name: 'duplicar',
        type: 'boolean',
        useNull: true
    }, {
        name: 'modificar',
        type: 'boolean',
        useNull: true
    }, {
        name: 'clienteDatoTipoTipoNombre',
        type: 'string'
    }]
});