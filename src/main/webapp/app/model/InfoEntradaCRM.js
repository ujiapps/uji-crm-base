Ext.define('CRM.model.InfoEntradaCRM',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'actoId',
                type: 'int'
            },
            {
                name: 'clienteId',
                type: 'int'
            },
            {
                name: 'persona',
                type: 'string'
            },
            {
                name: 'entradas',
                type: 'int',
                useNull: true
            },
            {
                name: 'sobrantes',
                type: 'int',
                useNull: true
            },
            {
                name: 'nuevas',
                type: 'int'
            },
            {
                name: 'dadas',
                type: 'boolean'
            }]
    });