Ext.define('CRM.store.StoreCampanyaFormularios',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CampanyaFormulario',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanyaformulario',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters :
    {
        property : 'orden',
        direction : 'ASC'
    }
});