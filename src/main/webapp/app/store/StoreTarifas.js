Ext.define('CRM.store.StoreTarifas',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Tarifa',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tarifa',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});