Ext.define('CRM.store.StoreGruposCampanya',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Grupo',
    autoLoad : false,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanya/0/grupo',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters :
    {
        property : 'orden',
        direction : 'ASC'
    }

});