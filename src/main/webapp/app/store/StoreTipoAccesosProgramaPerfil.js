Ext.define('CRM.store.StoreTipoAccesosProgramaPerfil',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoAccesoProgramaPerfil',
    autoLoad : 'true',

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipoaccesoprogramaperfil',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});