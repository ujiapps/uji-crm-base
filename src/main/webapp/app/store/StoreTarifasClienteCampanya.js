Ext.define('CRM.store.StoreTarifasClienteCampanya',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteTarifa',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/clientetarifa',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});