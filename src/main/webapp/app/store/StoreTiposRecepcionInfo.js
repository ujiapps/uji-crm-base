Ext.define('CRM.store.StoreTiposRecepcionInfo',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.RecepcionInfo',
    autoLoad : true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipo/recepcioninfo',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});
