Ext.define('CRM.store.StoreEnviosItems',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.EnvioItem',
    autoLoad : false,
    autoSync : true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/envio/item',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});