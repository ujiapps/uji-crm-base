Ext.define('CRM.store.StoreCampanyaCartas',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CampanyaCarta',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanyacarta',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});