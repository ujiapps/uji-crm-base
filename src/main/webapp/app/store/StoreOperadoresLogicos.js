Ext.define('CRM.store.StoreOperadoresLogicos',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.TipoDato',
        autoLoad: true,

        data: [
            {
                id: 1,
                nombre: 'Y'
            },
            {
                id: 2,
                nombre: 'O'
            },
            {
                id: 3,
                nombre: 'Y No'
            },
            {
                id: 4,
                nombre: 'O No'
            }],

        proxy: {
            type: 'memory',

            reader: {
                type: 'json'
            }
        }
    });