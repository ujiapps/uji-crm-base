Ext.define('CRM.store.StoreSeguimientoTipos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CampanyaSeguimiento',
    autoLoad : true,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/seguimientotipo',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});