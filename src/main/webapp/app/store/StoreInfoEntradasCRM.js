Ext.define('CRM.store.StoreInfoEntradasCRM',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.InfoEntradaCRM',
        autoLoad: false,
        autoSync: false,
        proxy: {
            type: 'rest',
            url: '/crm/rest/entradas',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            },

            writer: {
                type: 'json'
            }
        }

    });