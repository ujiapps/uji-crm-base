Ext.define('CRM.store.StoreClientesDatosOpciones',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteDatoOpcion',
    autoLoad : false,
    autoSync : false,
    sorters : [ 'etiqueta' ],
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/clientedatoopcion',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});