Ext.define('CRM.store.StoreProgramasTree',
{
    extend : 'Ext.data.TreeStore',
    model : 'CRM.model.ProgramaTree',
    autoLoad : false,
    expanded : true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/programa/cliente/tree',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'row'
        },

        writer :
        {
            type : 'json'
        }
    }

});