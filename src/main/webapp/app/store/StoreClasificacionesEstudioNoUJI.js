Ext.define('CRM.store.StoreClasificacionesEstudioNoUJI',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClasificacionEstudioNoUJI',
    autoLoad : true,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/clasificacionestudionouji',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});