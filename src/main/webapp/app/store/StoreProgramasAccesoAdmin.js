Ext.define('CRM.store.StoreProgramasAccesoAdmin',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Programa',
    autoLoad : true,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/programa/admin',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        }
    }
});