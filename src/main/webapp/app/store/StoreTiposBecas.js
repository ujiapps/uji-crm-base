Ext.define('CRM.store.StoreTiposBecas',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoBeca',
    autoLoad : true,
    sortOnLoad: true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipobeca',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});