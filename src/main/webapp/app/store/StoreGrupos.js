Ext.define('CRM.store.StoreGrupos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Grupo',
    autoLoad : true,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/grupo',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters : [
    {
        property : 'programaNombre',
        direction : 'ASC'
    },
    {
        property : 'orden',
        direction : 'ASC'
    } ]

});