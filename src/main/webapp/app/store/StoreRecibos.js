Ext.define('CRM.store.StoreRecibos',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.Recibo',
        autoLoad: false,
        autoSync: false,
        pageSize: 50,
        remoteSort: true,
        proxy: {
            type: 'rest',
            url: '/crm/rest/recibo',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data',
                totalProperty: 'totalCount'
            },
            simpleSortMode: true,

            extraParams: {
                arrayCampanyas: null,
                arrayFechas: null,
                arrayEstados : null,
                arrayExportaciones: null
            },

            writer: {
                type: 'json'
            }
        },
        sorters: [
            {
                property: 'id',
                direction: 'DESC'
            }]
    });