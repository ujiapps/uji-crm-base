Ext.define('CRM.store.StoreCampanyasEnviosProgramados',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.CampanyaEnvioProgramado',
        autoLoad: false,
        autoSync: false,
        proxy: {
            type: 'rest',
            url: '/crm/rest/campanyaenvioprogramado',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            },

            writer: {
                type: 'json'
            }
        }

    });