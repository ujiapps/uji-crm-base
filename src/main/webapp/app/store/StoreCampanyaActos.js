Ext.define('CRM.store.StoreCampanyaActos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CampanyaActo',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanyaacto',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});