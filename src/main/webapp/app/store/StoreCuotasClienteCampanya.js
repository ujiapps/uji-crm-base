Ext.define('CRM.store.StoreCuotasClienteCampanya',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteCuota',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/clientecuota',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});