Ext.define('CRM.store.StoreTiposAcceso',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoAcceso',
    autoLoad: true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipoacceso',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});