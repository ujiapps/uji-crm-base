Ext.define('CRM.store.StoreCampanyasAdminTree', {
    extend: 'Ext.data.TreeStore',
    requires: 'CRM.model.CampanyaAdminTree',
    model: 'CRM.model.CampanyaAdminTree',
    autoLoad: false,
    root: {
        id: '0',
        urlPath: '',
        text: ' ',
        title: '/',
        expanded: true,
    },
    proxy: {
        type: 'rest',
        url: '/crm/rest/campanya/user/tree/root/admin',
        reader: {
            type: 'json',
            successProperty: 'success',
            rootProperty: 'data',
            root: 'data',
            totalProperty: 'totalCount'
        }
    }
});