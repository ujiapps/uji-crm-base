Ext.define('CRM.store.StoreSeguimientosCampanya',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Seguimiento',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/seguimiento',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});