Ext.define('CRM.store.StorePersonasFicheroCsv',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.Persona',
        autoLoad: false,
        autoSync: false,
        pageSize: 50,
        proxy: {
            type: 'rest',
            url: '/crm/rest/fichero/persona',
            batchActions: true,
            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data',
                totalProperty: 'totalCount'
            },


            writer: {
                type: 'json'
            }
        }

    });