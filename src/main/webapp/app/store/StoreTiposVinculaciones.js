Ext.define('CRM.store.StoreTiposVinculaciones',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDato',
    autoLoad : true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipo/vinculaciones',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});
