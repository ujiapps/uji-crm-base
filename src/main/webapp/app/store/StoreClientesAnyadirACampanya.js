Ext.define('CRM.store.StoreClientesAnyadirACampanya',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Cliente',
    autoLoad : false,
    autoSync : false,
    pageSize : 50,
    sorters : [ 'personaNombre', 'correo', 'movil', 'etiqueta' ],
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/cliente',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data',
            totalProperty : 'totalCount'
        },

        extraParams :
        {
            busqueda : null,
            etiqueta : null,
            tipo : null,
            correo : null,
            movil : null,
            campanya : null,
            campanyaTipoEstado : null,
            tipoDato : null,
            valorDato : null,
            programa : null,
            grupo : null,
            item : null,
            fecha : null,
            fechaMax : null,
            tipoComparacion : null,
            tipoFecha : null,
            mes : null,
            anyo : null,
            mesMax : null,
            anyoMax : null
        },

        writer :
        {
            type : 'json'
        }
    }

});