Ext.define('CRM.store.StoreEtiquetasVentanaCriterios',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Etiqueta',
    autoLoad : true,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/etiqueta',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters :
    {
        property : 'nombre',
        direction : 'ASC'
    }

});