Ext.define('CRM.store.StoreClientes',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.Cliente',
        autoLoad: false,
        autoSync: false,
        proxy: {
            type: 'rest',
            url: '/crm/rest/cliente',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            },

            writer: {
                type: 'json'
            }
        }

    });