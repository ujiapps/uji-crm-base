Ext.define('CRM.store.StoreTiposIdentificacion',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.TipoDato',
        autoLoad: true,
        proxy: {
            type: 'rest',
            url: '/crm/rest/tipo/tipoidentificacion',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            },

            writer: {
                type: 'json'
            }
        }

    });