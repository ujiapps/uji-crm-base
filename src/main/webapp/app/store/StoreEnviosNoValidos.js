Ext.define('CRM.store.StoreEnviosNoValidos',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.EnvioNoValido',
        autoLoad: true,
        pageSize: 40,
        remoteSort: true,
        proxy: {
            type: 'rest',
            url: '/crm/rest/correonovalido',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data',
                totalProperty: 'totalCount'
            },

            extraParams: {
                correo : null
                // arrayBusqueda: null,
                //fecha: null,
                //fechaMax: null,
                //tipoComparacion: null,
                //tipoFecha: null,
                //mes: null,
                //anyo: null,
                //mesMax: null,
                //anyoMax: null,
                //comportamiento: 0,
                // modificado: 0,
                // operador : 0
            },

            writer: {
                type: 'json'
            }
        },
        sorters: {
            property: 'nombre',
            direction: 'ASC'
        }

    });