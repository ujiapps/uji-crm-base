Ext.define('CRM.store.StoreEnviosAdjuntos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.EnvioAdjunto',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/envio/adjunto',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});