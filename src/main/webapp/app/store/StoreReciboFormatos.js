Ext.define('CRM.store.StoreReciboFormatos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ReciboFormato',
    autoLoad : true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/reciboformato',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});