Ext.define('CRM.store.StoreTiposIdioma',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoIdioma',
    autoLoad : true,

    data : [
    {
        id : 'CA',
        nombre : 'Valencià'
    },
    {
        id : 'ES',
        nombre : 'Castellano'
    },
    {
        id : 'UK',
        nombre : 'English'
    } ],

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json'
        }
    }
});