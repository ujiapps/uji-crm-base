Ext.define('CRM.store.StoreSeguimientos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Seguimiento',
    autoLoad : false,
    autoSync : false,
    pageSize: 25,
    sortOnLoad: true,
    sorters: [{
        property: 'nombre',
        direction: 'DESC'
    }],
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/seguimiento',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data',
            totalProperty: 'totalCount'
        },

        writer :
        {
            type : 'json'
        }
    }
});