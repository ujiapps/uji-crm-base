Ext.define('CRM.store.StoreSeguimientoFicheros',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.SeguimientoFichero',
    autoLoad : false,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/seguimientofichero',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters :
    {
        property : 'nombreFichero',
        direction : 'ASC'
    }

});