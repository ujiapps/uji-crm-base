Ext.define('CRM.store.StoreCampanyaSeguimientos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CampanyaSeguimiento',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanyaseguimiento',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});