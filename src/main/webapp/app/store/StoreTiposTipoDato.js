Ext.define('CRM.store.StoreTiposTipoDato',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDato',
    autoLoad : true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipo/tipodatos',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});
