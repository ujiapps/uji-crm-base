Ext.define('CRM.store.StoreClienteEtiquetas',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteEtiqueta',
    autoLoad : false,
    pageSize: 25,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/clienteetiqueta',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data',
            totalProperty: 'totalCount'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters :
    {
        property : 'etiquetaNombre',
        direction : 'ASC'
    }

});