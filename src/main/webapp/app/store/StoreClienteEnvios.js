Ext.define('CRM.store.StoreClienteEnvios',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.EnvioCliente',
        autoLoad: false,
        autoSync: true,
        sortOnLoad: true,
        pageSize: 25,
        remoteSort: true,
        sorters: [{
            property: 'fechaEnvio',
            direction: 'DESC'
        }, {
            property: 'fecha',
            direction: 'DESC'
        }],

        proxy: {
            type: 'rest',
            url: '/crm/rest/enviocliente/',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data',
                totalProperty: 'totalCount'
            },
            simpleSortMode: true,

            writer: {
                type: 'json'
            }
        }

    });