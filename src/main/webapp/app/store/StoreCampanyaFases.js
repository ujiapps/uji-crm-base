Ext.define('CRM.store.StoreCampanyaFases',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CampanyaFase',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanyafase',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});