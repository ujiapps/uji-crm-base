Ext.define('CRM.store.StoreEnviosCriterios', {
    extend: 'Ext.data.Store',
    model: 'CRM.model.EnvioCriterio',
    autoLoad: false,
    autoSync: false,
    proxy: {
        type: 'rest',
        url: '/crm/rest/enviocriterio',

        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data'
        },

        writer: {
            type: 'json'
        }
    }
});