Ext.define('CRM.store.StoreTarifasEnvios',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.TipoTarifaEnvio',
        autoLoad: false,
        autoSync: false,
        proxy: {
            type: 'rest',
            url: '/crm/rest/tipotarifaenvio',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            },

            writer: {
                type: 'json'
            }
        }

    });