Ext.define('CRM.store.StoreCampanyaEnviosAdmin',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CampanyaEnvioAdmin',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanyaenvioadmin',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});