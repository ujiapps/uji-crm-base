Ext.define('CRM.store.StoreGruposFaltanCliente',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Grupo',
    autoLoad : false,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/grupo/faltan',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters : [
    {
        property : 'programaNombre',
        direction : 'ASC'
    },
    {
        property : 'orden',
        direction : 'ASC'
    } ]

});