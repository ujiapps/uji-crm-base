Ext.define('CRM.store.StoreLineasFacturacion',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.LineaFacturacion',
    autoLoad : false,
    autoSync: false,
    
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/lineafacturacion',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});