Ext.define('CRM.store.StoreVinculacionesUJI',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.VinculoUji',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/vinculouji',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});