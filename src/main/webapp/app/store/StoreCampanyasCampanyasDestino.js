Ext.define('CRM.store.StoreCampanyasCampanyasDestino',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CampanyaCampanya',
    autoLoad : false,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanyacampanya',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters :
    {
        property : 'orden',
        direction : 'ASC'
    }

});