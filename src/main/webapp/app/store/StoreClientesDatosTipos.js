Ext.define('CRM.store.StoreClientesDatosTipos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteDatoTipo',
    autoLoad : true,
    autoSync : false,
    sorters : [ 'orden' ],
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/clientedatotipo',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});