Ext.define('CRM.store.StoreTiposAccionProgramacionEnvio',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.TipoDato',
        autoLoad: true,

        data: [
            {
                id: 1,
                nombre: 'Abans'
            },
            {
                id: 2,
                nombre: 'Despues'
            }],

        proxy: {
            type: 'memory',

            reader: {
                type: 'json'
            }
        }
    });