Ext.define('CRM.store.StoreMovimientosRecibo',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.MovimientoRecibo',
        autoLoad: false,
        autoSync: false,

        proxy: {
            type: 'rest',
            url: '/crm/rest/movimientorecibo',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            }
        },

        sortOnLoad: true,
        sorters: {
            property: 'fecha',
            direction: 'ASC'
        }

    });