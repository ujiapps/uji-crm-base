Ext.define('CRM.store.StoreClientesDatosTiposClientes',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteDatoTipo',
    autoLoad : true,
    autoSync : false,
    sorters : [ 'nombre' ],
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/clientedatotipo/clientes',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});