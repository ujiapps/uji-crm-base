Ext.define('CRM.store.StoreEnviosPlantillas', {
    extend: 'Ext.data.Store',
    model: 'CRM.model.EnvioPlantilla',
    autoLoad: true,
    autoSync: false,
    proxy: {
        type: 'rest',
        url: '/crm/rest/envioplantilla',

        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data',
            totalProperty: 'totalCount'
        },

        writer: {
            type: 'json'
        }
    }
});