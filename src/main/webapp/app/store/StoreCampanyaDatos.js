Ext.define('CRM.store.StoreCampanyaDatos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CampanyaDato',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanyadato',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters :
    {
        property : 'orden',
        direction : 'ASC'
    }
});