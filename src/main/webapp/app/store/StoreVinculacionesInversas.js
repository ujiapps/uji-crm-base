Ext.define('CRM.store.StoreVinculacionesInversas',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Vinculacion',
    autoLoad : false,
    autoSync : false,
    pageSize: 25,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/vinculacion/inversa/cliente',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data',
            totalProperty: 'totalCount'
        },

        writer :
        {
            type : 'json'
        }
    }

});