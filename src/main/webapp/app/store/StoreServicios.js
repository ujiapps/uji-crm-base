Ext.define('CRM.store.StoreServicios',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Servicio',
    autoLoad : true,
    autoSync: false,
    
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/servicio',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});