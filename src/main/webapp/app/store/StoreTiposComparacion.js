Ext.define('CRM.store.StoreTiposComparacion',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.TipoDato',
        autoLoad: true,
        proxy: {
            type: 'rest',
            url: '/crm/rest/tipo/comparaciones',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            },

            writer: {
                type: 'json'
            }
        }

    });