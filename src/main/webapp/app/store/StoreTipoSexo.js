Ext.define('CRM.store.StoreTipoSexo',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDato',
    autoLoad : true,

    data : [
    {
        id : 2,
        nombre : 'Mujer'
    },
    {
        id : 1,
        nombre : 'Hombre'
    }],

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json'
        }
    }
});