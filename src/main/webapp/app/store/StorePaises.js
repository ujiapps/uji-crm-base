Ext.define('CRM.store.StorePaises',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.Pais',
        autoLoad: true,
        proxy: {
            type: 'rest',
            url: '/crm/rest/pais',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            },

            writer: {
                type: 'json'
            }
        },
        sortOnLoad: true,
        sorters: [{
            property: 'orden',
            direction: 'asc'
        }, {
            property: 'nombre',
            direction: 'asc'
        }]

    })
;