Ext.define('CRM.store.StoreTiposCampanyaEnviosAuto',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDato',
    autoLoad: true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipo/campanyaenviosauto',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});