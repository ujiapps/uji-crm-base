Ext.define('CRM.store.StoreGruposTodos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Grupo',
    autoLoad : true,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/grupo/all',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters : [
    {
        property : 'programaNombre',
        direction : 'ASC'
    },
    {
        property : 'orden',
        direction : 'ASC'
    } ]

});