Ext.define('CRM.store.StoreCampanyaEnvios',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CampanyaEnvio',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanyaenvioauto',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});