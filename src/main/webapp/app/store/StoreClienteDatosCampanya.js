Ext.define('CRM.store.StoreClienteDatosCampanya',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteDatoExtra',
    autoLoad : false,
    autoSync : true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/cliente/datosextra',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});