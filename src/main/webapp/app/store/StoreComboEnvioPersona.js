Ext.define('CRM.store.StoreComboEnvioPersona',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ComboBusqueda',
    autoLoad : true,

    data : [
    {
        id : -1,
        etiqueta : 'por mi'
    },
    {
        id : -2,
        etiqueta : 'por cualquier persona'
    },
    {
        id : -3,
        etiqueta : 'creados automáticamente'
    } ],

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json'
        }
    }
});