Ext.define('CRM.store.StoreProgramas', {
    extend: 'Ext.data.Store',
    model: 'CRM.model.Programa',
    autoLoad: true,
    autoSync: true,
    remoteSort: true,

    proxy: {
        type: 'rest',
        url: '/crm/rest/programa',

        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data'
        },

        simpleSortMode: true,
        writer: {
            type: 'json'
        }
    },
    sorters: [{
        property: 'orden',
        direction: 'ASC'
    }, {
        property: 'nombreCa',
        direction: 'ASC'
    }]

});