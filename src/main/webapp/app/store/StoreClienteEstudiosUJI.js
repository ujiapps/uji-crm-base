Ext.define('CRM.store.StoreClienteEstudiosUJI',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteEstudioUJI',
    autoLoad : false,
    autoSync : false,
    pageSize: 25,
    sortOnLoad: true,
    sorters: [{
        property: 'nombre',
        direction: 'DESC'
    }],
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/clienteestudiouji',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data',
            totalProperty: 'totalCount'
        },
        writer :
        {
            type : 'json'
        }
    }

});