Ext.define('CRM.store.StoreAnyos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDato',
    autoLoad : true,

    listeners :
    {
        load : function(store)
        {
            var data = []
            var i = 1920;
            var fechaMax= new Date().getFullYear();
            while (i <= fechaMax)
            {
                data.push(
                {
                    'id' : i,
                    'nombre' : i
                });
                i++;

            }
            store.loadData(data, false);
        }
    },
    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json'
        }
    }
});