Ext.define('CRM.store.StoreProvincias',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.Provincia',
        autoLoad: false,
        proxy:
            {
                type: 'rest',
                url: '/crm/rest/provincia',

                reader:
                    {
                        type: 'json',
                        successProperty: 'success',
                        root: 'data'
                    },

                writer:
                    {
                        type: 'json'
                    }
            },
        // sortOnLoad: true,
        // sorters: [{
        //     property: 'nombre',
        //     direction: 'asc',
        //     // transform: function(value){
        //     //
        //     // }
        // }, {
        //     property: 'orden',
        //     direction: 'asc'
        // }]

    });