Ext.define('CRM.store.StoreTiposValidacion',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.TipoValidacion',
        autoLoad: true,
        proxy: {
            type: 'rest',
            url: '/crm/rest/tipovalidacion',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            },

            writer: {
                type: 'json'
            }
        }

    });