Ext.define('CRM.store.StoreClienteCampanyasOrigen',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CampanyaCliente',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanyacliente',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});