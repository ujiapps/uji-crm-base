Ext.define('CRM.store.StoreItemsItemsOrigen',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ItemItem',
    autoLoad : false,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/itemitem',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters :
    {
        property : 'orden',
        direction : 'ASC'
    }

});