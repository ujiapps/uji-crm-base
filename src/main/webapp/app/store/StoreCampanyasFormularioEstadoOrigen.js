Ext.define('CRM.store.StoreCampanyasFormularioEstadoOrigen',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.CampanyaFormularioEstadoOrigen',
        autoLoad: false,
        autoSync: false,

        proxy: {
            type: 'rest',
            url: '/crm/rest/campanyaformularioestadoorigen',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            },

            writer: {
                type: 'json'
            }
        },

        sortOnLoad: true,
        sorters: {
            property: 'orden',
            direction: 'ASC'
        }

    });