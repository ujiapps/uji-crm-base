Ext.define('CRM.store.StoreClienteCuentas',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CuentaCorporativa',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/cuentacorporativa',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },
        writer :
        {
            type : 'json'
        }
    }

});