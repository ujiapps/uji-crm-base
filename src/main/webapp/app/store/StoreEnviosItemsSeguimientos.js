Ext.define('CRM.store.StoreEnviosItemsSeguimientos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.EnvioItem',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/envio/item',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});