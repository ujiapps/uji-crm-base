Ext.define('CRM.store.StoreTiposEstadoCuota',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.TipoDato',
        autoLoad: true,
        proxy: {
            type: 'rest',
            url: '/crm/rest/tipo/estadocuota',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            },

            writer: {
                type: 'json'
            }
        }

    });