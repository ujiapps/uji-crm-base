Ext.define('CRM.store.StoreProgramaCursos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ProgramaCurso',
    autoLoad : false,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/programacurso',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});