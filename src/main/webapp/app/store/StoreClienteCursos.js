Ext.define('CRM.store.StoreClienteCursos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CursoInscripcion',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/cursoinscripcion',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },
        writer :
        {
            type : 'json'
        }
    }

});