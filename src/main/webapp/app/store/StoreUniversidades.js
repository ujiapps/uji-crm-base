Ext.define('CRM.store.StoreUniversidades',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Universidad',
    autoLoad : true,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/universidad',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});