Ext.define('CRM.store.StoreCampanyasUserTree', {
    extend: 'Ext.data.TreeStore',
    // requires: 'CRM.model.CampanyaUserTree',
    // model: 'CRM.model.CampanyaUserTree',
    autoLoad: false,
    root: {
        id: '0',
        urlPath: '',
        text: ' ',
        title: '/',
        expanded: true,
    },
    proxy: {
        type: 'rest',
        url: '/crm/rest/campanya/user/tree/root/',
        reader: {
            type: 'json',
            successProperty: 'success',
            rootProperty: 'data',
            root: 'data',
            totalProperty: 'totalCount'
        }
    }
});