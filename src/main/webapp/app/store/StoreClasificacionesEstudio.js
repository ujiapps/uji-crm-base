Ext.define('CRM.store.StoreClasificacionesEstudio',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClasificacionEstudio',
    autoLoad : true,
    autoSync : false,
    sortOnLoad: true,
    sorters: [{
        property: 'nombre',
        direction: 'DESC'
    }],
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/clasificacionestudio/',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data',
            totalProperty: 'totalCount'
        },

        writer :
        {
            type : 'json'
        }
    }

});