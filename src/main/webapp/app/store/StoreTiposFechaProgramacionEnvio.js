Ext.define('CRM.store.StoreTiposFechaProgramacionEnvio',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.TipoDato',
        autoLoad: true,

        data: [
            {
                id: 1,
                nombre: 'Data inici rebut'
            },
            {
                id: 2,
                nombre: 'Data fí rebut'
            },
            {
                id: 3,
                nombre: 'Data fí tipo tarifa'
            }],

        proxy: {
            type: 'memory',

            reader: {
                type: 'json'
            }
        }
    });