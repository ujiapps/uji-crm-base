Ext.define('CRM.store.StoreVinculaciones',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Vinculacion',
    autoLoad : false,
    autoSync : false,
    pageSize: 25,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/vinculacion',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data',
            totalProperty: 'totalCount'
        },

        writer :
        {
            type : 'json'
        }
    }

});