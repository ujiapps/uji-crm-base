Ext.define('CRM.store.StoreMeses',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDato',
    autoLoad : true,

    data : [
    {
        id : 1,
        nombre : 'Enero'
    },
    {
        id : 2,
        nombre : 'Febrero'
    },
    {
        id : 3,
        nombre : 'Marzo'
    },
    {
        id : 4,
        nombre : 'Abril'
    },
    {
        id : 5,
        nombre : 'Mayo'
    },
    {
        id : 6,
        nombre : 'Junio'
    },
    {
        id : 7,
        nombre : 'Julio'
    },
    {
        id : 8,
        nombre : 'Agosto'
    },
    {
        id : 9,
        nombre : 'Septiembre'
    },
    {
        id : 10,
        nombre : 'Octubre'
    },
    {
        id : 11,
        nombre : 'Noviembre'
    },
    {
        id : 12,
        nombre : 'Diciembre'
    } ],

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json'
        }
    }
});