Ext.define('CRM.store.StoreClienteEstadoCampanyaMotivos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteEstadoCampanyaMotivo',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/clienteestadocampanyamotivo',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});