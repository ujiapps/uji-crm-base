Ext.define('CRM.store.StoreEnviosSeguimientos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Envio',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/envio',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});