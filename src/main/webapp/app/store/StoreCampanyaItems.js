Ext.define('CRM.store.StoreCampanyaItems',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CampanyaItem',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanyaitem',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});