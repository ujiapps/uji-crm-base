Ext.define('CRM.store.StoreFases',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Fase',
    autoLoad : true,
    autoSync : true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/fase',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});