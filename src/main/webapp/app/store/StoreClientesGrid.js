Ext.define('CRM.store.StoreClientesGrid',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteGrid',
    autoLoad : false,
    autoSync : false,
    pageSize : 50,
    sorters : [ 'nombreCompleto', 'correo', 'movil'],

    groupers: [{
        property: 'id',
        direction: 'ASC'
    }],

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/clientegrid',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data',
            idProperty: 'clienteId',
            totalProperty : 'totalCount'
        },

        extraParams :
        {
            busqueda : null,
            etiqueta : null,
            tipo : null,
            correo : null,
            movil : null,
            campanya : null,
            campanyaTipoEstado : null,
            tipoDato : null,
            valorDato : null,
            programa : null,
            grupo : null,
            item : null,
            fecha : null,
            fechaMax : null,
            tipoComparacion : null,
            tipoFecha : null,
            mes : null,
            anyo : null,
            mesMax : null,
            anyoMax : null
        },

        writer :
        {
            type : 'json'
        }
    }

});