Ext.define('CRM.store.StoreCampanyaTextos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CampanyaTexto',
    autoLoad : true,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanyatexto',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});