Ext.define('CRM.store.StoreCampanyaClientes',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CampanyaCliente',
    autoLoad : false,
    autoSync : false,
    pageSize : 50,
    remoteSort: true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanyacliente',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data',
            totalProperty : 'totalCount'
        },

        extraParams :
        {
            campanyaId : null,
            campanyaClienteTipoId : null,
            busqueda : null,
            etiqueta : null,
            items : null,
            itemsOpcion : null,
            seguimientoTipo : null,
            seguimientoTipoOpcion : null
        },

        writer :
        {
            type : 'json'
        }
    }

});