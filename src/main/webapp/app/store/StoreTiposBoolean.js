Ext.define('CRM.store.StoreTiposBoolean',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDato',
    autoLoad : true,

    data : [
    {
        id : 1,
        nombre : 'Si'
    },
    {
        id : 0,
        nombre : 'No'
    } ],

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json'
        }
    }
});