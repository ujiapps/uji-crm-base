Ext.define('CRM.store.StoreClienteDatos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteDato',
    autoLoad : false,
    autoSync : false,
    sortOnLoad: true,
    pageSize: 25,
    sorters: [{
        property: 'fechaUltimaModificacion',
        direction: 'DESC'
    }],
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/dato',

        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data',
            totalProperty: 'totalCount'
        },
        simpleSortMode: true,

        writer :
        {
            type : 'json'
        }
    }

});