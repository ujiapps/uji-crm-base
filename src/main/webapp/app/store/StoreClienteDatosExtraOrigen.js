Ext.define('CRM.store.StoreClienteDatosExtraOrigen',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteDatoExtra',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/cliente/datosextra',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});