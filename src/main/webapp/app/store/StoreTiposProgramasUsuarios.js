Ext.define('CRM.store.StoreTiposProgramasUsuarios',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoProgramasUsuarios',
    autoLoad : 'true',

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipoaccesoprogramausuario',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});