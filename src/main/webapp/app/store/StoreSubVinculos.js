Ext.define('CRM.store.StoreSubVinculos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.SubVinculo',
    autoLoad : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/subvinculo',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});