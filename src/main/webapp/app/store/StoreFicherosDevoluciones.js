Ext.define('CRM.store.StoreFicherosDevoluciones',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.FicheroDevolucion',
    autoLoad : true,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/ficherodevolucion',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});