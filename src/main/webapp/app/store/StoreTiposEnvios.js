Ext.define('CRM.store.StoreTiposEnvios',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDato',
    autoLoad : true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipo/envio',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});