Ext.define('CRM.store.StoreTiposCorreo',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.TipoDato',
        autoLoad: true,

        data: [
            {
                id: 0,
                nombre: 'Correu Personal'
            },
            {
                id: 1,
                nombre: 'Correu Oficial'
            }],

        proxy: {
            type: 'memory',

            reader: {
                type: 'json'
            }
        }
    });