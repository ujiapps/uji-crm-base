Ext.define('CRM.store.StoreEmisoras',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.Emisora',
        autoLoad: true,
        autoSync: false,

        proxy: {
            type: 'rest',
            url: '/crm/rest/emisora/',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            },

            writer: {
                type: 'json'
            }
        }

    });