Ext.define('CRM.store.StoreMovimientos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Movimiento',
    autoLoad : false,
    autoSync : false,
    pageSize: 25,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/movimiento',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data',
            totalProperty: 'totalCount'
        }
    },

    sortOnLoad : true,
    sorters :
    {
        property : 'fecha',
        direction : 'ASC'
    }

});