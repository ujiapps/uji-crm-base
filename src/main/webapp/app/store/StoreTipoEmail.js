Ext.define('CRM.store.StoreTipoEmail',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDato',
    autoLoad : true,

    data : [
    {
        id : 2,
        nombre : 'Personal'
    },
    {
        id : 1,
        nombre : 'Oficial'
    }],

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json'
        }
    }
});