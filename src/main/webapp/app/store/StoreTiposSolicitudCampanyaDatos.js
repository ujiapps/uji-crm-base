Ext.define('CRM.store.StoreTiposSolicitudCampanyaDatos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDato',
    autoLoad : 'true',

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tiposolicitudcampo',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});