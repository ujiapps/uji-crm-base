Ext.define('CRM.store.StoreProgramasAccesoUser',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Programa',
    autoLoad : true,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/programa/user',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        }
    }
});