Ext.define('CRM.store.StoreTiposDatosAcceso',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDato',
    autoLoad: true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipo/acceso',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});