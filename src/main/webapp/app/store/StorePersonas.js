Ext.define('CRM.store.StorePersonas',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Persona',
    autoLoad: false,
    autoSync: false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/persona',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});