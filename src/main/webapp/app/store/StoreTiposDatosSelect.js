Ext.define('CRM.store.StoreTiposDatosSelect',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDatoOpciones',
    autoLoad: false,
    autoSync : true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipo/opciones',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});