Ext.define('CRM.store.StoreTiposMovimientos',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDato',
    autoLoad : true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipo/movimientos',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});