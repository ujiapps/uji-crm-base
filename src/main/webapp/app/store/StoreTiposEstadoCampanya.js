Ext.define('CRM.store.StoreTiposEstadoCampanya',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.TipoEstadoCampanya',
        autoLoad: false,
        autoSync: false,
        proxy: {
            type: 'rest',
            url: '/crm/rest/tipoestadocampanya',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            },

            writer: {
                type: 'json'
            }
        }
    });