Ext.define('CRM.store.StoreTiposEstadoRecibo',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.TipoDato',
        autoLoad: true,

        data: [
            {
                id: 1,
                nombre: 'Sense data de pagament'
            },
            {
                id: 2,
                nombre: 'Retornats del banc'
            },
            {
                id: 3,
                nombre: 'Pendents de enviament al banc'
            },
            {
                id: 4,
                nombre: 'Pagats'
            }],

        proxy: {
            type: 'memory',

            reader: {
                type: 'json'
            }
        }
    });