Ext.define('CRM.store.StoreItemsSeguimiento',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Item',
    autoLoad : false,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/item/campanya',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});