Ext.define('CRM.store.StoreEnviosClientesMail', {
    extend: 'Ext.data.Store',
    model: 'CRM.model.EnvioCliente',
    autoLoad: false,
    autoSync: true,
    pageSize: 50,
    remoteSort: true,
    proxy: {
        type: 'rest',
        url: '/crm/rest/enviocliente/envio',
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data',
            totalProperty: 'totalCount'
        },
        simpleSortMode: true,

        writer: {
            type: 'json'
        }
    },
    sorters: [{
        property: 'id',
        direction: 'DESC'
    }]

});