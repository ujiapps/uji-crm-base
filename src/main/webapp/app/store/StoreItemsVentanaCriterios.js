Ext.define('CRM.store.StoreItemsVentanaCriterios',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Item',
    autoLoad : false,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/item',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters :
    {
        property : 'orden',
        direction : 'ASC'
    }

});