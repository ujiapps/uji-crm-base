Ext.define('CRM.store.StoreRecibosCliente',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Recibo',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/recibo',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data',
            totalProperty : 'totalCount'
        },

        writer :
        {
            type : 'json'
        }
    }
});