Ext.define('CRM.store.StoreTiposCliente',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoCliente',
    autoLoad : true,

    data : [
    {
        id : 'F',
        nombre : 'Persona física'
    },
    {
        id : 'J',
        nombre : 'Empresa'
    } ],

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json'
        }
    }
});