Ext.define('CRM.store.StoreCampanyasAccesoUser', {
    extend: 'Ext.data.Store',
    model: 'CRM.model.Campanya',
    autoLoad: false,
    autoSync: false,
    proxy: {
        type: 'rest',
        url: '/crm/rest/campanya/user',

        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data'
        },

        writer: {
            type: 'json'
        }
    },

    sortOnLoad: true,
    sorters: {
        property: 'programaNombre',
        direction: 'ASC'
    }
});