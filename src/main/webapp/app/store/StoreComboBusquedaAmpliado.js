Ext.define('CRM.store.StoreComboBusquedaAmpliado',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ComboBusqueda',
    autoLoad : true,

    data : [
    {
        id : -1,
        etiqueta : 'No tiene ninguno'
    },

    {
        id : -2,
        etiqueta : 'Tiene todos'
    },
    {
        id : -3,
        etiqueta : 'Tiene alguno'
    } ],

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json'
        }
    }
});