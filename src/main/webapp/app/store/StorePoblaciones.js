Ext.define('CRM.store.StorePoblaciones', {
    extend: 'Ext.data.Store',
    model: 'CRM.model.Poblacion',
    autoLoad: false,
    proxy: {
        type: 'rest',
        url: '/crm/rest/poblacion',

        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data'
        },

        writer: {
            type: 'json'
        }
    },
    sortOnLoad: true,
    sorters: {
        property: 'nombre',
        direction: 'ASC'
    }

});