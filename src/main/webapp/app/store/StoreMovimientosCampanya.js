Ext.define('CRM.store.StoreMovimientosCampanya',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Movimiento',
    autoLoad : false,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/movimiento',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        }
    },

    sortOnLoad : true,
    sorters :
    {
        property : 'fecha',
        direction : 'ASC'
    }

});