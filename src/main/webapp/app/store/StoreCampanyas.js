Ext.define('CRM.store.StoreCampanyas',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Campanya',
    autoLoad : true,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanya',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters :
    {
        property : 'programaNombre',
        direction : 'ASC'
    }

});