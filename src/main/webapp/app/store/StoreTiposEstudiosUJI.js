Ext.define('CRM.store.StoreTiposEstudiosUJI',
{
    extend : 'Ext.data.Store',
    fields: ['id', 'nombre'],

    data: [{
        id: 'G',
        nombre: 'Grau'
    }, {
        id: '120C',
        nombre: '+120 Créditos'
    },{
        id: 'MO',
        nombre: 'Master Oficial'
    }, {
        id: 'MP',
        nombre: 'Master Propi'
    }, {
        id: 'MC',
        nombre: 'Curs d\'especializació i d\'expert'
    }, {
        id: 'D',
        nombre: 'Doctorats'
    }, {
        id: 'M55',
        nombre: 'Escola per a majors'
    }, {
        id: 'O',
        nombre: 'Intercanvi'
    }]
});