Ext.define('CRM.store.StoreDatoTiposById',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteDatoTipo',
    autoLoad : false,
    autoSync : false,
    sorters : [ 'nombre' ],
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipo',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});