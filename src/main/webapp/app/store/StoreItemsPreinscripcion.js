Ext.define('CRM.store.StoreItemsPreinscripcion', {
    extend: 'Ext.data.Store',
    model: 'CRM.model.Item',
    autoLoad: false,
    autoSync: false,
    grupoId:-1,

    proxy: {
        type: 'rest',
        url: '/crm/rest/item/preinscripcion',

        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data'
        },

        writer: {
            type: 'json'
        }
    },

    sortOnLoad: true,
    sorters: {
        property: 'orden',
        direction: 'ASC'
    },

    constructor: function () {
        this.callParent(arguments);
        if(this.grupoId!=-1){
            this.proxy.extraParams={grupoId:this.grupoId};
        }
    }

});