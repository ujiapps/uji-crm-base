Ext.define('CRM.store.StoreItemsCampanya',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Item',
    autoLoad : false,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/campanya/0/item',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters :
    {
        property : 'orden',
        direction : 'ASC'
    }

});