Ext.define('CRM.store.StoreEnviosImagenes',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.EnvioImagen',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/envioimagen',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});