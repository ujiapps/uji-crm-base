Ext.define('CRM.store.StoreUbicaciones',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Ubicacion',
    autoLoad : true,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/ubicacion',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});