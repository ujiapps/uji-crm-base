Ext.define('CRM.store.StoreCartasImagenes',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.CartaImagen',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/cartaimagen',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});