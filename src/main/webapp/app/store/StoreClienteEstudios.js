Ext.define('CRM.store.StoreClienteEstudios', {
    extend: 'Ext.data.Store',
    model: 'CRM.model.ClienteEstudio',
    autoLoad: false,
    autoSync: false,
    proxy: {
        type: 'rest',
        url: '/crm/rest/estudio',

        reader:
            {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            },
        writer:
            {
                type: 'json'
            }
    }

});