Ext.define('CRM.store.StorePersonaPasPdi',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.PersonaPasPdi',
   // autoLoad: false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/personapaspdi',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});