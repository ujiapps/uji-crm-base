Ext.define('CRM.store.StoreTiposFecha',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDato',
    autoLoad : true,

    data : [
    {
        id : 1,
        nombre : 'Mes'
    },
    {
        id : 2,
        nombre : 'Año'
    },
    {
        id : 3,
        nombre : 'Fecha Completa'
    } ],

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json'
        }
    }
});