Ext.define('CRM.store.StoreEnvios',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Envio',
    autoLoad : true,
    autoSync : false,
    sortOnLoad : true,
    pageSize : 10,
    sorters : [
    {
        property : 'fechaEnvioDestinatario',
        direction : 'DESC'
    },
    {
        property : 'fecha',
        direction : 'DESC'
    } ],
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/envio',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data',
            totalProperty : 'totalCount'
        },

        extraParams :
        {
            busqueda : null,
            fechaDesde : null,
            fechaHasta : null,
            tipoEnvioId : null,
            persona : null,
            email : null
        },


        writer :
        {
            type : 'json'
        }
    }

});