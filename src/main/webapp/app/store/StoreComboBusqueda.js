Ext.define('CRM.store.StoreComboBusqueda',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ComboBusqueda',
    autoLoad : true,

    data : [
    {
        id : -1,
        etiqueta : 'No tiene'
    },
    {
        id : -2,
        etiqueta : 'Tiene'
    } ],

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json'
        }
    }
});