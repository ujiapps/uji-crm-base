Ext.define('CRM.store.StoreServiciosByUsuarioId',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Servicio',
    autoLoad : true,
    autoSync: true,
    
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/servicio/usuario',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});