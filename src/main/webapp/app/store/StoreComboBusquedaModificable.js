Ext.define('CRM.store.StoreComboBusquedaModificable',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ComboBusqueda',
    autoLoad : false,
    autoSync : false,
    sorters : [ 'etiqueta' ],
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipo/opciones/',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});