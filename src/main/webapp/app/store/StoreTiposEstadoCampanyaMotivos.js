Ext.define('CRM.store.StoreTiposEstadoCampanyaMotivos',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.TipoEstadoCampanyaMotivo',
        autoLoad: false,
        autoSync: false,

        proxy: {
            type: 'rest',
            url: '/crm/rest/tipoestadocampanyamotivo',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data',
                totalProperty: 'totalCount'
            },

            writer: {
                type: 'json'
            }
        }
    });