Ext.define('CRM.store.StorePerfiles',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Perfil',
    autoLoad : true,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/perfil',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sortOnLoad : true,
    sorters :
    {
        property : 'nombre',
        direction : 'ASC'
    }
});