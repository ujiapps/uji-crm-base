Ext.define('CRM.store.StoreTiposEstudiosNoUJI',
{
    extend : 'Ext.data.Store',
    fields: ['id', 'nombre'],

    data: [{
        id: 1,
        nombre: 'Grau'
    }, {
        id: 2,
        nombre: 'PostGrau'
    }, {
        id: 3,
        nombre: 'Doctorat'
    }, {
        id: 4,
        nombre: 'Elemental'
    }, {
        id: 5,
        nombre: 'Mitjans'
    }, {
        id: 6,
        nombre: 'Superiors no Universitaris'
    }]
});