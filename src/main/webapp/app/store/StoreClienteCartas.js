Ext.define('CRM.store.StoreClienteCartas',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteCarta',
    autoLoad : false,
    autoSync : true,
    sortOnLoad : true,
    pageSize: 25,
    sorters : [
    {
        property : 'fechaCreacion',
        direction : 'DESC'
    }],
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/clientecarta/',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data',
            totalProperty: 'totalCount'
        },

        writer :
        {
            type : 'json'
        }
    }

});