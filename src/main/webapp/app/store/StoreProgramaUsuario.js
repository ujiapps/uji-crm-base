Ext.define('CRM.store.StoreProgramaUsuario',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ProgramaUsuario',
    autoLoad: false,
    autoSync: false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/programausuario',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});