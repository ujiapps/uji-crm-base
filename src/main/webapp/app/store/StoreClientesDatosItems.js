Ext.define('CRM.store.StoreClientesDatosItems',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteDatoExtra',
    autoLoad : false,
    autoSync : true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/itemdato',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});