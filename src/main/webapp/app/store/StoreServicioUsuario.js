Ext.define('CRM.store.StoreServicioUsuario',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ServicioUsuario',
    autoLoad: false,
    autoSync: false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/serviciousuario',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});