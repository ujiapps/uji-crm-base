Ext.define('CRM.store.StoreLineasRecibo',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Linea',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/linea',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data',
            totalProperty : 'totalCount'
        },

        writer :
        {
            type : 'json'
        }
    }
});