Ext.define('CRM.store.StoreClienteItems',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteItem',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/clienteitem',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});