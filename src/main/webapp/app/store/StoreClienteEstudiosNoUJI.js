Ext.define('CRM.store.StoreClienteEstudiosNoUJI',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ClienteEstudioNoUJI',
    autoLoad : false,
    autoSync : false,
    pageSize: 25,
    sortOnLoad: true,
    sorters: [{
        property: 'nombre',
        direction: 'DESC'
    }],
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/estudionouji',

        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data',
            totalProperty: 'totalCount'
        },
        simpleSortMode: true,

        writer :
        {
            type : 'json'
        }
    }

});