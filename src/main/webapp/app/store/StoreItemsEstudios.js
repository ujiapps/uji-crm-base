Ext.define('CRM.store.StoreItemsEstudios', {
    extend: 'Ext.data.Store',
    model: 'CRM.model.ItemEstudio',
    autoLoad: false,
    autoSync: false,

    proxy: {
        type: 'rest',
        url: '/crm/rest/itemclasificacion',

        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data'
        },

        writer: {
            type: 'json'
        }
    },

    sortOnLoad: true,
    sorters: {
        property: 'orden',
        direction: 'ASC'
    }

});