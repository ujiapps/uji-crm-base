Ext.define('CRM.store.StoreProgramaPerfiles',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.ProgramaPerfil',
    autoLoad : false,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/crm/rest/programaperfil',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});