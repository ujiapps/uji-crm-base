Ext.define('CRM.store.StoreTiposTarifas',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoTarifa',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/tipotarifa',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});