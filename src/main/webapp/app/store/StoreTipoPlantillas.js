Ext.define('CRM.store.StoreTipoPlantillas',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.TipoDato',
    autoLoad : true,

    data : [
    {
        id : 1,
        nombre : 'sauji'
    }],

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json'
        }
    }
});