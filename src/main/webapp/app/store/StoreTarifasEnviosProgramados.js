Ext.define('CRM.store.StoreTarifasEnviosProgramados',
    {
        extend: 'Ext.data.Store',
        model: 'CRM.model.TipoTarifaEnvioProgramado',
        autoLoad: false,
        autoSync: false,
        proxy: {
            type: 'rest',
            url: '/crm/rest/tipotarifaenvioprogramado',

            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data'
            },

            writer: {
                type: 'json'
            }
        }

    });