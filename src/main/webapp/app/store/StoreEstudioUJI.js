Ext.define('CRM.store.StoreEstudioUJI',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.EstudioUJI',
    autoLoad : false,
    autoSync : false,

    proxy :
    {
        type : 'rest',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});