Ext.define('CRM.store.StoreClientesByItemId',
{
    extend : 'Ext.data.Store',
    model : 'CRM.model.Cliente',
    autoLoad : false,
    autoSync : true,
    proxy :
    {
        type : 'rest',
        url : '/crm/rest/cliente',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});