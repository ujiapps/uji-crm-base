Ext.define('CRM.controller.ControllerPanelClientesEtiquetas',
    {
        extend: 'Ext.app.Controller',
        models: ['ClienteEtiqueta'],
        views: ['clientes.GridClientesEtiquetas'],

        refs: [{
            selector: 'gridClientesEtiquetas',
            ref: 'gridClientesEtiquetas'
        }, {
            selector: 'gridClientesEtiquetas button[name=borrar]',
            ref: 'botonBorrarClienteEtiqueta'

        }],

        dirty: false,

        init: function () {

            this.control({
                'gridClientesEtiquetas button[name=anyadir]': {
                    click: this.anyadirClienteEtiqueta
                },
                'gridClientesEtiquetas combobox': {
                    'keyup': function (f, e) {
                        if (e.getCharCode() == 13) {
                            this.anyadirClienteEtiqueta();
                        }
                    }
                },
                'gridClientesEtiquetas button[name=borrar]': {
                    click: this.borrarClienteEtiqueta
                },
                'gridClientesEtiquetas': {
                    select: this.setEstadoComponentes
                }
            });
        },

        dameStoreEtiquetas: function () {
            var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
            var clienteId = tabsServiciosCliente.getActiveTab().itemId;
            return tabsServiciosCliente.down("[itemId=" + clienteId + "]").down("[itemId=gridEtiquetas-" + clienteId + "]").getStore();
        },


        anyadirClienteEtiqueta: function () {
            var clienteId = this.getController('ControllerPanelClientes').dameIdCliente();
            var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
            var combo = tabsServiciosCliente.getActiveTab().down("combo[name=comboAnyadirEtiqueta"+clienteId+"]");

            var etiqueta = combo.getRawValue();

            if (!etiqueta.length) {
                Ext.Msg.show({
                    title: 'Error',
                    msg: 'No es poden afegir etiquetes buides',
                    width: 300,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR
                });
                return;
            }

            var clienteEtiqueta = Ext.ModelManager.create({
                clienteId: clienteId,
                etiquetaNombre: etiqueta
            }, "CRM.model.ClienteEtiqueta");

            var store = this.dameStoreEtiquetas();
            store.add(clienteEtiqueta);
            store.sync({
                scope: this,
                callback: function () {
                    this.dameStoreEtiquetas().reload();
                }
            });

            combo.setValue('');
        },

        borrarClienteEtiqueta: function () {
            var ref = this;
            var seguimiento = this.getGridClientesEtiquetas().getSelectionModel().getSelection();

            if (seguimiento.length == 1) {
                Ext.Msg.confirm('Esborrar', 'Esteu segur/a de voler esborrar el registre sel·leccionat?', function (btn) {
                    if (btn == 'yes') {
                        var storeClienteEtiqueta = ref.dameStoreEtiquetas();
                        storeClienteEtiqueta.remove(seguimiento[0]);
                        ref.getBotonBorrarClienteEtiqueta().setDisabled(true);
                        storeClienteEtiqueta.sync();
                    }
                });
            }
        },

        // rellenaGridClientesEtiquetas: function (clienteId) {
        //     var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
        //     var store = tabsServiciosCliente.down("[itemId=" + clienteId + "]").down("[itemId=gridEtiquetas-" + clienteId + "]").getStore();
        //
        //     store.load({
        //         params: {
        //             clienteId: clienteId
        //         },
        //         scope: this,
        //         callback: function () {
        //             this.setEstadoComponentes();
        //         }
        //     });
        // },

        setEstadoComponentes: function () {
            var tieneSeleccionGridClientesEtiquetas = this.getGridClientesEtiquetas().getSelectionModel().hasSelection();
            this.getBotonBorrarClienteEtiqueta().setDisabled(!tieneSeleccionGridClientesEtiquetas);
        }

    });