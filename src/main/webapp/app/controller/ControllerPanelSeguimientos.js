Ext.define('CRM.controller.ControllerPanelSeguimientos', {
    extend: 'Ext.app.Controller',
    stores: ['StoreCampanyaClientes', 'StoreTiposCampanyasClientes', 'StoreCampanyaSeguimientos', 'StoreSeguimientoTipos', 'StoreCampanyaFases', 'StoreSeguimientoFicheros',
        'StoreEnviosSeguimientos', 'StoreEtiquetasSeguimiento', 'StoreItemsSeguimiento', 'StoreSeguimientosCampanya', 'StoreComboBusquedaAmpliado',
        'StoreClientesAnyadirACampanya', 'StoreComboBusqueda', 'StoreInfoEntradas', 'StoreCampanyaActos', 'StoreInfoEntradasCRM', 'StorePersonasFicheroCsv'],
    models: ['CampanyaCliente', 'Seguimiento'],

    views: ['seguimientos.FiltroCampanyaClientes', 'seguimientos.VentanaSeguimientosClienteCampanya', 'seguimientos.anyadirACampanya.VentanaAnyadirClientesACampanya',
        'seguimientos.actos.PanelActos', 'seguimientos.GridCampanyaClientes', 'seguimientos.TabSeguimientos', 'seguimientos.FiltroCampanyaFases', 'seguimientos.GridCampanyaSeguimientos',
        'seguimientos.GridSeguimientoFicheros', 'seguimientos.TreeCampanyas', 'seguimientos.anyadirACampanya.VentanaAnyadirClientesACampanyaCsv'],

    ventanaSeguimientosCliente: {},
    ventanaAnyadirClientesACampanya: {},
    ventanaAnyadirClientesACampanyaCsv: {},
    campanya: "",

    getVentanaSeguimientosCliente: function () {
        return this.getView('seguimientos.VentanaSeguimientosClienteCampanya').create();
    },

    getVentanaAnyadirClientesACampanya: function () {
        return this.getView('seguimientos.anyadirACampanya.VentanaAnyadirClientesACampanya').create();
    },

    // getVentanaAnyadirClientesACampanyaCsv: function () {
    //     return
    // },

    refs: [{
        selector: 'panelSeguimientos',
        ref: 'panelSeguimientos'
    },
        {
            selector: 'tabSeguimientos',
            ref: 'tabSeguimientos'
        },
        {
            selector: 'filtroCampanyaClientes',
            ref: 'filtroCampanyaClientes'
        },
        {
            selector: 'gridCampanyaClientes',
            ref: 'gridCampanyaClientes'
        },
        {
            selector: 'gridCampanyaClientes button[name=borrarCampanyaCliente]',
            ref: 'botonBorrarCampanyaCliente'
        },
        {
            selector: 'gridCampanyaClientes combobox[name=comboCampanyaClienteTipoEditor]',
            ref: 'comboCampanyaClienteTipoEditor'
        },
        {
            //    selector: 'gridCampanyaClientes combobox[name=comboAnyadirColumna]',
            //    ref: 'comboAnyadirColumna'
            //},
            //{
            selector: 'filtroCampanyaClientes combo[name=comboEtiqueta]',
            ref: 'filtroComboEtiqueta'
        },
        {
            selector: 'filtroCampanyaClientesAvanzado combo[name=comboItems]',
            ref: 'filtroComboItems'
        },
        {
            selector: 'filtroCampanyaClientesAvanzado combo[name=busquedaComboItemsOpcion]',
            ref: 'filtroComboItemsOpcion'
        },
        {
            selector: 'filtroCampanyaClientesAvanzado combo[name=busquedaSeguimientoTipo]',
            ref: 'filtroComboSeguimientoTipo'
        },
        {
            selector: 'filtroCampanyaClientesAvanzado combo[name=busquedaSeguimientoTipoOpcion]',
            ref: 'filtroComboSeguimientoTipoOpcion'
        },
        {
            selector: 'filtroCampanyaFases combo[name=campanyaFases]',
            ref: 'filtroComboCampanyaFases'
        },
        {
            selector: 'gridCampanyaSeguimientos',
            ref: 'gridCampanyaSeguimientos'
        },
        {
            selector: 'gridCampanyaSeguimientos button[name=borrarCampanyaSeguimiento]',
            ref: 'botonBorrarCampanyaSeguimiento'
        },
        {
            selector: 'formCampanyaSeguimientos',
            ref: 'formCampanyaSeguimientos'
        },
        {
            selector: 'tabCampanyaSeguimientos [name=panelEnvios]',
            ref: 'panelEnvios'
        },
        {
            selector: 'panelCampanyaSeguimientos',
            ref: 'panelCampanyaSeguimientos'
        },
        {
            selector: 'tabCampanyaSeguimientos form[name=formSeguimientoFicheros]',
            ref: 'formSeguimientoFicheros'
        },
        {
            selector: 'gridSeguimientoFicheros',
            ref: 'gridSeguimientoFicheros'
        },
        {
            selector: 'gridSeguimientoFicheros button[name=borrar]',
            ref: 'botonBorrarSeguimientoFichero'
        },
        {
            selector: 'gridSeguimientoFicheros button[name=descargar]',
            ref: 'botonDescargarSeguimientoFichero'
        },
        {
            selector: 'anyadirACampanyaPanelClientes [name=filtrosBusquedaSeguimientos]',
            ref: 'filtroBusqueda'
        },
        {
            selector: 'anyadirACampanyaPanelClientes grid[name=anyadirACampanyaGridClientes]',
            ref: 'gridBusquedaClientes'
        },
        {
            selector: 'anyadirACampanyaPanelClientes combobox[name=campanyaEstadoDestinoId]',
            ref: 'comboTipoEstadoDestino'
        },
        {
            selector: 'ventanaSeguimientosClienteCampanya grid[name=gridVentanaSeguimientosClienteCampanya]',
            ref: 'ventanaGridSeguimientosClienteCampanya'
        },
        {
            selector: 'ventanaSeguimientosClienteCampanya button[name=borrarSeguimientoCampanyaCliente]',
            ref: 'borrarSeguimientoCampanyaCliente'
        },
        //    {
        //        selector : 'gridSeguimientosClienteCampanya',
        //        ref : 'ventanaGridSeguimientosClienteCampanya'
        //    },
        {
            selector: 'treeCampanyas',
            ref: 'treeCampanyas'
        },
        {
            selector: 'panelActos textfield[name=numEntradasNuevas]',
            ref: 'panelActosNumEntradasNuevas'
        },
        {
            selector: 'panelActos combobox[name=comboActos]',
            ref: 'comboActos'
        },
        {
            selector: 'panelActos grid[name=gridInfoEntradas]',
            ref: 'gridInfoEntradas'
        },
        {
            selector: 'panelActos grid[name=gridInfoEntradasCRM]',
            ref: 'gridInfoEntradasCRM'
        }],

    dirty: false,

    init: function () {

        this.control({
            'panelSeguimientos': {
                render: function () {
                    this.getStoreCampanyaClientesStore().loadData([], false);
                }
            },
            'treeCampanyas': {
                select: this.onCampanyaSelectedTree
            },

            'filtroCampanyaClientes textfield[name=busqueda]': {
                specialkey: function (field, e) {
                    if (e.getKey() == e.ENTER) {
                        this.buscarCampanyaClientes();
                    }
                }
            },
            'filtroCampanyaClientes combo[name=comboEtiqueta]': {
                specialkey: function (field, e) {
                    if (e.getKey() == e.ENTER) {
                        this.buscarCampanyaClientes();
                    }
                }
            },
            'filtroCampanyaClientesAvanzado combo[name=comboItems]': {
                select: this.buscarCampanyaClientes
            },
            'filtroCampanyaClientes combobox[name=campanyaClienteTipoId]': {
                select: this.buscarCampanyaClientes
            },
            'filtroCampanyaClientes button[name=buscar]': {
                click: this.buscarCampanyaClientes
            },
            'filtroCampanyaClientes button[name=limpiarFiltros]': {
                click: function () {
                    this.limpiarFiltrosCampanyaClientes();
                }
            },
            'gridCampanyaClientes': {
                selectionchange: this.onCampanyaClienteSelected,
                validateedit: this.actualizarIdsFilaGrid,

                edit: function (editor, e) {
                    this.updateCampanyaCliente(editor, e);
                    this.getStoreCampanyaClientesStore().sync();
                    // editor.grid.columns[6].setVisible(true);
                },
                beforeedit: function (editor) {
                    // editor.grid.columns[6].setVisible(false);
                },
                canceledit: function (editor) {
                    // editor.grid.columns[6].setVisible(true);
                }
            },
            //
            // 'gridCampanyaClientes button[name=anyadirCampanyaCliente]': {
            //     click: this.anyadirCampanyaCliente
            // },
            'gridCampanyaClientes button[name=borrarCampanyaCliente]': {
                click: this.borrarCampanyaCliente
            },
            'gridCampanyaClientes button[name=anyadirCampanyaClienteMultiple]': {
                click: this.anyadirCampanyaClienteMultiple
            },
            'gridCampanyaClientes button[name=anyadirCampanyaClienteCsv]': {
                click: this.anyadirCampanyaClienteCsv
            },
            'gridCampanyaClientes actioncolumn[name=abrirCliente]': {
                click: function (obj, r, it, i, e, rec) {
                    this.abrirCliente(obj, rec);
                }
            },
            // 'gridCampanyaClientes actioncolumn[name=abrirClienteDuplicado]': {
            //     click: function (obj, r, it, i, e, rec) {
            //         this.abrirClienteDuplicado(obj, rec);
            //     }
            // },
            'gridCampanyaClientes actioncolumn[name=anyadirSeguimientoCliente]': {
                click: this.abrirVentanaSeguimientosCliente
            },

            'filtroCampanyaFases combo[name=campanyaFases]': {
                select: this.onCampanyaFasesSelected
            },
            'gridCampanyaSeguimientos': {
                select: this.onCampanyaSeguimientoSelected,
                validateedit: function (editor, e) {
                    this.actualizarIdsGridCampanyaSeguimiento(editor, e);
                },
                edit: function () {
                    this.getStoreCampanyaSeguimientosStore().sync();
                },
                canceledit: function () {
                    this.setEstadoComponentes();
                }
            },
            'gridCampanyaSeguimientos button[name=anyadirCampanyaSeguimiento]': {
                click: this.anyadirCampanyaSeguimiento
            },
            'gridCampanyaSeguimientos button[name=borrarCampanyaSeguimiento]': {
                click: this.borrarCampanyaSeguimiento
            },
            'formCampanyaSeguimientos button[name=guardar]': {
                click: this.guardarCampanyaSeguimiento
            },
            'formCampanyaSeguimientos button[name=borrar]': {
                click: this.borrarCampanyaSeguimiento
            },

            'gridSeguimientoFicheros button[name=subir]': {
                click: function () {
                    this.anyadirSeguimientoFichero();
                }
            },

            'gridSeguimientoFicheros': {
                select: this.setEstadoComponentes,
                itemdblclick: this.descargarSeguimientoFichero
            },
            'gridSeguimientoFicheros button[name=borrar]': {
                click: this.borrarSeguimientoFichero
            },
            'gridSeguimientoFicheros button[name=descargar]': {
                click: this.descargarSeguimientoFichero
            },
            'ventanaSeguimientosClienteCampanya button[name=anyadirSeguimientoCampanyaCliente]': {
                click: this.anyadirCampanyaClienteSeguimiento
            },

            'ventanaSeguimientosClienteCampanya grid': {
                validateedit: function (editor, e) {
                    this.actualizarIdsGridCampanyaSeguimiento(editor, e);
                },
                edit: function () {
                    this.getStoreSeguimientosCampanyaStore().sync();
                }
            },

            'ventanaSeguimientosClienteCampanya button[name=borrarSeguimientoCampanyaCliente]': {
                click: function (obj) {
                    this.borrarCampanyaClienteSeguimiento(obj.up('grid').getSelectionModel().getSelection()[0]);
                }
            },

            'ventanaSeguimientosClienteCampanya button[name=finalizarSeguimientosClienteCampanya]': {
                click: function () {
                    this.cerrarVentanaSeguimientosCliente();
                }
            },
            'anyadirACampanyaPanelClientes [name=filtrosBusquedaSeguimientos] button[name=botonBusqueda]': {
                click: function () {
                    this.realizarBusquedaClientesAnyadirCampanya();
                }
            },
            'anyadirACampanyaPanelClientes button[name=cerrarVentana]': {
                click: this.cerrarVentanaAnyadirACampanyaPanelClientes
            },
            'anyadirACampanyaPanelClientes button[name=afegirDesdeVentana]': {
                click: this.anyadirClientesACampanya
            },
            'panelActos combobox[name=comboActos]': {
                select: this.cargaGridEntradas
            },
            'panelActos button[name=asignarNumEntradasNuevas]': {
                click: this.asignarNumEntradasNuevas
            },

            'panelActos button[name=enviarNumEntradas]': {
                click: this.enviarNumEntradas
            },

            'panelActos grid[name=gridInfoEntradasCRM]': {
                edit: this.guardaNumEntradasFaltan
            }

        });
    },

    asignarNumEntradasNuevas: function () {

        Ext.Ajax.request(
            {
                url: '/crm/rest/clientedato/entradasacto',
                method: 'POST',
                params: {
                    actoId: this.getComboActos().getValue(),
                    nuevas: this.getPanelActosNumEntradasNuevas().getValue()
                },

                scope: this,
                callback: function () {
                    this.getStoreInfoEntradasCRMStore().reload();
                }

            });
    },

    guardaNumEntradasFaltan: function (editor, obj) {

        Ext.Ajax.request(
            {
                url: '/crm/rest/clientedato/entradasacto/cliente',
                method: 'POST',
                params: {
                    clienteId: obj.newValues.clienteId,
                    actoId: obj.newValues.actoId,
                    nuevas: obj.newValues.nuevas
                },
                scope: this,
                callback: function () {
                    this.getStoreInfoEntradasCRMStore().reload();
                }

            });
    },

    enviarNumEntradas: function () {
        Ext.Ajax.request(
            {
                url: '/crm/rest/entradas/todas',
                method: 'POST',
                params: {
                    actoId: this.getComboActos().getValue()
                },

                scope: this,
                callback: function () {
                    this.getStoreInfoEntradasCRMStore().reload();
                }

            });
    },

    cerrarVentanaAnyadirACampanyaPanelClientes: function () {
        this.ventanaAnyadirClientesACampanya.destroy();
    },

    realizarBusquedaClientesAnyadirCampanya: function () {
        var MIN_CARACTERES_BUSQUEDA = 3;

        var busqueda = this.getFiltroBusqueda().busqueda(MIN_CARACTERES_BUSQUEDA);

        if (busqueda != null) {
            var clientes = this.getGridBusquedaClientes().getStore();

            clientes.getProxy().extraParams = busqueda;
            clientes.load();
        } else {
            Ext.Msg.show(
                {
                    title: 'Informaciò',
                    msg: 'Has d\'introduir almenys ' + MIN_CARACTERES_BUSQUEDA + ' caràcters',
                    width: 300,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR
                });
        }
    },

    anyadirClientesACampanya: function () {
        var MIN_CARACTERES_BUSQUEDA = 3;

        var busqueda = this.getFiltroBusqueda().busqueda(MIN_CARACTERES_BUSQUEDA);

        var campanyaAnadir = this.getTreeCampanyas().getSelectionModel().getSelection()[0].data.id;
        var estadoAnyadir = this.getComboTipoEstadoDestino().getValue();

        if (busqueda != null && campanyaAnadir != null && estadoAnyadir != null) {
            var busquedaCampanya =
                {
                    campanyaAnyadir: campanyaAnadir,
                    estadoAnyadir: estadoAnyadir
                };

            var res = Ext.Object.merge(busqueda, busquedaCampanya);

            var ref = this;
            ref.getGridBusquedaClientes().setLoading(true);
            Ext.Ajax.request(
                {
                    url: '/crm/rest/campanyacliente/anyadirgridacampanya/',
                    method: 'POST',
                    params: res,

                    callback: function () {
                        ref.getStoreCampanyaClientesStore().load();
                        ref.getGridBusquedaClientes().setLoading(false);
                        ref.cerrarVentanaAnyadirACampanyaPanelClientes();
                    }

                });

        }

    },

    abrirVentanaSeguimientosCliente: function (obj, r, it, i, e, rec) {
        this.getStoreSeguimientosCampanyaStore().load(
            {
                url: '/crm/rest/seguimiento/campanya',
                params: {
                    clienteId: rec.data.clienteId,
                    campanyaId: rec.data.campanyaId
                }
            });

        this.getGridCampanyaClientes().getSelectionModel().select(rec);
        this.getVentanaSeguimientosCliente().clienteId = rec.data.clienteId;
        this.ventanaSeguimientosCliente = this.getVentanaSeguimientosCliente();
        this.ventanaSeguimientosCliente.show();
    },

    cerrarVentanaSeguimientosCliente: function () {
        this.getStoreCampanyaClientesStore().reload();
        this.ventanaSeguimientosCliente.destroy();
    },

    limpiarFiltrosCampanyaClientes: function () {
        var filtro = this.getFiltroCampanyaClientes();
        filtro.down("combobox[name=campanyaClienteTipoId]").clearValue();
        this.getFiltroComboEtiqueta().clearValue();
        filtro.down("textfield[name=busqueda]").setValue("");
        this.getFiltroComboSeguimientoTipo().clearValue();
        this.getFiltroComboSeguimientoTipoOpcion().clearValue();
        this.getFiltroComboItems().clearValue();
    },

    buscarCampanyaClientes: function () {
        var filtro = this.getFiltroCampanyaClientes();
        var busqueda = filtro.down("textfield[name=busqueda]").getValue();
        var campanyaClienteTipoId = filtro.down("combobox[name=campanyaClienteTipoId]").getValue();
        var campanyaId = this.getTreeCampanyas().getSelectionModel().getSelection()[0].data.id;
        var etiqueta = this.getFiltroComboEtiqueta().getRawValue();
        var items = this.getFiltroComboItems().getValue().join(';');
        var itemsOpcion = this.getFiltroComboItemsOpcion().getValue();
        var seguimientoTipos = this.getFiltroComboSeguimientoTipo().getValue().join(';');
        var seguimientoTipoOpcion = this.getFiltroComboSeguimientoTipoOpcion().getValue();

        this.cargaStoreCampanyaClientes(campanyaId, campanyaClienteTipoId, busqueda, etiqueta, items, itemsOpcion, seguimientoTipos, seguimientoTipoOpcion);
    },

    onCampanyaSelectedTree: function (record, item) {
        this.getTabSeguimientos().setActiveTab(0);
        var campanyaId;

        if (item.get('root'))
            campanyaId = null;
        else
            campanyaId = item.get('id');

        this.campanya = campanyaId;

        this.limpiarFiltrosCampanyaClientes();
        //this.getGridCampanyaClientes().refreshGrid();
        this.cargaStoreCampanyaFases(campanyaId);
        this.cargaStoreCampanyaClientes(campanyaId, null, null, null, null);
        this.cargaStoreItemsSeguimiento(campanyaId);
        this.cargaStoreComboActos(campanyaId);
        this.cargaStoresCombosTipoEstado(campanyaId);

        this.getStoreCampanyaSeguimientosStore().loadData([], false);
        this.getStoreInfoEntradasCRMStore().loadData([], false);

    },


    onCampanyaFasesSelected: function (combo, records) {
        var campanyaFaseId = records[0].get('id');
        this.cargaStoreCampanyaSeguimientos(campanyaFaseId);
        this.getStoreEnviosSeguimientosStore().loadData([], false);
    },

    cargaStoresCombosTipoEstado: function (campanyaId) {

        var storeFiltro = this.getFiltroCampanyaClientes().down('combobox[name=campanyaClienteTipoId]').getStore();
        storeFiltro.load({
            url: '/crm/rest/tipoestadocampanya/campanya/',
            params: {
                campanyaId: campanyaId
            }
        });
    },

    cargaStoreCampanyaClientes: function (campanyaId, campanyaClienteTipoId, busqueda, etiqueta, items, itemsOpcion, seguimientoTipo, seguimientoTipoOpcion) {
        var storeCampanyaClientes = this.getStoreCampanyaClientesStore();
        var ref = this;

        if (campanyaId == null) {
            storeCampanyaClientes.loadData([], false);
            ref.setEstadoComponentes();
            return;
        }

        storeCampanyaClientes.getProxy().extraParams =
            {
                campanyaId: campanyaId,
                campanyaClienteTipoId: campanyaClienteTipoId,
                busqueda: busqueda,
                etiqueta: etiqueta,
                items: items,
                itemsOpcion: itemsOpcion,
                seguimientoTipo: seguimientoTipo,
                seguimientoTipoOpcion: seguimientoTipoOpcion
            };

        storeCampanyaClientes.load(
            {
                callback: function () {
                    ref.setEstadoComponentes();

                },
                params: {
                    start: 0,
                    page: 0
                },
                scope: this
            });
        storeCampanyaClientes.currentPage = 1;
    },

    cargaStoreCampanyaSeguimientos: function (campanyaFaseId) {
        var storeCampanyaSeguimientos = this.getStoreCampanyaSeguimientosStore();
        var ref = this;

        if (campanyaFaseId == null) {
            storeCampanyaSeguimientos.loadData([], false);
            this.setEstadoComponentes();
            return;
        }

        storeCampanyaSeguimientos.load(
            {
                params: {
                    campanyaFaseId: campanyaFaseId
                },
                callback: function () {
                    ref.setEstadoComponentes();
                },
                scope: this
            });
    },

    cargaStoreCampanyaFases: function (campanyaId) {
        var storeCampanyaFases = this.getStoreCampanyaFasesStore();
        var ref = this;

        this.getFiltroComboCampanyaFases().setValue(null);

        if (campanyaId == null) {
            storeCampanyaFases.loadData([], false);
            ref.setEstadoComponentes();
            return;
        }

        storeCampanyaFases.load(
            {
                params: {
                    campanyaId: campanyaId
                },
                callback: function () {
                    ref.setEstadoComponentes();
                },
                scope: this
            });
    },

    cargaStoreItemsSeguimiento: function (campanyaId) {
        var storeItemsSeguimiento = this.getStoreItemsSeguimientoStore();
        var ref = this;

        if (campanyaId == null) {
            storeItemsSeguimiento.loadData([], false);
            ref.setEstadoComponentes();
            return;
        }

        storeItemsSeguimiento.load(
            {
                url: '/crm/rest/item/campanya/' + campanyaId,
                callback: function () {
                    ref.setEstadoComponentes();
                },
                scope: this
            });
    },

    cargaStoreComboActos: function (campanyaId) {

        this.getComboActos().setValue("");

        var store = this.getComboActos().getStore();
        store.load(
            {
                params: {
                    campanyaId: campanyaId
                }
            });
    },

    onCampanyaClienteSelected: function () {
        this.setEstadoComponentes();
    },

    onCampanyaSeguimientoSelected: function (panel, record) {
        this.cargaStoreSeguimientoFicheros(record.get('id'));
        this.cargaDatosEnvios(record.get('id'));
        this.setEstadoComponentes();
    },

    cargaDatosEnvios: function (campanyaSeguimientoId) {
        if (campanyaSeguimientoId != null) {
            var storeEnvios = this.getStoreEnviosSeguimientosStore();
            storeEnvios.load(
                {
                    url: '/crm/rest/envio/seguimiento/' + campanyaSeguimientoId
                });
        }
    },

    cargaStoreSeguimientoFicheros: function (campanyaSeguimientoId) {
        var store = this.getStoreSeguimientoFicherosStore();
        var ref = this;

        store.load(
            {
                params: {
                    campanyaSeguimientoId: campanyaSeguimientoId
                },
                callback: function () {
                    ref.setEstadoComponentes();
                },
                scope: this
            });
    },

    updateCampanyaCliente: function (editor, e) {
        var controllerAlta = this.getController('ControllerVentanaClienteCampanya');
        var ref = this;
        controllerAlta.abrirVentanaAsociacionPorCampanyaYCliente(
            this.getGridCampanyaClientes().getStore().findRecord('clienteId', e.record.data.clienteId),
            this.getGridCampanyaClientes().down("[name=comboCampanyaClienteTipo]").getEditor().getStore().findRecord('id', e.record.data.campanyaClienteTipoId),
            function () {
                ref.getGridCampanyaClientes().getStore().sync({
                    scope: this,
                    callback: function () {
                        ref.getGridCampanyaClientes().getStore().reload();
                        ref.getGridCampanyaClientes().setLoading(false);
                    }
                })
            });
    },

    // anyadirCampanyaCliente: function () {
    //     var storeCampanya = Ext.create('CRM.store.StoreCampanyas');
    //     storeCampanya.load(
    //         {
    //             id: this.getTreeCampanyas().getSelectionModel().getSelection()[0].data.id,
    //             callback: function (records) {
    //                 var controllerAlta = this.getController('ControllerVentanaClienteCampanya');
    //                 controllerAlta.abrirVentanaAsociacionPorCampanya(records[0]);
    //             },
    //             scope: this
    //         });
    // },

    //anyadirColumna: function () {
    //    this.getComboAnyadirColumna().setVisible(true);
    //},
    //
    //anyadirColumnaTipo: function (combo, select) {
    //    var grid = this.getGridCampanyaClientes();
    //    grid.addColumn(select[0].data.id);
    //},
    //
    anyadirCampanyaClienteMultiple: function () {
        this.ventanaAnyadirClientesACampanya = this.getVentanaAnyadirClientesACampanya();
        this.getGridBusquedaClientes().getStore().loadData([], false);
        this.ventanaAnyadirClientesACampanya.show();
    },

    anyadirCampanyaClienteCsv: function () {
        var campanya =
            this.ventanaAnyadirClientesACampanyaCsv = this.getView('seguimientos.anyadirACampanya.VentanaAnyadirClientesACampanyaCsv').create({
                campanya: this.campanya,
                gridCampanyaClientes: this.getGridCampanyaClientes()
            });
        this.ventanaAnyadirClientesACampanyaCsv.down("grid[name=gridUsuariosFicheroCsv]").getStore().getProxy().extraParams = {campanya: this.campanya};
        this.ventanaAnyadirClientesACampanyaCsv.down("grid[name=gridUsuariosFicheroCsv]").getStore().load();
        this.ventanaAnyadirClientesACampanyaCsv.show();
    },

    borrarCampanyaCliente: function () {
        var store = this.getStoreCampanyaClientesStore();
        var fila = this.getGridCampanyaClientes().getSelectionModel().getSelection()[0];
        var camapanyaCliente = store.findRecord('id', fila.data.id);

        Ext.Msg.confirm('Eliminar el client', 'El client s\'eliminarà de la campanya. Estàs segur de voler continuar?', function (btn) {
            if (btn == 'yes') {
                store.remove(camapanyaCliente);
                store.sync(
                    {
                        failure: function () {
                            store.rejectChanges();
                            Ext.Msg.alert('Clients', '<b>No s\'ha potgut eliminar el client</b>');
                        }
                    });
            }
        });
    },

    abrirCliente: function (objeto, record) {
        var newPanel = objeto.up("viewport").addNewTab('CRM.view.clientes.PanelClientes');
        this.getController("ControllerPanelClientes").rellenaPestanyasClientes(record.data.clienteGeneralId, record);
        // this.getController("ControllerPanelClientes").busquedaPorId(record.data.clienteGeneralId);
    },

    // abrirClienteDuplicado: function (objeto, record) {
    //     var identificacion = record.data.identificacion;
    //     var newPanel = objeto.up("viewport").addNewTab('CRM.view.clientes.PanelClientes');
    //     this.getController("ControllerPanelClientes").ponerBusqueda(identificacion);
    //     this.getController("ControllerPanelClientes").busqueda();
    // },

    abrirClienteBusqueda: function (objeto) {
        var campanyaPadre = this.getTreeCampanyas().getSelectionModel().getSelection()[0].data.parentId;
        var campanyaHija = this.getTreeCampanyas().getSelectionModel().getSelection()[0].data.id;
        //        var newPanel = objeto.up("viewport").addNewTab('CRM.view.clientes.PanelClientes');
        //        this.getController("ControllerPanelClientes").ponerBusquedaCampanya(campanyaPadre, campanyaHija);
        //        this.getController("ControllerPanelClientes").busqueda();
    },

    anyadirCampanyaClienteSeguimiento: function () {
        var store = this.getStoreSeguimientosCampanyaStore();
        var seguimiento = this.getSeguimientoModel().create(
            {
                fecha: new Date(),
                clienteId: this.getGridCampanyaClientes().getSelectionModel().getSelection()[0].data.clienteId,
                campanyaId: this.getTreeCampanyas().getSelectionModel().getSelection()[0].data.id
            });

        var rowEditor = this.getVentanaGridSeguimientosClienteCampanya().getPlugin();
        rowEditor.cancelEdit();
        store.insert(0, seguimiento);
        rowEditor.startEdit(0, 0);
    },

    borrarCampanyaClienteSeguimiento: function (fila) {
        var store = this.getStoreSeguimientosCampanyaStore();
        var rec = store.findRecord('id', fila.data.id);
        var ref = this;

        Ext.Msg.confirm('Eliminar el seguimient', 'El seguiment s\'eliminarà del client. Estàs segur de voler continuar?', function (btn) {
            if (btn == 'yes') {
                store.remove(rec);
                store.sync(
                    {
//                    success : function()
//                    {
////                        ref.setEstadoComponentesVentanaSeguimientos(false);
//                    },
                        failure: function () {
                            Ext.Msg.alert('Esborrar Seguimient', 'No s`ha pogut esborrar el seguiment.');
                            store.rejectChanges();
//                        ref.setEstadoComponentesVentanaSeguimientos(false);
                        }
                    });
            }
        });
    },

    anyadirCampanyaSeguimiento: function () {
        var store = this.getStoreCampanyaSeguimientosStore();
        var campanyaSeguimiento = Ext.ModelManager.create(
            {
                campanyaFaseId: this.getFiltroComboCampanyaFases().getValue(),
                fecha: new Date()
            }, 'CRM.model.CampanyaSeguimiento');

        var rowEditor = this.getGridCampanyaSeguimientos().getPlugin();
        rowEditor.cancelEdit();
        store.insert(0, campanyaSeguimiento);
        rowEditor.startEdit(0, 0);

        this.getStoreEnviosSeguimientosStore().loadData([], false);

    },

    borrarCampanyaSeguimiento: function () {
        var store = this.getStoreCampanyaSeguimientosStore();
        var fila = this.getGridCampanyaSeguimientos().getSelectionModel().getSelection()[0];
        var rec = store.findRecord('id', fila.data.id);
        var ref = this;

        Ext.Msg.confirm('Eliminar el seguimient', 'El seguiment s\'eliminarà de la campanya. Estàs segur de voler continuar?', function (btn) {
            if (btn == 'yes') {
                store.remove(rec);
                store.sync(
                    {
                        success: function () {
                            ref.setEstadoComponentes();
                        },
                        failure: function () {
                            Ext.Msg.alert('Esborrar Seguimient', 'No s`ha pogut esborrar el seguiment perquè té registres associats.');
                            store.rejectChanges();
                            ref.setEstadoComponentes();
                        }
                    });
            }
        });
    },

    actualizarIdsGridCampanyaSeguimiento: function (editor, e) {
        var fila = editor.grid.getSelectionModel().getSelection()[0];

        if (fila.data.seguimientoTipoNombre != e.newValues.seguimientoTipoNombre) {
            fila.data.seguimientoTipoId = e.newValues.seguimientoTipoNombre;
        }
    },

    borrarSeguimientoFichero: function () {
        var store = this.getStoreSeguimientoFicherosStore();
        var fila = this.getGridSeguimientoFicheros().getSelectionModel().getSelection()[0];
        var rec = store.findRecord('id', fila.data.id);
        var ref = this;

        Ext.Msg.confirm('Eliminar el fitxer', 'El fitxer s\'eliminarà. Estàs segur de voler continuar?', function (btn) {
            if (btn == 'yes') {
                store.remove(rec);

                store.sync(
                    {
                        success: function () {
                            ref.setEstadoComponentes();
                        }
                    });
            }
        });
    },

    anyadirSeguimientoFichero: function () {
        var ref = this;
        var seguimientoId = this.getGridCampanyaSeguimientos().getSelectionModel().getSelection()[0].data.id;
        var form = this.getFormSeguimientoFicheros();
        form.submit(
            {
                url: '/crm/rest/seguimientofichero/',
                params: {
                    campanyaSeguimientoId: seguimientoId
                },
                success: function () {
                    ref.getStoreSeguimientoFicherosStore().reload();
                },
                failure: function () {
                    Ext.Msg.alert('Insercció de fitxers', '<b>El fitxer no ha pogut ser insertat correctament</b>');
                }
            });
    },

    descargarSeguimientoFichero: function () {
        null;
        //var fila = this.getGridSeguimientoFicheros().getSelectionModel().getSelection()[0];
        //var seguimientoFicheroId = fila.data.id;
        //var url = '/crm/rest/seguimientofichero/' + seguimientoFicheroId + '/download';
        //this.descargaFichero(url);
    },

    descargaFichero: function (url) {
        var body = Ext.getBody();

        var frame = body.getById('hiddenform-iframe');
        if (frame == null) {
            body.createChild(
                {
                    tag: 'iframe',
                    cls: 'x-hidden',
                    id: 'hiddenform-iframe',
                    name: 'iframe'
                });
        }

        var form = body.getById('hiddenform-form');
        if (form == null) {
            form = body.createChild(
                {
                    tag: 'form',
                    cls: 'x-hidden',
                    id: 'hiddenform-form',
                    target: 'iframe',
                    method: 'get'
                });
        }
        form.set(
            {
                action: url
            });
        form.dom.submit();
    },

    guardarCampanyaSeguimiento: function () {
        var form = this.getFormCampanyaSeguimientos().getForm();

        if (form.isValid()) {
            var campanyaSeguimiento;
            var store = this.getStoreCampanyaSeguimientosStore();
            var campanyaSeguimientoId = form.findField('id').getValue();
            var ref = this;

            if (campanyaSeguimientoId === '') {
                campanyaSeguimiento = Ext.ModelManager.create({}, 'CRM.model.CampanyaSeguimiento');
                store.add(campanyaSeguimiento);
            } else {
                campanyaSeguimiento = store.findRecord('id', campanyaSeguimientoId);
            }

            campanyaSeguimiento.set(form.getValues(false, false, false, true));

            store.sync(
                {
                    success: function () {
                        store.reload();
                        ref.setEstadoComponentes();
                    }
                });
        }
    },

    cargaGridEntradas: function (ref, select) {
        var actoId = select[0].data.id;
        var store = this.getGridInfoEntradas().getStore();
        store.load({
            url: '/crm/rest/entradas/infoentradas/' + actoId
        });

        var storeCrm = this.getStoreInfoEntradasCRMStore();
        storeCrm.load({
            url: '/crm/rest/entradas/crm/' + actoId
        })
    },

    actualizarIdsFilaGrid: function (editor, e) {
        var fila = editor.grid.getSelectionModel().getSelection()[0];

        if (fila.data.campanyaClienteTipoNombre != e.newValues.campanyaClienteTipoNombre) {
            fila.data.campanyaClienteTipoId = e.newValues.campanyaClienteTipoNombre;
        }

        if (fila.data.clienteNombre != e.newValues.clienteNombre) {
            fila.data.clienteId = e.newValues.clienteNombre;
        }
    },

    setEstadoComponentes: function () {
        var valorTreeCampanya = this.getTreeCampanyas().getSelectionModel().getSelection()[0].data.root;
        this.getTabSeguimientos().setDisabled(valorTreeCampanya);

        var valorFiltroCampanyaFases = this.getFiltroComboCampanyaFases().getValue();
        this.getGridCampanyaSeguimientos().setDisabled(valorFiltroCampanyaFases == null);

        var tieneSeleccionGridCampanyaClientes = this.getGridCampanyaClientes().getSelectionModel().hasSelection();
        this.getBotonBorrarCampanyaCliente().setDisabled(!tieneSeleccionGridCampanyaClientes);

        var tieneSeleccionGridCampanyaSeguimientos = this.getGridCampanyaSeguimientos().getSelectionModel().hasSelection();
        this.getBotonBorrarCampanyaSeguimiento().setDisabled(!tieneSeleccionGridCampanyaSeguimientos);
        this.getFormSeguimientoFicheros().setDisabled(!tieneSeleccionGridCampanyaSeguimientos);
        this.getGridSeguimientoFicheros().setDisabled(!tieneSeleccionGridCampanyaSeguimientos);
        this.getPanelEnvios().setDisabled(!tieneSeleccionGridCampanyaSeguimientos);

        var tieneSeleccionGridSeguimientoFicheros = this.getGridSeguimientoFicheros().getSelectionModel().hasSelection();
        this.getBotonBorrarSeguimientoFichero().setDisabled(!tieneSeleccionGridSeguimientoFicheros);
        this.getBotonDescargarSeguimientoFichero().setDisabled(!tieneSeleccionGridSeguimientoFicheros);
    }
//
//    setEstadoComponentesVentanaSeguimientos : function(estado)
//    {
//        this.getBorrarSeguimientoCampanyaCliente().setDisabled(estado);
//    }

});