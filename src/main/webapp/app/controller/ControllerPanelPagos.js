Ext.define('CRM.controller.ControllerPanelPagos',
    {
        extend: 'Ext.app.Controller',
        stores: ['StoreTarifas', 'StoreFicherosDevoluciones', 'StoreRecibos', 'StoreCampanyasAccesoUserConTarifas', 'StoreTiposEstadoRecibo', 'StoreTarifasEnvios',
            'StoreTarifasEnviosProgramados', 'StoreLineasRecibo', 'StoreMovimientosRecibo', 'StoreCampanyasTarifa'],
        models: ['TipoTarifa', 'Tarifa', 'TipoTarifaEnvio', 'TipoTarifaEnvioProgramado'],
        requires: ['CRM.model.enums.TiposFechas', 'CRM.model.enums.TiposComparaciones'],
        views: ['pagos.tarifas.GridTarifas', 'pagos.tarifas.GridTiposTarifas', 'pagos.TabPanelPagos', 'pagos.recibos.PanelFiltroRecibos', 'pagos.tarifas.GridTarifasEnvios',
            'pagos.tarifas.PanelTarifaEnvios', 'pagos.tarifas.GridEnviosProgramados', 'pagos.recibos.GridRecibos', 'pagos.recibos.VentanaRecibo', 'pagos.recibos.GridLineasRecibo', 'pagos.recibos.GridMovimientosRecibo'],

        ventanaRecibo: {},

        getVentanaRecibo: function () {
            return this.getView('pagos.recibos.VentanaRecibo').create();
        },

        refs: [
            {
                selector: 'gridTiposTarifas',
                ref: 'gridTiposTarifas'
            },
            {
                selector: 'gridTiposTarifas button[name=borrarTipoTarifa]',
                ref: 'botonBorrarTipoTarifa'
            },
            {
                selector: 'gridTiposTarifas combo[name=comboCampanyasTarifas]',
                ref: 'comboCampanya'
            },
            {
                selector: 'gridTarifas',
                ref: 'gridTarifas'
            },
            {
                selector: 'panelTarifas tabpanel',
                ref: 'tabTarifas'
            },
            {
                selector: 'gridTarifas button[name=borrarTarifa]',
                ref: 'botonBorrarTarifa'
            }, {
                selector: 'gridFicherosDevoluciones button[name=buttonAnyadirFicheroDevolucion]',
                ref: 'botonAnyadirFicherosDevolucion'
            }, {
                selector: 'tabPanelPagos form[name=formFicheroDevolucion]',
                ref: 'formFicheroDevolucion'
            },
            {
                selector: 'filtroRecibos',
                ref: 'filtroBusqueda'
            },
            {
                selector: 'gridTarifasEnvios',
                ref: 'gridTarifasEnvios'
            },
            {
                selector: 'gridTarifasEnvios button[name=borrarTarifaEnvio]',
                ref: 'botonBorrarTarifaEnvio'
            },
            {
                selector: 'panelTarifaEnvios grid[name=gridEnviosProgramados]',
                ref: 'gridEnviosProgramados'
            },
            {
                selector: 'panelTarifaEnvios grid[name=gridEnviosProgramados] button[name=borrarEnvioProgramado]',
                ref: 'botonBorrarEnvioProgramado'
            },
            {
                selector: 'ventanaRecibo form[name=formRecibo]',
                ref: 'formRecibo'
            }
        ],

        dirty: false,
        init: function () {
            this.control(
                {
                    'gridTiposTarifas': {

                        edit: function () {
                            this.getGridTiposTarifas().getStore().sync(
                                {
                                    scope: this,
                                    callback: function (call) {
                                        if (call.operations[0].action == 'create') {
                                            this.getStoreTarifasStore().loadData([], false);
                                            this.setEstadoComponentesGridTiposTarifas();
                                            this.setEstadoComponentesGridTarifas();
                                        }
                                    }
                                });
                        },
                        select: function (seleccion, elemento) {
                            if (elemento.data.id)
                                this.cargaGridTarifas(elemento.data);
                        }
                    },

                    'gridTiposTarifas button[name=anyadirTipoTarifa]': {
                        click: this.anyadirTipoTarifa
                    },
                    'gridTiposTarifas button[name=borrarTipoTarifa]': {
                        click: this.borrarTipoTarifa
                    },

                    'gridTarifas': {
                        edit: function () {
                            this.getStoreTarifasStore().sync(
                                {
                                    scope: this,
                                    callback: function () {
                                        this.setEstadoComponentesGridTiposTarifas();
                                    }
                                });
                        },
                        select: function () {
                            this.setEstadoComponentesGridTarifas();
                        }
                    },

                    'gridTarifas button[name=anyadirTarifa]': {
                        click: this.anyadirTarifa
                    },
                    'gridTarifas button[name=borrarTarifa]': {
                        click: this.borrarTarifa
                    },

                    'gridEnviosProgramados': {
                        edit: function () {
                            this.getStoreTarifasEnviosProgramadosStore().sync()
                        }
                    },

                    'panelTarifaEnvios grid[name=gridEnviosProgramados] button[name=anyadirEnvioProgramado]': {
                        click: this.editarEnviosProgramados
                    },
                    'panelTarifaEnvios grid[name=gridEnviosProgramados] button[name=borrarEnvioProgramado]': {
                        click: this.borrarEnviosProgramados
                    },

                    'gridFicherosDevoluciones filefield[name=anyadirFicheroDevolucion]': {
                        change: function () {
                            this.getBotonAnyadirFicherosDevolucion().setDisabled(false);
                        }
                    },
                    'gridFicherosDevoluciones button[name=buttonAnyadirFicheroDevolucion]': {
                        click: this.anyadirFicheroDevolucion
                    },

                    'filtroRecibos button[name=anyadirFiltroFechaCreacionRecibos]': {
                        click: this.anyadirFiltroFechaCreacionRecibos
                    },

                    'filtroRecibos button[name=anyadirFiltroCampanyaRecibos]': {
                        click: this.anyadirFiltroCampanyaRecibos
                    },

                    'filtroRecibos button[name=anyadirFiltroExportacionRecibos]': {
                        click: this.anyadirFiltroExportacionRecibos
                    },

                    'filtroRecibos button[name=anyadirFiltroEstadoRecibos]': {
                        click: this.anyadirFiltroEstadoRecibos
                    },

                    'filtroRecibos button[name=botonBusqueda]': {
                        click: this.busqueda
                    },
                    'filtroRecibos button[name=botonExportar]': {
                        click: this.exportarCsv
                    },
                    'gridRecibos': {
                        itemdblclick: function (obj, record) {
                            this.showVentanaRecibo(record);
                        }
                    },
                    'gridMovimientosRecibo actioncolumn[name=quitarMovimientoExportado]': {
                        click: function (obj, r, it, i, e, rec) {
                            this.quitarMovimientoExportado(obj, rec);
                        }
                    }
                });

        },

        quitarMovimientoExportado: function (grid, movimiento) {

            var ref = this;
            Ext.Msg.confirm('Eliminació de \'exportació', '<b>Esteu segur/a de voler esborrar el moviment de la llista d\'exportats?</b>', function (btn) {
                if (btn == 'yes') {

                    Ext.Ajax.request(
                        {
                            url: '/crm/rest/recibomovimientoexportado/' + movimiento.data.id,
                            method: 'PUT',
                            callback: function () {
                                ref.getStoreMovimientosReciboStore().reload();
                            }
                        });
                }
            });
        }        ,

        showVentanaRecibo: function (recibo) {
            this.ventanaRecibo = this.getVentanaRecibo();

            var form = this.getFormRecibo();
            form.loadRecord(recibo);

            var storeLineas = this.getStoreLineasReciboStore();
            storeLineas.load({
                url: 'rest/linea/recibo/' + recibo.data.id
            });

            var storeMovimientos = this.getStoreMovimientosReciboStore();
            storeMovimientos.load({
                url: 'rest/movimientorecibo/recibo/' + recibo.data.id
            });

            this.ventanaRecibo.show();
        },

        anyadirFiltroCampanyaRecibos: function () {

            var obj = this.getFiltroBusqueda().dameObjetoCampanya();
            if (this.getFiltroBusqueda().esFiltroCampanyaValido(obj)) {
                var array = this.getFiltroBusqueda().arrayCampanyas;
                array.push(obj);
                var text = this.getFiltroBusqueda().down('textarea[name=textoFiltros]').getValue();
                text = text + this.getFiltroBusqueda().toStringCampanya(obj) + "\n";
                this.getFiltroBusqueda().down('textarea[name=textoFiltros]').setValue(text);
                this.getFiltroBusqueda().limpiarCamposCampanya();
            }
        }
        ,

        anyadirFiltroFechaCreacionRecibos: function () {
            var obj = this.getFiltroBusqueda().dameObjetoFecha();
            if (this.getFiltroBusqueda().esFiltroFechaValido(obj)) {
                var array = this.getFiltroBusqueda().arrayFechas;
                array.push(obj);
                var text = this.getFiltroBusqueda().down('textarea[name=textoFiltros]').getValue();
                text = text + this.getFiltroBusqueda().toStringFecha(obj) + "\n";
                this.getFiltroBusqueda().down('textarea[name=textoFiltros]').setValue(text);
                this.getFiltroBusqueda().limpiarCamposFecha();
            }
        }
        ,

        anyadirFiltroEstadoRecibos: function () {
            var obj = this.getFiltroBusqueda().dameObjetoEstado();
            if (this.getFiltroBusqueda().esFiltroEstadoValido(obj)) {
                var array = this.getFiltroBusqueda().arrayEstados;
                array.push(obj);
                var text = this.getFiltroBusqueda().down('textarea[name=textoFiltros]').getValue();
                text = text + this.getFiltroBusqueda().toStringEstado(obj) + "\n";
                this.getFiltroBusqueda().down('textarea[name=textoFiltros]').setValue(text);
                this.getFiltroBusqueda().limpiarCamposEstado();
            }
        }
        ,

        anyadirFiltroExportacionRecibos: function () {
            var obj = this.getFiltroBusqueda().dameObjetoExportacion();

            if (this.getFiltroBusqueda().esFiltroExportacionValido(obj)) {
                var array = this.getFiltroBusqueda().arrayExportaciones;
                array.push(obj);
                var text = this.getFiltroBusqueda().down('textarea[name=textoFiltros]').getValue();
                text = text + this.getFiltroBusqueda().toStringExportacion(obj) + "\n";
                this.getFiltroBusqueda().down('textarea[name=textoFiltros]').setValue(text);
                this.getFiltroBusqueda().limpiarCamposExportacion();
            }
        }
        ,

        busqueda: function () {

            var obj = {
                arrayFechas: Ext.encode(this.getFiltroBusqueda().arrayFechas),
                arrayCampanyas: Ext.encode(this.getFiltroBusqueda().arrayCampanyas),
                arrayEstados: Ext.encode(this.getFiltroBusqueda().arrayEstados),
                arrayExportaciones: Ext.encode(this.getFiltroBusqueda().arrayExportaciones)
            };

            var recibos = this.getStoreRecibosStore();

            recibos.getProxy().extraParams = obj;
            recibos.loadPage(1);
        }
        ,

        exportarCsv: function () {

            var url = '/crm/rest/recibo/csv/?arrayEstados=' + JSON.stringify(this.getFiltroBusqueda().arrayEstados)
                + '&arrayCampanyas=' + JSON.stringify(this.getFiltroBusqueda().arrayCampanyas)
                + '&arrayFechas=' + JSON.stringify(this.getFiltroBusqueda().arrayFechas)
                + '&arrayExportaciones=' + JSON.stringify(this.getFiltroBusqueda().arrayExportaciones);
            window.open(url);
        }
        ,

        listaFicherosDevolucion: function () {
            this.getStoreFicherosDevolucionesStore().reload();
        }
        ,

        anyadirFicheroDevolucion: function () {
            this.getFormFicheroDevolucion().submit(
                {
                    url: '/crm/rest/ficherodevolucion/',
                    scope: this,
                    success: function () {
                        this.listaFicherosDevolucion();
                        this.getBotonAnyadirFicherosDevolucion().setDisabled(true);
                    },
                    failure: function () {
                        Ext.Msg.alert('Insercció de fitxers', '<b>El fitxer no ha pogut ser insertat correctament</b>');
                    }
                });
        }
        ,


        cargaGridTarifas: function (elemento) {
            this.getStoreTarifasStore().load(
                {
                    params: {
                        tipoTarifaId: elemento.id
                    },
                    scope: this,
                    callback: function () {
                        this.setEstadoComponentesGridTiposTarifas();
                    }
                });

            this.getStoreTarifasEnviosStore().getProxy().setExtraParam('tipoTarifaId', elemento.id);
            this.getStoreTarifasEnviosStore().load(
                {
                    scope: this,
                    callback: function () {
                        this.setEstadoComponentesGridTiposTarifas();
                    }
                });

        },

        anyadirTipoTarifa: function () {
            var campanya = this.getComboCampanya().getValue();
            var tipoTarifa = this.getTipoTarifaModel().create({
                campanyaId: campanya
            });
            var store = this.getGridTiposTarifas().getStore();

            var rowEditor = this.getGridTiposTarifas().getPlugin("editingTipoTarifa");
            rowEditor.cancelEdit();
            store.insert(0, tipoTarifa);
            rowEditor.startEdit(0, 0);

        },

        borrarTipoTarifa: function () {
            var ref = this;
            var grid = ref.getGridTiposTarifas();
            var store = ref.getGridTiposTarifas().getStore();
            Ext.Msg.confirm('Eliminació de tipus de tarifes', '<b>Esteu segur/a de voler esborrar el tipus de tarifa sel·leccionada?</b>', function (btn) {
                if (btn == 'yes') {
                    var tipoTarifa = grid.getSelectionModel().getSelection();
                    if (tipoTarifa.length == 1) {
                        var registro = tipoTarifa[0];
                        store.remove(registro);
                        store.sync(
                            {
                                callback: function () {
                                    ref.setEstadoTodos();
                                }
                            });
                    }
                }
            });
        }
        ,

        anyadirTarifa: function () {
            var tarifa = this.getTarifaModel().create(
                {
                    tipoTarifaId: this.getController("ControllerVarios").dameIdGrid(this.getGridTiposTarifas())
                });
            var store = this.getStoreTarifasStore();

            var rowEditor = this.getGridTarifas().getPlugin("editingTarifa");
            rowEditor.cancelEdit();
            store.insert(0, tarifa);
            rowEditor.startEdit(0, 0);

        }
        ,

        borrarTarifa: function () {
            var ref = this;
            Ext.Msg.confirm('Eliminació de tarifes', '<b>Esteu segur/a de voler esborrar la tarifa sel·leccionada?</b>', function (btn) {
                if (btn == 'yes') {
                    var tarifa = ref.getGridTarifas().getSelectionModel().getSelection();
                    if (tarifa.length == 1) {
                        var registro = tarifa[0];
                        var store = ref.getStoreTarifasStore();
                        store.remove(registro);
                        store.sync(
                            {
                                callback: function () {
                                    ref.setEstadoTodos();
                                }
                            });
                    }
                }
            });
        },

        editarEnviosProgramados: function () {

            var store = this.getStoreTarifasEnviosProgramadosStore();

            var rec = this.getTipoTarifaEnvioProgramadoModel().create({
                tarifaTipoEnvioId: this.getController("ControllerVarios").dameIdGrid(this.getGridTarifasEnvios()),
                tipoFecha: 1,
                cuandoRealizarEnvio: 1
            });

            var rowEditor = this.getGridEnviosProgramados().getPlugin('editingEnvioProgramado');
            rowEditor.cancelEdit();
            store.insert(0, rec);
            rowEditor.startEdit(0, 0);
        },

        borrarEnviosProgramados: function () {

            var ref = this;
            Ext.Msg.confirm('Eliminació de enviaments', '<b>Esteu segur/a de voler esborrar la programació de l\'enviament sel·leccionat?</b>', function (btn) {
                if (btn == 'yes') {
                    var envio = ref.getGridEnviosProgramados().getSelectionModel().getSelection();
                    if (envio.length == 1) {
                        var registro = envio[0];
                        var store = ref.getStoreTarifasEnviosProgramadosStore();
                        store.remove(registro);
                        store.sync(
                            {
                                callback: function () {
                                    ref.setEstadoTodos();
                                }
                            });
                    }
                }
            });
        },

        setEstadoTodos: function () {
            this.setEstadoComponentesGridTiposTarifas();
            this.setEstadoComponentesGridTarifas();
            this.setEstadoComponentesGridTarifasEnvios();
        },

        setEstadoComponentesGridTiposTarifas: function () {
            var tieneSeleccionGridTiposTarifas = this.getGridTiposTarifas().getSelectionModel().hasSelection();
            var tarifas = this.getGridTarifas().store.getCount();

            this.getBotonBorrarTipoTarifa().setDisabled(tarifas || !tieneSeleccionGridTiposTarifas);
            this.getTabTarifas().setDisabled(!tieneSeleccionGridTiposTarifas);
        },

        setEstadoComponentesGridTarifas: function () {
            var tieneSeleccionGridTarifas = this.getGridTarifas().getSelectionModel().hasSelection();
            this.getBotonBorrarTarifa().setDisabled(!tieneSeleccionGridTarifas);
        },

        setEstadoComponentesGridTarifasEnvios: function () {
            var tieneSeleccionGridTarifas = this.getGridTarifasEnvios().getSelectionModel().hasSelection();
            this.getBotonBorrarTarifaEnvio().setDisabled(!tieneSeleccionGridTarifas);
        }

    })
;