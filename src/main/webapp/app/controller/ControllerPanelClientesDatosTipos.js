Ext.define('CRM.controller.ControllerPanelClientesDatosTipos',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreClientesDatosTipos', 'StoreTiposTipoDato', 'StoreClientesDatosOpciones' ],
    models : [ 'ClienteDatoTipo', 'ClienteDatoOpcion' ],
    requires : [ 'CRM.model.enums.TiposDato' ],

    refs : [
    {
        selector : 'gridClientesDatosTipos',
        ref : 'gridClientesDatosTipos'
    },
    {
        selector : 'gridClientesDatosOpciones',
        ref : 'gridClientesDatosOpciones'
    },
    {
        selector : 'gridClientesDatosTipos button[name=borrarClienteDatoTipo]',
        ref : 'botonBorrarClienteDatoTipo'
    },
    {
        selector : 'gridClientesDatosOpciones button[name=borrarClienteDatoOpcion]',
        ref : 'botonBorrarClienteDatoOpcion'
    } ],

    init : function()
    {
        this.control(
        {
            'gridClientesDatosTipos button[name=borrarClienteDatoTipo]' :
            {
                click : this.borrarClienteDatoTipo
            },
            'gridClientesDatosTipos button[name=anyadirClienteDatoTipo]' :
            {
                click : this.anyadirClienteDatoTipo
            },
            'gridClientesDatosTipos' :
            {
                select : function()
                {
                    this.cargaGridOpciones();
                    this.setEstadoComponentes();
                },
                edit : this.guardaClientesDatosTipo
            },
            'gridClientesDatosOpciones button[name=borrarClienteDatoOpcion]' :
            {
                click : this.borrarClienteDatoOpcion
            },
            'gridClientesDatosOpciones button[name=anyadirClienteDatoOpcion]' :
            {
                click : this.anyadirClienteDatoOpcion
            },
            'gridClientesDatosOpciones' :
            {
                select : this.setEstadoComponentes,
                edit : this.guardaClientesDatosOpcion
            }
        });
    },

    // Aciones Grid Tipos de datos

    anyadirClienteDatoTipo : function()
    {
        var store = this.getStoreClientesDatosTiposStore();
        var etiqueta = this.getClienteDatoTipoModel().create();
        var rowEditor = this.getGridClientesDatosTipos().getPlugin('editingClienteDatoTipo');

        rowEditor.cancelEdit();
        store.insert(0, etiqueta);
        rowEditor.startEdit(0, 0);
    },

    borrarClienteDatoTipo : function()
    {
        var ref = this;
        var store = ref.getStoreClientesDatosTiposStore();
        var selection = this.getGridClientesDatosTipos().getSelectionModel().getSelection();
        var etiquetaId = (selection.length > 0) ? selection[0].data.id : null;
        var etiqueta = store.findRecord('id', etiquetaId);

        if (etiqueta)
        {
            Ext.Msg.confirm('Esborrar Tipus de dada', 'Esteu segur/a de voler esborrar el registre sel·leccionat?', function(btn)
            {
                if (btn == 'yes')
                {
                    store.remove(etiqueta);
                    store.sync(
                    {
                        success : function()
                        {
                            ref.setEstadoComponentes();
                        },
                        failure : function()
                        {
                            Ext.Msg.alert('Esborrar ClientesDatosTipo', 'No s`ha pogut esborrar la etiqueta perquè té registres associats.');
                            store.rejectChanges();
                            ref.setEstadoComponentes();
                        }
                    });
                }
            });
        }
    },

    cargaGridOpciones : function()
    {
        var clienteDatoTipoId = this.getController("ControllerVarios").dameIdGrid(this.getGridClientesDatosTipos());

        if (clienteDatoTipoId)
        {
            var store = this.getStoreClientesDatosOpcionesStore();
            store.load(
            {
                params :
                {
                    clienteDatoTipoId : clienteDatoTipoId
                },
                scope : this,
                callback : function()
                {
                    this.setEstadoComponentes();
                }
            });
        }
        else
        {
            this.limpiarGridOpciones();
        }
    },

    limpiarGridOpciones : function()
    {
        this.getStoreClientesDatosOpcionesStore().loadData([], false);
        this.getGridClientesDatosOpciones().setDisabled(true);
    },

    guardaClientesDatosTipo : function()
    {
        var store = this.getStoreClientesDatosTiposStore();
        store.sync(
        {
            success : function()
            {
                this.setEstadoComponentes();
            },
            failure : function()
            {
                Ext.Msg.alert('Dades', 'S\'ha produït un error en modificar la dada');
                store.rejectChanges();
                this.setEstadoComponentes();
            },
            scope : this
        });
    },

    // Aciones Grid Opciones

    anyadirClienteDatoOpcion : function()
    {
        var selection = this.getGridClientesDatosTipos().getSelectionModel().getSelection();
        var clienteDatoTipoId = (selection.length > 0) ? selection[0].data.id : null;
        var store = this.getStoreClientesDatosOpcionesStore();
        var clienteDatoOpcion = this.getClienteDatoOpcionModel().create(
        {
            clienteDatoTipoId : clienteDatoTipoId
        });
        var rowEditor = this.getGridClientesDatosOpciones().getPlugin('editingClienteDatoOpcion');

        rowEditor.cancelEdit();
        store.insert(0, clienteDatoOpcion);
        rowEditor.startEdit(0, 0);
    },

    borrarClienteDatoOpcion : function()
    {
        var ref = this;
        var store = ref.getStoreClientesDatosOpcionesStore();
        var selection = this.getGridClientesDatosOpciones().getSelectionModel().getSelection();
        var clienteDatoOpcionId = (selection.length > 0) ? selection[0].data.id : null;
        var clienteDatoOpcion = store.findRecord('id', clienteDatoOpcionId);

        if (clienteDatoOpcion)
        {
            Ext.Msg.confirm('Esborrar', 'Esteu segur/a de voler esborrar el registre sel·leccionat?', function(btn)
            {
                if (btn == 'yes')
                {
                    store.remove(clienteDatoOpcion);
                    store.sync(
                    {
                        success : function()
                        {
                            ref.setEstadoComponentes();
                        },
                        failure : function()
                        {
                            Ext.Msg.alert('Esborrar ClientesDatosOpcion', 'No s`ha pogut esborrar la etiqueta perquè té registres associats.');
                            store.rejectChanges();
                            ref.setEstadoComponentes();
                        }
                    });
                }
            });
        }
    },

    guardaClientesDatosOpcion : function()
    {
        var store = this.getStoreClientesDatosOpcionesStore();
        store.sync();
    },

    setEstadoComponentes : function()
    {
        var tieneSeleccionGridClientesDatosTipos = this.getGridClientesDatosTipos().getSelectionModel().hasSelection();
        var tieneSeleccionGridClientesDatosOpciones = this.getGridClientesDatosOpciones().getSelectionModel().hasSelection();
        this.getBotonBorrarClienteDatoTipo().setDisabled(!tieneSeleccionGridClientesDatosTipos);
        this.getBotonBorrarClienteDatoOpcion().setDisabled(!tieneSeleccionGridClientesDatosOpciones);

        var selection = this.getGridClientesDatosTipos().getSelectionModel().getSelection();
        var clienteDatoTipoTipoId = (selection.length > 0) ? selection[0].data.clienteDatoTipoTipoId : null;
        this.getGridClientesDatosOpciones().setDisabled(!tieneSeleccionGridClientesDatosTipos || (clienteDatoTipoTipoId !== CRM.model.enums.TiposDato.SELECCION.id));
    }

});