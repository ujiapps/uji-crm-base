Ext.define('CRM.controller.ControllerPanelCampanyasFormularios',
    {
        extend: 'Ext.app.Controller',
        stores: ['StoreCampanyaDatos', 'StoreCampanyaFormularios', 'StoreTiposSolicitudCampanyaDatos', 'StoreCampanyasFormularioEstadoOrigen', 'StoreItemsContestacion',
            'StoreTiposValidacion', 'StoreCampanyaFormulariosVentana'],
        models: ['CampanyaDato', 'CampanyaFormulario', 'CampanyaFormularioEstadoOrigen', 'Cliente', 'Campanya', 'TipoEstadoCampanya'],
        views: ['campanyas.formularios.GridCampanyaDatos', 'campanyas.formularios.GridCampanyaFormularios', 'campanyas.formularios.VentanaCampanyaFormularios', 'campanyas.formularios.VentanaCampanyaDatos',
            'campanyas.formularios.GridCampanyaFormularioEstadosOrigen'],

        ventanaCampanyaFormularios: {},

        refs: [
            {
                selector: 'gridCampanyaFormularios',
                ref: 'gridCampanyaFormularios'
            },
            {
                selector: 'gridCampanyaFormularios button[name=borrar]',
                ref: 'botonGridCampanyaFormularioBorrar'
            },
            {
                selector: 'gridCampanyaFormularios button[name=duplicar]',
                ref: 'botonGridCampanyaFormularioDuplicar'
            },
            {
                selector: 'gridCampanyaFormularios button[name=direccionGenerica]',
                ref: 'direccionGenerica'
            },
            {
                selector: 'ventanaCampanyaFormularios',
                ref: 'ventanaCampanyaFormularios'
            },
            {
                selector: 'ventanaCampanyaFormularios form[name=formFormularioCampanya]',
                ref: 'formFormularioCampanya'
            },
            {
                selector: 'ventanaCampanyaFormularios button[name=anyadirFormularioCampanya]',
                ref: 'botonAnyadirFormularioCampanya'
            },
            {
                selector: 'ventanaCampanyaFormularios button[name=actualizarFormularioCampanya]',
                ref: 'botonActualizarFormularioCampanya'
            },
            {
                selector: 'ventanaCampanyaDatos form[name=formDatoCampanya]',
                ref: 'formDatoCampanya'
            },
            {
                selector: 'ventanaCampanyaDatos button[name=anyadirDatoCampanya]',
                ref: 'botonAnyadirDatoCampanya'
            },
            {
                selector: 'ventanaCampanyaDatos button[name=actualizarDatoCampanya]',
                ref: 'botonActualizarDatoCampanya'
            },
            {
                selector: 'gridCampanyaDatos',
                ref: 'gridCampanyaDatos'
            },
            {
                selector: 'gridCampanyaDatos button[name=borrar]',
                ref: 'botonBorrarCampanyaDatos'
            },
            {
                selector: 'gridCampanyaFormularioEstadosOrigen',
                ref: 'gridCampanyaFormularioEstadosOrigen'
            },
            {
                selector: 'ventanaCampanyaFormularios [name=formPrevisualizacionClienteCampanya]',
                ref: 'formPrevisualizacionClienteCampanya'
            }
        ],

        dirty: false,
        init: function () {
            this.control(
                {
                    'gridCampanyaFormularios': {
                        selectionchange: this.onGridCampanyaFormulariosSelectionChange,
                        itemdblclick: this.mostrarVentanaActualizarCampanyaFormulario
                    },

                    'gridCampanyaFormularios button[name=anyadir]': {
                        click: this.mostrarVentanaFormularioCampanya
                    },
                    'gridCampanyaFormularios button[name=borrar]': {
                        click: this.borrarFormularioCampanya
                    },

                    'gridCampanyaFormularios button[name=duplicar]': {
                        click: this.duplicarFormularioCampanya
                    },

                    // 'ventanaCampanyaFormularios': {
                    //     render: this.cargaPrevisualizacionFormulario
                    // },
                    'ventanaCampanyaFormularios button[name=anyadirFormularioCampanya]': {
                        click: this.anyadirCampanyaFormulario
                    },

                    'ventanaCampanyaFormularios button[name=actualizarFormularioCampanya]': {
                        click: this.actualizarCampanyaFormulario
                    },

                    'ventanaCampanyaFormularios button[name=cancelarFormularioCampanya]': {
                        click: this.cerrarVentanaCampanyaFormularios
                    },

                    'gridCampanyaDatos': {
                        selectionchange: this.onGridCampanyaDatosSelectionChange,
                        itemdblclick: this.mostrarVentanaActualizarCampanyaDato
                    }
                    ,
                    'gridCampanyaDatos button[name=anyadir]': {
                        click: this.mostrarVentanaCampanyaDatos
                    }
                    ,
                    'gridCampanyaDatos button[name=borrar]': {
                        click: this.borrarCampanyaDato
                    }
                    ,

                    'ventanaCampanyaDatos button[name=anyadirDatoCampanya]': {
                        click: this.anyadirCampanyaDato
                    }
                    ,

                    'ventanaCampanyaDatos button[name=actualizarDatoCampanya]': {
                        click: this.actualizarCampanyaDato
                    }
                    ,

                    'ventanaCampanyaDatos button[name=cancelarDatoCampanya]': {
                        click: this.cerrarVentanaCampanyaDatos
                    },

                    'gridCampanyaFormularioEstadosOrigen button[name=anyadirCampanyaFormularioEstadoOrigen]': {
                        click: this.anyadirCampanyaFormularioEstadoOrigen
                    },

                    'gridCampanyaFormularioEstadosOrigen button[name=borrarCampanyaFormularioEstadoOrigen]': {
                        click: this.borrarCampanyaFormularioEstadoOrigen
                    },

                    'gridCampanyaFormularioEstadosOrigen': {
                        edit: this.sincronizarCampanyaFormularioEstadoOrigen
                    }
                })
            ;
        },

        sincronizarCampanyaFormularioEstadoOrigen: function () {
            this.getStoreCampanyasFormularioEstadoOrigenStore().sync({});
        },

        borrarCampanyaFormularioEstadoOrigen: function () {
            var store = this.getStoreCampanyasFormularioEstadoOrigenStore();

            var rec = store.findRecord('id', this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaFormularioEstadosOrigen()));

            Ext.Msg.confirm('Eliminar', 'El registre s\'eliminarà. Estàs segur de voler continuar?', function (btn) {
                if (btn == 'yes') {
                    store.remove(rec);
                    store.sync();
                }
            });
        },

        anyadirCampanyaFormularioEstadoOrigen: function () {

            var campanyaFormularioEstadoOrigen = this.getCampanyaFormularioEstadoOrigenModel().create({
                formularioId: this.formularioId
            });

            var store = this.getStoreCampanyasFormularioEstadoOrigenStore();
            store.insert(0, campanyaFormularioEstadoOrigen);
            //this.getStoreCampanyasFormularioEstadoOrigenStore().sync();

        },

        anyadirCampanyaDato: function () {

            var store = this.getStoreCampanyaDatosStore();

            var form = this.getFormDatoCampanya();

            if (form.getForm().isValid()) {

                var rec = this.getCampanyaDatoModel().create({
                    campanyaFormularioId: this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaFormularios()),
                    clienteDatoTipoId: form.down("combobox[name=clienteDatoTipo]").getValue(),
                    orden: form.down("textfield[name=orden]").getValue(),
                    textoCa: form.down("textfield[name=textoCa]").getValue(),
                    textoEs: form.down("textfield[name=textoEs]").getValue(),
                    textoUk: form.down("textfield[name=textoUk]").getValue(),
                    textoAyudaCa: form.down("textfield[name=textoAyudaCa]").getValue(),
                    textoAyudaEs: form.down("textfield[name=textoAyudaEs]").getValue(),
                    textoAyudaUk: form.down("textfield[name=textoAyudaUk]").getValue(),
                    grupoId: form.down("combobox[name=grupo]").getValue(),
                    itemId: form.down("combobox[name=item]").getValue(),
                    requerido: form.down("checkbox[name=requerido]").getValue(),
                    tipoAccesoId: form.down("combobox[name=tipoAcceso]").getValue(),
                    vinculadoCampanya: form.down("checkbox[name=vinculadoCampanya]").getValue(),
                    campanyaDatoExtraId: form.down("combobox[name=campanyaDatoExtra]").getValue(),
                    tamanyo: form.down("textfield[name=tamanyo]").getValue(),
                    etiqueta: form.down("textfield[name=etiqueta]").getValue(),
                    visualizar: form.down("radiofield[name=radioBoxVisualizarSoloSi]").getGroupValue(),
                    preguntaRelacionadaId: form.down("combobox[name=campanyaPreguntaRelacionada]").getValue(),
                    respuestaRelacionadaId: form.down("combobox[name=campanyaRespuestaRelacionada]").getValue(),
                    tipoValidacionId: form.down("combobox[name=validar]").getValue()


                });

                store.insert(0, rec);
                store.sync({
                    scope : this,
                    callback : function(){
                        store.sort();
                        this.cerrarVentanaCampanyaDatos();
                        var formulario = this.getGridCampanyaFormularios().getSelectionModel().getSelection()[0];
                        this.cargaPrevisualizacionFormulario(formulario.data.campanyaId, formulario.data.estadoDestinoId, formulario.data.accionDestino);
                    }
                });

            }
            else {
                Ext.Msg.alert('Afegir Dades', 'El formulari no es correcte');
            }
        },

        actualizarCampanyaDato: function () {

            var form = this.getFormDatoCampanya();

            if (form.getForm().isValid()) {

                var store = this.getStoreCampanyaDatosStore();
                var record = store.findRecord('id', this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaDatos()));

                record.set('clienteDatoTipoId', form.down("combobox[name=clienteDatoTipo]").getValue());
                record.set('textoCa', form.down("textfield[name=textoCa]").getValue());
                record.set('textoEs', form.down("textfield[name=textoEs]").getValue());
                record.set('textoUk', form.down("textfield[name=textoUk]").getValue());
                record.set('textoAyudaCa', form.down("textfield[name=textoAyudaCa]").getValue());
                record.set('textoAyudaEs', form.down("textfield[name=textoAyudaEs]").getValue());
                record.set('textoAyudaUk', form.down("textfield[name=textoAyudaUk]").getValue());
                record.set('grupoId', form.down("combobox[name=grupo]").getValue());
                record.set('itemId', form.down("combobox[name=item]").getValue());
                record.set('requerido', form.down("checkbox[name=requerido]").getValue());
                record.set('orden', form.down("textfield[name=orden]").getValue());
                record.set('tipoAccesoId', form.down("combobox[name=tipoAcceso]").getValue());
                record.set('vinculadoCampanya', form.down("checkbox[name=vinculadoCampanya]").getValue());
                record.set('campanyaDatoExtraId', form.down("combobox[name=campanyaDatoExtra]").getValue());
                record.set('tamanyo', form.down("textfield[name=tamanyo]").getValue());
                record.set('etiqueta', form.down("textfield[name=etiqueta]").getValue());
                record.set('visualizar', form.down("radiofield[name=radioBoxVisualizarSoloSi]").getGroupValue());
                record.set('preguntaRelacionadaId', form.down("combobox[name=campanyaPreguntaRelacionada]").getValue());
                record.set('respuestaRelacionadaId', form.down("combobox[name=campanyaRespuestaRelacionada]").getValue());
                record.set('tipoValidacionId', form.down("combobox[name=validar]").getValue());
                store.update({
                    scope: this,
                    callback : function(){
                        store.sort();
                        this.cerrarVentanaCampanyaDatos();
                        var formulario = this.getGridCampanyaFormularios().getSelectionModel().getSelection()[0];
                        this.cargaPrevisualizacionFormulario(formulario.data.campanyaId, formulario.data.estadoDestinoId, formulario.data.accionDestino);
                    }
                });
            }
            else {
                Ext.Msg.alert('Afegir Dades', 'El formulari no es correcte');
            }
        },

        borrarCampanyaDato: function () {
            var store = this.getStoreCampanyaDatosStore();
            var ref = this;

            var campanyaDato = store.findRecord('id', this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaDatos()));
            var formulario = this.getGridCampanyaFormularios().getSelectionModel().getSelection()[0];
            Ext.Msg.confirm('Eliminar', 'El registre s\'eliminarà. Estàs segur de voler continuar?', function (btn) {
                if (btn == 'yes') {
                    store.remove(campanyaDato);
                    store.sync({
                        success: function () {
                            ref.cargaPrevisualizacionFormulario(formulario.data.campanyaId, formulario.data.estadoDestinoId, formulario.data.accionDestino);
                        }
                    });
                }
            });
        },

        ////////////////////////////////// VENTANA FORMULARIO /////////////////////////////////////////////////

        getVentanaCampanyaFormularioView: function () {
            return this.getView('campanyas.formularios.VentanaCampanyaFormularios').create();
        },

        mostrarVentanaFormularioCampanya: function () {

            this.ventanaCampanyaFormularios = this.getVentanaCampanyaFormularioView();
            var form = this.getFormFormularioCampanya();

            var storeTiposEstadoCampanya = Ext.getStore('StoreTiposEstadoCampanya');

            var radiobox = form.down("fieldcontainer[name=radiofieldEstadoDestino]");
            for (var i in storeTiposEstadoCampanya.data.items) {
                var check = Ext.create(
                    {
                        xclass: 'Ext.form.field.Radio',
                        name: 'radioboxEstadoDestino',
                        boxLabel: storeTiposEstadoCampanya.data.items[i].data.nombre,
                        inputValue: storeTiposEstadoCampanya.data.items[i].data.id
                    });
                radiobox.insert(check);
            }

            var checkbox = form.down("checkboxgroup[name=checkboxfieldEstadosOrigen]");
            for (var i in storeTiposEstadoCampanya.data.items) {
                var check = Ext.create(
                    {
                        xclass: 'Ext.form.field.Checkbox',
                        name: 'checkboxEstadosOrigen',
                        boxLabel: storeTiposEstadoCampanya.data.items[i].data.nombre,
                        inputValue: storeTiposEstadoCampanya.data.items[i].data.id
                    });
                checkbox.insert(check);
            }
            var check = Ext.create(
                {
                    xclass: 'Ext.form.field.Checkbox',
                    name: 'checkboxEstadosOrigen',
                    boxLabel: 'Cualsevol',
                    inputValue: null
                });

            checkbox.insert(check);

            // this.cargaPrevisualizacionFormulario(4);

            this.getBotonActualizarFormularioCampanya().setVisible(false);
            this.getBotonAnyadirFormularioCampanya().setVisible(true);
            this.ventanaCampanyaFormularios.show();
        },

        mostrarVentanaActualizarCampanyaFormulario: function (obj, record) {

            var storeCampanyaDatos = this.getStoreCampanyaDatosStore();

            storeCampanyaDatos.load(
                {
                    params: {
                        formularioId: record.data.id
                    },
                    scope: this,
                    callback: function () {
                        //this.setEstadoComponentes();
                        this.ventanaCampanyaFormularios = this.getVentanaCampanyaFormularioView();
                        var form = this.getFormFormularioCampanya();

                        form.down("textfield[name=titulo]").setValue(record.data.titulo);
                        form.down("textfield[name=cabecera]").setValue(record.data.cabecera);
                        form.down("combobox[name=plantilla]").setValue(record.data.plantilla);
                        form.down("combobox[name=solicitarCorreo]").setValue(record.data.solicitarCorreo);
                        form.down("combobox[name=solicitarIdentificacion]").setValue(record.data.solicitarIdentificacion);
                        form.down("combobox[name=solicitarMovil]").setValue(record.data.solicitarMovil);
                        form.down("combobox[name=solicitarNombre]").setValue(record.data.solicitarNombre);
                        form.down("combobox[name=solicitarPostal]").setValue(record.data.solicitarPostal);
                        form.down("checkbox[name=autenticaFormulario]").setValue(record.data.autenticaFormulario);
                        form.down("datefield[name=fechaInicio]").setValue(record.data.fechaInicio);
                        form.down("datefield[name=fechaFin]").setValue(record.data.fechaFin);
                        form.down("combobox[name=acto]").setValue(record.data.actoId);

                        var storeTiposEstadoCampanya = Ext.getStore('StoreTiposEstadoCampanya');

                        var radiobox = form.down("fieldcontainer[name=radiofieldEstadoDestino]");
                        for (var i in storeTiposEstadoCampanya.data.items) {
                            var check = Ext.create(
                                {
                                    xclass: 'Ext.form.field.Radio',
                                    name: 'radioboxEstadoDestino',
                                    boxLabel: storeTiposEstadoCampanya.data.items[i].data.nombre,
                                    inputValue: storeTiposEstadoCampanya.data.items[i].data.id
                                });
                            if (record.data.estadoDestinoId == storeTiposEstadoCampanya.data.items[i].data.id) {
                                check.setValue(true);
                            }
                            radiobox.insert(check);
                        }

                        this.getStoreCampanyasFormularioEstadoOrigenStore().load({
                            params: {
                                campanyaFormularioId: record.data.id
                            },
                            scope: this,
                            callback: function () {
                                var checkbox = form.down("checkboxgroup[name=checkboxfieldEstadosOrigen]");
                                for (var i in storeTiposEstadoCampanya.data.items) {
                                    var check = Ext.create(
                                        {
                                            xclass: 'Ext.form.field.Checkbox',
                                            name: 'checkboxEstadosOrigen',
                                            boxLabel: storeTiposEstadoCampanya.data.items[i].data.nombre,
                                            inputValue: storeTiposEstadoCampanya.data.items[i].data.id
                                        });
                                    if (this.getStoreCampanyasFormularioEstadoOrigenStore().find('estadoOrigenId', storeTiposEstadoCampanya.data.items[i].data.id) != -1) {
                                        check.setValue(true);
                                    }

                                    checkbox.insert(check);
                                }
                                var check = Ext.create(
                                    {
                                        xclass: 'Ext.form.field.Checkbox',
                                        name: 'checkboxEstadosOrigen',
                                        boxLabel: 'Cualsevol',
                                        inputValue: null
                                    });
                                if (this.getStoreCampanyasFormularioEstadoOrigenStore().find('estadoOrigenId', 'null') != -1) {
                                    check.setValue(true);
                                }

                                checkbox.insert(check);
                            }
                        });

                        this.getBotonActualizarFormularioCampanya().setVisible(true);
                        this.getBotonAnyadirFormularioCampanya().setVisible(false);
                        this.formularioId = record.data.id;

                        this.cargaPrevisualizacionFormulario(record.data.campanyaId, record.data.estadoDestinoId, record.data.accionDestino);

                        this.ventanaCampanyaFormularios.show();
                    }
                });
        },

        cerrarVentanaCampanyaFormularios: function () {
            this.ventanaCampanyaFormularios.destroy();
        },

        cargaPrevisualizacionFormulario: function (campanyaId, estadoDestino, accion) {
            var form = this.getFormPrevisualizacionClienteCampanya();

            var cliente = this.getClienteModel().create(
                {
                    'id': null,
                    'clienteGeneralId': 1,
                    'identificacion': '',
                    'tipoIdentificacion': '',
                    'nombreOficial': '',
                    'apellidosOficial': '',
                    'correo': '',
                    'movil': '',
                    'postal': ''
                });

            var campanya = this.getCampanyaModel().create({
                id: campanyaId
            });

            var estado = this.getTipoEstadoCampanyaModel().create({
                id: estadoDestino,
                accion: accion
            });

            this.getController("ControllerVentanaClienteCampanya").cliente = cliente;
            this.getController("ControllerVentanaClienteCampanya").campanya = campanya;
            this.getController("ControllerVentanaClienteCampanya").estado = estado;

            // var formulario = this.getStoreCampanyaFormulariosVentanaStore();

            // formulario.load({
            //     url: '/crm/rest/campanyaformulario/estado/',
            //     params: {
            //         campanyaId: campanyaId,
            //         estadoId: estadoDestino
            //     },
            //     scope: this,
            //     callback: function (records) {
            this.getController("ControllerVentanaClienteCampanya").construyeFormulario(form);
            // }
            // });
        },

        anyadirCampanyaFormulario: function () {

            var store = this.getStoreCampanyaFormulariosStore();

            var form = this.getFormFormularioCampanya();

            if (form.getForm().isValid()) {
                var estadosOrigen = form.down("checkboxgroup[name=checkboxfieldEstadosOrigen]").getChecked();
                var arrayEstadosOrigen = [];
                for (var i in estadosOrigen) {
                    var obj = {
                        id: estadosOrigen[i].inputValue
                    };
                    arrayEstadosOrigen.push(obj);
                }

                var rec = this.getCampanyaFormularioModel().create({
                    campanyaId: this.getController("ControllerPanelCampanyas").dameIdCampanya(),
                    titulo: form.down("textfield[name=titulo]").getValue(),
                    solicitarCorreo: form.down("combobox[name=solicitarCorreo]").getValue(),
                    solicitarIdentificacion: form.down("combobox[name=solicitarIdentificacion]").getValue(),
                    solicitarMovil: form.down("combobox[name=solicitarMovil]").getValue(),
                    solicitarNombre: form.down("combobox[name=solicitarNombre]").getValue(),
                    solicitarPostal: form.down("combobox[name=solicitarPostal]").getValue(),
                    autenticaFormulario: form.down("checkbox[name=autenticaFormulario]").getValue(),
                    fechaInicio: form.down("datefield[name=fechaInicio]").getValue(),
                    fechaFin: form.down("datefield[name=fechaFin]").getValue(),
                    estadoDestinoId: form.down("radiofield[name=radioboxEstadoDestino]").getGroupValue(),
                    actoId: form.down("combobox[name=acto]").getValue()
                });

                store.insert(0, rec);
                store.sync({
                    callback: function () {
                        Ext.Ajax.request(
                            {
                                url: '/crm/rest/campanyaformularioestadoorigen/',
                                method: 'POST',
                                params: {
                                    campanyaFormularioId: rec.data.id,
                                    arrayEstadosOrigen: JSON.stringify(arrayEstadosOrigen)
                                }
                            });
                    }
                });
                this.cerrarVentanaCampanyaFormularios();
            }
            else {
                Ext.Msg.alert('Afegir Formulari', 'El formulari no es correcte');
            }
        },

        actualizarCampanyaFormulario: function () {

            var form = this.getFormFormularioCampanya();

            if (form.getForm().isValid()) {

                var store = this.getStoreCampanyaFormulariosStore();
                var record = store.findRecord('id', this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaFormularios()));

                record.set('titulo', form.down("textfield[name=titulo]").getValue());
                record.set('plantilla', form.down("combobox[name=plantilla]").getValue());
                record.set('cabecera', form.down("textfield[name=cabecera]").getValue());
                record.set('solicitarCorreo', form.down("combobox[name=solicitarCorreo]").getValue());
                record.set('solicitarIdentificacion', form.down("combobox[name=solicitarIdentificacion]").getValue());
                record.set('solicitarMovil', form.down("combobox[name=solicitarMovil]").getValue());
                record.set('solicitarNombre', form.down("combobox[name=solicitarNombre]").getValue());
                record.set('solicitarPostal', form.down("combobox[name=solicitarPostal]").getValue());
                record.set('autenticaFormulario', form.down("checkbox[name=autenticaFormulario]").getValue());
                record.set('fechaInicio', form.down("datefield[name=fechaInicio]").getValue());
                record.set('fechaFin', form.down("datefield[name=fechaFin]").getValue());
                record.set('estadoDestinoId', form.down("radiofield[name=radioboxEstadoDestino]").getGroupValue());
                record.set('actoId', form.down("combobox[name=acto]").getValue());

                store.sync();

                var estadosOrigen = form.down("checkboxgroup[name=checkboxfieldEstadosOrigen]").getChecked();
                var arrayEstadosOrigen = [];
                for (var i in estadosOrigen) {
                    var obj = {
                        id: estadosOrigen[i].inputValue
                    };
                    arrayEstadosOrigen.push(obj);
                }
                Ext.Ajax.request(
                    {
                        url: '/crm/rest/campanyaformularioestadoorigen/',
                        method: 'POST',
                        params: {
                            campanyaFormularioId: record.get('id'),
                            arrayEstadosOrigen: JSON.stringify(arrayEstadosOrigen)
                        }
                    });
                this.cerrarVentanaCampanyaFormularios();
            }
            else {
                Ext.Msg.alert('Afegir Formularis', 'El formulari no es correcte');
            }
        },

        borrarFormularioCampanya: function () {
            var ref = this;
            Ext.Msg.confirm('Eliminació de Formularis', '<b>Esteu segur/a de voler esborrar el formulari sel·leccionat?</b>', function (btn) {
                if (btn == 'yes') {
                    var formulario = ref.getGridCampanyaFormularios().getSelectionModel().getSelection();
                    if (formulario.length == 1) {
                        var registro = formulario[0];
                        var store = ref.getStoreCampanyaFormulariosStore();
                        store.remove(registro);
                        store.sync({
                            callback : function(){
                                ref.getController("ControllerPanelCampanyas").setEstadoComponentes();
                            }
                        });
                    }
                }
            });
        },

        duplicarFormularioCampanya: function () {
            var formulario = this.getGridCampanyaFormularios().getSelectionModel().getSelection();
            var store = this.getStoreCampanyaFormulariosStore();
            Ext.Ajax.request(
                {
                    url: '/crm/rest/campanyaformulario/duplicar',
                    method: 'POST',
                    params: {
                        campanyaFormularioId: formulario[0].data.id,
                    },
                    scope: this,
                    callback: function () {
                        store.reload();
                    }
                });
        },


//////////////////////////////// VENTANA DATOS ///////////////////////////////////////////////////////////////

        getVentanaCampanyaDatosView: function () {
            return this.getView('campanyas.formularios.VentanaCampanyaDatos').create();
        }
        ,

        mostrarVentanaCampanyaDatos: function () {

            this.ventanaCampanyaDatos = this.getVentanaCampanyaDatosView();
            this.getBotonActualizarDatoCampanya().setVisible(false);
            this.getBotonAnyadirDatoCampanya().setVisible(true);
            this.ventanaCampanyaDatos.show();
        }
        ,

        mostrarVentanaActualizarCampanyaDato: function (obj, record) {

            this.ventanaCampanyaDatos = this.getVentanaCampanyaDatosView();

            var form = this.getFormDatoCampanya();

            form.down("combobox[name=clienteDatoTipo]").setValue(record.data.clienteDatoTipoId);
            form.down("textfield[name=orden]").setValue(record.data.orden);
            form.down("textfield[name=tamanyo]").setValue(record.data.tamanyo);
            form.down("textfield[name=textoCa]").setValue(record.data.textoCa);
            form.down("textfield[name=textoEs]").setValue(record.data.textoEs);
            form.down("textfield[name=textoUk]").setValue(record.data.textoUk);
            form.down("textfield[name=textoAyudaCa]").setValue(record.data.textoAyudaCa);
            form.down("textfield[name=textoAyudaEs]").setValue(record.data.textoAyudaEs);
            form.down("textfield[name=textoAyudaUk]").setValue(record.data.textoAyudaUk);
            form.down("combobox[name=grupo]").setValue(record.data.grupoId);
            form.down("combobox[name=item]").setValue(record.data.itemId);
            form.down("checkbox[name=requerido]").setValue(record.data.requerido);
            form.down("combobox[name=tipoAcceso]").setValue(record.data.tipoAccesoId);
            form.down("checkbox[name=vinculadoCampanya]").setValue(record.data.vinculadoCampanya);
            form.down("combobox[name=campanyaDatoExtra]").setValue(record.data.campanyaDatoExtraId);
            form.down("textfield[name=etiqueta]").setValue(record.data.etiqueta);
            form.down("combobox[name=validar]").setValue(record.data.tipoValidacionId);


            // form.down("combobox[name=campanyaPreguntaRelacionada]").setVisible(!record.data.visualizar);
            // form.down("combobox[name=campanyaRespuestaRelacionada]").setVisible(!record.data.visualizar);

            if (!record.data.visualizar) {
                form.down("radiofield[id=radio2]").setValue(true);

                form.down("combobox[name=campanyaPreguntaRelacionada]").setValue(record.data.preguntaRelacionadaId);
                form.down("combobox[name=campanyaRespuestaRelacionada]").setValue(record.data.respuestaRelacionadaId);
            }
            else {
                form.down("radiofield[id=radio1]").setValue(true);
            }


            this.getBotonActualizarDatoCampanya().setVisible(true);
            this.getBotonAnyadirDatoCampanya().setVisible(false);
            this.ventanaCampanyaDatos.show();
        }
        ,

        cerrarVentanaCampanyaDatos: function () {
            this.ventanaCampanyaDatos.destroy();
        },

        cargaStoreFormularios: function (campanyaId) {
            var storeCampanyaFormularios = this.getStoreCampanyaFormulariosStore();
            storeCampanyaFormularios.load(
                {
                    params: {
                        campanyaId: campanyaId
                    },
                    scope: this,
                    callback: function () {
                        //this.setEstadoComponentes();
                        this.getDireccionGenerica().campanyaId = campanyaId;
                    }
                });
        }
        ,

////////////////////////////////// GRID CAMPANYA FORMULARIOS ///////////////////////////////////////////
        onGridCampanyaFormulariosSelectionChange: function () {
            var tieneSeleccionGridFormularios = this.getGridCampanyaFormularios().getSelectionModel().hasSelection();
            this.getBotonGridCampanyaFormularioBorrar().setDisabled(!tieneSeleccionGridFormularios);
            this.getBotonGridCampanyaFormularioDuplicar().setDisabled(!tieneSeleccionGridFormularios);

        }
        ,

////////////////////////////////////// GRID CAMPANYA DATOS ////////////////////////////////////////////
        onGridCampanyaDatosSelectionChange: function () {
            this.setEstadoComponentes();
        }
        ,

        setEstadoComponentes: function () {

            var tieneSeleccionGridCampanyaDatos = this.getGridCampanyaDatos().getSelectionModel().hasSelection();
            this.getBotonBorrarCampanyaDatos().setDisabled(!tieneSeleccionGridCampanyaDatos);

        }

    })
;