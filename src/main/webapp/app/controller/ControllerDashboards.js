Ext.define('CRM.controller.ControllerDashboards',
{
    extend : 'Ext.app.Controller',
    views : [ 'dashboard.PanelDashboard', 'ApplicationViewport' ],
    stores : ['StoreServicios']

});