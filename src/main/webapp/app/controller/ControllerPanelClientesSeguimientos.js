Ext.define('CRM.controller.ControllerPanelClientesSeguimientos', {
    extend: 'Ext.app.Controller',
    stores: ['StoreSeguimientos'],
    models: ['Seguimiento'],
    views: ['clientes.GridSeguimientos', 'clientes.VentanaSeguimiento'],

    ventanaSeguimiento: {},

    refs: [{
        selector: 'gridSeguimientos',
        ref: 'gridSeguimientos'
    }, {
        selector: 'gridSeguimientos button[name=borrar]',
        ref: 'botonBorrarSeguimiento'
    // }, {
    //     selector: 'gridSeguimientos button[name=editar]',
    //     ref: 'botonEditarSeguimiento'
    }, {
        selector: 'ventanaSeguimiento',
        ref: 'ventanaSeguimiento'
    }, {
        selector: 'ventanaSeguimiento form',
        ref: 'ventanaSeguimientoForm'
    }],

    dirty: false,

    init: function () {

        this.control(
            {
                'gridSeguimientos button[name=anyadir]': {
                    click: function () {
                        this.anyadirSeguimiento();
                    }
                },
                'gridSeguimientos button[name=editar]': {
                    click: function () {
                        this.editarSeguimiento();
                    }
                },
                'gridSeguimientos button[name=borrar]': {
                    click: function () {
                        this.borrarSeguimiento();
                    }
                },
                'gridSeguimientos': {
                    select: function () {
                        this.setEstadoComponentes();
                    },
                    edit: function () {
                        this.dameStoreSeguimientos().sync();
                    },
                },
                'ventanaSeguimiento button[action=cancelar]': {
                    click: function () {
                        this.cerrarVentanaSeguimiento();
                    }
                },
                'ventanaSeguimiento button[action=guardar]': {
                    click: function () {
                        this.guardarSeguimiento();
                    }
                }
            });
    },

    dameStoreSeguimientos: function () {
        var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
        var clienteId = tabsServiciosCliente.getActiveTab().itemId;
        return tabsServiciosCliente.down("[itemId=" + clienteId + "]").down("[itemId=gridSeguimientos-" + clienteId + "]").getStore();
    },

    getVentanaSeguimientoView: function () {
        return this.getView('clientes.VentanaSeguimiento').create();
    },

    cerrarVentanaSeguimiento: function () {
        this.ventanaSeguimiento.destroy();
    },

    anyadirSeguimiento: function () {

        var seguimiento = this.getSeguimientoModel().create({
            clienteId: this.getController("ControllerPanelClientes").dameIdCliente(),
            fecha: new Date()
        });

        var store = this.dameStoreSeguimientos();

        var rowEditor = this.getGridSeguimientos().getPlugin("rowediting");
        rowEditor.cancelEdit();
        store.insert(0, seguimiento);
        rowEditor.startEdit(0, 0);

    },

    editarSeguimiento: function () {
        var seguimientos = this.getGridSeguimientos().getSelectionModel().getSelection();

        if (seguimientos.length == 1) {
            this.ventanaSeguimiento = this.getVentanaSeguimientoView();
            this.getVentanaSeguimientoForm().getForm().loadRecord(seguimientos[0]);
            this.ventanaSeguimiento.show();
        }
    },

    borrarSeguimiento: function () {
        var store = this.dameStoreSeguimientos();
        var fila = this.getGridSeguimientos().getSelectionModel().getSelection()[0];

        var rec = store.findRecord('id', fila.data.id);
        var ref = this;

        Ext.Msg.confirm('Eliminar el seguimient', 'El seguiment s\'eliminarà del client. Estàs segur de voler continuar?', function (btn) {
            if (btn == 'yes') {
                store.remove(rec);
                store.sync(
                    {
                        success: function () {
                            ref.setEstadoComponentes();
                        },
                        failure: function () {
                            Ext.Msg.alert('Esborrar Seguimient', 'No s`ha pogut esborrar el seguiment.');
                            store.rejectChanges();
                        }
                    });
            }

        });
    },

    // rellenaGridSeguimientos: function (clienteId) {
    //     var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
    //     var store = tabsServiciosCliente.down("[itemId=" + clienteId + "]").down("[itemId=gridSeguimientos-" + clienteId + "]").getStore();
    //     store.load(
    //         {
    //             params: {
    //                 clienteId: clienteId
    //             },
    //             scope: this,
    //             callback: function () {
    //                 this.setEstadoComponentes();
    //             }
    //         });
    // },

    guardarSeguimiento: function () {
        var form = this.getVentanaSeguimientoForm().getForm();

        if (form.isValid()) {
            var seguimiento;
            var store = this.dameStoreSeguimientos();
            var seguimientoId = form.findField('id').getValue();
            var ref = this;

            if (seguimientoId === '') {
                seguimiento = this.getSeguimientoModel().create();
                store.add(seguimiento);
            } else {
                seguimiento = store.findRecord('id', seguimientoId);
            }

            seguimiento.set(form.getValues(false, false, false, true));

            store.sync(
                {
                    success: function (response) {
                        store.reload();
                        ref.setEstadoComponentes();
                    }
                });
            ref.cerrarVentanaSeguimiento();
        }
    },
    //
    // actualizarIdsFilaGrid: function (editor, e) {
    //     var fila = editor.grid.getSelectionModel().getSelection()[0];
    //
    //     if (fila.data.seguimientoTipoNombre != e.newValues.seguimientoTipoNombre) {
    //         fila.data.seguimientoTipoId = e.newValues.seguimientoTipoNombre;
    //     }
    //     if (fila.data.campanyaNombre != e.newValues.campanyaNombre) {
    //         fila.data.campanyaId = e.newValues.campanyaNombre;
    //     }
    // },

    setEstadoComponentes: function () {
        var tieneSeleccionGridSeguimientos = this.getGridSeguimientos().getSelectionModel().hasSelection();
        this.getBotonBorrarSeguimiento().setDisabled(!tieneSeleccionGridSeguimientos);
    }

});