Ext.define('CRM.controller.ControllerPanelGrupos',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreGrupos', 'StoreTiposAcceso', 'StoreItems', 'StoreProgramasAccesoAdmin' ],
    models : [ 'Grupo' ],
    views : [ 'grupos.FiltroGrupos', 'grupos.GridGrupos', 'grupos.PanelGrupos', 'grupos.VentanaGrupos' ],

    ventanaGrupos : null,

    refs : [
    {
        selector : 'filtroGrupos combo[name=programa]',
        ref : 'filtroComboPrograma'
    },
    {
        selector : 'gridGrupos',
        ref : 'gridGrupos'
    },
    {
        selector : 'gridGrupos button[name=anyadirGrupo]',
        ref : 'botonAnyadirGrupo'
    },
    {
        selector : 'ventanaGrupos [name=formGrupo]',
        ref : 'formGrupos'
    },
    {
        selector : 'gridGrupos button[name=borrarGrupo]',
        ref : 'botonBorrarGrupo'
    },
    {
        selector : 'gridItems',
        ref : 'gridItems'
    },
    {
        selector : 'gridItems button[name=borrarItem]',
        ref : 'botonBorrarItem'
    },
    {
        selector : 'panelGrupos [name=tabItems]',
        ref : 'tabItems'
    } ],

    dirty : false,

    init : function()
    {
        this.control(
        {
            'panelGrupos' :
            {
                render : function()
                {
                    this.getStoreGruposStore().loadData([], false);
                    this.getStoreItemsStore().loadData([], false);
                    if (this.ventanaGrupos === null)
                    {
                        this.ventanaGrupos = this.getView('grupos.VentanaGrupos').create();
                    }
                }
            },
            'filtroGrupos combobox[name=programa]' :
            {
                select : this.onProgramaSelected
            },
            'gridGrupos button[name=anyadirGrupo]' :
            {
                click : this.anyadirGrupo
            },
            'gridGrupos button[name=borrarGrupo]' :
            {
                click : this.borrarGrupo
            },
            'gridGrupos' :
            {
                select : this.onGrupoSelected,
                itemdblclick : this.mostrarVentanaGrupos
            },
            'ventanaGrupos button[name=guardarGrupo]' :
            {
                click : this.guardarGrupo
            },
            'filtroGrupos button[name=limpiarFiltros]' :
            {
                click : this.limpiarFiltros
            }
        });
    },

    onProgramaSelected : function(combo, records)
    {
        this.cargaStoreGrupos(records[0].get('id'));
    },

    limpiarFiltros : function()
    {
        this.getFiltroComboPrograma().clearValue();
        this.cargaStoreGrupos(null);
    },

    cargaStoreGrupos : function(programaId)
    {
        var storeGrupos = this.getStoreGruposStore();
        var ref = this;

        this.getStoreItemsStore().loadData([], false);

        if (programaId !== null)
        {
            storeGrupos.load(
            {
                params :
                {
                    programaId : programaId
                },
                callback : function()
                {
                    ref.setEstadoComponentes();
                },
                scope : this
            });
        }
        else
        {
            this.getStoreGruposStore().loadData([], false);
            ref.setEstadoComponentes();
        }
    },

    cargaStoreItems : function(grupoId)
    {
        var store = this.getStoreItemsStore();
        store.load(
        {
            params :
            {
                grupoId : grupoId
            },
            scope : this
        });
    },

    onGrupoSelected : function(panel, record)
    {
        this.cargaStoreItems(record.getId());
        this.setEstadoComponentes();
    },

    anyadirGrupo : function()
    {
        var grupo = Ext.create('CRM.model.Grupo');
        grupo.set('programaId', this.getFiltroComboPrograma().getValue());
        this.getFormGrupos().loadRecord(grupo);
        this.ventanaGrupos.show();
    },

    borrarGrupo : function()
    {
        var ref = this;
        var store = this.getStoreGruposStore();
        var selection = this.getGridGrupos().getSelectionModel().getSelection();
        var grupoId = (selection.length > 0) ? selection[0].data.id : null;
        var grupo = store.findRecord('id', grupoId);

        Ext.Msg.confirm('Eliminar el suscriptor', 'Totes les dades d\'aquest suscriptor s\'esborraràn. Estàs segur de voler continuar?', function(btn, text)
        {
            if (btn == 'yes')
            {
                store.remove(grupo);

                store.sync(
                {
                    success : function()
                    {
                        ref.setEstadoComponentes();
                        ref.getFormGrupos().getForm().reset();
                    },
                    failure : function()
                    {
                        store.rejectChanges();
                        ref.setEstadoComponentes();
                        Ext.Msg.alert('Error', 'No s\'ha pogut esborrar el suscriptor perque té items asignats.');
                    }
                });
            }
        });
    },

    guardarGrupo : function()
    {
        var form = this.getFormGrupos().getForm();

        if (form.isValid())
        {
            var grupo;
            var store = this.getStoreGruposStore();
            var grupoId = form.findField('id').getValue();
            var ref = this;

            if (grupoId === '')
            {
                grupo = Ext.create('CRM.model.Grupo');
                store.add(grupo);
            }
            else
            {
                grupo = store.findRecord('id', grupoId);
            }

            grupo.set(form.getValues(false, false, false, true));

            store.sync(
            {
                success : function()
                {
                    store.reload();
                    ref.ventanaGrupos.hide();
                    ref.setEstadoComponentes();
                }
            });
        }
    },

    mostrarVentanaGrupos : function(grid, record)
    {
        this.getFormGrupos().loadRecord(record);
        this.ventanaGrupos.show();
    },

    setEstadoComponentes : function()
    {
        var valorFiltroPrograma = this.getFiltroComboPrograma().getValue();
        this.getGridGrupos().setDisabled(valorFiltroPrograma === null);

        var tieneSeleccionGridGrupos = this.getGridGrupos().getSelectionModel().hasSelection();
        var tieneSeleccionGridItems = this.getGridItems().getSelectionModel().hasSelection();
        this.getBotonBorrarGrupo().setDisabled(!tieneSeleccionGridGrupos);
        this.getBotonBorrarItem().setDisabled(!tieneSeleccionGridItems);
        this.getGridItems().setDisabled(!tieneSeleccionGridGrupos);
        this.getTabItems().setDisabled(!tieneSeleccionGridItems);
    }

});