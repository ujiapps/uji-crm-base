Ext.define('CRM.controller.ControllerPanelServicios',
    {
        extend: 'Ext.app.Controller',
        stores: ['StoreServicios', 'StoreServicioUsuario', 'StoreProgramas', 'StoreLineasFacturacion'],

        models: ['Servicio', 'Programa', 'ServicioUsuario', 'LineaFacturacion'],
        views: ['personas.VentanaNewPersona', 'servicios.GridServicios', 'personas.GridPersonas', 'servicios.GridLineasFacturacion'],
        ventanaPersona: {},

        getVentanaNewPersonaView: function () {
            return this.getView('personas.VentanaNewPersona').create();
        },

        refs: [
            {
                selector: 'gridServicios',
                ref: 'gridServicios'
            },
            {
                selector: 'panelServicios tabpanel',
                ref: 'tabServicios'
            },
            {
                selector: 'gridServicios button[name=borrarServicio]',
                ref: 'botonBorrarServicio'
            },
            {
                selector: 'ventanaNewPersona',
                ref: 'ventanaNewPersona'
            },
            {
                selector: 'ventanaNewPersona combo[name=comboPersona]',
                ref: 'comboPersona'
            },
            {
                selector: 'ventanaNewPersona form[name=formNewPersona]',
                ref: 'formNewPersona'
            },

            {
                selector: 'gridPersonas',
                ref: 'gridPersonas'
            },
            {
                selector: 'gridPersonas button[name=borrarPersona]',
                ref: 'botonBorrarPersona'
            },
            {
                selector: 'gridPersonas button[name=anyadirPersona]',
                ref: 'botonAnyadirPersona'
            },

            {
                selector: 'gridLineasFacturacion',
                ref: 'gridLineasFacturacion'
            },
            {
                selector: 'gridLineasFacturacion button[name=borrarLineaFacturacion]',
                ref: 'botonBorrarLineaFacturacion'
            }],

        dirty: false,
        init: function () {

            this.control(
                {
                    'gridServicios button[name=borrarServicio]': {
                        click: this.borrarServicio
                    },
                    'gridServicios button[name=anyadirServicio]': {
                        click: this.anyadirServicio
                    },
                    'gridServicios ': {
                        select: this.cargaServicioDatos,
                        edit: this.sincronizarServicio
                    },
                    'gridPersonas button[name=borrarPersona]': {
                        click: this.borraPersona
                    },
                    'gridPersonas button[name=anyadirPersona]': {
                        click: this.showVentanaAddPersona
                    },
                    'gridPersonas': {
                        select: this.setEstadoComponentes
                    },
                    'ventanaNewPersona button[action=cancelar]': {
                        click: this.closeVentanaAddPersona
                    },

                    'ventanaNewPersona button[action=guardar-persona]': {
                        click: this.guardarPersona
                    },
                    'gridLineasFacturacion': {
                        edit: this.sincronizarLineasFacturacion,
                        select: this.setEstadoComponentes
                    },
                    'gridLineasFacturacion button[name=anyadirLineaFacturacion]': {
                        click: this.guardarLineaFacturacion
                    },
                    'gridLineasFacturacion button[name=borrarLineaFacturacion]': {
                        click: this.borrarLineaFacturacion
                    }


                });
        },

        //-------------------------------------------------  SERVICIO  ----------------------------------------------------------------

        borrarServicio: function () {
            var ref = this;
            var store = this.getStoreServiciosStore();
            var servicio = store.findRecord('id', this.getController("ControllerVarios").dameIdGrid(this.getGridServicios()));

            if (servicio) {
                Ext.Msg.confirm('Eliminació de serveis', '<b>Esteu segur/a de voler esborrar les serveis sel·leccionats?</b>', function (btn) {
                    if (btn == 'yes') {

                        store.remove(servicio);
                        store.sync(
                            {
                                callback: function () {
                                    ref.setEstadoComponentes();
                                }
                            });
                    }
                });
            }

        },

        anyadirServicio: function () {
            var store = this.getStoreServiciosStore();
            var rec = this.getServicioModel().create();
            var rowEditor = this.getGridServicios().getPlugin('editingServicio');

            rowEditor.cancelEdit();
            store.insert(0, rec);
            rowEditor.startEdit(0, 0);
        },

        sincronizarServicio: function () {
            this.getStoreServiciosStore().sync(
                {
                    scope: this,
                    callback: function () {
                        this.setEstadoComponentes();
                    }
                });
        },

        //---------------------------------------  VENTANA PERSONAS  --------------------------------------------

        showVentanaAddPersona: function () {
            this.ventanaPersona = this.getVentanaNewPersonaView();
            this.ventanaPersona.show();
        },

        closeVentanaAddPersona: function () {
            this.ventanaPersona.destroy();
        },

        //---------------------------------------- GRID PERSONAS ------------------------------------------------------

        guardarPersona: function () {
            var comboPersona = this.getComboPersona();

            var personaId = comboPersona.getValue();
            var servicioId = this.getController("ControllerVarios").dameIdGrid(this.getGridServicios());

            var persona = this.getServicioUsuarioModel().create(
                {
                    personaId: personaId,
                    servicioId: servicioId
                });

            var store = this.getStoreServicioUsuarioStore();
            store.add(persona);
            store.sync(
                {
                    scope: this,
                    success: function () {
                        this.closeVentanaAddPersona();
                        this.cargaGridPersonas();

                    },
                    failure: function () {
                        Ext.Msg.alert('Asociació de persones a Serveis', '<b>No s\'ha pogut afegir a la persona al servei seleccionat</b>');
                        this.cargaGridPersonas();
                    }
                });
        },

        borraPersona: function () {
            var ref = this;

            var store = ref.getStoreServicioUsuarioStore();
            var servicioUsuario = store.findRecord('id', this.getController("ControllerVarios").dameIdGrid(this.getGridPersonas()));

            if (servicioUsuario) {
                Ext.Msg.confirm('Eliminació de personas', '<b>Esteu segur/a de voler esborrar les persones sel·leccionats?</b>', function (btn) {
                    if (btn == 'yes') {
                        store.remove(servicioUsuario);
                        store.sync(
                            {
                                callback: function () {
                                    ref.cargaGridPersonas();
                                }
                            })
                    }
                });
            }
        },

        cargaGridPersonas: function (servicioId) {

            if (servicioId) {
                var store = this.getStoreServicioUsuarioStore();
                store.load(
                    {
                        url: '/crm/rest/serviciousuario/' + servicioId,
                        scope: this,
                        callback: function () {
                            this.setEstadoComponentes();
                        }
                    });
            }
            else {
                this.limpiarGridPersonas();
            }
        },

        limpiarGridPersonas: function () {
            this.getStoreServicioUsuarioStore().loadData([], false);
            this.getTabServicios().setDisabled(true);
        },


        //-----------------------------------------LINEAS FACTURACION--------------------------------------------

        guardarLineaFacturacion: function () {

            var servicioId = this.getController("ControllerVarios").dameIdGrid(this.getGridServicios());

            var lineaFacturacion = this.getLineaFacturacionModel().create(
                {
                    servicioId: servicioId
                });

            var rowEditor = this.getGridLineasFacturacion().getPlugin('editingLineaFacturacion');
            var store = this.getStoreLineasFacturacionStore();

            rowEditor.cancelEdit();
            store.insert(0, lineaFacturacion);
            rowEditor.startEdit(0, 0);
        },

        sincronizarLineasFacturacion: function () {
            var store = this.getStoreLineasFacturacionStore();
            var servicioId = this.getController("ControllerVarios").dameIdGrid(this.getGridServicios());

            store.sync({
                scope: this,
                callback: function () {
                    this.cargaGridLineasFacturacion(servicioId);
                }
            });
        },

        borrarLineaFacturacion: function () {
            var ref = this;
            var servicioId = this.getController("ControllerVarios").dameIdGrid(this.getGridServicios());

            var store = this.getStoreLineasFacturacionStore();
            var lineaFacturacion = store.findRecord('id', this.getController("ControllerVarios").dameIdGrid(this.getGridLineasFacturacion()));

            if (lineaFacturacion) {
                Ext.Msg.confirm('Eliminació de llínies de facturació', '<b>Esteu segur/a de voler esborrar les llinies sel·leccionats?</b>', function (btn) {
                    if (btn == 'yes') {
                        store.remove(lineaFacturacion);
                        store.sync(
                            {
                                callback: function () {
                                    ref.cargaGridLineasFacturacion(servicioId);
                                }
                            })
                    }
                });
            }
        },


        cargaGridLineasFacturacion: function (servicioId) {

            if (servicioId) {
                var store = this.getStoreLineasFacturacionStore();
                store.load(
                    {
                        url: '/crm/rest/lineafacturacion/',
                        params: {
                            servicioId: servicioId
                        },
                        scope: this,
                        callback: function () {
                            this.setEstadoComponentes();
                        }
                    });
            }
            else {
                this.limpiarGridLineasFacturacion();
            }
        },

        limpiarGridLineasFacturacion: function () {
            this.getStoreLineasFacturacionStore().loadData([], false);
            this.getTabServicios().setDisabled(true);
        },

        //------------------------------------------  VARIOS  ----------------------------------------------------


        cargaServicioDatos: function () {
            var servicioId = this.getController("ControllerVarios").dameIdGrid(this.getGridServicios());

            this.cargaGridPersonas(servicioId);
            this.cargaGridLineasFacturacion(servicioId);
        },

        setEstadoComponentes: function () {

            var servicioId = this.getController("ControllerVarios").dameIdGrid(this.getGridServicios());
            var tieneSeleccionGridServicios = this.getGridServicios().getSelectionModel().hasSelection();

            this.getTabServicios().setDisabled(!tieneSeleccionGridServicios);

            var storeProgramas = new Ext.data.Store(
                {
                    proxy: this.getStoreProgramasStore().getProxy(),
                    model: 'CRM.model.Programa',
                    autoLoad: false
                });

            storeProgramas.load(
                {
                    url: '/crm/rest/programa/' + servicioId,
                    scope: this,
                    callback: function (records) {
                        this.getBotonBorrarServicio().setDisabled(!(tieneSeleccionGridServicios && !records.length));
                    }
                });

            var tieneSeleccionGridPersonas = this.getGridPersonas().getSelectionModel().hasSelection();
            this.getBotonBorrarPersona().setDisabled(!tieneSeleccionGridPersonas);

            var tieneSeleccionGridLineasFacturacion = this.getGridLineasFacturacion().getSelectionModel().hasSelection();
            this.getBotonBorrarLineaFacturacion().setDisabled(!tieneSeleccionGridLineasFacturacion);
        }

    });