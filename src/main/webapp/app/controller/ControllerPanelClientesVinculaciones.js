Ext.define('CRM.controller.ControllerPanelClientesVinculaciones',
    {
        extend: 'Ext.app.Controller',
        stores: ['StoreVinculaciones', 'StoreVinculacionesInversas', 'StoreTiposVinculaciones'],
        models: ['Vinculacion'],
        views: ['clientes.GridClientesVinculos', 'clientes.GridClientesVinculosInversos', 'clientes.VentanaNuevaVinculacion', 'CRM.view.clientes.PanelVinculaciones'],

        ventanaNuevaVinculacion: {},

        getVentanaNuevaVinculacionView: function () {
            return this.getView('clientes.VentanaNuevaVinculacion').create();
        },

        refs: [{
            selector: 'panelVinculaciones',
            ref: 'panelVinculaciones'
        }, {
            selector: 'gridClientesVinculos',
            ref: 'gridClientesVinculos'
        }, {
            selector: 'ventanaNuevaVinculacion combo[name=comboClientesVinculaciones]',
            ref: 'comboClientesVinculaciones'
        }, {
            selector: 'ventanaNuevaVinculacion combo[name=comboTipoVinculo]',
            ref: 'comboTipoVinculo'
        }, {
            selector: 'gridClientesVinculos button[name=borrarVinculo]',
            ref: 'botonBorrarVinculo'
        }],

        dirty: false,
        init: function () {

            this.control({
                'gridClientesVinculos button[name=anyadirVinculo]': {
                    click: this.mostrarVentanaNuevaVinculacion
                },

                'gridClientesVinculos button[name=borrarVinculo]': {
                    click: this.borrarVinculacion
                },

                'gridClientesVinculos': {
                    select: function () {
                        this.getBotonBorrarVinculo().setDisabled(false);
                    }
                },

                'ventanaNuevaVinculacion button[action=cancelar-vinculacion]': {
                    click: this.cerrarVentanaNuevaVinculacion

                },

                'ventanaNuevaVinculacion button[action=guardar-vinculacion]': {
                    click: this.guardarNuevaVinculacion
                }
            })
            ;
        },

        mostrarVentanaNuevaVinculacion: function () {
            this.ventanaNuevaVinculacion = this.getVentanaNuevaVinculacionView();
            this.ventanaNuevaVinculacion.show();
        },

        cerrarVentanaNuevaVinculacion: function () {
            this.ventanaNuevaVinculacion.destroy();
        },

        borrarVinculacion: function () {
            var ref = this;

            // var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
            // var clienteId = tabsServiciosCliente.getActiveTab().itemId;
            var gridVinculacion = this.dameGridVinculaciones();
            var vinculacion = gridVinculacion.getSelectionModel().getSelection();

            if (vinculacion.length == 1) {
                Ext.Msg.confirm('Esborrar', 'Esteu segur/a de voler esborrar el registre sel·leccionat?', function (btn) {
                    if (btn == 'yes') {
                        Ext.Ajax.request({
                            url: '/crm/rest/vinculacion/' + vinculacion[0].data.id,
                            method: 'DELETE',
                            scope: this,
                            success: function () {
                                gridVinculacion.getStore().load();
                            },
                            failure: function () {
                                Ext.Msg.alert('Enviament', '<b>No s\'han pogut eliminar</b>');
                            }
                        });
                    }
                });
            }
        },

        dameGridVinculaciones: function(){
            var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
            var clienteId = tabsServiciosCliente.getActiveTab().itemId;
            return tabsServiciosCliente.down("[itemId=" + clienteId + "]").down("[itemId=gridVinculos-" + clienteId + "]");
        },

        guardarNuevaVinculacion: function () {

            var cliente2Id = this.getComboClientesVinculaciones().getValue();

            if (cliente2Id != null) {
                var tipoVinculo = this.getComboTipoVinculo().getValue();

                var vinculacion = this.getVinculacionModel().create(
                    {
                        cliente2Id: cliente2Id,
                        tipoId: tipoVinculo
                    });

                var storeVinculaciones = this.dameGridVinculaciones().getStore();

                storeVinculaciones.add(vinculacion);
                storeVinculaciones.sync(
                    {
                        scope: this,
                        success: function () {
                            this.cerrarVentanaNuevaVinculacion();
                            this.actualizaGrid();
                        }
                    });
            }
            else {
                Ext.Msg.alert('Insercció de vinculacions', '<b>No es pot insertar la vinculació</b>');
            }

        },

        actualizaGrid : function(){
            this.dameGridVinculaciones().getStore().load();
        },

        setEstadoComponentes : function(){

        }

    });