Ext.define('CRM.controller.ControllerPanelClientesPagos',
    {
        extend: 'Ext.app.Controller',
        stores: ['StoreRecibosCliente', 'StoreTiposIdioma', 'StoreLineasRecibo', 'StoreMovimientosRecibo', 'StoreRecibos'],
        views: ['clientes.PestanyaPagos.PanelClientesPagos', 'pagos.recibos.VentanaRecibo', 'pagos.recibos.GridLineasRecibo', 'pagos.recibos.GridMovimientosRecibo'
        ],
        models: [],

        ventanaRecibo: {},
        getVentanaRecibo: function () {
            return this.getView('pagos.recibos.VentanaRecibo').create();
        },

        refs: [
            {
                selector: 'panelClientesPagos',
                ref: 'panelClientesPagos'
            },
            {
                selector: 'ventanaRecibo form[name=formRecibo]',
                ref: 'formRecibo'
            }
        ],

        dirty: false,
        init: function () {
            this.control(
                {
                    'panelClientesPagos grid[name=gridClientesPagos]': {
                        itemdblclick: function (obj, record) {
                            this.showVentanaRecibo(record);
                        }
                    }
                });
        },

        // rellenaPanelClientesPagos: function (personaId) {
        //     var store = this.getStoreRecibosClienteStore();
        //     if (personaId) {
        //         store.load({
        //             url: 'rest/recibo/' + personaId
        //         });
        //     }
        //     else {
        //         store.loadData([], false);
        //     }
        //
        // },

        showVentanaRecibo: function (recibo) {
            this.ventanaRecibo = this.getVentanaRecibo();

            var form = this.getFormRecibo();
            form.loadRecord(recibo);

            var storeLineas = this.getStoreLineasReciboStore();
            storeLineas.load({
                url: 'rest/linea/recibo/' + recibo.data.id
            });

            var storeMovimientos = this.getStoreMovimientosReciboStore();
            storeMovimientos.load({
                url: 'rest/movimientorecibo/recibo/' + recibo.data.id
            });

            this.ventanaRecibo.show();
        },

        closeVentanaRecibo: function () {
            this.ventanaRecibo.destroy();
        }
    });