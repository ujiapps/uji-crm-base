Ext.define('CRM.controller.ControllerPanelTiposSeguimientos',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreSeguimientoTipos' ],
    models : [ 'SeguimientoTipo' ],

    refs : [
    {
        selector : 'gridTiposSeguimientos',
        ref : 'gridTiposSeguimientos'
    },
    {
        selector : 'gridTiposSeguimientos button[name=borrarTipoSeguimientos]',
        ref : 'botonBorrarTipoSeguimientos'
    } ],

    init : function()
    {
        this.control(
        {
            'gridTiposSeguimientos button[name=borrarTipoSeguimientos]' :
            {
                click : this.borrarTipoSeguimientos
            },
            'gridTiposSeguimientos button[name=anyadirTipoSeguimientos]' :
            {
                click : this.anyadirTipoSeguimientos
            },
            'gridTiposSeguimientos' :
            {
                select : this.setEstadoComponentes,
                edit: this.guardaTipoSeguimientos
            }
        });
    },

    anyadirTipoSeguimientos : function()
    {
        var store = this.getStoreSeguimientoTiposStore();
        var tipoSeguimientos = this.getSeguimientoTipoModel().create();
        var rowEditor = this.getGridTiposSeguimientos().getPlugin('editingTipoSeguimientos');

        rowEditor.cancelEdit();
        store.insert(0, tipoSeguimientos);
        rowEditor.startEdit(0, 0);
    },

    borrarTipoSeguimientos : function()
    {
        var ref = this;
        var store = ref.getStoreSeguimientoTiposStore();
        var selection = this.getGridTiposSeguimientos().getSelectionModel().getSelection();
        var TipoSeguimientosId = (selection.length > 0) ? selection[0].data.id : null;
        var TipoSeguimientos = store.findRecord('id', TipoSeguimientosId);

        if (TipoSeguimientos)
        {
            Ext.Msg.confirm('Esborrar Vinculació', 'Esteu segur/a de voler esborrar el registre sel·leccionada?', function(btn)
            {
                if (btn == 'yes')
                {
                    store.remove(TipoSeguimientos);
                    store.sync(
                    {
                        success : function()
                        {
                            ref.setEstadoComponentes();
                        },
                        failure : function()
                        {
                            Ext.Msg.alert('Esborrar Vinculació', 'No s`ha pogut esborrar la vinculació registre perquè té registres associats.');
                            store.rejectChanges();
                            ref.setEstadoComponentes();
                        }
                    });
                }
            });
        }
    },

    guardaTipoSeguimientos : function()
    {
        var store = this.getStoreSeguimientoTiposStore();
        store.sync();
    },

    setEstadoComponentes : function()
    {
        var tieneSeleccionGridTiposSeguimientos = this.getGridTiposSeguimientos().getSelectionModel().hasSelection();
        this.getBotonBorrarTipoSeguimientos().setDisabled(!tieneSeleccionGridTiposSeguimientos);
    }

});