Ext.define('CRM.controller.ControllerPanelEnvios', {
    extend: 'Ext.app.Controller',
    requires: ['CRM.model.enums.TiposEnvio', 'CRM.model.enums.TiposCriteriosEnvio', 'CRM.model.enums.TiposFechas'],

    stores: ['StoreEnvios', 'StoreEnviosAdjuntos', 'StoreEnviosClientesVista', 'StoreEnviosClientesPostal', 'StoreEnviosSeguimientos', 'StoreItemsVentanaCriterios', 'StoreGruposVentanaCriterios',
        'StoreEtiquetasVentanaCriterios', 'StoreCampanyasVentanaCriterios', 'StoreEnviosCriterios', 'StoreTiposEnvios', 'StoreComboEnvioPersona', 'StoreTiposMovimientos', 'StoreTiposComparacion',
        'StoreTiposComparacionEnvios', 'StoreEnviosImagenes', 'StoreTiposCorreo', 'StoreTiposEstadoCampanya', 'StoreTarifas', 'StorePaises', 'StoreTipoSexo', 'StoreTipoEmail'],

    views: ['envios.PanelEnvios', 'envios.FormEnvioMail', 'envios.FormEnvioPostal', 'envios.GridEnvios', 'envios.GridArchivosAdjuntos', 'envios.GridEnvioClientesMail', 'envios.PanelEnvioMail',
        'envios.PanelCriterios', 'envios.GridEnvioClientesPostal', 'envios.FiltroEnvios', 'envios.VentanaAnyadirPersona', 'envios.FiltroEnviosAvanzado', 'envios.GridImagenes',
        'envios.VentanaGridEnvios', 'envios.VentanaBusquedaCriterio', 'envios.VentanaBusquedaCliente', 'envios.VentanaAnyadirPlantilla'],

    models: ['Envio', 'EnvioCliente'],

    ventanaBusquedaCriterio: {},
    ventanaBusquedaCliente: {},
    ventanaGridEnvios: {},
    ventanaAnyadirPlantilla: {},

    getVentanaBusquedaCriterio: function () {
        return this.getView('envios.VentanaBusquedaCriterio').create({
            envioId: this.getPanelEnvios().envioId,
            storeEnviosClientes: this.getStoreEnviosClientesVistaStore()
        });
    },

    getVentanaBusquedaClienteEnvio: function () {
        return this.getView('envios.VentanaBusquedaCliente').create({
            envioId: this.getPanelEnvios().envioId,
            storeEnviosClientes: this.getStoreEnviosClientesVistaStore()
        });
    },

    getVentanaGridEnvios: function () {
        return this.getView('envios.VentanaGridEnvios').create();
    },

    getVentanaAnyadirPlantilla: function () {
        return this.getView('envios.VentanaAnyadirPlantilla').create({
            editor: this.getEditor().down('[name=cuerpo]')
        });
    },

    refs: [{
        selector: 'panelEnvios',
        ref: 'panelEnvios'
    }, {
        selector: 'panelEnvios [name=formEtiquetaEnvioSeguimiento]',
        ref: 'etiquetaEnvioSeguimiento'
    }, {
        selector: 'panelEnvioMail',
        ref: 'panelEnvioMail'
    }, {
        selector: 'gridEnvios',
        ref: 'gridEnvios'
    }, {
        selector: 'gridEnviosSeguimientos',
        ref: 'gridEnviosSeguimientos'
    }, {
        selector: 'panelCampanyaSeguimientos',
        ref: 'panelCampanyaSeguimientos'
    }, {
        selector: 'formEnvioMail',
        ref: 'formEnvioMail'
    }, {
        selector: 'formEnvioPostal',
        ref: 'formEnvioPostal'
    }, {
        selector: 'formEnvioMail container[name=editor]',
        ref: 'editor'
    }, {
        selector: 'formEnvioPostal button[name=guardar]',
        ref: 'buttonGuardarPostal'
    }, {
        selector: 'formEnvioPostal button[name=borrar]',
        ref: 'buttonBorrarPostal'
    }, {
        selector: 'gridArchivosAdjuntos',
        ref: 'gridArchivosAdjuntos'
    }, {
        selector: 'gridArchivosAdjuntos button[name=botonAnyadirAdjunto]',
        ref: 'botonAnyadirAdjunto'
    }, {
        selector: 'gridArchivosAdjuntos button[name=botonBorrarAdjunto]',
        ref: 'botonBorrarAdjunto'
    }, {
        selector: 'gridArchivosAdjuntos button[name=botonDescargarAdjunto]',
        ref: 'botonDescargarAdjunto'
    }, {
        selector: 'gridArchivosAdjuntos filefield[name=anyadirAdjunto]',
        ref: 'anyadirAdjunto'
    }, {
        selector: 'panelEnvioMail form[name=formEnvioAdjuntos]',
        ref: 'formArchivosAdjuntos'
    }, {
        selector: 'panelEnvioMail form[name=formSubirImagenes]',
        ref: 'formImagenes'
    }, {
        selector: 'panelSeguimientos',
        ref: 'panelSeguimientos'
    }, {
        selector: 'gridCampanyaSeguimientos',
        ref: 'gridCampanyaSeguimientos'
    }, {
        selector: 'filtroEnvios textfield[name=busqueda]',
        ref: 'busqueda'
    }, {
        selector: 'filtroEnviosAvanzado datefield[name=fechaDesde]',
        ref: 'fechaDesde'
    }, {
        selector: 'filtroEnviosAvanzado datefield[name=fechaHasta]',
        ref: 'fechaHasta'
    }, {
        selector: 'filtroEnvios combobox[name=envioTipoId]',
        ref: 'envioTipoId'
    }, {
        selector: 'filtroEnviosAvanzado combobox[name=envioPersona]',
        ref: 'envioPersona'
    }, {
        selector: 'filtroEnviosAvanzado textfield[name=busquedaEmail]',
        ref: 'envioEmail'
    }, {
        selector: 'gridEnvioClientesMail',
        ref: 'gridEnvioClientesMail'
    }, {
        selector: 'gridEnvioClientesPostal',
        ref: 'gridEnvioClientesPostal'
    }, {
        selector: 'gridEnvioClientesPostal button[name=borrarPersona]',
        ref: 'botonBorrarPostalPersona'
    }, {
        selector: 'gridImagenes',
        ref: 'gridImagenes'
    }, {
        selector: 'gridImagenes button[name=botonAnyadirImagen]',
        ref: 'botonAnyadirImagen'
    }, {
        selector: 'gridImagenes button[name=botonBorrarImagen]',
        ref: 'botonBorrarImagen'
    }, {
        selector: 'gridImagenes filefield[name=anyadirImagen]',
        ref: 'anyadirImagen'
    }, {
        selector: 'ventanaBusquedaCriterio combo[name=campanyaId]',
        ref: 'busquedaCriterioComboCampanya'
    }, {
        selector: 'ventanaBusquedaCriterio combo[name=tipoEstadoCampanyaId]',
        ref: 'busquedaCriterioComboCampanyaEstado'
    }, {
        selector: 'ventanaBusquedaCriterio combo[name=criterioCirculoId]',
        ref: 'busquedaCriterioComboUsuarioCirculo'
    }],

    dirty: false,
    init: function () {
        this.control(
            {
                'panelEnvios': {
                    afterrender: function () {
                        if (Ext.isDefined(this.getGridEnviosSeguimientos()) && this.getGridCampanyaSeguimientos().getSelectionModel().getSelection().length > 0) {
                            var ruta = this.getPanelSeguimientos().down("treepanel[name=campanya]").getSelectionModel().getSelection()[0].data.text + ' » ' + this.getPanelSeguimientos().down("combobox[name=campanyaFases]").getRawValue()
                                + ' » ' + this.getGridCampanyaSeguimientos().getSelectionModel().getSelection()[0].data.nombre;

                            this.asignarRutaySeguimiento(ruta, this.getGridCampanyaSeguimientos().getSelectionModel().getSelection()[0].data.id, true);

                            // var envio = null;
                            // if (this.getGridEnviosSeguimientos().getSelectionModel().getSelection().length > 0) {
                            //     envio = this.getGridEnviosSeguimientos().getSelectionModel().getSelection()[0].data.id;
                            // }

                            // this.cargaGridEnvios(this.getPanelEnvios().seguimientoId, envio);
                        } else {
                            this.asignarRutaySeguimiento(null, null, false);
                        }
                    },
                    beforerender: function () {
                        this.getFormEnvioMail().envioTipoId = CRM.model.enums.TiposEnvio.EMAIL;
                        this.getFormEnvioPostal().envioTipoId = CRM.model.enums.TiposEnvio.POSTAL;

                    }
                },

                'panelEnvios button[name=borrarRuta]': {
                    click: this.borrarRuta
                },

                'gridEnvios': {
                    selectionchange: function (model, selected) {
                        if (selected.length > 0) {
                            this.rellenaForm(selected[0].data);
                            this.ventanaGridEnvios.destroy();
                        }
                    }
                },

                'panelEnvios button[name=nuevoMail]': {
                    click: function () {
                        this.envioNuevo(CRM.model.enums.TiposEnvio.EMAIL);
                    }
                },

                'panelEnvios button[name=nuevoPostal]': {
                    click: function () {
                        this.envioNuevo(CRM.model.enums.TiposEnvio.POSTAL);
                    }
                },

                'panelEnvios button[name=enviar]': {
                    click: function () {
                        this.enviar(1);
                    }
                },

                'panelEnvios button[name=guardar]': {
                    click: function () {
                        this.enviar(0);
                    }
                },

                'panelEnvios button[name=enviarPrueba]': {
                    click: function () {
                        this.enviarPrueba();
                    }
                },
                'panelEnvios button[name=borrar]': {
                    click: this.borrar
                },
                'panelEnvios button[name=imprimirListadoPostal]': {
                    click: this.imprimirListadoPostal
                },
                'panelEnvios button[name=imprimirListado]': {
                    click: this.imprimirListado
                },

                'formEnvioMail button[name=afegirPlantilla]': {
                    click: this.showVentanaAnyadirPlantilla
                },

                'gridEnvioClientesPostal': {
                    selectionchange: function (model, selected) {
                        if (selected.length > 0) {
                            this.getBotonBorrarPostalPersona().setDisabled(false);
                        }
                    }
                },

                // 'datefield[name=fechaEnvioDestinatario]': {
                //     select: function (sel) {
                //         this.cambiaFechaVigencia(sel.getBubbleTarget().getBubbleTarget().getBubbleTarget().getBubbleTarget().envioTipoId);
                //     }
                // },

                'gridArchivosAdjuntos': {
                    selectionchange: function () {
                        this.getBotonDescargarAdjunto().setDisabled(false);
                        this.getBotonBorrarAdjunto().setDisabled(false);
                    }
                },

                'gridArchivosAdjuntos button[name=botonAnyadirAdjunto]': {
                    click: this.anyadirFicheroAdjunto
                },

                'gridArchivosAdjuntos button[name=botonBorrarAdjunto]': {
                    click: this.borrarFicheroAdjunto
                },
                'gridArchivosAdjuntos button[name=botonDescargarAdjunto]': {
                    click: this.descargarFichero
                },

                'gridArchivosAdjuntos filefield[name=anyadirAdjunto]': {
                    change: function () {
                        this.getBotonAnyadirAdjunto().setDisabled(false);
                        this.getBotonBorrarAdjunto().setDisabled(true);
                        this.getBotonDescargarAdjunto().setDisabled(true);
                    }
                },

                'gridImagenes': {
                    selectionchange: function () {
                        //this.getBotonDescargarAdjunto().setDisabled(false);
                        this.getBotonBorrarImagen().setDisabled(false);
                    }
                },

                'gridImagenes button[name=botonAnyadirImagen]': {
                    click: this.anyadirImagen
                },

                'gridImagenes button[name=botonBorrarImagen]': {
                    click: this.borrarImagenes
                },
                'gridImagenes filefield[name=anyadirImagen]': {
                    change: function () {
                        this.getBotonAnyadirImagen().setDisabled(false);
                        this.getBotonBorrarImagen().setDisabled(true);
                        //this.getBotonDescargarAdjunto().setDisabled(true);
                    }
                },

                'filtroEnvios textfield[name=busqueda]': {
                    specialkey: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                            this.buscarEnvios();
                        }
                    }
                },
                'filtroEnvios button[name=limpiarFiltros]': {
                    click: this.limpiarFiltros
                },
                'filtroEnvios button[name=buscar]': {
                    click: this.buscarEnvios
                },
                'filtroEnvios combobox[name=envioTipoId]': {
                    select: this.buscarEnvios
                },
                'filtroEnvios datefield[name=fechaDesde]': {
                    change: this.buscarEnvios
                },
                'filtroEnvios datefield[name=fechaHasta]': {
                    change: this.buscarEnvios
                },

                'gridEnvioClientesMail button[name=botonBusquedaEnvioCriterio]': {
                    click: this.showVentanaBusquedaCriterio
                },

                'gridEnvioClientesMail button[name=botonAnyadirClienteEnvio]': {
                    click: this.showVentanaBusquedaCliente
                },

                'ventanaBusquedaCriterio button[name=botonEliminarBusquedaEnvioCriterio]': {
                    click: this.eliminarBusquedaCriterio
                }

            })
    },

    showVentanaAnyadirPlantilla: function () {
        this.ventanaAnyadirPlantilla = this.getVentanaAnyadirPlantilla();
        this.ventanaAnyadirPlantilla.show();
    },

    borrar: function () {
        var ref = this;
        Ext.Msg.confirm('Eliminació de enviament', '<b>Estàs segur d\'esborrar l\'enviament?</b>', function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: '/crm/rest/envio/' + ref.getPanelEnvios().envioId,
                    method: 'DELETE',
                    scope: this,
                    success: function () {
                        ref.limpiaFormAll();
                    },
                    failure: function () {
                        Ext.Msg.alert('Enviament', '<b>No s\'han pogut eliminar l\'enviament</b>');
                    }
                });
            }
        });
    },

    eliminarBusquedaCriterio: function () {
        var ref = this;

        Ext.Ajax.request({
            url: '/crm/rest/enviocriterio/envio/' + ref.ventanaBusquedaCriterio.envioId,
            method: 'DELETE',
            scope: this,
            success: function () {
                ref.ventanaBusquedaCriterio.destroy();
                ref.getStoreEnviosClientesVistaStore().load();
            },
            failure: function () {
                Ext.Msg.alert('Enviament', '<b>No s\'han pogut eliminar els criteris</b>');
            }
        });
    },

    showVentanaBusquedaCliente: function () {
        this.ventanaBusquedaCliente = this.getVentanaBusquedaClienteEnvio();
        this.ventanaBusquedaCliente.show();
    },

    showVentanaBusquedaCriterio: function () {

        this.ventanaBusquedaCriterio = this.getVentanaBusquedaCriterio();
        var ventana = this.ventanaBusquedaCriterio,
            form = ventana.down("form[name=panelBusqueda]");
        this.cargaDatosFormularioBusqueda(function () {

            Ext.Ajax.request(
                {
                    url: '/crm/rest/enviocriterio/envio/' + ventana.envioId,
                    method: 'GET',
                    scope: this,
                    success: function (req) {
                        ventana.show();

                        var criterio = Ext.JSON.decode(req.responseText);

                        if (criterio.data.programaId != null) {
                            form.down("combobox[name=programaId]").select(criterio.data.programaId);
                            form.down("combobox[name=campanyaId]").getStore().filterBy(function (record) {
                                return record.data.programaId == criterio.data.programaId;
                            });
                        }

                        if (criterio.data.campanyaId != null) {
                            ventana.cargaEstadosCampanya(criterio.data.campanyaId);
                            form.down("combobox[name=campanyaId]").setDisabled(false);
                        }
                        form.loadRecord(criterio);

                        if (Array.isArray(criterio.data.criterioCirculoId)) {
                            form.down("[name=criterioCirculoId]").select(criterio.data.criterioCirculoId.map(Number));
                        } else {
                            form.down("[name=criterioCirculoId]").select(criterio.data.criterioCirculoId);
                        }

                        if (Array.isArray(criterio.data.criterioEstudioUJIId)) {
                            form.down("[name=criterioEstudioUJIId]").select(criterio.data.criterioEstudioUJIId.map(Number));
                        } else {
                            form.down("[name=criterioEstudioUJIId]").select(criterio.data.criterioEstudioUJIId);
                        }

                        if (Array.isArray(criterio.data.criterioEstudioNoUJIId)) {
                            form.down("[name=criterioEstudioNoUJIId]").select(criterio.data.criterioEstudioNoUJIId.map(Number));
                        } else {
                            form.down("[name=criterioEstudioNoUJIId]").select(criterio.data.criterioEstudioNoUJIId);
                        }

                        if (Array.isArray(criterio.data.tipoEstadoCampanyaId)) {
                            if (criterio.data.tipoEstadoCampanyaId.length > 0) {
                                form.down("[name=tipoEstadoCampanyaId]").setDisabled(false);
                                form.down("[name=tipoEstadoCampanyaId]").select(criterio.data.tipoEstadoCampanyaId.map(Number));
                            }
                        } else {
                            if (criterio.data.tipoEstadoCampanyaId != null) {
                                form.down("[name=tipoEstadoCampanyaId]").setDisabled(false);
                                form.down("[name=tipoEstadoCampanyaId]").select(criterio.data.tipoEstadoCampanyaId);
                            }
                        }

                        if (Array.isArray(criterio.data.criterioEstudioUJIBeca)) {
                            form.down("[name=criterioEstudioUJIBeca]").select(criterio.data.criterioEstudioUJIBeca.map(Number));
                        } else {
                            form.down("[name=criterioEstudioUJIBeca]").select(parseInt(criterio.data.criterioEstudioUJIBeca));
                        }

                        if (criterio.data.mesNacimiento !== null) {
                            form.down("[name=mesNacimiento]").setValue(parseInt(criterio.data.mesNacimiento));
                        }
                        if (criterio.data.anyoNacimiento !== null) {
                            form.down("[name=anyoNacimiento]").setValue(parseInt(criterio.data.anyoNacimiento));
                        }


                    },
                    failure: function () {
                        Ext.Msg.alert('Enviament', '<b>No s\'han pogut cargar els criteris</b>');
                    }
                });
        })
    }
    ,

    asignarRutaySeguimiento: function (ruta, seguimientoId, visible) {
        this.getPanelEnvios().ruta = ruta;
        this.getEtiquetaEnvioSeguimiento().down("displayfield[name=etiquetaEnvioSeguimiento]").setValue(this.getPanelEnvios().ruta);
        this.getPanelEnvios().seguimientoId = seguimientoId;
        this.getEtiquetaEnvioSeguimiento().setVisible(visible);
    }
    ,

    // cambiaFechaVigencia: function (envioTipoId) {
    //     var form = this.getFormTipo(envioTipoId);
    //     form.down("datefield[name=fechaFinVigencia]").setValue(form.down("datefield[name=fechaEnvioDestinatario]").getValue());
    //     form.down("datefield[name=fechaFinVigencia]").setMinValue(form.down("datefield[name=fechaEnvioDestinatario]").getValue());
    // }
    // ,

    borrarRuta: function () {
        this.asignarRutaySeguimiento(null, null, false);
        this.cargaGridEnviosTodos();
        this.limpiaFormAll();
    }
    ,

    getFormTipo: function (envioTipoId) {
        var form;

        if (envioTipoId === CRM.model.enums.TiposEnvio.EMAIL)
            form = this.getFormEnvioMail();
        if (envioTipoId === CRM.model.enums.TiposEnvio.POSTAL)
            form = this.getFormEnvioPostal();

        return form;
    }
    ,

    rellenaForm: function (envio) {

        this.getPanelEnvios().envioId = envio.id;
        this.getPanelEnvios().envioTipoId = envio.envioTipoId;

        this.activaForm(envio.envioTipoId);

        var form = this.getFormTipo(envio.envioTipoId);
        form.down("textfield[name=nombre]").setValue(envio.nombre);

        if (envio.envioTipoId != CRM.model.enums.TiposEnvio.POSTAL) {
            form.down("textfield[name=asunto]").setValue(envio.asunto);
            form.down("datefield[name=fechaEnvioDestinatario]").setValue(envio.fechaEnvioDestinatario);
            form.down("textfield[name=horaEnvioDestinatario]").setValue(this.getController("ControllerVarios").sacaHoraCompleta(envio.fechaEnvioDestinatario));

            if (envio.fechaFinVigencia != null) {
                form.down("datefield[name=fechaFinVigencia]").setValue(envio.fechaFinVigencia);
                form.down("textfield[name=horaFinVigencia]").setValue(this.getController("ControllerVarios").sacaHoraCompleta(envio.fechaFinVigencia));
            }

        }
        this.deshabilitarPanelEnvio(envio.fechaEnvioPlataforma);

        if (envio.envioTipoId == CRM.model.enums.TiposEnvio.EMAIL) {
            form.down("textfield[name=desde]").setValue(envio.desde);
            form.down("textfield[name=responder]").setValue(envio.responder);
            form.down("combobox[name=tipoCorreoEnvio]").setValue(envio.tipoCorreoEnvio);
            form.down("combobox[name=programaId]").setValue(envio.programaId);

            this.listaFicherosAdjuntos(envio.id);
            this.listaImagenes(envio.id);
            this.getStoreEnviosClientesVistaStore().getProxy().url = '/crm/rest/enviocliente/envio/' + envio.id;
            this.getStoreEnviosClientesVistaStore().load();

            var txt = this.getEditor().down('[name=cuerpo]');

            if (txt != null) {
                txt.destroy();
            }

            var config = {
                width: '100%',
                name: 'cuerpo'
            };

            var editorNuevo = new Ext.ux.CKEditor(config);

            editorNuevo.setValue(envio.cuerpo);

            this.getEditor().add(editorNuevo);
            editorNuevo.setUrl('/envioimagen/visualizar/' + envio.id);
        }
    }
    ,

    envioEnviado: function (fecha) {
        return fecha != null;
    }
    ,

    desactivaFormsTodos: function () {
        this.getPanelEnvioMail().setVisible(false);
        this.getFormEnvioPostal().setVisible(false);
    }
    ,

    activaForm: function (tipoForm) {
        this.desactivaFormsTodos();

        if (tipoForm == CRM.model.enums.TiposEnvio.EMAIL)
            this.getPanelEnvioMail().setVisible(true);
        if (tipoForm == CRM.model.enums.TiposEnvio.POSTAL)
            this.getFormEnvioPostal().setVisible(true);

    }
    ,

    deshabilitarPanelEnvio: function (fecha) {
        // this.getPanelEnvios().down("container[name=datosEnvio]").setDisabled(this.envioEnviado(fecha));
    }
    ,

    borraGridEnviosClientes: function () {
        var storeEnvioClientes = this.dameGridEnviosClientes();
        storeEnvioClientes.remove();
        storeEnvioClientes.sync();
    }
    ,

//////////////////////////////// Imagenes para mail //////////////////////////////////////////////////////

    listaImagenes: function (envioId) {
        this.getStoreEnviosImagenesStore().load(
            {
                url: '/crm/rest/envioimagen/' + envioId
            })
    }
    ,

    anyadirImagen: function () {
        var envioId = this.getPanelEnvios().envioId,
            formImagenes = this.getFormImagenes();

        if (formImagenes.getForm().isValid()) {
            formImagenes.submit(
                {
                    url: '/crm/rest/envioimagen/' + envioId,
                    scope: this,
                    success: function () {
                        this.getBotonAnyadirImagen().setDisabled(true);
                        this.getBotonBorrarImagen().setDisabled(true);
                        this.listaImagenes(envioId);
                    },
                    failure: function (x, resp) {
                        Ext.Msg.alert('Insercció de imatges', '<b>' + resp.result.message + '</b>');
                    }
                });
        } else {
            Ext.Msg.alert('Insercció de imatges', '<b>El fitxer no es una imatge</b>');
        }
    }
    ,

    borrarImagenes: function () {
        var storeImagenes = this.getStoreEnviosImagenesStore();
        var selection = this.getGridImagenes().getSelectionModel().getSelection();

        var ref = this;
        Ext.Msg.confirm('Eliminar', 'El registre s\'eliminarà. Estàs segur de voler continuar?', function (btn) {
            if (btn == 'yes') {
                ref.getGridImagenes().setLoading(true);
                storeImagenes.remove(selection);
                storeImagenes.sync(
                    {
                        success: function () {
                            ref.getBotonBorrarImagen().setDisabled(true);
                            ref.getGridImagenes().setLoading(false);
                        },
                        failure: function () {
                            Ext.Msg.alert('Esborrat de fitxers', '<b>El fitxer no ha pogut ser borrat correctament</b>');
                            ref.getGridImagenes().setLoading(false);
                        }

                    });

            }
        });

    }
    ,

//////////////////////////////// Ficheros Adjuntos sólo para envio Mail /////////////////////////////////////////////////////////////////

    listaFicherosAdjuntos: function (envioId) {

        this.getStoreEnviosAdjuntosStore().load(
            {
                url: '/crm/rest/envio/' + envioId + '/adjunto',
                scope: this
            })
    }
    ,

    anyadirFicheroAdjunto: function () {
        var envioId = this.getPanelEnvios().envioId;
        this.getFormArchivosAdjuntos().submit(
            {
                url: '/crm/rest/envio/' + envioId + '/fichero/',
                scope: this,
                success: function () {
                    this.listaFicherosAdjuntos(envioId);
                    this.getBotonDescargarAdjunto().setDisabled(true);
                    this.getBotonAnyadirAdjunto().setDisabled(true);
                    this.getBotonBorrarAdjunto().setDisabled(true);
                },
                failure: function () {
                    Ext.Msg.alert('Insercció de fitxers', '<b>El fitxer no ha pogut ser insertat correctament</b>');
                }
            });
    }
    ,

    borrarFicheroAdjunto: function () {
        var storeAdjuntos = this.getStoreEnviosAdjuntosStore();
        var selection = this.getGridArchivosAdjuntos().getSelectionModel().getSelection();

        var ref = this;
        Ext.Msg.confirm('Eliminar', 'El registre s\'eliminarà. Estàs segur de voler continuar?', function (btn) {
            if (btn == 'yes') {
                ref.getGridArchivosAdjuntos().setLoading(true);
                storeAdjuntos.remove(selection);
                storeAdjuntos.sync(
                    {
                        success: function () {
                            ref.getBotonDescargarAdjunto().setDisabled(true);
                            ref.getBotonBorrarAdjunto().setDisabled(true);
                            ref.getGridArchivosAdjuntos().setLoading(false);
                        },
                        failure: function () {
                            Ext.Msg.alert('Esborrat de fitxers', '<b>El fitxer no ha pogut ser borrat correctament</b>');
                            ref.getGridArchivosAdjuntos().setLoading(false);
                        }

                    });

            }
        });

    }
    ,

    descargarFichero: function () {

        var referencia = this.getGridArchivosAdjuntos().getSelectionModel().getSelection()[0].data.referencia;
        var url = '//ujiapps.uji.es/ade/rest/storage/' + referencia;

        window.open(url, "_blank");

    }
    ,

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////// Borrar formularios y ponerlos en estado inicial ///////////////////////////////////////////////////////////////////////////////

    limpiaForm: function (form) {
        var envio = this.getEnvioModel();
        var envioVacio = new envio(
            {
                id: '',
                nombre: '',
                cuerpo: '',
                responder: '',
                desde: '',
                personaId: '',
                asunto: '',
                fecha: '',
                fechaEnvioDestinatario: new Date(),
                fechaFinVigencia: new Date(),
                seguimientoId: ''
            });
        form.loadRecord(envioVacio);

    }
    ,

    limpiaFormMail: function () {
        this.limpiaForm(this.getFormEnvioMail());

        this.getStoreEnviosAdjuntosStore().loadData([], false);
    }
    ,

    limpiaFormAll: function () {
        // this.getPanelCriterios().setVisible(false);

        this.limpiaFormMail();

        this.getPanelEnvioMail().setVisible(false);
        this.getFormEnvioPostal().setVisible(false);

        this.getPanelEnvios().down("container[name=datosEnvio]").setDisabled(false);

        // this.getStoreEnviosClientesMailStore().loadData([], false);
    }
    ,

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    esFormCorrecto: function (envioTipoId) {
        var form = this.getFormTipo(envioTipoId);
        return form.getForm().isValid();
    }
    ,

    enviarPrueba: function () {

        var ref = this;
        var form = this.getFormTipo(this.getPanelEnvios().envioTipoId);
        this.getPanelEnvios().setLoading(true);
        var envio = this.getEnvioModel().create(form.getValues());

        envio.data.cuerpo = form.down('ckeditor[name=cuerpo]').getValue();
        envio.data.correoPrueba = form.getBubbleTarget().down('textfield[name=correoPrueba]').getValue();

        Ext.Ajax.request(
            {
                url: '/crm/rest/envio/prueba',
                method: 'POST',
                params: envio.data,
                scope: this,
                success: function () {
                    Ext.Msg.alert('Enviament', '<b>S\'ha enviat la prova</b>');
                    ref.getPanelEnvios().setLoading(false);

                },
                failure: function () {
                    Ext.Msg.alert('Enviament', '<b>No s\'ha pogut enviar la prova</b>');
                    ref.getPanelEnvios().setLoading(false);
                }
            });
    }
    ,

    enviar: function (enviar) {
        var envioTipoId = this.getPanelEnvios().envioTipoId;
        var envioId = this.getPanelEnvios().envioId;
        var form = this.getFormTipo(envioTipoId);

        if (envioTipoId == CRM.model.enums.TiposEnvio.EMAIL) {

            if (this.esFormCorrecto(envioTipoId) && (!enviar || form.down("textfield[name=nombre]").value != "NOU") && form.down("ckeditor[name=cuerpo]").getValue() != "") {
                this.envioExistente(envioTipoId, enviar, envioId);
            } else
                Ext.Msg.alert('Enviament', '<b>El formulari no es correcte</b>');
        } else {
            if (this.esFormCorrecto(envioTipoId) && (!enviar || form.down("textfield[name=nombre]").value != "NOU")) {
                this.envioExistente(envioTipoId, enviar, envioId);
            } else
                Ext.Msg.alert('Enviament', '<b>El formulari no es correcte</b>');
        }

    }
    ,

    envioNuevo: function (tipoId, seguimientoId) {
        // var gridEnvios = this.getGridEnvios();
        var storeEnvios = this.getStoreEnviosStore();
        var panelEnvios = this.getPanelEnvios();

        var envio = this.getEnvioModel();
        var envioVacio = new envio({
            nombre: 'NOU',
            seguimientoId: seguimientoId,
            envioTipoId: tipoId
        });

        panelEnvios.setLoading(true);

        storeEnvios.add(envioVacio);
        storeEnvios.sync({
            scope: this,

            success: function (result) {
                panelEnvios.down("[name=panelEnvioMail]").setActiveTab(0);
                this.rellenaForm(result.operations[0].records[0].data);
                panelEnvios.setLoading(false);
            },

            failure: function () {
                Ext.Msg.alert('Enviament', '<b>No s\'ha pogut crear l\'enviament</b>');
                panelEnvios.envioId = '';
                storeEnvios.reload();
                this.limpiaFormAll();
                this.desactivaFormsTodos();
                panelEnvios.setLoading(false);
            }
        });
    }
    ,

    envioExistente: function (tipoId, enviar, id) {
        var form = this.getFormTipo(tipoId);
        var ref = this;
        var storeEnvios = this.getStoreEnviosStore();
        // var gridEnvios = this.getGridEnvios();


        var values = form.getValues();
        if (tipoId == CRM.model.enums.TiposEnvio.EMAIL) {
            values.cuerpo = form.down('[name=cuerpo]').getValue();
        }
        this.getPanelEnvios().setLoading(true);
        Ext.Ajax.request(
            {
                url: '/crm/rest/envio/' + id + "/enviar/" + enviar,
                method: 'PUT',
                params: values,
                scope: this,
                success: function () {
                    Ext.Msg.alert('Enviament', '<b>S\'ha guardat l\'enviament</b>');
                    storeEnvios.reload(
                        {
                            scope: this,
                            callback: function () {
                                if (enviar) {
                                    ref.limpiaFormAll();
                                    ref.desactivaFormsTodos();
                                }
                            }
                        });
                    ref.getPanelEnvios().setLoading(false);

                },
                failure: function () {
                    Ext.Msg.alert('Enviament', '<b>No s\'ha pogut desar l\'enviament</b>');
                    ref.getPanelEnvios().setLoading(false);
                }
            });

    }
    ,

    imprimirListadoPostal: function () {
        var envioId = this.getController("ControllerVarios").dameIdGrid(this.getGridEnvios());
        window.open("/crm/rest/pdf/" + envioId + "/listadoPostal/pdf");
    }
    ,
    imprimirListado: function () {
        var envioId = this.getController("ControllerVarios").dameIdGrid(this.getGridEnvios());
        window.open("/crm/rest/pdf/" + envioId + "/listado/pdf");
    }
    ,

    limpiarFiltros: function () {
        // this.cargaGridEnviosTodos();
        this.limpiaFormAll();

        this.getBusqueda().setValue(null);
        this.getFechaDesde().setValue(null);
        this.getFechaHasta().setValue(null);
        this.getEnvioTipoId().setValue(null);
        this.getEnvioPersona().setValue(null);
        this.getEnvioEmail().setValue(null);
    }
    ,

    buscarEnvios: function () {

        this.ventanaGridEnvios = this.getVentanaGridEnvios();

        var store = this.ventanaGridEnvios.down("grid[name=gridEnvios]").getStore();

        store.getProxy().extraParams = {
            busqueda: this.getBusqueda().getValue(),
            fechaDesde: this.getFechaDesde().getValue(),
            fechaHasta: this.getFechaHasta().getValue(),
            tipoEnvioId: this.getEnvioTipoId().getValue(),
            persona: this.getEnvioPersona().getValue(),
            email: this.getEnvioEmail().getValue()
        };

        store.load({
            scope: this,
            callback: function () {
                this.ventanaGridEnvios.show();
                this.limpiaFormAll();
            }
        });
    }
    ,

// cargaEstadosCampanya: function (el, campanyaId) {
//     if (campanyaId !== null) {
//         var comboCampanyaEstado = this.getBusquedaCriterioComboCampanyaEstado();
//         comboCampanyaEstado.setValue(null);
//         comboCampanyaEstado.getStore().load({
//             url: '/crm/rest/tipoestadocampanya/campanya',
//             params: {
//                 campanyaId: campanyaId
//             }
//         });
//     }
//
//     /*var comboCampanyaTarifa = this.getBusquedaComboCampanyaTarifa();
//     comboCampanyaTarifa.setValue(null);
//     comboCampanyaTarifa.getStore().load({
//         params: {
//             campanyaId: campanyaId
//         }
//     });*/
// }
// ,

// cargaProvincias: function (el, paisId) {
//     var comboUsuarioProvincia = this.getBusquedaComboUsuarioProvincia();
//     comboUsuarioProvincia.setValue(null);
//     comboUsuarioProvincia.getStore().load({
//         params: {
//             paisId: paisId
//         }
//     });
// },
//
// cargaPoblaciones: function (el, provinciaId) {
//     var comboUsuarioPoblacion = this.getBusquedaComboUsuarioPoblacion();
//     comboUsuarioPoblacion.setValue(null);
//     comboUsuarioPoblacion.getStore().load({
//         url: '/crm/rest/poblacion/' + provinciaId
//     });
// },

    cargaDatosFormularioBusqueda: function (cb) {
        // this.getBusquedaComboUsuarioSituacionLaboral().getStore().load({
        //     params: {
        //         grupoId: 996675
        //     }
        // });

        // this.getBusquedaComboUsuarioNivelEstudios().getStore().load({
        //     params: {
        //         grupoId: 3229080
        //     }
        // });

        this.getBusquedaCriterioComboUsuarioCirculo().getStore().load({
            params: {
                grupoId: 1095138
            },
            callback: function () {
                cb();
            }
        });

        // this.getBusquedaComboUsuarioCheck().getStore().load({
        //     params: {
        //         grupoId: 171015,
        //         soloVisibles: true
        //     }
        // });

        // this.getBusquedaComboUsuarioUniverdidad().getStore().load({
        //     params: {
        //         clienteDatoTipoId: 1645772
        //     }
        // });
    }
})
;