Ext.define('CRM.controller.ControllerPanelClientesEstudios', {
    extend: 'Ext.app.Controller',
    stores: [],
    models: ['ClienteEstudioNoUJI'],
    views: ['clientes.GridClienteEstudiosNoUJI', 'clientes.GridClienteEstudiosUJI'],

    refs: [{
        selector: 'gridClienteEstudiosNoUJI button[name=borrarClienteEstudioNoUJI]',
        ref: 'botonBorrarClienteEstudioNoUJI'
    }
        //
    ],

    init: function () {
        this.control(
            {
                'gridClienteEstudiosNoUJI button[name=borrarClienteEstudioNoUJI]':
                    {
                        click: this.borrarClienteEstudioNoUJI
                    },
                'gridClienteEstudiosNoUJI button[name=anyadirClienteEstudioNoUJI]':
                    {
                        click: this.anyadirClienteEstudioNoUJI
                    },
                'gridClienteEstudiosNoUJI':
                    {
                        select: function () {
                            this.setEstadoComponentes();
                        },
                        edit: this.guardaClienteEstudioNoUJI,

                        canceledit: function () {
                            this.setEstadoComponentes();

                        }
                    }
            });
    },

    dameStoreEstudiosNoUJI: function () {
        return this.dameGridEstudiosNoUJI().getStore();
    }
    ,

    dameGridEstudiosNoUJI: function () {
        var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
        var clienteId = tabsServiciosCliente.getActiveTab().itemId;
        return tabsServiciosCliente.down("[itemId=" + clienteId + "]").down("[itemId=gridEstudiosNoUJI-" + clienteId + "]");
    }
    ,

    anyadirClienteEstudioNoUJI: function () {
        var store = this.dameStoreEstudiosNoUJI();
        var estudio = this.getClienteEstudioNoUJIModel().create({
            clienteId: this.getController("ControllerPanelClientes").dameIdCliente()
        });
        var rowEditor = this.dameGridEstudiosNoUJI().getPlugin();

        rowEditor.cancelEdit();
        store.insert(0, estudio);
        rowEditor.startEdit(0, 0);

    }
    ,

    borrarClienteEstudioNoUJI: function () {
        var ref = this;
        var store = ref.dameStoreEstudiosNoUJI();
        var selection = this.dameGridEstudiosNoUJI().getSelectionModel().getSelection();
        var estudioId = (selection.length > 0) ? selection[0].data.id : null;
        var estudio = store.findRecord('id', estudioId);

        if (estudio) {
            Ext.Msg.confirm('Esborrar Tipus de dada', 'Esteu segur/a de voler esborrar el registre sel·leccionat?', function (btn) {
                if (btn == 'yes') {
                    store.remove(estudio);
                    store.sync(
                        {
                            success: function () {
                                ref.setEstadoComponentes();
                            },
                            failure: function () {
                                Ext.Msg.alert('Esborrar', 'No s`ha pogut esborrar');
                                store.rejectChanges();
                                ref.setEstadoComponentes();
                            }
                        });
                }
            });
        }
    }
    ,


    guardaClienteEstudioNoUJI: function () {
        var store = this.dameStoreEstudiosNoUJI();
        var ref = this;
        store.sync(
            {
                success: function () {
                    ref.dameGridEstudiosNoUJI().getPlugin().editor.down("combobox[name=clasificacionId]").getStore().clearFilter();
                    store.load();
                    ref.setEstadoComponentes();
                },
                failure: function () {
                    Ext.Msg.alert('Dades', 'S\'ha produït un error en modificar la dada');
                    store.rejectChanges();
                    ref.setEstadoComponentes();
                }
            });
    }
    ,

    setEstadoComponentes: function () {
        var tieneSeleccionGrid = this.dameGridEstudiosNoUJI().getSelectionModel().hasSelection();
        this.getBotonBorrarClienteEstudioNoUJI().setDisabled(!tieneSeleccionGrid);


    }

})
;