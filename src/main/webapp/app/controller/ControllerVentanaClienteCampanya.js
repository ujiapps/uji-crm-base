Ext.apply(Ext.form.field.VTypes, {

    _ibanregex: {
        'AD': '^AD[0-9]{2}[0-9]{8}[A-Z0-9]{12}$',
        'AT': '^AT[0-9]{2}[0-9]{5}[0-9]{11}$',
        'BA': '^BA[0-9]{2}[0-9]{6}[0-9]{10}$',
        'BE': '^BE[0-9]{2}[0-9]{3}[0-9]{9}$',
        'BG': '^BG[0-9]{2}[A-Z]{4}[0-9]{4}[0-9]{2}[A-Z0-9]{8}$',
        'CH': '^CH[0-9]{2}[0-9]{5}[A-Z0-9]{12}$',
        'CS': '^CS[0-9]{2}[0-9]{3}[0-9]{15}$',
        'CY': '^CY[0-9]{2}[0-9]{8}[A-Z0-9]{16}$',
        'CZ': '^CZ[0-9]{2}[0-9]{4}[0-9]{16}$',
        'DE': '^DE[0-9]{2}[0-9]{8}[0-9]{10}$',
        'DK': '^DK[0-9]{2}[0-9]{4}[0-9]{10}$',
        'EE': '^EE[0-9]{2}[0-9]{4}[0-9]{12}$',
        'ES': '^ES[0-9]{2}[0-9]{8}[0-9]{12}$',
        'FR': '^FR[0-9]{2}[0-9]{10}[A-Z0-9]{13}$',
        'FI': '^FI[0-9]{2}[0-9]{6}[0-9]{8}$',
        'GB': '^GB[0-9]{2}[A-Z]{4}[0-9]{14}$',
        'GI': '^GI[0-9]{2}[A-Z]{4}[A-Z0-9]{15}$',
        'GR': '^GR[0-9]{2}[0-9]{7}[A-Z0-9]{16}$',
        'HR': '^HR[0-9]{2}[0-9]{7}[0-9]{10}$',
        'HU': '^HU[0-9]{2}[0-9]{7}[0-9]{1}[0-9]{15}[0-9]{1}$',
        'IE': '^IE[0-9]{2}[A-Z0-9]{4}[0-9]{6}[0-9]{8}$',
        'IS': '^IS[0-9]{2}[0-9]{4}[0-9]{18}$',
        'IT': '^IT[0-9]{2}[A-Z]{1}[0-9]{10}[A-Z0-9]{12}$',
        'LI': '^LI[0-9]{2}[0-9]{5}[A-Z0-9]{12}$',
        'LU': '^LU[0-9]{2}[0-9]{3}[A-Z0-9]{13}$',
        'LT': '^LT[0-9]{2}[0-9]{5}[0-9]{11}$',
        'LV': '^LV[0-9]{2}[A-Z]{4}[A-Z0-9]{13}$',
        'MK': '^MK[0-9]{2}[A-Z]{3}[A-Z0-9]{10}[0-9]{2}$',
        'MT': '^MT[0-9]{2}[A-Z]{4}[0-9]{5}[A-Z0-9]{18}$',
        'NL': '^NL[0-9]{2}[A-Z]{4}[0-9]{10}$',
        'NO': '^NO[0-9]{2}[0-9]{4}[0-9]{7}$',
        'PL': '^PL[0-9]{2}[0-9]{8}[0-9]{16}$',
        'PT': '^PT[0-9]{2}[0-9]{8}[0-9]{13}$',
        'RO': '^RO[0-9]{2}[A-Z]{4}[A-Z0-9]{16}$',
        'SE': '^SE[0-9]{2}[0-9]{3}[0-9]{17}$',
        'SI': '^SI[0-9]{2}[0-9]{5}[0-9]{8}[0-9]{2}$',
        'SK': '^SK[0-9]{2}[0-9]{4}[0-9]{16}$',
        'TN': '^TN[0-9]{2}[0-9]{5}[0-9]{15}$',
        'TR': '^TR[0-9]{2}[0-9]{5}[A-Z0-9]{17}$'
    },

    _formatA: [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
    ],

    _formatN: [
        '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22',
        '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35'
    ],

    _ibanUnsupportedRegionText: 'IBAN incorrecto.',
    _ibanFalseFormatText: 'Formato incorrecto',
    _ibanCheckFailText: 'Fallo al checkear el IBAN.',
    _ibanDefultText: 'IBAN incorrecto.',
    _setIbanError: function (v) {
        this.ibanText = v;
    },
    ibanText: 'IBAN incorrecto',
    ibanMask: /[a-z0-9_]/i,

    iban: function (v) {
        v = v.toUpperCase();
        var me = this,
            preg,
            format,
            temp,
            len,
            region = v.substr(0, 2);

        if (!me._ibanregex[region]) {
            me._setIbanError(me._ibanUnsupportedRegionText);
            return false;
        }

        preg = new RegExp(me._ibanregex[region]);
        if (!v.match(preg)) {
            me._setIbanError(me._ibanFalseFormatText);
            return false;
        }

        format = v.substr(4) + v.substr(0, 4);
        // optimizie...maybe...
        for (var x in me._formatA) {
            if (!!me._formatA[x] && !!me._formatN[x]) {
                var reg = new RegExp(me._formatA[x], 'g');
                format = format.replace(reg, me._formatN[x]);
            }
        }
        temp = parseInt(format.charAt(0));
        len = format.length;

        for (var i = 1; i < len; i++) {
            temp *= 10;
            temp += parseInt(format.substr(i, 1));
            temp %= 97;
        }

        if (temp != 1) {
            me._setIbanError(me._ibanCheckFailText);
            return false;
        }
        return true;
    }
});

Ext.apply(Ext.form.field.VTypes, {

    anyoText: 'Año incorrecto, formato YYYY',
    anyo: function (v) {
        return /^\d{4}$/.test(v) && v > 1900;
    }
});

Ext.apply(Ext.form.field.VTypes, {

    DNIText: 'Identificacion incorrecta',
    // DNIMask: /[a-z0-9_]/i,

    DNI: function (v) {
        var campoLimpio = v.toUpperCase().replace(" ", "").replace("-", "").replace("/", "");
        var letra = campoLimpio.substr(-1);
        var numeros = campoLimpio.substr(0, campoLimpio.length - 1);
        return "TRWAGMYFPDXBNJZSQVHLCKE".substr(numeros % 23, 1) == letra && letra.length == 1 && numeros.length == 8;

    }
});

Ext.apply(Ext.form.field.VTypes, {

    NIEText: 'Identificacion incorrecta',
    // NIEMask: /[a-z0-9_]/i,

    NIE: function (v) {
        var campoLimpio = v.toUpperCase().replace(" ", "").replace("-", "").replace("/", "");
        var letra = campoLimpio.substr(-1);
        var numeros = campoLimpio.substr(0, campoLimpio.length - 1);
        numeros = numeros.replace('X', 0);
        numeros = numeros.replace('Y', 1);
        numeros = numeros.replace('Z', 2);
        return "TRWAGMYFPDXBNJZSQVHLCKE".substr(numeros % 23, 1) == letra && letra.length == 1 && numeros.length == 8;

    }
});

Ext.apply(Ext.form.field.VTypes, {

    CIFText: 'Identificacion incorrecta',
    // CIFMask: /[a-z0-9_]/i,

    CIF: function (v) {
        var pares = 0;
        var impares = 0;
        var suma;
        var ultima;
        var unumero;
        var xxx;
        var uletra = new Array("J", "A", "B", "C", "D", "E", "F", "G", "H", "I");
        var campoLimpio = v.toUpperCase().replace(" ", "").replace("-", "").replace("/", "");
        ultima = campoLimpio.substr(8, 1);
        for (var cont = 1; cont < 7; cont++) {
            xxx = (2 * parseInt(campoLimpio.substr(cont++, 1))).toString() + "0";
            impares += parseInt(xxx.substr(0, 1)) + parseInt(xxx.substr(1, 1));
            pares += parseInt(campoLimpio.substr(cont, 1));
        }

        xxx = (2 * parseInt(campoLimpio.substr(cont, 1))).toString() + "0";
        impares += parseInt(xxx.substr(0, 1)) + parseInt(xxx.substr(1, 1));

        suma = (pares + impares).toString();
        unumero = parseInt(suma.substr(suma.length - 1, 1));
        unumero = (10 - unumero).toString();
        if (unumero == 10) unumero = 0;

        return (ultima == unumero) || (ultima == uletra[unumero]);

    }
});

Ext.define('CRM.controller.ControllerVentanaClienteCampanya', {
    extend: 'Ext.app.Controller',
    requires: ['CRM.model.enums.TiposSolicitudCampo', 'CRM.model.enums.TiposEstadoConfirmacionDato', 'CRM.model.enums.TiposValidaciones'],
    stores: ['StoreClientes', 'StoreClientesById', 'StoreCampanyasAccesoUser', 'StoreCampanyaDatos', 'StoreClienteCampanyas', 'StoreCampanyaClientes', 'StoreClientesDatosExtra',
        'StoreTiposDatosSelect', 'StoreTiposBoolean', 'StoreClienteItems', 'StoreTiposTarifasCampanya', 'StoreClienteTarifas', 'StoreTarifasClienteCampanya', 'StoreTiposEstadoCampanyaMotivos',
        'StoreClienteEstadoCampanyaMotivos', 'StoreTiposEstadoCampanya', 'StoreCampanyaFormulariosVentana', 'StoreTiposIdentificacion', 'StoreCampanyas', 'StoreClientesGrid'],
    models: ['Cliente', 'Persona', 'ClienteItem', 'ClienteDatoExtra', 'TipoDatoOpcionesNombre', 'DatosFormularioCampanya', 'DatosExtraFormulario', 'ClienteTarifa',
        'ClienteEstadoCampanyaMotivo', 'CampanyaCliente', 'ClienteGeneral'],
    views: ['clientes.VentanaClienteCampanya.PanelClienteCampanya', 'seguimientos.ComboCampanyas', 'clientes.VentanaClienteCampanya.PanelClienteCampanyaTarifa',
        'clientes.VentanaClienteCampanya.GridTarifasCampanya', 'clientes.VentanaClienteCampanya.PanelClienteCampanyaEstadoMotivos',
        'clientes.VentanaClienteCampanya.GridEstadoMotivosCampanya', 'clientes.VentanaClienteCampanya.PanelClienteCampanyaEstados', 'campanyas.formularios.VentanaCampanyaFormularios'],

    ventanaCliente: {},
    ventanaClienteTarifa: {},

    actualizarPanelSeguimientos: false,
    actualizarPanelClientes: false,

    campanya: null,
    cliente: null,
    estado: null,
    idAux: 0,

    refs: [{
        selector: 'ventanaAnyadirClienteCampanya button[name=finalizar]',
        ref: 'botonFinalizar'
    }, {
        selector: 'ventanaClienteCampanya',
        ref: 'ventanaClienteCampanya'
    }, {
        selector: 'ventanaClienteCampanya form[name=formClienteCampanya]',
        ref: 'formClienteCampanya'
    }, {
        selector: 'ventanaClienteCampanya button[action=anyadir-cliente]',
        ref: 'botonAnyadirClienteCampanya'
    }, {
        selector: 'ventanaClienteCampanya button[action=limpiar]',
        ref: 'botonLimpiarFormClienteCampanya'
    }, {
        selector: 'panelClienteCampanyaEstados combobox[name=comboCampanyas]',
        ref: 'comboCampanya'
    }, {
        selector: 'panelClienteCampanyaEstados combobox[name=comboClientes]',
        ref: 'ventanaComboCliente'
    }, {
        selector: 'panelClienteCampanyaEstados combobox[name=estadoCampanya]',
        ref: 'comboEstadoCampanya'
    }, {
        selector: 'panelClienteCampanyaTarifa grid[name=gridTarifasCampanya]',
        ref: 'gridTarifasCampanya'
    }, {
        selector: 'panelClienteCampanyaEstadoMotivos grid[name=gridEstadoMotivosCampanya]',
        ref: 'gridEstadoMotivosCampanya'
    }, {
        selector: 'ventanaCampanyaFormularios form[name=formPrevisualizacionClienteCampanya]',
        ref: 'formPrevisualizacionClienteCampanya'
    }],

    dirty: false,
    init: function () {
        this.control({
            'ventanaAnyadirClienteCampanya button[id=move-next]': {
                click: this.seleccionSiguientePanel
            },

            'ventanaAnyadirClienteCampanya button[id=finalizar]': {
                click: this.finalizarVentanaClienteCampanya
            },

            'ventanaAnyadirClienteCampanya button[action=close]': {
                click: this.cerrarVentanaClienteCampanya
            },
            'panelClienteCampanyaEstados combobox[name=comboCampanyas]': {
                select: this.seleccionaCampanya
            },
            'panelClienteCampanyaEstados combobox[name=comboClientes]': {
                LookoupWindowClickSeleccion: function (record) {
                    var store = this.getStoreClientesByIdStore();
                    store.load(
                        {
                            id: record.get('id'),
                            callback: function (records) {

                                this.cliente = records[0];
                            },
                            scope: this
                        });
                }
            },
            'panelClienteCampanyaEstados combobox[name=estadoCampanya]': {
                select: this.seleccionaEstadoCampanya
            },

            'ventanaClienteCampanya button[action=anyadir-cliente]': {
                click: this.anyadirCliente
            },
            'ventanaClienteCampanya button[action=limpiar]': {
                click: this.limpiarVentanaClienteCampanya
            },
            'ventanaClienteCampanya button[name=anyadirNuevoCampoSeleccionMultiple]': {
                click: function (but) {
                    var ant = this.getFormClienteCampanya().down("[name=seleccionMultiple" + but.grupoId + "]");
                    ant.add(this.getCampoTipoGrupoSeleccionMultiple(but.etiqueta, but.tipoId, but.campanyaDatoId, but.grupoId, null, but.requerido, but.subArray, false));
                }
            },
            'panelClienteCampanyaTarifa button[name=anyadir]': {
                click: this.anyadirTipoTarifaCampanyaCliente
            },

            'ventanaCampanyaFormularios form[name=formPrevisualizacionClienteCampanya]': {
                relacionados: function (mostrar, ocultar) {
                    this.mostrarRelacionados(this.getFormPrevisualizacionClienteCampanya(), mostrar, ocultar);
                }
            },
            'ventanaClienteCampanya form[name=formClienteCampanya]': {
                relacionados: function (mostrar, ocultar) {
                    this.mostrarRelacionados(this.getFormClienteCampanya(), mostrar, ocultar);
                }
            }

        });
    },

    seleccionSiguientePanel: function (btn) {
        switch (btn.up("panel").getLayout().getActiveItem().getXType()) {
            case 'panelClienteCampanyaEstados':
                this.anyadirClienteSinFormulario();
                this.navegar(btn.up("panel"), "next");
                break;
            case 'ventanaClienteCampanya':
                this.anyadirCliente(false);
                break;
            case 'panelClienteCampanyaTarifa':
                this.anyadirTipoTarifaCampanyaCliente();
                this.navegar(btn.up("panel"), "next");
                break;
            case 'panelClienteCampanyaEstadoMotivos':
                this.anyadirCampanyaClienteEstadoMotivo();
                break;
        }
    },

    seleccionaEstadoCampanya: function (comboEstados, estadoCampanyaSeleccionado) {
        var storeClienteItems = this.getStoreClienteItemsStore();
        storeClienteItems.load(
            {
                params: {
                    clienteId: this.cliente.get('id')
                },
                scope: this,
                callback: function () {
                    this.estado = estadoCampanyaSeleccionado[0];
                    this.creaVentana('CLI');
                }
            });
    },

    seleccionaCampanya: function (comboCampanyas, campanyaSeleccionada) {

        this.campanya = campanyaSeleccionada[0];
        this.getComboEstadoCampanya().setValue(null);
        var storeEstadosCampanya = this.getStoreTiposEstadoCampanyaStore();

        storeEstadosCampanya.load({
            url: '/crm/rest/tipoestadocampanya/campanya',
            params: {
                campanyaId: this.campanya.get('id')
            },
            scope: this,
            callback: function (records) {
                if (records.length > 0)
                    this.getComboEstadoCampanya().setVisible(true);
                else {
                    this.getComboEstadoCampanya().setVisible(false);
                }
            }
        });

        this.getStoreTiposTarifasCampanyaStore().load({
            params: {
                campanyaId: this.campanya.get('id')
            }
        })
    },

    finalizarVentanaClienteCampanya: function (btn) {
        // Si añado campanya y luego tarifa -> panelClienteCampanyaTarifa
        // Si añado tarifa -> panelClienteCampanyaTarifa
        if (btn.up("panel[name=panelPrincipalCardFormularios]").getLayout().getActiveItem()) {
            switch (btn.up("panel[name=panelPrincipalCardFormularios]").getLayout().getActiveItem().getXType()) {
                case 'panelClienteCampanyaEstados' :
                    this.anyadirClienteSinFormulario();
                    this.cerrarVentanaClienteCampanya();
                    break;
                case 'ventanaClienteCampanya':
                    this.anyadirCliente(true, this.ventanaCliente.cb);
                    break;
                case 'panelClienteCampanyaTarifa':
                    this.anyadirTipoTarifaCampanyaCliente(btn.up("panel").items.length, this.ventanaCliente.cb);
                    break;
                case 'panelClienteCampanyaEstadoMotivos':
                    this.anyadirCampanyaClienteEstadoMotivo(btn.up("panel").items.length, this.ventanaCliente.cb);
                    break;
            }
        }
        else {
            this.cerrarVentanaClienteCampanya();
            this.ventanaCliente.cb();
        }
    },

    navegar: function (panel, direction) {
        var layout = panel.getLayout();
        layout[direction]();
        Ext.getCmp('move-prev').setDisabled(!layout.getPrev());
        Ext.getCmp('move-next').setDisabled(!layout.getNext());

        Ext.getCmp('finalizar').setVisible(!layout.getNext());
    }    ,

    mostrarRelacionados: function (formulario, mostrar, ocultar) {
        Ext.Array.each(mostrar, function (item) {
            Ext.Array.each(formulario.query("[campanyaDatoId=" + item + "]"), function (objeto) {
                objeto.setVisible(true);
                objeto.setDisabled(false);
            });
        });

        Ext.Array.each(ocultar, function (item) {
            Ext.Array.each(formulario.query("[campanyaDatoId=" + item + "]"), function (objeto) {
                objeto.setVisible(false);
                objeto.setDisabled(true);
            });
        });

    },

    abrirVentanaAsociacionPorCampanyaYCliente: function (cam_cli, estado, cb) {
        var ref = this;
        ref.estado = estado;
        ref.actualizarPanelSeguimientos = true;
        var storeCampanya = Ext.create('CRM.store.StoreCampanyas').load({
            url: 'rest/campanya/' + cam_cli.get('campanyaId'),
            callback: function () {
                ref.campanya = storeCampanya.getById(cam_cli.get('campanyaId'));
                var storeCliente = Ext.create('CRM.store.StoreClientes').load({
                    url: 'rest/cliente/' + cam_cli.get('clienteId'),
                    callback: function () {
                        ref.cliente = storeCliente.getById(cam_cli.get('clienteId'));
                        ref.ventanaCliente = ref.getView('clientes.VentanaClienteCampanya.VentanaAnyadirClienteCampanya').create();
                        ref.ventanaCliente.cb = cb;
                        ref.creaVentana('CC');
                        ref.ventanaCliente.show();
                    }
                });
            }

        })
    },

    abrirVentanaAsociacionPorCliente: function (cliente, cb) {
        this.cliente = cliente;
        this.campanya = null;
        this.actualizarPanelClientes = true;
        this.ventanaCliente = this.getView('clientes.VentanaClienteCampanya.VentanaAnyadirClienteCampanya').create();
        this.ventanaCliente.cb = cb;
        var panel = this.ventanaCliente.down("panel");
        panel.insert(0, Ext.create('CRM.view.clientes.VentanaClienteCampanya.PanelClienteCampanyaEstados'));

        this.getVentanaComboCliente().setVisible(false);
        this.getComboEstadoCampanya().setVisible(false);

        this.ventanaCliente.show();
    },


    abrirVentanaClienteTarifa: function (campanya, clienteId) { //, cb) {
        this.campanya = campanya;
        var cliente = this.getController("ControllerPanelClientes").dameStoreTab().findRecord('id', clienteId);
        this.cliente = cliente;

        if (this.getStoreTiposTarifasCampanyaStore().getTotalCount() > 0) {
            this.ventanaCliente = this.getView('clientes.VentanaClienteCampanya.VentanaAnyadirClienteCampanya').create();
            var panel = this.ventanaCliente.down("panel");

            panel.remove(panel.down('[name=ventanaClienteCampanya]'));
            panel.remove(panel.down('[name=panelClienteCampanyaTarifa]'));
            panel.remove(panel.down('[name=panelClienteCampanyaEstadoMotivos]'));

            panel.insert(0, Ext.create('CRM.view.clientes.VentanaClienteCampanya.PanelClienteCampanyaTarifa'));
            panel.down("button[id=finalizar]").setVisible(true);
            this.ventanaCliente.show();
        }
        else {
            Ext.Msg.alert('Insercció de tarifes', '<b>No hi ha tarifes asignades a la campanya que es pugan afegir a el client</b>');
        }
    },

    creaVentana: function (origen) {
        var panel = this.ventanaCliente.down("panel");

        panel.remove(panel.down('[name=ventanaClienteCampanya]'));
        panel.remove(panel.down('[name=panelClienteCampanyaTarifa]'));
        panel.remove(panel.down('[name=panelClienteCampanyaEstadoMotivos]'));

        var storeEstadosMotivosCampanyaFormulario = this.getStoreTiposEstadoCampanyaMotivosStore();
        storeEstadosMotivosCampanyaFormulario.load({
            url: '/crm/rest/tipoestadocampanyamotivo',
            params: {tipoEstadoCampanyaId: this.estado.get('id')},
            scope: this,
            callback: function () {
                switch (origen) {
                    case 'CC':
                    case 'CLI':
                        var storeDatosExtra = this.getStoreClientesDatosExtraStore();
                        storeDatosExtra.load(
                            {
                                url: '/crm/rest/cliente/' + this.cliente.get('id') + '/datosextra',
                                callback: function () {
                                    var storeClienteItems = this.getStoreClienteItemsStore();
                                    storeClienteItems.load(
                                        {
                                            params: {
                                                clienteId: this.cliente.get('id')
                                            },
                                            callback: function () {
                                                this.cargaFormularioConCliente();
                                            },
                                            scope: this
                                        });
                                },
                                scope: this
                            });
                        break;
                    default :
                        this.cargaTarifasYMotivos();
                        break;
                }
            }
        });
    }
    ,

    cargaTarifasYMotivos: function () {
        var panel = this.ventanaCliente.down("panel");
        var storeEstadosMotivosCampanyaFormulario = this.getStoreTiposEstadoCampanyaMotivosStore();

        if (this.getStoreTiposTarifasCampanyaStore().getTotalCount() > 0 && this.estado.get('accion') == 'ESTADO-ALTA') {
            panel.insert(2, Ext.create('CRM.view.clientes.VentanaClienteCampanya.PanelClienteCampanyaTarifa'));
            if (panel.items.length > 1) {
                panel.down("button[id=move-next]").setDisabled(false);
                panel.down("button[id=finalizar]").setVisible(false);
            }
            if (panel.items.length == 1) {
                panel.down("button[id=finalizar]").setVisible(true);
            }
        }

        if (storeEstadosMotivosCampanyaFormulario.getTotalCount() > 0) {
            panel.insert(3, Ext.create('CRM.view.clientes.VentanaClienteCampanya.PanelClienteCampanyaEstadoMotivos'));
            if (panel.items.length > 1) {
                panel.down("button[id=move-next]").setDisabled(false);
                panel.down("button[id=finalizar]").setVisible(false);
            }
            if (panel.items.length == 1) {
                panel.down("button[id=finalizar]").setVisible(true);
            }
        }
    }
    ,

    abrirVentanaAsociacionPorClienteYCampanya: function (cliente, campanya, estadoCampanya, cb) {//, cb) {
        this.cliente = cliente;
        this.campanya = campanya;
        this.estado = estadoCampanya;


        this.actualizarPanelClientes = true;
        this.ventanaCliente = this.getView('clientes.VentanaClienteCampanya.VentanaAnyadirClienteCampanya').create();
        var panel = this.ventanaCliente.down("panel");
        panel.removeAll();
        this.ventanaCliente.cb = cb;

        this.creaVentana('CC');
        this.ventanaCliente.show();
    }
    ,

    cerrarVentanaClienteCampanya: function () {

        this.cliente = null;
        this.campanya = null;
        this.estado = null;
        this.ventanaCliente.destroy();
    }
    ,

    anyadirCampanyaClienteEstadoMotivo: function (numPaneles, cb) {
        var grid = this.getGridEstadoMotivosCampanya();
        var sel = grid.getSelectionModel().getSelection();
        if (sel.length > 0) {
            var estadoMotivoCampanyaCliente = this.getClienteEstadoCampanyaMotivoModel().create({
                campanyaEstadoMotivoId: sel[0].data.id,
                clienteId: this.cliente.get('id'),
                campanyaId: this.campanya.get('id'),
                comentarios: ''
            });

            var store = this.getStoreClienteEstadoCampanyaMotivosStore();
            store.add(estadoMotivoCampanyaCliente);
            store.sync({
                scope: this,
                callback: function () {
                    cb();
                    this.cerrarVentanaClienteCampanya();
                }
            });
        }
    }
    ,

    anyadirTipoTarifaCampanyaCliente: function (numPaneles, cb) {
        var grid = this.getGridTarifasCampanya();
        var sel = grid.getSelectionModel().getSelection();

        if (sel.length > 0) {

            var clienteId = this.cliente.get('id');
            var campanyaId = this.campanya;

            var clienteTarifa = this.getClienteTarifaModel().create({
                clienteId: clienteId,
                tipoTarifaId: sel[0].data.id
            });

            var gridClienteTarifasCampanya = Ext.ComponentQuery.query('gridClienteTarifasCampanya')[0];

            var store = gridClienteTarifasCampanya.getStore();
            store.add(clienteTarifa);

            store.sync({
                scope: this,
                callback: function (a, b) {

                    this.cerrarVentanaClienteCampanya();
                    if (cb) {
                        cb();
                    }
                    // Si no vamos a mostrar la pestaña directamente despues de añadir podemos evitar cargarlo
                    // numPaneles = 1 -> El cliente ya está en la campaña y le hemos añadido una tarifa
                    // numPaneles = 2 -> El cliente no estaba en la campaña, le hemos añadido y ademas creado una tarifa
                    if (numPaneles == 1) {
                        store.load({
                            url: '/crm/rest/clientetarifa/',
                            params: {
                                clienteId: clienteId,
                                campanyaId: campanyaId
                            }
                        });
                    }
                }
            });
        }
    },

    anyadirCliente: function (cerrarVentana, cb) {
        var form = this.getFormClienteCampanya();
        if (form.getForm().isValid()) {
            form.setLoading(true);
            var cliente = this.getClienteModel().create({
                'id': this.cliente.get('id'),
                // 'tipoIdentificacionId': this.getValorCampo('tipoIdentificacion'),
                // 'identificacion': this.getValorCampo('identificacion'),
                'nombre': this.getValorCampo('nombreOficial'),
                'apellidos': this.getValorCampo('apellidosOficial'),
                'correo': this.getValorCampo('correo'),
                'movil': this.getValorCampo('movil'),
                'prefijoTelefono': this.getValorCampo('prefijoTelefono'),
                'postal': this.getValorCampo('postal')
            });

            var clienteGeneral = this.getClienteGeneralModel().create({
                'id': this.cliente.get('clienteGeneralId'),
                'tipoIdentificacion': this.getValorCampo('tipoIdentificacion'),
                'identificacion': this.getValorCampo('identificacion'),
                'personaId': this.cliente.get('personaId')
            });

            var items = this.getFormClienteCampanya().getForm().getFields().items;
            var datos = [];
            var j = 0;
            for (var i = 0, lenj = items.length; i < lenj; i++) {
                var item = items[i];
                if (item.fieldCls == 'extra-data') {
                    var valor = '';
                    if (item.getXType() == 'datefield') {
                        valor = item.getRawValue();
                    } else {
                        valor = item.getValue();
                    }
                    var dato = this.getDatosExtraFormularioModel().create(
                        {
                            campanyaDatoId: item.campanyaDatoId,
                            valor: valor,
                            id: item.idAux,
                            padre: item.padreId,
                            clienteDatoId: item.clienteDatoId
                        });
                    datos[j] = dato.data;
                    j++;
                }
            }
            var formulario = this.getDatosFormularioCampanyaModel().create(
                {
                    clienteGeneral: clienteGeneral.data,
                    cliente: cliente.data,
                    estadoCampanya: this.estado.get("id"),
                    datosExtraFormulario: datos
                });

            Ext.Ajax.request(
                {
                    url: '/crm/rest/campanyacliente/campanya/' + this.campanya.get('id'),
                    method: 'POST',
                    jsonData: formulario.data,
                    scope: this,
                    success: function () {
                        form.setLoading(false);
                        if (cerrarVentana) {
                            this.ventanaCliente.cb();
                            this.cerrarVentanaClienteCampanya();
                        }
                        else {
                            this.navegar(form.up("panel[name=panelPrincipalCardFormularios]"), "next");
                        }
                    },
                    failure: function () {
                        form.setLoading(false);
                        Ext.Msg.alert('Insercció de clients', '<b>No es pot afegir el client</b>');
                    }
                });

        }
        else {
            var errorFields = form.query("field{isValid()==false}");
            var error = '';
            errorFields.forEach(function (field) {
                error += '<b>-Error en: ' + field.getName() + '<br>' + field.getErrors()[0] + '.</b><br>';
            });
            if (error != '' && errorFields.length > 0) {
                Ext.Msg.alert('Insercció de clients', error);
            }
            else {
                Ext.Msg.alert('Insercció de clients', '<b>El formulari no es correcte</b>');
            }
        }
    }
    ,

    anyadirClienteSinFormulario: function () {

        var campanyaCliente = this.getCampanyaClienteModel().create({
            clienteId: this.cliente.get('id'),
            campanyaId: this.campanya.get('id'),
            campanyaClienteTipoId: this.estado.get("id")
        });


        Ext.Ajax.request(
            {
                url: '/crm/rest/campanyacliente/',
                method: 'POST',
                jsonData: campanyaCliente.data,
                scope: this,
                success: function () {
                    var grid = this.getController("ControllerPanelClientesCampanyas").dameGridCampanyas();
                    grid.getStore().load();
                },
                failure: function () {
                }
            });



    }
    ,

    getValorCampo: function (nombreCampo) {
        var campo = this.getFormClienteCampanya().down('[name=' + nombreCampo + ']');
        return (campo != null) ? campo.value : null;
    }
    ,

    limpiarVentanaClienteCampanya: function () {
        this.getFormClienteCampanya().getForm().reset();
    }
    ,

    cargaFormularioConCliente: function () {
        var panel = this.ventanaCliente.down("panel");

        var store = this.getStoreCampanyaFormulariosVentanaStore();
        store.load({
            url: '/crm/rest/campanyaformulario/estado/',
            params: {
                campanyaId: this.campanya.get('id'),
                estadoId: this.estado.get('id')
            },
            scope: this,
            callback: function (records) {

                if (records.length > 0) {
                    panel.insert(1, Ext.create('CRM.view.clientes.VentanaClienteCampanya.PanelClienteCampanya'));
                    if (panel.items.length > 1) {
                        panel.down("button[id=move-next]").setDisabled(false);
                        panel.down("button[id=finalizar]").setVisible(false);
                    }
                    if (panel.items.length == 1) {
                        panel.down("button[id=finalizar]").setVisible(true);
                    }

                    var form = this.getFormClienteCampanya();
                    form.removeAll();

                    this.anyadirCamposCampanya(form);

                    var storeDatos = this.getStoreCampanyaDatosStore();
                    storeDatos.load(
                        {
                            url: '/crm/rest/campanyadato/',
                            params: {
                                formularioId: records[0].data.id
                            },
                            scope: this,
                            callback: function (records) {
                                for (var i = 0; i < records.length; i++) {
                                    if (!records[i].get('campanyaDatoExtraId')) {
                                        var field = this.introduceCampo(records[i], records, false);
                                        form.add(field);
                                    }
                                }
                                this.cargaTarifasYMotivos();
                            }

                        });

                }
                else {
                    panel.down("button[id=finalizar]").setVisible(true);
                    this.cargaTarifasYMotivos();
                }
            }

        });
    }
    ,

    dame_subarray: function (array, campanyaDatoExtraId) {
        var subarray = [];
        for (var i = 0; i < array.length; i++) {
            if (array[i].get('campanyaDatoExtraId') == campanyaDatoExtraId) {
                subarray.push(array[i]);
            }
        }
        return subarray;
    }
    ,

    construyeFormulario: function (form) {

        var store = this.getStoreCampanyaFormulariosVentanaStore();
        store.load(
            {
                url: '/crm/rest/campanyaformulario/estado/',
                params: {
                    campanyaId: this.campanya.get('id'),
                    estadoId: this.estado.get('id')
                },
                scope: this,
                callback: function (records) {

                    form.removeAll();
                    this.anyadirCamposCampanya(form);

                    var storeDatos = this.getStoreCampanyaDatosStore();
                    if (storeDatos.count() > 0) {
                        for (var i = 0; i < storeDatos.data.length; i++) {
                            if (!storeDatos.data.items[i].get('campanyaDatoExtraId')) {
                                var field = this.introduceCampo(storeDatos.data.items[i], storeDatos.data.items, false);
                                form.add(field);
                            }
                        }
                    }
                }
            })
    }
    ,

    introduceCampo: function (record, records, vacio, padreIdAux, tipoHijo, padreId) {
        var subArray = [];
        var tipoTipoId = record.get('clienteDatoTipoTipoId');
        var tipoId = record.get('clienteDatoTipoId');
        var etiqueta = record.get('textoCa');
        var requerido = record.get('requerido');
        var tipoAcceso = record.get('tipoAccesoId');
        var grupoId = record.get('grupoId');
        var campanyaDatoId = record.get('id');
        var visible = record.get('visualizar');
        var validarValue = record.get('validacionValue');

        var value = '';
        if (!vacio)
            value = this.getDatoExtraClienteByTipoId(tipoId, tipoHijo, padreId);
        if (tipoTipoId != CRM.model.enums.TiposDato.CABECERA.id) {
            subArray = this.dame_subarray(records, record.get('id'));
        }
        switch (tipoTipoId) {
            case CRM.model.enums.TiposDato.TEXTO.id:
                return this.getCampoTipoTexto(etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, visible, validarValue);
                break;
            case CRM.model.enums.TiposDato.TEXTO_LARGO.id:
                return this.getCampoTipoTextArea(etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, visible, validarValue);
                break;
            case CRM.model.enums.TiposDato.NUMERICO.id:
                return this.getCampoTipoNumerico(etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, visible, validarValue);
                break;
            case CRM.model.enums.TiposDato.FECHA.id:
                return this.getCampoTipoFecha(etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, visible);
                break;
            case CRM.model.enums.TiposDato.SELECCION.id:
                return this.getCampoTipoSeleccion(etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, record, visible);
                break;
            case CRM.model.enums.TiposDato.BINARIO.id:
                break;
            case CRM.model.enums.TiposDato.TELEFONO.id:
                return this.getCampoTipoNumerico(etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, visible);
                break;
            case CRM.model.enums.TiposDato.EMAIL.id:
                return this.getCampoTipoEmail(etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, visible);
                break;
            case CRM.model.enums.TiposDato.BOOLEAN.id:
                return this.getCampoTipoBoolean(etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, visible);
                break;
            case CRM.model.enums.TiposDato.ITEMS_SELECCION_MULTIPLE.id:
            case CRM.model.enums.TiposDato.ITEMS_SELECCION_MULTIPLE_CHECKS.id:
                return this.getCampoTipoGrupoSeleccionMultiple(etiqueta, tipoId, campanyaDatoId, grupoId, padreIdAux, requerido, subArray, true, visible);
                break;
            case CRM.model.enums.TiposDato.ITEMS_SELECCION_UNICA.id:
            case CRM.model.enums.TiposDato.ITEMS_SELECCION_UNICA_RADIO.id:
                return this.getCampoTipoGrupoSeleccionUnica(etiqueta, tipoId, campanyaDatoId, grupoId, padreIdAux, requerido, subArray, visible);
                break;
        }
    }
    ,

    getDatosExtraBytipoId: function (records, tipoId) {

        var limpios = [];
        var j = 0;
        for (var i = 0; i < records.length; i++) {
            if (records.items[i].data.tipoDatoId == tipoId) {
                limpios[j] = records.items[i].data;
                j++;
            }
        }
        return limpios;
    }
    ,

    getDatoExtraClienteByTipoId: function (tipoId, tipoHijo, padreId) {

        var i;

        var store = this.getStoreClientesDatosExtraStore();
        var encontrados = this.getDatosExtraBytipoId(store.data, tipoId);

        if (encontrados.length == 0) {
            return null;
        }
        else {
            if (padreId) {
                if (tipoHijo == 'DATO') {
                    for (i = 0; i < encontrados.length; i++) {
                        if (encontrados[i].clienteDatoId == padreId) {
                            return encontrados[i];
                        }
                    }
                }
                else {
                    for (i = 0; i < encontrados.length; i++) {
                        if (encontrados[i].itemId == padreId) {
                            return encontrados[i];
                        }
                    }
                }
                return null;
            }
            else
                return encontrados[0];
        }
    }
    ,

    dameIdAux: function () {
        this.idAux = this.idAux + 1;
        return this.idAux;
    }
    ,

    getCampoTipoTexto: function (etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, visible, validarValue) {
        var container;
        var padre;
        var field;

        if (subArray.length > 0) {
            container = Ext.create('Ext.form.FieldSet',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    campanyaDatoId: campanyaDatoId,
                    hidden: !visible,
                    disabled: !visible
                });

            field = Ext.create('Ext.form.field.Text',
                {
                    name: 'extra-P-' + tipoId,
                    fieldLabel: etiqueta,
                    flex: 1,
                    fieldCls: 'extra-data',
                    allowBlank: !requerido,
                    tipoAcceso: tipoAcceso,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible
                });
            field.tipoDatoId = tipoId;
            field.idAux = this.dameIdAux();

            if (value) {
                field.setValue(value.valor);
                field.clienteDatoId = value.id;
                padre = value.id;
            }
            container.add(field);

            for (var i = 0; i < subArray.length; i++) {
                container.add(this.introduceCampo(subArray[i], subArray, false, field.idAux, 'DATO', padre));
            }
            return container;
        }
        else {
            field = Ext.create('Ext.form.field.Text',
                {
                    name: 'extra-H-' + tipoId,
                    fieldLabel: etiqueta,
                    flex: 1,
                    fieldCls: 'extra-data',
                    allowBlank: !requerido,
                    tipoAcceso: tipoAcceso,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    // regex: this.dameRegexp(''),
                    hidden: !visible,
                    disabled: !visible,
                    vtype: validarValue
                });
            field.tipoDatoId = tipoId;
            field.idAux = this.dameIdAux();
            if (value) {
                field.setValue(value.valor);
                field.clienteDatoId = value.id;
            }

            return field;
        }
    }
    ,

    getCampoTipoTextArea: function (etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, visible, validarValue) {
        var container;
        var padre;
        var field;

        if (subArray.length > 0) {
            container = Ext.create('Ext.form.FieldSet',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    campanyaDatoId: campanyaDatoId,
                    hidden: !visible,
                    disabled: !visible
                });

            field = Ext.create('Ext.form.field.TextArea',
                {
                    name: 'extra-P-' + tipoId,
                    fieldLabel: etiqueta,
                    flex: 1,
                    fieldCls: 'extra-data',
                    allowBlank: !requerido,
                    tipoAcceso: tipoAcceso,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible
                });
            field.tipoDatoId = tipoId;
            field.idAux = this.dameIdAux();

            if (value) {
                field.setValue(value.valor);
                field.clienteDatoId = value.id;
                padre = value.id;
            }
            container.add(field);

            for (var i = 0; i < subArray.length; i++) {
                container.add(this.introduceCampo(subArray[i], subArray, false, field.idAux, 'DATO', padre));
            }
            return container;
        }
        else {
            field = Ext.create('Ext.form.field.TextArea',
                {
                    name: 'extra-H-' + tipoId,
                    fieldLabel: etiqueta,
                    flex: 1,
                    fieldCls: 'extra-data',
                    allowBlank: !requerido,
                    tipoAcceso: tipoAcceso,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible,
                    vtype: validarValue
                });
            field.tipoDatoId = tipoId;
            field.idAux = this.dameIdAux();
            if (value) {
                field.setValue(value.valor);
                field.clienteDatoId = value.id;
            }

            return field;
        }
    }
    ,

    getCampoTipoNumerico: function (etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, visible, validarValue) {
        var container;
        var padre;
        var field;

        if (subArray.length > 0) {
            container = Ext.create('Ext.form.FieldSet',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    campanyaDatoId: campanyaDatoId,
                    hidden: !visible,
                    disabled: !visible
                });
            field = Ext.create('Ext.form.field.Text',
                {
                    name: 'extra-P-' + tipoId,
                    fieldLabel: etiqueta,
                    regex: /^[0-9]+$/,
                    regexText: 'Sols pot contenir caracters numerics',
                    flex: 1,
                    fieldCls: 'extra-data',
                    allowBlank: !requerido,
                    tipoAcceso: tipoAcceso,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible
                });
            field.tipoDatoId = tipoId;
            field.idAux = this.dameIdAux();

            if (value) {
                field.setValue(value.valor);
                padre = value.id;
                field.clienteDatoId = value.id;
            }
            container.add(field);

            for (var i = 0; i < subArray.length; i++) {
                container.add(this.introduceCampo(subArray[i], subArray, false, field.idAux, 'DATO', padre));
            }
            return container;
        }
        else {
            field = Ext.create('Ext.form.field.Text',
                {
                    name: 'extra-H-' + tipoId,
                    fieldLabel: etiqueta,
                    flex: 1,
                    fieldCls: 'extra-data',
                    allowBlank: !requerido,
                    tipoAcceso: tipoAcceso,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible,
                    vtype: validarValue
                });
            field.tipoDatoId = tipoId;
            field.idAux = this.dameIdAux();

            if (value) {
                field.setValue(value.valor);
                field.clienteDatoId = value.id;
            }
            return field;
        }
    }
    ,

    getCampoTipoFecha: function (etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, visible) {
        var container;
        var padre;
        var field;

        if (subArray.length > 0) {
            container = Ext.create('Ext.form.FieldSet',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    campanyaDatoId: campanyaDatoId,
                    hidden: !visible,
                    disabled: !visible
                });
            field = Ext.create('Ext.form.field.Date',
                {
                    name: 'extra-P-' + tipoId,
                    fieldLabel: etiqueta,
                    format: 'd/m/Y',
                    flex: 1,
                    fieldCls: 'extra-data',
                    allowBlank: !requerido,
                    tipoAcceso: tipoAcceso,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible
                });
            field.tipoDatoId = tipoId;
            field.idAux = this.dameIdAux();

            if (value) {
                field.setValue(value.valor);
                padre = value.id;
                field.clienteDatoId = value.id;
            }
            container.add(field);

            for (var i = 0; i < subArray.length; i++) {
                container.add(this.introduceCampo(subArray[i], subArray, false, field.idAux, 'DATO', padre));
            }
            return container;
        }
        else {
            field = Ext.create('Ext.form.field.Date',
                {
                    name: 'extra-H-' + tipoId,
                    fieldLabel: etiqueta,
                    format: 'd/m/Y',
                    submitFormat: 'd/m/Y',
                    flex: 1,
                    fieldCls: 'extra-data',
                    allowBlank: !requerido,
                    tipoAcceso: tipoAcceso,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible
                });
            field.tipoDatoId = tipoId;
            field.idAux = this.dameIdAux();
            if (value) {
                field.setValue(value.valor);
                field.clienteDatoId = value.id;
            }

            return field;
        }
    }
    ,

    getCampoTipoEmail: function (etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, visible) {
        var container;
        var padre;
        var field;
        if (subArray.length > 0) {
            container = Ext.create('Ext.form.FieldSet',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    campanyaDatoId: campanyaDatoId,
                    hidden: !visible,
                    disabled: !visible
                });
            field = Ext.create('Ext.form.field.Text',
                {
                    name: 'extra-P-' + tipoId,
                    fieldLabel: etiqueta,
                    flex: 1,
                    vtype: 'email',
                    fieldCls: 'extra-data',
                    allowBlank: !requerido,
                    tipoAcceso: tipoAcceso,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible
                });
            field.tipoDatoId = tipoId;
            field.idAux = this.dameIdAux();

            if (value) {
                field.setValue(value.valor);
                padre = value.id;
                field.clienteDatoId = value.id;
            }
            container.add(field);

            for (var i = 0; i < subArray.length; i++) {
                container.add(this.introduceCampo(subArray[i], subArray, false, field.idAux, 'DATO', padre));
            }
            return container;
        }
        else {
            field = Ext.create('Ext.form.field.Text',
                {
                    name: 'extra-H-' + tipoId,
                    fieldLabel: etiqueta,
                    flex: 1,
                    vtype: 'email',
                    fieldCls: 'extra-data',
                    allowBlank: !requerido,
                    tipoAcceso: tipoAcceso,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible
                });
            field.tipoDatoId = tipoId;
            field.idAux = this.dameIdAux();
            if (value) {
                field.setValue(value.valor);
                field.clienteDatoId = value.id;
            }

            return field;
        }
    }
    ,

    getCampoTipoSeleccion: function (etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, recordpadre, visible) {
        var padre;
        var field;
        var store = Ext.create('Ext.data.Store', {
            model: 'CRM.model.TipoDatoOpcionesNombre'
        });

        var container;
        if (subArray.length > 0) {
            container = Ext.create('Ext.form.FieldSet',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    campanyaDatoId: campanyaDatoId,
                    hidden: !visible,
                    disabled: !visible
                });
            field = Ext.create('Ext.form.field.ComboBox',
                {
                    name: 'extra-P-' + tipoId,
                    fieldLabel: etiqueta,
                    flex: 1,
                    store: store,
                    displayField: 'nombre',
                    valueField: 'id',
                    fieldCls: 'extra-data',
                    queryMode: 'local',
                    allowBlank: !requerido,
                    tipoAcceso: tipoAcceso,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible,
                    listeners: {
                        select: function (obj, rec) {

                            this.up('form').fireEvent('relacionados', rec[0].data.mostrar, rec[0].data.ocultar);
                        }
                    }
                });
            field.tipoDatoId = tipoId;
            field.idAux = this.dameIdAux();
            field.store.loadData(recordpadre.get('opciones'), false);

            if (value) {
                field.setValue(parseInt(value.valor));
                padre = value.id;
                field.clienteDatoId = value.id;
            }

            container.add(field);

            for (var i = 0; i < subArray.length; i++) {
                container.add(this.introduceCampo(subArray[i], subArray, false, field.idAux, 'DATO', padre));
            }
            return container;
        }
        else {
            field = Ext.create('Ext.form.field.ComboBox',
                {
                    name: 'extra-H-' + tipoId,
                    fieldLabel: etiqueta,
                    flex: 1,
                    store: store,
                    displayField: 'nombre',
                    valueField: 'id',
                    fieldCls: 'extra-data',
                    queryMode: 'local',
                    allowBlank: !requerido,
                    tipoAcceso: tipoAcceso,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible,
                    listeners: {
                        select: function (obj, rec) {

                            this.up('form').fireEvent('relacionados', rec[0].data.mostrar, rec[0].data.ocultar);
                        }
                    }
                });
            field.tipoDatoId = tipoId;
            field.idAux = this.dameIdAux();
            field.store.loadData(recordpadre.get('opciones'), false);
            if (value) {
                field.setValue(parseInt(value.valor));
                field.clienteDatoId = value.id;
            }
            return field;
        }
    }
    ,

    getCampoTipoGrupoSeleccionMultiple: function (etiqueta, tipoId, campanyaDatoId, grupoId, padreIdAux, requerido, subArray, first, visible) {

        var idAux2;
        var field;
        var itemsDelGrupo;
        var combo;
        var i;

        var storeDatos = this.getStoreCampanyaDatosStore();
        var record = storeDatos.findRecord('id', campanyaDatoId);
        var store = Ext.create('Ext.data.Store',
            {
                model: 'CRM.model.Item'
            });
        var containerPrincipal = Ext.create('Ext.container.Container',
            {
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                }
            });
        var container;
        if (subArray.length > 0) {
            var containerFirst = Ext.create('Ext.container.Container',
                {
                    name: 'seleccionMultiple' + grupoId
                });

            container = Ext.create('Ext.form.FieldSet',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    campanyaDatoId: campanyaDatoId,
                    hidden: !visible,
                    disabled: !visible
                });
            if (first) {
                itemsDelGrupo = this.getClienteItemKeysByGrupoId(grupoId);
                if (itemsDelGrupo.length == 0) {
                    idAux2 = this.dameIdAux();
                    field = Ext.create('Ext.container.Container',
                        {
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    name: 'extra-P-' + tipoId,
                                    fieldLabel: etiqueta,
                                    flex: 1,
                                    store: store,
                                    displayField: 'nombreCa',
                                    valueField: 'id',
                                    fieldCls: 'extra-data',
                                    queryMode: 'local',
                                    allowBlank: !requerido,
                                    grupoId: grupoId,
                                    campanyaDatoId: campanyaDatoId,
                                    padreId: padreIdAux,
                                    idAux: idAux2,
                                    hidden: !visible,
                                    disabled: !visible
                                }]
                        });

                    combo = field.down("combobox[name=extra-P-" + tipoId + "]");
                    combo.store.loadData(record.get('itemsGrupo'), false);
                    for (i = 0; i < subArray.length; i++) {
                        field.add(this.introduceCampo(subArray[i], subArray, true, idAux2));
                    }

                    container.add(field);
                }
                else {
                    for (var j = 0; j < itemsDelGrupo.length; j++) {
                        idAux2 = this.dameIdAux();
                        field = Ext.create('Ext.container.Container',
                            {
                                layout: {
                                    type: 'vbox',
                                    align: 'stretch'
                                },
                                items: [
                                    {
                                        xtype: 'combobox',
                                        name: 'extra-P-' + tipoId,
                                        fieldLabel: etiqueta,
                                        flex: 1,
                                        store: store,
                                        displayField: 'nombreCa',
                                        valueField: 'id',
                                        fieldCls: 'extra-data',
                                        queryMode: 'local',
                                        allowBlank: !requerido,
                                        grupoId: grupoId,
                                        campanyaDatoId: campanyaDatoId,
                                        padreId: padreIdAux,
                                        idAux: idAux2,
                                        hidden: !visible,
                                        disabled: !visible
                                    }]
                            });

                        combo = field.down("combobox[name=extra-P-" + tipoId + "]");
                        combo.store.loadData(record.get('itemsGrupo'), false);
                        combo.setValue(itemsDelGrupo[j].itemId);
                        for (i = 0; i < subArray.length; i++) {
                            field.add(this.introduceCampo(subArray[i], subArray, false, idAux2, 'ITEM', itemsDelGrupo[j]));
                        }

                        container.add(field);
                    }
                }

                var button = Ext.create('Ext.button.Button',
                    {
                        text: 'afegir altre',
                        name: 'anyadirNuevoCampoSeleccionMultiple',
                        tipoId: tipoId,
                        tipoTipoId: CRM.model.enums.TiposDato.ITEMS_SELECCION_MULTIPLE.id,
                        etiqueta: etiqueta,
                        grupoId: grupoId,
                        requerido: requerido,
                        campanyaDatoId: campanyaDatoId,
                        subArray: subArray,
                        hidden: !visible,
                        disabled: !visible
                    });
                containerFirst.add(container);
                containerPrincipal.add(containerFirst);
                containerPrincipal.add(button);

                return containerPrincipal;
            }
            else {
                field = Ext.create('Ext.form.field.ComboBox',
                    {
                        name: 'extra-P-' + tipoId,
                        fieldLabel: etiqueta,
                        flex: 1,
                        store: store,
                        displayField: 'nombreCa',
                        valueField: 'id',
                        fieldCls: 'extra-data',
                        queryMode: 'local',
                        allowBlank: !requerido,
                        campanyaDatoId: campanyaDatoId,
                        padreId: padreIdAux,
                        hidden: !visible,
                        disabled: !visible
                    });
                field.grupoId = grupoId;
                field.idAux = this.dameIdAux();
                field.store.loadData(record.get('itemsGrupo'), false);

                container.add(field);
                for (i = 0; i < subArray.length; i++) {
                    container.add(this.introduceCampo(subArray[i], subArray, true, field.idAux));
                }
                return container;
            }
        }
        else {
            field = Ext.create('Ext.ux.form.field.BoxSelect',
                {
                    name: 'extra-H-' + tipoId,
                    fieldLabel: etiqueta,
                    flex: 1,
                    store: store,
                    displayField: 'nombreCa',
                    valueField: 'id',
                    fieldCls: 'extra-data',
                    queryMode: 'local',
                    allowBlank: !requerido,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible
                });
            field.grupoId = grupoId;
            field.idAux = this.dameIdAux();
            field.store.loadData(record.get('itemsGrupo'), false);
            itemsDelGrupo = this.getClienteItemKeysByGrupoId(grupoId);
            field.setValue(itemsDelGrupo.itemId);
            return field;
        }
    }
    ,

    getCampoTipoGrupoSeleccionUnica: function (etiqueta, tipoId, campanyaDatoId, grupoId, padreIdAux, requerido, subArray, visible) {

        var field;
        var itemsDelGrupo;

        var storeDatos = this.getStoreCampanyaDatosStore();
        var record = storeDatos.findRecord('id', campanyaDatoId);
        var store = new Ext.data.Store(
            {
                model: 'CRM.model.Item'
            });
        var container;
        if (subArray.length > 0) {
            container = Ext.create('Ext.form.FieldSet',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    campanyaDatoId: campanyaDatoId,
                    hidden: !visible,
                    disabled: !visible
                });
            field = Ext.create('Ext.form.field.ComboBox',
                {
                    name: 'extra-P-' + tipoId,
                    fieldLabel: etiqueta,
                    flex: 1,
                    store: store,
                    displayField: 'nombreCa',
                    valueField: 'id',
                    fieldCls: 'extra-data',
                    queryMode: 'local',
                    allowBlank: !requerido,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible,
                    listeners: {
                        select: function (obj, rec) {

                            this.up('form').fireEvent('relacionados', rec[0].data.mostrar, rec[0].data.ocultar);
                        }
                    }
                });
            field.grupoId = grupoId;
            field.idAux = this.dameIdAux();
            field.store.loadData(record.get('itemsGrupo'), false);
            itemsDelGrupo = this.getClienteItemKeysByGrupoId(grupoId);

            if (itemsDelGrupo.length > 0) {
                field.setValue(itemsDelGrupo[0].itemId);
                field.clienteDatoId = itemsDelGrupo[0].id;
            }

            container.add(field);

            for (var i = 0; i < subArray.length; i++) {
                container.add(this.introduceCampo(subArray[i], subArray, false, field.idAux, 'ITEM', itemsDelGrupo[0]));
            }
            return container;
        }
        else {
            field = Ext.create('Ext.form.field.ComboBox',
                {
                    name: 'extra-H-' + tipoId,
                    fieldLabel: etiqueta,
                    flex: 1,
                    store: store,
                    displayField: 'nombreCa',
                    valueField: 'id',
                    fieldCls: 'extra-data',
                    queryMode: 'local',
                    allowBlank: !requerido,
                    campanyaDatoId: campanyaDatoId,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible,
                    listeners: {
                        select: function (obj, rec) {

                            this.up('form').fireEvent('relacionados', rec[0].data.mostrar, rec[0].data.ocultar);
                        }
                    }
                });
            field.grupoId = grupoId;
            field.idAux = this.dameIdAux();
            field.store.loadData(record.get('itemsGrupo'), false);
            itemsDelGrupo = this.getClienteItemKeysByGrupoId(grupoId);
            if (itemsDelGrupo.length > 0) {
                field.setValue(itemsDelGrupo[0].itemId);
                field.clienteDatoId = itemsDelGrupo[0].id;
            }

            return field;
        }
    }
    ,

    getClienteItemKeysByGrupoId: function (grupoId) {
        var keys = [];
        var store = this.getStoreClienteItemsStore();
        var records = store.snapshot || store.data;
        records.each(function (record) {
            if (record.data.grupoId === grupoId) {
                keys.push({itemId: record.data.itemId, id: record.data.id});
            }
        });
        return keys;
    }
    ,

    getCampoTipoBoolean: function (etiqueta, tipoId, campanyaDatoId, value, padreIdAux, requerido, tipoAcceso, subArray, visible) {
        var container;
        var padre;
        var field;

        if (subArray.length > 0) {
            container = Ext.create('Ext.form.FieldSet',
                {
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    campanyaDatoId: campanyaDatoId,
                    clienteDatoId: value.id,
                    hidden: !visible,
                    disabled: !visible
                });
            field = Ext.create('Ext.form.field.ComboBox',
                {
                    name: 'extra-P-' + tipoId,
                    fieldLabel: etiqueta,
                    flex: 1,
                    store: 'StoreTiposBoolean',
                    displayField: 'nombre',
                    valueField: 'id',
                    fieldCls: 'extra-data',
                    queryMode: 'local',
                    allowBlank: !requerido,
                    tipoAcceso: tipoAcceso,
                    campanyaDatoId: campanyaDatoId,
                    clienteDatoId: value.id,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible,
                    listeners: {
                        select: function (obj, rec) {

                            this.up('form').fireEvent('relacionados', rec[0].data.mostrar, rec[0].data.ocultar);
                        }
                    }
                });
            field.tipoDatoId = tipoId;
            field.idAux = this.dameIdAux();

            if (value) {
                field.setValue(parseInt(value.valor));
                padre = value.id;
            }
            container.add(field);

            for (var i = 0; i < subArray.length; i++) {
                container.add(this.introduceCampo(subArray[i], subArray, false, field.idAux, 'DATO', padre));
            }
            return container;
        }

        else {
            field = Ext.create('Ext.form.field.ComboBox',
                {
                    name: 'extra-H-' + tipoId,
                    fieldLabel: etiqueta,
                    flex: 1,
                    store: 'StoreTiposBoolean',
                    displayField: 'nombre',
                    valueField: 'id',
                    fieldCls: 'extra-data',
                    queryMode: 'local',
                    allowBlank: !requerido,
                    tipoAcceso: tipoAcceso,
                    campanyaDatoId: campanyaDatoId,
                    clienteDatoId: value.id,
                    padreId: padreIdAux,
                    hidden: !visible,
                    disabled: !visible,
                    listeners: {
                        select: function (obj, rec) {

                            this.up('form').fireEvent('relacionados', rec[0].data.mostrar, rec[0].data.ocultar);
                        }
                    }
                });
            field.tipoDatoId = tipoId;
            field.idAux = this.dameIdAux();
            if (value)
                field.setValue(parseInt(value.valor));
            return field;
        }

    }
    ,

    anyadirCamposCampanya: function (form) {

        var cliente = this.getCliente();
        var clienteGeneralId = cliente.get('clienteGeneralId');
        var storeClienteGeneral = Ext.create('CRM.store.StoreClientesGrid').load({
            url: 'rest/clientegrid/' + clienteGeneralId,
            scope : this,
            callback: function(){

                var clienteGeneral = storeClienteGeneral.getById(clienteGeneralId);
                var contenedorIdentificacion = Ext.create('Ext.container.Container', {
                    layout: {
                        type: 'hbox'
                    }
                });

                var identificacionTipo = Ext.create('Ext.form.ComboBox', {
                    name: 'tipoIdentificacion',
                    fieldLabel: 'Tipo',
                    allowBlank: true,
                    displayField: 'nombre',
                    valueField: 'id',
                    queryMode: 'local',
                    store: 'StoreTiposIdentificacion',
                    value: clienteGeneral.get('tipoIdentificacion'),
                    flex: 1,
                    labelWidth: 40,
                    padding: '0 5 5 0',
                    listeners: {
                        select: function (store, sel) {
                            if (sel[0].data.nombre == 'Pasaporte') {
                                form.down("textfield[name=identificacion]").vtype = '';
                            }
                            else {
                                form.down("textfield[name=identificacion]").vtype = sel[0].data.nombre;
                            }
                        }
                    }
                });
                contenedorIdentificacion.add(identificacionTipo);
                var identificacionField = Ext.create('Ext.form.field.Text',
                    {
                        name: 'identificacion',
                        fieldLabel: 'Identificación',
                        allowBlank: true,
                        displayField: 'identificacion',
                        valueField: 'identificacion',
                        value: clienteGeneral.get('identificacion'),
                        flex: 2,
                        labelWidth: 70
                    });
                contenedorIdentificacion.add(identificacionField);
                form.add(contenedorIdentificacion);

                this.setPropiedadesCampo(identificacionField, this.getStoreCampanyaFormulariosVentanaStore().data.items[0].data.solicitarIdentificacion);
                this.setPropiedadesCampo(identificacionTipo, this.getStoreCampanyaFormulariosVentanaStore().data.items[0].data.solicitarIdentificacion);
                // this.setPropiedadesCampo(identificacionField, this.getStoreCampanyaDatosStore().data.items[0].data.solicitarIdentificacion);
                // this.setPropiedadesCampo(identificacionTipo, this.getStoreCampanyaDatosStore().data.items[0].data.solicitarIdentificacion);

                var nombreOficialField = Ext.create('Ext.form.field.Text',
                    {
                        name: 'nombreOficial',
                        fieldLabel: 'Nom',
                        allowBlank: true,
                        displayField: 'nombre',
                        valueField: 'nombre',
                        maxLength: 200,
                        value: cliente.get('nombre')
                    });
                form.add(nombreOficialField);
                this.setPropiedadesCampo(nombreOficialField, this.getStoreCampanyaFormulariosVentanaStore().data.items[0].data.solicitarNombre);

                var apellidosOficialField = Ext.create('Ext.form.field.Text',
                    {
                        name: 'apellidosOficial',
                        fieldLabel: 'Cognoms',
                        allowBlank: true,
                        displayField: 'apellidos',
                        valueField: 'apellidos',
                        maxLength: 200,
                        value: cliente.get('apellidos')
                    });
                form.add(apellidosOficialField);
                this.setPropiedadesCampo(apellidosOficialField, this.getStoreCampanyaFormulariosVentanaStore().data.items[0].data.solicitarNombre);

                var correoField = Ext.create('Ext.form.field.Text',
                    {
                        name: 'correo',
                        fieldLabel: 'Correu electrónic',
                        allowBlank: true,
                        displayField: 'correo',
                        valueField: 'correo',
                        maxLength: 50,
                        vtype: 'email',
                        value: cliente.get('correo')
                    });
                form.add(correoField);
                this.setPropiedadesCampo(correoField, this.getStoreCampanyaFormulariosVentanaStore().data.items[0].data.solicitarCorreo);

                var containerMovil = Ext.create('Ext.container.Container', {
                    layout: {
                        type: 'hbox'
                    }
                });

                var prefijoTelefonoField = Ext.create('Ext.form.field.Text',
                    {
                        name: 'prefijoTelefono',
                        fieldLabel: 'Prefijo',
                        allowBlank: true,
                        displayField: 'prefijoTelefono',
                        valueField: 'prefijoTelefono',
                        maxLength: 5,
                        value: cliente.get('prefijoTelefono'),
                        padding: '0 5 5 0',
                        labelWidth: 40,
                        regex: new RegExp('^\\+[0-9]+$'),
                        regexText: 'Prefijo incorrecto',
                        flex: 1
                    });
                // form.add(movilField);
                containerMovil.add(prefijoTelefonoField);


                var movilField = Ext.create('Ext.form.field.Text',
                    {
                        name: 'movil',
                        fieldLabel: 'Móbil',
                        allowBlank: true,
                        displayField: 'movil',
                        valueField: 'movil',
                        maxLength: 9,
                        minLength: 9,
                        value: cliente.get('movil'),
                        labelWidth: 70,
                        flex: 2,
                        regex: new RegExp('^[0-9]+$'),
                        regexText: 'Número incorrecto'
                    });
                // form.add(movilField);
                containerMovil.add(movilField);

                form.add(containerMovil);
                this.setPropiedadesCampo(movilField, this.getStoreCampanyaFormulariosVentanaStore().data.items[0].data.solicitarMovil);
                this.setPropiedadesCampo(prefijoTelefonoField, this.getStoreCampanyaFormulariosVentanaStore().data.items[0].data.solicitarMovil);


                var postalField = Ext.create('Ext.form.field.TextArea',
                    {
                        name: 'postal',
                        fieldLabel: 'Correu Postal',
                        allowBlank: true,
                        displayField: 'postal',
                        valueField: 'postal',
                        value: cliente.get('postal')
                    });
                this.setPropiedadesCampo(postalField, this.getStoreCampanyaFormulariosVentanaStore().data.items[0].data.solicitarPostal);
                form.add(postalField);
            }
        });


    }
    ,

    setPropiedadesCampo: function (campo, tipoSolicitud) {
        switch (tipoSolicitud) {
            case CRM.model.enums.TiposSolicitudCampo.NO_SOLICITAR:
                campo.setVisible(false);
                campo.allowBlank = true;
                break;
            case CRM.model.enums.TiposSolicitudCampo.OPCIONAL:
                campo.setVisible(true);
                campo.allowBlank = true;
                campo.validate();
                break;
            case CRM.model.enums.TiposSolicitudCampo.OBLIGATORIO:
                campo.setVisible(true);
                campo.allowBlank = false;
                campo.validate();
                break;
        }
    }
    ,

    getCliente: function () {
        return this.cliente;
    }
    ,

    getCampanya: function () {
        return this.campanya;
    }
})
;