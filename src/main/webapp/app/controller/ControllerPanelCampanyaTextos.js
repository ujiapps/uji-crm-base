Ext.define('CRM.controller.ControllerPanelCampanyaTextos', {
    extend: 'Ext.app.Controller',
    stores: ['StoreCampanyaTextos'],
    models: ['CampanyaTexto'],
    views: ['auxiliares.campanyaTextos.VentanaCampanyaTextos'],

    ventanaCampanyaTextos: undefined,

    refs: [{
        selector: 'gridCampanyaTextos',
        ref: 'gridCampanyaTextos'
    }, {
        selector: 'gridCampanyaTextos button[name=borrarCampanyaTexto]',
        ref: 'botonBorrarCampanyaTexto'
    }, {
        selector: 'ventanaCampanyaTextos',
        ref: 'ventanaCampanyaTextos'
    }, {
        selector: 'ventanaCampanyaTextos [id=formCampanyaDato]',
        ref: 'formCampanyaDato'
    }, {
        selector: 'ventanaCampanyaTextos [id=formCampanyaDato] [name=codigo]',
        ref: 'textfieldCodigo'
    }],

    init: function () {
        this.control(
            {
                'gridCampanyaTextos button[name=borrarCampanyaTexto]':
                    {
                        click: this.borrarCampanyaTexto
                    },
                'gridCampanyaTextos':
                    {
                        select: this.setEstadoComponentes,
                        celldblclick: this.mostrarVentanaCampanyaTextos
                    },
                'gridCampanyaTextos button[name=anyadirCampanyaTexto]':
                    {
                        click: this.mostrarVentanaCampanyaTextos
                    },
                'ventanaCampanyaTextos button[action=cancelar]':
                    {
                        click: this.cerrarVentanaCampanyaTextos
                    },
                'ventanaCampanyaTextos button[action=guardar]':
                    {
                        click: this.guardarCampanyaTexto
                    }
            });
    },

    borrarCampanyaTexto: function () {
        var ref = this;
        var store = ref.getStoreCampanyaTextosStore();
        var selection = this.getGridCampanyaTextos().getSelectionModel().getSelection();
        var etiquetaId = (selection.length > 0) ? selection[0].data.id : null;
        var etiqueta = store.findRecord('id', etiquetaId);

        if (etiqueta) {
            Ext.Msg.confirm('Esborrar', 'Esteu segur/a de voler esborrar el registre sel·leccionat?', function (btn) {
                if (btn == 'yes') {
                    store.remove(etiqueta);
                    store.sync(
                        {
                            success: function () {
                                ref.setEstadoComponentes();
                            },
                            failure: function () {
                                Ext.Msg.alert('Esborrar', 'No s`ha pogut esborrar el registre perquè té registres associats.');
                                store.rejectChanges();
                                ref.setEstadoComponentes();
                            }
                        });
                }
            });
        }
    },

    crearVentanaCampanyaTextosView: function () {
        if (this.ventanaCampanyaTextos === undefined) {
            this.ventanaCampanyaTextos = this.getView('auxiliares.campanyaTextos.VentanaCampanyaTextos').create();
        }
        return this.ventanaCampanyaTextos;
    },

    cerrarVentanaCampanyaTextos: function () {
        this.ventanaCampanyaTextos.destroy();
        this.ventanaCampanyaTextos = undefined;
        this.getStoreCampanyaTextosStore().rejectChanges();
    },

    mostrarVentanaCampanyaTextos: function (grid, view, index, select) {
        this.crearVentanaCampanyaTextosView().show();
        if (select) {
            this.getFormCampanyaDato().getForm().setValues(select.data);
        }
        else {
            this.getTextfieldCodigo().setDisabled(false);
        }
    },

    guardarCampanyaTexto: function () {
        var store = this.getStoreCampanyaTextosStore();
        var valoresForm = this.getFormCampanyaDato().getForm().getValues();
        if (valoresForm.id !== '') {
            var campanyaTexto = store.findRecord('id', valoresForm.id);
            campanyaTexto.set(valoresForm);
        }
        else {
            var campanyaTexto = this.getCampanyaTextoModel().create(valoresForm);
            store.add(campanyaTexto);
        }

        store.sync(
            {
                callback: function (success) {
                    if (success) {
                        this.cerrarVentanaCampanyaTextos();
                        this.setEstadoComponentes();
                    }
                    else {
                        Ext.Msg.alert('Textes LOPD', '<b>Error al desar les dades</b>');
                    }
                },
                scope: this
            });
    },

    setEstadoComponentes: function () {
        var tieneSeleccionGridCampanyaTextos = this.getGridCampanyaTextos().getSelectionModel().hasSelection();
        this.getBotonBorrarCampanyaTexto().setDisabled(!tieneSeleccionGridCampanyaTextos);
    }

});