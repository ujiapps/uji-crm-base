Ext.define('CRM.controller.ControllerPanelCampanyas',
    {
        extend: 'Ext.app.Controller',
        stores: ['StoreCampanyas', 'StoreProgramas', 'StoreCampanyaFases', 'StoreCampanyaItems', 'StoreFases', 'StoreClientesDatosTipos', 'StoreGrupos', 'StoreGruposCampanya', 'StoreItemsCampanya',
            'StoreItemsVentanaCampanyas', 'StoreCampanyaEnvios', 'StoreTiposCampanyaEnviosAuto', 'StoreSubVinculos', 'StoreCampanyaTextos', 'StoreServiciosByUsuarioId', 'StoreCampanyaEnviosAdmin',
            'StoreCampanyasCampanyasDestino', 'StoreGruposTodos', 'StoreCampanyaActos', 'StoreLineasFacturacionByCampanya', 'StoreTiposEstadoCampanya',
            'StoreTiposEstadoCampanyaMotivos', 'StoreCampanyasEnviosProgramados', 'StoreTiposTarifas'],
        models: ['Campanya', 'CampanyaFase', 'CampanyaEnvio', 'CampanyaItem', 'CampanyaEnvioAdmin', 'CampanyaCampanya', 'CampanyaActo', 'TipoEstadoCampanya',
            'TipoEstadoCampanyaMotivo', 'CampanyaEnvioProgramado'],
        views: ['campanyas.GridCampanyaFases', 'campanyas.GridCampanyaItems', 'campanyas.VentanaCampanyaItems', 'campanyas.GridItems', 'campanyas.ComboItems',
            'campanyas.GridCampanyaEnvios', 'campanyas.VentanaCampanya', 'campanyas.VentanaCampanyaEnvios', 'campanyas.VentanaCampanyaEnviosAdmin', 'campanyas.VentanaCampanyaActos',
            'campanyas.estados.GridCampanyaEstados', 'campanyas.estados.GridCampanyaEstadoMotivos', 'campanyas.GridEnviosCampanyaProgramados', 'campanyas.VentanaCampanyaDestino'],

        ventanaCampanya: {},
        ventanaCampanyaItems: {},
        ventanaCampanyaEnvios: {},
        ventanaCampanyaEnviosAdmin: {},
        ventanaCampanyaActos: {},
        ventanaCampanyaDestino: {},

        refs: [{
            selector: 'panelCampanyas',
            ref: 'panelCampanyas'
        }, {
            selector: 'panelCampanyas treepanel[name=gridCampanyas]',
            ref: 'gridCampanyas'
        }, {
            selector: 'panelCampanyas treepanel[name=gridCampanyas] button[name=anyadirCampanya]',
            ref: 'botonAnyadirCampanya'
        }, {
            selector: 'panelCampanyas treepanel[name=gridCampanyas] button[name=borrarCampanya]',
            ref: 'botonBorrarCampanya'
        }, {
            selector: 'gridCampanyaFases',
            ref: 'gridCampanyaFases'
        }, {
            selector: 'gridCampanyaFases button[name=borrarCampanyaFase]',
            ref: 'botonBorrarCampanyaFase'
        }, {
            selector: 'gridCampanyaActos',
            ref: 'gridCampanyaActos'
        }, {
            selector: 'gridCampanyaActos button[name=borrar]',
            ref: 'botonBorrarCampanyaActo'
        }, {
            selector: 'gridCampanyaItems',
            ref: 'gridCampanyaItems'
        }, {
            selector: 'gridCampanyaItems button[name=borrar]',
            ref: 'botonBorrarCampanyaItem'
        }, {
            selector: 'ventanaCampanyaItems[name=gridItems]',
            ref: 'gridItems'
        }, {
            selector: 'gridCampanyaEstados',
            ref: 'gridCampanyaEstados'
        }, {
            selector: 'gridCampanyaEstados button[name=borrar]',
            ref: 'botonBorrarCampanyaEstados'
        }, {
            selector: 'gridCampanyaEstadoMotivos',
            ref: 'gridCampanyaEstadoMotivos'
        }, {
            selector: 'gridCampanyaEstadoMotivos button[name=borrar]',
            ref: 'botonBorrarCampanyaEstadoMotivo'
        }, {
            selector: 'gridCampanyaEstadoMotivos button[name=anyadir]',
            ref: 'botonAnyadirCampanyaEstadoMotivo'
        }, {
            selector: 'tabCampanyas',
            ref: 'tabCampanyas'
        }, {
            selector: 'gridCampanyaEnviosAuto',
            ref: 'gridCampanyaEnviosAuto'
        }, {
            selector: 'gridCampanyaEnviosAdmin',
            ref: 'gridCampanyaEnviosAdmin'
        }, {
            selector: 'gridCampanyaEnviosAuto button[name=borrarCampanyaEnvio]',
            ref: 'botonBorrarCampanyaEnvio'
        }, {
            selector: 'gridCampanyaEnviosAdmin button[name=borrarCampanyaEnvioAdmin]',
            ref: 'botonBorrarCampanyaEnvioAdmin'
        }, {
            selector: 'ventanaCampanyaEnvios',
            ref: 'ventanaCampanyaEnvios'
        }, {
            selector: 'ventanaCampanyaEnvios container[name=editor]',
            ref: 'editorVentanaCampanyaEnvios'
        }, {
            selector: 'ventanaCampanyaEnviosAdmin',
            ref: 'ventanaCampanyaEnviosAdmin'
        }, {
            selector: 'ventanaCampanyaActos',
            ref: 'ventanaCampanyaActos'
        }, {
            selector: 'ventanaCampanyaActos form[name=formActoCampanya]',
            ref: 'formActoCampanya'
        }, {
            selector: 'ventanaCampanyaActos button[name=anyadirActoCampanya]',
            ref: 'botonAnyadirActoCampanya'
        }, {
            selector: 'ventanaCampanyaActos button[name=actualizarActoCampanya]',
            ref: 'botonActualizarActoCampanya'
        }, {
            selector: 'gridCampanyaEnviosAuto textfield[name=emailPrueba]',
            ref: 'emailPrueba'
        }, {
            selector: 'gridCampanyaEnviosAuto button[name=botonEnviarPrueba]',
            ref: 'botonEnviarPruebaEnvio'
        }, {
            selector: 'gridCampanyasCampanyasDestino',
            ref: 'gridCampanyasCampanyasDestino'
        }, {
            selector: 'gridCampanyasCampanyasDestino button[name=borrarCampanyasCampanyasDestino]',
            ref: 'botonBorrarCampanyasCampanyasDestino'
        }, {
            selector: 'ventanaCampanyaDestino combobox[name=estadoDestino]',
            ref: 'comboCampanyaEstadoDestino'
        }, {
            selector: 'ventanaCampanya',
            ref: 'ventanaCampanya'
        }, {
            selector: 'gridEnviosCampanyaProgramados',
            ref: 'gridEnviosCampanyaProgramados'
        }, {
            selector: 'gridEnviosCampanyaProgramados button[name=borrarEnvioProgramado]',
            ref: 'botonBorrarEnvioProgramado'
        }],

        dirty: false,
        init: function () {
            this.control(
                {
                    'panelCampanyas treepanel[name=gridCampanyas]': {
                        selectionchange: this.onGridCampanyaSelectionChange,
                        itemdblclick: this.mostrarVentanaActualizarCampanya
                        // render: this.setEstadoComponentes
                    },
                    'panelCampanyas treepanel[name=gridCampanyas] button[name=anyadirCampanya]': {
                        click: this.mostrarVentanaCampanya
                    },
                    'panelCampanyas treepanel[name=gridCampanyas] button[name=borrarCampanya]': {
                        click: this.borrarCampanya
                    },

                    'panelCampanyas treepanel[name=gridCampanyas] button[name=anyadirCampanyaActos]': {
                        click: this.anyadirCampanyaGenericaActo
                    },

                    'panelCampanyas treepanel[name=gridCampanyas] button[name=anyadirCampanyaActosConPagos]': {
                        click: this.anyadirCampanyaGenericaActoConPago
                    },

                    'ventanaCampanya button[name=anyadirCampanya]': {
                        click: this.anyadirCampanya
                    },
                    'ventanaCampanya button[name=actualizarCampanya]': {
                        click: this.actualizarCampanya
                    },

                    'gridCampanyaFases': {
                        select: this.setEstadoComponentes,
                        validateedit: this.modificarFaseId,
                        edit: this.sincronizarCampanyaFase,
                        canceledit: this.setEstadoComponentes
                    },

                    'gridCampanyaFases button[name=anyadirCampanyaFase]': {
                        click: this.anyadirCampanyaFase
                    },

                    'gridCampanyaFases button[name=borrarCampanyaFase]': {
                        click: this.borrarCampanyaFase
                    },

                    'gridCampanyaItems button[name=anyadir]': {
                        click: this.mostrarVentanaCampanyaItem
                    },
                    'gridCampanyaItems button[name=borrar]': {
                        click: this.borrarCampanyaItem
                    },
                    'gridCampanyaItems': {
                        selectionchange: this.onGridCampanyaItemsSelectionChange
                    },

                    'ventanaCampanyaItems combobox[name=filtroGrupos]': {
                        select: this.onGrupoSelected
                    },
                    'ventanaCampanyaItems button[action=anyadir]': {
                        click: this.anyadirCampanyaItems
                    },
                    'ventanaCampanyaItems button[action=cancelar]': {
                        click: this.cerrarVentanaCampanyaItems
                    },
                    'ventanaCampanyaItems [name=gridItems]': {
                        itemdblclick: this.anyadirCampanyaItems
                    },
                    'gridCampanyaEnviosAuto': {
                        select: this.setEstadoComponentes,
                        celldblclick: this.mostrarVentanaCampanyaEnvios
                    },
                    'gridCampanyaEnviosAuto button[name=anyadirCampanyaEnvio]': {
                        click: this.mostrarVentanaCampanyaEnvios
                    },
                    'gridCampanyaEnviosAuto button[name=borrarCampanyaEnvio]': {
                        click: this.borrarCampanyaEnvioAuto

                    },
                    'gridCampanyaEnviosAuto button[name=botonEnviarPrueba]': {
                        click: this.enviarPruebaEmail
                    },
                    'ventanaCampanyaEnvios button[action=cancelar]': {
                        click: this.cerrarVentanaCampanyaEnvios
                    },
                    'ventanaCampanyaEnvios button[action=anyadir]': {
                        click: this.anyadirCampanyaEnvioAuto
                    },
                    'ventanaCampanyaEnvios button[action=actualizar]': {
                        click: this.actualizarCampanyaEnvioAuto
                    },

                    'gridCampanyaEnviosAdmin': {
                        select: this.setEstadoComponentes,
                        celldblclick: this.mostrarVentanaCampanyaEnviosAdmin
                    },
                    'gridCampanyaEnviosAdmin button[name=anyadirCampanyaEnvioAdmin]': {
                        click: this.mostrarVentanaCampanyaEnviosAdmin
                    },
                    'gridCampanyaEnviosAdmin button[name=borrarCampanyaEnvioAdmin]': {
                        click: this.borrarCampanyaEnvioAdmin

                    },
                    'ventanaCampanyaEnviosAdmin button[action=cancelar]': {
                        click: this.cerrarVentanaCampanyaEnviosAdmin
                    },
                    'ventanaCampanyaEnviosAdmin button[action=anyadir]': {
                        click: this.anyadirCampanyaEnvioAdmin
                    },
                    'ventanaCampanyaEnviosAdmin button[action=actualizar]': {
                        click: this.actualizarCampanyaEnvioAdmin
                    },

                    'gridCampanyasCampanyasDestino': {
                        //validateedit: function (editor, e) {
                        //    this.modificarValoresGridCampanyaDestino(editor, e);
                        //},
                        // edit: this.sincronizarCampanyaCampanyaDestino,
                        select: this.setEstadoComponentes
                    },
                    'gridCampanyasCampanyasDestino button[name=anyadirCampanyasCampanyasDestino]': {
                        click: this.abrirVentanaCampanyaCampanyaDestino
                    },
                    'gridCampanyasCampanyasDestino button[name=borrarCampanyasCampanyasDestino]': {
                        click: this.borrarCampanyaCampanyaDestino
                    },

                    'ventanaCampanyaDestino button[name=anyadirCampanyaDestino]': {
                        click: this.anyadirCampanyaCampanyaDestino
                    },

                    'ventanaCampanyaDestino combobox[name=campanyaDestino]': {
                        select: this.cargaStoreEstadoDestinoCampanyas
                    },

                    'gridCampanyaActos':
                        {
                            selectionchange: this.onGridCampanyaActosSelectionChange,
                            itemdblclick:
                            this.mostrarVentanaActualizarCampanyaActo
                            //validateedit: this.actualizarIdsFilaGridCampanyaDatos,
                            //edit: this.sincronizarDatos
                        }
                    ,

                    'gridCampanyaActos button[name=anyadir]':
                        {
                            //click: this.anyadirCampanyaDato
                            click: this.mostrarVentanaCampanyaActos
                        }
                    ,
                    'gridCampanyaActos button[name=borrar]':
                        {
                            click: this.borrarCampanyaActo
                        }
                    ,

                    'ventanaCampanyaActos button[name=anyadirActoCampanya]':
                        {
                            click: this.anyadirCampanyaActo
                        }
                    ,

                    'ventanaCampanyaActos button[name=actualizarActoCampanya]':
                        {
                            click: this.actualizarCampanyaActo
                        }
                    ,

                    'ventanaCampanyaActos button[name=cancelarActoCampanya]':
                        {
                            click: this.cerrarVentanaCampanyaActos
                        }
                    ,
                    'gridCampanyaEstados button[name=anyadir]':
                        {
                            click: this.anyadirCampanyaEstado
                        }
                    ,
                    'gridCampanyaEstados button[name=borrar]':
                        {
                            click: this.borrarCampanyaEstado
                        }
                    ,
                    'gridCampanyaEstados':
                        {
                            edit: this.sincronizarCampanyaEstado,
                            select:
                            this.cargaGridCampanyaEstadoMotivos
                            //selectionchange: this.onGridCampanyaEstadoSelectionChange
                        }
                    ,
                    'gridCampanyaEstadoMotivos button[name=anyadir]':
                        {
                            click: this.anyadirCampanyaEstadoMotivo
                        }
                    ,
                    'gridCampanyaEstadoMotivos button[name=borrar]':
                        {
                            click: this.borrarCampanyaEstadoMotivo
                        }
                    ,
                    'gridCampanyaEstadoMotivos':
                        {
                            edit: this.sincronizarCampanyaEstadoMotivo,
                            select:
                            this.setEstadoComponentes
                        }
                    ,
                    'gridEnviosCampanyaProgramados button[name=anyadirEnvioProgramado]':
                        {
                            click: this.anyadirEnvioCampanyaProgramado
                        }
                    ,

                    'gridEnviosCampanyaProgramados button[name=borrarEnvioProgramado]':
                        {
                            click: this.borrarEnvioCampanyaProgramado
                        }
                    ,

                    'gridEnviosCampanyaProgramados':
                        {
                            edit: this.sincronizarEnvioCampanyaProgramado,
                            select:
                            this.setEstadoComponentesVentanaEnvio
                        }

                })
            ;
        },


        //////////////////////////  CAMPAÑAS  /////////////////////////////////////////////////////////////////


        getVentanaCampanyaView: function () {
            return this.getView('campanyas.VentanaCampanya').create();
        },

        mostrarVentanaActualizarCampanya: function (obj, record) {

            this.ventanaCampanya = this.getVentanaCampanyaView();
            this.getVentanaCampanya().cargaVentanaCampanya(record);
            this.ventanaCampanya.show();
        },

        mostrarVentanaCampanya: function () {
            this.ventanaCampanya = this.getVentanaCampanyaView();
            this.getVentanaCampanya().cargaVentanaCampanya();
            this.ventanaCampanya.show();
        },

        anyadirCampanya: function () {

            var store = this.getStoreCampanyasStore();
            var campanya = this.getCampanyaModel().create();
            store.add(campanya);
            campanya.set(this.getVentanaCampanya().dameDatosCampanya());

            store.sync({
                scope: this,
                success: function () {
                    // store.reload();
                    this.getVentanaCampanya().cerrarVentanaCampanya();
                    this.getPanelCampanyas().setLoading(true);
                    this.getGridCampanyas().getStore().load({
                        scope : this,
                        callback: function(){
                            this.getPanelCampanyas().setLoading(false);
                        }
                    });
                }
            });
        },

        anyadirCampanyaGenericaActo: function () {
            var ref = this;
            var store = this.getGridCampanyas().getStore();
            ref.getPanelCampanyas().setLoading(true);
            Ext.Ajax.request(
                {
                    url: '/crm/rest/campanya/generica/acto',
                    method: 'POST',
                    callback: function () {
                        store.load({
                            callback: function(){
                                ref.getPanelCampanyas().setLoading(false);
                            }
                        });
                    }
                });
        },

        anyadirCampanyaGenericaActoConPago: function () {
            var ref = this;
            var store = this.getGridCampanyas().getStore();
            var tarifas = this.getStoreTiposTarifasStore();
            ref.getPanelCampanyas().setLoading(true);

            Ext.Ajax.request(
                {
                    url: '/crm/rest/campanya/generica/actoconpago',
                    method: 'POST',
                    callback: function () {
                        store.load({
                            callback: function(){
                                ref.getPanelCampanyas().setLoading(false);
                            }
                        });
                        tarifas.reload();
                    }
                });
        },

        actualizarCampanya: function () {
            var ref = this;
            var store = this.getStoreCampanyasStore();
            var campanyaId = this.getVentanaCampanya().dameCampanyaId();
            var campanya = store.findRecord('id', campanyaId);
            ref.getPanelCampanyas().setLoading(true);
            campanya.set(this.getVentanaCampanya().dameDatosCampanya());
            store.sync({
                scope: this,
                success: function () {
                    ref.getGridCampanyas().getStore().load({
                        callback: function(){
                            ref.getPanelCampanyas().setLoading(false);
                            this.getVentanaCampanya().cerrarVentanaCampanya();
                        }
                    });
                }
            });
        },

        borrarCampanya: function () {
            var ref = this;
            Ext.Msg.confirm('Eliminació de campanyes', '<b>Esteu segur/a de voler esborrar la campanya sel·leccionada?</b>', function (btn) {
                if (btn == 'yes') {
                    var campanya = ref.getGridCampanyas().getSelectionModel().getSelection();
                    if (campanya.length == 1) {
                        var registro = campanya[0];
                        Ext.Ajax.request({
                            url: '/crm/rest/campanya/' + registro.data.id,
                            method: 'DELETE',
                            success: function () {
                                ref.setEstadoComponentes();
                                ref.getPanelCampanyas().setLoading(true);
                                ref.getGridCampanyas().getStore().load({
                                    callback: function () {
                                        ref.getPanelCampanyas().setLoading(false);
                                    }
                                });
                            },
                            failure: function () {
                                Ext.Msg.alert('Esborrar Campanya', 'No s`ha pogut esborrar la campanya perquè té registres associats.');
                                ref.setEstadoComponentes();
                            }
                        });
                        // var store = ref.getStoreCampanyasStore();
                        // console.log(store);
                        // store.remove(registro);
                        // store.sync(
                        //     {
                        //         success: function () {
                        //             ref.setEstadoComponentes();
                        //             ref.getGridCampanyas().setLoading(true);
                        //             ref.getGridCampanyas().getStore().load({
                        //                 callback: function(){
                        //                     ref.getGridCampanyas().setLoading(false);
                        //                 }
                        //             });
                        //         },
                        //         failure: function () {
                        //             Ext.Msg.alert('Esborrar Campanya', 'No s`ha pogut esborrar la campanya perquè té registres associats.');
                        //             store.rejectChanges();
                        //             ref.setEstadoComponentes();
                        //         }
                        //     });
                    }
                }
            });
        },

        onGridCampanyaSelectionChange: function () {
            if (this.getGridCampanyas().getSelectionModel().hasSelection()) {
                if (this.getGridCampanyas().getSelectionModel().getSelection()[0].data.leaf) {
                    var campanyaId = this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyas());
                    var programaId = this.getGridCampanyas().getSelectionModel().getSelection()[0].data.programaId;

                    this.cargaStoreTiposEstadoCampanya(campanyaId);
                    this.cargaStoreFases(campanyaId);
                    this.cargaStoreActos(campanyaId);
                    this.cargaStoreItemsCampanya(campanyaId);
                    this.cargaStoreCampanyaEnvios(campanyaId);
                    this.cargaStoreCampanyaEnviosAdmin(campanyaId);
                    this.getController("ControllerPanelCampanyasCartas").cargaStoreCampanyaCartas(campanyaId);
                    this.cargaStoreCampanyasCampanyasDestino(campanyaId);
                    this.cargaStoreLineasFacturacionByCampanya(campanyaId);
                    this.cargaStoreSubVinculos(programaId);

                    this.getController("ControllerPanelCampanyasFormularios").cargaStoreFormularios(campanyaId);

                    var storeGruposCampanya = this.getStoreGruposCampanyaStore();
                    storeGruposCampanya.load(
                        {
                            url: '/crm/rest/campanya/' + campanyaId + '/grupo'
                        });

                    var storeItemsCampanya = this.getStoreItemsCampanyaStore();
                    storeItemsCampanya.load(
                        {
                            url: '/crm/rest/campanya/' + campanyaId + '/item'
                        });
                } else {
                    this.limpiarGridDetalles();
                }

            } else {
                this.limpiarGridDetalles();
            }
            this.setEstadoComponentes();
            this.getTabCampanyas().setActiveTab(0);
        },

        limpiarGridDetalles: function () {
            this.getStoreCampanyaFasesStore().loadData([], false);
            //this.getStoreCampanyaDatosStore().loadData([], false);
            this.getStoreCampanyaItemsStore().loadData([], false);
            this.getStoreCampanyaActosStore().loadData([], false);
            this.getStoreTiposEstadoCampanyaMotivosStore().loadData([], false);
            this.getStoreTiposEstadoCampanyaStore().loadData([], false);
            this.getController("ControllerPanelCampanyasFormularios").getStoreCampanyaFormulariosStore().loadData([], false);
            this.getStoreCampanyaEnviosAdminStore().loadData([], false);
            this.getStoreCampanyaEnviosStore().loadData([], false);
            this.getController("ControllerPanelCampanyasCartas").getStoreCampanyaCartasStore().loadData([], false);
            this.getStoreCampanyasCampanyasDestinoStore().loadData([], false);
            this.getStoreLineasFacturacionByCampanyaStore().loadData([], false);
            this.getStoreSubVinculosStore().loadData([], false);
        }
        ,

////////////////////////////// FASES  ////////////////////////////////////////////////////////////////////

        anyadirCampanyaFase: function () {

            var store = this.getStoreCampanyaFasesStore();

            var rec = this.getCampanyaFaseModel().create(
                {
                    campanyaId: this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyas())
                });
            var rowEditor = this.getGridCampanyaFases().getPlugin('editingFases');

            rowEditor.cancelEdit();
            store.insert(0, rec);
            rowEditor.startEdit(0, 0);
        }
        ,

        sincronizarCampanyaFase: function () {
            this.getStoreCampanyaFasesStore().sync(
                {
                    scope: this,
                    callback: function () {
                        this.getStoreCampanyaFasesStore().reload();

                    }
                });

        }
        ,

        borrarCampanyaFase: function () {
            var ref = this;
            Ext.Msg.confirm('Eliminació de fases', '<b>Esteu segur/a de voler esborrar la fase de la campanya sel·leccionada?</b>', function (btn) {
                if (btn == 'yes') {
                    var campanyaFase = ref.getGridCampanyaFases().getSelectionModel().getSelection();
                    if (campanyaFase.length == 1) {
                        var registro = campanyaFase[0];
                        var store = ref.getStoreCampanyaFasesStore();
                        store.remove(registro);
                        store.sync(
                            {
                                success: function () {
                                    ref.setEstadoComponentes();
                                },
                                failure: function () {
                                    Ext.Msg.alert('Esborrar fase', 'No s`ha pogut esborrar la fase perquè té seguiments associats.');
                                    store.rejectChanges();
                                    ref.setEstadoComponentes();
                                },
                                scope: this
                            });
                    }
                }
            });
        }
        ,

/////////////////////////////////////  ENVIOS  ///////////////////////////////////////////////////

        anyadirCampanyaEnvioAuto: function () {

            var store = this.getStoreCampanyaEnviosStore();
            var form = this.getVentanaCampanyaEnvios();
            var campanyaId = this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyas());

            if (form.down("form").getForm().isValid()) {
                var rec = this.getCampanyaEnvioModel().create(
                    {
                        campanyaId: campanyaId,
                        campanyaEnvioTipoId: form.down("combo[name=tipoEnvioAuto]").getValue(),
                        asunto: form.down("textfield[name=asuntoEnvioAuto]").getValue(),
                        //cuerpo: form.down("htmleditor[name=cuerpoEnvioAuto]").getValue(),
                        cuerpo: form.down('ckeditor[name=cuerpo]').getValue(),
                        desde: form.down("textfield[name=desde]").getValue(),
                        responder: form.down("textfield[name=responder]").getValue(),
                        tipoCorreoEnvio: form.down("combobox[name=tipoCorreoEnvio]").getValue(),
                        base: form.down("textfield[name=base]").getValue()
                    });

                store.add(rec);
                store.sync(
                    {
                        scope: this,
                        callback: function (success) {
                            if (success) {
                                this.cerrarVentanaCampanyaEnvios();
                                this.cargaStoreCampanyaEnvios(campanyaId);
                            } else {
                                Ext.Msg.alert('Eliminació de enviaments', '<b>L\'enviament no ha sigut eliminat</b>');
                            }
                        }
                    });
            }
        },

        actualizarCampanyaEnvioAuto: function () {
            var store = this.getStoreCampanyaEnviosStore();
            var form = this.getVentanaCampanyaEnvios();
            var campanyaId = this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyas());
            var envioId = this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaEnviosAuto());

            if (form.down("form").getForm().isValid()) {
                var envio = store.findRecord('id', envioId);

                envio.set('campanyaEnvioTipoId', form.down("combo[name=tipoEnvioAuto]").getValue());
                envio.set('asunto', form.down("textfield[name=asuntoEnvioAuto]").getValue());
                //envio.set('cuerpo', form.down("htmleditor[name=cuerpoEnvioAuto]").getValue());
                envio.set('cuerpo', form.down('ckeditor[name=cuerpo]').getValue().replace(/<\/?base[^>]*>/g, ""));
                envio.set('desde', form.down("textfield[name=desde]").getValue());
                envio.set('responder', form.down("textfield[name=responder]").getValue());
                envio.set('tipoCorreoEnvio', form.down("combobox[name=tipoCorreoEnvio]").getValue());
                envio.set('base', form.down("textfield[name=base]").getValue());
                store.sync(
                    {
                        scope: this,
                        callback: function (success) {
                            if (success) {
                                this.cerrarVentanaCampanyaEnvios();
                                this.cargaStoreCampanyaEnvios(campanyaId);
                            } else {
                                Ext.Msg.alert('Actualització de enviaments', '<b>L\'enviament no ha sigut actualitzar</b>');
                            }
                        }
                    });

            }
        },

        borrarCampanyaEnvioAuto: function () {
            var ref = this;
            Ext.Msg.confirm('Eliminació de Enviaments', '<b>Esteu segur/a de voler esborrar el enviament de la campanya sel·leccionada?</b>', function (btn) {
                if (btn == 'yes') {
                    var campanyaEnvioAuto = ref.getGridCampanyaEnviosAuto().getSelectionModel().getSelection();
                    if (campanyaEnvioAuto.length == 1) {
                        var registro = campanyaEnvioAuto[0];
                        var store = ref.getStoreCampanyaEnviosStore();
                        store.remove(registro);
                        store.sync(
                            {
                                success: function () {
                                    ref.setEstadoComponentes();
                                },
                                failure: function () {
                                    Ext.Msg.alert('Esborrar Enviament', 'No s`ha pogut esborrar el enviament.');
                                    store.rejectChanges();
                                    ref.setEstadoComponentes();
                                },
                                scope: this
                            });
                    }
                }
            });
        }
        ,

        enviarPruebaEmail: function () {
            var emailPrueba = this.getEmailPrueba();
            var selection = this.getGridCampanyaEnviosAuto().getSelectionModel().getSelection();
            var campanyaEnvioId = (selection.length > 0) ? selection[0].data.id : null;
            if (!emailPrueba.isValid() || emailPrueba.getValue() === '') {
                Ext.Msg.alert('Proba d\'Enviament', 'La direcció de eMail no es válida');
                return;
            }

            Ext.Ajax.request(
                {
                    url: '/crm/rest/campanyaenvioauto/prueba',
                    method: 'POST',
                    jsonData: {
                        email: emailPrueba.getValue(),
                        id: campanyaEnvioId
                    },
                    success: function () {
                        Ext.Msg.alert('Proba d\'Enviament', 'El enviament s\'ha realitzat correctament');
                    },
                    failure: function () {
                        Ext.Msg.alert('Proba d\'Enviament', 'S\'ha produït un error en realitzar l\'enviament');
                    }
                });
        }
        ,

///////////////////////////////CAMPANYAS DESTINO ///////////////////////////////////////

        abrirVentanaCampanyaCampanyaDestino: function () {
            ventanaCampanyaDestino = this.getView('campanyas.VentanaCampanyaDestino').create();
            ventanaCampanyaDestino.show();
        },


        cargaStoreEstadoDestinoCampanyas: function (selector, elem) {
            var storeCampanyaEstadoDestino = this.getComboCampanyaEstadoDestino().getStore();
            storeCampanyaEstadoDestino.load(
                {
                    url: '/crm/rest/tipoestadocampanya/campanya/',
                    params: {
                        campanyaId: elem[0].data.id
                    },
                    scope: this,
                    callback: function () {
                        this.setEstadoComponentes();
                    }
                });
        },

        anyadirCampanyaCampanyaDestino: function () {
            var store = this.getStoreCampanyasCampanyasDestinoStore();

            var rec = this.getCampanyaCampanyaModel().create(
                {
                    campanyaOrigenId: this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyas()),
                    campanyaDestinoId: ventanaCampanyaDestino.down("combobox[name=campanyaDestino]").value,
                    estadoOrigenId: ventanaCampanyaDestino.down("combobox[name=estadoOrigen]").value,
                    estadoDestinoId: ventanaCampanyaDestino.down("combobox[name=estadoDestino]").value
                });

            store.insert(0, rec);
            store.sync();
            ventanaCampanyaDestino.destroy();

        },

        borrarCampanyaCampanyaDestino: function () {
            var ref = this;
            Ext.Msg.confirm('Eliminació de campanyes relacionades', '<b>Esteu segur/a de voler esborrar la campanya relacionada con la campanya sel·leccionada?</b>', function (btn) {
                if (btn == 'yes') {
                    var campanyaCampanya = ref.getGridCampanyasCampanyasDestino().getSelectionModel().getSelection();
                    if (campanyaCampanya.length == 1) {
                        var registro = campanyaCampanya[0];
                        var store = ref.getStoreCampanyasCampanyasDestinoStore();
                        store.remove(registro);
                        store.sync(
                            {
                                success: function () {
                                    ref.setEstadoComponentes();
                                },
                                failure: function () {
                                    Ext.Msg.alert('Eliminació de campanyes relacionades', 'No s`ha pogut esborrar la campanya.');
                                    store.rejectChanges();
                                    ref.setEstadoComponentes();
                                },
                                scope: this
                            });
                    }
                }
            });
        }
        ,

//////////////////////////////////ENVIOS ADMIN ////////////////////////////////////////////////////////

        anyadirCampanyaEnvioAdmin: function () {
            var store = this.getStoreCampanyaEnviosAdminStore();
            var form = this.getVentanaCampanyaEnviosAdmin();
            var campanyaId = this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyas());

            var rec = this.getCampanyaEnvioAdminModel().create(
                {
                    campanyaId: campanyaId,
                    campanyaEnvioTipoId: form.down("combo[name=tipoEnvioAuto]").getValue(),
                    asunto: form.down("textfield[name=asuntoEnvioAuto]").getValue(),
                    cuerpo: form.down("htmleditor[name=cuerpoEnvioAuto]").getValue(),
                    emails: form.down("textfield[name=emails]").getValue()
                });

            store.add(rec);
            store.sync(
                {
                    scope: this,
                    callback: function (success) {
                        if (success) {
                            this.cerrarVentanaCampanyaEnviosAdmin();
                            this.cargaStoreCampanyaEnviosAdmin(campanyaId);
                        } else {
                            Ext.Msg.alert('Afegir enviament', '<b>L\'enviament no ha sigut afegit</b>');
                        }
                    }
                });
        }
        ,

        actualizarCampanyaEnvioAdmin: function () {
            var store = this.getStoreCampanyaEnviosAdminStore();
            var form = this.getVentanaCampanyaEnviosAdmin();
            var campanyaId = this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyas());
            var envioId = this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaEnviosAdmin());

            var envio = store.findRecord('id', envioId);

            envio.set('campanyaEnvioTipoId', form.down("combo[name=tipoEnvioAuto]").getValue());
            envio.set('asunto', form.down("textfield[name=asuntoEnvioAuto]").getValue());
            envio.set('cuerpo', form.down("htmleditor[name=cuerpoEnvioAuto]").getValue());
            envio.set('emails', form.down("textfield[name=emails]").getValue());

            store.sync(
                {
                    scope: this,
                    callback: function (success) {
                        if (success) {
                            this.cerrarVentanaCampanyaEnviosAdmin();
                            this.cargaStoreCampanyaEnviosAdmin(campanyaId);
                        } else {
                            Ext.Msg.alert('Actualització de enviaments', '<b>L\'enviament no ha sigut actualitzar</b>');
                        }
                    }
                });

        }
        ,

        borrarCampanyaEnvioAdmin: function () {
            var ref = this;
            Ext.Msg.confirm('Eliminació de Enviaments', '<b>Esteu segur/a de voler esborrar el enviament de la campanya sel·leccionada?</b>', function (btn) {
                if (btn == 'yes') {
                    var campanyaEnvioAdmin = ref.getGridCampanyaEnviosAdmin().getSelectionModel().getSelection();
                    if (campanyaEnvioAdmin.length == 1) {
                        var registro = campanyaEnvioAdmin[0];
                        var store = ref.getStoreCampanyaEnviosAdminStore();
                        store.remove(registro);
                        store.sync(
                            {
                                success: function () {
                                    ref.setEstadoComponentes();
                                },
                                failure: function () {
                                    Ext.Msg.alert('Esborrar Enviament', 'No s`ha pogut esborrar el enviament.');
                                    store.rejectChanges();
                                    ref.setEstadoComponentes();
                                },
                                scope: this
                            });
                    }
                }
            });
        }
        ,
//////////////////////////////////////CAMPANYA ACTOS  ////////////////////////////////////////////////////////////////////////////////////////

        anyadirCampanyaActo: function () {

            var form = this.getFormActoCampanya();

            if (form.getForm().isValid()) {
                form.submit({
                    url: '/crm/rest/campanyaacto/' + this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyas()),
                    scope: this,
                    success: function () {
                        this.cerrarVentanaCampanyaActos();
                        this.sincronizarActos();
                    },
                    failure: function () {
                        Ext.Msg.alert('Insercció de actes', '<b>El formulari no es correcte</b>');
                    }
                });
            } else {
                Ext.Msg.alert('Afegir Actes', 'El formulari no es correcte');
            }
        }
        ,

        actualizarCampanyaActo: function () {

            var form = this.getFormActoCampanya();

            if (form.getForm().isValid()) {

                //var store = this.getStoreCampanyaActosStore();

                form.submit({
                    url: '/crm/rest/campanyaacto/actualizar/' + this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaActos()),
                    scope: this,
                    success: function () {
                        this.cerrarVentanaCampanyaActos();
                        this.sincronizarActos();
                    },
                    failure: function () {
                        Ext.Msg.alert('Insercció de actes', '<b>El formulari no es correcte</b>');
                    }
                });
            } else {
                Ext.Msg.alert('Actualitzar Actes', 'El formulari no es correcte');
            }
        }
        ,


        sincronizarActos: function () {
            var store = this.getStoreCampanyaActosStore();
            store.reload();
        }
        ,

        borrarCampanyaActo: function () {
            var store = this.getStoreCampanyaActosStore();
            var ref = this;

            var campanyaActo = store.findRecord('id', this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaActos()));

            Ext.Msg.confirm('Eliminar', 'El registre s\'eliminarà. Estàs segur de voler continuar?', function (btn) {
                if (btn == 'yes') {
                    store.remove(campanyaActo);
                    store.sync({
                        callback: function () {
                            ref.setEstadoComponentes();
                        }
                    });
                }
            });
        }
        ,

////////////////////////////////////// CAMPANYA ESTADOS ////////////////////////////////////////////////////////////////////////////////////////

        anyadirCampanyaEstado: function () {

            var store = this.getStoreTiposEstadoCampanyaStore();

            var rec = this.getTipoEstadoCampanyaModel().create(
                {
                    campanyaId: this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyas())
                });

            var rowEditor = this.getGridCampanyaEstados().getPlugin('editingCampanyaEstado');

            rowEditor.cancelEdit();
            store.insert(0, rec);
            rowEditor.startEdit(0, 0);

        }
        ,

        sincronizarCampanyaEstado: function () {
            var store = this.getStoreTiposEstadoCampanyaStore();
            store.sync({
                callback: function () {
                    store.reload();
                }
            });
        }
        ,

        cargaGridCampanyaEstadoMotivos: function () {

            if (this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaEstados()) != null) {

                var storeCampanyaEstadoMotivos = this.getStoreTiposEstadoCampanyaMotivosStore();
                storeCampanyaEstadoMotivos.load(
                    {
                        params: {
                            tipoEstadoCampanyaId: this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaEstados())
                        },
                        scope: this,
                        callback: function () {
                            this.setEstadoComponentes();
                        }
                    });
            }
        }
        ,

        borrarCampanyaEstado: function () {
            var store = this.getStoreTiposEstadoCampanyaStore();

            var ref = this;
            var campanyaEstado = store.findRecord('id', this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaEstados()));

            Ext.Msg.confirm('Eliminar', 'El registre s\'eliminarà. Estàs segur de voler continuar?', function (btn) {
                if (btn == 'yes') {
                    store.remove(campanyaEstado);
                    store.sync({
                        callback: function () {
                            ref.setEstadoComponentes();
                        }
                    });
                }
            });
        },

////////////////////////////////////// ENVIOS PROGRAMADOS //////////////////////////////////////////////////////////////////////////////////////

        anyadirEnvioCampanyaProgramado: function () {

            var store = this.getStoreCampanyasEnviosProgramadosStore();

            var rec = this.getCampanyaEnvioProgramadoModel().create(
                {
                    envioId: this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaEnviosAuto())
                });

            var rowEditor = this.getGridEnviosCampanyaProgramados().getPlugin('editingEnvioProgramado');

            rowEditor.cancelEdit();
            store.insert(0, rec);
            rowEditor.startEdit(0, 0);

        },

        sincronizarEnvioCampanyaProgramado: function () {
            var store = this.getStoreCampanyasEnviosProgramadosStore();
            store.sync({
                //callback: function () {
                //    store.reload();
                //}
            });
        },
        borrarEnvioCampanyaProgramado: function () {
            var store = this.getStoreCampanyasEnviosProgramadosStore();

            var envioProgramado = store.findRecord('id', this.getController("ControllerVarios").dameIdGrid(this.getGridEnviosCampanyaProgramados()));

            Ext.Msg.confirm('Eliminar', 'El registre s\'eliminarà. Estàs segur de voler continuar?', function (btn) {
                if (btn == 'yes') {
                    store.remove(envioProgramado);
                    store.sync();
                }
            });
        },

////////////////////////////////////// CAMPANYA ESTADOS MOTIVOS ////////////////////////////////////////////////////////////////////////////////////////

        anyadirCampanyaEstadoMotivo: function () {

            var store = this.getStoreTiposEstadoCampanyaMotivosStore();

            var rec = this.getTipoEstadoCampanyaMotivoModel().create(
                {
                    tipoEstadoCampanyaId: this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaEstados())
                });

            var rowEditor = this.getGridCampanyaEstadoMotivos().getPlugin('editingCampanyaEstadoMotivo');

            rowEditor.cancelEdit();
            store.insert(0, rec);
            rowEditor.startEdit(0, 0);

        },

        sincronizarCampanyaEstadoMotivo: function () {
            var store = this.getStoreTiposEstadoCampanyaMotivosStore();
            store.sync({
                callback: function () {
                    store.reload();
                }
            });
        },

        borrarCampanyaEstadoMotivo: function () {
            var store = this.getStoreTiposEstadoCampanyaMotivosStore();

            var campanyaEstadoMotivo = store.findRecord('id', this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaEstadoMotivos()));

            Ext.Msg.confirm('Eliminar', 'El registre s\'eliminarà. Estàs segur de voler continuar?', function (btn) {
                if (btn == 'yes') {
                    store.remove(campanyaEstadoMotivo);
                    store.sync();
                }
            });
        },

//////////////////////////  ITEMS  ////////////////////////////////////////////////////////////////
        borrarCampanyaItem: function () {
            var ref = this;
            Ext.Msg.confirm('Eliminació de items', '<b>Esteu segur/a de voler esborrar el item de la campanya sel·leccionada?</b>', function (btn) {
                if (btn == 'yes') {
                    var campanyaItem = ref.getGridCampanyaItems().getSelectionModel().getSelection();
                    if (campanyaItem.length == 1) {
                        var registro = campanyaItem[0];
                        var store = ref.getStoreCampanyaItemsStore();
                        store.remove(registro);
                        store.sync();
                    }
                }
            });
        },

        anyadirCampanyaItems: function () {
            var store = this.getStoreCampanyaItemsStore();

            var campanyaId = this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyas());
            var seleccionItems = this.getGridItems().getSelectionModel().getSelection();

            if (seleccionItems.length > 0) {
                var ids = [];

                for (var i = 0; i < seleccionItems.length; i++) {
                    ids.push(seleccionItems[i].getId());
                }

                Ext.Ajax.request(
                    {
                        url: '/crm/rest/campanyaitem/campanya/' + campanyaId,
                        method: 'POST',
                        params: {
                            items: ids
                        },
                        scope: this,
                        callback: function () {
                            store.reload();
                            this.cerrarVentanaCampanyaItems();
                        }
                    });
            } else {
                Ext.Msg.alert('Afegir Items', 'No hi ha items seleccionats.');
            }
        }
        ,

////////////////////////// VENTANA ITEMS /////////////////////////////////////////////////////////
        getVentanaCampanyaItemsView: function () {
            return this.getView('campanyas.VentanaCampanyaItems').create();
        }
        ,

        cerrarVentanaCampanyaItems: function () {
            this.ventanaCampanyaItems.destroy();
        }
        ,

        mostrarVentanaCampanyaItem: function () {
            this.ventanaCampanyaItems = this.getVentanaCampanyaItemsView();
            this.getStoreItemsVentanaCampanyasStore().loadData([], false);
            this.ventanaCampanyaItems.show();
        }
        ,

        onGridCampanyaItemsSelectionChange: function () {
            this.setEstadoComponentes();
        }
        ,

//////////////////////////////////////////  VENTANA ENVIOS  ////////////////////////////////////////////////////////////
        getVentanaCampanyaEnviosView: function () {
            return this.getView('campanyas.VentanaCampanyaEnvios').create();
        }
        ,

        cerrarVentanaCampanyaEnvios: function () {
            this.ventanaCampanyaEnvios.destroy();
        }
        ,

        mostrarVentanaCampanyaEnvios: function (grid, view, index, select) {
            this.ventanaCampanyaEnvios = this.getVentanaCampanyaEnviosView();
            this.getStoreCampanyasEnviosProgramadosStore().loadData([], false);
            var txt = this.getEditorVentanaCampanyaEnvios().down('[name=cuerpo]');

            if (txt != null) {
                txt.destroy();
            }

            var config = {
                //width: 700,
                height: 400,
                name: 'cuerpo',
                fieldLabel: 'Corp'
            };
            var editorNuevo = new Ext.ux.CKEditor(config);
            this.getEditorVentanaCampanyaEnvios().add(editorNuevo);

            var form = this.getVentanaCampanyaEnvios();
            if (select) {
                form.down("combo[name=tipoEnvioAuto]").setValue(select.data.campanyaEnvioTipoId);
                form.down("textfield[name=asuntoEnvioAuto]").setValue(select.data.asunto);
                form.down("textfield[name=desde]").setValue(select.data.desde);
                form.down("textfield[name=responder]").setValue(select.data.responder);
                form.down("combobox[name=tipoCorreoEnvio]").setValue(select.data.tipoCorreoEnvio);
                form.down("textfield[name=base]").setValue(select.data.base);

                //form.down("htmleditor[name=cuerpoEnvioAuto]").setValue(select.data.cuerpo);
                editorNuevo.setValue(select.data.cuerpo);
                //editorNuevo.setUrl(select.data.id);

                form.down("button[name=actualizar]").setVisible(true);
                form.down("button[name=anyadir]").setVisible(false);

                this.getStoreCampanyasEnviosProgramadosStore().load(
                    {
                        params: {
                            envioId: select.data.id
                        },
                        scope: this,
                        callback: function () {
                            this.ventanaCampanyaEnvios.show();
                        }
                    }
                );
            } else {
                form.down("button[name=actualizar]").setVisible(false);
                form.down("button[name=anyadir]").setVisible(true);
                this.getStoreTiposCampanyaEnviosAutoStore().loadData([], false);
                this.ventanaCampanyaEnvios.show();
            }
        },

//////////////////////////////////////////  VENTANA ENVIOS ADMIN ////////////////////////////////////////////////////////////
        getVentanaCampanyaEnviosAdminView: function () {
            return this.getView('campanyas.VentanaCampanyaEnviosAdmin').create();
        }
        ,

        cerrarVentanaCampanyaEnviosAdmin: function () {
            this.ventanaCampanyaEnviosAdmin.destroy();
        }
        ,

        mostrarVentanaCampanyaEnviosAdmin: function (grid, view, index, select) {
            this.ventanaCampanyaEnviosAdmin = this.getVentanaCampanyaEnviosAdminView();
            var form = this.getVentanaCampanyaEnviosAdmin();

            if (select) {
                form.down("combo[name=tipoEnvioAuto]").setValue(select.data.campanyaEnvioTipoId);
                form.down("textfield[name=asuntoEnvioAuto]").setValue(select.data.asunto);
                form.down("textfield[name=emails]").setValue(select.data.emails);
                form.down("htmleditor[name=cuerpoEnvioAuto]").setValue(select.data.cuerpo);
                form.down("button[name=actualizar]").setVisible(true);
                form.down("button[name=anyadir]").setVisible(false);
            } else {
                form.down("button[name=actualizar]").setVisible(false);
                form.down("button[name=anyadir]").setVisible(true);
                this.getStoreTiposCampanyaEnviosAutoStore().loadData([], false);

            }
            this.ventanaCampanyaEnviosAdmin.show();
        },

////////////////////////////////////////// VENTANA ACTOS CAMPAÑA //////////////////////////////////////////////////////////


        getVentanaCampanyaActosView: function () {
            return this.getView('campanyas.VentanaCampanyaActos').create();
        },

        mostrarVentanaCampanyaActos: function () {

            this.ventanaCampanyaActos = this.getVentanaCampanyaActosView();
            this.getBotonActualizarActoCampanya().setVisible(false);
            this.getBotonAnyadirActoCampanya().setVisible(true);
            this.ventanaCampanyaActos.show();
        },

        mostrarVentanaActualizarCampanyaActo: function (obj, record) {

            this.ventanaCampanyaActos = this.getVentanaCampanyaActosView();

            var form = this.getFormActoCampanya();

            form.down("textfield[name=titulo]").setValue(record.data.titulo);
            form.down("textarea[name=resumen]").setValue(record.data.resumen);
            form.down("textfield[name=duracion]").setValue(record.data.duracion);
            form.down("textfield[name=contenido]").setValue(record.data.contenido);
            form.down("datefield[name=fecha]").setValue(record.data.fecha);
            form.down("textfield[name=hora]").setValue(record.data.hora);
            form.down("textfield[name=horaApertura]").setValue(record.data.horaApertura);
            form.down("checkbox[name=rssActivo]").setValue(record.data.rssActivo);
            form.down("filefield[name=imagen]").setRawValue(record.data.imagenNombre);

            this.getBotonActualizarActoCampanya().setVisible(true);
            this.getBotonAnyadirActoCampanya().setVisible(false);
            this.ventanaCampanyaActos.show();
        },

        cerrarVentanaCampanyaActos: function () {
            this.ventanaCampanyaActos.destroy();
        },

        onGridCampanyaActosSelectionChange: function () {
            this.setEstadoComponentes();
        },

//////////////////////////////////////////////////////////////////////////////////////////////

        //onGridCampanyaDatosSelectionChange: function () {
        //    this.setEstadoComponentes();
        //},

        modificarFaseId: function (editor, e) {
            var fila = editor.grid.getSelectionModel().getSelection();

            if (fila[0].data.faseNombre != e.newValues.faseNombre) {
                fila[0].data.faseId = e.newValues.faseNombre;
            }

        },

/////////////////////////////// CARGA STORES /////////////////////////////////////////////////////

        cargaStoreItemsCampanya: function (campanyaId) {
            var storeCampanyaItems = this.getStoreCampanyaItemsStore();
            storeCampanyaItems.load(
                {
                    params: {
                        campanyaId: campanyaId
                    },
                    scope: this,
                    callback: function () {
                        this.setEstadoComponentes();
                    }
                });
        },

        cargaStoreActos: function (campanyaId) {
            var storeCampanyaActos = this.getStoreCampanyaActosStore();
            storeCampanyaActos.load(
                {
                    params: {
                        campanyaId: campanyaId
                    },
                    scope: this,
                    callback: function () {
                        this.setEstadoComponentes();
                    }
                });
        },

        cargaStoreFases: function (campanyaId) {
            var store = this.getStoreCampanyaFasesStore();
            store.load(
                {
                    url: '/crm/rest/campanyafase',
                    params: {
                        campanyaId: campanyaId
                    },
                    scope: this,
                    callback: function () {
                        this.setEstadoComponentes();
                    }
                });
        },

        cargaStoreCampanyaEnvios: function (campanyaId) {
            var storeCampanyaEnvios = this.getStoreCampanyaEnviosStore();

            storeCampanyaEnvios.load(
                {
                    params: {
                        campanyaId: campanyaId
                    },
                    scope: this,
                    callback: function () {
                        this.setEstadoComponentes();
                    }
                });
        },

        cargaStoreCampanyaEnviosAdmin: function (campanyaId) {
            var storeCampanyaEnviosAdmin = this.getStoreCampanyaEnviosAdminStore();

            storeCampanyaEnviosAdmin.load(
                {
                    params: {
                        campanyaId: campanyaId
                    },
                    scope: this,
                    callback: function () {
                        this.setEstadoComponentes();
                    }
                });
        },

        // cargaStoreCampanyaCartas: function (campanyaId) {
        //     var storeCampanyaCartas = this.getStoreCampanyaCartasStore();
        //
        //     storeCampanyaCartas.load(
        //         {
        //             params: {
        //                 campanyaId: campanyaId
        //             },
        //             scope: this,
        //             callback: function () {
        //                 this.setEstadoComponentes();
        //             }
        //         });
        // },

        cargaStoreCampanyasCampanyasDestino: function (campanyaId) {
            var storeCampanyaCampanyasDestino = this.getStoreCampanyasCampanyasDestinoStore();

            storeCampanyaCampanyasDestino.load(
                {
                    url: '/crm/rest/campanyacampanya/campanya/destino',
                    params: {
                        campanyaId: campanyaId
                    },
                    scope: this,
                    callback: function () {
                        this.setEstadoComponentes();
                    }
                });
        },

        cargaStoreLineasFacturacionByCampanya: function (campanyaId) {
            var storeLineasFacturacionByCampanya = this.getStoreLineasFacturacionByCampanyaStore();

            storeLineasFacturacionByCampanya.load(
                {
                    url: '/crm/rest/lineafacturacion/campanya/',
                    params: {
                        campanyaId: campanyaId
                    },
                    scope: this,
                    callback: function () {
                        this.setEstadoComponentes();
                    }
                });
        },

        cargaStoreSubVinculos: function (programaId) {
            var storeSubVinculos = this.getStoreSubVinculosStore();

            storeSubVinculos.load({
                url: '/crm/rest/subvinculo/programa',
                params: {
                    programaId: programaId
                },
                scope: this,
                callback: function () {
                    this.setEstadoComponentes();
                }
            });
        },

        cargaStoreTiposEstadoCampanya: function (campanyaId) {
            var storeTiposEstadoCampanya = this.getStoreTiposEstadoCampanyaStore();

            storeTiposEstadoCampanya.load(
                {
                    url: '/crm/rest/tipoestadocampanya/campanya/',
                    params: {
                        campanyaId: campanyaId
                    },
                    scope: this,
                    callback: function () {
                        this.setEstadoComponentes();
                    }
                });
        },

        cargaStoreItems: function (grupoId) {
            var store = this.getStoreItemsVentanaCampanyasStore();
            store.load(
                {
                    params: {
                        grupoId: grupoId
                    },
                    scope: this
                });
        },

        onGrupoSelected: function (combo, records) {
            this.cargaStoreItems(records[0].get('id'));
        },

        setEstadoComponentes: function () {
            var tieneSeleccionGridCampanyas = this.getGridCampanyas().getSelectionModel().hasSelection();
            var leaf = this.getGridCampanyas().getSelectionModel().getSelection()[0].data.leaf;
            this.getTabCampanyas().setDisabled((tieneSeleccionGridCampanyas && !leaf) || !tieneSeleccionGridCampanyas);

            var tieneSeleccionGridCampanyaFases = this.getGridCampanyaFases().getSelectionModel().hasSelection();
            this.getBotonBorrarCampanyaFase().setDisabled(!tieneSeleccionGridCampanyaFases);

            var tieneSeleccionGridCampanyaActos = this.getGridCampanyaActos().getSelectionModel().hasSelection();
            this.getBotonBorrarCampanyaActo().setDisabled(!tieneSeleccionGridCampanyaActos);

            var tieneSeleccionGridCampanyaItems = this.getGridCampanyaItems().getSelectionModel().hasSelection();
            this.getBotonBorrarCampanyaItem().setDisabled(!tieneSeleccionGridCampanyaItems);

            var tieneSeleccionGridCampanyaEnvios = this.getGridCampanyaEnviosAuto().getSelectionModel().hasSelection();
            this.getBotonBorrarCampanyaEnvio().setDisabled(!tieneSeleccionGridCampanyaEnvios);
            this.getBotonEnviarPruebaEnvio().setDisabled(!tieneSeleccionGridCampanyaEnvios);

            var tieneSeleccionGridCampanyaEnviosAdmin = this.getGridCampanyaEnviosAdmin().getSelectionModel().hasSelection();
            this.getBotonBorrarCampanyaEnvioAdmin().setDisabled(!tieneSeleccionGridCampanyaEnviosAdmin);

            this.getController("ControllerPanelCampanyasCartas").setEstadoComponentes();

            var tieneSeleccionGridCampanyaCampanyaDestino = this.getGridCampanyasCampanyasDestino().getSelectionModel().hasSelection();
            this.getBotonBorrarCampanyasCampanyasDestino().setDisabled(!tieneSeleccionGridCampanyaCampanyaDestino);

            var tieneSeleccionGridCampanyaEstados = this.getGridCampanyaEstados().getSelectionModel().hasSelection();
            // var campanyaEstadoMotivos = this.getGridCampanyaEstadoMotivos().store.getCount();
            this.getBotonBorrarCampanyaEstados().setDisabled(!tieneSeleccionGridCampanyaEstados);
            this.getBotonAnyadirCampanyaEstadoMotivo().setDisabled(!tieneSeleccionGridCampanyaEstados);

            var tieneSeleccionGridCampanyaEstadoMotivos = this.getGridCampanyaEstadoMotivos().getSelectionModel().hasSelection();
            this.getBotonBorrarCampanyaEstadoMotivo().setDisabled(!tieneSeleccionGridCampanyaEstadoMotivos);

            var campanyaFasesCount = this.getGridCampanyaFases().store.getCount();
            //var campanyaFasesDatos = this.getGridCampanyaDatos().store.getCount();
            var campanyaFasesItems = this.getGridCampanyaItems().store.getCount();
            var campanyaEnvios = this.getGridCampanyaEnviosAuto().store.getCount();
            var campanyaEnviosAdmin = this.getGridCampanyaEnviosAdmin().store.getCount();
            var campanyaCartas = this.getController("ControllerPanelCampanyasCartas").getGridCampanyaCartas().store.getCount();
            var campanyasCampanyasDestino = this.getGridCampanyasCampanyasDestino().store.getCount();
            var campanyaEstados = this.getGridCampanyaEstados().store.getCount();

            this.getBotonBorrarCampanya().setDisabled(
                campanyaFasesCount || campanyaFasesItems || campanyaEnvios || campanyaCartas || campanyasCampanyasDestino
                || !tieneSeleccionGridCampanyas || campanyaEnviosAdmin || campanyaEstados);

            var serviciosUsuario = this.getStoreServiciosByUsuarioIdStore().getCount();
            this.getBotonAnyadirCampanya().setDisabled(serviciosUsuario === 0);
        },

        setEstadoComponentesVentanaEnvio: function () {
            var tieneSeleccionGridCampanyaEnviosProgramados = this.getGridEnviosCampanyaProgramados().getSelectionModel().hasSelection();
            this.getBotonBorrarEnvioProgramado().setDisabled(!tieneSeleccionGridCampanyaEnviosProgramados);
        },

        dameIdCampanya: function () {
            return this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyas());
        }

    })
;