Ext.define('CRM.controller.ControllerPanelListados',
    {
        extend: 'Ext.app.Controller',
        stores: ['StoreEnviosNoValidos'],
        views: ['listados.enviosNoValidos.GridEnviosNoValidos', 'listados.enviosNoValidos.FiltroEnviosNoValidos'],
        dirty: false,

        refs: [
            {
                selector: 'filtroEnviosNoValidos',
                ref: 'filtroEnviosNoValidos'
            }
        ],

        init: function () {

            this.control(
                {
                    'gridEnviosNoValidos': {
                        edit: this.sincronizarEnviosNoValidos
                    },
                    'gridEnviosNoValidos actioncolumn[name=abrirCliente]': {
                        click: function (obj, r, it, i, e, rec) {
                            this.abrirCliente(obj, rec);
                        }
                    },
                    // 'filtroEnviosNoValidos button[name=botonBusqueda]': {
                    //     click: function () {
                    //         this.busqueda();
                    //     }
                    // },

                    'filtroEnviosNoValidos button[name=anyadirFiltroFecha]': {
                        click: this.anyadirFiltroFecha
                    }
                });

        },

        anyadirFiltroFecha: function () {

            var array = this.getFiltroEnviosNoValidos().arrayBusqueda;

            array.push(this.getFiltroEnviosNoValidos().busqueda());
            var text = "";
            for (var i = 0; i < array.length; i++) {
                text = text + this.getFiltroEnviosNoValidos().toString(array[i])+ "\n";
            }
            this.getFiltroEnviosNoValidos().down('textarea[name=textoFiltros]').setValue(text);

            //this.getFiltroEnviosNoValidos().limpiarCampos();

        },

        abrirCliente: function (objeto, record) {
            // this.getController("ControllerPanelClientes").abrirVentanaBusquedaCliente();
            this.getController("ControllerPanelClientes").busquedaPorMail(record.data.nombre, objeto);
        }
        ,

        sincronizarEnviosNoValidos: function () {
            var store = this.getStoreEnviosNoValidosStore();
            store.sync({
                callback: function () {
                    store.load();
                }
            });
        }
        ,
        //
        // busqueda: function () {
        //
        //     var obj = {
        //         arrayBusqueda: Ext.encode(this.getFiltroEnviosNoValidos().arrayBusqueda),
        //         modificado: this.getFiltroEnviosNoValidos().down("radiogroup[name=radioModificadoGroup]").getValue(),
        //         operador: this.getFiltroEnviosNoValidos().down("combobox[name=comboOperadorLogicoModificado]").getValue()
        //     };
        //
        //     if (obj != null) {
        //         var envios = this.getStoreEnviosNoValidosStore();
        //
        //         envios.getProxy().extraParams = obj;
        //         envios.load({
        //             params: {
        //                 start: 0,
        //                 page: 0
        //             }
        //         });
        //         envios.currentPage = 1;
        //     }
        //     //else {
        //     //    Ext.Msg.show(
        //     //        {
        //     //            title: 'Informaciò',
        //     //            msg: 'Has d\'introduir almenys ' + MIN_CARACTERES_BUSQUEDA + ' caràcters',
        //     //            width: 300,
        //     //            buttons: Ext.MessageBox.OK,
        //     //            icon: Ext.MessageBox.ERROR
        //     //        });
        //     //}
        // }
//,
    })
;