Ext.define('CRM.controller.ControllerPrincipal',
{
    extend : 'Ext.app.Controller',
    refs : [
    {
        selector : 'applicationViewportCrm',
        ref : 'viewportCrm'

    } ],
    init : function()
    {
        this.control(
        {
            'viewport > treepanel' :
            {
                itemclick : this.navigationTreeItemClick
            }
        });
    },

    navigationTreeItemClick : function(view, node)
    {
        view.up("viewport").addNewTab(node.data.id);
    }

});