Ext.define('CRM.controller.ControllerPanelItems',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreItems', 'StoreTiposAcceso', 'StoreTiposRecepcionInfo', 'StoreItemsItemsDestino', 'StoreVentanaItems', 'StoreVentanaGrupos', 'StoreItemsItemsOrigen' ],
    models : [ 'Item', 'ItemItem' ],
    views : [ 'items.GridItems', 'grupos.PanelGrupos', 'items.GridItemsItemsDestino', 'items.VentanaItemsItems', 'items.GridVentanaItems', 'items.VentanaItems', 'items.GridItemsItemsOrigen' ],

    ventanaItemsItems : {},
    ventanaItems : null,

    getVentanaItemsItems : function()
    {
        return this.getView('items.VentanaItemsItems').create();
    },

    refs : [
    {
        selector : 'gridGrupos',
        ref : 'gridGrupos'
    },
    {
        selector : 'gridItems',
        ref : 'gridItems'
    },
    {
        selector : 'gridItemsItemsDestino',
        ref : 'gridItemsItemsDestino'
    },
    {
        selector : 'gridItemsItemsOrigen',
        ref : 'gridItemsItemsOrigen'
    },
    {
        selector : 'ventanaItems [name=formItem]',
        ref : 'formItems'
    },
    {
        selector : 'gridItems button[name=borrarItem]',
        ref : 'botonBorrarItem'
    },
    {
        selector : 'panelGrupos [name=tabItems]',
        ref : 'tabItems'
    },
    {
        selector : 'gridVentanaItems',
        ref : 'gridVentanaItems'
    },
    {
        selector : 'ventanaItemsItems button[action=anyadirOrigen]',
        ref : 'buttonAnyadirOrigen'
    },
    {
        selector : 'ventanaItemsItems button[action=anyadirDestino]',
        ref : 'buttonAnyadirDestino'
    } ],

    dirty : false,

    init : function()
    {
        this.control(
        {
            'panelGrupos' :
            {
                render : function()
                {
                    if (this.ventanaItems === null)
                    {
                        this.ventanaItems = this.getView('items.VentanaItems').create();
                    }
                }
            },
            'gridItems button[name=anyadirItem]' :
            {
                click : this.anyadirItem
            },
            'gridItems button[name=editarItem]' :
            {
                click : this.anyadirItem
            },
            'gridItems button[name=borrarItem]' :
            {
                click : this.borrarItem
            },
            'gridItems' :
            {
                select : this.onItemSelected,
                itemdblclick : this.mostrarVentanaItems
            },
            'ventanaItems button[name=guardarItem]' :
            {
                click : this.guardarItem
            },
            'gridItemsItemsDestino button[name=anyadirItemItemDestino]' :
            {
                click : function()
                {
                    this.showVentanaItemsItems('DEST');
                }
            },
            'gridItemsItemsDestino button[name=borrarItemItemDestino]' :
            {
                click : function()
                {
                    this.borrarItemItem('DEST');
                }
            },
            'gridItemsItemsOrigen button[name=borrarItemItemOrigen]' :
            {
                click : function()
                {
                    this.borrarItemItem('ORI');
                }
            },
            'gridItemsItemsOrigen button[name=anyadirItemItemOrigen]' :
            {
                click : function()
                {
                    this.showVentanaItemsItems('ORI');
                }
            },
            'ventanaItemsItems combobox[name=filtroGrupos]' :
            {
                select : this.cargaStoreItems
            },
            'ventanaItemsItems button[action=cancelar]' :
            {
                click : this.cerrarVentanaItemsItems
            },
            'ventanaItemsItems button[action=anyadirDestino]' :
            {
                click : function()
                {
                    this.anyadirItemsItems('DEST');
                }
            },
            'ventanaItemsItems button[action=anyadirOrigen]' :
            {
                click : function()
                {
                    this.anyadirItemsItems('ORI');
                }
            }
        });
    },

    cargaStoreItems : function(combo, records)
    {
        var store = this.getStoreVentanaItemsStore();
        store.load(
        {
            url : '/crm/rest/item/',
            params :
            {
                grupoId : records[0].get('id')
            }
        });
    },

    showVentanaItemsItems : function(origen)
    {
        this.ventanaItemsItems = this.getVentanaItemsItems();
        this.getStoreVentanaGruposStore().load(
        {
            url : '/crm/rest/grupo/'
        });
        this.getStoreVentanaItemsStore().loadData([], false);
        if (origen == 'ORI')
        {
            this.getButtonAnyadirOrigen().setVisible(true);
            this.getButtonAnyadirDestino().setVisible(false);
        }
        else
        {
            this.getButtonAnyadirOrigen().setVisible(false);
            this.getButtonAnyadirDestino().setVisible(true);
        }
        this.ventanaItemsItems.show();
    },

    cerrarVentanaItemsItems : function()
    {
        this.ventanaItemsItems.destroy();
    },

    anyadirItemsItems : function(origen)
    {
        var itemId = this.getController("ControllerVarios").dameIdGrid(this.getGridItems());
        var seleccionItems = this.getGridVentanaItems().getSelectionModel().getSelection();
        var store, item, i, rec;
        if (origen == 'ORI')
        {
            store = this.getStoreItemsItemsOrigenStore();
            for (i = 0; i < seleccionItems.length; i++)
            {
                item = seleccionItems[i];
                rec = this.getItemItemModel().create(
                {
                    itemIdDestino : itemId,
                    itemIdOrigen : item.getId()
                });
                store.add(rec);
            }

        }
        else
        {
            store = this.getStoreItemsItemsDestinoStore();
            for (i = 0; i < seleccionItems.length; i++)
            {
                item = seleccionItems[i];
                rec = this.getItemItemModel().create(
                {
                    itemIdDestino : item.getId(),
                    itemIdOrigen : itemId
                });
                store.add(rec);
            }
        }

        store.sync(
        {
            scope : this,
            success : function()
            {
                store.reload();
                this.cerrarVentanaItemsItems();
            },
            failure : function()
            {
                Ext.Msg.alert('Afegir item', '<b>El item no ha sigut afegit</b>');
                this.cerrarVentanaItemsItems();
            }
        });
    },

    borrarItemItem : function(origen)
    {
        var ref = this;
        var itemItemId, itemItem, store;

        if (origen == 'ORI')
        {
            itemItemId = this.getController("ControllerVarios").dameIdGrid(this.getGridItemsItemsOrigen());
            itemItem = this.getGridItemsItemsOrigen().getSelectionModel().getSelection();
            store = ref.getStoreItemsItemsOrigenStore();

        }
        else
        {
            itemItemId = this.getController("ControllerVarios").dameIdGrid(this.getGridItemsItemsDestino());
            itemItem = this.getGridItemsItemsDestino().getSelectionModel().getSelection();
            store = ref.getStoreItemsItemsDestinoStore();
        }
        if (itemItemId)
        {
            Ext.Msg.confirm('Eliminació de items', '<b>Esteu segur/a de voler esborrar el item sel·leccionat?</b>', function(btn)
            {
                if (btn == 'yes')
                {
                    store.remove(itemItem);
                    store.sync(
                    {
                        failure : function()
                        {
                            store.rejectChanges();
                            Ext.Msg.alert('Items', '<b>No s\'ha potgut eliminar el item</b>');
                        }
                    });

                }
            });
        }
    },

    onItemSelected : function(panel, record)
    {
        this.getFormItems().loadRecord(record);
        this.getStoreItemsItemsDestinoStore().loadData([], false);
        this.getStoreItemsItemsOrigenStore().loadData([], false);
        this.getStoreItemsItemsDestinoStore().load(
        {
            url : '/crm/rest/itemitem/item/' + record.data.id
        });
        this.getStoreItemsItemsOrigenStore().load(
        {
            url : '/crm/rest/itemitem/item/origen/' + record.data.id
        });
        this.setEstadoComponentes();
    },

    anyadirItem : function()
    {
        var item = this.getItemModel().create(
        {
            grupoId : this.getGrupoSeleccionado().getId(),
            correoId : CRM.model.Item.RECEPCION_MODIFICABLE_Y_POR_DEFECTO,
            smsId : CRM.model.Item.RECEPCION_NO_ACTIVO,
            aplicacionMovilId : CRM.model.Item.RECEPCION_MODIFICABLE_Y_POR_DEFECTO,
            correoPostalId : CRM.model.Item.RECEPCION_MODIFICABLE_Y_POR_DEFECTO
        });
        this.getFormItems().loadRecord(item);

        this.getStoreItemsItemsDestinoStore().loadData([], false);
        this.getStoreItemsItemsOrigenStore().loadData([], false);

        this.ventanaItems.show();
        this.getTabItems().setDisabled(false);
        this.getGridItems().getSelectionModel().deselectAll();
    },

    borrarItem : function()
    {
        var ref = this;
        var store = this.getStoreItemsStore();
        var selection = this.getGridItems().getSelectionModel().getSelection();
        var itemId = (selection.length > 0) ? selection[0].data.id : null;
        var item = store.findRecord('id', itemId);

        Ext.Msg.confirm('Eliminar el item', 'Totes les dades d\'aquest item s\'esborraràn. Estàs segur de voler continuar?', function(btn)
        {
            if (btn == 'yes')
            {
                store.remove(item);

                store.sync(
                {
                    success : function()
                    {
                        ref.setEstadoComponentes();
                        ref.getFormItems().getForm().reset();
                    },
                    failure : function()
                    {
                        store.rejectChanges();
                        ref.setEstadoComponentes();
                        Ext.Msg.alert('Error', 'No s\'ha pogut esborrar el item perque té items asignats.');
                    }
                });
            }
        });
    },

    guardarItem : function()
    {
        var form = this.getFormItems().getForm();

        if (form.isValid())
        {
            var item;
            var store = this.getStoreItemsStore();
            var itemId = form.findField('id').getValue();
            var ref = this;

            if (itemId === '')
            {
                item = this.getItemModel().create();
                store.add(item);
            }
            else
            {
                item = store.findRecord('id', itemId);
            }

            item.set(form.getValues(false, false, false, true));

            store.sync(
            {
                success : function()
                {
                    store.reload();
                    ref.ventanaItems.hide();
                    ref.setEstadoComponentes();
                }
            });
        }
    },

    getGrupoSeleccionado : function()
    {
        var grid = this.getGridGrupos();
        if (!grid.getSelectionModel().hasSelection())
        {
            return null;
        }

        return grid.getSelectionModel().getSelection()[0];
    },

    mostrarVentanaItems : function()
    {
        this.ventanaItems.show();
    },

    setEstadoComponentes : function()
    {
        var tieneSeleccionGridItems = this.getGridItems().getSelectionModel().hasSelection();
        this.getTabItems().setDisabled(!tieneSeleccionGridItems);
        this.getBotonBorrarItem().setDisabled(!tieneSeleccionGridItems);
    }

});