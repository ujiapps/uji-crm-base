Ext.define('CRM.controller.ControllerVarios',
{
    extend : 'Ext.app.Controller',

    dirty : false,

    sacaHoraCompleta : function(fecha)
    {
        return (fecha == null) ? null : this.muestraHoras(fecha) + ":" + this.muestraMinutos(fecha);
    },

    muestraHoras : function(fecha)
    {
        return Ext.String.leftPad(fecha.getHours(), 2, '0');
    },

    muestraMinutos : function(fecha)
    {
        return Ext.String.leftPad(fecha.getMinutes(), 2, '0');
    },

    estaCompleto : function(nodo)
    {
        for (i in nodo.childNodes)
        {
            if (nodo.childNodes[i].data.cls == 'tree-vacio' || nodo.childNodes[i].data.cls == 'tree-mitad' || nodo.childNodes[i].data.cls == '')
            {
                return false;
            }
        }
        return true;
    },

    dameIdGrid : function(grid)
    {
        if(grid) {
            var item = grid.getSelectionModel().getSelection();
            return (item.length == 1) ? item[0].data.id : 0;
        }
    },

    estaVacio : function(nodo)
    {
        for (i in nodo.childNodes)
        {
            if (nodo.childNodes[i].data.cls == 'tree-completo' || nodo.childNodes[i].data.cls == 'tree-mitad')
            {
                return false;
            }
        }
        return true;
    },

    rellenaChecksArbol : function(nodo)
    {
        if (!nodo.isLeaf())
        {
            for (i in nodo.childNodes)
                this.rellenaChecksArbol(nodo.childNodes[i]);
        }

        if (nodo.isLeaf())
        {
            if (nodo.data.checked == true)
                nodo.set('cls', 'tree-completo');
            else
                nodo.set('cls', 'tree-vacio');
        }
        else
        {
            if (this.estaCompleto(nodo))
                nodo.set('cls', 'tree-completo');
            else
            {
                if (this.estaVacio(nodo))
                    nodo.set('cls', 'tree-vacio');
                else
                    nodo.set('cls', 'tree-mitad');
            }
        }
    },

    cambiaCheck : function(node, checked)
    {
        if (checked)
            node.set('cls', 'tree-completo');
        else
            node.set('cls', 'tree-vacio');

        node.cascadeBy(function(child)
        {
            child.set("checked", checked);
            if (checked)
                child.set('cls', 'tree-completo');
            else
                child.set('cls', 'tree-vacio');
        });

        node.bubble(function(parent)
        {
            if (parent != null)
            {
                if (checked)
                {
                    parent.set("checked", checked);

                    if (this.estaCompleto(parent))
                        parent.set('cls', 'tree-completo');
                    else
                    {
                        if (this.estaVacio(parent))
                            parent.set('cls', 'tree-vacio');
                        else
                            parent.set('cls', 'tree-mitad');
                    }
                }
                else
                {
                    if (parent.findChild("checked", true) != null)
                    {
                        parent.set('checked', true);
                        if (this.estaCompleto(parent))
                            parent.set('cls', 'tree-completo');
                        else
                        {
                            if (this.estaVacio(parent))
                                parent.set('cls', 'tree-vacio');
                            else
                                parent.set('cls', 'tree-mitad');
                        }
                    }
                    else
                    {
                        parent.set('checked', false);
                        parent.set('cls', 'tree-vacio');
                    }
                }
            }
        }, this)

    },

    limpiaCheck : function(node)
    {
        node.cascadeBy(function(child)
        {
            child.set("checked", false);
            child.set('cls', 'tree-vacio');
        });
    },

    creaArrayItems : function(arrayChecked)
    {
        var arrayItems = [];
        var i;
        for (i in arrayChecked)
        {
            if (arrayChecked[i].isLeaf())
            {
                arrayItems.push(arrayChecked[i].data.id);
            }
        }

        return arrayItems;
    }

});