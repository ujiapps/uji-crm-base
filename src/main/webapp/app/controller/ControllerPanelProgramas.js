Ext.define('CRM.controller.ControllerPanelProgramas',
    {
        extend: 'Ext.app.Controller',
        stores: ['StoreServiciosByUsuarioId', 'StoreProgramaUsuario', 'StoreProgramas', 'StoreTiposProgramasUsuarios', 'StoreProgramaPerfiles', 'StorePerfiles', 'StoreTipoAccesosProgramaPerfil',
            'StoreProgramaCursos', 'StoreUbicaciones'],
        models: ['Programa', 'ProgramaUsuario', 'ProgramaPerfil', 'ProgramaCurso'],
        views: ['programas.VentanaNewPersona', 'programas.GridProgramas', 'programas.GridPersonas', 'programas.PanelProgramas', 'programas.GridProgramaCursos', 'programas.FormProgramas'],
        ventanaProgramasPersona: {},

        getVentanaNewProgramasPersonaView: function () {
            return this.getView('programas.VentanaNewPersona').create();
        },

        refs: [
            {
                selector: 'gridProgramas',
                ref: 'gridProgramas'
            },
            {
                selector: 'gridProgramas button[name=borrarPrograma]',
                ref: 'botonBorrarPrograma'
            },
            {
                selector: 'ventanaNewProgramaPersona',
                ref: 'ventanaNewProgramaPersona'
            },
            {
                selector: 'ventanaNewProgramaPersona combo[name=comboProgramaPersonas]',
                ref: 'comboProgramaPersonas'
            },
            {
                selector: 'ventanaNewProgramaPersona form[name=formNewProgramaPersona]',
                ref: 'formNewProgramaPersona'
            },
            {
                selector: 'gridProgramasPersonas',
                ref: 'gridProgramasPersonas'
            },
            {
                selector: 'gridProgramasPersonas button[name=borrarProgramasPersona]',
                ref: 'botonBorrarPersona'
            },
            {
                selector: 'gridProgramaPerfiles',
                ref: 'gridProgramaPerfiles'
            },
            {
                selector: 'gridProgramaPerfiles button[name=borrar]',
                ref: 'botonBorrarProgramaPerfil'
            },
            {
                selector: 'gridProgramaCursos',
                ref: 'gridProgramaCursos'
            },
            {
                selector: 'gridProgramaCursos button[name=borrarUbicacion]',
                ref: 'botonBorrarProgramaCursos'
            },
            {
                selector: 'tabProgramas',
                ref: 'tabProgramas'
            },
            {
                selector: 'panelProgramas',
                ref: 'panelProgramas'
            },
            {
                selector: 'formularioProgramas',
                ref: 'formularioProgramas'
            }],

        dirty: false,
        init: function () {
            this.control(
                {
                    'panelProgramas': {
                        render: function () {
                            this.getStoreProgramasStore().load();
                        }
                    },
                    'gridProgramas': {
                        selectionchange: this.onGridProgramaSelectionChange,
                        validateedit: this.modificarStoreProgramas
                    },
                    'gridProgramas button[name=borrarPrograma]': {
                        click: this.borrarPrograma
                    },
                    'gridProgramas button[name=anyadirPrograma]': {
                        click: this.anyadirPrograma
                    },
                    'gridProgramasPersonas': {
                        select: this.setEstadoComponentes,
                        edit: this.modificarProgramaUsuario
                    },
                    'gridProgramasPersonas button[name=borrarProgramasPersona]': {
                        click: this.borrarProgramaUsuario
                    },
                    'gridProgramasPersonas button[name=anyadirProgramasPersona]': {
                        click: this.showVentanaAddProgramasPersona
                    },
                    'gridProgramaPerfiles': {
                        select: this.setEstadoComponentes,
                        validateedit: this.actualizarIdsFilaGridProgramaPerfiles,
                        edit: this.sincronizarProgramaPerfiles
                    },
                    'gridProgramaPerfiles button[name=anyadir]': {
                        click: this.anyadirProgramaPerfil
                    },
                    'gridProgramaPerfiles button[name=borrar]': {
                        click: this.borrarProgramaPerfil
                    },
                    'gridProgramaCursos': {
                        selectionchange: function () {
                            this.setEstadoComponentes();
                        },
                        validateedit: this.actualizarIdsFilaGridProgramaCursos,
                        edit: this.sincronizarProgramaCursos
                    },
                    'gridProgramaCursos button[name=borrarUbicacion]': {
                        click: this.borrarProgramaCursos
                    },
                    'gridProgramaCursos button[name=anyadirUbicacion]': {
                        click: this.anyadirProgramaCursos
                    },
                    'ventanaNewProgramaPersona button[action=cancelar]': {
                        click: this.closeVentanaAddProgramasPersona
                    },
                    'ventanaNewProgramaPersona button[action=guardar-persona]': {
                        click: this.anyadirProgramaUsuario
                    }
                });
        },

        //-------------------FUNCIONES VENTANA PROGRAMA-PERSONA---------------------------------------------

        showVentanaAddProgramasPersona: function () {
            this.ventanaProgramasPersona = this.getVentanaNewProgramasPersonaView();
            this.ventanaProgramasPersona.show();
        },

        closeVentanaAddProgramasPersona: function () {
            this.ventanaProgramasPersona.destroy();
        },

        //------------------FUNCIONES PROGRAMA----------------------------------------------------------------

        anyadirPrograma: function () {
            var storeServicios = this.getStoreServiciosByUsuarioIdStore();
            storeServicios.reload(
                {
                    scope: this,
                    callback: function () {
                        if (storeServicios.getTotalCount() === 0) {
                            Ext.Msg.alert('Creació de programes', '<b>No pots afegir programes perque no tens access a ningú servei</b>');
                        }

                        else {
                            var store = this.getStoreProgramasStore();
                            var rec = this.getProgramaModel().create();
                            var rowEditor = this.getGridProgramas().getPlugin('editingPrograma');

                            rowEditor.cancelEdit();
                            store.insert(0, rec);
                            rowEditor.startEdit(0, 0);
                            this.setEstadoComponentes();
                        }
                    }
                });

        },

        borrarPrograma: function () {
            var ref = this;
            var store = ref.getStoreProgramasStore();
            var programa = this.getGridProgramas().getSelectionModel().getSelection();
            Ext.Msg.confirm('Eliminació de programes', '<b>Esteu segur/a de voler esborrar el programa sel·leccionat?</b>', function (btn) {
                if (btn == 'yes') {
                    store.remove(programa);
                    ref.setEstadoComponentes();
                }
            });
        },

        onGridProgramaSelectionChange: function () {
            var programaId = this.getController("ControllerVarios").dameIdGrid(this.getGridProgramas());
            if (programaId) {
                this.cargaStoreProgramaUsuario(programaId);
            }
            else {
                this.limpiarGridPersonas();
            }
        },

        //--------------FUNCIONES PROGRAMA-USUARIO-------------------------------------------------------------------

        anyadirProgramaUsuario: function () {
            var ref = this;
            var personaId = this.getComboProgramaPersonas().getValue();
            var programaId = this.getController("ControllerVarios").dameIdGrid(this.getGridProgramas());

            var persona = this.getProgramaUsuarioModel().create(
                {
                    personaPasPdiId: personaId,
                    programaId: programaId,
                    tipo: 'USER'
                });

            var store = this.getStoreProgramaUsuarioStore();
            store.add(persona);
            store.sync(
                {
                    success: function () {
                        ref.closeVentanaAddProgramasPersona();
                        ref.onGridProgramaSelectionChange();
                    },
                    failure: function () {
                        ref.closeVentanaAddProgramasPersona();
                        Ext.Msg.alert('Asociació de persones a programes', '<b>No s\'ha pogut afegir a la persona al programa seleccionat</b>');
                        store.remove(persona);
                    }
                });

        },

        borrarProgramaUsuario: function () {
            var ref = this;
            var store = ref.getStoreProgramaUsuarioStore();
            var programaUsuario = store.findRecord('id', this.getController("ControllerVarios").dameIdGrid(this.getGridProgramasPersonas()));

            if (programaUsuario) {
                Ext.Msg.confirm('Eliminació de personas', '<b>Esteu segur/a de voler esborrar les persones sel·leccionats?</b>', function (btn) {
                    if (btn == 'yes') {
                        store.remove(programaUsuario);
                        store.sync(
                            {
                                callback: function () {
                                    ref.onGridProgramaSelectionChange();
                                }
                            });

                    }
                });
            }
            else {
                Ext.Msg.alert('Asociació de persones a programes', '<b>No hi ha usuari seleccionat per a eliminar</b>');
            }
        },

        modificarProgramaUsuario: function (editor, e) {

            var storeProgramaUsuario = this.getStoreProgramaUsuarioStore();

            editor.cancelEdit();
            storeProgramaUsuario.update(e);
            editor.startEdit();

        },

        //---------------FUNCIONES PROGRAMA-PERFIL------------------------------------------------------------------

        anyadirProgramaPerfil: function () {
            var programaId = this.getController("ControllerVarios").dameIdGrid(this.getGridProgramas());

            this.getStorePerfilesStore().load(
                {
                    url: '/crm/rest/perfil/programa/faltan',
                    params: {
                        programaId: programaId
                    },
                    scope: this,
                    callback: function (records) {
                        if (records.length > 0) {
                            var store = this.getStoreProgramaPerfilesStore();

                            var rec = this.getProgramaPerfilModel().create(
                                {
                                    programaId: programaId
                                });
                            var rowEditor = this.getGridProgramaPerfiles().getPlugin('editingProgramaPerfil');

                            rowEditor.cancelEdit();
                            store.insert(0, rec);
                            rowEditor.startEdit(0, 0);
                        }
                        else {
                            Ext.Msg.alert('Asociació de perfils a programes', '<b>No es poden afegir mes perfils</b>');
                        }
                    }
                });

        },

        borrarProgramaPerfil: function () {
            var ref = this;
            var store = this.getStoreProgramaPerfilesStore();
            var rec = store.findRecord('id', this.getController("ControllerVarios").dameIdGrid(this.getGridProgramaPerfiles()));

            Ext.Msg.confirm('Eliminar', 'El registre s\'eliminarà. Estàs segur de voler continuar?', function (btn) {
                if (btn == 'yes') {
                    store.remove([rec]);
                    store.sync(
                        {
                            callback: function () {
                                ref.onGridProgramaSelectionChange();
                            }
                        });
                }
            });
        },

        sincronizarProgramaPerfiles: function () {
            this.getStoreProgramaPerfilesStore().sync(
                {
                    scope: this,
                    failure: function () {
                        Ext.Msg.alert('Afegir Perfils', '<b>No pots afegir aquest perfil al programa</b>');
                        this.getStoreProgramaPerfilesStore().reload();
                    }
                });
        },

        ////////////////////////// FUNCIONES GRID PROGRAMA CURSOS ///////////////////////////////////////////////

        borrarProgramaCursos: function () {
            var ref = this;
            var store = ref.getStoreProgramaCursosStore();
            var programaCurso = this.getGridProgramaCursos().getSelectionModel().getSelection();
            Ext.Msg.confirm('Eliminació de programes', '<b>Esteu segur/a de voler esborrar el curs sel·leccionat?</b>', function (btn) {
                if (btn == 'yes') {
                    store.remove(programaCurso);
                    store.sync();
                    ref.setEstadoComponentes();
                }
            });
        },

        anyadirProgramaCursos: function () {
            var programaId = this.getController("ControllerVarios").dameIdGrid(this.getGridProgramas());

            var store = this.getStoreProgramaCursosStore();

            var rec = this.getProgramaCursoModel().create(
                {
                    programaId: programaId
                });
            var rowEditor = this.getGridProgramaCursos().getPlugin('editingProgramaCurso');

            rowEditor.cancelEdit();
            store.insert(0, rec);
            rowEditor.startEdit(0, 0);
        },

        sincronizarProgramaCursos: function () {
            this.getStoreProgramaCursosStore().sync(
                {
                    scope: this,
                    failure: function () {
                        Ext.Msg.alert('Afegir Cursos', '<b>No pots afegir aquest curso al programa</b>');
                        this.getStoreProgramaCursosStore().reload();
                    }
                });
        },

        ///////////////////////  CARGA STORES /////////////////////////////////////////////////////////////

        cargaStoreProgramaUsuario: function (programaId) {
            var storeProgramaUsuario = this.getStoreProgramaUsuarioStore();

            storeProgramaUsuario.load(
                {
                    url: '/crm/rest/programausuario/programa',
                    params: {
                        programaId: programaId
                    },
                    scope: this,
                    callback: function () {
                        this.getTabProgramas().setActiveTab(0);
                        this.cargaStoreProgramaCursos(programaId);
                    }
                });
        },

        cargaStoreProgramaCursos: function (programaId) {
            var storeProgramaCursos = this.getStoreProgramaCursosStore();
            storeProgramaCursos.load(
                {
                    params: {
                        programaId: programaId
                    },
                    scope: this,
                    callback: function () {
                        this.cargaStoreProgramaPerfil(programaId);
                    }
                })

        },
        cargaStoreProgramaPerfil: function (programaId) {
            var storeProgramaPerfiles = this.getStoreProgramaPerfilesStore();
            storeProgramaPerfiles.load(
                {
                    params: {
                        programaId: programaId
                    },
                    scope: this,
                    callback: function () {
                        this.setEstadoComponentes();
                    }
                });
        },

        //-----------------FUNCIONES VARIAS------------------------------------------------------------------------------------

        actualizarIdsFilaGridProgramaPerfiles: function (editor, e) {
            var fila = editor.grid.getSelectionModel().getSelection()[0];

            if (fila.data.perfilNombre != e.newValues.perfilNombre) {
                fila.data.perfilId = e.newValues.perfilNombre;
            }
        },

        modificarStoreProgramas: function (editor, e) {

            var fila = editor.grid.getSelectionModel().getSelection();
            if (fila[0].data.servicioNombre != e.newValues.servicioNombre) {
                fila[0].data.servicioId = e.newValues.servicioNombre;
            }
            if (fila[0].data.tipoAccesoNombre != e.newValues.tipoAccesoNombre) {
                fila[0].data.tipoAccesoId = e.newValues.tipoAccesoNombre;
            }

        },

        actualizarIdsFilaGridProgramaCursos: function (editor, e) {
            var fila = editor.grid.getSelectionModel().getSelection()[0];

            if (fila.data.uestNombre != e.newValues.uestNombre) {
                fila.data.uestId = e.newValues.uestNombre;
            }
        },

        limpiarGridPersonas: function () {
            this.getStoreProgramaUsuarioStore().loadData([], false);
            this.setEstadoComponentes();
        },

        setEstadoComponentes: function () {
            var tieneSeleccionGridPrograma = this.getGridProgramas().getSelectionModel().hasSelection();
            this.getBotonBorrarPrograma().setDisabled(!(tieneSeleccionGridPrograma && (!this.getStoreProgramaUsuarioStore().totalCount && !this.getStoreProgramaPerfilesStore().totalCount)));

            this.getGridProgramasPersonas().setDisabled(!tieneSeleccionGridPrograma);
            this.getGridProgramaPerfiles().setDisabled(!tieneSeleccionGridPrograma);
            this.getGridProgramaCursos().setDisabled(!tieneSeleccionGridPrograma);
            this.getFormularioProgramas().setDisabled(!tieneSeleccionGridPrograma);

            var tieneSeleccionGridProgramaPerfiles = this.getGridProgramaPerfiles().getSelectionModel().hasSelection();
            this.getBotonBorrarProgramaPerfil().setDisabled(!tieneSeleccionGridProgramaPerfiles);

            var tieneSeleccionGridProgramaPersonas = this.getGridProgramasPersonas().getSelectionModel().hasSelection();
            this.getBotonBorrarPersona().setDisabled(!tieneSeleccionGridProgramaPersonas);

            var tieneSeleccionGridProgramaCursos = this.getGridProgramaCursos().getSelectionModel().hasSelection();
            this.getBotonBorrarProgramaCursos().setDisabled(!tieneSeleccionGridProgramaCursos)
        }

    });