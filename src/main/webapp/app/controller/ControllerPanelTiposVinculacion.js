Ext.define('CRM.controller.ControllerPanelTiposVinculacion',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreTiposVinculaciones' ],
    models : [ 'TipoDato' ],

    refs : [
    {
        selector : 'gridTiposVinculacion',
        ref : 'gridTiposVinculacion'
    },
    {
        selector : 'gridTiposVinculacion button[name=borrarTipoVinculacion]',
        ref : 'botonBorrarTipoVinculacion'
    } ],

    init : function()
    {
        this.control(
        {
            'gridTiposVinculacion button[name=borrarTipoVinculacion]' :
            {
                click : this.borrarTipoVinculacion
            },
            'gridTiposVinculacion button[name=anyadirTipoVinculacion]' :
            {
                click : this.anyadirTipoVinculacion
            },
            'gridTiposVinculacion' :
            {
                select : this.setEstadoComponentes,
                edit: this.guardaTipoVinculacion
            }
        });
    },

    anyadirTipoVinculacion : function()
    {
        var store = this.getStoreTiposVinculacionesStore();
        var tipoVinculacion = this.getTipoDatoModel().create();
        var rowEditor = this.getGridTiposVinculacion().getPlugin('editingTipoVinculacion');

        rowEditor.cancelEdit();
        store.insert(0, tipoVinculacion);
        rowEditor.startEdit(0, 0);
    },

    borrarTipoVinculacion : function()
    {
        var ref = this;
        var store = ref.getStoreTiposVinculacionesStore();
        var selection = this.getGridTiposVinculacion().getSelectionModel().getSelection();
        var TipoVinculacionId = (selection.length > 0) ? selection[0].data.id : null;
        var TipoVinculacion = store.findRecord('id', TipoVinculacionId);

        if (TipoVinculacion)
        {
            Ext.Msg.confirm('Esborrar Vinculació', 'Esteu segur/a de voler esborrar el registre sel·leccionada?', function(btn)
            {
                if (btn == 'yes')
                {
                    store.remove(TipoVinculacion);
                    store.sync(
                    {
                        success : function()
                        {
                            ref.setEstadoComponentes();
                        },
                        failure : function()
                        {
                            Ext.Msg.alert('Esborrar Vinculació', 'No s`ha pogut esborrar la vinculació registre perquè té registres associats.');
                            store.rejectChanges();
                            ref.setEstadoComponentes();
                        }
                    });
                }
            });
        }
    },

    guardaTipoVinculacion : function()
    {
        var store = this.getStoreTiposVinculacionesStore();
        store.sync();
    },

    setEstadoComponentes : function()
    {
        var tieneSeleccionGridTiposVinculacion = this.getGridTiposVinculacion().getSelectionModel().hasSelection();
        this.getBotonBorrarTipoVinculacion().setDisabled(!tieneSeleccionGridTiposVinculacion);
    }

});