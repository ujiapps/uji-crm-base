Ext.define('CRM.controller.ControllerPanelClientesCampanyas',
    {
        extend: 'Ext.app.Controller',
        stores: ['StoreClienteCampanyas', 'StoreMovimientosCampanya', 'StoreTarifasClienteCampanya', 'StoreCuotasClienteCampanya', 'StoreClienteDatosCampanya', 'StoreTiposTarifasCampanya', 'StoreClienteEstadoCampanyaMotivos'],
        models: ['CampanyaCliente', 'TipoEstadoCampanya', 'ClienteEstadoCampanyaMotivo'],
        views: ['clientes.PestanyaCampanyas.GridClienteCampanyas', 'clientes.GridClientes', 'clientes.PanelClientesCampanyas', 'clientes.PestanyaCampanyas.GridClienteMovimientosCampanya',
            'clientes.PestanyaCampanyas.GridClienteTarifasCampanya', 'clientes.PestanyaCampanyas.GridClienteCuotasCampanya', 'clientes.PestanyaCampanyas.GridClienteDatosCampanya',
            'clientes.VentanaClienteCampanya.VentanaAnyadirClienteCampanya'],

        refs: [{
            selector: 'panelClientesCampanyas',
            ref: 'panelClientesCampanyas'
        }, {
            selector: 'ventanaAnyadirClienteCampanya',
            ref: 'ventanaAnyadirClienteCampanya'
        }],

        ventanaClienteCampanya: {},
        init: function () {

            this.control({
                'gridClienteCampanyas button[name=anyadir]': {
                    click: this.anyadirClienteCampanya
                },
                'gridClienteCampanyas button[name=borrar]': {
                    click: this.borrarClienteCampanya
                },
                'gridClienteCampanyas': {
                    select: this.rellenaDatosClienteCampanya,
                    edit: this.sincronizarStoreClienteCampanya
                },
                'gridClienteTarifasCampanya': {
                    select: this.rellenaDatosClienteCuotas,
                    edit: this.modificarClienteTarifa
                },
                'gridClienteTarifasCampanya button[name=anyadirTarifaClienteCampanya]': {
                    click: this.anyadirTarifaClienteCampanya
                },
                'gridClienteTarifasCampanya button[name=eliminarTarifaClienteCampanya]': {
                    click: this.borrarTarifaClienteCampanya
                },
                'gridClienteDatosCampanya button[name=anyadirClienteDatoCampanya]': {
                    click: this.anyadirClienteDatoCampanya
                },
                'gridClienteDatosCampanya': {
                    itemdblclick: this.getController("ControllerPanelClientesDatos").mostrarVentanaGeneral
                },
                'gridClienteCuotasCampanya': {
                    edit: this.modificarClienteCuota
                }
            });
        },

        dameGridCampanyas: function (clienteId) {
            var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
            if (!clienteId) {
                clienteId = tabsServiciosCliente.getActiveTab().itemId;
            }
            return tabsServiciosCliente.down("[itemClienteId=" + clienteId + "]").down("[itemId=gridCampanyas-" + clienteId + "]");
        },

        dameGridClienteTarifas: function (clienteId) {
            var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
            if (!clienteId) {
                clienteId = tabsServiciosCliente.getActiveTab().itemId;
            }
            return tabsServiciosCliente.down("[itemClienteId=" + clienteId + "]").down("[itemId=gridClienteTarifaCampanya-" + clienteId + "]");
        },

        dameGridClienteDatos: function (clienteId) {
            var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
            if (!clienteId) {
                clienteId = tabsServiciosCliente.getActiveTab().itemId;
            }
            return tabsServiciosCliente.down("[itemClienteId=" + clienteId + "]").down("[itemId=gridClienteDatosCampanya-" + clienteId + "]");
        },

        dameGridClienteCuotas: function (clienteId) {
            var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
            if (!clienteId) {
                clienteId = tabsServiciosCliente.getActiveTab().itemId;
            }
            return tabsServiciosCliente.down("[itemClienteId=" + clienteId + "]").down("[itemId=gridClienteCuotaCampanya-" + clienteId + "]");
        },

        dameGridClienteMovimientos: function (clienteId) {
            var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
            if (!clienteId) {
                clienteId = tabsServiciosCliente.getActiveTab().itemId;
            }
            return tabsServiciosCliente.down("[itemClienteId=" + clienteId + "]").down("[itemId=gridClienteMovimientosCampanya-" + clienteId + "]");
        },

        sincronizarStoreClienteCampanya: function (editor, e) {

            var ref = this;

            if (e.colIdx == 3) {

                var estadoMotivoCampanyaCliente = this.getClienteEstadoCampanyaMotivoModel().create({
                    campanyaEstadoMotivoId: e.newValues.campanyaClienteMotivoId,
                    clienteId: e.record.data.clienteId,
                    campanyaId: e.record.data.campanyaId,
                    comentarios: ''
                });

                var store = this.getStoreClienteEstadoCampanyaMotivosStore();
                store.add(estadoMotivoCampanyaCliente);
                store.sync({
                    scope: this,
                    callback: function () {
                        this.dameGridCampanyas().getStore().reload();
                    }
                });
            } else {

                var grid = this.dameGridCampanyas();
                var campanyaCliente = this.getCampanyaClienteModel().create({
                    clienteId: this.getController("ControllerPanelClientes").dameIdCliente(),
                    campanyaId: e.record.data.campanyaId,
                    campanyaClienteTipoId: e.newValues.campanyaClienteTipoId
                });

                grid.setLoading(true);
                Ext.Ajax.request(
                    {
                        url: '/crm/rest/campanyacliente/',
                        method: 'PUT',
                        jsonData: campanyaCliente.data,
                        scope: this,
                        success: function () {
                            grid.getStore().load();
                            ref.limpiaTab();
                            grid.setLoading(false);

                        },
                        failure: function () {
                            grid.setLoading(false);
                            ref.limpiaTab();

                        }
                    });
            }
        },

        anyadirTarifaClienteCampanya: function () {

            var campanya = this.dameGridCampanyas().getSelectionModel().getSelection()[0].data.campanyaId;
            var cliente = this.getController("ControllerPanelClientes").dameIdCliente();
            this.getController("ControllerVentanaClienteCampanya").abrirVentanaClienteTarifa(campanya, cliente); //, function () {
        },

        borrarTarifaClienteCampanya: function () {
            var ref = this;

            var tarifa = this.dameGridClienteTarifas().getSelectionModel().getSelection();
            var tarifaId = tarifa[0].data.id;

            if (tarifaId) {
                Ext.Msg.confirm('Eliminació de tarifes', '<b>Esteu segur/a de voler esborrar la tarifa sel·leccionada?</b>', function (btn) {
                    if (btn == 'yes') {
                        var store = ref.dameGridClienteTarifas().getStore();
                        store.remove(tarifa);
                        store.sync({
                            scope: this,
                            callback: function () {
                                ref.dameGridClienteCuotas().getStore().reload();
                            }
                        });
                    }
                });
            }
        },

        modificarClienteTarifa: function (editor, e) {
            var storeClienteTarifa = this.dameGridClienteTarifas().getStore(); //this.getStoreTarifasClienteCampanyaStore();
            storeClienteTarifa.sync();
        }
        ,

        rellenaDatosClienteCuotas: function (grid, clienteTarifa) {
            this.dameGridClienteCuotas().getStore().load({
                params: {
                    clienteTarifa: clienteTarifa.data.id
                }
            });
            this.setEstadoComponentesTabTarifas();
        }
        ,

        modificarClienteCuota: function (editor, e) {
            var storeClienteCuota = this.dameGridClienteCuotas().getStore(); //this.getStoreTarifasClienteCampanyaStore();
            storeClienteCuota.sync();
        },


        rellenaDatosClienteCampanya: function (grid, select) {

            var storeEstadoCampanya = this.getStore('StoreTiposEstadoCampanyaCliente');
            storeEstadoCampanya.load({
                url: '/crm/rest/tipoestadocampanya/campanya/',
                params: {
                    campanyaId: select.data.campanyaId
                }
            });

            var storeTiposEstadoCampanyaMotivosCliente = this.getStore('StoreTiposEstadoCampanyaMotivosCliente');
            storeTiposEstadoCampanyaMotivosCliente.load({
                url: '/crm/rest/tipoestadocampanyamotivo',
                params: {
                    tipoEstadoCampanyaId: select.data.campanyaClienteTipoId
                }
            });

            var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
            var clienteId = tabsServiciosCliente.getActiveTab().itemId;
            var tab = tabsServiciosCliente.down("[itemClienteId=" + clienteId + "]").down("[itemId=tabCampanyaCliente-" + clienteId + "]");

            tab.setActiveTab(0);
            this.dameGridClienteCuotas().getStore().loadData([], false);
            tab.setDisabled(false);
            this.rellenaCampanyaMovimientos(grid, select);
            this.rellenaCampanyaTarifas(grid, select);
            this.rellenaCampanyaDatos(grid, select);
        }
        ,

        rellenaCampanyaTarifas: function (grid, select) {
            this.dameGridClienteTarifas().getStore().load({
                // this.getStoreTarifasClienteCampanyaStore().load(
                //     {
                params: {
                    campanyaId: select.data.campanyaId,
                    clienteId: select.data.clienteId
                },
                scope: this,
                callback: function () {
                    this.setEstadoComponentes();
                    this.setEstadoComponentesTabTarifas();
                }
            });

            var storeTiposTarifas = this.getStoreTiposTarifasCampanyaStore();
            storeTiposTarifas.load({
                    url: 'rest/tipotarifa/campanya/' + select.data.campanyaId
                }
            );
        }
        ,

        rellenaCampanyaDatos: function (grid, select) {

            this.dameGridClienteDatos().getStore().load({
                url: '/crm/rest/dato/campanya',
                params: {
                    clienteId: select.data.clienteId,
                    campanyaId: select.data.campanyaId
                }
            });
        }
        ,

        anyadirClienteDatoCampanya: function () {
            this.campanyaId = this.dameGridCampanyas().getSelectionModel().getSelection()[0].data.campanyaId;
            this.getController("ControllerPanelClientesDatos").mostrarVentanaAnyadirTipoDato();
        }
        ,

        rellenaCampanyaMovimientos: function (grid, select) {
            this.dameGridClienteMovimientos().getStore().load({
                // this.getStoreMovimientosCampanyaStore().load({
                params: {
                    campanyaId: select.data.campanyaId,
                    clienteId: select.data.clienteId
                },
                scope: this,
                callback: function () {
                    this.setEstadoComponentes();
                }
            });
        }
        ,

        anyadirClienteCampanya: function () {

            var cliente = this.getController("ControllerPanelClientes").dameCliente();
            var controllerAlta = this.getController('ControllerVentanaClienteCampanya');
            var storeCampanyacliente = this.dameGridCampanyas().getStore();

            controllerAlta.abrirVentanaAsociacionPorCliente(cliente, function () {
                storeCampanyacliente.sync({
                    scope: this,
                    callback: function () {
                        storeCampanyacliente.reload();
                    }
                });
            });

        }
        ,

        borrarClienteCampanya: function () {
            var ref = this;
            var clienteCampanya = this.dameGridCampanyas().getSelectionModel().getSelection();
            if (clienteCampanya.length == 1) {
                Ext.Msg.confirm('Eliminar el client', 'El client s\'eliminarà de la campanya. Estàs segur de voler continuar?', function (btn) {
                    if (btn == 'yes') {
                        Ext.Ajax.request(
                            {
                                url: '/crm/rest/campanyacliente/' + clienteCampanya[0].data.id,
                                method: 'DELETE',
                                scope: this,
                                success: function () {
                                    ref.dameGridCampanyas().getStore().load();
                                    ref.setEstadoComponentes();
                                    ref.limpiaTab();

                                },
                                failure: function () {
                                    ref.setEstadoComponentes();
                                    ref.limpiaTab();
                                    // grid.setLoading(false);

                                }
                            });
                    }
                });
            }
        },

        limpiaTab: function (clienteId) {
            this.dameGridClienteMovimientos(clienteId).getStore().loadData([], false);
            this.dameGridClienteTarifas(clienteId).getStore().loadData([], false);
            this.dameGridClienteDatos(clienteId).getStore().loadData([], false);

            var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
            if (!clienteId) {
                clienteId = tabsServiciosCliente.getActiveTab().itemId;
            }
            var tab = tabsServiciosCliente.down("[itemClienteId=" + clienteId + "]").down("[itemId=tabCampanyaCliente-" + clienteId + "]");

            tab.setActiveTab(0);
            tab.setDisabled(true);
        },

        setEstadoComponentes: function () {
            var tieneSeleccionGridClienteCampanyas = this.dameGridCampanyas().getSelectionModel().hasSelection();
            this.dameGridCampanyas().down("button[name=borrar]").setDisabled(!tieneSeleccionGridClienteCampanyas);
        },

        setEstadoComponentesTabTarifas: function () {
            var tieneSeleccionGridClienteTarifas = this.dameGridClienteTarifas().getSelectionModel().hasSelection();
            this.dameGridClienteTarifas().down("button[name=eliminarTarifaClienteCampanya]").setDisabled(!tieneSeleccionGridClienteTarifas);
        }

    });