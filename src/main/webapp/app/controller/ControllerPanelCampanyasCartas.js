Ext.define('CRM.controller.ControllerPanelCampanyasCartas',
    {
        extend: 'Ext.app.Controller',
        stores: ['StoreCampanyaCartas', 'StoreTiposCampanyaEnviosAuto', 'StoreCartasImagenes'],
        models: ['CampanyaCarta'],
        views: ['campanyas.cartas.FormularioCampanyaCarta', 'campanyas.cartas.GridCartasImagenes'],

        refs: [
            {
                selector: 'gridCampanyaCartas',
                ref: 'gridCampanyaCartas'
            },
            {
                selector: 'gridCampanyaCartas button[name=borrarCampanyaCarta]',
                ref: 'botonBorrarCampanyaCarta'
            },
            {
                selector: 'formularioCampanyaCarta',
                ref: 'formularioCampanyaCarta'
            },
            {
                selector: 'formularioCampanyaCarta container[name=editorCartas]',
                ref: 'editorFormularioCampanyaCartas'
            },
            {
                selector: 'gridCartasImagenes',
                ref: 'gridCartasImagenes'
            },
            {
                selector: 'gridCartasImagenes button[name=botonAnyadirImagen]',
                ref: 'botonAnyadirCartaImagen'
            },
            {
                selector: 'gridCartasImagenes button[name=botonBorrarImagen]',
                ref: 'botonBorrarCartaImagen'
            },
            {
                selector: 'gridCartasImagenes filefield[name=anyadirImagen]',
                ref: 'anyadirCartaImagen'
            },
            {
                selector: 'formularioCampanyaCarta form[name=formSubirImagenesCartas]',
                ref: 'formImagenesCarta'
            }],

        dirty: false,
        init: function () {
            this.control(
                {
                    'gridCampanyaCartas': {
                        select: this.mostrarFormularioCampanyaCarta,
                        edit: this.sincronizarStoreCampanyaCarta
                    },
                    'gridCampanyaCartas button[name=anyadirCampanyaCarta]': {
                        click: this.anyadirCampanyaCarta
                    },
                    'gridCampanyaCartas button[name=borrarCampanyaCarta]': {
                        click: this.borrarCampanyaCarta

                    },
                    'formularioCampanyaCarta button[action=actualizar]': {
                        click: this.actualizarCampanyaCarta
                    },

                    'gridCartasImagenes': {
                        select: function () {
                            this.getBotonBorrarCartaImagen().setDisabled(false);
                        }
                    },

                    'gridCartasImagenes filefield[name=anyadirImagen]': {
                        change: function () {
                            this.getBotonAnyadirCartaImagen().setDisabled(false);
                            this.getBotonBorrarCartaImagen().setDisabled(true);
                        }
                    },
                    'gridCartasImagenes button[name=botonAnyadirImagen]': {
                        click: this.anyadirImagen
                    },

                    'gridCartasImagenes button[name=botonBorrarImagen]': {
                        click: this.borrarImagenes
                    }
                });
        },

        sincronizarStoreCampanyaCarta: function () {
            this.getStoreCampanyaCartasStore().sync(
                {
                    scope: this,
                    success: function () {
                        this.mostrarFormularioCampanyaCarta(this.getGridCampanyaCartas(), this.getGridCampanyaCartas().getSelectionModel().getSelection()[0]);
                    },
                    failure: function () {
                        Ext.Msg.alert('Afegir Campanya carta', '<b>No s\'ha pogut afegir la carta</b>');
                        this.getStoreCampanyaCartasStore().reload();
                    }
                });
        },

        anyadirCampanyaCarta: function () {

            var store = this.getStoreCampanyaCartasStore();
            var campanyaId = this.getController("ControllerPanelCampanyas").dameIdCampanya();

            var rec = this.getCampanyaCartaModel().create({
                campanyaId: campanyaId
            });
            var rowEditor = this.getGridCampanyaCartas().getPlugin('editingCampanyaCartas');

            rowEditor.cancelEdit();
            store.insert(0, rec);
            rowEditor.startEdit(0, 0);
        },

        actualizarCampanyaCarta: function () {

            var store = this.getStoreCampanyaCartasStore();
            var form = this.getFormularioCampanyaCarta();

            var campanyaId = this.getController("ControllerPanelCampanyas").dameIdCampanya();
            var cartaId = this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaCartas());

            var carta = store.findRecord('id', cartaId);

            carta.set('campanyaCartaTipoId', form.down("combo[name=tipoCarta]").getValue());
            carta.set('cuerpo', form.down("[name=cuerpoCarta]").getValue());

            store.sync(
                {
                    scope: this,
                    callback: function (success) {
                        if (success) {
                            this.cargaStoreCampanyaCartas(campanyaId);
                        }
                        else {
                            Ext.Msg.alert('Actualització de cartes', '<b>La carta no ha sigut actualitzada</b>');
                        }
                    }
                });

        },

        borrarCampanyaCarta: function () {
            var ref = this;
            Ext.Msg.confirm('Eliminació de cartes', '<b>Esteu segur/a de voler esborrar la carta de la campanya sel·leccionada?</b>', function (btn) {
                if (btn == 'yes') {
                    var campanyaCarta = ref.getGridCampanyaCartas().getSelectionModel().getSelection();
                    if (campanyaCarta.length == 1) {
                        var registro = campanyaCarta[0];
                        var store = ref.getStoreCampanyaCartasStore();
                        store.remove(registro);
                        store.sync(
                            {
                                success: function () {
                                    ref.setEstadoComponentes();
                                },
                                failure: function () {
                                    Ext.Msg.alert('Esborrar Carta', 'No s`ha pogut esborrar la carta.');
                                    store.rejectChanges();
                                    ref.setEstadoComponentes();
                                },
                                scope: this
                            });
                    }
                }
            });
        },

        mostrarFormularioCampanyaCarta: function (grid, select) {

            if (select.data.id) {
                this.setEstadoComponentes();

                var form = this.getFormularioCampanyaCarta();

                var txt = this.getEditorFormularioCampanyaCartas().down('[name=cuerpoCarta]');

                if (txt != null) {
                    txt.destroy();
                }

                var config = {
                    // height: 400,
                    name: 'cuerpoCarta',
                    fieldLabel: 'Corp'
                };
                var editorNuevo = new Ext.ux.CKEditor(config);
                this.getEditorFormularioCampanyaCartas().add(editorNuevo);

                // if (select) {
                form.down("combo[name=tipoCarta]").setValue(select.data.campanyaCartaTipoId);
                editorNuevo.setValue(select.data.cuerpo);

                var campanyaCartaId = this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaCartas());
                editorNuevo.setUrl('/cartaimagen/visualizar/' + campanyaCartaId);
                this.listaImagenes(campanyaCartaId);
            }
        },

        cargaStoreCampanyaCartas: function (campanyaId) {
            var storeCampanyaCartas = this.getStoreCampanyaCartasStore();

            var form = this.getFormularioCampanyaCarta();
            var txt = this.getEditorFormularioCampanyaCartas().down('[name=cuerpoCarta]');

            if (txt != null) {
                txt.destroy();
            }
            form.down("combo[name=tipoCarta]").setValue(null);

            storeCampanyaCartas.load(
                {
                    params: {
                        campanyaId: campanyaId
                    },
                    scope: this,
                    callback: function () {
                        this.setEstadoComponentes();

                    }
                });
        },

        listaImagenes: function (campanyaCarta) {
            this.getStoreCartasImagenesStore().load(
                {
                    url: '/crm/rest/cartaimagen/' + campanyaCarta
                })
        },

        anyadirImagen: function () {
            var campanyaCarta = this.getController("ControllerVarios").dameIdGrid(this.getGridCampanyaCartas()),
                formImagenes = this.getFormImagenesCarta();

            if (formImagenes.getForm().isValid()) {
                formImagenes.submit(
                    {
                        url: '/crm/rest/cartaimagen/' + campanyaCarta,
                        scope: this,
                        success: function () {
                            this.getBotonAnyadirCartaImagen().setDisabled(true);
                            this.getBotonBorrarCartaImagen().setDisabled(true);
                            this.listaImagenes(campanyaCarta);
                        },
                        failure: function (x, resp) {
                            Ext.Msg.alert('Insercció de imatges', '<b>' + resp.result.message + '</b>');
                        }
                    });
            }
            else {
                Ext.Msg.alert('Insercció de imatges', '<b>El fitxer no es una imatge</b>');
            }
        },

        borrarImagenes: function () {
            var storeImagenes = this.getStoreCartasImagenesStore();
            var selection = this.getGridCartasImagenes().getSelectionModel().getSelection();

            var ref = this;
            Ext.Msg.confirm('Eliminar', 'El registre s\'eliminarà. Estàs segur de voler continuar?', function (btn) {
                if (btn == 'yes') {
                    ref.getGridCartasImagenes().setLoading(true);
                    storeImagenes.remove(selection);
                    storeImagenes.sync(
                        {
                            success: function () {
                                ref.getBotonBorrarCartaImagen().setDisabled(true);
                                ref.getGridCartasImagenes().setLoading(false);
                            },
                            failure: function () {
                                Ext.Msg.alert('Esborrat de fitxers', '<b>El fitxer no ha pogut ser borrat correctament</b>');
                                ref.getGridCartasImagenes().setLoading(false);
                            }

                        });

                }
            });

        },

        setEstadoComponentes: function () {
            var tieneSeleccionGridCampanyaCartas = this.getGridCampanyaCartas().getSelectionModel().hasSelection();
            this.getBotonBorrarCampanyaCarta().setDisabled(!tieneSeleccionGridCampanyaCartas);
            this.getFormularioCampanyaCarta().setDisabled(!tieneSeleccionGridCampanyaCartas);
        }
    })
;