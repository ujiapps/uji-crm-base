Ext.define('CRM.controller.ControllerPanelEtiquetas',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreEtiquetas' ],
    models : [ 'Etiqueta' ],

    refs : [
    {
        selector : 'gridEtiquetas',
        ref : 'gridEtiquetas'
    },
    {
        selector : 'gridEtiquetas button[name=borrarEtiqueta]',
        ref : 'botonBorrarEtiqueta'
    } ],

    init : function()
    {
        this.control(
        {
            'gridEtiquetas button[name=borrarEtiqueta]' :
            {
                click : this.borrarEtiqueta
            },
            'gridEtiquetas button[name=anyadirEtiqueta]' :
            {
                click : this.anyadirEtiqueta
            },
            'gridEtiquetas' :
            {
                select : this.setEstadoComponentes,
                edit: this.guardaEtiqueta
            }
        });
    },

    anyadirEtiqueta : function()
    {
        var store = this.getStoreEtiquetasStore();
        var etiqueta = this.getEtiquetaModel().create();
        var rowEditor = this.getGridEtiquetas().getPlugin('editingEtiqueta');

        rowEditor.cancelEdit();
        store.insert(0, etiqueta);
        rowEditor.startEdit(0, 0);
    },

    borrarEtiqueta : function()
    {
        var ref = this;
        var store = ref.getStoreEtiquetasStore();
        var selection = this.getGridEtiquetas().getSelectionModel().getSelection();
        var etiquetaId = (selection.length > 0) ? selection[0].data.id : null;
        var etiqueta = store.findRecord('id', etiquetaId);

        if (etiqueta)
        {
            Ext.Msg.confirm('Esborrar Etiqueta', 'Esteu segur/a de voler esborrar la etiqueta sel·leccionada?', function(btn)
            {
                if (btn == 'yes')
                {
                    store.remove(etiqueta);
                    store.sync(
                    {
                        success : function()
                        {
                            ref.setEstadoComponentes();
                        },
                        failure : function()
                        {
                            Ext.Msg.alert('Esborrar Etiqueta', 'No s`ha pogut esborrar la etiqueta perquè té registres associats.');
                            store.rejectChanges();
                            ref.setEstadoComponentes();
                        }
                    });
                }
            });
        }
    },

    guardaEtiqueta : function()
    {
        var store = this.getStoreEtiquetasStore();
        store.sync();
    },

    setEstadoComponentes : function()
    {
        var tieneSeleccionGridEtiquetas = this.getGridEtiquetas().getSelectionModel().hasSelection();
        this.getBotonBorrarEtiqueta().setDisabled(!tieneSeleccionGridEtiquetas);
    }

});