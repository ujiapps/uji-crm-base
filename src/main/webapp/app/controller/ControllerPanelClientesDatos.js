Ext.define('CRM.controller.ControllerPanelClientesDatos',
    {
        extend: 'Ext.app.Controller',
        views: ['clientes.PestanyaDatos.GridClienteDatos', 'clientes.PestanyaDatos.GridClienteDatosRelacionados', 'clientes.PestanyaDatos.VentanaGeneralTipo',
            'clientes.PestanyaDatos.FormTipoDato', 'clientes.PestanyaDatos.FormTipoItem', 'clientes.PestanyaDatos.VentanaElegirTipoDato'],
        stores: ['StoreClienteDatosExtraOrigen', 'StoreTiposDatosSelect', 'StoreClienteItems', 'StoreTiposDatosEstadosEnvios'],

        refs: [
            {
                selector: 'gridClienteDatos',
                ref: 'gridClienteDatos'
            },
            {
                selector: 'gridClienteDatosRelacionados',
                ref: 'gridClienteDatosRelacionados'
            },
            {
                selector: 'ventanaGeneralTipo',
                ref: 'ventanaGeneralTipo'
            },
            {
                selector: 'ventanaGeneralTipo [name=formTipoDato]',
                ref: 'formTipoDato'
            },
            {
                selector: 'ventanaGeneralTipo [name=formTipoItem]',
                ref: 'formTipoItem'
            },
            {
                selector: 'ventanaElegirTipoDato',
                ref: 'ventanaElegirTipoDato'
            }
        ],

        dirty: false,
        init: function () {

            this.control(
                {
                    'gridClienteDatos': {
                        itemdblclick: this.mostrarVentanaGeneral
                    },

                    'button[name=anyadirClienteDato]': {
                        click: this.anyadirTipoDatoGenerico
                    },
                    'button[name=borrarClienteDato]': {
                        click: this.borrarTipoDato
                    },

                    'gridClienteDatosRelacionados': {
                        itemdblclick: this.mostrarVentanaTipoDatoRelacionado
                    },
                    'button[name=afegirTipoDato]': {
                        click: this.anyadirTipoDato
                    },
                    'button[name=cerrarVentanaTipoDato]': {
                        click: this.cancelarVentanaTipoDato
                    },
                    'button[name=modificarTipoDato]': {
                        click: this.modificarTipoDato
                    },
                    'ventanaElegirTipoDato combobox[name=ventanaElegirTipoDatoCombo]': {
                        select: this.mostrarVentanaElegirTipoDato
                    },
                    'button[name=botonDescargarFichero]': {
                        click: this.descargarDatoFichero
                    }
                });
        },

        dameStoreDatos: function () {
            var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
            var clienteId = tabsServiciosCliente.getActiveTab().itemId;
            return tabsServiciosCliente.down("[itemId=" + clienteId + "]").down("[itemId=gridDatos-" + clienteId + "]").getStore();
        },

        descargarDatoFichero: function () {

            var url = '/crm/rest/cliente/fichero/' + this.getVentanaGeneralTipo().conf.data.id + '/raw';

            var body = Ext.getBody();
            var frame = body.getById('hiddenform-iframe');
            if (frame == null) {
                var frame = body.createChild(
                    {
                        tag: 'iframe',
                        cls: 'x-hidden',
                        id: 'hiddenform-iframe',
                        name: 'iframe'
                    });
            }
            var form = body.getById('hiddenform-form');
            if (form == null) {
                var form = body.createChild(
                    {
                        tag: 'form',
                        cls: 'x-hidden',
                        id: 'hiddenform-form',
                        target: 'iframe'
                    });
            }
            form.set(
                {
                    action: url
                });
            form.dom.submit();
        },

        cancelarVentanaTipoDato: function (tipo, id) {

            if (this.getVentanaGeneralTipo().tipoOrigen) {
                var tipoOrigen = this.getVentanaGeneralTipo().tipoOrigen;
                var origen = this.getVentanaGeneralTipo().origen;
                var tipoCancela = this.getVentanaGeneralTipo().tipo;
            }

            if (tipo == 'CONTINUAR') {
                var tipoOrigenContinuar = this.getVentanaGeneralTipo().tipo;
            }


            this.getVentanaGeneralTipo().destroy();

            if (tipoOrigen) {
                this.creaVentana(tipoOrigen, origen, tipoCancela);
            }
            if (tipo == 'CONTINUAR') {
                this.creaVentana(tipoOrigenContinuar, id);
            }
        },

        borrarTipoDato: function (boton) {

            var grid = boton.up("grid");

            var elem = grid.getSelectionModel().getSelection();

            var ref = this;

            if (elem[0].data.tipoDato == 'DATO-EXTRA') {
                Ext.Ajax.request(
                    {
                        url: '/crm/rest/clientedato/' + elem[0].data.id,
                        method: 'DELETE',
                        success: function () {
                            grid.getStore().reload();
                            Ext.Msg.alert('Esborrar dada', 'La dada a sigut esborrada correctament');
                        },
                        failure: function () {
                            Ext.Msg.alert('Esborrar dada', 'S\'ha produït un error al esborrar la dada');
                        }
                    });
            }
            else if (elem[0].data.tipoDato == 'ITEM') {
                Ext.Ajax.request(
                    {
                        url: '/crm/rest/clienteitem/' + elem[0].data.id,
                        method: 'DELETE',
                        success: function () {
                            grid.getStore().reload();
                            Ext.Msg.alert('Esborrar dada', 'La dada a sigut esborrada correctament');
                        },
                        failure: function () {
                            Ext.Msg.alert('Esborrar dada', 'S\'ha produït un error al esborrar la dada');
                        }
                    });
            }
        },

        creaVentana: function (tipoDato, id, tipoCancela) {

            var ventana;
            var vista;

            ventana = this.getView('clientes.PestanyaDatos.VentanaGeneralTipo');

            if (tipoDato == 'DATO-EXTRA') {

                var storeDato = Ext.create('CRM.store.StoreClienteDatosExtraOrigen').load({

                    url: '/crm/rest/clientedato/' + id,
                    callback: function () {
                        vista = ventana.create({conf: storeDato.getAt(0), tipo: 'DATO-EXTRA'});
                        vista.show();
                    }
                });
            }
            else { //ITEM

                var storeItem = Ext.create('CRM.store.StoreClienteItems').load({
                    url: '/crm/rest/clienteitem/' + id,
                    callback: function () {
                        vista = ventana.create({conf: storeItem.getAt(0), tipo: 'ITEM'});
                        vista.show();
                    }
                });
            }
        },

        mostrarVentanaGeneral: function (grid, item) {
            this.getController("ControllerPanelClientesDatos").creaVentana(item.data.tipoDato, item.data.id);
        },

        anyadirTipoDatoGenerico: function () {
            this.getController("ControllerPanelClientesCampanyas").campanyaId = null;
            this.mostrarVentanaAnyadirTipoDato();
        },

        mostrarVentanaAnyadirTipoDato: function () {
            var ventana, vista;

            ventana = this.getView('clientes.PestanyaDatos.VentanaElegirTipoDato');
            vista = ventana.create();
            vista.show();
        },


        mostrarVentanaElegirTipoDato: function (combo, campo) {

            var datoOrigen, tipoOrigen;

            if (this.getVentanaGeneralTipo()) {

                datoOrigen = this.getVentanaGeneralTipo().conf.data.id;
                tipoOrigen = this.getVentanaGeneralTipo().tipo;

                this.getVentanaGeneralTipo().destroy();
            }

            var ventana, vista;

            ventana = this.getView('clientes.PestanyaDatos.VentanaGeneralTipo');
            vista = ventana.create({tipo: campo[0].data.id, origen: datoOrigen, tipoOrigen: tipoOrigen});
            vista.show();
            this.getVentanaElegirTipoDato().destroy();
        },

        mostrarVentanaTipoDatoRelacionado: function (grid, item) {

            this.getVentanaGeneralTipo().destroy();

            this.creaVentana(item.data.tipoDato, item.data.id);
        },

        modificarTipoDato: function (boton) {

            if (this.getVentanaGeneralTipo().tipo == 'DATO-EXTRA') {
                this.modificarTipoDatoExtra(boton.tipo);

            }
            else if (this.getVentanaGeneralTipo().tipo == 'ITEM') {
                this.modificarTipoDatoItem(boton.tipo);
            }
        },

        modificarTipoDatoItem: function (tipo) {

            var ref = this;
            var form = this.getFormTipoItem();

            if (form.getForm().isValid()) {

                var values = form.getValues();

                Ext.Ajax.request(
                    {
                        url: '/crm/rest/clienteitem/' + values.id,
                        method: 'PUT',
                        params: {
                            itemId: values.comboVentanaTipoItemItems,
                            correo: values.ventanaTipoItemCorreo,
                            postal: values.ventanaTipoItemPostal
                        },

                        success: function () {

                            ref.accionDespuesDeSucces(tipo, values.id);
                        },
                        failure: function () {
                        }
                    }
                );

            }

        },

        modificarTipoDatoExtra: function (tipo) {

            var ref = this;
            var form = this.getFormTipoDato();

            if (form.getForm().isValid()) {

                var values = form.getValues();
                var valor;


                if (values.ventanaTipoDatoCampoFecha) {
                    valor = values.ventanaTipoDatoCampoFecha;
                }
                if (values.ventanaTipoDatoCampoFichero) {
                    valor = values.ventanaTipoDatoCampoFichero;
                }
                if (values.ventanaTipoDatoCampoNumerico) {
                    valor = values.ventanaTipoDatoCampoNumerico;
                }
                if (values.ventanaTipoDatoCampoSelect) {
                    valor = values.ventanaTipoDatoCampoSelect;
                }
                if (values.ventanaTipoDatoCampoTexto) {
                    valor = values.ventanaTipoDatoCampoTexto;
                }
                if (values.ventanaTipoDatoCampoTextoArea) {
                    valor = values.ventanaTipoDatoCampoTextoArea;
                }

                Ext.Ajax.request(
                    {
                        url: '/crm/rest/clientedato/' + values.id,
                        method: 'PUT',
                        params: {
                            valor: valor,
                            tipoAcceso: values.comboTipoAcceso,
                            comentarios: values.ventanaTipoDatoCampoComentario
                        },
                        success: function () {
                            ref.accionDespuesDeSucces(tipo, values.id);
                        },
                        failure: function () {
                        }
                    });
            }
            else {
                Ext.Msg.alert('Modificació de dades', '<b>La dada no es correcta</b>');
            }
        },

        anyadirTipoDato: function (boton) {


            var clienteId = this.getController("ControllerPanelClientes").dameIdCliente();

            if (this.getVentanaGeneralTipo().tipo == 'DATO-EXTRA') {
                this.anyadirTipoDatoExtra(clienteId, boton.tipo);

            }
            else if (this.getVentanaGeneralTipo().tipo = 'ITEM') {
                this.anyadirTipoDatoItem(clienteId, boton.tipo);
            }
        },

        anyadirTipoDatoItem: function (clienteId, tipo) {
            var ref = this;
            var form = this.getFormTipoItem();

            var tipoOrigen, clienteItemRelacionado, datoExtraRelacionado;
            if (this.getVentanaGeneralTipo().tipoOrigen) {
                tipoOrigen = this.getVentanaGeneralTipo().tipoOrigen;

                if (tipoOrigen == 'DATO-EXTRA') {
                    datoExtraRelacionado = this.getVentanaGeneralTipo().origen;
                }
                else {
                    clienteItemRelacionado = this.getVentanaGeneralTipo().origen;
                }
            }

            if (form.getForm().isValid()) {

                var values = form.getValues();

                Ext.Ajax.request(
                    {
                        url: '/crm/rest/clienteitem/',
                        method: 'POST',
                        params: {
                            clienteId: clienteId,
                            itemId: values.comboVentanaTipoItemItems,
                            correo: values.ventanaTipoItemCorreo,
                            postal: values.ventanaTipoItemPostal,
                            clienteItemRelacionado: clienteItemRelacionado,
                            clienteDato: datoExtraRelacionado,
                            campanyaId: this.getController("ControllerPanelClientesCampanyas").campanyaId
                        },

                        success: function (obj) {

                            var resp = Ext.JSON.decode(obj.responseText);
                            ref.accionDespuesDeSucces(tipo, resp.data.id);
                        },
                        failure: function () {
                        }
                    });

            }
        }
        ,

        anyadirTipoDatoExtra: function (clienteId, tipo) {

            var ref = this;
            var form = this.getFormTipoDato();

            if (form.getForm().isValid()) {

                var values = form.getValues();
                var valor;


                if (values.ventanaTipoDatoCampoFecha) {
                    valor = values.ventanaTipoDatoCampoFecha;
                }
                if (values.ventanaTipoDatoCampoFichero) {
                    valor = values.ventanaTipoDatoCampoFichero;
                }
                if (values.ventanaTipoDatoCampoNumerico) {
                    valor = values.ventanaTipoDatoCampoNumerico;
                }
                if (values.ventanaTipoDatoCampoSelect) {
                    valor = values.ventanaTipoDatoCampoSelect;
                }
                if (values.ventanaTipoDatoCampoTexto) {
                    valor = values.ventanaTipoDatoCampoTexto;
                }
                if (values.ventanaTipoDatoCampoTextoArea) {
                    valor = values.ventanaTipoDatoCampoTextoArea;
                }

                var tipoOrigen, clienteItemRelacionado, datoExtraRelacionado;
                if (this.getVentanaGeneralTipo().tipoOrigen) {
                    tipoOrigen = this.getVentanaGeneralTipo().tipoOrigen;

                    if (tipoOrigen == 'DATO-EXTRA') {
                        datoExtraRelacionado = this.getVentanaGeneralTipo().origen;
                    }
                    else {
                        clienteItemRelacionado = this.getVentanaGeneralTipo().origen;
                    }
                }

                Ext.Ajax.request(
                    {
                        url: '/crm/rest/clientedato/',
                        method: 'POST',
                        params: {
                            valor: valor,
                            clienteId: clienteId,
                            tipoDatoId: values.ventanaTipoDatoCampoTipoCombobox,
                            tipoAccesoId: values.comboTipoAcceso,
                            comentarios: values.ventanaTipoDatoCampoComentario,
                            clienteDatoId: datoExtraRelacionado,
                            itemId: clienteItemRelacionado,
                            clienteItemId: clienteItemRelacionado,
                            campanyaId: this.getController("ControllerPanelClientesCampanyas").campanyaId
                        },
                        success: function (obj) {
                            var resp = Ext.JSON.decode(obj.responseText);
                            ref.accionDespuesDeSucces(tipo, resp.data.id);
                        },
                        failure: function () {
                        }
                    });
            }
            else {
                Ext.Msg.alert('Insercció de dades', '<b>La dada no es correcta</b>');
            }
        },

        accionDespuesDeSucces: function (tipo, id) {

            this.cancelarVentanaTipoDato(tipo, id);
            if (this.getController("ControllerPanelClientesCampanyas").campanyaId)
                this.getController("ControllerPanelClientesCampanyas").dameGridClienteDatos().getStore().reload();
            else
                this.dameStoreDatos().reload();

        }

    });