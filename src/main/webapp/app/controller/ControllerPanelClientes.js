Ext.define('CRM.controller.ControllerPanelClientes', {
    alias: 'controllerPanelClientes',
    extend: 'Ext.app.Controller',
    stores: ['StoreClientesGrid', 'StoreClientes', 'StoreClientesById', 'StorePersonas', 'StoreClientesDatosTipos', 'StoreTiposDatosAcceso', 'StoreVinculacionesUJI',
        'StorePersonaCliente', 'StoreEtiquetasClientes', 'StoreClienteCursos', 'StoreTiposCliente', 'StoreMovimientos', 'StoreCampanyas', 'StoreClientesDatosTiposClientes',
        'StoreComboBusqueda', 'StoreTiposDatosSelect', 'StoreComboBusquedaModificable', 'StoreGrupos', 'StoreProgramasAccesoUser', 'StoreGruposPrograma', 'StoreItemsPrograma',
        'StoreTiposEstadoCampanya', 'StoreServicios', 'StoreClienteEstudiosUJI', 'StorePaises', 'StoreProvincias', 'StorePoblaciones', 'StoreTipoSexo', 'StoreClienteCuentas',
        'StoreClienteEstudios'],
    models: ['Cliente', 'Persona', 'ClienteGeneral', 'TipoDato'],

    views: ['clientes.GridClientes', 'clientes.PanelClientes', 'clientes.FormClientesDatos', 'clientes.VentanaNewCliente', 'clientes.GridClientesVinculacionesUJI',
        'clientes.GridClienteCursos', 'clientes.GridClienteMovimientos', 'clientes.VentanaExportacion', 'clientes.VentanaAnyadirACampanya', 'clientes.TabCliente',
        'clientes.GridClienteEstudios', 'clientes.GridClienteCuentas', 'clientes.TabClienteDatos', 'clientes.VentanaBusquedaCliente'],

    requires: ['CRM.model.enums.TiposCliente'],

    ventanaCliente: {},
    ventanaBusquedaCliente: {},

    getVentanaNuevoClienteCampanya: function () {
        return this.getView('clientes.VentanaNewCliente').create();
    },

    getVentanaBusquedaCliente: function (group) {
        var win = this.getView('clientes.VentanaBusquedaCliente').create();
        win.on('close', this.onCloseWindowBusqueda, this);
        return win;
    },

    refs: [{
        selector: 'gridClientes',
        ref: 'gridClientes'
    }, {
        selector: 'formClientes',
        ref: 'formClientes'
    }, {
        selector: 'panelClientes',
        ref: 'panelClientes'
    }, {
        selector: 'tabCliente',
        ref: 'tabCliente'
    }, {
        selector: 'panelClientes [name=tabsServiciosCliente]',
        ref: 'tabsServiciosCliente'
    }, {
        selector: 'panelClientes tabpanel[name=tabs]',
        ref: 'tabPanelCliente'
    }, {
        selector: 'ventanaNewCliente',
        ref: 'ventanaNewCliente'
    }, {
        selector: 'ventanaNewCliente combo[name=comboCliente]',
        ref: 'comboCliente'
    }, {
        selector: 'ventanaNewCliente form[name=formNewCliente]',
        ref: 'formNewCliente'
    }, {
        selector: 'gridClientesVinculacionesUJI',
        ref: 'gridClientesVinculacionesUJI'
    }, {
        selector: 'gridClienteCuentas',
        ref: 'gridClienteCuentas'
    }, {
        selector: 'gridClienteCursos',
        ref: 'gridClienteCursos'
    }, {
        selector: 'panelClientesFotos',
        ref: 'panelClientesFotos'
    }, {
        selector: 'gridClienteEstudios',
        ref: 'gridClienteEstudios'
    }, {
        selector: 'ventanaBusquedaCliente [name=filtrosBusquedaClientes]',
        ref: 'filtroBusqueda'
    }, {
        selector: 'applicationViewport',
        ref: 'applicationViewport'
    }],

    dirty: false,
    init: function () {
        this.control({

            'ventanaBusquedaCliente button[name=botonBusqueda]': {
                click: this.busqueda
            },

            'ventanaBusquedaCliente [name=filtrosBusquedaClientes] textfield[name=busquedaDNI]': {
                specialkey: function (field, e) {
                    if (e.getKey() == e.ENTER) {
                        this.busqueda();
                    }
                }
            },

            // 'ventanaBusquedaCliente [name=filtrosBusquedaClientes] combobox[name=busquedaEtiqueta]': {
            //     specialkey: function (field, e) {
            //         if (e.getKey() == e.ENTER) {
            //             var ref = this;
            //             setTimeout(function () {
            //                 ref.busqueda();
            //             }, 200);
            //         }
            //     }
            // },

            // 'ventanaBusquedaCliente [name=filtrosBusquedaClientes] combobox[name=busquedaTipo]': {
            //     select: this.busqueda
            // },

            'panelClientes button[name=anyadirCliente]': {
                click: this.showVentanaAddCliente
            },

            // 'gridClientes button[name=borrarCliente]': {
            //     click: this.eliminarClienteGeneral
            //
            // },

            'panelClientes button[name=busquedaCliente]': {
                click: this.abrirVentanaBusquedaCliente
            },

            'gridClientes': {
                groupclick: function (view, node, group, e, eOpts) {
                    var t = e.getTarget('.grpCheckbox');
                    if (t == null) {
                        t = Ext.get(e.target).query('.grpCheckbox')[0];
                        if (t.checked) {
                            t.checked = false;
                        } else {
                            t.checked = true;
                        }
                    }
                    if (t) {
                        var checkBoxArray = view.up('gridClientes').getEl().query('.grpCheckbox');
                        Ext.each(checkBoxArray, function (check) {
                            if (t.id != check.id) {
                                check.checked = false;
                            }
                        });
                        // view.up('gridClientes').setLoading(true);

                        if (t.checked) {
                            this.clienteGeneralSeleccionado = group;
                            this.ventanaBusquedaCliente.close();


                            // this.clienteGeneralSeleccionado = group;
                            // var clienteGeneral = this.getGridClientes().getStore().findRecord('id', group);
                            // this.rellenaPestanyasClientes(group, clienteGeneral);
                        } else {
                            this.clienteGeneralSeleccionado = null;
                            // this.limpiaPestanyasClientes();
                        }
                    }

                }
            },

            'formClientes button[name=actualizarCliente]': {
                click: this.actualizarCliente

            },

            'formClientes button[name=eliminarCliente]': {
                click: this.eliminarCliente
            },

            'formClientes combobox[name=paisPostalId]': {
                select: this.cargaComboProvincias
            },

            'formClientes combobox[name=provinciaPostalId]': {
                select: this.cargaComboPoblaciones
            },

            'ventanaNewCliente button[action=cancelar]': {
                click: this.closeVentanaAddCliente
            },

            'ventanaNewCliente textfield[name=identificacion]': {
                specialkey: function (field, e) {
                    if ((e.getKey() == e.ENTER || e.getKey() == e.TAB) && this.getFormNewCliente().down("textfield[name=identificacion]").getValue()) {
                        this.completaInformacionNuevoCliente();
                    }
                }
            },

            'ventanaNewCliente button[action=guardar-cliente]': {
                click: this.insertarCliente
            },

            'ventanaNewCliente button[action=limpiar]': {
                click: this.limpiarVentanaNewCliente
            },

            'ventanaNewCliente combobox[name=paisPostalId]': {
                select: this.cargaComboProvinciasVentanaAnyadirCliente
            },

            'ventanaNewCliente combobox[name=provinciaPostalId]': {
                select: this.cargaComboPoblacionesVentanaAnyadirCliente
            },

            'panelClientesFotos filefield': {
                change: this.onButtonUploadFile
            },
            'panelClientesFotos button[name=descargar]': {
                click: this.descargarFotoCRM
            },
            'panelClientesFotos button[name=validar]': {
                click: this.validarFotoCRM
            }
        });
    },

    abrirVentanaBusquedaCliente: function () {
        this.ventanaBusquedaCliente = this.getVentanaBusquedaCliente().show();
    },

    rellenaPestanyasClientes: function (clienteGeneralId, clienteGeneral) {

        this.getApplicationViewport().addNewTab('CRM.view.clientes.PanelClientes');
        var panelCliente = this.getPanelClientes();
        var tabsServiciosCliente = panelCliente.down("[name=tabsServiciosCliente]");

        if (clienteGeneralId) {

            tabsServiciosCliente.store.load({
                url: '/crm/rest/cliente/general/',
                params: {
                    clienteGeneralId: clienteGeneralId
                },
                scope: this,
                callback: function (records) {
                    tabsServiciosCliente.clienteGeneralId = clienteGeneralId;
                    tabsServiciosCliente.removeAll();

                    tabsServiciosCliente.creaPestanyasServicios(records, clienteGeneral);
                    tabsServiciosCliente.creaPestanyasPersona(clienteGeneral.data.personaId);

                    tabsServiciosCliente.down("[name=gridClienteVinculacionesUJI]").rellenaGridVinculacionesUJI(clienteGeneral.data.personaId);
                    tabsServiciosCliente.down("[name=gridClienteCuentasCorporativas]").rellenaGridCuentasCorporativas(clienteGeneral.data.personaId);
                    tabsServiciosCliente.down("[name=gridClienteCursos]").rellenaGridClienteCursos(clienteGeneral.data.personaId);
                    tabsServiciosCliente.down("[name=gridClienteEstudios]").rellenaGridClienteEstudios(clienteGeneral.data.personaId);
                    tabsServiciosCliente.down("[name=panelClienteFotos]").rellenaPanelClientesFotos(clienteGeneral.data.clienteId);

                    tabsServiciosCliente.setActiveTab(0);
                    var tabCliente = tabsServiciosCliente.down("[name=tabCliente]");
                    tabsServiciosCliente.setDisabled(false);
                    tabCliente.setActiveTab(0);
                }
            });
        }
    },

    // rellenaPestanyasClientes: function (clienteGeneralId) {

    // var ventana = this.getView('clientes.VentanaCliente').create({clienteGeneralId: clienteGeneralId});
    //
    // ventana.setLoading(true);
    // if (clienteGeneralId) {
    //     var clienteGeneral = this.getStoreClientesGridStore().findRecord('id', clienteGeneralId);
    //     var tabsServiciosCliente = ventana.down("tabsServiciosCliente");
    //     tabsServiciosCliente.store.load({
    //         url: '/crm/rest/cliente/general/',
    //         params: {
    //             clienteGeneralId: clienteGeneralId
    //         },
    //         scope: this,
    //         callback: function (records) {
    //             tabsServiciosCliente.removeAll();
    //
    //             tabsServiciosCliente.creaPestanyasServicios(records, clienteGeneral);
    //             tabsServiciosCliente.creaPestanyasPersona(clienteGeneral.data.personaId);
    //
    //             tabsServiciosCliente.down("[name=gridClienteVinculacionesUJI]").rellenaGridVinculacionesUJI(clienteGeneral.data.personaId);
    //             tabsServiciosCliente.down("[name=gridClienteCuentasCorporativas]").rellenaGridCuentasCorporativas(clienteGeneral.data.personaId);
    //             tabsServiciosCliente.down("[name=gridClienteCursos]").rellenaGridClienteCursos(clienteGeneral.data.personaId);
    //             tabsServiciosCliente.down("[name=gridClienteEstudios]").rellenaGridClienteEstudios(clienteGeneral.data.personaId);
    //             // tabsServiciosCliente.down("[name=panelClienteRecibos]").down("grid").rellenaPanelClientesPagos(clienteGeneral.data.personaId);
    //             tabsServiciosCliente.down("[name=panelClienteFotos]").rellenaPanelClientesFotos(clienteGeneral.data.clienteId);
    //
    //             tabsServiciosCliente.setActiveTab(0);
    //             var tabCliente = tabsServiciosCliente.down("[name=tabCliente]");
    //             tabsServiciosCliente.setDisabled(false);
    //             tabCliente.setActiveTab(0);
    //             this.getGridClientes().setLoading(false);
    //         }
    //     });
    // }
    //
    // ventana.show();
    // ventana.setLoading(false);
    // },

    //////////////////////////////////////////// Ventana Nuevo Cliente //////////////////////////////////////////////////////////////////

    showVentanaAddCliente: function () {
        this.ventanaCliente = this.getVentanaNuevoClienteCampanya();
        this.ventanaCliente.show();
    },

    closeVentanaAddCliente: function () {
        this.ventanaCliente.destroy();
    },

    limpiarVentanaNewCliente: function () {
        var persona = this.getPersonaModel().create();

        var form = this.getFormNewCliente();
        form.loadRecord(persona);

        // form.down("textfield[name=identificacion]").setDisabled(false);
        // this.setEstadoComponentesVentanaNewCliente(true);
        form.down("combobox[name=tipoIdentificacion]").setValue("");
        form.down("combobox[name=servicio]").setValue("");
        form.down("combobox[name=tipo]").setValue("");
        form.down("combobox[name=nacionalidad]").setValue("");
        form.down("combobox[name=sexoId]").setValue("");


    },

    completaInformacionNuevoCliente: function () {
        var formCliente = this.getFormNewCliente();
        var storePersona = this.getStorePersonasStore();

        formCliente.setLoading(true);
        storePersona.load(
            {
                url: '/crm/rest/persona/identificacion/' + formCliente.getValues().identificacion,
                scope: this,
                callback: function (records, options, success) {
                    if (success) {
                        if (records[0].data.paisPostalId && records[0].data.paisPostalId != null) {
                            formCliente.down("combobox[name=provinciaPostalId]").getStore().reload();
                            if (records[0].data.provinciaPostalId != null) {
                                formCliente.down("combobox[name=poblacionPostalId]").getStore().load({
                                    url: 'rest/poblacion/' + records[0].data.provinciaPostalId,
                                    callback: function () {
                                        formCliente.loadRecord(records[0]);
                                        formCliente.setLoading(false);
                                    }
                                });
                            } else {
                                formCliente.loadRecord(records[0]);
                                formCliente.setLoading(false);
                            }
                        } else {
                            formCliente.loadRecord(records[0]);
                            formCliente.setLoading(false);
                        }
                    } else {
                        Ext.Msg.alert('Cerca de persones', '<b>No s\'ha trobat cap persona amb aquesta identificació</b>');
                        formCliente.setLoading(false);
                    }
                }
            });
    },

    onCloseWindowBusqueda: function () {
        var clienteGeneral = this.getGridClientes().getStore().findRecord('id', this.clienteGeneralSeleccionado);
        this.rellenaPestanyasClientes(this.clienteGeneralSeleccionado, clienteGeneral);
    },
    ///////////////////////////////////// Grid Clientes ///////////////////////////////////////////////////////////////////////

    // dameIdGridClientes: function () {
    //     return this.getGridClientes().clienteGeneralSeleccionado;
    // },

    // dameGridClienteGeneralSeleccionado: function () {
    //     return this.getGridClientes().getStore().findRecord('id', this.getGridClientes().clienteGeneralSeleccionado);
    // },

    dameIdCliente: function () {
        var tabsServiciosCliente = this.getTabsServiciosCliente();
        return tabsServiciosCliente.getActiveTab().itemId;
    },

    dameCliente: function () {
        var tabsServiciosCliente = this.getTabsServiciosCliente();
        return tabsServiciosCliente.getActiveTab().cliente;
    },

    dameStoreTab: function () {
        return this.getTabsServiciosCliente().store;
    },

    dameTabActivo: function () {
        return this.getTabsServiciosCliente().getActiveTab();
    },

    ponerBusqueda: function (identificacion) {

        this.getPanelClientes().down("textfield[name=busquedaDNI]").setValue(identificacion);

    },

    busqueda: function () {
        var MIN_CARACTERES_BUSQUEDA = 3;

        var busqueda = this.getFiltroBusqueda().busqueda(MIN_CARACTERES_BUSQUEDA);

        if (busqueda != null) {
            var clientes = this.getStoreClientesGridStore();

            clientes.getProxy().extraParams = busqueda;
            clientes.load({
                scope: this,
                callback: function () {
                    this.limpiaFormClientes();
                }
            });
        } else {
            Ext.Msg.show({
                title: 'Informaciò',
                msg: 'Has d\'introduir almenys ' + MIN_CARACTERES_BUSQUEDA + ' caràcters',
                width: 300,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    },

    busquedaPorMail: function (mail) {

        this.ventanaBusquedaCliente = this.getVentanaBusquedaCliente();

        var clientes = this.getStoreClientesGridStore();

        clientes.getProxy().extraParams = {
            correo: mail
        };

        clientes.load({
            scope: this,
            callback: function () {
                this.limpiaFormClientes();
            }
        });

        this.ventanaBusquedaCliente.show();
    },

    busquedaPorId: function (id) {
        var clientes = this.getStoreClientesGridStore();

        clientes.load({
            url: '/crm/rest/clientegrid/' + id,
            scope: this,
            callback: function (record) {
                this.getGridClientes().getSelectionModel().select(record);
            }
        });

    },

    limpiaFormClientes: function () {
        this.setEstadoComponentes();
    },

    cargaComboProvincias: function (combo, sel) {

        var tabsServiciosCliente = this.getTabsServiciosCliente();

        var cliente = tabsServiciosCliente.getActiveTab().itemId;
        tabsServiciosCliente.down("[itemId=" + cliente + "]").down("[name=formClientes]").down("combobox[name=provinciaPostalId]").getStore().reload();

        // if (sel[0].data.id === "E") {
        // }

        if (sel[0].data.id !== "E") {
            tabsServiciosCliente.down("[itemId=" + cliente + "]").down("[name=formClientes]").down("combobox[name=provinciaPostalId]").setValue(99);
            tabsServiciosCliente.down("[itemId=" + cliente + "]").down("[name=formClientes]").down("combobox[name=poblacionPostalId]").setValue(null);
            tabsServiciosCliente.down("[itemId=" + cliente + "]").down("[name=formClientes]").down("combobox[name=poblacionPostalId]").getStore().load({
                url: 'rest/poblacion/99'
            });
            //     tabsServiciosCliente.down("[itemId=" + cliente + "]").down("[name=formClientes]").down("combobox[name=provinciaPostalId]").getStore().loadData([]);
            //     tabsServiciosCliente.down("[itemId=" + cliente + "]").down("[name=formClientes]").down("combobox[name=poblacionPostalId]").getStore().loadData([]);
        }
    },

    cargaComboPoblaciones: function (combo, sel) {

        var tabsServiciosCliente = this.getTabsServiciosCliente();

        var cliente = tabsServiciosCliente.getActiveTab().itemId;

        if (sel[0].data == null) {
            tabsServiciosCliente.down("[itemId=" + cliente + "]").down("[name=formClientes]").down("combobox[name=poblacionPostalId]").setValue(null);
            tabsServiciosCliente.down("[itemId=" + cliente + "]").down("[name=formClientes]").down("combobox[name=poblacionPostalId]").getStore().loadData([]);
        } else {
            tabsServiciosCliente.down("[itemId=" + cliente + "]").down("[name=formClientes]").down("combobox[name=poblacionPostalId]").setValue(null);
            tabsServiciosCliente.down("[itemId=" + cliente + "]").down("[name=formClientes]").down("combobox[name=poblacionPostalId]").getStore().load({
                url: 'rest/poblacion/' + sel[0].data.id
            });
        }
    },

    cargaComboProvinciasVentanaAnyadirCliente: function (combo, sel) {

        this.getVentanaNewCliente().down("combobox[name=provinciaPostalId]").getStore().reload();
        if (sel[0].data.id !== "E") {
            this.getVentanaNewCliente().down("combobox[name=provinciaPostalId]").setValue(99);
            this.getVentanaNewCliente().down("combobox[name=poblacionPostalId]").setValue(null);
            this.getVentanaNewCliente().down("combobox[name=poblacionPostalId]").getStore().load({
                url: 'rest/poblacion/99'
            });
        } else {
            this.getVentanaNewCliente().down("combobox[name=poblacionPostalId]").getStore().loadData([]);
            this.getVentanaNewCliente().down("combobox[name=poblacionPostalId]").setValue(null);
        }
    },

    cargaComboPoblacionesVentanaAnyadirCliente: function (combo, sel) {


        if (sel[0].data == null) {
            this.getVentanaNewCliente().down("combobox[name=poblacionPostalId]").setValue(null);
            this.getVentanaNewCliente().down("combobox[name=poblacionPostalId]").getStore().loadData([]);
        } else {
            this.getVentanaNewCliente().down("combobox[name=poblacionPostalId]").setValue(null);
            this.getVentanaNewCliente().down("combobox[name=poblacionPostalId]").getStore().load({
                url: 'rest/poblacion/' + sel[0].data.id
            });
        }
    },

    insertarCliente: function () {
        var ref = this;
        var valoresNuevoCliente = this.getFormNewCliente().getValues();

        if (valoresNuevoCliente.id || valoresNuevoCliente.correo || valoresNuevoCliente.movil) {
            var form = this.getFormNewCliente().getForm();
            if (form.isValid()) {
                form.submit({
                    url: '/crm/rest/cliente/',
                    method: 'POST',
                    params: {
                        nacionalidadId: valoresNuevoCliente.nacionalidadId
                    },
                    success: function () {
                        Ext.Msg.alert('Inserció de clients', '<b>El client ha sigut insertat</b>');
                        ref.limpiaFormClientes();
                        ref.closeVentanaAddCliente();
                    },
                    failure: function () {
                        ref.limpiaFormClientes();
                        ref.closeVentanaAddCliente();
                    }
                });
            }
        } else {
            Ext.Msg.alert('Inserció de clients',
                '<b>El client no ha pogut ser insertat, comprova que les dades són correctes (si vols insertar un client sense identificació fara falta móvil o un eMail)</b>');
        }

    },

    actualizarCliente: function (button) {
        var ref = this,
            storeClientesGrid = this.getStoreClientesGridStore(),
            form = button.up('formClientes').getForm();
        if (form.getValues().correo == '' && form.getValues().movil == '') {
            Ext.Msg.alert('Modificació de clients', '<b>No es pot modificar el client si el eMail o el mòbil estan buits</b>');
            return;
        }
        if (form.isValid()) {
            form.submit({
                url: '/crm/rest/cliente/',
                method: 'PUT',
                params: {
                    clienteId: this.dameIdCliente(),
                    clienteGeneralId: ref.getTabsServiciosCliente().clienteGeneralId
                },
                success: function () {
                    storeClientesGrid.reload();
                }
            });
        }
    },

    eliminarCliente: function (button) {

        var ref = this;
        var clienteId = this.dameIdCliente();

        Ext.Ajax.request({
            url: '/crm/rest/cliente/' + clienteId,
            method: 'DELETE',
            scope: this,
            success: function () {
                ref.getStoreClientesGridStore().reload();
                button.up("[name=tabCliente]").eliminarCliente();
                ref.limpiaFormClientes();
            },
            failure: function () {
            }
        });
    },

    onButtonUploadFile: function (input, value) {
        var clienteId = this.getPanelClientesFotos().clienteId;
        var form = this.getPanelClientesFotos().down('form');
        var me = this;
        if (!clienteId) {
            return;
        }

        form.getForm().submit(
            {
                method: 'POST',
                url: '/crm/rest/cliente/' + clienteId + '/fotos/',
                success: function (formx, action) {
                    form.getForm().reset();
                    me.getPanelClientesFotos().loadFotos();
                }
            });
    },

    descargarFotoCRM: function () {
        var clienteId = this.getPanelClientesFotos().clienteId;
        var url = '/crm/rest/cliente/' + clienteId + '/fotos/descargar';

        var body = Ext.getBody();
        var frame = body.getById('hiddenform-iframe');
        if (frame == null) {
            var frame = body.createChild(
                {
                    tag: 'iframe',
                    cls: 'x-hidden',
                    id: 'hiddenform-iframe',
                    name: 'iframe'
                });
        }
        var form = body.getById('hiddenform-form');
        if (form == null) {
            var form = body.createChild(
                {
                    tag: 'form',
                    cls: 'x-hidden',
                    id: 'hiddenform-form',
                    target: 'iframe'
                });
        }
        form.set(
            {
                action: url
            });
        form.dom.submit();
    },

    validarFotoCRM: function () {
        var me = this;
        var clienteId = me.getPanelClientesFotos().clienteId;
        Ext.Msg.confirm('Validar', '<b>Esteu segur/a de voler validar la foto?</b>', function (btn, text) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: '/crm/rest/cliente/' + clienteId + '/fotos/validar',
                    method: 'POST',
                    jsonData: 'valida',
                    success: function () {
                        me.getPanelClientesFotos().loadFotos();
                    },
                    failure: function () {
                        Ext.Msg.alert('Validar foto', '<b>La foto no ha pogut ser validar correctament</b>');
                    }

                });
            }
        });
    },


    /////////////////////////////////////////  Varios  ///////////////////////////////////////////////////////////////////////////////////////////////////////

    setEstadoComponentes: function () {
        // var tieneSeleccionGridCliente = this.getGridClientes().getSelectionModel().hasSelection();

        // this.getTabsServiciosCliente().setActiveTab(0);
        // this.getTabsServiciosCliente().setDisabled(true);
    }

    // setEstadoComponentesVentanaNewCliente: function (success) {
    //
    //     var formCliente = this.getFormNewCliente();
    //
    //     formCliente.down("textfield[name=nombre]").setDisabled(success);
    //     formCliente.down("textfield[name=apellidos]").setDisabled(success);
    // }
});