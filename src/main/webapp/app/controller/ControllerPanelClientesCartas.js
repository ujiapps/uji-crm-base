Ext.define('CRM.controller.ControllerPanelClientesCartas',
    {
        extend: 'Ext.app.Controller',
        // stores: ['StoreClienteCartas'],
        views: ['clientes.PestanyaCartas.GridClientesCartas'],
        // models: [],

        // refs: [{
        //     selector: 'gridClientesCartas',
        //     ref: 'gridClientesCartas'
        // }],

        dirty: false,
        init: function () {
            this.control(
                {
                    'gridClientesCartas button[name=imprimirCartaPdf]': {
                        click: this.imprimirClienteCartaPdf
                    }
                });
        },

        imprimirClienteCartaPdf: function () {

            var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
            var clienteId = tabsServiciosCliente.getActiveTab().itemId;
            var grid = tabsServiciosCliente.down("[itemId=" + clienteId + "]").down("[itemId=gridCartas-" + clienteId + "]");

            var clienteCartaId = this.getController('ControllerVarios').dameIdGrid(grid);
            window.open("/crm/rest/clientecarta/" + clienteCartaId + "/pdf");

        },

        // rellenaPanelClientesCartas: function (clienteId) {
        //     var tabsServiciosCliente = this.getController("ControllerPanelClientes").getTabsServiciosCliente();
        //     var store = tabsServiciosCliente.down("[itemId=" + clienteId + "]").down("[itemId=gridCartas-" + clienteId + "]").getStore();
        //     // var store = this.getStoreClienteCartasStore();
        //     if (clienteId) {
        //         store.load({
        //             url: 'rest/clientecarta/cliente',
        //             params: {
        //                 clienteId: clienteId
        //             }
        //         });
        //     }
        //     else {
        //         store.loadData([], false);
        //     }
        //
        // }
    });