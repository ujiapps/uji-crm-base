Ext.define('CRM.controller.ControllerPanelEnviosSeguimientos',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreEnviosSeguimientos' ],
    views : [ 'enviosSeguimientos.PanelEnviosSeguimientos', 'envios.PanelEnvios', 'seguimientos.PanelSeguimientos', 'seguimientos.GridCampanyaSeguimientos' ],
    refs : [
    {
        selector : 'gridEnviosSeguimientos',
        ref : 'gridEnviosSeguimientos'
    },
    {
        selector : 'applicationViewportCrm',
        ref : 'application'
    } ],

    dirty : false,
    init : function()
    {
        this.control(
        {
            'gridEnviosSeguimientos' :
            {
                itemdblclick : function()
                {
                    this.abreEnvios();
                }
            },

            'gridEnviosSeguimientos button[name=seguimientoEnvioNuevo]' :
            {
                click : function()
                {
                    this.abreEnviosCampanya();
                }
            }

        })
    },

    abreEnvios : function()
    {
        eval('this.getApplication().tabPanel.addTab(Ext.create("' + this.getApplication().codigoAplicacion + '.view.envios.PanelEnvios", {}));');
    },

    abreEnviosCampanya : function()
    {
        this.getGridEnviosSeguimientos().getSelectionModel().deselectAll();
        eval('this.getApplication().tabPanel.addTab(Ext.create("' + this.getApplication().codigoAplicacion + '.view.envios.PanelEnvios", {}));');
    }
});