Ext.define('CRM.controller.ControllerPanelEstudios', {
    extend: 'Ext.app.Controller',
    views: ['estudios.VentanaAddItemAsociado', 'estudios.GridItemsAsociados', 'estudios.GridClasificacionEstudiosUJI', 'estudios.GridClasificacionEstudiosNoUJI'],

    ventanaAddItemAsociado: {},

    getVentanaAddItemAsociado: function (clasificacionId, tipo) {
        var grid;
        if (tipo == 'UJI') {
            grid = this.getGridItemsAsociados();
        } else {
            grid = this.getGridItemsAsociadosNoUJI();
        }
        return this.getView('estudios.VentanaAddItemAsociado').create({
            clasificacionId: clasificacionId,
            grid: grid
        });
    },

    refs: [{
        selector: 'gridClasificacionEstudiosUJI',
        ref: 'gridClasificacionEstudiosUJI'
    }, {
        selector: 'gridClasificacionEstudiosNoUJI',
        ref: 'gridClasificacionEstudiosNoUJI'
    }, {
        selector: 'gridItemsAsociados[name=gridItemsAsociados]',
        ref: 'gridItemsAsociados'
    }, {
        selector: 'gridItemsAsociados[name=gridItemsAsociadosNoUJI]',
        ref: 'gridItemsAsociadosNoUJI'
    }],

    dirty: false,
    init: function () {
        this.control({
            'gridItemsAsociados button[name=anyadirItemAsociado]': {
                click: function (button) {
                    this.showVentanaAddItemAsociado(button);
                }
            }
        })
    },

    showVentanaAddItemAsociado: function (button) {
        var sel;

        if (button.up("grid").tipo == "UJI") {
            sel = this.getGridClasificacionEstudiosUJI().getSelectionModel().getSelection()[0];
        } else {
            sel = this.getGridClasificacionEstudiosNoUJI().getSelectionModel().getSelection()[0];
        }
        this.ventanaAddItemAsociado = this.getVentanaAddItemAsociado(sel.data.id, button.up("grid").tipo);
        this.ventanaAddItemAsociado.show();
    }
})
;