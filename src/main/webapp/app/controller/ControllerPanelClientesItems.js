Ext.define('CRM.controller.ControllerPanelClientesItems',
    {
        extend: 'Ext.app.Controller',
        views: ['clientes.GridClienteItems', 'clientes.VentanaAddClienteItem'],

        ventanaAddClienteItem: {},

        getVentanaAddClienteItem: function (clienteId) {
            var grid = this.getGridClienteItems();

            return this.getView('clientes.VentanaAddClienteItem').create({
                clienteId: clienteId,
                grid: grid
            });
        },
        refs: [{
            selector: 'gridClienteItems',
            ref: 'gridClienteItems'
        }],

        dirty: false,
        init: function () {

            this.control(
                {
                    'gridClienteItems button[name=anyadirClienteItem]': {
                        click: this.showVentanaAddClienteItem
                    }
                });
        },

        showVentanaAddClienteItem: function () {
            var clienteId = this.getController("ControllerPanelClientes").dameIdCliente();

            this.ventanaAddClienteItem = this.getVentanaAddClienteItem(clienteId);
            this.ventanaAddClienteItem.show();
        }

    });