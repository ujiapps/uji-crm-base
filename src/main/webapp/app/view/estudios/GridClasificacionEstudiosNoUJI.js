Ext.define('CRM.view.estudios.GridClasificacionEstudiosNoUJI', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridClasificacionEstudiosNoUJI',
    name: 'gridClasificacionEstudioNoUJI',
    requires: ['Ext.grid.plugin.RowEditing'],
    plugins: [{
        ptype: 'rowediting',
        clicksToEdit: 2
    }],

    listeners: {
        selectionchange: function () {
            this.setEstadoComponentes();
        },
        edit: function () {
            this.guardaClasificacionEstudioNoUJI();
        }
    },
    frame: true,
    labelAlign: 'top',
    autoScroll: true,
    store: 'StoreClasificacionesEstudioNoUJI',
    tbar: [{
        xtype: 'button',
        name: 'anyadirEstudioNoUJI',
        text: 'Afegir',
        iconCls: 'application-add',
        listeners: {
            click: function () {
                var store = this.up("grid[name=gridClasificacionEstudioNoUJI]").getStore();
                var clasificacion = store.getProxy().getModel().create({});
                var rowEditor = this.up("grid[name=gridClasificacionEstudioNoUJI]").getPlugin();

                rowEditor.cancelEdit();
                store.insert(0, clasificacion);
                rowEditor.startEdit(0, 0);
            }
        }
    }, {
        xtype: 'button',
        name: 'borrarEstudioNoUJI',
        text: 'Esborrar',
        iconCls: 'application-delete',
        disabled: true,
        listeners: {
            click: function () {
                var ref = this;
                var grid = this.up("grid[name=gridClasificacionEstudioNoUJI]");
                var store = grid.getStore();
                var selection = grid.getSelectionModel().getSelection();
                var clasificacionId = (selection.length > 0) ? selection[0].data.id : null;
                var clasificacion = store.findRecord('id', clasificacionId);

                if (clasificacion) {
                    Ext.Msg.confirm('Esborrar Tipus de dada', 'Esteu segur/a de voler esborrar el registre sel·leccionat?', function (btn) {
                        if (btn == 'yes') {
                            store.remove(clasificacion);
                            store.sync(
                                {
                                    success: function () {
                                        grid.setEstadoComponentes();
                                    },
                                    failure: function () {
                                        Ext.Msg.alert('Esborrar', 'No s`ha pogut esborrar');
                                        store.rejectChanges();
                                        grid.setEstadoComponentes();
                                    }
                                });
                        }
                    });
                }
            }
        }
    }, '->', {
        xtype: 'combobox',
        name: 'numItemsAsociados',
        store: {
            proxy: {
                type: 'ajax',
                reader: {
                    type: 'array'
                }
            },
            fields: ['id', 'nombre'],
            data: [{
                id: 2,
                nombre: 'Sense items asociats'
            }, {
                id: 1,
                nombre: 'Amb items asociats'
            }]
        },
        queryMode: 'local',
        displayField: 'nombre',
        fieldLabel: 'Items',
        valueField: 'id',
        editable: false,
        trigger2Cls: 'x-form-clear-trigger',
        onTrigger2Click: function (event) {
            var me = this;
            me.clearValue();
        },
        listeners: {
            change: function (elem, newVal) {
                var grid = this.up("grid[name=gridClasificacionEstudioNoUJI]");
                var filtroTipoEstudio = this.up("grid[name=gridClasificacionEstudioNoUJI]").down("combobox[name=clasificacionTipo]").getValue();
                var store = grid.getStore();
                if (newVal) {
                    store.filterBy(function (record) {
                        if (filtroTipoEstudio) {
                            return record.data.tieneItems == newVal && record.data.tipo == filtroTipoEstudio;
                        } else {
                            return record.data.tieneItems == newVal;
                        }
                    })
                } else {
                    if (filtroTipoEstudio) {
                        store.filterBy(function (record) {
                            return record.data.tipo == filtroTipoEstudio;
                        })
                    } else {
                        store.clearFilter();
                    }
                }
            }
        }
    }, {
        xtype: 'combobox',
        name: 'clasificacionTipo',
        store: Ext.create('CRM.store.StoreTiposEstudiosNoUJI'),
        displayField: 'nombre',
        fieldLabel: 'Tipus estudi',
        valueField: 'id',
        editable: false,
        trigger2Cls: 'x-form-clear-trigger',
        onTrigger2Click: function (event) {
            var me = this;
            me.clearValue();
        },
        listeners: {
            change: function (elem, newVal) {
                var grid = this.up("grid[name=gridClasificacionEstudioNoUJI]");
                var filtroTieneItems = this.up("grid[name=gridClasificacionEstudioNoUJI]").down("combobox[name=numItemsAsociados]").getValue();
                var store = grid.getStore();
                if (newVal) {
                    store.filterBy(function (record) {

                        if (filtroTieneItems) {
                            return record.data.tipo == newVal && record.data.tieneItems == filtroTieneItems;
                        } else {
                            return record.data.tipo == newVal;
                        }
                    })
                } else {
                    if (filtroTieneItems) {
                        store.filterBy(function (record) {
                            return record.data.tieneItems == filtroTieneItems;
                        })
                    } else {
                        store.clearFilter();
                    }
                }
            }
        }
    }, {
        xtype: 'button',
        iconCls: 'x-tbar-loading',
        listeners: {
            click: function () {
                this.up("grid[name=gridClasificacionEstudioNoUJI]").down("combobox[name=numItemsAsociados]").clearValue();
                this.up("grid[name=gridClasificacionEstudioNoUJI]").down("combobox[name=clasificacionTipo]").clearValue();
                this.up("grid[name=gridClasificacionEstudioNoUJI]").getStore().load();
            }
        }

    }],

    columns: [{
        text: 'Nom',
        dataIndex: 'nombreCa',
        editor: {allowBlank: false},
        flex: 2
    }, {
        text: 'Nombre',
        dataIndex: 'nombreEs',
        editor: {allowBlank: false},
        flex: 2
    }, {
        text: 'Name',
        dataIndex: 'nombreUk',
        editor: {allowBlank: false},
        flex: 2
    }, {
        header: 'Tipus Estudi',
        dataIndex: 'tipo',
        flex: 1,
        xtype: 'combocolumn',
        combo: {
            xtype: 'combobox',
            editable: false,
            allowBlank: false,
            store: Ext.create('CRM.store.StoreTiposEstudiosNoUJI'),
            displayField: 'nombre',
            valueField: 'id',
            name: 'tipoEstudio',
            queryMode: 'local'
        }
    }],

    setEstadoComponentes: function () {
        var tieneSeleccionGrid = this.getSelectionModel().hasSelection();
        this.down("button[name=borrarEstudioNoUJI]").setDisabled(!tieneSeleccionGrid);
    },

    guardaClasificacionEstudioNoUJI: function () {
        var store = this.getStore();
        store.sync(
            {
                success: function () {
                    this.setEstadoComponentes();
                },
                failure: function () {
                    Ext.Msg.alert('Dades', 'S\'ha produït un error en modificar la dada');
                    store.rejectChanges();
                    this.setEstadoComponentes();
                },
                scope: this
            });
    }
});