Ext.define('CRM.view.estudios.GridClasificacionEstudiosUJI', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridClasificacionEstudiosUJI',
    name: 'gridClasificacionEstudioUJI',

    listeners: {},

    frame: true,
    labelAlign: 'top',
    autoScroll: true,
    store: 'StoreClasificacionesEstudio',
    tbar: ['->', {
        xtype: 'combobox',
        name: 'numItemsAsociados',
        store: {
            proxy: {
                type: 'ajax',
                reader: {
                    type: 'array'
                }
            },
            fields: ['id', 'nombre'],
            data: [{
                id: 2,
                nombre: 'Sense items asociats'
            }, {
                id: 1,
                nombre: 'Amb items asociats'
            }]
        },
        queryMode: 'local',
        displayField: 'nombre',
        fieldLabel: 'Items',
        valueField: 'id',
        editable: false,
        trigger2Cls: 'x-form-clear-trigger',
        onTrigger2Click: function (event) {
            var me = this;
            me.clearValue();
        },
        listeners: {
            change: function (elem, newVal) {
                var grid = this.up("grid[name=gridClasificacionEstudioUJI]");
                var filtroTipoEstudio = this.up("grid[name=gridClasificacionEstudioUJI]").down("combobox[name=clasificacionTipo]").getValue();
                var store = grid.getStore();
                if (newVal) {
                    store.filterBy(function (record) {
                        if (filtroTipoEstudio) {
                            return record.data.tieneItems == newVal && record.data.tipo == filtroTipoEstudio;
                        } else {
                            return record.data.tieneItems == newVal;
                        }
                    })
                } else {
                    if (filtroTipoEstudio) {
                        store.filterBy(function (record) {
                            return record.data.tipo == filtroTipoEstudio;
                        })
                    } else {
                        store.clearFilter();
                    }
                }
            }
        }
    }, {
        xtype: 'combobox',
        name: 'clasificacionTipo',
        store: Ext.create('CRM.store.StoreTiposEstudiosUJI'),
        displayField: 'nombre',
        fieldLabel: 'Tipus estudi',
        valueField: 'id',
        editable: false,
        trigger2Cls: 'x-form-clear-trigger',
        onTrigger2Click: function (event) {
            var me = this;
            me.clearValue();
        },
        listeners: {
            change: function (elem, newVal) {
                var grid = this.up("grid[name=gridClasificacionEstudioUJI]");
                var filtroTieneItems = this.up("grid[name=gridClasificacionEstudioUJI]").down("combobox[name=numItemsAsociados]").getValue();
                var store = grid.getStore();
                if (newVal) {
                    store.filterBy(function (record) {

                        if (filtroTieneItems) {
                            return record.data.tipo == newVal && record.data.tieneItems == filtroTieneItems;
                        } else {
                            return record.data.tipo == newVal;
                        }
                    })
                } else {
                    if (filtroTieneItems) {
                        store.filterBy(function (record) {
                            return record.data.tieneItems == filtroTieneItems;
                        })
                    } else {
                        store.clearFilter();
                    }
                }
            }
        }
    },],

    columns: [{
        text: 'Nom',
        dataIndex: 'nombre',
        flex: 2
    }, {
        header: 'Tipus Estudi',
        dataIndex: 'tipo',
        flex: 1,
        xtype: 'combocolumn',
        combo: {
            xtype: 'combobox',
            editable: false,
            allowBlank: false,
            store: Ext.create('CRM.store.StoreTiposEstudiosUJI'),
            displayField: 'nombre',
            valueField: 'id',
            name: 'tipoEstudio',
            queryMode: 'local'
        }
    }]
});