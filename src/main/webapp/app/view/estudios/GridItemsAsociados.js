Ext.define('CRM.view.estudios.GridItemsAsociados', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridItemsAsociados',
    frame: true,
    labelAlign: 'top',
    autoScroll: true,

    listeners: {
        selectionchange: function () {
            this.setEstadoComponentes();
        },
        edit: function () {
            this.guardaItems();
        }
    },

    tbar: [{
        xtype: 'button',
        name: 'anyadirItemAsociado',
        text: 'Afegir',
        iconCls: 'application-add',
    }, {
        xtype: 'button',
        name: 'borrarItemAsociado',
        text: 'Esborrar',
        iconCls: 'application-delete',
        disabled: true,
        listeners: {
            click: function () {
                var ref = this;
                var grid = this.up("grid");
                var store = grid.getStore();
                var selection = grid.getSelectionModel().getSelection();
                var itemId = (selection.length > 0) ? selection[0].data.id : null;
                var item = store.findRecord('id', itemId);

                if (item) {
                    Ext.Msg.confirm('Esborrar Tipus de dada', 'Esteu segur/a de voler esborrar el registre sel·leccionat?', function (btn) {
                        if (btn == 'yes') {
                            store.remove(item);
                            store.sync(
                                {
                                    success: function () {
                                        grid.setEstadoComponentes();
                                    },
                                    failure: function () {
                                        Ext.Msg.alert('Esborrar', 'No s`ha pogut esborrar');
                                        store.rejectChanges();
                                        grid.setEstadoComponentes();
                                    }
                                });
                        }
                    });
                }
            }
        }
    }],
    columns: [{
        header: 'Grup',
        xtype: 'combocolumn',
        dataIndex: 'grupoId',
        flex: 1,
        combo: {
            xtype: 'combobox',
            store: Ext.create('CRM.store.StoreGrupos'),
            displayField: 'nombreEs',
            valueField: 'id',
            editable: false,
            queryMode: 'local'
        }
    }, {
        text: 'Nom',
        dataIndex: 'itemNombre',
        flex: 4,
    }],

    setEstadoComponentes: function () {
        var tieneSeleccionGrid = this.getSelectionModel().hasSelection();
        this.down("button[name=borrarItemAsociado]").setDisabled(!tieneSeleccionGrid);
    }
});