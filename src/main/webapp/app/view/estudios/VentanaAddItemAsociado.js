Ext.define('CRM.view.estudios.VentanaAddItemAsociado', {
    extend: 'Ext.Window',
    // extend: 'Ext.form.FormPanel',
    alias: 'widget.ventanaAddItemAsociado',
    name: 'ventanaAddItemAsociado',
    modal: true,
    width: 400,
    title: 'Gestió Items asociats a Clasificacións',
    frame: true,
    border: 0,
    padding: 10,

    fbar: [{
        xtype: 'button',
        text: 'Guardar',
        listeners: {
            click: function () {
                var form = this.up("window").down("form[name=formClasificacionItem]");
                var clasificacionId = this.up("window").clasificacionId;
                if (form.getForm().isValid()) {
                    form.submit({
                        url: '/crm/rest/itemclasificacion/clasificacion/' + clasificacionId,
                        scope: this,
                        success: function () {
                            this.up("window").grid.getStore().load({
                                url: '/crm/rest/itemclasificacion/clasificacion/' + clasificacionId
                            });
                            this.up("window").destroy();
                        }

                    });
                }
            }
        }
    },],

    items: [{
        xtype: 'form',
        frame: true,
        name: 'formClasificacionItem',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'combobox',
            store: Ext.create('CRM.store.StoreProgramas'),
            displayField: 'nombre',
            valueField: 'id',
            name: 'programaId',
            fieldLabel: 'Programa',
            editable: false,
            allowBlank: false,
            queryMode: 'local',
            listeners: {
                select: function (combo, elem) {
                    var form = this.up();
                    // form.setLoading(true);
                    form.down("combobox[name=grupoId]").setValue(null);
                    form.down("combobox[name=itemId]").setValue(null);
                    form.down("combobox[name=grupoId]").getStore().filterBy(function (record) {
                        return record.data.programaId == elem[0].data.id;
                    });
                    form.down("combobox[name=itemId]").getStore().loadData([], false);
                }
            }
        }, {
            xtype: 'combobox',
            store: Ext.create('CRM.store.StoreGrupos', {
                // autoLoad: false
            }),
            displayField: 'nombre',
            valueField: 'id',
            name: 'grupoId',
            fieldLabel: 'Grup',
            editable: false,
            allowBlank: false,
            queryMode: 'local',
            listeners: {
                select: function (combo, elem) {
                    var form = this.up();
                    form.setLoading(true);
                    form.down("combobox[name=itemId]").setValue(null);
                    form.down("combobox[name=itemId]").getStore().load({
                        params: {
                            grupoId: elem[0].data.id
                        },
                        callback: function () {
                            form.setLoading(false);
                        }
                    })
                }
            }
        }, {
            xtype: 'combobox',
            store: Ext.create('CRM.store.StoreItems'),
            displayField: 'nombre',
            valueField: 'id',
            editable: false,
            allowBlank: false,
            fieldLabel: 'Item',
            queryMode: 'local',
            name: 'itemId'
        }]
    }]
})
;