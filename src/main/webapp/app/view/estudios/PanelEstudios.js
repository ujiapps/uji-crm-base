Ext.define('CRM.view.estudios.PanelEstudios',
    {
        extend: 'Ext.tab.Panel',
        title: 'Estudis',
        alias: 'widget.panelEstudios',
        // requires: ['CRM.view.estudios.PanelTitulacionesGrados', 'CRM.view.estudios.PanelTitulacionesPostGrados', 'CRM.view.estudios.PanelTitulacionesDoctorados',
        requires: ['CRM.view.estudios.GridClasificacionEstudiosUJI', 'CRM.view.estudios.GridClasificacionEstudiosNoUJI', 'CRM.view.estudios.GridTitulaciones',
            'CRM.view.estudios.GridItemsAsociados'],
        closable: true,
        layout: {
            type: 'border',
            align: 'stretch'
        },
        items: [{
            xtype: 'panel',
            title: 'Estudis UJI',
            name: 'panelTitulaciones',
            layout: 'border',
            items: [{
                xtype: 'gridClasificacionEstudiosUJI',
                region: 'north',
                flex: 1,
                listeners: {
                    select: function (model, elem) {
                        this.up("panel[name=panelTitulaciones]").down("tabpanel[name=tabTitulaciones]").setDisabled(false);
                        var store = this.up("panel[name=panelTitulaciones]").down("grid[name=gridTitulaciones]").getStore();
                        store.getProxy().url = '/crm/rest/clasificacionestudio/' + elem.data.id + '/estudioUJI/';
                        store.load();

                        var store = this.up("panel[name=panelTitulaciones]").down("grid[name=gridItemsAsociados]").getStore();
                        // store.getProxy().url = '/crm/rest/itemclasificacion/clasificacion/' + elem.data.id;
                        store.load({
                            url: '/crm/rest/itemclasificacion/clasificacion/' + elem.data.id
                        });
                    }
                }
            }, {
                xtype: 'tabpanel',
                name: 'tabTitulaciones',
                region: 'center',
                disabled: true,
                flex: 1,
                items: [{
                    xtype: 'gridTitulaciones',
                    title: 'Titulacions',
                    name: 'gridTitulaciones',
                }, {
                    xtype: 'gridItemsAsociados',
                    title: 'Items',
                    name: 'gridItemsAsociados',
                    tipo : 'UJI',
                    store: Ext.create('CRM.store.StoreItemsEstudios', {itemId: 'storeItemsEstudiosUJI'}),
                }]
            }]
        }, {
            xtype: 'panel',
            name: 'panelTitulacionesNoUJI',
            title: 'Estudis no UJI',
            layout: 'border',
            items: [{
                xtype: 'gridClasificacionEstudiosNoUJI',
                region: 'north',
                flex: 1,
                listeners: {
                    select: function (model, elem) {
                        this.up("panel[name=panelTitulacionesNoUJI]").down("grid[name=gridItemsAsociadosNoUJI]").setDisabled(false);
                        var store = this.up("panel[name=panelTitulacionesNoUJI]").down("grid[name=gridItemsAsociadosNoUJI]").getStore();
                        store.load({
                            url: '/crm/rest/itemclasificacion/clasificacion/' + elem.data.id
                        });
                    }
                }
            }, {
                xtype: 'gridItemsAsociados',
                title: 'Items',
                name: 'gridItemsAsociadosNoUJI',
                region: 'center',
                disabled: true,
                flex: 1,
                tipo : 'NOUJI',
                store: Ext.create('CRM.store.StoreItemsEstudios', {itemId: 'storeItemsEstudiosNoUJI'}),

            }]
        }]
    });