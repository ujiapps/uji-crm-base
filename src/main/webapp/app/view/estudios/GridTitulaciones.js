Ext.define('CRM.view.estudios.GridTitulaciones', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridTitulaciones',
    frame: true,
    labelAlign: 'top',
    autoScroll: true,
    store: Ext.create('CRM.store.StoreEstudioUJI'),
    columns: [{
        text: 'Nombre',
        dataIndex: 'nombre',
        flex: 4
    }]
});