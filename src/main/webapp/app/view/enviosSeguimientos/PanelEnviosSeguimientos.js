Ext.define('CRM.view.enviosSeguimientos.PanelEnviosSeguimientos',
{
    extend : 'Ext.panel.Panel',
    title : 'Envios',
    alias : 'widget.panelEnviosSeguimientos',
    requires : [ 'CRM.view.enviosSeguimientos.TreeProgramasSeguimientos' ],
    closable : true,

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xType : 'container',
        layout : 'hbox',
        minHeight : 550,
        items : [

        {
            xtype : 'treeProgramasSeguimientos',
            flex : 1,
            height : 550
        },
        {

            xtype : 'tabEnvios',
            minHeight : 550,
            flex : 3
        } ]
    } ]

});