Ext.define('CRM.view.enviosSeguimientos.TreeProgramasSeguimientos',
{
    extend : 'Ext.tree.Panel',
    title : 'Menu',
    alias : 'widget.treeProgramasSeguimientos',
    closable : false,
    rootVisible : false,
    animate : false,
    store : 'StoreProgramasEnviosSeguimientosTree'
});