Ext.define('CRM.view.seguimientos.ComboCampanyas',
{
    extend : 'Ext.form.field.ComboBox',
    alias : 'widget.comboCampanyas',

    constructor : function(args)
    {
        var me = this;
        var groupField = "programaId";
        var groupDisplayField = "programaNombre";
        var displayField = "nombre";

        args.tpl = new Ext.XTemplate('<tpl for=".">', '<tpl if="this.' + groupField + ' != values.' + groupField + '">', '<tpl exec="this.' + groupField + ' = values.' + groupField + '"></tpl>',
                '<div class="x-panel-header-default x-panel-header-text-container x-panel-header-text x-panel-header-text-default" title="{' + groupDisplayField + '}">{' + groupDisplayField
                    + '}</div>', '</tpl>', '<div class="x-boundlist-item">{' + displayField + '}</div>', '</tpl>');
        me.callParent(arguments);
    }
});