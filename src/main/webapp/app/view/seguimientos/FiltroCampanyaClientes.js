Ext.define('CRM.view.seguimientos.FiltroCampanyaClientes',
    {
        extend: 'Ext.panel.Panel',
        alias: 'widget.filtroCampanyaClientes',
        frame: true,
        border: 0,
        padding: 5,
        closable: false,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        fbar: [
            {
                xtype: 'button',
                text: 'Netejar Filtres',
                name: 'limpiarFiltros'
            },
            {
                xtype: 'button',
                name: 'buscar',
                icon: '//static.uji.es/js/extjs/uji-commons-extjs/img/find.png',
                maxWidth: 23
            }],

        items: [
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                padding: '5 0 0 0',
                border: 0,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Cerca',
                        name: 'busqueda',
                        margin: '0 20 0 0',
                        labelWidth: 50,
                        flex: 2
                    },
                    {
                        xtype: 'combobox',
                        store: 'StoreEtiquetasSeguimiento',
                        fieldLabel: 'Etiqueta',
                        labelWidth: 60,
                        emptyText: 'Escriu la etiqueta...',
                        displayField: 'nombre',
                        valueField: 'id',
                        name: 'comboEtiqueta',
                        queryMode: 'local',
                        editable: true,
                        hideTrigger: true,
                        margin: '0 10 0 0',
                        enableKeyEvents: true,
                        flex: 1
                    },
                    {
                        border: 0,
                        xtype: 'combobox',
                        editable: false,
                        margin: '0 10 0 0',
                        fieldLabel: 'Tipus',
                        labelWidth: 50,
                        emptyText: 'Trieu...',
                        store: Ext.create('CRM.store.StoreTiposEstadoCampanya', {storeId: 'StoreTiposEstadoCampanyaSeguimientos'}),
                        displayField: 'nombre',
                        valueField: 'id',
                        name: 'campanyaClienteTipoId',
                        flex: 1,
                        queryMode: 'local'
                    }]
            },
            {
                title: 'Opcions avançades de busqueda',
                xtype: 'filtroCampanyaClientesAvanzado',
                collapsible: true,
                collapsed: true,
                animCollapse: true,
                margin: '5 0 0 0',
                border: 0,
                collapseDirection: 'bottom'
            }]

    });