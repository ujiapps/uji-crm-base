Ext.define('CRM.view.seguimientos.PanelCampanyaSeguimientos',
{
    extend : 'Ext.panel.Panel',
    title : 'Serveis',
    requires : [ 'CRM.view.seguimientos.GridCampanyaSeguimientos', 'CRM.view.seguimientos.FiltroCampanyaFases', 'CRM.view.seguimientos.TabCampanyaSeguimientos' ],
    alias : 'widget.panelCampanyaSeguimientos',
    closable : true,
    border: 0,

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'filtroCampanyaFases',
        height : 45
    },
    {
        xtype : 'gridCampanyaSeguimientos',
        flex : 1
    },
    {
        xtype : 'tabCampanyaSeguimientos',
        flex : 1
    } ]

});