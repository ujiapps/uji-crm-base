Ext.define('CRM.view.seguimientos.anyadirACampanya.VentanaAnyadirClientesACampanyaCsv',
    {
        extend: 'Ext.Window',
        alias: 'widget.anyadirACampanyaPanelClientesCsv',
        closable: true,
        width: 600,
        border: 0,
        required: ['Ext.grid.Panel'],
        resizable: false,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },


        items: [{
            xtype: 'form',
            padding: 5,
            frame: true,
            name: 'formAnyadirFicheroCsv',
            fbar: [{
                xtype: 'button',
                text: 'afegir',
                name: 'afegirDesdeVentana',
                listeners: {
                    click: function () {
                        var window = this.up("window");
                        window.setLoading(true);
                        var correoPersonal = window.down("[name=correoPersonal]").getValue();
                        var nacimiento = window.down("[name=fechaNacimiento]").getValue();
                        var direccionPostal = window.down("[name=direccionPostal]").getValue();
                        var nacionalidad = window.down("[name=nacionalidad]").getValue();
                        var uji = window.down("[name=uji]").getValue();
                        var campanya = window.campanya;
                        var estado = window.down("[name=estadoDestinoId]").getValue();

                        Ext.Ajax.request({
                            url: '/crm/rest/fichero/persona/',
                            method: 'POST',
                            params: {
                                correoPersonal: correoPersonal,
                                nacimiento: nacimiento,
                                uji: uji,
                                campanya: campanya,
                                direccionPostal: direccionPostal,
                                nacionalidad: nacionalidad,
                                estado: estado
                            },
                            scope: this,
                            success: function () {
                                window.setLoading(false);
                                window.gridCampanyaClientes.getStore().load();
                                window.destroy();
                            },
                            failure : function(){
                                window.setLoading(false);
                            }
                        });
                    }
                }
            }],
            items: [{
                xtype: 'grid',
                name: 'gridUsuariosFicheroCsv',
                autoScroll: true,
                dockedItems: [
                    {
                        xtype: 'pagingtoolbar',
                        store: 'StorePersonasFicheroCsv',
                        dock: 'bottom',
                        displayInfo: true
                    }],
                height: 400,
                tbar: [{
                    xtype: 'filefield',
                    fieldLabel: 'Fitxer',
                    labelWidth: 50,
                    name: 'anyadirFicheroCsv',
                    buttonConfig: {
                        text: 'Sel·lecciona...',
                        disabled: false
                    },
                    regex: /^.*\.(csv|CSV)$/,
                    regexText: 'Solo CSV',
                    flex: 3
                }, {
                    xtype: 'button',
                    name: 'buttonAnyadirFicheroCsv',
                    iconCls: 'accept',
                    text: 'Carregar',
                    flex: 1,
                    margin: '0 0 0 5',
                    listeners: {
                        click: function () {
                            var storeGrid = this.up("window").down("[name=gridUsuariosFicheroCsv]").getStore();
                            var campanya = this.up("window").campanya;
                            this.up("[name=formAnyadirFicheroCsv]").submit({
                                url: '/crm/rest/fichero/persona/campanya/' + campanya,
                                scope: this,
                                success: function (obj, res, data) {
                                    storeGrid.loadData(res.result.data);
                                },
                                failure: function () {
                                    Ext.Msg.alert('Insercció de fitxers', 'S\'ha produït un error al cargar les dades, comprova que el document es correcte');
                                }
                            });
                        }
                    }
                }

                ],
                columns: [{
                    text: 'Identificació',
                    dataIndex: 'identificacion',
                    flex: 1
                }, {
                    text: 'Nom',
                    dataIndex: 'apellidosNombre',
                    flex: 1
                }, {
                    dataIndex: 'correo',
                    text: 'Correu',
                    flex: 1
                }],
                store: 'StorePersonasFicheroCsv'
            }, {
                xtype: 'combobox',
                editable: false,
                labelWidth: 150,
                fieldLabel: 'Estat destí a insertar',
                emptyText: 'Trieu...',
                store: 'StoreTiposEstadoCampanyaSeguimientos',
                displayField: 'nombre',
                valueField: 'id',
                name: 'estadoDestinoId',
                queryMode: 'local'
            }, {
                xtype: 'fieldset',
                title: 'Selec·ciona d\'aquestes camps cuals vols que se inclouen a la ficha del usuari',
                items: [{
                    xtype: 'checkbox',
                    name: 'correoPersonal',
                    labelWidth: 150,
                    fieldLabel: 'Correu electrònic'
                },{
                    xtype: 'checkbox',
                    name: 'fechaNacimiento',
                    labelWidth: 150,
                    fieldLabel: 'Data Naixement'
                },{
                    xtype: 'checkbox',
                    fieldLabel: 'Nacionalidad',
                    labelWidth: 150,
                    name: 'nacionalidad'
                }, {
                    xtype: 'checkbox',
                    fieldLabel: 'Dirección postal',
                    labelWidth: 150,
                    name: 'direccionPostal'

                }, {
                    xtype: 'checkbox',
                    fieldLabel: 'Vincular usuari UJI',
                    labelWidth: 150,
                    name: 'uji',
                    checked: true

                }]
            }]
        }]
    });