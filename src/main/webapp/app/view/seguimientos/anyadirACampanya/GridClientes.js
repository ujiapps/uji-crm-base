Ext.define('CRM.view.seguimientos.anyadirACampanya.GridClientes',
{
    extend : 'Ext.grid.Panel',
    title : 'Llistat de Clients',
    alias : 'widget.anyadirACampanyaGridClientes',
    store : 'StoreClientesAnyadirACampanya',
    layout : 'fit',
    name : 'anyadirCC',
    requires : [ 'CRM.model.enums.TiposCliente' ],
    dockedItems : [
    {
        xtype : 'pagingtoolbar',
        store : 'StoreClientesAnyadirACampanya',
        dock : 'bottom',
        displayInfo : true
    } ],

    sortableColumns : true,
    selModel :
    {
        mode : 'SINGLE'
    },

    columns : [
    {
        text : 'Identificació',
        dataIndex : 'identificacion',
        flex : 1
    },
    {
        text : 'Nom',
        dataIndex : 'nombreCompleto',
        flex : 2,
        renderer : function(value, metadata, record)
        {
            return (record.get('tipo') == CRM.model.enums.TiposCliente.FISICA.id) ? record.get('nombreCompleto') : record.get('nombreComercial');
        }
    },
    {
        text : 'Tipus',
        dataIndex : 'tipo',
        flex : 1,
        renderer : function(value)
        {
            var store = Ext.getStore('StoreTiposCliente');
            var data = store.getById(value);
            return value ? data.get('nombre') : '';
        }
    },
    {
        text : 'Correu',
        dataIndex : 'correo',
        flex : 1
    },
    {
        text : 'Movil',
        dataIndex : 'movil',
        flex : 1
    },
    {
        text : 'Perfil',
        dataIndex : 'etiqueta',
        flex : 1
    },
    {
        text : 'Id',
        dataIndex : 'id',
        name : 'id',
        hidden : true,
        flex : 1

    } ]

});
