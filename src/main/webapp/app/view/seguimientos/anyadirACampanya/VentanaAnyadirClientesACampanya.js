Ext.define('CRM.view.seguimientos.anyadirACampanya.VentanaAnyadirClientesACampanya',
    {
        extend: 'Ext.Window',
        requires: ['CRM.view.seguimientos.anyadirACampanya.GridClientes', 'CRM.view.busqueda.FiltrosBusqueda'],
        alias: 'widget.anyadirACampanyaPanelClientes',
        closable: true,
        width: 600,

        resizable: false,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        fbar: [
            {
                xtype: 'button',
                text: 'afegir',
                name: 'afegirDesdeVentana'
            },
            {
                xtype: 'button',
                text: 'cerrar',
                name: 'cerrarVentana'
            }],

        items: [
            {
                border: 0,
                xtype: 'combobox',
                editable: false,
                fieldLabel: 'Estat destí',
                labelWidth: 110,
                padding: 5,
                //width: 400,
                emptyText: 'Trieu...',
                store: 'StoreTiposEstadoCampanyaSeguimientos',
                displayField: 'nombre',
                valueField: 'id',
                name: 'campanyaEstadoDestinoId',
                queryMode: 'local'
            },
            {
                xtype: 'filtrosBusqueda',
                name: 'filtrosBusquedaSeguimientos',
                showSearchDNI: false
            },
            {
                xtype: 'anyadirACampanyaGridClientes',
                name: 'anyadirACampanyaGridClientes',
                flex: 4,
                height: 250
            }]
    });