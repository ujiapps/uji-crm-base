Ext.define('CRM.view.seguimientos.TreeCampanyas', {
    extend: 'Ext.tree.Panel',
    title: 'Campanyes',
    alias: 'widget.treeCampanyas',
    closable: false,
    name: 'campanya',
    // expanded: true,
    store: Ext.create('CRM.store.StoreCampanyasUserTree'),
    useArrows: true,
    rootVisible: false,
    tbar: ['->', {
        xtype: 'button',
        name: 'refrescarTree',
        iconCls: 'x-tbar-loading',
        listeners:{
            click : function(){
                var panel = this.up("treepanel[name=campanya]");
                panel.up("panel[name=panelSeguimientos]").down("tabpanel[name=tabSeguimientos]").setDisabled(true);
                panel.setLoading(true);
                panel.getStore().load({
                    callback: function(){
                        panel.setLoading(false);
                    }
                });
            }
        }
    }]
});