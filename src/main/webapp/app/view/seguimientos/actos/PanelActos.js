Ext.define('CRM.view.seguimientos.actos.PanelActos',
    {
        extend: 'Ext.panel.Panel',
        title: 'Actos',
        alias: 'widget.panelActos',
        requires: ['CRM.view.seguimientos.actos.TabCampanyaActos'],
        closable: true,
        border: 0,

        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        items: [
            {
                xtype: 'combo',
                editable: false,
                padding: 5,
                fieldLabel: 'Tipus',
                store: Ext.create('CRM.store.StoreCampanyaActos'),
                displayField: 'titulo',
                valueField: 'id',
                name: 'comboActos',
                queryMode: 'local'
            },
            {
                xtype: 'container',
                layout: {type: 'hbox', align: 'stretch'},
                padding: 5,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Num. entradas a asignar',
                        name: 'numEntradasNuevas',
                        //padding: 5,
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: 'asignar a todos',
                        name : 'asignarNumEntradasNuevas',
                        margin: '0 0 0 10'
                    },
                    {
                        xtype: 'button',
                        text: 'enviar entradas a todos',
                        name : 'enviarNumEntradas',
                        margin: '0 0 0 10'
                    }
                ]

            },
            {
                xtype: 'tabCampanyaActos'
            }]

    });