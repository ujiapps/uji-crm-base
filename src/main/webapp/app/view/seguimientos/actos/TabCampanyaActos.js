Ext.define('CRM.view.seguimientos.actos.TabCampanyaActos',
    {
        extend: 'Ext.tab.Panel',
        alias: 'widget.tabCampanyaActos',
        activeTab: 0,
        flex: 1,
        items: [

            {
                title: 'Info CRM',
                xtype: 'grid',
                store: 'StoreInfoEntradasCRM',
                name: 'gridInfoEntradasCRM',
                autoScroll: true,
                selModel: {
                    mode: 'MULTI'
                },
                viewConfig: {
                    enableTextSelection: true
                },
                padding: 5,
                flex: 1,
                features: [{
                    ftype: 'summary'
                }],
                requires: ['Ext.grid.plugin.RowEditing'],
                plugins: [
                    {
                        ptype: 'rowediting',
                        clicksToEdit: 2
                    }],

                columns: [
                    {
                        dataIndex: 'clienteId',
                        hidden: true
                    },
                    {
                        dataIndex: 'actoId',
                        hidden: true
                    },
                    {
                        text: 'Usuari',
                        dataIndex: 'persona',
                        menuDisabled: true,
                        flex: 4,
                        summaryRenderer: function () {
                            return 'Total'
                        }
                    },
                    {
                        text: 'solicitadas',
                        dataIndex: 'entradas',
                        menuDisabled: true,
                        flex: 1,
                        summaryType: 'sum'
                    },
                    {
                        text: 'pendientes',
                        dataIndex: 'sobrantes',
                        menuDisabled: true,
                        flex: 1,
                        summaryType: 'sum'
                    },
                    {
                        text: 'Nuevas',
                        dataIndex: 'nuevas',
                        menuDisabled: true,
                        flex: 1,
                        summaryType: 'sum',
                        editor: {
                            allowBlank: true
                        }
                    },
                    {
                        text: 'Dadas',
                        dataIndex: 'dadas',
                        menuDisabled: true,
                        xtype: 'booleancolumn',
                        trueText: 'Sí',
                        falseText: 'No',
                        flex: 1
                    },

                    {
                        xtype: 'actioncolumn',
                        width: 30,
                        menuDisabled: true,
                        align: 'center',
                        name: 'solicitarEntradas',
                        items: [
                            {
                                xtype: 'button',
                                name: 'solicitarEntradas',
                                icon: '//static.uji.es/js/extjs/uji-commons-extjs/img/application_add.png'
                            }]
                    }
                ]
            },
            {
                title: 'Info Paraninf',
                xtype: 'grid',
                store: 'StoreInfoEntradas',
                name: 'gridInfoEntradas',
                selModel: {
                    mode: 'MULTI'
                },
                viewConfig: {
                    enableTextSelection: true
                },
                padding: 5,
                flex: 1,
                autoScroll: true,
                features: [{
                    ftype: 'summary'
                }],
                columns: [
                    {
                        text: 'email',
                        dataIndex: 'email',
                        menuDisabled: true,
                        flex: 2,
                        summaryRenderer: function () {
                            return 'Total'
                        }
                    },
                    {
                        text: 'pedidas',
                        dataIndex: 'compras',
                        menuDisabled: true,
                        flex: 2,
                        summaryType: 'sum'
                    },
                    {
                        text: 'presentadas',
                        dataIndex: 'presentadas',
                        menuDisabled: true,
                        flex: 2,
                        summaryType: 'sum'
                    }
                ]
            }]
    });