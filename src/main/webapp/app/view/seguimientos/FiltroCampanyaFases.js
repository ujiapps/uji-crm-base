Ext.define('CRM.view.seguimientos.FiltroCampanyaFases',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.filtroCampanyaFases',
    frame : true,

    border : false,
    padding : 5,
    closable : false,
    layout : 'anchor',

    items : [
    {
        xtype : 'container',
        layout : 'hbox',

        items : [
        {
            border : 0,
            xtype : 'combobox',
            editable : false,
            labelWidth : 60,
            labelAlign : 'left',
            margin : '3 20 0 0',
            fieldLabel : 'Fase',
            emptyText : 'Trieu...',
            store : 'StoreCampanyaFases',
            displayField : 'faseNombre',
            valueField : 'id',
            name : 'campanyaFases',
            width : 500 ,
            queryMode: 'local'
        } ]
    } ]
});