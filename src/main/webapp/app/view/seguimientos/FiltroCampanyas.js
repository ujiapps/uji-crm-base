Ext.define('CRM.view.seguimientos.FiltroCampanyas', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.filtroCampanyas',
    frame: true,
    requires: ['CRM.view.seguimientos.ComboCampanyas'],
    border: false,
    padding: 5,
    closable: false,
    layout: 'anchor',

    items: [{
        xtype: 'container',
        layout: 'hbox',

        items: [{
            border: 0,
            xtype: 'comboCampanyas',
            labelWidth: 60,
            labelAlign: 'left',
            margin: '3 20 0 0',
            fieldLabel: 'Campanya',
            emptyText: 'Trieu...',
            store: 'StoreCampanyasAccesoUser',
            displayField: 'nombre',
            valueField: 'id',
            name: 'campanya',
            width: 500,
            queryMode: 'local',
            editable: true
        }, {
            xtype: 'button',
            text: 'Netejar Filtres',
            name: 'limpiarFiltros'
        }]
    }],
    listeners: {
        activate: function () {
            var combo = this.down("[name=campanya]");
            var store = combo.getStore();
            store.load();
        }
    }
});