Ext.define('CRM.view.seguimientos.FormCampanyaSeguimientos',
{
    extend : 'Ext.form.FormPanel',
    alias : 'widget.formCampanyaSeguimientos',
    frame : true,
    labelAlign : 'left',
    autoScroll : true,
    padding : 10,

    layout :
    {
        type : 'vbox'
    },

    fbar : [
    {
        xtype : 'button',
        name : 'guardar',
        text : 'Actualitzar'

    },
    {
        xtype : 'button',
        name : 'borrar',
        text : 'Esborrar'
    } ],

    items : [
    {
        xtype : 'datefield',
        fieldLabel : 'Data',
        displayField : 'fecha',
        name : 'fecha',
        format : 'd/m/y',
        allowBlank : false,
        width : 400
    },
    {
        xtype : 'combobox',
        name : 'seguimientoTipoId',
        store : 'StoreSeguimientoTipos',
        displayField : 'nombre',
        fieldLabel : 'Tipus',
        valueField : 'id',
        allowBlank : false,
        editable : false,
        width : 400
    },

    {
        xtype : 'textfield',
        fieldLabel : 'Nom',
        displayField : 'nombre',
        name : 'nombre',
        allowBlank : false,
        width : 400
    },
    {
        xtype : 'textareafield',
        fieldLabel : 'Descripció',
        displayField : 'descripcion',
        name : 'descripcion',
        allowBlank : false,
        width : 600
    },
    {
        xtype : 'textfield',
        name : 'campanyaFaseId',
        hidden : true
    },
    {
        xtype : 'textfield',
        name : 'id',
        hidden : true
    } ]
});