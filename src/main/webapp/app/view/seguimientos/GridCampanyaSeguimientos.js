Ext.define('CRM.view.seguimientos.GridCampanyaSeguimientos',
{
    extend : 'Ext.grid.Panel',
    title : 'Seguimients de la fase',
    alias : 'widget.gridCampanyaSeguimientos',
    store : 'StoreCampanyaSeguimientos',
    sortableColumns : true,
    border : 0,

    selModel :
    {
        mode : 'SINGLE'
    },

    plugins : [
    {
        ptype : 'rowediting',
        clicksToEdit : 2
    } ],

    tbar : [

    {
        xtype : 'button',
        name : 'anyadirCampanyaSeguimiento',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarCampanyaSeguimiento',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'Data',
        dataIndex : 'fecha',
        width : 125,
        renderer : Ext.util.Format.dateRenderer('d/m/Y'),
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            allowBlank : false,
            width : 125
        }
    },
    {
        text : 'Nom',
        dataIndex : 'nombre',
        editor :
        {
            allowBlank : false
        },
        flex : 1
    },
    {
        text : 'Tipus',
        dataIndex : 'seguimientoTipoNombre',
        width : 150,
        editor :
        {
            xtype : 'combobox',
            name : 'seguimientoTipoId',
            store : 'StoreSeguimientoTipos',
            displayField : 'nombre',
            valueField : 'id',
            allowBlank : false,
            editable : false,
            width : 150
        }
    },
    {
        text : 'Descripció',
        dataIndex : 'descripcion',
        editor :
        {
            allowBlank : true
        },
        flex : 2
    } ]

});