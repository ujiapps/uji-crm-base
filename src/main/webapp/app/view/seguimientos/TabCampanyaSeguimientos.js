Ext.define('CRM.view.seguimientos.TabCampanyaSeguimientos',
{
    extend : 'Ext.tab.Panel',
    alias : 'widget.tabCampanyaSeguimientos',
    requires : [ 'CRM.view.seguimientos.GridEnviosSeguimientos', 'CRM.view.seguimientos.GridSeguimientoFicheros' ],
    activeTab : 0,
    items : [
    {
        xtype : 'form',
        name : 'formSeguimientoFicheros',
        title : 'Archius',

        items : [
        {
            xtype : 'gridSeguimientoFicheros',
            name : 'gridSeguimientoFicheros'
        } ]
    //        
    //        xtype : 'gridSeguimientoFicheros',
    //        title : 'Archius',
    //        layout : 'fit'
    },
    {
        xtype : 'gridEnviosSeguimientos',
        title : 'Enviaments',
        name : 'panelEnvios',
        closable : false,
        autoScroll : true

    } ]
});