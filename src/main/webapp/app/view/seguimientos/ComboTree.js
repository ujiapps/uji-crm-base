Ext.define('CRM.view.seguimientos.ComboTree',
{
    extend : 'Ext.form.field.ComboBox',
    alias : 'widget.comboTree',


    constructor : function(args)
    {
        var me = this;
        var groupField = "programaId";
        var groupDisplayField = "programaNombre";
        var displayField = "nombre";
        var campanyaId = "campanyaId";

        args.tpl = new Ext.XTemplate('<tpl for=".">',
        //                                            '<div class="x-boundlist-item">{' + campanyaId + '}</div>',
        '<tpl if="this.' + groupField + ' != values.' + groupField + '">', '<tpl exec="this.' + groupField + ' = values.' + groupField + '"></tpl>',
                '<div class="x-panel-header-default x-panel-header-text-container x-panel-header-text x-panel-header-text-default" title="{' + groupDisplayField + '}">{' + groupDisplayField
                        + '}</div>', '</tpl>', '<tpl if="this.isEmpty(' + campanyaId + ')">',
                '<div class="x-panel-header-default x-panel-header-text-container x-panel-header-text x-panel-header-text-default" title="{' + displayField + '}">{' + displayField + '}</div>',
                '<tpl else>', '<div class="x-boundlist-item">{' + displayField + '}</div>', '</tpl>',

                '</tpl>',
            {
                isEmpty : function(value)
                {
                    return value == '' || value == null;
                }
            });
        me.callParent(arguments);
    }
});