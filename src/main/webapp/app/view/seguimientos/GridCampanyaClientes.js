Ext.define('CRM.view.seguimientos.GridCampanyaClientes', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridCampanyaClientes',
    store: 'StoreCampanyaClientes',
    // requires: ['Ext.grid.plugin.RowEditing', 'Ext.ux.RowExpander'],
    requires: ['Ext.grid.plugin.RowEditing'],
    sortableColumns: true,

    selModel: {
        mode: 'SINGLE'
    },

    plugins: [{
        ptype: 'rowediting',
        clicksToEdit: 2
    }],

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'StoreCampanyaClientes',
        dock: 'bottom',
        displayInfo: true
    }],

    tbar: [{
        xtype: 'button',
        name: 'anyadirCampanyaClienteMultiple',
        text: 'Afegir multiples clients a la campanya',
        iconCls: 'application-add'
    }, {
        xtype: 'button',
        name: 'borrarCampanyaCliente',
        text: 'Esborrar client de la campanya',
        iconCls: 'application-delete',
        disabled: true
    }, {
        xtype: 'button',
        name: 'anyadirCampanyaClienteCsv',
        text: "Afegir clients des d'archiu",
        iconCls: 'application-add'
    }],

    initComponent: function () {
        var me = this;

        var store = Ext.getStore('StoreTiposEstadoCampanyaSeguimientos');

        var columnsDefault = [{
            text: 'DNI',
            dataIndex: 'identificacion',
            flex: 1
        }, {
            text: 'Client',
            dataIndex: 'clienteNombre',
            flex: 2
        }, {
            text: 'Correu',
            dataIndex: 'correo',
            flex: 2
        },{
            text: 'Estat',
            dataIndex: 'campanyaClienteTipoNombre',
            name: 'comboCampanyaClienteTipo',
            flex: 1,
            editor: {
                xtype: 'combobox',
                name: 'comboCampanyaClienteTipoEditor',
                store: store,
                editable: false,
                displayField: 'nombre',
                valueField: 'id',
                allowBlank: false,
                emptyText: 'Selecciona un tipus',
                queryMode: 'local'
            }
        }, {
            text: 'Comentaris',
            dataIndex: 'estado',
            flex: 1,
            editor: {
                allowBlank: true
            }
        }, {
            xtype: 'actioncolumn',
            text: 'Seguiments',
            name: 'anyadirSeguimientoCliente',
            flex: 0.5,
            menuDisabled: true,
            align: 'center',
            renderer: function (value, metaData, record) {
                if (record.data.numSeguimientos) {
                    return "<span style='padding-right: 10px;'>(" + record.data.numSeguimientos + ")</span>";
                } else {
                    return "<span style='padding-right: 10px;'>(0)</span>";
                }

            },
            items: [
                {
                    xtype: 'button',
                    name: 'anyadirSeguimientoCliente',
                    icon: '//static.uji.es/js/extjs/uji-commons-extjs/img/application_add.png'
                }]
        }, {
            xtype: 'actioncolumn',
            width: 30,
            menuDisabled: true,
            align: 'center',
            name: 'abrirCliente',
            items: [
                {
                    xtype: 'button',
                    name: 'abrirCliente',
                    icon: '//static.uji.es/js/extjs/uji-commons-extjs/img/application_edit.png'
                }]
        }, {
            dataIndex: 'campanyaClienteTipoId',
            hidden: true
        }, {
            name: 'clienteId',
            dataIndex: 'clienteId',
            hidden: true
        }];

        me.columns = columnsDefault;
        me.callParent(arguments);
    },

    refreshGrid: function () {
        this.reconfigure('StoreCampanyaClientes', this.columnsDefault);
    },

    addColumn: function (tipo) {

        var elemento;
        var tipoElemento;
        if (tipo == 1) {
            elemento = 'mail';
            tipoElemento = 'string';
        } else if (tipo == 2) {
            elemento = 'entradas';
            tipoElemento = 'int';
        }

        var modeloNuevo = Ext.define('ModelNuevo', {
            extend: 'CRM.model.CampanyaCliente',
            fields: [
                {
                    name: elemento,
                    type: tipoElemento
                }]
        });

        var classStoreNuevo = Ext.define('StoreNuevo', {
            extend: 'Ext.data.Store',
            model: modeloNuevo,
            autoLoad: false,
            autoSync: false,
            proxy: {
                type: 'rest',
                url: '/crm/rest/campanyacliente/',

                reader: {
                    type: 'json',
                    successProperty: 'success',
                    root: 'data'
                },

                writer: {
                    type: 'json'
                }
            }
        });

        var storeNuevo = new classStoreNuevo();
        storeNuevo.load(
            {
                url: '/crm/rest/campanyacliente/',
                params: {
                    elementoNuevo: elemento
                }
            }
        );

        var columna = Ext.create('Ext.grid.column.Column', {
            text: 'Prueba',
            dataIndex: 'prueba',
            flex: 1
        });
        this.headerCt.insert(this.columns.length, columna);
        this.reconfigure(storeNuevo);
    }

});