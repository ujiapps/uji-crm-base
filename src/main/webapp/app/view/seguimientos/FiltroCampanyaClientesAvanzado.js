Ext.define('CRM.view.seguimientos.FiltroCampanyaClientesAvanzado',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.filtroCampanyaClientesAvanzado',
    frame : true,

    border : false,
    padding : 5,
    closable : false,
    layout : 'anchor',

    items : [
    {
        xtype : 'container',
        layout : 'vbox',
        padding : '5 0 0 0',

        items : [
                {
                    xtype : 'container',
                    layout : 'hbox',
                    items : [
                            {
                                xtype : 'boxselect',
                                store : 'StoreItemsSeguimiento',
                                fieldLabel : 'Items',
                                labelWidth : 80,
                                width : 500,
                                emptyText : 'Escriu el item...',
                                displayField : 'nombreCa',
                                valueField : 'id',
                                name : 'comboItems',
                                queryMode : 'local',
                                editable : true,
                                hideTrigger : true,
                                padding : '5 0 0 0',
                                enableKeyEvents : true,
                                tpl : new Ext.XTemplate('<tpl for=".">', '<tpl if="this.' + 'grupoId' + ' != values.' + 'grupoId' + '">', '<tpl exec="this.' + 'grupoId' + ' = values.' + 'grupoId'
                                        + '"></tpl>', '<div class="x-panel-header-default x-panel-header-text-container x-panel-header-text x-panel-header-text-default" title="{' + 'grupoNombre'
                                        + '}">{' + 'grupoNombre' + '}</div>', '</tpl>', '<div class="x-boundlist-item">{' + 'nombreCa' + '}</div>', '</tpl>')
                            },
                            {
                                xtype : 'combobox',
                                store : 'StoreComboBusquedaAmpliado',
                                name : 'busquedaComboItemsOpcion',
                                padding : '5 0 0 5',
                                labelWidth : 80,
                                queryMode : 'local',
                                displayField : 'etiqueta',
                                valueField : 'id'
                            } ]
                },
                {
                    xtype : 'container',
                    layout : 'hbox',
                    items : [
                    {
                        xtype : 'boxselect',
                        store : 'StoreSeguimientoTipos',
                        fieldLabel : 'Seguiments',
                        labelWidth : 80,
                        width : 500,
                        emptyText : 'Escriu ...',
                        displayField : 'nombre',
                        valueField : 'id',
                        name : 'busquedaSeguimientoTipo',
                        queryMode : 'local',
                        editable : true,
                        hideTrigger : true,
                        //                        margin : '0 10 0 0',
                        padding : '5 0 0 0',
                        enableKeyEvents : true,
                        listConfig :
                        {
                            width : 640,
                            emptyText : 'Escriu ...'
                        }
                    },
                    //                    {
                    //                        xtype : 'combobox',
                    //                        labelWidth : 80,
                    //                        width : 500,
                    //                        padding : '5 0 0 0',
                    //                        fieldLabel : 'Seguiments',
                    //                        name : 'busquedaSeguimientoTipo',
                    //                        store : 'StoreSeguimientoTipos',
                    //                        displayField : 'nombre',
                    //                        valueField : 'id'
                    //                    },
                    {
                        xtype : 'combobox',
                        store : 'StoreComboBusquedaAmpliado',
                        //                        fieldLabel : '',
                        name : 'busquedaSeguimientoTipoOpcion',
                        padding : '5 0 0 5',
                        labelWidth : 80,
                        queryMode : 'local',
                        displayField : 'etiqueta',
                        valueField : 'id'
                    } ]
                } ]
    } ]
});