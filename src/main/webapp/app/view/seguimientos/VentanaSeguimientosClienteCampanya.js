function formatDate(value)
{
    return value ? Ext.Date.dateFormat(value, 'd/m/Y') : '';
}

Ext.define('CRM.view.seguimientos.VentanaSeguimientosClienteCampanya',
{
    extend : 'Ext.Window',
    title : 'Seguiments',
    width : 700,
    height : 300,
    closable : false,
    alias : 'widget.ventanaSeguimientosClienteCampanya',
    modal : true,
    layout :
    {
        type : 'hbox',
        align : 'stretch'
    },
    fbar : [
    {
        xtype : 'button',
        name : 'finalizarSeguimientosClienteCampanya',
        text : 'Finalitzar'
    } ],

    items : [
    {
        xtype : 'grid',
        labelAlign : 'top',
        flex : 1,
        autoScroll : true,
        requires : [ 'Ext.grid.plugin.RowEditing' ],
        store : 'StoreSeguimientosCampanya',
        plugins : [
        {
            ptype : 'rowediting',
            clicksToEdit : 2
        } ],
        border : 0,
        name : 'gridVentanaSeguimientosClienteCampanya',
        selModel :
        {
            mode : 'MULTI'
        },

        columns : [
        {
            text : 'Data',
            dataIndex : 'fecha',
            format : 'd/m/Y',
            renderer : formatDate,
            flex : 1,
            editor :
            {
                xtype : 'datefield',
                format : 'd/m/y',
                allowBlank : false,
                width : 125
            }
        },
        {
            text : 'Descripció',
            dataIndex : 'descripcion',
            flex : 4,
            editor :
            {
                allowBlank : true
            }
        },

        {
            text : 'Nom',
            dataIndex : 'nombre',
            flex : 2,
            editor :
            {
                allowBlank : false
            }
        },
        {
            text : 'Tipus',
            dataIndex : 'seguimientoTipoNombre',
            editor :
            {
                xtype : 'combobox',
                name : 'seguimientoTipoId',
                store : 'StoreSeguimientoTipos',
                displayField : 'nombre',
                valueField : 'id',
                allowBlank : false,
                editable : false,
                width : 150
            },
            flex : 1.5
        } ],
        tbar : [
        {
            xtype : 'button',
            name : 'anyadirSeguimientoCampanyaCliente',
            text : 'Afegir',
            iconCls : 'application-add'
        },
        {
            xtype : 'button',
            name : 'borrarSeguimientoCampanyaCliente',
            text : 'Esborrar',
//            disabled : true,
            iconCls : 'application-delete'
        } ]
    } ]

});