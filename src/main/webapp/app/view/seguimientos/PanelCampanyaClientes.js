Ext.define('CRM.view.seguimientos.PanelCampanyaClientes',
{
    extend : 'Ext.panel.Panel',
    title : 'Serveis',
    requires : [ 'CRM.view.seguimientos.FiltroCampanyaClientes', 'CRM.view.seguimientos.GridCampanyaClientes', 'CRM.view.seguimientos.FiltroCampanyaClientesAvanzado' ],
    alias : 'widget.panelCampanyaClientes',
    closable : true,
    border : 0,

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'filtroCampanyaClientes',
        border: 0
    },
    {
        xtype : 'gridCampanyaClientes',
        flex : 1
    } ]

});