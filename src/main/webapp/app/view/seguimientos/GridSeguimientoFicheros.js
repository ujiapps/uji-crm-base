Ext.define('CRM.view.seguimientos.GridSeguimientoFicheros',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridSeguimientoFicheros',
    store : 'StoreSeguimientoFicheros',
    sortableColumns : true,

    tbar : [
    {
        xtype : 'filefield',
        fieldLabel : 'Fitxer',
        value : '',
        labelWidth : 50,
        allowBlank : false,
        buttonConfig :
        {
            text : 'Sel·lecciona...',
            disabled : false
        }
    },
    {
        xtype : 'button',
        name : 'subir',
        iconCls : 'accept',
        text : 'Pujar'
    },

    {
        xtype : 'button',
        name : 'borrar',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    },
    {
        xtype : 'button',
        name : 'descargar',
        text : 'Descarregar',
        iconCls : 'arrow-down',
        disabled : true
    } ],

    columns : [
    {
        text : 'Fitxer',
        dataIndex : 'nombreFichero',
        flex : 1
    } ]

});