Ext.define('CRM.view.seguimientos.PanelSeguimientos', {
    extend: 'Ext.panel.Panel',
    title: 'Seguimients',
    requires: ['CRM.view.seguimientos.TabSeguimientos', 'CRM.view.seguimientos.TreeCampanyas'],
    alias: 'widget.panelSeguimientos',
    closable: true,
    name: 'panelSeguimientos',
    layout: 'border',

    items: [{
        xtype: 'treeCampanyas',
        region: 'north',
        split: true,
        collapsible: true,
        flex: 1
    }, {
        xtype: 'tabSeguimientos',
        region: 'center',
        name: 'tabSeguimientos',
        flex: 2
    }]
});