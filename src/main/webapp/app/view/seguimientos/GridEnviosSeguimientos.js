function formatDate(value)
{
    return value ? Ext.Date.dateFormat(value, 'd/m/Y') : '';
}

Ext.define('CRM.view.seguimientos.GridEnviosSeguimientos',
{
    extend : 'Ext.grid.Panel',
    title : 'Enviaments',
    alias : 'widget.gridEnviosSeguimientos',
    store : 'StoreEnviosSeguimientos',
    sortableColumns : true,
    autoScroll : true,
    viewConfig :
    {
        getRowClass : function(record)
        {
            if (record.get('fechaEnvioPlataforma') != null)
            {
                return "grid-row-not-avaible";
            }
        }
    },

    selModel :
    {
        mode : 'SINGLE'
    },

    tbar : [
    {
        xtype : 'button',
        name : 'seguimientoEnvioNuevo',
        text : 'Afegir Enviament',
        iconCls : 'application-add'
    } ],

    columns : [
    {
        text : 'id',
        dataIndex : 'id',
        hidden : true
    },
    {
        text : 'Nom',
        dataIndex : 'nombre',
        flex : 2
    },
    {
        text : 'Asumpte',
        dataIndex : 'asunto',
        flex : 2
    },
    {
        text : 'Data',
        dataIndex : 'fechaEnvio',
        format : 'd/m/Y',
        renderer : formatDate,
        flex : 1
    },
    {
        text : 'Tipus',
        dataIndex : 'envioTipoNombre',
        flex : 1
    } ]

});
