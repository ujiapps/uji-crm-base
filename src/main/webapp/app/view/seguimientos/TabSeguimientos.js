Ext.define('CRM.view.seguimientos.TabSeguimientos',
    {
        extend: 'Ext.tab.Panel',
        alias: 'widget.tabSeguimientos',
        requires: ['CRM.view.seguimientos.PanelCampanyaClientes', 'CRM.view.seguimientos.PanelCampanyaSeguimientos', 'CRM.view.seguimientos.actos.PanelActos'],
//    activeTab : 0,
        disabled: true,
        border: 0,

        items: [
            {
                title: 'Clients Campanya',
                xtype: 'panelCampanyaClientes',
                closable: false
            },
            {
                title: 'Seguiments Campanya',
                xtype: 'panelCampanyaSeguimientos',
                closable: false
            },
            {
                title: 'Actos Campanya',
                xtype: 'panelActos',
                closable: false
            }]
    });