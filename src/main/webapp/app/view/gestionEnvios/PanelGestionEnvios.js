Ext.define('CRM.view.gestionEnvios.PanelGestionEnvios', {
    extend: 'Ext.panel.Panel',
    requires: ['CRM.view.gestionEnvios.gestionEnviosImagenes.PanelGestionEnviosImagenes',
        'CRM.view.gestionEnvios.gestionEnviosPlantillas.PanelGestionEnviosPlantillas'],
    title: 'Gestió Enviaments',
    alias: 'widget.panelGestionEnvios',
    closable: true,
    layout: 'border',
    items: [{
        xtype: 'tabpanel',
        name: 'tabGestionEnvios',
        region: 'center',
        flex: 1,
        items: [{
            title: 'Plantillas',
            xtype: 'panelGestionEnviosPlantillas',
        }, {
            xtype: 'panelGestionEnviosImagenes',
            title: 'Imatges',
            listeners:{
                show: function(){
                    this.down('grid[name=gridGestionEnviosImagenes]').getStore().load();
                }
            }
        }]
    }]

});