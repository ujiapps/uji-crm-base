Ext.define('CRM.view.gestionEnvios.gestionEnviosImagenes.GridGestionEnviosImagenes', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridGestionEnviosImagenes',
    autoScroll: true,
    store : Ext.create('CRM.store.StoreEnviosImagenes', {storeId: 'storeEnviosImagenesGenerico'}),
    tbar: [{
        xtype: 'filefield',
        fieldLabel: 'Imatge',
        labelWidth: 50,
        name: 'anyadirImagen',
        buttonConfig: {
            text: 'Sel·lecciona...',
            disabled: false
        },
        flex: 5,
        regex: /(.)+((\.png)|(\.jpg)|(\.jpeg)(\w)?)$/i,
        regexText: 'Only PNG and JPEG image formats are accepted',
        listeners: {
            change: function () {
                var grid = this.up('grid[name=gridGestionEnviosImagenes]'),
                    botonAnyadir = grid.down('button[name=botonAnyadirImagen]'),
                    botonEliminar = grid.down('button[name=botonBorrarImagen]');

                botonAnyadir.setDisabled(false);
                botonEliminar.setDisabled(true);
            }
        }
    }, {
        xtype: 'button',
        name: 'botonAnyadirImagen',
        iconCls: 'accept',
        disabled: true,
        flex: 0.5,
        listeners: {
            click: function () {
                var formImagenes = this.up('form[name=formSubirImagenes]'),
                    grid = this.up('grid[name=gridGestionEnviosImagenes]'),
                    botonAnyadir = this,
                    botonEliminar = grid.down('button[name=botonBorrarImagen]');

                if (formImagenes.getForm().isValid()) {
                    formImagenes.submit(
                        {
                            url: '/crm/rest/envioimagen/',
                            scope: this,
                            success: function () {
                                botonAnyadir.setDisabled(true);
                                botonEliminar.setDisabled(true);
                                grid.getStore().load();
                            },
                            failure: function (x, resp) {
                                Ext.Msg.alert('Insercció de imatges', '<b>' + resp.result.message + '</b>');
                            }
                        });
                } else {
                    Ext.Msg.alert('Insercció d\'imatges', '<b>El fitxer no es una imatge</b>');
                }
            }
        }
    }, {
        xtype: 'button',
        name: 'botonBorrarImagen',
        iconCls: 'application-delete',
        disabled: true,
        flex: 0.5,
        listeners:{
            click: function(){
                var grid = this.up('grid[name=gridGestionEnviosImagenes]'),
                    storeImagenes = grid.getStore(),
                    selection = grid.getSelectionModel().getSelection(),
                    botonEliminar = grid.down('button[name=botonBorrarImagen]');

                var ref = this;
                Ext.Msg.confirm('Eliminar', 'El registre s\'eliminarà. Estàs segur de voler continuar?', function (btn) {
                    if (btn == 'yes') {
                        grid.setLoading(true);
                        storeImagenes.remove(selection);
                        storeImagenes.sync(
                            {
                                success: function () {
                                    botonEliminar.setDisabled(true);
                                    grid.setLoading(false);
                                },
                                failure: function () {
                                    Ext.Msg.alert('Esborrat d\'imatges', '<b>La imatge no s\'ha pogut esborrar correctament</b>');
                                    grid.setLoading(false);
                                }

                            });

                    }
                });

            }
        }
    }],

    columns: [{
        text: 'id',
        dataIndex: 'id',
        hidden: true
    }, {
        dataIndex: 'nombre',
        header: 'Nom',
        flex: 2
    }],

    listeners: {
        selectionchange: function (sel) {
                this.down('button[name=botonBorrarImagen]').setDisabled(false);
        }
    }
});