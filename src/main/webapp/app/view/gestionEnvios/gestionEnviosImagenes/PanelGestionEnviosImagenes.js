Ext.define('CRM.view.gestionEnvios.gestionEnviosImagenes.PanelGestionEnviosImagenes', {
    extend: 'Ext.panel.Panel',
    title: 'Gestió Imatges',
    alias: 'widget.panelGestionEnviosImagenes',
    requires: ['CRM.view.gestionEnvios.gestionEnviosImagenes.GridGestionEnviosImagenes'],
    layout: 'border',
    name: 'panelGestionEnviosImagenes',

    items: [{
        xtype: 'form',
        name: 'formSubirImagenes',
        region: 'center',
        layout: 'border',
        // title: 'Imagenes',
        items: [{
            xtype: 'gridGestionEnviosImagenes',
            name: 'gridGestionEnviosImagenes',
            region: 'center',
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: 'storeEnviosImagenesGenerico',
                dock: 'bottom',
                displayInfo: true
            }],
        }]
    }]

});