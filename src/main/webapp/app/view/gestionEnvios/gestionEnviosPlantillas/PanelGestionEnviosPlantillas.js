Ext.define('CRM.view.gestionEnvios.gestionEnviosPlantillas.PanelGestionEnviosPlantillas', {
    extend: 'Ext.panel.Panel',
    requires: ['CRM.view.gestionEnvios.gestionEnviosPlantillas.GridGestionEnviosPlantillas', 'CRM.view.gestionEnvios.gestionEnviosPlantillas.FormGestionEnviosPlantillas'],
    // title: 'Gestió Enviaments',
    alias: 'widget.panelGestionEnviosPlantillas',
    layout: 'border',
    name: 'panelGestionEnviosPlantillas',

    items: [{
        xtype: 'gridGestionEnviosPlantillas',
        name: 'gridGestionEnviosPlantillas',
        region: 'north',
        flex : 1,
        dockedItems: [{
            xtype: 'pagingtoolbar',
            store: 'storeEnviosPlantillas',
            dock: 'bottom',
            displayInfo: true
        }],
    },{
        xtype: 'formGestionEnviosPlantillas',
        name: 'formGestionEnviosPlantillas',
        region: 'center',
        flex : 3,
        disabled : true
    }]

});