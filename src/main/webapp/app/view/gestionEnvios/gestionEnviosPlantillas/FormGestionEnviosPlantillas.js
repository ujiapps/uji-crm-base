Ext.define('CRM.view.gestionEnvios.gestionEnviosPlantillas.FormGestionEnviosPlantillas', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.formGestionEnviosPlantillas',
    frame: true,
    border: 0,
    autoScroll: true,

    fbar: [{
        xtype: 'button',
        name: 'actualizarEnvioPlantilla',
        text: 'Actualitzar',
        listeners: {
            click: function () {

                var form = this.up('form'),
                    values = form.getValues(),
                    store = Ext.getStore('storeEnviosPlantillas');

                form.setLoading(true);
                values.plantilla = form.down('[name=plantilla]').getValue();
                if (values.id == "") {
                    Ext.Ajax.request({
                        url: '/crm/rest/envioplantilla',
                        method: 'POST',
                        params: values,
                        scope: this,
                        success: function () {
                            Ext.Msg.alert('Enviament', '<b>S\'ha guardat la plantilla</b>');
                            store.reload({
                                scope: this,
                                callback: function () {
                                    form.limpiaCampos();
                                }
                            });
                            form.setLoading(false);
                            form.setDisabled(true);

                        },
                        failure: function () {
                            Ext.Msg.alert('Peu', '<b>No s\'ha pogut desar la plantilla</b>');
                            form.setLoading(false);
                        }
                    });
                } else {
                    Ext.Ajax.request({
                        url: '/crm/rest/envioplantilla/' + values.id,
                        method: 'PUT',
                        params: values,
                        scope: this,
                        success: function () {
                            Ext.Msg.alert('Enviament', '<b>S\'ha guardat la plantilla</b>');
                            store.reload({
                                scope: this,
                                callback: function () {
                                    form.limpiaCampos();
                                }
                            });
                            form.setLoading(false);
                            form.setDisabled(true);

                        },
                        failure: function () {
                            Ext.Msg.alert('Peu', '<b>No s\'ha pogut desar la plantilla</b>');
                            form.setLoading(false);
                        }
                    });
                }
            }
        }
    }],

    items: [{
        name: 'id',
        xtype: 'textfield',
        hidden: true
    }, {
        xtype: 'textfield',
        fieldLabel: 'Nom',
        name: 'nombre'
    }, {
        xtype: 'label',
        text: 'Plantilla:',
        padding: '5 0 0 0'
    }, {
        xtype: 'container',
        name: 'editor',
        padding: '5 0 0 0'
    }],

    rellenaForm: function (plantilla) {

        var form = this,
            editor = form.down("container[name=editor]"),
            config = {width: '100%', height: '100%', name: 'plantilla'},
            editorNuevo = new Ext.ux.CKEditor(config);

        var txt = editor.down('[name=plantilla]');

        if (txt != null) {
            txt.destroy();
        }

        if (plantilla) {
            form.down("textfield[name=nombre]").setValue(plantilla.nombre);
            editorNuevo.setValue(plantilla.plantilla);

            form.down("[name=id]").setValue(plantilla.id);
        } else {
            form.down("textfield[name=nombre]").setValue(null);
            form.down("[name=id]").setValue(null);
        }
        editor.add(editorNuevo);
        editorNuevo.setUrl('/envioimagen/visualizar/');
    },

    limpiaCampos: function () {
        var form = this,
            editor = form.down("container[name=editor]"),
            txt = editor.down('[name=plantilla]');

        form.down("textfield[name=nombre]").setValue(null);
        if (txt != null) {
            txt.destroy();
        }
    }
})
;