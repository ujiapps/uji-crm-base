Ext.define('CRM.view.gestionEnvios.gestionEnviosPlantillas.GridGestionEnviosPlantillas', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridGestionEnviosPlantillas',
    autoScroll: true,
    store: Ext.create('CRM.store.StoreEnviosPlantillas', {storeId: 'storeEnviosPlantillas'}),

    tbar: [{
        xtype: 'button',
        name: 'anyadirEnvioPlantilla',
        text: 'Afegir',
        iconCls: 'application-add',
        listeners: {
            click: function () {
                var form = this.up("panel[name=panelGestionEnviosPlantillas]").down("form[name=formGestionEnviosPlantillas]");
                form.rellenaForm();
                form.setDisabled(false);
            }
        }
    }, {
        xtype: 'button',
        name: 'borrarEnvioPlantilla',
        text: 'Esborrar',
        iconCls: 'application-delete',
        disabled: true,
        listeners: {
            click: function () {
                var grid = this.up("grid[name=gridGestionEnviosPlantillas]"),
                    store = grid.getStore();
                store.remove(grid.getSelectionModel().getSelection()[0]);
                grid.setLoading(true);
                store.sync({
                    callback: function(){
                        store.load({
                            callback: function(){
                                grid.setLoading(false);
                            }
                        });
                    }
                });
            }
        }
    }],

    columns: [{
        dataIndex: 'id',
        hidden: true,
    }, {
        text: 'Nombre',
        dataIndex: 'nombre',
        flex: 1
    }, {
        text: 'Plantilla',
        dataIndex: 'plantilla',
        flex: 3
    }],

    listeners: {

        selectionchange: function (sel) {
            this.down("button[name=borrarEnvioPlantilla]").setDisabled(!sel.hasSelection());
            var form = this.up("panel[name=panelGestionEnviosPlantillas]").down("form[name=formGestionEnviosPlantillas]");
            form.setDisabled(!sel.hasSelection());
            form.limpiaCampos();
            if (sel.hasSelection()) {
                form.rellenaForm(sel.getSelection()[0].data);
            }
        }
    }
});