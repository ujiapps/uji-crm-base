Ext.define('CRM.view.busqueda.FiltrosBusqueda', {
    extend: 'Ext.panel.Panel',
    requires: ['CRM.view.busqueda.FiltroAvanzado'],
    alias: 'widget.filtrosBusqueda',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    config: {
        showSearchDNI: true
    },

    enableKeyEvents: true,
    frame: true,
    fbar: [{
        xtype: 'button',
        name: 'limpiarBusqueda',
        text: 'limpiar',
        listeners: {
            click: function () {
                this.up("panel").limpiarBusqueda();
            }
        }
    }, {
        xtype: 'button',
        name: 'botonBusqueda',
        icon: '//static.uji.es/js/extjs/uji-commons-extjs/img/find.png',
        maxWidth: 23
    }],

    items: [{
        xtype: 'container',
        padding: '5 0 0 0',
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Nom / Cognoms / Identificació',
            name: 'busquedaDNI',
            labelWidth: 170,
            padding: '0 5 0 10',
            flex: 1,
            hidden: true
        }, {
            xtype: 'combobox',
            store: 'StoreEtiquetasClientes',
            fieldLabel: 'Etiqueta',
            labelWidth: 60,
            emptyText: 'Escriu la etiqueta...',
            displayField: 'nombre',
            valueField: 'id',
            name: 'busquedaEtiqueta',
            queryMode: 'local',
            editable: true,
            padding: '0 5 0 10',
            hideTrigger: true,
            enableKeyEvents: true,
            flex: 1
        }, {
            xtype: 'combobox',
            store: 'StoreTiposCliente',
            fieldLabel: 'Tipus',
            labelWidth: 60,
            displayField: 'nombre',
            valueField: 'id',
            name: 'busquedaTipo',
            queryMode: 'local',
            editable: true,
            padding: '0 5 0 10',
            flex: 1
        }]
    }, {
        xtype: 'container',
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        padding: '5 0 0 0',
        items: [{
            xtype: 'combobox',
            store: 'StoreComboBusqueda',
            queryMode: 'local',
            editable: true,
            fieldLabel: 'Correu electrónic',
            displayField: 'etiqueta',
            valueField: 'id',
            name: 'filtroCorreoElectronico',
            labelWidth: 100,
            padding: '0 5 0 10',
            flex: 1
        }, {
            xtype: 'combobox',
            store: 'StoreComboBusqueda',
            queryMode: 'local',
            editable: true,
            fieldLabel: 'Correu postal',
            displayField: 'etiqueta',
            valueField: 'id',
            name: 'filtroCorreoPostal',
            labelWidth: 100,
            padding: '0 5 0 10',
            flex: 1
        }, {
            xtype: 'combobox',
            store: 'StoreComboBusqueda',
            queryMode: 'local',
            editable: true,
            fieldLabel: 'Movil',
            displayField: 'etiqueta',
            valueField: 'id',
            name: 'filtroMovil',
            labelWidth: 60,
            padding: '0 5 0 10',
            flex: 1
        }]
    }, {
        title: 'Opcions avançades',
        xtype: 'filtroAvanzado',
        margin: '5 0 0 0',
        padding: '5 0 5 5',
        // resizable : true,
        // collapsible : true
    }],

    initComponent: function () {
        var ref = this;

        this.callParent(arguments);
        if (this.showSearchDNI) {
            this.down("textfield[name=busquedaDNI]").setVisible(true);
        }
    },

    busqueda: function (MIN_CARACTERES_BUSQUEDA) {
        var objeto = {
            busqueda: this.down("textfield[name=busquedaDNI]").getValue(),
            etiqueta: this.down("textfield[name=busquedaEtiqueta]").getRawValue(),
            tipo: this.down("combobox[name=busquedaTipo]").getValue(),
            correo: this.down("combobox[name=filtroCorreoElectronico]").getValue(),
            postal: this.down("combobox[name=filtroCorreoPostal]").getValue(),
            movil: this.down("combobox[name=filtroMovil]").getValue(),
            campanya: this.down("combobox[name=comboFiltroCampanya]").getValue(),
            campanyaTipoEstado: this.down("combobox[name=campanyaClienteTipoId]").getValue(),
            tipoDato: this.down("combobox[name=comboFiltroTipoDato]").getValue(),
            valorDato: this.down("combobox[name=textoFiltroValor]").getValue(),
            programa: this.down("combobox[name=comboFiltroPrograma]").getValue(),
            grupo: this.down("combobox[name=comboFiltroGrupo]").getValue(),
            item: this.down("combobox[name=comboFiltroItem]").getValue(),
            fecha: this.down("datefield[name=fechaValorMin]").getValue(),
            fechaMax: this.down("datefield[name=fechaValorMax]").getValue(),
            tipoComparacion: this.down("combobox[name=comboComparacion]").getValue(),
            tipoFecha: this.down("combobox[name=comboTipoFecha]").getValue(),
            mes: this.down("combobox[name=comboMeses]").getValue(),
            anyo: this.down("combobox[name=comboAnyos]").getValue(),
            mesMax: this.down("combobox[name=comboMesesMax]").getValue(),
            anyoMax: this.down("combobox[name=comboAnyosMax]").getValue(),
            nacionalidad: this.down("combobox[name=comboFiltroNacionalidad]").getValue(),
            codigoPostal: this.down("combobox[name=comboFiltroCodigoPostal]").getValue(),
            paisDomicilio: this.down("combobox[name=comboFiltroPaisDomicilio]").getValue(),
            provinciaDomicilio: this.down("combobox[name=comboFiltroProvinciaDomicilio]").getValue(),
            poblacionDomicilio: this.down("combobox[name=comboFiltroPoblacionDomicilio]").getValue(),
            estudioUJI: this.down("combobox[name=comboFiltroEstudioUJI]").getValue(),
            estudioUJIBeca: this.down("combobox[name=comboFiltroEstudioUJIBeca]").getValue(),
            estudioNoUJI: this.down("combobox[name=comboFiltroEstudioNoUJI]").getValue()


        };

        if (this.campoValido(objeto.correo, MIN_CARACTERES_BUSQUEDA) || this.campoValido(objeto.postal, MIN_CARACTERES_BUSQUEDA) || this.campoValido(objeto.movil, MIN_CARACTERES_BUSQUEDA) || objeto.tipoDato != null || objeto.campanya != null
            || ((objeto.busqueda.length >= MIN_CARACTERES_BUSQUEDA) || (objeto.etiqueta.length >= MIN_CARACTERES_BUSQUEDA)) || objeto.programa != null || objeto.tipo != null || objeto.nacionalidad != null
            || this.campoValido(objeto.codigoPostal, MIN_CARACTERES_BUSQUEDA) || objeto.paisDomicilio != null || objeto.provinciaDomicilio != null || objeto.poblacionDomicilio != null
            || objeto.estudioUJI != null || objeto.estudioNoUJI != null || objeto.estudioUJIBeca != null)
            return objeto;
        else
            return null;
    },

    limpiarBusqueda: function () {
        this.down("textfield[name=busquedaDNI]").setValue(null);
        this.down("textfield[name=busquedaEtiqueta]").setValue(null);
        this.down("combobox[name=busquedaTipo]").clearValue();
        this.down("combobox[name=filtroCorreoElectronico]").clearValue();
        this.down("combobox[name=filtroCorreoPostal]").clearValue();
        this.down("combobox[name=filtroMovil]").clearValue();
        this.down("combobox[name=comboFiltroCampanya]").clearValue();
        this.down("combobox[name=campanyaClienteTipoId]").clearValue();
        this.down("combobox[name=comboFiltroTipoDato]").clearValue();
        this.down("combobox[name=textoFiltroValor]").clearValue();
        this.down("combobox[name=comboFiltroPrograma]").clearValue();
        this.down("combobox[name=comboFiltroGrupo]").clearValue();
        this.down("combobox[name=comboFiltroItem]").clearValue();
        this.down("datefield[name=fechaValorMin]").setValue(null);
        this.down("datefield[name=fechaValorMax]").setValue(null);
        this.down("combobox[name=comboComparacion]").clearValue();
        this.down("combobox[name=comboTipoFecha]").clearValue();
        this.down("combobox[name=comboMeses]").clearValue();
        this.down("combobox[name=comboAnyos]").clearValue();
        this.down("combobox[name=comboMesesMax]").clearValue();
        this.down("combobox[name=comboAnyosMax]").clearValue();
        this.down("combobox[name=comboFiltroNacionalidad]").clearValue();
        this.down("combobox[name=comboFiltroCodigoPostal]").clearValue();
        this.down("combobox[name=comboFiltroPaisDomicilio]").clearValue();
        this.down("combobox[name=comboFiltroProvinciaDomicilio]").clearValue();
        this.down("combobox[name=comboFiltroPoblacionDomicilio]").clearValue();
        this.down("combobox[name=comboFiltroEstudioUJI]").clearValue();
        this.down("combobox[name=comboFiltroEstudioNoUJI]").clearValue();
        this.down("combobox[name=comboFiltroEstudioUJIBeca]").clearValue();


    },

    campoValido: function (valor, num_caracteres) {
        if (valor != null)
            return valor.length >= num_caracteres || valor == -1 || valor == -2;
        else
            return false;
    }

});