Ext.define('CRM.view.busqueda.FiltroAvanzado', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.filtroAvanzado',
    frame: true,
    border: false,
    padding: 5,
    closable: false,
    layout: 'anchor',

    initComponent: function () {
        var me = this;

        var storeGrupos = Ext.create('CRM.store.StoreGruposPrograma');
        var storeItems = Ext.create('CRM.store.StoreItemsPrograma');
        var storeValor = Ext.create('CRM.store.StoreComboBusquedaModificable');

        me.items = [{
            xtype: 'container',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            padding: '5 0 0 0',

            items: [{
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                margin: '5 10 0 0',
                items: [{
                    xtype: 'comboCampanyas',
                    store: 'StoreCampanyasAccesoUser',
                    fieldLabel: 'Campanyes',
                    labelWidth: 100,
                    displayField: 'nombre',
                    valueField: 'id',
                    name: 'comboFiltroCampanya',
                    // queryMode: 'local',
                    emptyText: 'Trieu...',
                    editable: true,
                    margin: '0 10 0 0',
                    flex: 2,
                    listeners: {
                        select: function (combo, campanya) {
                            var storeFiltro = this.up("panel").down('combobox[name=campanyaClienteTipoId]').getStore();

                            storeFiltro.load({
                                url: '/crm/rest/tipoestadocampanya/campanya/',
                                params: {
                                    campanyaId: campanya[0].data.id
                                }
                            })
                        }
                    }
                }, {
                    xtype: 'combobox',
                    editable: true,
                    fieldLabel: 'Estat',
                    labelWidth: 50,
                    emptyText: 'Trieu...',
                    //store : 'StoreTiposCampanyasClientes',
                    store: Ext.create('CRM.store.StoreTiposEstadoCampanya', {storeId: 'StoreTiposEstadoCampanyaFiltro'}),
                    displayField: 'nombre',
                    valueField: 'id',
                    name: 'campanyaClienteTipoId',
                    flex: 1,
                    queryMode: 'local'
                }]
            }, {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                margin: '5 10 0 0',
                items: [{
                    xtype: 'combobox',
                    store: 'StoreProgramasAccesoUser',
                    fieldLabel: 'Programa',
                    labelWidth: 100,
                    displayField: 'nombreCa',
                    valueField: 'id',
                    name: 'comboFiltroPrograma',
                    emptyText: 'Trieu...',
                    queryMode: 'local',
                    editable: true,
                    margin: '0 10 0 0',
                    flex: 1,
                    listeners: {
                        select: function (combo, selection) {
                            this.up('panel').down("combobox[name=comboFiltroGrupo]").setValue(null);
                            this.up('panel').down("combobox[name=comboFiltroItem]").setValue(null);
                            storeGrupos.load({
                                url: '/crm/rest/grupo/',
                                params: {
                                    programaId: selection[0].data.id
                                }
                            });
                            storeItems.loadData([], false);
                        }
                    }
                }, {
                    xtype: 'combobox',
                    editable: true,
                    fieldLabel: 'Grup',
                    labelWidth: 50,
                    emptyText: 'Trieu...',
                    store: storeGrupos,
                    displayField: 'nombreCa',
                    valueField: 'id',
                    queryMode: 'local',
                    name: 'comboFiltroGrupo',
                    margin: '0 10 0 0',
                    flex: 1,
                    listeners: {
                        select: function (combo, selection) {
                            this.up('panel').down("combobox[name=comboFiltroItem]").setValue(null);
                            storeItems.load({
                                url: '/crm/rest/item/',
                                params: {
                                    grupoId: selection[0].data.id
                                }
                            });
                        }
                    }
                }, {
                    xtype: 'combobox',
                    editable: true,
                    fieldLabel: 'Item',
                    labelWidth: 50,
                    emptyText: 'Trieu...',
                    store: storeItems,
                    displayField: 'nombreCa',
                    valueField: 'id',
                    queryMode: 'local',
                    name: 'comboFiltroItem',
                    flex: 1
                }]
            }, {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                margin: '5 10 0 0',
                items: [{
                    xtype: 'combobox',
                    store: 'StoreClientesDatosTiposClientes',
                    fieldLabel: 'Tipus de dades',
                    labelWidth: 100,
                    displayField: 'nombre',
                    valueField: 'id',
                    name: 'comboFiltroTipoDato',
                    queryMode: 'local',
                    emptyText: 'Trieu...',
                    margin: '0 10 0 0',
                    editable: true,
                    flex: 2,
                    listeners: {
                        select: function (combo, selection, lll) {
                            this.cambiaStoreComboValor(selection);
                        }
                    },
                    cambiaStoreComboValor: function (tipoDato) {
                        var data = [{
                            id: -1,
                            etiqueta: 'No tiene'
                        }, {
                            id: -2,
                            etiqueta: 'Tiene'
                        }];

                        if (tipoDato[0].data.clienteDatoTipoTipoId == CRM.model.enums.TiposDato.SELECCION.id) {
                            storeValor.load({
                                url: '/crm/rest/tipo/opciones/' + tipoDato[0].data.id,
                                params: {
                                    tipoDatoNombre: 'S'
                                },
                                scope: this,
                                callback: function () {
                                    this.up('panel').setEstadoComponentes(true);
                                    storeValor.loadData(data, true);
                                }
                            });

                        } else if (tipoDato[0].data.clienteDatoTipoTipoId == CRM.model.enums.TiposDato.FECHA.id) {
                            this.up('panel').setEstadoComponentes(false);
                        } else {
                            this.up('panel').setEstadoComponentes(true);
                            storeValor.loadData(data, false);
                        }
                    }

                }, {
                    xtype: 'combobox',
                    store: storeValor,
                    queryMode: 'local',
                    editable: true,
                    fieldLabel: 'valor',
                    displayField: 'etiqueta',
                    valueField: 'id',
                    emptyText: 'Trieu...',
                    labelWidth: 50,
                    name: 'textoFiltroValor',
                    flex: 1
                }, {
                    xtype: 'combobox',
                    editable: false,
                    emptyText: 'Trieu...',
                    store: 'StoreTiposFecha',
                    displayField: 'nombre',
                    valueField: 'id',
                    name: 'comboTipoFecha',
                    padding: 5,
                    hidden: true,
                    flex: 1,
                    listeners: {
                        select: function (combo, selection) {
                            this.up('panel').setEstadoCamposFechas(selection[0].data.id);
                        }
                    }
                }, {
                    xtype: 'combobox',
                    editable: false,
                    emptyText: 'Trieu...',
                    store: 'StoreTiposComparacion',
                    displayField: 'nombre',
                    valueField: 'id',
                    name: 'comboComparacion',
                    padding: 5,
                    hidden: true,
                    flex: 1,
                    listeners: {
                        select: function (combo, selection) {
                            var tipoComparacion = selection[0].data.id == 64;

                            if (this.up('panel').down("combobox[name=comboTipoFecha]").getValue() == 1) {
                                this.up('panel').down("combobox[name=comboMesesMax]").setVisible(tipoComparacion);
                            }
                            if (this.up('panel').down("combobox[name=comboTipoFecha]").getValue() == 2) {
                                this.up('panel').down("combobox[name=comboAnyosMax]").setVisible(tipoComparacion);
                            }
                            if (this.up('panel').down("combobox[name=comboTipoFecha]").getValue() == 3) {
                                this.up('panel').down("datefield[name=fechaValorMax]").setVisible(tipoComparacion);
                            }
                            if (!tipoComparacion) {
                                this.up('panel').down("combobox[name=comboAnyosMax]").setValue(null);
                                this.up('panel').down("combobox[name=comboMesesMax]").setValue(null);
                                this.up('panel').down("datefield[name=fechaValorMax]").setValue(null);
                            }

                        }
                    }
                }, {
                    xtype: 'combobox',
                    editable: false,
                    emptyText: 'Trieu...',
                    store: 'StoreMeses',
                    displayField: 'nombre',
                    valueField: 'id',
                    name: 'comboMeses',
                    padding: 5,
                    hidden: true,
                    flex: 1
                }, {
                    xtype: 'combobox',
                    editable: false,
                    emptyText: 'Trieu...',
                    store: 'StoreMeses',
                    displayField: 'nombre',
                    valueField: 'id',
                    name: 'comboMesesMax',
                    padding: 5,
                    hidden: true,
                    flex: 1
                }, {
                    xtype: 'combobox',
                    editable: false,
                    emptyText: 'Trieu...',
                    store: 'StoreAnyos',
                    displayField: 'id',
                    valueField: 'id',
                    name: 'comboAnyos',
                    padding: 5,
                    hidden: true,
                    flex: 1
                }, {
                    xtype: 'combobox',
                    editable: false,
                    emptyText: 'Trieu...',
                    store: 'StoreAnyos',
                    displayField: 'id',
                    valueField: 'id',
                    name: 'comboAnyosMax',
                    padding: 5,
                    hidden: true,
                    flex: 1
                }, {
                    xtype: 'datefield',
                    fieldLabel: 'Data',
                    name: 'fechaValorMin',
                    format: 'd/m/Y',
                    editable: false,
                    padding: 5,
                    labelWidth: 50,
                    hidden: true,
                    flex: 1
                }, {
                    xtype: 'datefield',
                    fieldLabel: 'Data Fí',
                    name: 'fechaValorMax',
                    format: 'd/m/Y',
                    editable: false,
                    padding: 5,
                    labelWidth: 50,
                    hidden: true,
                    flex: 1
                }]
            }, {
                xtype: 'combobox',
                editable: true,
                fieldLabel: 'Nacionalidad',
                labelWidth: 100,
                emptyText: 'Trieu...',
                store: Ext.create('CRM.store.StorePaises'),
                displayField: 'nombre',
                valueField: 'id',
                queryMode: 'local',
                margin: '5 10 0 0',
                name: 'comboFiltroNacionalidad',
                flex: 1
            },
                {
                    xtype: 'fieldset',
                    title: 'Direcció postal',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    margin: '5 10 0 0',
                    items: [{
                        xtype: 'combobox',
                        store: 'StoreComboBusqueda',
                        fieldLabel: 'Codi postal',
                        labelWidth: 100,
                        displayField: 'etiqueta',
                        valueField: 'id',
                        name: 'comboFiltroCodigoPostal',
                        queryMode: 'local',
                        emptyText: 'Trieu...',
                        margin: '0 10 0 0',
                        editable: true,
                        flex: 1
                    }, {
                        xtype: 'combobox',
                        editable: false,
                        fieldLabel: 'Pais',
                        labelWidth: 50,
                        emptyText: 'Trieu...',
                        store: Ext.create('CRM.store.StorePaises', {
                            listeners: {
                                load: function (store) {
                                    store.insert(0, new store.model({
                                        id: '-2',
                                        nombre: 'Tiene',
                                        orden: -2
                                    }));
                                    store.insert(0, new store.model({
                                        id: '-1',
                                        nombre: 'No tiene',
                                        orden: -1
                                    }));
                                }
                            }
                        }),
                        displayField: 'nombre',
                        valueField: 'id',
                        queryMode: 'local',
                        margin: '5 10 0 0',
                        name: 'comboFiltroPaisDomicilio',
                        flex: 1,
                        trigger2Cls: 'x-form-clear-trigger',
                        onTrigger2Click: function (event) {
                            var me = this;
                            me.clearValue();
                        }
                    }, {
                        xtype: 'combobox',
                        editable: false,
                        fieldLabel: 'Provincia',
                        labelWidth: 55,
                        emptyText: 'Trieu...',
                        store: Ext.create('CRM.store.StoreProvincias', {
                            autoLoad: true,
                            listeners: {
                                load: function (store) {
                                    store.insert(0, new store.model({
                                        id: '-2',
                                        nombre: 'Tiene',
                                        orden: -2
                                    }));
                                    store.insert(0, new store.model({
                                        id: '-1',
                                        nombre: 'No tiene',
                                        orden: -1
                                    }));
                                }
                            }
                        }),
                        displayField: 'nombre',
                        valueField: 'id',
                        queryMode: 'local',
                        margin: '5 10 0 0',
                        name: 'comboFiltroProvinciaDomicilio',
                        flex: 1,
                        trigger2Cls: 'x-form-clear-trigger',
                        onTrigger2Click: function (event) {
                            var me = this;
                            me.clearValue();
                        }
                    }, {
                        xtype: 'lookupcombobox',
                        labelWidth: 55,
                        fieldLabel: 'Poblacion',
                        emptyText: 'Trieu...',
                        appPrefix: 'crm',
                        bean: 'poblacion',
                        margin: '5 10 0 0',
                        flex: 1,
                        name: 'comboFiltroPoblacionDomicilio',
                        editable: false,
                        displayField: 'nombre',
                        valueField: 'id',
                        extraFields: ['Provincia'],
                        trigger2Cls: 'x-form-clear-trigger',
                        onTrigger2Click: function (event) {
                            var me = this;
                            me.clearValue();
                        }

                    }]
                }, {
                    xtype: 'fieldset',
                    title: 'Estudio UJI',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    margin: '5 10 0 0',
                    items: [{
                        xtype: 'lookupcombobox',
                        // labelWidth: 55,
                        fieldLabel: 'Estudio',
                        emptyText: 'Trieu...',
                        appPrefix: 'crm',
                        bean: 'clasificacionEstudio',
                        margin: '0 10 0 0',
                        flex: 1,
                        name: 'comboFiltroEstudioUJI',
                        editable: false,
                        displayField: 'nombre',
                        valueField: 'id',
                        extraFields: ['tipoEstudio'],
                        trigger2Cls: 'x-form-clear-trigger',
                        onTrigger2Click: function (event) {
                            var me = this;
                            me.clearValue();
                        }
                    }, {
                        xtype: 'combobox',
                        editable: false,
                        fieldLabel: 'Tipo Beca',
                        // labelWidth: 55,
                        margin: '0 10 0 0',
                        flex: 1,
                        emptyText: 'Trieu...',
                        displayField: 'nombre',
                        valueField: 'id',
                        name: 'comboFiltroEstudioUJIBeca',
                        store: Ext.create('CRM.store.StoreTiposBecas', {
                            autoLoad: true,
                            listeners: {
                                load: function (store) {
                                    store.insert(0, new store.model({
                                        id: '-2',
                                        nombre: 'Tiene',
                                        orden: -2
                                    }));
                                    store.insert(0, new store.model({
                                        id: '-1',
                                        nombre: 'No tiene',
                                        orden: -1
                                    }));
                                }
                            }
                        }),
                        multiSelect: true,
                        trigger2Cls: 'x-form-clear-trigger',
                        onTrigger2Click: function (event) {
                            var me = this;
                            me.clearValue();
                        },
                        listeners: {
                            change: function(combo, sel){
                                if (sel.includes("-1")){
                                    combo.setValue("-1");
                                }
                                if (sel.includes("-2")){
                                    combo.setValue("-2");
                                }
                            }
                        }
                    }]
                }, {
                    xtype: 'lookupcombobox',
                    // labelWidth: 55,
                    fieldLabel: 'Estudio No UJI',
                    emptyText: 'Trieu...',
                    appPrefix: 'crm',
                    bean: 'tipoEstudio',
                    margin: '5 10 0 0',
                    flex: 1,
                    name: 'comboFiltroEstudioNoUJI',
                    editable: false,
                    displayField: 'nombre',
                    valueField: 'id',
                    extraFields: ['tipoEstudio'],
                    trigger2Cls: 'x-form-clear-trigger',
                    onTrigger2Click: function (event) {
                        var me = this;
                        me.clearValue();
                    }

                }
            ]
        }];

        this.callParent(arguments);
    },
    listeners: {
        activate: function () {
            var combo = this.down("[name=comboFiltroCampanya]");
            var store = combo.getStore();
            store.load();
        }
    },

    setEstadoComponentes: function (esTexto) {
        this.down("combobox[name=textoFiltroValor]").setVisible(esTexto);
        this.down("combobox[name=comboTipoFecha]").setVisible(!esTexto);
        this.down("combobox[name=comboComparacion]").setVisible(!esTexto);
        this.down("datefield[name=fechaValorMin]").setVisible(false);
        this.down("datefield[name=fechaValorMax]").setVisible(false);
        this.down("combobox[name=comboMeses]").setVisible(false);
        this.down("combobox[name=comboAnyos]").setVisible(false);
        this.down("combobox[name=comboAnyosMax]").setVisible(false);
        this.down("combobox[name=comboMesesMax]").setVisible(false);
    },

    setEstadoCamposFechas: function (tipoFecha) {
        this.down("combobox[name=comboComparacion]").setValue(null);
        this.down("datefield[name=fechaValorMin]").setValue(null);
        this.down("datefield[name=fechaValorMax]").setValue(null);
        this.down("combobox[name=comboAnyos]").setValue(null);
        this.down("combobox[name=comboAnyosMax]").setValue(null);
        this.down("combobox[name=comboMeses]").setValue(null);
        this.down("combobox[name=comboMesesMax]").setValue(null);

        this.down("combobox[name=comboAnyosMax]").setVisible(false);
        this.down("combobox[name=comboMesesMax]").setVisible(false);
        this.down("datefield[name=fechaValorMax]").setVisible(false);

        if (tipoFecha == 1) {
            this.down("datefield[name=fechaValorMin]").setVisible(false);
            this.down("combobox[name=comboMeses]").setVisible(true);
            this.down("combobox[name=comboAnyos]").setVisible(false);
        }

        if (tipoFecha == 2) {
            this.down("datefield[name=fechaValorMin]").setVisible(false);
            this.down("combobox[name=comboMeses]").setVisible(false);
            this.down("combobox[name=comboAnyos]").setVisible(true);
        }

        if (tipoFecha == 3) {
            this.down("datefield[name=fechaValorMin]").setVisible(true);
            this.down("combobox[name=comboMeses]").setVisible(false);
            this.down("combobox[name=comboAnyos]").setVisible(false);
        }
    }
});