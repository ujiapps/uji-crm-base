Ext.define('CRM.view.busqueda.FiltroFechas',
    {
        extend: 'Ext.container.Container',
        alias: 'widget.filtroFechas',
        frame: true,
        //padding: 5,
        closable: false,
        layout: 'anchor',
        //arrayFechas: new Array(),

        initComponent: function () {
            var me = this;

            me.items = [
                {
                    xtype: 'container',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            //margin: '5 10 0 0',
                            items: [
                                {
                                    xtype: 'combobox',
                                    labelWidth: 120,
                                    editable: false,
                                    emptyText: 'Trieu...',
                                    store: 'StoreTiposFecha',
                                    displayField: 'nombre',
                                    valueField: 'id',
                                    name: 'comboTipoFecha',
                                    //padding: 5,
                                    flex: 1,
                                    listeners: {
                                        select: function (combo, selection) {
                                            this.up('container').up('container').up('container').setEstadoCamposFechas(selection[0].data.id);
                                        }
                                    }
                                },

                                {
                                    xtype: 'combobox',
                                    editable: false,
                                    emptyText: 'Trieu...',
                                    store: 'StoreTiposComparacion',
                                    displayField: 'nombre',
                                    valueField: 'id',
                                    name: 'comboComparacion',
                                    padding: '5 0 5 5',
                                    flex: 1,
                                    listeners: {
                                        select: function (combo, selection) {
                                            var tipoComparacion = selection[0].data.id == 64;

                                            if (this.up('container').down("combobox[name=comboTipoFecha]").getValue() == 1) {
                                                this.up('container').down("combobox[name=comboMesesMax]").setVisible(tipoComparacion);
                                            }
                                            if (this.up('container').down("combobox[name=comboTipoFecha]").getValue() == 2) {
                                                this.up('container').down("combobox[name=comboAnyosMax]").setVisible(tipoComparacion);
                                            }
                                            if (this.up('container').down("combobox[name=comboTipoFecha]").getValue() == 3) {
                                                this.up('container').down("datefield[name=fechaValorMax]").setVisible(tipoComparacion);
                                            }
                                            if (!tipoComparacion) {
                                                this.up('container').down("combobox[name=comboAnyosMax]").setValue(null);
                                                this.up('container').down("combobox[name=comboMesesMax]").setValue(null);
                                                this.up('container').down("datefield[name=fechaValorMax]").setValue(null);
                                            }

                                        }
                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    editable: false,
                                    emptyText: 'Trieu...',
                                    store: 'StoreMeses',
                                    displayField: 'nombre',
                                    valueField: 'id',
                                    name: 'comboMeses',
                                    padding: '5 0 5 5',
                                    hidden: true,
                                    flex: 1
                                },
                                {
                                    xtype: 'combobox',
                                    editable: false,
                                    emptyText: 'Trieu...',
                                    store: 'StoreMeses',
                                    displayField: 'nombre',
                                    valueField: 'id',
                                    name: 'comboMesesMax',
                                    padding: '5 0 5 5',
                                    hidden: true,
                                    flex: 1
                                },
                                {
                                    xtype: 'combobox',
                                    editable: false,
                                    emptyText: 'Trieu...',
                                    store: 'StoreAnyos',
                                    displayField: 'id',
                                    valueField: 'id',
                                    name: 'comboAnyos',
                                    padding: '5 0 5 5',
                                    hidden: true,
                                    flex: 1
                                },
                                {
                                    xtype: 'combobox',
                                    editable: false,
                                    emptyText: 'Trieu...',
                                    store: 'StoreAnyos',
                                    displayField: 'id',
                                    valueField: 'id',
                                    name: 'comboAnyosMax',
                                    padding: '5 0 5 5',
                                    hidden: true,
                                    flex: 1
                                },

                                {
                                    xtype: 'datefield',
                                    fieldLabel: 'Data',
                                    name: 'fechaValorMin',
                                    format: 'd/m/Y',
                                    editable: false,
                                    padding: '5 0 5 5',
                                    labelWidth: 50,
                                    hidden: true,
                                    flex: 1
                                },
                                {
                                    xtype: 'datefield',
                                    fieldLabel: 'Data Fí',
                                    name: 'fechaValorMax',
                                    format: 'd/m/Y',
                                    editable: false,
                                    padding: '5 0 5 5',
                                    labelWidth: 50,
                                    hidden: true,
                                    flex: 1
                                }]
                        }]
                }
            ];

            this.callParent(arguments);
        },

        setEstadoCamposFechas: function (tipoFecha) {
            this.down("combobox[name=comboComparacion]").setValue(null);
            this.down("datefield[name=fechaValorMin]").setValue(null);
            this.down("datefield[name=fechaValorMax]").setValue(null);
            this.down("combobox[name=comboAnyos]").setValue(null);
            this.down("combobox[name=comboAnyosMax]").setValue(null);
            this.down("combobox[name=comboMeses]").setValue(null);
            this.down("combobox[name=comboMesesMax]").setValue(null);

            this.down("combobox[name=comboAnyosMax]").setVisible(false);
            this.down("combobox[name=comboMesesMax]").setVisible(false);
            this.down("datefield[name=fechaValorMax]").setVisible(false);

            if (tipoFecha == 1) {
                this.down("datefield[name=fechaValorMin]").setVisible(false);
                this.down("combobox[name=comboMeses]").setVisible(true);
                this.down("combobox[name=comboAnyos]").setVisible(false);
            }

            if (tipoFecha == 2) {
                this.down("datefield[name=fechaValorMin]").setVisible(false);
                this.down("combobox[name=comboMeses]").setVisible(false);
                this.down("combobox[name=comboAnyos]").setVisible(true);
            }

            if (tipoFecha == 3) {
                this.down("datefield[name=fechaValorMin]").setVisible(true);
                this.down("combobox[name=comboMeses]").setVisible(false);
                this.down("combobox[name=comboAnyos]").setVisible(false);
            }

        },

        dameObjeto: function () {
            return {
                fecha: this.down("datefield[name=fechaValorMin]").getValue(),
                fechaMax: this.down("datefield[name=fechaValorMax]").getValue(),
                tipoComparacion: this.down("combobox[name=comboComparacion]").getValue(),
                tipoFecha: this.down("combobox[name=comboTipoFecha]").getValue(),
                mes: this.down("combobox[name=comboMeses]").getValue(),
                mesNombre: this.down("combobox[name=comboMeses]").getRawValue(),
                anyo: this.down("combobox[name=comboAnyos]").getValue(),
                mesMax: this.down("combobox[name=comboMesesMax]").getValue(),
                anyoMax: this.down("combobox[name=comboAnyosMax]").getValue()
            };

        },

        esCampoFechaValido : function(obj){
            return (obj.tipoFecha != null && obj.tipoComparacion != null && (obj.mes != null || obj.anyo != null || obj.fecha != null));
        },

        esCampoFechaVacio : function(obj){
            return (obj.tipoFecha == null && obj.tipoComparacion == null && obj.mes== null && obj.anyo == null && obj.fecha == null);
        },

        campoValido: function (valor, num_caracteres) {
            if (valor != null)
                return valor.length >= num_caracteres || valor == -1 || valor == -2;
            else
                return false;
        },

        toString: function (objeto) {
            var texto = '';
            if (objeto.tipoFecha != null) {
                if (objeto.tipoFecha == CRM.model.enums.TiposFechas.MENSUAL) {
                    texto = texto + ' Mes ';
                }
                if (objeto.tipoFecha == CRM.model.enums.TiposFechas.ANUAL) {
                    texto = texto + ' Any ';
                }
                if (objeto.tipoFecha == CRM.model.enums.TiposFechas.COMPLETA) {
                    texto = texto + ' Data completa ';
                }
                if (objeto.tipoComparacion != null) {
                    if (objeto.tipoComparacion == CRM.model.enums.TiposComparaciones.MAYORQUE) {
                        texto = texto + ' mayor que ';
                    }
                    if (objeto.tipoComparacion == CRM.model.enums.TiposComparaciones.MENORQUE) {
                        texto = texto + ' menor que ';
                    }
                    if (objeto.tipoComparacion == CRM.model.enums.TiposComparaciones.IGUAL) {
                        texto = texto + ' igual a ';
                    }
                    if (objeto.tipoComparacion == CRM.model.enums.TiposComparaciones.MAYOROIGUAL) {
                        texto = texto + ' mayor o igual a ';
                    }
                    if (objeto.tipoComparacion == CRM.model.enums.TiposComparaciones.MENOROIGUAL) {
                        texto = texto + ' menor o igual a ';
                    }
                    if (objeto.tipoComparacion == CRM.model.enums.TiposComparaciones.ENTRE) {
                        texto = texto + ' entre ';
                    }

                }

                if (objeto.fecha != null) {
                    texto = texto + Ext.Date.dateFormat(objeto.fecha, 'd/m/Y').toString();
                }
                if (objeto.mes != null) {
                    texto = texto + ' ' + objeto.mesNombre + ' ';
                }

                if (objeto.anyo != null) {
                    texto = texto + objeto.anyo;
                }

                if (objeto.tipoComparacion != null) {
                    if (objeto.tipoComparacion == CRM.model.enums.TiposComparaciones.ENTRE) {
                        texto = texto + ' y ';

                        if (objeto.fechaMax != null) {
                            texto = texto + Ext.Date.dateFormat(objeto.fechaMax, 'd/m/Y').toString();
                        }

                        if (objeto.mesMax != null) {
                            texto = texto + objeto.mesMax;
                        }

                        if (objeto.anyoMax != null) {
                            texto = texto + objeto.anyoMax;
                        }
                    }
                }
            }

            return texto;
        },

        limpiarCampos: function () {

            this.down("datefield[name=fechaValorMin]").setValue(null);
            this.down("datefield[name=fechaValorMax]").setValue(null);
            this.down("combobox[name=comboComparacion]").setValue(null);
            this.down("combobox[name=comboTipoFecha]").setValue(null);
            this.down("combobox[name=comboMeses]").setValue(null);
            this.down("combobox[name=comboAnyos]").setValue(null);
            this.down("combobox[name=comboMesesMax]").setValue(null);
            this.down("combobox[name=comboAnyosMax]").setValue(null);

            this.down("combobox[name=comboAnyos]").setVisible(false);
            this.down("combobox[name=comboMeses]").setVisible(false);
            this.down("datefield[name=fechaValorMin]").setVisible(false);

            this.down("combobox[name=comboAnyosMax]").setVisible(false);
            this.down("combobox[name=comboMesesMax]").setVisible(false);
            this.down("datefield[name=fechaValorMax]").setVisible(false);

            //while (this.arrayFechas.length > 0) {
            //    this.arrayFechas.pop();
            //}
            //
            //var objeto =
            //{
            //    arrayFechas: null
            //};

            //this.up('panel').down('grid').getStore().getProxy().extraParams = objeto;

            //this.up('panel').down('grid').getStore().load({
            //    params: {
            //        start: 0,
            //        page: 0
            //    }
            //});
        }
    });