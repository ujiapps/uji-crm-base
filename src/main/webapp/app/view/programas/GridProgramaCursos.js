Ext.define('CRM.view.programas.GridProgramaCursos',
{
    extend : 'Ext.grid.Panel',
    store : 'StoreProgramaCursos',
    alias : 'widget.gridProgramaCursos',
    requires : [ 'Ext.grid.plugin.RowEditing' ],
    sortableColumns : true,
    disabled : true,
    selModel :
    {
        mode : 'SINGLE'
    },

    plugins : [
    {
        ptype : 'rowediting',
        clicksToEdit : 2,
        pluginId : 'editingProgramaCurso'
    } ],

    tbar : [
    {
        xtype : 'button',
        name : 'anyadirUbicacion',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarUbicacion',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'id',
        hidden : true,
        dataIndex : 'id',
        menuDisabled : true
    },
    {
        text : 'nombre',
        dataIndex : 'uestNombre',
        menuDisabled : true,
        flex : 3,
        editor :
        {
            xtype : 'combobox',
            store : 'StoreUbicaciones',
            editable : false,
            displayField : 'nombre',
            valueField : 'id',
            queryMode : 'local',
            allowBlank : false
        }
    } ]
});
