Ext.define('CRM.view.programas.GridPersonas',
{
    extend : 'Ext.grid.Panel',
    store : 'StoreProgramaUsuario',
    alias : 'widget.gridProgramasPersonas',
    requires : [ 'Ext.grid.plugin.RowEditing' ],
    sortableColumns : true,
    disabled : true,
    selModel :
    {
        mode : 'SINGLE'
    },
    plugins : [
    {
        ptype : 'rowediting',
        clicksToEdit : 2
    } ],
    tbar : [
    {
        xtype : 'button',
        name : 'anyadirProgramasPersona',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarProgramasPersona',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'id',
        hidden : true,
        dataIndex : 'id',
        menuDisabled : true,
        flex : 1

    },
    {
        text : 'Persona',
        dataIndex : 'nombrePersona',
        menuDisabled : true,
        flex : 3
    },
    {
        text : 'Tipus',
        dataIndex : 'tipo',
        name : 'comboTipos',
        flex : 1,
        editor :
        {
            xtype : 'combobox',
            store : 'StoreTiposProgramasUsuarios',
            editable : false,
            displayField : 'nombre',
            valueField : 'nombre',
            queryMode : 'local',
            allowBlank : false
        }
    } ]

});
