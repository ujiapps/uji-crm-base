Ext.define('CRM.view.programas.VentanaNewPersona',
{
    extend : 'Ext.Window',
    title : 'Afegir una nova persona',
    width : 400,
    alias : 'widget.ventanaNewProgramaPersona',
    layout : 'fit',
    modal : true,

    buttonAlign : 'right',
    bbar : [
    {
        xtype : 'tbfill'
    },
    {
        xtype : 'button',
        text : 'Guardar',
        action : 'guardar-persona'
    },
    {
        xtype : 'button',
        text : 'Cancel·lar',
        action : 'cancelar'
    } ],

    items : [
    {
        xtype : 'form',
        name : 'formNewProgramaPersona',
        padding : 10,
        layout : 'fit',
        items : [
        {
            xtype : 'lookupcombobox',
            appPrefix : 'crm',
            bean : 'personaPasPdi',
            name : 'comboProgramaPersonas',
            padding : 20,
            fieldLabel : 'Persona',
            labelWidth : 75,
            editable : false,
            allowBlank : false,
            displayField : 'nombre',
            valueField : 'id'
        } ]
    } ]

});