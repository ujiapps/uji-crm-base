Ext.define('CRM.view.programas.GridProgramas', {
    extend: 'Ext.grid.Panel',
    title: 'Programes',
    alias: 'widget.gridProgramas',
    store: 'StoreProgramas',
    requires: ['Ext.grid.plugin.RowEditing'],
    forceFit: true,
    sortableColumns: true,

    selModel:
        {
            mode: 'SINGLE'
        },

    plugins: [
        {
            ptype: 'rowediting',
            clicksToEdit: 2,
            pluginId: 'editingPrograma'
        }],

    tbar: [
        {
            xtype: 'button',
            name: 'anyadirPrograma',
            text: 'Afegir',
            iconCls: 'application-add'
        },
        {
            xtype: 'button',
            name: 'borrarPrograma',
            text: 'Esborrar',
            iconCls: 'application-delete',
            disabled: true
        }],

    columns: [{
        text: 'Nom',
        dataIndex: 'nombreCa',
        flex: 2,
        editor: {
            allowBlank: false
        }
    }, {
        text: 'Nombre',
        dataIndex: 'nombreEs',
        flex: 2,
        editor: {
            allowBlank: true
        }
    }, {
        text: 'Name',
        dataIndex: 'nombreUk',
        flex: 2,
        editor: {
            allowBlank: true
        }
    }, {
        text: 'Plantilla web',
        dataIndex: 'plantilla',
        flex: 2,
        editor: {
            allowBlank: true
        }
    }, {
        text: 'Servei',
        dataIndex: 'servicioNombre',
        name: 'comboServicios',
        flex: 2,
        editor: {
            xtype: 'combobox',
            store: 'StoreServiciosByUsuarioId',
            editable: false,
            displayField: 'nombre',
            valueField: 'id',
            allowBlank: false,
            emptyText: 'Selecciona un Servei'
        }
    }, {
        text: 'Ordre',
        dataIndex: 'orden',
        flex: 1,
        editor: {
            allowBlank: false
        }
    }, {
        text: 'Visible',
        dataIndex: 'visible',
        xtype: 'booleancolumn',
        trueText: 'Sí',
        falseText: 'No',
        flex: 1,
        editor: {
            xtype: 'checkbox'
        }

    }, {
        text: 'Tipo Access',
        dataIndex: 'tipoAccesoNombre',
        name: 'comboAccesos',
        flex: 1,
        editor: {
            xtype: 'combobox',
            store: 'StoreTiposAcceso',
            editable: false,
            displayField: 'nombre',
            valueField: 'id',
            allowBlank: false,
            emptyText: 'Selecciona un tipus de access'
        }
    }, {
        text: 'Id',
        dataIndex: 'id',
        name: 'id',
        hidden: true

    }]
});
