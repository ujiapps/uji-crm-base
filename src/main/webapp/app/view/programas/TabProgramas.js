Ext.define('CRM.view.programas.TabProgramas',
    {
        extend: 'Ext.tab.Panel',
        alias: 'widget.tabProgramas',
        requires: ['CRM.view.programas.GridPersonas', 'CRM.view.programas.GridProgramaPerfiles', 'CRM.view.programas.FormProgramas'],
        activeTab: 0,
        //    disabled : true,

        items: [
            {
                title: 'Persones',
                xtype: 'gridProgramasPersonas',
                closable: false
            },
            {
                title: 'Perfils',
                xtype: 'gridProgramaPerfiles',
                closable: false
            },
            {
                title: 'Formulario',
                xtype: 'formularioProgramas',
                closable: false
            },
            {
                title: 'Cursos',
                xtype: 'gridProgramaCursos',
                closable: false
            }]
    });