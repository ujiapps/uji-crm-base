Ext.define('CRM.view.programas.GridProgramaPerfiles',
{
    extend : 'Ext.grid.Panel',
    store : 'StoreProgramaPerfiles',
    alias : 'widget.gridProgramaPerfiles',
    requires : [ 'Ext.grid.plugin.RowEditing' ],
    sortableColumns : true,
    disabled : true,
    selModel :
    {
        mode : 'SINGLE'
    },

    plugins : [
    {
        ptype : 'rowediting',
        clicksToEdit : 2,
        pluginId: 'editingProgramaPerfil'
    } ],

    tbar : [
    {
        xtype : 'button',
        name : 'anyadir',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrar',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'id',
        hidden : true,
        dataIndex : 'id',
        menuDisabled : true
    },
    {
        text : 'programaId',
        hidden : true,
        dataIndex : 'programaId',
        menuDisabled : true
    },
    {
        text : 'Perfil',
        dataIndex : 'perfilNombre',
        menuDisabled : true,
        flex : 3,
        editor :
        {
            xtype : 'combobox',
            store : 'StorePerfiles',
            editable : false,
            displayField : 'nombre',
            valueField : 'id',
            queryMode : 'local',
            allowBlank : false
        }
    },
    {
        text : 'Access',
        dataIndex : 'tipoAcceso',
        name : 'comboTipos',
        flex : 1,
        editor :
        {
            xtype : 'combobox',
            store : 'StoreTipoAccesosProgramaPerfil',
            editable : false,
            displayField : 'nombre',
            valueField : 'nombre',
            allowBlank : false
        }
    } ]
});
