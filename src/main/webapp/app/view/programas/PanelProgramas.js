Ext.define('CRM.view.programas.PanelProgramas',
{
    extend : 'Ext.panel.Panel',
    title : 'Programes',
    requires : [ 'CRM.view.programas.GridProgramas', 'CRM.view.programas.TabProgramas' ],
    alias : 'widget.panelProgramas',
    closable : true,

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'gridProgramas',
        flex : 1
    },
    {
        xtype : 'tabProgramas',
        flex : 2
    } ]

});