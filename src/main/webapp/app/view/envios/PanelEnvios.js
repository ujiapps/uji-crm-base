Ext.define('CRM.view.envios.PanelEnvios', {
    extend: 'Ext.panel.Panel',
    title: 'Enviaments',
    alias: 'widget.panelEnvios',
    name : 'panelEnvios',
    envioId: '',
    envioTipoId: '',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    closable: true,
    defaults: {
        border: 0
    },
    items: [{
        xtype: 'panel',
        name: 'formEtiquetaEnvioSeguimiento',
        height: 35,
        rbar: [{
            xtype: 'container',
            padding: 3,
            items: [{
                xtype: 'button',
                name: 'borrarRuta',
                text: 'Esborrar ruta'
            }]
        }],
        items: [{
            xtype: 'displayfield',
            name: 'etiquetaEnvioSeguimiento',
            padding: 5
        }]
    }, {
        xtype: 'filtroEnvios',
        border: 1
    },
        //     {
        //
        //     xtype: 'gridEnvios',
        //     height: 150,
        //     border: 1,
        //     resizable: true
        //
        // },
        {
            xType: 'container',
            tbar: [{
                xtype: 'button',
                text: 'Nou mail',
                name: 'nuevoMail',
                iconCls: 'application-add'
            }, {
                xtype: 'button',
                text: 'Nou postal',
                name: 'nuevoPostal',
                iconCls: 'application-add'
            }],

            border: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            name: 'datosEnvio',
            flex: 1,
            items: [{
                xtype: 'container',
                flex: 3,
                border: 0,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [{
                    xtype: 'panelEnvioMail',
                    title: 'Mail',
                    border: 0,
                    itemId: 'mail',
                    name: 'panelEnvioMail',
                    hidden: true

                }, {
                    xtype: 'formEnvioPostal',
                    title: 'Postal',
                    itemId: 'postal',
                    border: 0,
                    name: 'formEnvioPostal',
                    hidden: true
                }]
            }]
        }]
});