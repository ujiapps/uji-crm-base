Ext.define('CRM.view.envios.VentanaBusquedaCriterio', {
    extend: 'Ext.Window',
    alias: 'widget.ventanaBusquedaCriterio',
    name: 'ventanaBusquedaCriterio',
    modal: true,
    title: 'Busqueda',
    frame: true,
    border: 0,
    padding: 10,
    width: '80%',
    requires: ['CRM.view.envios.FiltroUsuario.FieldsetDatosUsuario', 'CRM.view.envios.FiltroUsuario.FieldsetEstudios',
        'CRM.view.envios.FiltroUsuario.FieldsetCampanyas'],

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    fbar: [{
        xtype: 'button',
        name: 'botonLimpiaBusquedaCriterio',
        text: 'Limpiar filtro',
        listeners: {
            click: function () {
                var window = this.up("window");
                window.limpiaBusquedaCriterio();
                window.down("[name=fieldsetEstudios]").limpiaFieldsetEstudios();
                window.down("[name=fieldsetDatosUsuario]").limpiaFieldsetDatosUsuario();
                window.down("[name=fieldsetCampanyas]").limpiaFieldsetCampanya();
            }
        }
    }, {
        xtype: 'button',
        name: 'botonEliminarBusquedaEnvioCriterio',
        text: 'Eliminar filtro'
    }, {
        xtype: 'button',
        name: 'botonGuardarBusquedaEnvioCriterio',
        icon: window.location.protocol + '//static.uji.es/js/extjs/uji-commons-extjs/img/find.png',
        maxWidth: 25,
        text: '',
        listeners: {
            click: function () {
                var window = this.up("window"),
                    form = window.down("form[name=panelBusqueda]"),
                    store = window.storeEnviosClientes;

                form.submit({
                    url: '/crm/rest/enviocriterio/envio/' + window.envioId,
                    scope: this,
                    success: function () {
                        window.destroy();
                        store.load();
                    },
                    failure: function (x, resp) {
                        Ext.Msg.alert('Busqueda de usuaris', '<b>' + resp + '</b>');
                    }
                });
            }
        }

    }],

    listeners: {

        show: function () {
            this.setLoading(true);
            var ventana = this,
                form = ventana.down("form[name=panelBusqueda]"),
                comboPrograma = form.down("combobox[name=programaId]"),
                storeCampanyas = Ext.create('CRM.store.StoreCampanyas', {
                    storeId: 'storeCampanyasParaFiltro',
                    autoLoad: false
                }).load({
                    callback: function () {
                        comboPrograma.getStore().filterBy(function (record) {
                            return storeCampanyas.find("programaId", record.data.id) != -1;
                        });
                        ventana.setLoading(false);
                    }
                });
        }
    },
    items: [{
        xtype: 'form',
        frame: true,
        name: 'panelBusqueda',
        defaults: {
            padding: '2 5 2 5',
        },
        items: [{
            xtype: 'fieldsetCampanyas'
        }, {
            xtype: 'fieldset',
            title: 'Items',
            layout: {
                type: 'hbox',
                align: 'fit'
            },
            defaults: {padding: 2},
            items: [{
                xtype: 'combobox',
                fieldLabel: 'Cercles',
                name: 'criterioCirculoId',
                store: Ext.create('CRM.store.StoreItems'),
                displayField: 'nombreCa',
                valueField: 'id',
                queryMode: 'local',
                multiSelect: true,
                flex: 1
            }]
        }, {
            xtype: 'fieldsetDatosUsuario'
        }, {
            xtype: 'fieldsetEstudios',
            name: 'fieldsetEstudios'
        }]
    }],

    cargaEstadosCampanya: function (campanyaId) {
        if (campanyaId !== null) {
            var comboCampanyaEstado = this.down("combo[name=tipoEstadoCampanyaId]");
            comboCampanyaEstado.setValue(null);
            comboCampanyaEstado.getStore().load({
                url: '/crm/rest/tipoestadocampanya/campanya',
                params: {
                    campanyaId: campanyaId
                },
                callback: function () {
                    if (comboCampanyaEstado.getStore().data.length > 0) {
                        comboCampanyaEstado.setDisabled(false);
                    } else {
                        comboCampanyaEstado.setDisabled(true);
                    }
                }
            });
        }
    }
    ,

    limpiaBusquedaCriterio: function () {
        var form = this.down("form");
        form.down("[name=tipoEstadoCampanyaId]").setValue(null);
        form.down("[name=tipoEstadoCampanyaId]").setDisabled(true);
        form.down("[name=campanyaId]").setValue(null);
        form.down("[name=campanyaId]").getStore().clearFilter();
        form.down("[name=campanyaId]").setDisabled(true);
        form.down("[name=programaId]").setValue(null);
        form.down("[name=criterioCirculoId]").setValue(null);
        form.down("[id=combinadoCampanyas1]").setValue(false);
        form.down("[id=combinadoCampanyas3]").setValue(false);
    }

})
;