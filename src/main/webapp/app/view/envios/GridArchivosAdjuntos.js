Ext.define('CRM.view.envios.GridArchivosAdjuntos',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridArchivosAdjuntos',
        store: 'StoreEnviosAdjuntos',
        sortableColumns: true,
        autoScroll: true,
        hideHeaders: true,
        defaults: {
            border: 0
        },
        viewConfig: {
            enableTextSelection: true
        },
        selModel: {
            mode: 'SINGLE'
        },

        tbar: [

            {
                xtype: 'filefield',
                fieldLabel: 'Fitxer',
                labelWidth: 50,
                name: 'anyadirAdjunto',
                buttonConfig: {
                    text: 'Sel·lecciona...',
                    disabled: false
                },
                flex: 5
            },
            {
                xtype: 'button',
                name: 'botonAnyadirAdjunto',
                iconCls: 'accept',
                disabled: true,
                flex: 0.5

            },
            {
                xtype: 'button',
                name: 'botonBorrarAdjunto',
                iconCls: 'application-delete',
                disabled: true,
                flex: 0.5

            },
            {
                xtype: 'button',
                name: 'botonDescargarAdjunto',
                iconCls: 'arrow-down',
                disabled: true,
                flex: 0.5

            }],

        columns: [
            {
                text: 'id',
                dataIndex: 'id',
                hidden: true
            },
            {
                dataIndex: 'nombreFichero',
                flex: 2
            },
            {
                dataIndex: 'referencia',
                text: 'Link',
                flex: 1,
                renderer: function (value) {
                    return 'https://ujiapps.uji.es/ade/rest/storage/' + value;
                }

            }]

    });
