Ext.define('CRM.view.envios.FormEnvioPostal', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.formEnvioPostal',
    frame: true,
    labelAlign: 'left',
    autoScroll: true,
    border: 0,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    flex: 1,
    fbar: [{
        xtype: 'button',
        name: 'imprimirListado',
        text: 'Imprimir Llistat'
    }, {
        xtype: 'button',
        name: 'imprimirListadoPostal',
        text: 'Imprimir Cuadricula 2x7'
    }, {
        xtype: 'button',
        name: 'guardar',
        text: 'Guardar'
    }, {
        xtype: 'button',
        name: 'borrar',
        text: 'Esborrar'
    }],
    items: [{
        xtype: 'textfield',
        fieldLabel: 'Nom',
        name: 'nombre',
        allowBlank: false
    }, {
        title: 'Criteris',
        xtype: 'panelCriterios',
        // border: 0
        // width: 400,
    }, {
        xtype: 'gridEnvioClientesPostal',
        name: 'gridEnvioClientesPostal',
        //        autoScroll : true,
        padding: '10 0 0 0',
        flex: 1
    }]
});