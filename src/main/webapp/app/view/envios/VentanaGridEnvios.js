Ext.define('CRM.view.envios.VentanaGridEnvios', {
    extend: 'Ext.Window',
    title: 'Envios',
    width: 600,
    alias: 'widget.ventanaGridEnvios',
    layout: 'fit',
    modal: true,

    items: [{
        xtype: 'gridEnvios',
        name: 'gridEnvios'
    }]

});