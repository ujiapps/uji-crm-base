function formatDate(value) {
    return value ? Ext.Date.dateFormat(value, 'd/m/Y') : '';
}

Ext.define('CRM.view.envios.GridEnvios', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridEnvios',
    store: 'StoreEnvios',
    sortableColumns: true,
    autoScroll: true,
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'StoreEnvios',
        dock: 'bottom',
        displayInfo: true
    }],
    viewConfig: {
        getRowClass: function (record) {
            if (record.get('fechaEnvioPlataforma') != null) {
                return "grid-row-not-avaible";
            }
            if (record.get('nombre') == 'NOU') {
                return "hidden";
            }
        }
    },
    selModel: {
        mode: 'SINGLE'
    },

    columns: [{
        text: 'id',
        dataIndex: 'id',
        hidden: true
    }, {
        text: 'Nom',
        dataIndex: 'nombre',
        flex: 2
    }, {
        text: 'Asumpte',
        dataIndex: 'asunto',
        flex: 2
    }, {
        text: 'Data',
        dataIndex: 'fechaEnvioDestinatario',
        format: 'd/m/Y',
        renderer: formatDate,
        flex: 1
    }, {
        text: 'Data Enviament',
        dataIndex: 'fechaEnvioPlataforma',
        format: 'd/m/Y',
        renderer: formatDate,
        flex: 1
    }, {
        text: 'Tipus',
        dataIndex: 'envioTipoNombre',
        flex: 1
    }]
});
