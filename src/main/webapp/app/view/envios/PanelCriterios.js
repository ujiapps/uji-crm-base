Ext.define('CRM.view.envios.PanelCriterios', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panelCriterios',
    name: 'panelCriterios',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [{
        xtype: 'grid',
        name: 'gridCriterios',
        border: 0,
        store: 'StoreEnviosCriterios',

        tbar: [{

            xtype: 'button',
            name: 'anyadir',
            text: 'Afegir',
            iconCls: 'application-add',
            value: 1
        }, {

            xtype: 'button',
            name: 'borrarCriterio',
            text: 'Esborrar',
            iconCls: 'application-delete',
            value: 1,
            listeners: {
                click: function () {
                    this.up("[name=panelCriterios]").borrarCriterioEnvio();
                }
            }


        }],
        columns: [{
            text: 'Nom',
            dataIndex: 'nombre',
            flex: 1
        }]
    }],

    borrarCriterioEnvio: function () {

        var store = this.down("[name=gridCriterios]").getStore();
        var grid = this.down("[name=gridCriterios]");

        Ext.Msg.confirm('Eliminació de enviament', '<b>Estàs segur d\'esborrar el criteri associat a l\'enviament?</b>', function (btn) {
            if (btn === 'yes') {
                grid.setLoading(true);
                store.remove(grid.getSelectionModel().getSelection());
                store.sync({
                    callback: function (res) {
                        if (res.length === 1) {
                            if (res[0].data.origen === 'ConsultaSQL') {
                                grid.down("button[name=anyadir]").setDisabled(true);
                            }
                            else {
                                grid.down("button[name=anyadir]").setDisabled(false);
                            }
                        }
                        else {
                            grid.down("button[name=anyadir]").setDisabled(false);
                        }

                        grid.setLoading(false);
                    }
                });
            }
        })
    }
})
;