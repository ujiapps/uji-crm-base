Ext.define('CRM.view.envios.GridEnvioClientesMail', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridEnvioClientesMail',
    store: 'StoreEnviosClientesVista',
    sortableColumns: true,
    selModel: {
        mode: 'SINGLE'
    },

    tbar: [{
        xtype: 'button',
        name: 'botonAnyadirClienteEnvio',
        text: 'Afegir client',
        iconCls: 'application-add'
    }, {
        xtype: 'button',
        name: 'botonEliminarClienteEnvio',
        text: 'Eliminar de l\'enviament',
        iconCls: 'application-delete',
        disabled: true,
        listeners: {
            click: function () {
                var panel = this.up("panel[name=panelEnvios]"),
                    grid = panel.down("grid[name=gridEnvioClientesMail]");
                Ext.Ajax.request({
                    url: '/crm/rest/enviocriterio/envio/cliente/',
                    method: 'DELETE',
                    params: {
                        envioId: panel.envioId,
                        clienteId: grid.getSelectionModel().getSelection()[0].data.id
                    },
                    scope: this,
                    success: function () {
                        grid.setLoading(true);
                        grid.getStore().load({
                            callback: function () {
                                grid.setLoading(false);
                            }
                        });
                    },
                    failure: function () {

                    }
                })
            }
        }
    }, '->', {
        xtype: 'button',
        name: 'botonBusquedaEnvioCriterio',
        text: 'Busqueda d\'usuaris',
        icon: window.location.protocol + '//static.uji.es/js/extjs/uji-commons-extjs/img/find.png'
    }],

    listeners: {
        selectionchange: function () {
            this.setEstadoComponentes();
        }
    },

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'StoreEnviosClientesVista',
        dock: 'bottom',
        displayInfo: true
    }],

    columns: [{
        dataIndex: 'nombre',
        text: 'Nom',
        flex: 1
    }, {
        text: 'Correo',
        dataIndex: 'correo',
        flex: 2
    }],

    setEstadoComponentes: function () {

        var tieneSeleccionGrid = this.getSelectionModel().hasSelection();
        this.down("button[name=botonEliminarClienteEnvio]").setDisabled(!tieneSeleccionGrid);
    }
});
