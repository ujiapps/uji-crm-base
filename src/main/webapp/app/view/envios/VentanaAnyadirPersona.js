Ext.define('CRM.view.envios.VentanaAnyadirPersona',
{
    extend : 'Ext.Window',
    title : 'Afegir una nova persona',
    width : 400,
    alias : 'widget.ventanaAnyadirPersona',
    layout : 'fit',
    modal : true,

    buttonAlign : 'right',
    bbar : [
    {
        xtype : 'tbfill'
    },
    {
        xtype : 'button',
        text : 'Guardar',
        action : 'guardar-persona'
    },
    {
        xtype : 'button',
        text : 'Cancel·lar',
        action : 'cancelar'
    } ],

    items : [
    {
        xtype : 'form',
        name : 'formNewPersona',
        layout : 'fit',
        padding : 10,
        items : [
        {
            xtype : 'lookupcombobox',
            appPrefix : 'crm',
            bean: 'cliente',
            name : 'comboPersona',
            padding : 20,
            fieldLabel : 'Persona',
            labelWidth : 75,
            editable : false,
            allowBlank : false,
            displayField : 'nombre',
            valueField : 'id',
            extraFields: ['Servicio']
        } ]
    } ]

});