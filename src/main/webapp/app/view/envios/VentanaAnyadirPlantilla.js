Ext.define('CRM.view.envios.VentanaAnyadirPlantilla', {
    extend: 'Ext.Window',
    width: 400,
    alias: 'widget.ventanaAnyadirPlantilla',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        padding: 5
    },
    bbar: ['->',
        {
            xtype: 'button',
            text: 'Afegir',
            action: 'anyadir-plantilla',
            listeners: {
                click: function () {
                    var ventana = this.up("window"),
                        comboPlantilla = ventana.down("[name=comboPlantilla]");
                    ventana.editor.setValue(comboPlantilla.findRecord("id", comboPlantilla.getValue()).data.plantilla + ventana.editor.getValue());
                    ventana.destroy();
                }
            }
        },
        {
            xtype: 'button',
            text: 'Cancel·lar',
            action: 'cancelar'
        }],

    items: [{
        xtype: 'combobox',
        region: 'north',
        flex: 1,
        name: 'comboPlantilla',
        fieldLabel: 'Plantilla',
        editable: false,
        allowBlank: true,
        trigger2Cls: 'x-form-clear-trigger',
        onTrigger2Click: function (event) {
            var me = this;
            me.setValue(null);
        },
        displayField: 'nombre',
        valueField: 'id',
        store: Ext.create('CRM.store.StoreEnviosPlantillas', {itemId: 'comboEnvioPlantillas'})
    }]

})
;