Ext.define('CRM.view.envios.FiltroEnvios', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.filtroEnvios',
    frame: true,

    border: 0,
    closable: false,
    layout: 'anchor',
    fbar: [{
        xtype: 'button',
        text: 'Netejar Filtres',
        name: 'limpiarFiltros',
        margin: '1 20 0 0'
    }, {
        xtype: 'button',
        name: 'buscar',
        icon: '//static.uji.es/js/extjs/uji-commons-extjs/img/find.png',
        maxWidth: 23
    }],
    items: [{
        xtype: 'container',
        layout: 'hbox',
        padding: '5 0 0 0',
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Cerca',
            name: 'busqueda',
            margin: '0 15 0 0',
            labelWidth: 50
        }, {
            border: 0,
            xtype: 'combobox',
            editable: false,
            margin: '0 10 0 0',
            fieldLabel: 'Tipus',
            labelWidth: 50,
            emptyText: 'Trieu...',
            store: 'StoreTiposEnvios',
            displayField: 'nombre',
            valueField: 'id',
            name: 'envioTipoId'
        }]
    }, {
        xtype: 'panel',
        border: 0,
        padding: '5 0 0 0',
        items: [{
            title: 'Opcions avançades',
            xtype: 'filtroEnviosAvanzado',
            collapsible: true,
            collapsed: true,
            animCollapse: true,
            collapseDirection: 'bottom'
        }]
    }]
});