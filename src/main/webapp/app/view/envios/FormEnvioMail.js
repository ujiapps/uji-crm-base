Ext.define('CRM.view.envios.FormEnvioMail', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.formEnvioMail',
    requires: ['Ext.ux.CKEditor'],
    frame: true,
    labelAlign: 'left',
    autoScroll: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    border: 0,

    views: ['envios.VentanaAnyadirPlantilla'],

    items: [{
        xtype: 'textfield',
        fieldLabel: 'Nom',
        name: 'nombre',
        allowBlank: false
    }, {
        xtype: 'textfield',
        fieldLabel: 'Asumpte',
        name: 'asunto',
        allowBlank: false
    }, {
        xtype: 'container',
        layout: 'hbox',
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Desde',
            name: 'desde',
            flex: 1
        }, {
            xtype: 'textfield',
            fieldLabel: 'Responder a',
            padding: '0 0 5 5',
            name: 'responder',
            flex: 1

        }]
    }, {
        xtype: 'combobox',
        fieldLabel: 'Enviar a',
        emptyText: 'Trieu....',
        valueField: 'id',
        displayField: 'nombre',
        name: 'tipoCorreoEnvio',
        editable: false,
        queryMode: 'local',
        store: 'StoreTiposCorreo',
        allowBlank: false
    }, {
        xtype: 'combobox',
        fieldLabel: 'Programa',
        emptyText: 'Trieu....',
        valueField: 'id',
        displayField: 'nombre',
        name: 'programaId',
        editable: false,
        queryMode: 'local',
        store: 'StoreProgramas',
        allowBlank: true,
        trigger2Cls: 'x-form-clear-trigger',
        onTrigger2Click: function (event) {
            var me = this;
            me.setValue(null);
        }
    }, {
        xtype: 'textfield',
        fieldLabel: 'Texto Base',
        name: 'base',
        allowBlank: true
    }, {
        xtype: 'container',
        layout: 'hbox',
        items: [{
            xtype: 'datefield',
            flex: 1,
            fieldLabel: 'Data enviament',
            name: 'fechaEnvioDestinatario',
            format: 'd/m/Y',
            minValue: new Date(),
            editable: false,
            trigger2Cls: 'x-form-clear-trigger',
            onTrigger2Click: function (event) {
                var me = this;
                me.setValue(null);
            }
        }, {
            xtype: 'textfield',
            fieldLabel: 'Hora',
            padding: '0 0 0 10',
            flex: 1,
            regex: /^([0-9]$)|(0[0-9])|(1[0-9]|2[0-3]):([0-5][0-9])/,
            name: 'horaEnvioDestinatario'
        }]
    }, {
        xtype: 'label',
        text: 'Corp:',
        padding: '5 0 0 0',
    }, {
        xtype: 'button',
        maxWidth: 120,
        name: 'afegirPlantilla',
        text: 'Afegir plantilla',
        iconCls: 'application-add'
    }, {
        name: 'editor',
        padding: '5 0 0 0',
        border: 0,
        autoScroll: true,
        flex: 3
    }
    ]
})
;