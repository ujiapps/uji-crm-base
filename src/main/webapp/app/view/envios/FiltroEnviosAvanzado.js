Ext.define('CRM.view.envios.FiltroEnviosAvanzado',
    {
        extend: 'Ext.panel.Panel',
        alias: 'widget.filtroEnviosAvanzado',
        frame: true,

        border: 0,
        //    padding : 5,
        closable: false,
        layout: 'anchor',
        items: [
            {
                xtype: 'container',
                layout: 'hbox',
                padding: '5 0 0 0',
                items: [

                    {
                        xtype: 'textfield',
                        fieldLabel: 'email',
                        name: 'busquedaEmail',
                        margin: '0 15 0 0',
                        labelWidth: 50,
                        width: 240
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Des de',
                        name: 'fechaDesde',
                        margin: '0 15 0 0',
                        format: 'd/m/Y',
                        editable: false,
                        labelWidth: 50,
                        width: 150
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Fins',
                        labelWidth: 40,
                        name: 'fechaHasta',
                        margin: '0 15 0 0',
                        format: 'd/m/Y',
                        editable: false,
                        width: 150
                    },
                    {
                        border: 0,
                        xtype: 'combobox',
                        editable: true,
                        margin: '0 10 0 0',
                        fieldLabel: 'Envío creado por',
                        emptyText: 'Trieu...',
                        store: 'StoreComboEnvioPersona',
                        displayField: 'etiqueta',
                        valueField: 'id',
                        name: 'envioPersona'
                    }]
            }]
    });