Ext.define('CRM.view.envios.FiltroUsuario.FieldsetEstudios', {
    extend: 'Ext.form.FieldSet',
    alias: 'widget.fieldsetEstudios',
    title: 'Estudios (Inclusivos)',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [{
        xtype: 'fieldset',
        title: 'Estudio UJI',
        items: [{
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'fit'
            },
            defaults: {margin: '0 0 0 10'},
            items: [{
                xtype: 'combobox',
                fieldLabel: 'Tipus',
                labelWidth: 130,
                name: 'criterioTipoEstudioUJIId',
                store: Ext.create('CRM.store.StoreTiposEstudiosUJI'),
                displayField: 'nombre',
                valueField: 'id',
                multiSelect: true,
                queryMode: 'local',
                margin: '0 0 0 0',
                flex: 1,
                editable: false,
                trigger2Cls: 'x-form-clear-trigger',
                onTrigger2Click: function (event) {
                    var me = this;
                    me.clearValue();
                    var estudios = this.up("form").down("combobox[name=criterioEstudioUJIId]");
                    estudios.setValue(null);
                    var store = estudios.getStore();
                    store.clearFilter();
                },
                listeners: {
                    select: function (combo, elem) {
                        var estudios = this.up("form").down("combobox[name=criterioEstudioUJIId]");
                        estudios.setValue(null);
                        var store = estudios.getStore();
                        store.clearFilter();

                        if (elem.length > 0) {
                            store.filterBy(function (record) {
                                    let coincide = false;
                                    for (e in elem) {
                                        if (elem[e].data.id == record.data.tipo)
                                            coincide = true;
                                    }
                                    return coincide;
                                }
                            )
                        }
                    }
                }
            }, {
                xtype: 'combobox',
                fieldLabel: 'Estudi',
                name: 'criterioEstudioUJIId',
                store: Ext.create('CRM.store.StoreClasificacionesEstudio', {
                    sorters: {
                        property: 'nombre',
                        direction: 'ASC'
                    }
                }),
                displayField: 'nombre',
                valueField: 'id',
                queryMode: 'local',
                multiSelect: true,
                editable: false,
                flex: 2,
                trigger2Cls: 'x-form-clear-trigger',
                onTrigger2Click: function (event) {
                    var me = this;
                    me.clearValue();
                },
            }, {
                xtype: 'combobox',
                editable: false,
                fieldLabel: 'Tipo Beca',
                flex: 1,
                displayField: 'nombre',
                valueField: 'id',
                queryMode: 'local',
                name: 'criterioEstudioUJIBeca',
                store: Ext.create('CRM.store.StoreTiposBecas', {
                    sorters: {
                        property: 'nombre',
                        direction: 'ASC'
                    }
                }),
                multiSelect: true,
                trigger2Cls: 'x-form-clear-trigger',
                onTrigger2Click: function (event) {
                    var me = this;
                    me.clearValue();
                }
            }]
        }, {
            xtype: 'container',
            margin: '5 0 0 0',
            layout: {
                type: 'hbox',
                align: 'fit'
            },
            items: [{
                xtype: 'textfield',
                fieldLabel: 'Anyo finalització desde',
                name: 'anyoFinalizacionEstudioUJIInicio',
                labelWidth: 130,
                flex: 1
            }, {
                xtype: 'textfield',
                fieldLabel: 'hasta',
                margin: '0 0 0 10',
                name: 'anyoFinalizacionEstudioUJIFin',
                flex: 1
            }]
        }
        ]
    },
        {
            xtype: 'fieldset',
            title: 'Estudio NO UJI',
            items:
                [{
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'fit'
                    },
                    defaults: {margin: '0 0 0 10'},
                    items: [{
                        xtype: 'combobox',
                        fieldLabel: 'Tipus',
                        name: 'criterioTipoEstudioNoUJIId',
                        store: Ext.create('CRM.store.StoreTiposEstudiosNoUJI'),
                        displayField: 'nombre',
                        valueField: 'id',
                        margin: '0 0 0 0',
                        labelWidth: 130,
                        queryMode: 'local',
                        multiSelect: true,
                        flex: 1,
                        editable: false,
                        trigger2Cls: 'x-form-clear-trigger',
                        onTrigger2Click: function (event) {
                            var me = this;
                            me.clearValue();
                            var estudios = this.up("form").down("combobox[name=criterioEstudioNoUJIId]");
                            estudios.setValue(null);
                            var store = estudios.getStore();
                            store.clearFilter();
                        },
                        listeners: {
                            select: function (combo, elem) {
                                var estudios = this.up("form").down("combobox[name=criterioEstudioNoUJIId]");
                                estudios.setValue(null);
                                var store = estudios.getStore();
                                store.clearFilter();

                                if (elem.length > 0) {
                                    store.filterBy(function (record) {
                                            let coincide = false;
                                            for (e in elem) {
                                                if (elem[e].data.id == record.data.tipo)
                                                    coincide = true;
                                            }
                                            return coincide;
                                        }
                                    )
                                }
                            }
                        }
                    }, {
                        xtype: 'combobox',
                        fieldLabel: 'Estudi',
                        name: 'criterioEstudioNoUJIId',
                        store: Ext.create('CRM.store.StoreClasificacionesEstudioNoUJI', {
                            sorters: {
                                property: 'nombreCa',
                                direction: 'ASC'
                            }
                        }),
                        displayField: 'nombreCa',
                        valueField: 'id',
                        queryMode: 'local',
                        multiSelect: true,
                        editable: false,
                        flex: 2,
                        trigger2Cls: 'x-form-clear-trigger',
                        onTrigger2Click: function (event) {
                            var me = this;
                            me.clearValue();
                        }
                    }]
                },
                    {
                        xtype: 'container',
                        margin: '5 0 0 0',
                        layout: {
                            type: 'hbox',
                            align: 'fit'
                        },
                        items: [
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Anyo finalització desde',
                                name: 'anyoFinalizacionEstudioNoUJIInicio',
                                labelWidth: 130,
                                flex: 1
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'hasta',
                                name: 'anyoFinalizacionEstudioNoUJIFin',
                                margin: '0 0 0 10',
                                flex: 1
                            }]
                    }
                ]
        }
    ],

    limpiaFieldsetEstudios: function () {
        var form = this;

        form.down("textfield[name=anyoFinalizacionEstudioUJIInicio]").setValue(null);
        form.down("textfield[name=anyoFinalizacionEstudioUJIFin]").setValue(null);
        form.down("combobox[name=criterioEstudioUJIId]").setValue(null);
        form.down("combobox[name=criterioEstudioUJIBeca]").setValue(null);
        form.down("combobox[name=criterioEstudioUJIId]").getStore().clearFilter();
        form.down("combobox[name=criterioTipoEstudioUJIId]").setValue(null);

        form.down("textfield[name=anyoFinalizacionEstudioNoUJIInicio]").setValue(null);
        form.down("textfield[name=anyoFinalizacionEstudioNoUJIFin]").setValue(null);
        form.down("combobox[name=criterioEstudioNoUJIId]").setValue(null);
        form.down("combobox[name=criterioEstudioNoUJIId]").getStore().clearFilter();
        form.down("combobox[name=criterioTipoEstudioNoUJIId]").setValue(null);
    }

})
;