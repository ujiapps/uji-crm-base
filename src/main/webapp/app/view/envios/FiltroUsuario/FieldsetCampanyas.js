Ext.define('CRM.view.envios.FiltroUsuario.FieldsetCampanyas', {
    extend: 'Ext.form.FieldSet',
    alias: 'widget.fieldsetCampanyas',
    name: 'fieldsetCampanyas',
    requires: ['CRM.view.envios.FiltroUsuario.FieldsetDatosUsuario', 'CRM.view.envios.FiltroUsuario.FieldsetEstudios'],
    title: 'Dades campanya',

    items: [{
        xtype: 'fieldcontainer',
        defaultType: 'radiofield',
        items: [
            {
                boxLabel: 'Seleccionar campaña y Estado',
                name: 'tipoSeleccionCampanya',
                inputValue: '1',
                id: 'tipoSeleccionCampanya1',
                checked: true,
                listeners: {
                    change: function () {
                        if (this.checked) {
                            this.up("form").down("[name=combinadoCampanyas]").setVisible(false);
                            this.up("form").down("[name=filtroCampanya]").setVisible(true);
                        }
                    }
                }

            }, {
                boxLabel: 'Seleccionar filtro campaña predefinido',
                name: 'tipoSeleccionCampanya',
                inputValue: '2',
                id: 'tipoSeleccionCampanya2',
                listeners: {
                    change: function () {
                        if (this.checked) {
                            this.up("form").down("[name=combinadoCampanyas]").setVisible(true);
                            this.up("form").down("[name=filtroCampanya]").setVisible(false);
                        }
                    }
                }
            }
        ]

    }, {
        xtype: 'fieldset',
        name: 'filtroCampanya',

        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        defaults: {padding: 2},
        items: [{
            xtype: 'combobox',
            fieldLabel: 'Programa',
            name: 'programaId',
            store: Ext.create('CRM.store.StoreProgramas'),
            displayField: 'nombre',
            valueField: 'id',
            queryMode: 'local',
            flex: 1,
            forceSelection: true,
            listeners: {
                select: function (combo, elem) {
                    this.up("form").down("combobox[name=campanyaId]").setDisabled(false);
                    this.up("form").down("combobox[name=campanyaId]").setValue(null);
                    this.up("form").down("combobox[name=tipoEstadoCampanyaId]").setValue(null);
                    this.up("form").down("combobox[name=campanyaId]").getStore().filterBy(function (record) {
                        return record.data.programaId == elem[0].data.id;
                    });
                }
            }
        }, {
            xtype: 'combobox',
            fieldLabel: 'Campanya',
            name: 'campanyaId',
            store: Ext.create('CRM.store.StoreCampanyas'),
            displayField: 'nombre',
            valueField: 'id',
            queryMode: 'local',
            flex: 2,
            // width: 200,
            forceSelection: true,
            disabled: true,
            listeners: {
                select: function (combo, sel) {
                    this.up("window").cargaEstadosCampanya(sel[0].data.id);
                }
            }
        }, {
            xtype: 'combobox',
            fieldLabel: 'Estat',
            name: 'tipoEstadoCampanyaId',
            store: Ext.create('CRM.store.StoreTiposEstadoCampanya'),
            displayField: 'nombre',
            valueField: 'id',
            queryMode: 'local',
            multiSelect: true,
            flex: 1,
            disabled: true
        }]
    },
        {
            xtype: 'fieldset',
            defaultType: 'radiofield',
            hidden: true,
            layout: 'vbox',
            name: 'combinadoCampanyas',
            items: [
                {
                    boxLabel: 'Usuaris que siguen Premium o Bàsic (Definitiu)',
                    name: 'combinadoCampanyas',
                    inputValue: '1',
                    id: 'combinadoCampanyas1'
                }, {
                    boxLabel: 'Usuaris Bàsic que no siguen Premium (Definitiu)',
                    name: 'combinadoCampanyas',
                    inputValue: '3',
                    id: 'combinadoCampanyas3'
                }
            ]
        }],
    limpiaFieldsetCampanya: function () {
        var form = this;
        form.down("[name=tipoEstadoCampanyaId]").setValue(null);
        form.down("[name=tipoEstadoCampanyaId]").setDisabled(true);
        form.down("[name=campanyaId]").setValue(null);
        form.down("[name=campanyaId]").getStore().clearFilter();
        form.down("[name=campanyaId]").setDisabled(true);
        form.down("[name=programaId]").setValue(null);
        form.down("[id=combinadoCampanyas1]").setValue(false);
        form.down("[id=combinadoCampanyas3]").setValue(false);
    }

})
;