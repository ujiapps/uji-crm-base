Ext.define('CRM.view.envios.FiltroUsuario.FieldsetDatosUsuario', {
    extend: 'Ext.form.FieldSet',
    alias: 'widget.fieldsetDatosUsuario',
    title: 'Dades usuari',
    name: 'fieldsetDatosUsuario',
    items: [{
        xtype: 'container',
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        defaults: {padding: 2},
        items: [{
            xtype: 'combobox',
            fieldLabel: 'Mes naixement',
            name: 'mesNacimiento',
            queryMode: 'local',
            store: Ext.create('CRM.store.StoreMeses'),
            displayField: 'nombre',
            valueField: 'id',
            flex: 1
        }, {
            xtype: 'numberfield',
            fieldLabel: 'Anyo Naixement',
            name: 'anyoNacimiento',
            minValue: 1900,
            flex: 1
        }
        ]
    }],


    limpiaFieldsetDatosUsuario: function () {
        var form = this;
        form.down("[name=mesNacimiento]").setValue(null);
        form.down("[name=anyoNacimiento]").setValue(null);
    }
})
;