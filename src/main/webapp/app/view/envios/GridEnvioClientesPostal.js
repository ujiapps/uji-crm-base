Ext.define('CRM.view.envios.GridEnvioClientesPostal',
{
    extend : 'Ext.grid.Panel',
    //    title : 'Clients',
    alias : 'widget.gridEnvioClientesPostal',
    store : 'StoreEnviosClientesPostal',
    sortableColumns : true,
    autoScroll : true,
    hideHeaders : true,
    selModel :
    {
        mode : 'SINGLE'
    },
    dockedItems : [
    {
        xtype : 'pagingtoolbar',
        store : 'StoreEnviosClientesPostal',
        dock : 'bottom',
        displayInfo : true
    } ],
    tbar : [
    {
        xtype : 'button',
        name : 'anyadirPersona',
        text : 'afegir Persona',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarPersona',
        text : 'esborrar Persona',
        iconCls : 'application-delete',
        disabled : true
    }],
    columns : [
    {
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'clienteNombre',
        flex : 1
    },
    {
        dataIndex : 'clienteEtiqueta',
        flex : 1
    },
    {
        dataIndex : 'para',
        flex : 2
    } ],
    listeners: {
        activate: function () {
            var store= this.getStore();
            store.getProxy().setExtraParam('envioId',this.up("[name=panelEnvios]").envioId);
            store.load();
        }
    }

});
