Ext.define('CRM.view.envios.PanelEnvioMail', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.panelEnvioMail',
    frame: true,
    flex: 1,
    border: 0,
    fbar: [{
        xtype: 'textfield',
        name: 'correoPrueba',

    }, {
        xtype: 'button',
        name: 'enviarPrueba',
        text: 'Enviar Prova'
    }, {
        xtype: 'button',
        name: 'guardar',
        text: 'Guardar'
    }, {
        xtype: 'button',
        name: 'enviar',
        text: 'Guardar i Enviar'
    }, {
        xtype: 'button',
        name: 'duplicar',
        text: 'Duplicar',
        listeners: {
            click: function () {
                var envioId = this.up("panel[name=panelEnvios]").envioId,
                    panelEnvio = this.up("panel[name=panelEnvios]");

                panelEnvio.setLoading(true);
                Ext.Ajax.request(
                    {
                        url: '/crm/rest/envio/' + envioId + "/duplicar",
                        method: 'POST',
                        scope: this,
                        success: function (obj) {
                            Ext.Msg.alert('Enviament', '<b>S\'ha duplicat l\'enviament</b>');
                            var resp = Ext.JSON.decode(obj.responseText);
                            this.up("panel[name=panelEnvioMail]").rellenaForm(resp.data);
                            panelEnvio.setLoading(false);

                        },
                        failure: function () {
                            Ext.Msg.alert('Enviament', '<b>No s\'ha pogut duplicar l\'enviament</b>');
                            panelEnvio.setLoading(false);
                        }
                    });

            }
        }
    }, {
        xtype: 'button',
        name: 'borrar',
        text: 'Esborrar'
    }],
    items: [{
        title: 'Enviament',
        xtype: 'formEnvioMail',
        border: 0,
        name: 'formEnvioMail'
    }, {
        xtype: 'gridEnvioClientesMail',
        name: 'gridEnvioClientesMail',
        border: 0,
        title: 'Clients'
    }, {
        xtype: 'form',
        name: 'formEnvioAdjuntos',
        border: 0,
        title: 'Adjunts',
        items: [{
            xtype: 'gridArchivosAdjuntos',
            name: 'gridArchivosAdjuntos'
        }]
    }, {
        xtype: 'form',
        border: 0,
        name: 'formSubirImagenes',
        title: 'Imagenes',
        items: [{
            xtype: 'gridImagenes',
            name: 'gridImagenes'
        }]
    }],

    rellenaForm: function (envio) {

        var panelEnvio = this.up("panel[name=panelEnvios]"),
            form = this.down("form[name=formEnvioMail]"),
            editor = form.down("container[name=editor]"),
            config = {width: '100%', height: '100%', name: 'cuerpo'},
            txt = editor.down('[name=cuerpo]'),
            editorNuevo = new Ext.ux.CKEditor(config),
            storeClientes = this.down("grid[name=gridEnvioClientesMail]").getStore(),
            storeAdjuntos = this.down("grid[name=gridArchivosAdjuntos]").getStore(),
            storeImagenes = this.down("grid[name=gridImagenes]").getStore();

        panelEnvio.envioId = envio.id;
        panelEnvio.envioTipoId = envio.envioTipoId;

        //poner visible este panel
        panelEnvio.down("panel[name=formEnvioPostal]").setVisible(false);
        this.setVisible(true);

        form.down("textfield[name=nombre]").setValue(envio.nombre);
        form.down("textfield[name=desde]").setValue(envio.desde);
        form.down("textfield[name=responder]").setValue(envio.responder);
        form.down("combobox[name=tipoCorreoEnvio]").setValue(parseInt(envio.tipoCorreoEnvio));
        form.down("combobox[name=programaId]").setValue(parseInt(envio.programaId));

        storeAdjuntos.load({url: '/crm/rest/envio/' + envio.id + '/adjunto'});
        storeImagenes.load({url: '/crm/rest/envioimagen/' + envio.id});

        storeClientes.getProxy().url = '/crm/rest/enviocliente/envio/' + envio.id;
        storeClientes.load();

        if (txt != null) {
            txt.destroy();
        }

        editorNuevo.setValue(envio.cuerpo);

        editor.add(editorNuevo);
        editorNuevo.setUrl('/envioimagen/visualizar/' + envio.id);
    }

});