Ext.define('CRM.view.envios.VentanaBusquedaCliente', {
    extend: 'Ext.Window',
    alias: 'widget.ventanaBusquedaCliente',
    name: 'ventanaBusquedaCliente',
    modal: true,
    title: 'Busqueda',
    frame: true,
    border: 0,
    padding: 10,
    width: '60%',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    bbar: [{
        xtype: 'button',
        name: 'botonAnyadirClienteEnvio',
        text: 'Afegir a l\'enviament',
        listeners: {
            click: function () {
                var window = this.up("window");
                Ext.Ajax.request({
                    url: '/crm/rest/enviocriterio/envio/cliente/',
                    method: 'POST',
                    params: {
                        envioId: window.envioId,
                        clienteId: window.down("combo[name=comboBusquedaClienteEnvio]").getValue()
                    },
                    scope: this,
                    success: function () {
                        window.storeEnviosClientes.load();
                        window.close();
                    },
                    failure: function () {
                        window.close();
                    }
                })
            }
        }
        // iconCls: 'application-add'
    }],
    items: [{
        xtype: 'lookupcombobox',
        appPrefix: 'crm',
        bean: 'cliente',
        name: 'comboBusquedaClienteEnvio',
        padding: 20,
        fieldLabel: 'Persona',
        labelWidth: 75,
        editable: false,
        allowBlank: false,
        displayField: 'nombre',
        valueField: 'id',
        extraFields: ['Servicio']
    }]
});