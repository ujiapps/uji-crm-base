Ext.define('CRM.view.servicios.GridLineasFacturacion',
    {
        extend: 'Ext.grid.Panel',
        //title : 'Serveis',
        alias: 'widget.gridLineasFacturacion',
        store: 'StoreLineasFacturacion',
        requires: ['Ext.grid.plugin.RowEditing'],

        selModel: {
            mode: 'SINGLE'
        },

        sortableColumns: true,

        plugins: [
            {
                ptype: 'rowediting',
                clicksToEdit: 2,
                pluginId: 'editingLineaFacturacion'
            }],

        tbar: [
            {
                xtype: 'button',
                name: 'anyadirLineaFacturacion',
                text: 'Afegir',
                iconCls: 'application-add'
            },
            {
                xtype: 'button',
                name: 'borrarLineaFacturacion',
                text: 'Esborrar',
                iconCls: 'application-delete',
                disabled: true
            }],

        columns: [
            {
                text: 'Nom',
                dataIndex: 'nombre',
                menuDisabled: true,
                editor: {
                    allowBlank: false
                },
                flex: 1
            },
            {
                text: 'Descripcion',
                dataIndex: 'descripcion',
                menuDisabled: true,
                editor: {
                    allowBlank: true
                },
                flex: 1
            },
            {
                xtype: 'combocolumn',
                header: 'Emisora',
                dataIndex: 'emisoraId',
                menuDisabled: true,
                flex: 1,
                combo: {
                    xtype: 'combobox',
                    store: Ext.create('CRM.store.StoreEmisoras'),
                    displayField: 'nombre',
                    valueField: 'id',
                    allowBlank: true,
                    editable: true

                }
            },
            {
                xtype: 'combocolumn',
                header: 'Formato id recibo',
                dataIndex: 'reciboFormatoId',
                menuDisabled: true,
                flex: 1,
                combo: {
                    xtype: 'combobox',
                    store: Ext.create('CRM.store.StoreReciboFormatos'),
                    displayField: 'formato',
                    valueField: 'id',
                    allowBlank: true,
                    editable: true

                }
            }]

    });
