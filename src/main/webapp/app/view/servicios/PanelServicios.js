Ext.define('CRM.view.servicios.PanelServicios',
    {
        extend: 'Ext.panel.Panel',
        title: 'Serveis',
        requires: ['CRM.view.servicios.GridServicios', 'CRM.view.personas.GridPersonas', 'CRM.view.servicios.GridLineasFacturacion'],
        alias: 'widget.panelServicios',
        closable: true,

        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        items: [
            {
                xtype: 'gridServicios',
                flex: 1

            },
            {
                xtype: 'tabpanel',
                flex: 2,
                disabled: true,
                items: [
                    {
                        xtype: 'gridPersonas',
                        title: 'Persones'
                    },
                    {
                        xtype : 'gridLineasFacturacion',
                        title : 'Lineas Facturacion'
                    }
                ]
            }
        ]

    });