Ext.define('CRM.view.servicios.GridServicios',
{
    extend : 'Ext.grid.Panel',
    //title : 'Serveis',
    alias : 'widget.gridServicios',
    store : 'StoreServicios',
    requires : [ 'Ext.grid.plugin.RowEditing' ],

    selModel :
    {
        mode : 'SINGLE'
    },

    sortableColumns : true,

    plugins : [
    {
        ptype : 'rowediting',
        clicksToEdit : 2,
        pluginId : 'editingServicio'
    } ],

    tbar : [
    {
        xtype : 'button',
        name : 'anyadirServicio',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarServicio',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'Nom',
        dataIndex : 'nombre',
        menuDisabled : true,
        editor :
        {
            allowBlank : false
        },
        flex : 1
    },
    {
        text : 'Id',
        dataIndex : 'id',
        name : 'id',
        menuDisabled : true,
        hidden : true,
        flex : 1
    } ]

});
