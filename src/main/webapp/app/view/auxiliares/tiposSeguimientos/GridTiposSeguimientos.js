Ext.define('CRM.view.auxiliares.tiposVinculacion.GridTiposSeguimientos',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridTiposSeguimientos',
    store : 'StoreSeguimientoTipos',
    requires : [ 'Ext.grid.plugin.RowEditing' ],

    selModel :
    {
        mode : 'SINGLE'
    },

    sortableColumns : true,

    plugins : [
    {
        ptype : 'rowediting',
        clicksToEdit : 2,
        pluginId : 'editingTipoSeguimientos'
    } ],

    tbar : [
    {
        xtype : 'button',
        name : 'anyadirTipoSeguimientos',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarTipoSeguimientos',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'Nom',
        dataIndex : 'nombre',
        menuDisabled : true,
        editor :
        {
            allowBlank : false
        },
        flex : 1
    },
    {
        text : 'Id',
        dataIndex : 'id',
        name : 'id',
        menuDisabled : true,
        hidden : true,
        flex : 1
    } ]

});