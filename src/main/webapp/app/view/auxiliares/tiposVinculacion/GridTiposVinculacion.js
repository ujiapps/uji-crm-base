Ext.define('CRM.view.auxiliares.tiposVinculacion.GridTiposVinculacion',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridTiposVinculacion',
    store : 'StoreTiposVinculaciones',
    requires : [ 'Ext.grid.plugin.RowEditing' ],

    selModel :
    {
        mode : 'SINGLE'
    },

    sortableColumns : true,

    plugins : [
    {
        ptype : 'rowediting',
        clicksToEdit : 2,
        pluginId : 'editingTipoVinculacion'
    } ],

    tbar : [
    {
        xtype : 'button',
        name : 'anyadirTipoVinculacion',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarTipoVinculacion',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'Nom',
        dataIndex : 'nombre',
        menuDisabled : true,
        editor :
        {
            allowBlank : false
        },
        flex : 1
    },
    {
        text : 'Id',
        dataIndex : 'id',
        name : 'id',
        menuDisabled : true,
        hidden : true,
        flex : 1
    } ]

});