Ext.define('CRM.view.auxiliares.etiquetas.GridEtiquetas',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridEtiquetas',
    store : 'StoreEtiquetas',
    requires : [ 'Ext.grid.plugin.RowEditing' ],

    selModel :
    {
        mode : 'SINGLE'
    },

    sortableColumns : true,

    plugins : [
    {
        ptype : 'rowediting',
        clicksToEdit : 2,
        pluginId : 'editingEtiqueta'
    } ],

    tbar : [
    {
        xtype : 'button',
        name : 'anyadirEtiqueta',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarEtiqueta',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'Nom',
        dataIndex : 'nombre',
        menuDisabled : true,
        editor :
        {
            allowBlank : false
        },
        flex : 1
    },
    {
        text : 'Id',
        dataIndex : 'id',
        name : 'id',
        menuDisabled : true,
        hidden : true,
        flex : 1
    } ]

});