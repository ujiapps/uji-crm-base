Ext.define('CRM.view.auxiliares.etiquetas.GridCampanyaTextos',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridCampanyaTextos',
    store : 'StoreCampanyaTextos',
    requires : [ 'Ext.grid.plugin.RowEditing' ],

    selModel :
    {
        mode : 'SINGLE'
    },

    sortableColumns : true,

    tbar : [
    {
        xtype : 'button',
        name : 'anyadirCampanyaTexto',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarCampanyaTexto',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'Nom',
        dataIndex : 'nombre',
        menuDisabled : true,
        editor :
        {
            allowBlank : false
        },
        flex : 1
    },
    {
        text : 'Texte',
        dataIndex : 'textoCa',
        menuDisabled : true,
        flex : 4
    },
    {
        text : 'Id',
        dataIndex : 'id',
        name : 'id',
        menuDisabled : true,
        hidden : true
    } ]

});