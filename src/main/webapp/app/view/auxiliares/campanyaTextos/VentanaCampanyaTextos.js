Ext.define('CRM.view.auxiliares.campanyaTextos.VentanaCampanyaTextos', {
    extend: 'Ext.Window',
    title: 'Textes LOPD',
    width: 700,
    height: 400,
    alias: 'widget.ventanaCampanyaTextos',
    modal: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    fbar: [{
        xtype: 'button',
        text: 'Desar',
        action: 'guardar',
        name: 'guardar'
    }, {
        xtype: 'button',
        text: 'Cancel·lar',
        action: 'cancelar',
        name: 'cancelar'
    }],

    items: [{
        xtype: 'form',
        id: 'formCampanyaDato',
        items: [{
            xtype: 'textfield',
            name: 'id',
            hidden: true
        }, {
            xtype: 'textfield',
            fieldLabel: 'Nom',
            name: 'nombre',
            padding: 5
        }, {
            xtype: 'textfield',
            fieldLabel: 'Codigo',
            name: 'codigo',
            disabled: true,
            padding: 5
        }, {
            xtype: 'tabpanel',
            fieldLabel: 'Nom',
            name: 'nombre',
            padding: 5,
            items: [{
                xtype: 'htmleditor',
                title: 'Texte',
                name: 'textoCa',
                autoScroll: true,
                height: 250,
                padding: 5
            }, {
                xtype: 'htmleditor',
                title: 'Texto',
                name: 'textoEs',
                autoScroll: true,
                height: 250,
                padding: 5
            }, {
                xtype: 'htmleditor',
                title: 'Text',
                name: 'textoUk',
                autoScroll: true,
                height: 250,
                padding: 5
            }]
        }]
    }]
});