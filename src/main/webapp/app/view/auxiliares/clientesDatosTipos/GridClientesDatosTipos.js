Ext.define('CRM.view.auxiliares.clientesDatosTipos.GridClientesDatosTipos', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridClientesDatosTipos',
    store: 'StoreClientesDatosTipos',
    requires: ['Ext.grid.plugin.RowEditing'],

    selModel: {
        mode: 'SINGLE'
    },

    sortableColumns: true,

    plugins: [{
        ptype: 'rowediting',
        clicksToEdit: 2,
        pluginId: 'editingClienteDatoTipo'
    }],

    tbar: [{
        xtype: 'button',
        name: 'anyadirClienteDatoTipo',
        text: 'Afegir',
        iconCls: 'application-add'
    }, {
        xtype: 'button',
        name: 'borrarClienteDatoTipo',
        text: 'Esborrar',
        iconCls: 'application-delete',
        disabled: true
    }],

    columns: [{
        text: 'Nom',
        dataIndex: 'nombre',
        menuDisabled: true,
        editor: {
            allowBlank: false
        },
        flex: 1
    }, {
        text: 'Tipus',
        dataIndex: 'clienteDatoTipoTipoId',
        name: 'clienteDatoTipoTipoId',
        flex: 1,
        renderer: function (value) {
            var store = Ext.getStore('StoreTiposTipoDato');
            var data = store.getById(value);
            return data ? data.get('nombre') : '';
        },
        editor: {
            xtype: 'combobox',
            store: 'StoreTiposTipoDato',
            displayField: 'nombre',
            valueField: 'id',
            allowBlank: false
        }
    }, {
        text: 'Tamany',
        dataIndex: 'tamanyo',
        name: 'tamanyo',
        flex: 0.5,
        editor: {
            allowBlank: true
        }
    }, {
        text: 'Ordre',
        dataIndex: 'orden',
        name: 'orden',
        flex: 0.5,
        editor: {
            allowBlank: false
        }
    }, {
        text: 'Duplicar',
        dataIndex: 'duplicar',
        name: 'duplicar',
        flex: 0.5,
        xtype: 'booleancolumn',
        trueText: 'Sí',
        falseText: 'No',
        editor: {
            xtype: 'checkbox'
        }
    }, {
        text: 'Modificar',
        dataIndex: 'modificar',
        name: 'modificar',
        flex: 0.5,
        xtype: 'booleancolumn',
        trueText: 'Sí',
        falseText: 'No',
        editor: {
            xtype: 'checkbox'
        }
    }, {
        text: 'Id',
        dataIndex: 'id',
        name: 'id',
        menuDisabled: true,
        hidden: true
    }]
});