Ext.define('CRM.view.servicios.PanelClientesDatos',
{
    extend : 'Ext.panel.Panel',
    title : 'Tipus de dades',
    requires : [ 'CRM.view.auxiliares.clientesDatosTipos.GridClientesDatosTipos', 'CRM.view.auxiliares.clientesDatosTipos.GridClientesDatosOpciones' ],
    alias : 'widget.panelClientesDatos',
    closable : false,

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'gridClientesDatosTipos',
        flex : 1

    },
    {
        xtype : 'gridClientesDatosOpciones',
        disabled : true,
        flex : 1
    } ]

});