Ext.define('CRM.view.auxiliares.clientesDatosTipos.GridClientesDatosOpciones',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridClientesDatosOpciones',
    store : 'StoreClientesDatosOpciones',
    requires : [ 'Ext.grid.plugin.RowEditing' ],

    selModel :
    {
        mode : 'SINGLE'
    },

    sortableColumns : true,

    plugins : [
    {
        ptype : 'rowediting',
        clicksToEdit : 2,
        pluginId : 'editingClienteDatoOpcion'
    } ],

    tbar : [
    {
        xtype : 'button',
        name : 'anyadirClienteDatoOpcion',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarClienteDatoOpcion',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'Etiqueta',
        dataIndex : 'etiqueta',
        menuDisabled : true,
        editor :
        {
            allowBlank : false
        },
        flex : 1
    },
    {
        text : 'Valor',
        dataIndex : 'valor',
        name : 'valor',
        flex : 0.5,
        editor :
        {
            allowBlank : true
        }
    },
    {
        text : 'Tipo',
        dataIndex : 'clienteDatoTipoId',
        name : 'clienteDatoTipoId',
        menuDisabled : true,
        hidden : true
    },
    {
        text : 'Id',
        dataIndex : 'id',
        name : 'id',
        menuDisabled : true,
        hidden : true
    } ]

});