Ext.define('CRM.view.auxiliares.PanelAuxiliares',
{
    extend : 'Ext.tab.Panel',
    title : 'Altres gestions',
    requires : [ 'CRM.view.auxiliares.etiquetas.GridEtiquetas', 'CRM.view.auxiliares.clientesDatosTipos.PanelClientesDatos', 'CRM.view.auxiliares.campanyaTextos.GridCampanyaTextos',
            'CRM.view.auxiliares.tiposVinculacion.GridTiposVinculacion', 'CRM.view.auxiliares.tiposSeguimientos.GridTiposSeguimientos' ],
    alias : 'widget.panelAuxiliares',
    closable : true,

    layout :
    {
        type : 'border',
        align : 'stretch'
    },
    items : [
    {
        xtype : 'gridEtiquetas',
        title : 'Etiquetes'
    },
    {
        xtype : 'panelClientesDatos',
        title : 'Tipus de dades'
    },
    {
        xtype : 'gridCampanyaTextos',
        title : 'Textes LOPD'
    },
    {
        xtype : 'gridTiposVinculacion',
        title : 'Tipus vinculacions'
    },
    {
        xtype : 'gridTiposSeguimientos',
        title : 'Tipus seguiments'
    } ]
});