Ext.define('CRM.view.grupos.GridGrupos',
{
    extend : 'Ext.grid.Panel',
    title : 'Suscriptors',
    alias : 'widget.gridGrupos',
    store : 'StoreGrupos',
    forceFit : true,
    sortableColumns : true,

    tbar : [
    {
        xtype : 'button',
        name : 'anyadirGrupo',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarGrupo',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'Nom Suscriptor',
        dataIndex : 'nombreCa',
        flex : 2
    },
    {
        text : 'Ordre',
        dataIndex : 'orden',
        flex : 1
    },
    {
        text : 'Visible',
        dataIndex : 'visible',
        xtype : 'booleancolumn',
        trueText : 'Sí',
        falseText : 'No',
        flex : 1
    },
    {
        text : 'Tipo Access',
        dataIndex : 'tipoAccesoNombre',
        flex : 1
    },
    {
        text : 'Id',
        dataIndex : 'id',
        name : 'id',
        hidden : true
    } ]

});
