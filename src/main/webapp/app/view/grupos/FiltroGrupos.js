Ext.define('CRM.view.grupos.FiltroGrupos',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.filtroGrupos',
    frame : true,

    border : false,
    padding : 5,
    closable : false,
    layout : 'anchor',

    items : [
    {
        xtype : 'container',
        layout : 'hbox',

        items : [
        {
            border : 0,
            xtype : 'combobox',
            labelWidth : 60,
            labelAlign : 'left',
            margin : '3 20 0 0',
            fieldLabel : 'Programa',
            emptyText : 'Trieu...',
            store : 'StoreProgramasAccesoAdmin',
            displayField : 'nombreCa',
            valueField : 'id',
            name : 'programa',
            width : 500,
            queryMode: 'local',
            editable : true
        },
        {
            xtype : 'button',
            text : 'Netejar Filtres',
            name : 'limpiarFiltros'
        } ]
    } ]
});