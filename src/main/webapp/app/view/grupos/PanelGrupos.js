Ext.define('CRM.view.grupos.PanelGrupos',
{
    extend : 'Ext.panel.Panel',
    title : 'Suscriptores',
    alias : 'widget.panelGrupos',
    requires : [ 'CRM.view.grupos.FiltroGrupos', 'CRM.view.grupos.GridGrupos', 'CRM.view.items.GridItems', 'CRM.view.items.GridItemsItemsDestino', 'CRM.view.items.GridItemsItemsOrigen' ],
    closable : true,

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'filtroGrupos',
        height : 40
    },
    {
        xtype : 'gridGrupos',
        flex : 2,
        disabled : true
    },
    {
        xtype : 'gridItems',
        flex : 3,
        disabled : true
    },
    {
        xtype : 'tabpanel',
        disabled : true,
        height : 150,
        resizable: true,
        name : 'tabItems',
        items : [
        {
            xtype : 'gridItemsItemsDestino',
            title : 'Items Destino'
        },
        {
            xtype : 'gridItemsItemsOrigen',
            title : 'Items Origen'
        } ]

    } ]

});