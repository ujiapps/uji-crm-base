Ext.define('CRM.view.grupos.VentanaGrupos',
{
    extend : 'Ext.Window',
    title : 'Finestra Suscriptors',
    width : 700,
    height : 410,
    alias : 'widget.ventanaGrupos',
    modal : true,

    closeAction : 'hide',

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    fbar : [
    {
        xtype : 'button',
        name : 'guardarGrupo',
        text : 'Actualitzar'

    } ],

    items : [
    {
        xtype : 'form',
        name : 'formGrupo',
        frame : true,
        items : [
        {
            xtype : 'container',
            layout : 'hbox',
            padding : '5',

            items : [
            {
                xtype : 'textfield',
                fieldLabel : 'Id',
                name : 'id',
                hidden : true
            },
            {
                xtype : 'combobox',
                name : 'programaId',
                store : 'StoreProgramasAccesoAdmin',
                displayField : 'nombreCa',
                fieldLabel : 'Programa',
                valueField : 'id',
                allowBlank : false,
                padding : '3 5 3 0',
                labelWidth : 80,
                editable : false,
                flex : 1
            },
            {
                xtype : 'combobox',
                name : 'tipoAccesoId',
                store : 'StoreTiposAcceso',
                flex : 1,
                editable : false,
                displayField : 'nombre',
                fieldLabel : 'Access',
                valueField : 'id',
                allowBlank : false,
                padding : '3 5 3 0',
                labelWidth : 80
            } ]
        },
        {
            xtype : 'container',
            layout : 'hbox',
            padding : 5,

            items : [
            {
                xtype : 'textfield',
                fieldLabel : 'Ordre',
                name : 'orden',
                padding : '3 5 3 0',
                labelWidth : 80,
                allowBlank : false,
                regex : /^[0-9]+$/,
                regexText : 'Sols pot contenir caracters numerics',
                maxLength : 9,
                maxLengthText : 'El ordre no pot tindre mes de nou caracteres',
                flex : 1
            },
            {
                xtype : 'checkbox',
                name : 'visible',
                labelWidth : 80,
                flex : 1,
                padding : 5,
                fieldLabel : 'Visible'

            } ]
        },
        {
            xtype : 'container',
            layout : 'hbox',
            padding : 5,

            items : [
            {
                xtype : 'textfield',
                fieldLabel : 'Desde',
                name : 'desde',
                allowBlank : false,
                vtype : 'email',
                padding : '3 5 3 0',
                labelWidth : 80,
                flex : 1
            },
            {
                xtype : 'textfield',
                fieldLabel : 'Asunto',
                name : 'asunto',
                allowBlank : false,
                padding : '3 5 3 0',
                labelWidth : 80,
                flex : 1
            } ]
        },
        {
            xtype : 'container',
            layout : 'hbox',
            padding : 5,

            items : [
            {
                xtype : 'textfield',
                fieldLabel : 'Respondre a',
                name : 'responderA',
                allowBlank : false,
                vtype : 'email',
                padding : '3 5 3 0',
                labelWidth : 80,
                flex : 1
            },
            {
                xtype : 'textfield',
                fieldLabel : 'Signatura',
                name : 'firma',
                padding : '3 5 3 0',
                labelWidth : 80,
                flex : 1
            } ]
        },

        {
            xtype : 'tabpanel',
            fieldLabel : 'Nom',
            name : 'nombre',
            padding : 5,
            align : 'stretch',

            items : [
            {
                xtype : 'panel',
                layout : 'vbox',
                padding : '5',
                title : 'Texte',

                items : [
                {
                    xtype : 'textfield',
                    fieldLabel : 'Nom',
                    name : 'nombreCa',
                    allowBlank : false,
                    width : 640
                },
                {
                    xtype : 'textarea',
                    fieldLabel : 'Descripciò',
                    name : 'descripcionCa',
                    autoScroll : true,
                    width : 640,
                    height : 110
                } ]
            },
            {
                xtype : 'panel',
                layout : 'vbox',
                padding : '5',
                title : 'Texto',

                items : [
                {
                    xtype : 'textfield',
                    fieldLabel : 'Nombre',
                    name : 'nombreEs',
                    allowBlank : false,
                    width : 640
                },
                {
                    xtype : 'textarea',
                    fieldLabel : 'Descripción',
                    name : 'descripcionEs',
                    autoScroll : true,
                    width : 640,
                    height : 110
                } ]
            },
            {
                xtype : 'panel',
                layout : 'vbox',
                padding : '5',
                title : 'Text',

                items : [
                {
                    xtype : 'textfield',
                    fieldLabel : 'Name',
                    name : 'nombreUk',
                    allowBlank : false,
                    width : 640
                },
                {
                    xtype : 'textarea',
                    fieldLabel : 'Description',
                    name : 'descripcionUk',
                    autoScroll : true,
                    flex : 1,
                    width : 640,
                    height : 110
                } ]
            } ]
        } ]

    } ]
});