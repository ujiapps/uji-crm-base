Ext.define('CRM.view.commons.GrouppedComboBox',
{
    extend : 'Ext.form.field.ComboBox',
    alias : 'widget.grouppedComboBox',

    constructor : function(args)
    {
        args.tpl = new Ext.XTemplate('<tpl for=".">', '<tpl if="this.' + args.groupField + ' != values.' + args.groupField + '">', '<tpl exec="this.' + args.groupField + ' = values.' + args.groupField + '"></tpl>',
                '<div class="x-panel-header-default x-panel-header-text-container x-panel-header-text x-panel-header-text-default" title="{' + args.groupDisplayField + '}">{' + args.groupDisplayField
                        + '}</div>', '</tpl>', '<div class="x-boundlist-item">{' + args.displayField + '}</div>', '</tpl>');

        this.callParent(arguments);
    }
});