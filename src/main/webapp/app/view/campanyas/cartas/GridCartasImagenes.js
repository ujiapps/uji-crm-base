Ext.define('CRM.view.campanyas.cartas.GridCartasImagenes',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridCartasImagenes',
        store: 'StoreCartasImagenes',
        sortableColumns: true,
        autoScroll: true,
        hideHeaders: true,
        defaults: {
            border: 0
        },
        selModel: {
            mode: 'SINGLE'
        },

        tbar: [

            {
                xtype: 'filefield',
                fieldLabel: 'Imagen',
                labelWidth: 50,
                name: 'anyadirImagen',
                buttonConfig: {
                    text: 'Sel·lecciona...',
                    disabled: false
                },
                flex: 5,
                regex: /(.)+((\.png)|(\.jpg)|(\.jpeg)(\w)?)$/i,
                regexText: 'Only PNG and JPEG image formats are accepted'
            },
            {
                xtype: 'button',
                name: 'botonAnyadirImagen',
                iconCls: 'accept',
                disabled: true,
                flex: 0.5
            },
            {
                xtype: 'button',
                name: 'botonBorrarImagen',
                iconCls: 'application-delete',
                disabled: true,
                flex: 0.5
            }],

        columns: [
            {
                text: 'id',
                dataIndex: 'id',
                hidden: true
            },
            {
                dataIndex: 'nombre',
                flex: 2
            }]

    });
