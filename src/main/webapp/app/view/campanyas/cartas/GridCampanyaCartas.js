Ext.define('CRM.view.campanyas.cartas.GridCampanyaCartas',
    {
        extend: 'Ext.grid.Panel',
        store: 'StoreCampanyaCartas',
        alias: 'widget.gridCampanyaCartas',
        requires: ['Ext.grid.plugin.RowEditing'],
        sortableColumns: true,
        selModel: {
            mode: 'SINGLE'
        },
        plugins: [
            {
                ptype: 'rowediting',
                clicksToEdit: 2,
                pluginId: 'editingCampanyaCartas'
            }],
        tbar: [
            {
                xtype: 'button',
                name: 'anyadirCampanyaCarta',
                text: 'Afegir',
                iconCls: 'application-add'
            },
            {
                xtype: 'button',
                name: 'borrarCampanyaCarta',
                text: 'Esborrar',
                iconCls: 'application-delete',
                disabled: true
            }],

        columns: [
            {
                header: 'id',
                hidden: true,
                dataIndex: 'id',
                menuDisabled: true
            },
            {
                header: 'Tipus',
                dataIndex: 'campanyaCartaTipoId',
                menuDisabled: true,
                flex: 1,
                renderer: function (value) {
                    if (value) {
                        var store = Ext.getStore('StoreTiposEstadoCampanya');
                        var data = store.getById(value);
                        return value ? data.get('nombre') : '';
                    }
                },
                editor: {
                    xtype: 'combobox',
                    store: Ext.getStore('StoreTiposEstadoCampanya'),
                    queryMode : 'local',
                    editable: false,
                    displayField: 'nombre',
                    valueField: 'id',
                    allowBlank: false,
                    emptyText: 'Selecciona un estat'
                }
            },
            {
                header: 'Corp',
                dataIndex: 'cuerpo',
                menuDisabled: true,
                flex: 3
            }]
    });
