Ext.define('CRM.view.campanyas.cartas.FormularioCampanyaCarta',
    {
        extend: 'Ext.form.Panel',
        title: 'Cartes',
        alias: 'widget.formularioCampanyaCarta',
        modal: true,
        layout: {
            type: 'hbox',
            align: 'stretch'
        },

        fbar: [
            {
                xtype: 'button',
                text: 'Actualitzar',
                action: 'actualizar',
                name: 'actualizar'
            }],

        items: [
            {
                xtype: 'container',
                flex: 2,
                layout: {
                    type: 'form'

                },
                padding: 5,
                items: [
                    {
                        xtype: 'combo',
                        editable: false,
                        padding: 5,
                        margin: 5,
                        fieldLabel: 'Tipus',
                        store: 'StoreTiposEstadoCampanya',
                        displayField: 'nombre',
                        valueField: 'id',
                        name: 'tipoCarta',
                        queryMode: 'local'
                    },
                    {
                        xtype: 'container',
                        layout: 'fit',
                        name: 'editorCartas',
                        flex: 1
                    }
                ]
            }, {
                xtype: 'form',
                collapsible: true,
                collapsed: true,
                frame: true,
                collapseDirection: 'left',
                border: 0,
                name: 'formSubirImagenesCartas',
                title: 'Imagenes',
                width: 300,
                items: [
                    {
                        xtype: 'gridCartasImagenes',
                        name: 'gridCartasImagenes'
                    }]

            }
        ]
    })
;