Ext.define('CRM.view.campanyas.formularios.VentanaCampanyaDatos',
    {
        extend: 'Ext.Window',
        title: 'Datos Campanya',
        width: 700,
        height: 600,
        alias: 'widget.ventanaCampanyaDatos',
        modal: true,
        layout: 'fit',
        fbar: [
            {
                xtype: 'button',
                text: 'Afegir',
                //action: 'anyadirDatoCampanya',
                name: 'anyadirDatoCampanya'
            },
            {
                xtype: 'button',
                text: 'Actualitzar',
                //action: 'actualizarDatoCampanya',
                name: 'actualizarDatoCampanya'
            },
            {
                xtype: 'button',
                text: 'Cancel·lar',
                action: 'cancelarDatoCampanya',
                name: 'cancelarDatoCampanya'
            }],

        items: [
            {
                xtype: 'form',
                name: 'formDatoCampanya',
                modal: true,
                bodyPadding: 5,
                defaults: {
                    padding: 5,
                    xtype: 'textfield',
                    allowBlank: true
                },
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                autoScroll: true,
                items: [
                    {
                        fieldLabel: 'Dada',
                        name: 'clienteDatoTipo',
                        xtype: 'combobox',
                        store: 'StoreClientesDatosTipos',
                        editable: false,
                        displayField: 'nombre',
                        valueField: 'id',
                        queryMode: 'local',
                        allowBlank: false
                    },
                    {
                        fieldLabel: 'Texte',
                        name: 'textoCa'
                    },
                    {
                        fieldLabel: 'Texte ayuda',
                        name: 'textoAyudaCa'
                    },
                    {
                        fieldLabel: 'Texto',
                        name: 'textoEs'
                    },
                    {
                        fieldLabel: 'Text ayuda',
                        name: 'textoAyudaEs'
                    },
                    {
                        fieldLabel: 'Text',
                        name: 'textoUk'
                    },
                    {
                        fieldLabel: 'Texte help',
                        name: 'textoAyudaUk'
                    },
                    {
                        fieldLabel: 'Grup',
                        name: 'grupo',
                        xtype: 'combobox',
                        store: 'StoreGruposTodos',
                        editable: true,
                        displayField: 'nombreCa',
                        valueField: 'id',
                        queryMode: 'local'
                    },
                    {
                        fieldLabel: 'Item',
                        name: 'item',
                        xtype: 'comboItems',
                        store: 'StoreItemsCampanya',
                        editable: true,
                        displayField: 'nombreCa',
                        valueField: 'id',
                        queryMode: 'local'
                    },
                    {
                        fieldLabel: 'Dada Extra Relacionada',
                        name: 'campanyaDatoExtra',
                        xtype: 'combobox',
                        store: 'StoreCampanyaDatos',
                        editable: true,
                        displayField: 'textoCa',
                        valueField: 'id',
                        queryMode: 'local'
                    },
                    {
                        fieldLabel: 'Vinculado Campanya',
                        name: 'vinculadoCampanya',
                        xtype: 'checkbox'
                    },
                    {
                        fieldLabel: 'Tipus d\'access',
                        name: 'tipoAcceso',
                        xtype: 'combobox',
                        store: 'StoreTiposDatosAcceso',
                        editable: false,
                        displayField: 'nombre',
                        valueField: 'id',
                        queryMode: 'local',
                        allowBlank: false
                    },
                    {
                        fieldLabel: 'Requerit',
                        name: 'requerido',
                        xtype: 'checkbox'
                    },
                    {
                        fieldLabel: 'Tamany',
                        name: 'tamanyo'
                    },
                    {
                        fieldLabel: 'Ordre',
                        name: 'orden',
                        allowBlank: false
                    },
                    {
                        fieldLabel: 'Etiqueta',
                        name: 'etiqueta'
                    },
                    {
                        fieldLabel: 'Validar com a',
                        name: 'validar',
                        xtype: 'combobox',
                        store: 'StoreTiposValidacion',
                        editable: false,
                        displayField: 'nombre',
                        valueField: 'id',
                        queryMode: 'local',
                        allowBlank: true
                    },
                    {
                        title: 'Opciones de visualización',
                        xtype: 'fieldset',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                fieldLabel: 'Visualizar',
                                name: 'visualizarSoloSi',
                                xtype: 'fieldcontainer',
                                defaultType: 'radiofield',
                                items: [
                                    {
                                        boxLabel: 'Siempre',
                                        name: 'radioBoxVisualizarSoloSi',
                                        inputValue: true,
                                        id: 'radio1',
                                        listeners: {
                                            change: function (container, valor) {
                                                this.up('form').down("combobox[name=campanyaPreguntaRelacionada]").setVisible(!valor);
                                                this.up('form').down("combobox[name=campanyaRespuestaRelacionada]").setVisible(!valor);
                                            }
                                        }
                                    }, {
                                        boxLabel: 'Solo si marca esta contestación',
                                        name: 'radioBoxVisualizarSoloSi',
                                        inputValue: false,
                                        id: 'radio2',
                                        listeners: {
                                            change: function (container, valor) {
                                                this.up('form').down("combobox[name=campanyaPreguntaRelacionada]").setVisible(valor);
                                                this.up('form').down("combobox[name=campanyaRespuestaRelacionada]").setVisible(valor);
                                            }
                                        }
                                    }
                                ]

                            },
                            {
                                fieldLabel: 'Pregunta Relacionada',
                                name: 'campanyaPreguntaRelacionada',
                                xtype: 'combobox',
                                store: 'StoreCampanyaDatos',
                                editable: true,
                                displayField: 'textoCa',
                                valueField: 'id',
                                queryMode: 'local',
                                hidden: true,
                                listeners: {
                                    change: function (container, valor) {
                                        this.up('form').down("combobox[name=campanyaRespuestaRelacionada]").getStore().load({
                                            url: '/crm/rest/item/datoCampanya',
                                            params: {
                                                datoCampanya: valor
                                            }
                                        });
                                    }
                                }
                            },
                            {
                                fieldLabel: 'Contestación Relacionada',
                                name: 'campanyaRespuestaRelacionada',
                                xtype: 'combobox',
                                store: 'StoreItemsContestacion',
                                editable: true,
                                displayField: 'nombreCa',
                                valueField: 'id',
                                queryMode: 'local',
                                hidden: true
                            }
                        ]
                    }
                ]
            }]
    });