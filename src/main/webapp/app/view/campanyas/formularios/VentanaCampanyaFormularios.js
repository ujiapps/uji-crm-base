Ext.define('CRM.view.campanyas.formularios.VentanaCampanyaFormularios',
    {
        extend: 'Ext.Window',
        title: 'Datos Formulari',
        width: 1000,
        alias: 'widget.ventanaCampanyaFormularios',
        requires: ['CRM.view.campanyas.formularios.GridCampanyaFormularioEstadosOrigen', 'CRM.view.clientes.VentanaClienteCampanya.PanelClienteCampanya'],
        modal: true,
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        fbar: [
            {
                xtype: 'button',
                text: 'Afegir',
                name: 'anyadirFormularioCampanya'
            },
            {
                xtype: 'button',
                text: 'Actualitzar',
                name: 'actualizarFormularioCampanya'
            },
            {
                xtype: 'button',
                text: 'Cancel·lar',
                action: 'cancelarFormularioCampanya',
                name: 'cancelarFormularioCampanya'
            }],

        items: [
            {
                xtype: 'form',
                name: 'formFormularioCampanya',
                maxHeight: 600,
                autoScroll: true,
                modal: true,
                flex: 2,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        fieldLabel: 'Títol',
                        name: 'titulo',
                        padding: 5,
                        xtype: 'textfield'
                    },
                    {
                        fieldLabel: 'Plantilla',
                        name: 'plantilla',
                        padding: 5,
                        xtype: 'combobox',
                        store : Ext.create('CRM.store.StoreTipoPlantillas'),
                        editable: false,
                        displayField: 'nombre',
                        valueField: 'nombre',
                        queryMode: 'local',
                        allowBlank: true,
                        trigger2Cls: 'x-form-clear-trigger',
                        onTrigger2Click: function (event) {
                            var me = this;
                            me.clearValue();
                        }
                    },
                    {
                        fieldLabel: 'Cabecera',
                        name: 'cabecera',
                        padding: 5,
                        xtype: 'textfield'
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                fieldLabel: 'Sol. DNI',
                                name: 'solicitarIdentificacion',
                                renderer: function (val) {
                                    return this.up("window").renderSolicitudDato(val)
                                },
                                xtype: 'combobox',
                                store: 'StoreTiposSolicitudCampanyaDatos',
                                editable: false,
                                displayField: 'nombre',
                                valueField: 'id',
                                queryMode: 'local',
                                padding: 5,
                                allowBlank: false,
                                flex: 1
                            },
                            {
                                fieldLabel: 'Sol. Nom',
                                name: 'solicitarNombre',
                                renderer: function (val) {
                                    return this.up("window").renderSolicitudDato(val)
                                },
                                xtype: 'combobox',
                                store: 'StoreTiposSolicitudCampanyaDatos',
                                editable: false,
                                displayField: 'nombre',
                                valueField: 'id',
                                queryMode: 'local',
                                padding: 5,
                                allowBlank: false,
                                flex: 1
                            }]
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                fieldLabel: 'Sol. eMail',
                                name: 'solicitarCorreo',
                                flex: 1,
                                renderer: function (val) {
                                    return this.up("window").renderSolicitudDato(val)
                                },
                                xtype: 'combobox',
                                store: 'StoreTiposSolicitudCampanyaDatos',
                                editable: false,
                                displayField: 'nombre',
                                valueField: 'id',
                                queryMode: 'local',
                                padding: 5,
                                allowBlank: false
                            },
                            {
                                fieldLabel: 'Sol. mobil',
                                name: 'solicitarMovil',
                                flex: 1,
                                renderer: function (val) {
                                    return this.up("window").renderSolicitudDato(val)
                                },
                                xtype: 'combobox',
                                store: 'StoreTiposSolicitudCampanyaDatos',
                                editable: false,
                                displayField: 'nombre',
                                valueField: 'id',
                                queryMode: 'local',
                                allowBlank: false,
                                padding: 5
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                fieldLabel: 'Sol. postal',
                                name: 'solicitarPostal',
                                flex: 1,
                                renderer: function (val) {
                                    return this.up("window").renderSolicitudDato(val)
                                },
                                xtype: 'combobox',
                                store: 'StoreTiposSolicitudCampanyaDatos',
                                editable: false,
                                displayField: 'nombre',
                                valueField: 'id',
                                queryMode: 'local',
                                allowBlank: false,
                                padding: 5
                            },
                            {
                                fieldLabel: 'Activar Autenticación',
                                name: 'autenticaFormulario',
                                flex: 1,
                                padding: 5,
                                xtype: 'checkbox'
                            }]
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                fieldLabel: 'Fecha Inici',
                                name: 'fechaInicio',
                                padding: 5,
                                xtype: 'datefield',
                                flex: 1

                            },
                            {
                                fieldLabel: 'Fecha Fí',
                                name: 'fechaFin',
                                padding: 5,
                                xtype: 'datefield',
                                flex: 1
                            }]
                    },
                    {
                        xtype: 'container',
                        padding: 5,
                        height: 200,
                        autoScroll: true,
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'checkboxgroup',
                                fieldLabel: 'Estats Origen',
                                defaultType: 'checkboxfield',
                                name: 'checkboxfieldEstadosOrigen',
                                vertical: true,
                                columns: 1,
                                padding: 5,
                                flex: 1,
                                items: []

                            },
                            {
                                xtype: 'fieldcontainer',
                                fieldLabel: 'Estat Destí',
                                defaultType: 'radiofield',
                                name: 'radiofieldEstadoDestino',
                                padding: 5,
                                flex: 1,
                                items: []

                            }
                        ]
                    },
                    {
                        xtype: 'combobox',
                        padding: 5,
                        store: 'StoreCampanyaActos',
                        editable: false,
                        displayField: 'titulo',
                        valueField: 'id',
                        queryMode: 'local',
                        allowBlank: true,
                        fieldLabel: 'Acto asociado',
                        name: 'acto'
                    },
                    {
                        xtype: 'gridCampanyaDatos',
                        padding: 5,
                        height: 200,
                        title: 'Dades Formulari',
                        autoScroll: true
                    }
                ]
            },
            {
                xtype: 'panel',
                name: 'previsualizacionFormulario',
                maxHeight: 600,
                autoScroll: true,
                flex: 1,
                items: [
                    {
                        xtype: 'form',
                        name: 'formPrevisualizacionClienteCampanya',
                        padding: 10,
                        enableKeyEvents: true,
                        frame: true,
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: []
                    }]
            }
        ]
    })
;