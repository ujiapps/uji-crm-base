Ext.define('CRM.view.campanyas.formularios.GridCampanyaFormularioEstadosOrigen',
    {
        //extend: 'Ext.ux.uji.grid.Panel',
        extend: 'Ext.grid.Panel',
        store: 'StoreCampanyasFormularioEstadoOrigen',
        alias: 'widget.gridCampanyaFormularioEstadosOrigen',
        name: 'gridCampanyaFormularioEstadosOrigen',

        plugins: [
            {
                ptype: 'rowediting',
                clicksToEdit: 2,
                pluginId: 'editingCampanyaFormularioEstadoOrigen'
            }],

        tbar: [
            {
                xtype: 'button',
                name: 'anyadirCampanyaFormularioEstadoOrigen',
                text: 'Afegir',
                iconCls: 'application-add'
            },
            {
                xtype: 'button',
                name: 'borrarCampanyaFormularioEstadoOrigen',
                text: 'Esborrar',
                iconCls: 'application-delete'
            }],
        columns: [{
            header: 'Estat',
            dataIndex: 'estadoOrigenId',
            flex: 0.8,
            xtype: 'combocolumn',
            combo: {
                xtype: 'combobox',
                store: Ext.getStore('StoreTiposEstadoCampanya'),
                displayField: 'nombre',
                valueField: 'id',
                queryMode: 'local',
                name: 'comboEstadoOrigenFormularioCampanya'
            }
        }],


        //initComponent: function () {
        //    var me = this;
        //    me.columns = [
        //        {
        //            header: 'Estat',
        //            dataIndex: 'estadoOrigenId',
        //            flex: 0.8,
        //            xtype: 'combocolumn',
        //            combo: {
        //                xtype: 'combobox',
        //                store: Ext.getStore('StoreTiposEstadoCampanyaFormularioEstadosOrigen'),
        //displayField: 'nombre',
        //valueField: 'id',
        //queryMode: 'local',
        //name: 'comboEstadoOrigenFormularioCampanya'
        //}
        //
        //}];
        //this.callParent(arguments);
        //},

        getSelectedId: function () {
            var selection = this.getSelectionModel().getSelection();

            if (selection.length > 0) {
                return selection[0].get("id");
            }
        },

        getSelectedRow: function () {
            var selection = this.getSelectionModel().getSelection();

            if (selection.length > 0) {
                return selection[0];
            }
            //},
            //
            //reloadData: function () {
            //    this.getStore().load();
        }
    });
