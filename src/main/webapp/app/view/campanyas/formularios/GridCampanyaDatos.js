Ext.define('CRM.view.campanyas.formularios.GridCampanyaDatos',
    {
        extend: 'Ext.grid.Panel',
        store: 'StoreCampanyaDatos',
        alias: 'widget.gridCampanyaDatos',
        sortableColumns: true,

        selModel: {
            mode: 'SINGLE'
        },

        tbar: [
            {
                xtype: 'button',
                name: 'anyadir',
                text: 'Afegir',
                iconCls: 'application-add'
            },
            {
                xtype: 'button',
                name: 'borrar',
                text: 'Esborrar',
                iconCls: 'application-delete',
                disabled: true
            }],

        columns: [
            {
                text: 'Texte',
                dataIndex: 'textoCa',
                menuDisabled: true,
                flex: 2
            },
            {
                text: 'Tipus d\'access',
                dataIndex: 'accesoNombre',
                menuDisabled: true,
                flex: 1
            },
            {
                text: 'Ordre',
                dataIndex: 'orden',
                name: 'orden',
                flex: 0.5
            }]
    });
