function formatDate(value) {
    return value ? Ext.Date.dateFormat(value, 'd/m/Y') : '';
}

Ext.define('CRM.view.campanyas.formularios.GridCampanyaFormularios', {
    extend: 'Ext.grid.Panel',
    store: 'StoreCampanyaFormularios',
    alias: 'widget.gridCampanyaFormularios',
    sortableColumns: true,

    selModel: {
        mode: 'SINGLE'
    },

    tbar: [{
        xtype: 'button',
        name: 'anyadir',
        text: 'Afegir',
        iconCls: 'application-add'
    }, {
        xtype: 'button',
        name: 'borrar',
        text: 'Esborrar',
        iconCls: 'application-delete',
        disabled: true
    }, {
        xtype: 'button',
        name: 'duplicar',
        text: 'Duplicar',
        disabled: true
    }, {
        xtype: 'button',
        name: 'direccionGenerica',
        text: 'Copiar direccion formulario Generica',
        campanyaId: '',
        icon: '//static.uji.es/js/extjs/uji-commons-extjs/img/page_white_copy.png',
        handler: function () {
            const el = document.createElement('textarea');
            el.value = window.location.href + 'rest/publicacion/' + this.campanyaId;
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            Ext.Msg.alert('Copy', 'Direcció ' + el.value + ' copiada al portapapers');
            document.body.removeChild(el);
        }
    }],

    columns: [{
        text: 'Títol',
        dataIndex: 'titulo',
        menuDisabled: true,
        flex: 2
    }, {
        text: 'Data Inici',
        dataIndex: 'fechaInicio',
        format: 'd/m/Y',
        renderer: formatDate,
        flex: 1
    }, {
        text: 'Data Fí',
        dataIndex: 'fechaFin',
        format: 'd/m/Y',
        renderer: formatDate,
        flex: 1
    }, {
        xtype: 'actioncolumn',
        text: 'Copi direcció',
        name: 'direccionFormulario',
        flex: 0.5,
        menuDisabled: true,
        // formularioId: 'id',
        align: 'center',
        items: [{
            // type: 'button',
            name: 'copiarDireccionFormulario',
            icon: '//static.uji.es/js/extjs/uji-commons-extjs/img/page_white_copy.png',
            tooltip: 'Copi',
            handler: function (grid, rowIndex) {
                var rec = grid.getStore().getAt(rowIndex);
                const el = document.createElement('textarea');
                el.value = window.location.href + 'rest/publicacion/formulario/' + rec.data.id;
                el.setAttribute('readonly', '');
                el.style.position = 'absolute';
                el.style.left = '-9999px';
                document.body.appendChild(el);
                el.select();
                document.execCommand('copy');
                Ext.Msg.alert('Copy', 'Direcció ' + el.value + ' copiada al portapapers');
                document.body.removeChild(el);
            }
        }]
    }]
});
