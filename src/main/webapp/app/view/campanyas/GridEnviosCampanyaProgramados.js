Ext.define('CRM.view.campanyas.GridEnviosCampanyaProgramados',
    {
        extend: 'Ext.grid.Panel',
        //title : 'Tarifes',
        alias: 'widget.gridEnviosCampanyaProgramados',
        store: 'StoreCampanyasEnviosProgramados',
        requires: ['Ext.grid.plugin.RowEditing'],
        forceFit: true,
        sortableColumns: true,
        name: 'gridEnviosCampanyaProgramados',

        selModel: {
            mode: 'SINGLE'
        },

        plugins: [
            {
                ptype: 'rowediting',
                clicksToEdit: 2,
                pluginId: 'editingEnvioProgramado'
            }],

        tbar: [
            {
                xtype: 'button',
                name: 'anyadirEnvioProgramado',
                text: 'Afegir',
                iconCls: 'application-add'
            },
            {
                xtype: 'button',
                name: 'borrarEnvioProgramado',
                text: 'Esborrar',
                iconCls: 'application-delete',
                disabled: true
            }],

        columns: [
            {
                text: 'Díes',
                dataIndex: 'dias',
                flex: 1,
                editor: {
                    xtype: 'textfield',
                    allowBlank: false
                }
            }]

    });
