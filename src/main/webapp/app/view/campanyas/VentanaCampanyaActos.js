Ext.define('CRM.view.campanyas.VentanaCampanyaActos',
    {
        extend: 'Ext.Window',
        title: 'Actos Campanya',
        width: 700,
        alias: 'widget.ventanaCampanyaActos',
        modal: true,
        fbar: [
            {
                xtype: 'button',
                text: 'Afegir',
                name: 'anyadirActoCampanya'
            },
            {
                xtype: 'button',
                text: 'Actualitzar',
                name: 'actualizarActoCampanya',
                hidden: true
            },
            {
                xtype: 'button',
                text: 'Cancel·lar',
                name: 'cancelarActoCampanya'
            }],

        items: [
            {
                xtype: 'form',
                name: 'formActoCampanya',
                modal: true,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        fieldLabel: 'Titulo',
                        name: 'titulo',
                        allowBlank: true,
                        padding: 5,
                        xtype: 'textfield'
                    },
                    {
                        fieldLabel: 'Resumen',
                        allowBlank: true,
                        name: 'resumen',
                        padding: 5,
                        xtype: 'textarea'
                    }
                    ,
                    {
                        fieldLabel: 'Duracion',
                        name: 'duracion',
                        allowBlank: true,
                        padding: 5,
                        xtype: 'textfield',
                        regex: /^[0-9]+$/,
                        regexText: 'Sols pot contenir caracters numerics'
                    },
                    {
                        fieldLabel: 'Contenido',
                        name: 'contenido',
                        xtype: 'textfield',
                        allowBlank: true,
                        padding: 5
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'datefield',
                                name: 'fecha',
                                fieldLabel: 'Fecha',
                                format: 'd/m/Y',
                                allowBlank: true,
                                padding: 5,
                                flex: 2
                            },
                            {
                                fieldLabel: 'Hora Evento',
                                name: 'hora',
                                padding: 5,
                                allowBlank: true,
                                xtype: 'textfield',
                                regex: /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/,
                                regexText: 'Formato de hora hh:mm',
                                flex: 1
                            },
                            {
                                fieldLabel: 'Hora Apertura',
                                name: 'horaApertura',
                                padding: 5,
                                allowBlank: true,
                                xtype: 'textfield',
                                regex: /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/,
                                regexText: 'Formato de hora hh:mm',
                                flex: 1
                            }]
                    },
                    {
                        fieldLabel: 'Imagen',
                        name: 'imagen',
                        padding: 5,
                        xtype: 'filefield',
                        buttonConfig: {
                            text: 'Sel·lecciona...',
                            disabled: false
                        },
                        regex: /(.)+((\.png)|(\.jpg)|(\.jpeg)(\w)?)$/i,
                        regexText: 'Only PNG and JPEG image formats are accepted'
                    },

                    {
                        fieldLabel: 'Rss Activo',
                        name: 'rssActivo',
                        padding: 5,
                        xtype: 'checkbox'
                    }
                ]
            }]
    });