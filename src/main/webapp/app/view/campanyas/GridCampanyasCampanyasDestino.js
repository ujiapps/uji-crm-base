Ext.define('CRM.view.items.GridCampanyasCampanyasDestino',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridCampanyasCampanyasDestino',
        store: 'StoreCampanyasCampanyasDestino',
        forceFit: true,
        sortableColumns: true,
        tbar: [
            {
                xtype: 'button',
                name: 'anyadirCampanyasCampanyasDestino',
                text: 'Afegir',
                iconCls: 'application-add'
            },
            {
                xtype: 'button',
                name: 'borrarCampanyasCampanyasDestino',
                text: 'Esborrar',
                iconCls: 'application-delete'
            }],

        columns: [
            {
                header: 'Estado origen',
                dataIndex: 'estadoOrigen'
            },
            {
                header: 'Campaña',
                xtype: 'combocolumn',
                dataIndex: 'campanyaDestinoId',

                combo: {
                    xtype: 'combobox',
                    editable: false,
                    store: Ext.create('CRM.store.StoreCampanyas'),
                    displayField: 'nombre',
                    valueField: 'id',
                    name: 'campanyaDestino',
                    queryMode: 'local'
                }
            },
            {
                header: 'Estado destino',
                dataIndex: 'estadoDestino'
            }]

    });
