Ext.define('CRM.view.campanyas.GridCampanyas', {
    extend: 'Ext.grid.Panel',
    title: 'Campanyes',
    alias: 'widget.gridCampanyas',
    store: 'StoreCampanyas',
    sortableColumns: true,

    selModel: {
        mode: 'SINGLE'
    },
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'StoreCampanyas',
        dock: 'bottom',
        displayInfo: true
    }],
    tbar: [{
        xtype: 'button',
        name: 'anyadirCampanya',
        text: 'Afegir',
        iconCls: 'application-add'
    }, {
        xtype: 'button',
        name: 'borrarCampanya',
        text: 'Esborrar',
        iconCls: 'application-delete',
        disabled: true
    }, {
        xtype: 'button',
        name: 'anyadirCampanyaActos',
        text: 'Afegir nova campanya Actes de graduació',
        iconCls: 'application-add'
    }, {
        xtype: 'button',
        name: 'anyadirCampanyaActosConPagos',
        text: 'Afegir nova campanya Actes de graduació amb pagament d\'assistència',
        iconCls: 'application-add'
    }],

    columns: [{
        text: 'Nom',
        dataIndex: 'nombre',
        menuDisabled: true,
        flex: 2
    }, {
        text: 'Descripció',
        dataIndex: 'descripcion',
        menuDisabled: true,
        flex: 1
    }, {
        text: 'Programa',
        dataIndex: 'programaNombre',
        flex: 1
    }]
});
