Ext.define('CRM.view.programas.TabCampanyas',
    {
        extend: 'Ext.tab.Panel',
        alias: 'widget.tabCampanyas',
        requires: ['CRM.view.campanyas.GridCampanyaEnviosAdmin', 'CRM.view.campanyas.cartas.GridCampanyaCartas', 'CRM.view.campanyas.GridCampanyasCampanyasDestino',
            'CRM.view.campanyas.GridCampanyaActos', 'CRM.view.campanyas.estados.PanelCampanyaEstados', 'CRM.view.campanyas.cartas.FormularioCampanyaCarta'],
        activeTab: 0,
        disabled: true,

        items: [
            {
                title: 'Estados',
                xtype: 'panelCampanyaEstados',
                closable: false,
                tooltip: 'Defineix els estats en que es pot encontrar una inscripció a la campanya'
            },
            {
                title: 'Fases',
                xtype: 'gridCampanyaFases',
                closable: false,
                tooltip: 'Defineix les fases i períodes de les campanyes, posteriorment associarem les accions a aquestes fases'
            },
            {
                title: 'Formularis',
                xtype: 'gridCampanyaFormularios',
                closable: false,
                tooltip: 'Defineix els tipus de dades que necessitarem per a la campanya, a partir d\'aquests camps es genera el formulari d\'inscripció / modificació de dades'
            },
            {
                title: 'Items',
                xtype: 'gridCampanyaItems',
                closable: false,
                tooltip: 'Defineix els ítems que s\'associen a l\'usuari quan es doni d\'alta a la campanya'
            },
            {
                title: 'Campanyes destino-origen',
                xtype: 'gridCampanyasCampanyasDestino',
                closable: false,
                tooltip: 'Defineix les campanyes que s\'associen a l\'usuari quan es doni d\'alta a la campanya'
            },
            // {
            //     title: 'Campanyes origen',
            //     xtype: 'gridCampanyasCampanyasOrigen',
            //     closable: false,
            //     tooltip: 'Defineix en que campanyes al donarse d\'alta a l\'usuari s\'associarà aquesta campanya'
            // },
            {
                title: 'Enviaments',
                xtype: 'gridCampanyaEnviosAuto',
                closable: false,
                tooltip: 'Defineix els enviaments que s\'envien a l\'usuari quan canvia l\'estat del mateix en la campanya'
            }, {
                xtype: 'container',
                title: 'Cartes',
                tooltip: 'Defineix les cartes que se generen a l\'usuari',
                closable: false,
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                items: [{
                    xtype: 'gridCampanyaCartas',
                    collapsible: true,
                    collapsed: false,
                    frame: true,
                    collapseDirection: 'left',
                    border: 0,
                    flex: 2
                }, {
                    xtype: 'formularioCampanyaCarta',
                    flex: 1,
                    disabled: true
                }]
            },

            {
                title: 'Enviaments Administració',
                xtype: 'gridCampanyaEnviosAdmin',
                closable: false,
                tooltip: 'Defineix els enviaments que s\'envien a l\'administrador quan canvia l\'estat de un usuari en la campanya'
            },
            {
                title: 'Actes',
                xtype: 'gridCampanyaActos',
                closable: false,
                tooltip: ''
            }]
    });