Ext.define('CRM.view.campanyas.VentanaCampanyaDestino',
    {
        extend: 'Ext.Window',
        title: 'Añadir Campanya Destino',
        width: 700,
        alias: 'widget.ventanaCampanyaDestino',
        modal: true,
        fbar: [
            {
                xtype: 'button',
                text: 'Afegir',
                name: 'anyadirCampanyaDestino'
            },
            {
                xtype: 'button',
                text: 'Actualitzar',
                name: 'actualizarCampanyaDestino',
                hidden: true
            },
            {
                xtype: 'button',
                text: 'Cancel·lar',
                name: 'cancelarCampanyaDestino'
            }],

        items: [
            {
                xtype: 'form',
                name: 'formCampanyaDestino',
                modal: true,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'combobox',
                        editable: false,
                        fieldLabel : 'Estado Origen',
                        store: 'StoreTiposEstadoCampanya',
                        displayField: 'nombre',
                        valueField: 'id',
                        padding: 5,
                        name: 'estadoOrigen',
                        queryMode: 'local'
                    },
                    {
                        xtype: 'combobox',
                        fieldLabel: 'Campaña Destino',
                        editable: false,
                        padding: 5,
                        store: Ext.create('CRM.store.StoreCampanyas'),
                        displayField: 'nombre',
                        valueField: 'id',
                        name: 'campanyaDestino',
                        queryMode: 'local'
                    },
                    {
                        xtype: 'combobox',
                        editable: false,
                        fieldLabel : 'Estado Destino',
                        padding: 5,
                        store: Ext.create('CRM.store.StoreTiposEstadoCampanya', {storeId: 'StoreTiposEstadoCampanyaDestino'}),
                        displayField: 'nombre',
                        valueField: 'id',
                        name: 'estadoDestino',
                        queryMode: 'local'
                    }

                ]
            }]
    });