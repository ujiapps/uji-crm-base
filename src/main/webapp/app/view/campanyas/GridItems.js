Ext.define('CRM.view.campanyas.GridItems',
{
    extend : 'Ext.grid.Panel',
    title : 'Selecciona els ítems a associar i premeu Afegir',
    alias : 'widget.campanyas.gridItems',
    store : 'StoreItemsVentanaCampanyas',
    forceFit : true,
    sortableColumns : true,
    multiSelect : true,

    columns : [
    {
        text : 'Item',
        dataIndex : 'nombreCa',
        flex : 2
    },
    {
        text : 'Id',
        dataIndex : 'id',
        name : 'id',
        hidden : true
    } ]

});
