Ext.define('CRM.view.campanyas.GridCampanyaEnvios',
{
    extend : 'Ext.grid.Panel',
    store : 'StoreCampanyaEnvios',
    alias : 'widget.gridCampanyaEnviosAuto',
    requires: ['Ext.ux.CKEditor'],
    sortableColumns : true,
    selModel :
    {
        mode : 'SINGLE'
    },
    tbar : [
    {
        xtype : 'button',
        name : 'anyadirCampanyaEnvio',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarCampanyaEnvio',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    },
    {
        xtype : 'textfield',
        labelWidth : 60,
        emptyText : 'Escriu un eMail...',
        vtype : 'email',
        name : 'emailPrueba',
        padding : '5 5 5 0'
    },
    {
        xtype : 'button',
        name : 'botonEnviarPrueba',
        text : 'Enviar prova',
        iconCls : 'accept',
        padding : '5 5 5 0',
        disabled : true
    } ],

    columns : [
    {
        text : 'id',
        hidden : true,
        dataIndex : 'id',
        menuDisabled : true

    },
    {
        text : 'Tipus',
        dataIndex : 'campanyaEnvioTipoNombre',
        name : 'envioTipo',
        flex : 1
    },
    {
        text : 'Asumpte',
        dataIndex : 'asunto',
        menuDisabled : true,
        flex : 1
    },
    {
        text : 'Desde',
        dataIndex : 'desde',
        menuDisabled : true,
        flex : 1
    },
    {
        text : 'Responder a',
        dataIndex : 'responder',
        menuDisabled : true,
        flex : 1
    },

    {
        text : 'Corp',
        dataIndex : 'cuerpo',
        menuDisabled : true,
        flex : 1
    } ]
});
