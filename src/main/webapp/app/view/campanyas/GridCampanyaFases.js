function formatDate(value)
{
    return value ? Ext.Date.dateFormat(value, 'd/m/Y') : '';
}

Ext.define('CRM.view.campanyas.GridCampanyaFases',
{
    extend : 'Ext.grid.Panel',
    title : 'Campanya Fases',
    alias : 'widget.gridCampanyaFases',
    store : 'StoreCampanyaFases',
    requires : [ 'Ext.grid.plugin.RowEditing' ],
    sortableColumns : true,

    selModel :
    {
        mode : 'SINGLE'
    },

    plugins : [
    {
        ptype : 'rowediting',
        clicksToEdit : 2,
        pluginId : 'editingFases'
    } ],

    tbar : [

    {
        xtype : 'button',
        name : 'anyadirCampanyaFase',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarCampanyaFase',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        dataIndex : 'id',
        hidden : true
    },
    {
        xtype : 'datecolumn',
        text : 'Data Inici',
        dataIndex : 'fechaIni',
        menuDisabled : true,
        format : 'd/m/Y',
        renderer : formatDate,
        flex : 1,
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            minValue : '1',
            allowBlank : true
        }
    },
    {
        xtype : 'datecolumn',
        text : 'Data fi',
        dataIndex : 'fechaFin',
        menuDisabled : true,
        flex : 1,
        format : 'd/m/Y',
        renderer : formatDate,
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            minValue : '1',
            allowBlank : true
        }
    },
    {
        text : 'Fase',
        dataIndex : 'faseNombre',
        name : 'comboFases',
        flex : 4,
        editor :
        {
            xtype : 'combobox',
            store : 'StoreFases',
            editable : false,
            displayField : 'nombre',
            valueField : 'id',
            allowBlank : false,
            emptyText : 'Selecciona una fase'
        }
    },
    {
        name : 'faseId',
        dataIndex : 'faseId',
        hidden : true
    } ]

});
