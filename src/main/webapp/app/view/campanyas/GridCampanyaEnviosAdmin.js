Ext.define('CRM.view.campanyas.GridCampanyaEnviosAdmin',
{
    extend : 'Ext.grid.Panel',
    store : 'StoreCampanyaEnviosAdmin',
    alias : 'widget.gridCampanyaEnviosAdmin',
    sortableColumns : true,
    selModel :
    {
        mode : 'SINGLE'
    },
    tbar : [
    {
        xtype : 'button',
        name : 'anyadirCampanyaEnvioAdmin',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarCampanyaEnvioAdmin',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'id',
        hidden : true,
        dataIndex : 'id',
        menuDisabled : true

    },
    {
        text : 'Tipus',
        dataIndex : 'campanyaEnvioTipoNombre',
        menuDisabled : true,
//        name : 'envioTipo',
        flex : 1
    },
    {
        text : 'Asumpte',
        dataIndex : 'asunto',
        menuDisabled : true,
        flex : 1
    },
    {
        text : 'Corp',
        dataIndex : 'cuerpo',
        menuDisabled : true,
        flex : 1
    },
    {
        text : 'Mails',
        dataIndex : 'emails',
        menuDisabled : true,
        flex : 1
    } ]
});
