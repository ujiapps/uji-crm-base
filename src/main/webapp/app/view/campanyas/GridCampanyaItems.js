Ext.define('CRM.view.campanyas.GridCampanyaItems',
{
    extend : 'Ext.grid.Panel',
    store : 'StoreCampanyaItems',
    alias : 'widget.gridCampanyaItems',
    sortableColumns : true,

    selModel :
    {
        mode : 'SINGLE'
    },

    tbar : [
    {
        xtype : 'button',
        name : 'anyadir',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrar',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'id',
        hidden : true,
        dataIndex : 'id',
        menuDisabled : true
    },
    {
        text : 'Programa',
        dataIndex : 'programaNombre',
        menuDisabled : true,
        flex : 1
    },
    {
        text : 'Grupo',
        dataIndex : 'grupoNombre',
        menuDisabled : true,
        flex : 1
    },
    {
        text : 'Item',
        dataIndex : 'itemNombre',
        menuDisabled : true,
        flex : 4
    } ]
});
