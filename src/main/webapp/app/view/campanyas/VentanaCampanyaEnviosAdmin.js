Ext.define('CRM.view.campanyas.VentanaCampanyaEnviosAdmin',
    {
        extend: 'Ext.Window',
        title: 'Enviaments',
        width: 700,
        height: 470,
        alias: 'widget.ventanaCampanyaEnviosAdmin',
        modal: true,

        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        fbar: [
            {
                xtype: 'button',
                text: 'Afegir',
                action: 'anyadir',
                name: 'anyadir',
                hidden: true
            },
            {
                xtype: 'button',
                text: 'Actualitzar',
                action: 'actualizar',
                name: 'actualizar',
                hidden: true
            },
            {
                xtype: 'button',
                text: 'Cancel·lar',
                action: 'cancelar',
                name: 'cancelar'
            }],

        items: [
            {
                xtype: 'combobox',
                editable: false,
                padding: 5,
                fieldLabel: 'Tipus',
                store: 'StoreTiposEstadoCampanya',
                displayField: 'nombre',
                valueField: 'id',
                name: 'tipoEnvioAuto',
                queryMode: 'local'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Asumpte',
                name: 'asuntoEnvioAuto',
                padding: 5
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Emails',
                name: 'emails',
                padding: 5
            },
            {
                xtype: 'htmleditor',
                fieldLabel: 'Corp',
                name: 'cuerpoEnvioAuto',
                autoScroll: true,
                height: 250,
                padding: 5
            }]
    });