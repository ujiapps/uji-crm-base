Ext.define('CRM.view.campanyas.VentanaCampanyaEnvios',
    {
        extend: 'Ext.Window',
        title: 'Enviaments',
        width: 700,
        alias: 'widget.ventanaCampanyaEnvios',
        modal: false,
        border: false,
        fbar: [
            {
                xtype: 'button',
                text: 'Afegir',
                action: 'anyadir',
                name: 'anyadir',
                hidden: true
            },
            {
                xtype: 'button',
                text: 'Actualitzar',
                action: 'actualizar',
                name: 'actualizar',
                hidden: true
            },
            {
                xtype: 'button',
                text: 'Cancel·lar',
                action: 'cancelar',
                name: 'cancelar'
            }],

        items: [
            {
                xtype: 'form',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                frame : true,
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        editable: false,
                        padding: 5,
                        fieldLabel: 'Tipus',
                        store: 'StoreTiposEstadoCampanya',
                        displayField: 'nombre',
                        valueField: 'id',
                        queryMode: 'local',
                        name: 'tipoEnvioAuto',
                        allowBlank: false
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Asumpte',
                        name: 'asuntoEnvioAuto',
                        padding: 5,
                        allowBlank: false
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Desde',
                        name: 'desde',
                        padding: 5
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Responder a',
                        name: 'responder',
                        padding: 5
                    },
                    {
                        xtype: 'combobox',
                        fieldLabel: 'Enviar a',
                        emptyText: 'Trieu....',
                        valueField: 'id',
                        displayField: 'nombre',
                        name: 'tipoCorreoEnvio',
                        editable: false,
                        queryMode: 'local',
                        store: 'StoreTiposCorreo',
                        allowBlank: false,
                        padding: 5
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Texto Base',
                        name: 'base',
                        allowBlank: true,
                        padding: 5
                    },
                    {
                        xtype: 'container',
                        layout: 'fit',
                        name: 'editor',
                        padding: 5,
                        flex: 1
                    },
                    {
                        xtype: 'gridEnviosCampanyaProgramados',
                        title: 'Programació del enviament',
                        padding: 5,
                        flex: 2
                    }
                ]
            }
        ]
    });