function formatDate(value)
{
    return value ? Ext.Date.dateFormat(value, 'd/m/Y') : '';
}

Ext.define('CRM.view.campanyas.GridCampanyaActos',
    {
        extend: 'Ext.grid.Panel',
        store: 'StoreCampanyaActos',
        alias: 'widget.gridCampanyaActos',
        sortableColumns: true,
        selModel: {
            mode: 'SINGLE'
        },
        tbar: [
            {
                xtype: 'button',
                name: 'anyadir',
                text: 'Afegir',
                iconCls: 'application-add'
            },
            {
                xtype: 'button',
                name: 'borrar',
                text: 'Esborrar',
                iconCls: 'application-delete',
                disabled: true
            }],

        columns: [
            {
                text: 'id',
                hidden: true,
                dataIndex: 'id',
                menuDisabled: true
            },
            {
                text: 'Imatge',
                dataIndex: 'imagenNombre',
                flex: 1,
                menuDisabled: true
            },
            {
                text: 'Titulo',
                dataIndex: 'titulo',
                menuDisabled: true,
                flex: 1
            },
            {
                text: 'Resumen',
                dataIndex: 'resumen',
                menuDisabled: true,
                flex: 1
            },
            {
                text: 'Duracion',
                dataIndex: 'duracion',
                menuDisabled: true,
                flex: 1
            },
            {
                text: 'Contingut',
                dataIndex: 'contenido',
                menuDisabled: true,
                flex: 1
            },
            {
                text: 'Fecha',
                dataIndex: 'fecha',
                menuDisabled: true,
                format: 'd/m/Y',
                renderer : formatDate,
                flex: 1
            },
            {
                text: 'Hora Evento',
                dataIndex: 'hora',
                menuDisabled: true,
                flex: 1
            },
            {
                text: 'Hora Apertura',
                dataIndex: 'horaApertura',
                menuDisabled: true,
                flex: 1
            },
            {
                text: 'Rss Activo',
                dataIndex: 'rssActivo',
                menuDisabled: true,
                xtype: 'booleancolumn',
                trueText: 'Sí',
                falseText: 'No',
                flex: 0.5
            }]
    });
