Ext.define('CRM.view.campanyas.estados.GridCampanyaEstados',
    {
        extend: 'Ext.grid.Panel',
        store: 'StoreTiposEstadoCampanya',
        requires : [ 'Ext.grid.plugin.RowEditing' ],
        alias: 'widget.gridCampanyaEstados',
        sortableColumns: true,
        selModel: {
            mode: 'SINGLE'
        },
        tbar: [
            {
                xtype: 'button',
                name: 'anyadir',
                text: 'Afegir',
                iconCls: 'application-add'
            },
            {
                xtype: 'button',
                name: 'borrar',
                text: 'Esborrar',
                iconCls: 'application-delete',
                disabled: true
            }],
        plugins: [
            {
                ptype: 'rowediting',
                clicksToEdit: 2,
                pluginId: 'editingCampanyaEstado'
            }],

        columns: [
            {
                text: 'id',
                hidden: true,
                dataIndex: 'id',
                menuDisabled: true
            },
            {
                text: 'Nombre',
                dataIndex: 'nombre',
                flex: 1,
                menuDisabled: true,
                editor: {
                    allowBlank: false
                }
            },
            {
                text: 'Accion',
                dataIndex: 'accion',
                menuDisabled: true,
                flex: 1,
                editor: {
                    allowBlank: true
                }
            }]
    });
