Ext.define('CRM.view.campanyas.estados.PanelCampanyaEstados',
    {
        extend: 'Ext.panel.Panel',
        title: 'Campanyes Estats',
        requires: ['CRM.view.campanyas.estados.GridCampanyaEstados', 'CRM.view.campanyas.estados.GridCampanyaEstadoMotivos'],
        alias: 'widget.panelCampanyaEstados',
        closable: true,

        layout: {
            type: 'hbox',
            align: 'stretch'
        },

        items: [
            {
                xtype: 'gridCampanyaEstados',
                flex: 3
            },
            {
                xtype: 'gridCampanyaEstadoMotivos',
                flex: 2
            }]

    });