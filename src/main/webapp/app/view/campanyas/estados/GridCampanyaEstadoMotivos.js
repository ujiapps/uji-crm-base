Ext.define('CRM.view.campanyas.estados.GridCampanyaEstadoMotivos',
    {
        extend: 'Ext.grid.Panel',
        store: 'StoreTiposEstadoCampanyaMotivos',
        requires: ['Ext.grid.plugin.RowEditing'],
        alias: 'widget.gridCampanyaEstadoMotivos',
        sortableColumns: true,
        selModel: {
            mode: 'SINGLE'
        },
        tbar: [
            {
                xtype: 'button',
                name: 'anyadir',
                text: 'Afegir',
                iconCls: 'application-add'
            },
            {
                xtype: 'button',
                name: 'borrar',
                text: 'Esborrar',
                iconCls: 'application-delete',
                disabled: true
            }],
        plugins: [
            {
                ptype: 'rowediting',
                clicksToEdit: 2,
                pluginId: 'editingCampanyaEstadoMotivo'
            }],

        columns: [
            {
                text: 'id',
                hidden: true,
                dataIndex: 'id',
                menuDisabled: true
            },
            {
                text: 'Nombre',
                dataIndex: 'nombreEs',
                flex: 1,
                menuDisabled: true,
                editor: {
                    allowBlank: false
                }
            },
            {
                text: 'Nom',
                dataIndex: 'nombreCa',
                flex: 1,
                menuDisabled: true,
                editor: {
                    allowBlank: false
                }
            },
            {
                text: 'Name',
                dataIndex: 'nombreUk',
                flex: 1,
                menuDisabled: true,
                editor: {
                    allowBlank: false
                }
            }]
    });
