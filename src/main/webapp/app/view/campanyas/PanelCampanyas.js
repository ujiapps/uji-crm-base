Ext.define('CRM.view.campanyas.PanelCampanyas', {
    extend: 'Ext.panel.Panel',
    title: 'Campanyes',
    requires: ['CRM.view.campanyas.TabCampanyas'],
    alias: 'widget.panelCampanyas',
    closable: true,
    layout: 'border',
    name : 'panelCampanyas',

    items: [{
        xtype: 'treepanel',
        name : 'gridCampanyas',
        region: 'north',
        flex: 3,
        collapsible: true,
        split: true,
        useArrows: true,
        rootVisible: false,
        selModel: {
            mode: 'SINGLE'
        },
        tbar: [{
            xtype: 'button',
            name: 'anyadirCampanya',
            text: 'Afegir',
            iconCls: 'application-add'
        }, {
            xtype: 'button',
            name: 'borrarCampanya',
            text: 'Esborrar',
            iconCls: 'application-delete',
            disabled: true
        }, {
            xtype: 'button',
            name: 'anyadirCampanyaActos',
            text: 'Afegir nova campanya Actes de graduació',
            iconCls: 'application-add'
        }, {
            xtype: 'button',
            name: 'anyadirCampanyaActosConPagos',
            text: 'Afegir nova campanya Actes de graduació amb pagament d\'assistència',
            iconCls: 'application-add'
        }, '->', {
            xtype: 'button',
            name: 'refrescarGridTree',
            iconCls: 'x-tbar-loading',
            listeners: {
                click: function () {
                    var panel = this.up("treepanel[name=gridCampanyas]");
                    panel.up("panel[name=panelCampanyas]").down("tabpanel[name=tabCampanyas]").setDisabled(true);
                    panel.setLoading(true);
                    panel.getStore().load({
                        callback: function(){
                            panel.setLoading(false);
                        }
                    });
                },
            }
        }],
        store: Ext.create('CRM.store.StoreCampanyasAdminTree'),
        columns: [{
            xtype: 'treecolumn',
            text: 'Campanya',
            flex: 2.5,
            sortable: true,
            dataIndex: 'text'
        }, {
            text: 'Programa',
            flex: 1,
            dataIndex: 'programa',
            sortable: true
        }, {
            text: 'Descripció',
            flex: 2,
            dataIndex: 'descripcion'
        }]
    }, {
        xtype: 'tabCampanyas',
        name : 'tabCampanyas',
        region: 'center',
        flex: 2,
        disabled: true
    }]

});