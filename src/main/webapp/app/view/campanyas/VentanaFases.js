Ext.define('CRM.view.campanyas.VentanaFases',
{
    extend : 'Ext.Window',
    title : 'Administració de fases',
    width : 500,
    alias : 'widget.ventanaFases',
    modal : true,

    items : [
    {
        xtype : 'grid',
        name : 'gridFases',
        frame : true,
        padding : 10,
        layout :
        {
            type : 'vbox',
            align : 'stretch'
        },
        lbar : [
        {
            xtype : 'button',
            text : 'afegir',
            name : 'anyadirFase'
        },
        {
            xtype : 'button',
            text : 'Eliminar',
            name : 'eliminarFase'
        },
        {
            xtype : 'button',
            text : 'Cancel·lar',
            action : 'cancelar'
        } ],
        columns : [
        {

            name : 'id',
            valueField : 'id',
            hidden : true
        },

        {
            name : 'nombre',
            text : 'Nom',
            allowBlank : false,
            flex : 1
        },

        {
            xtype : 'textfield',
            name : 'activa',
            text : 'Activa',
            allowBlank : false,
            flex : 1
        } ]
    } ]

});