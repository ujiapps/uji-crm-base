Ext.define('CRM.view.campanyas.VentanaCampanyaItems',
{
    extend : 'Ext.Window',
    title : 'Associació de Campanyes i ítems',
    width : 700,
    height : 400,
    alias : 'widget.ventanaCampanyaItems',
    requires : [ 'CRM.view.commons.GrouppedComboBox' ],
    modal : true,

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    fbar : [
    {
        xtype : 'button',
        text : 'Afegir',
        action : 'anyadir'
    },
    {
        xtype : 'button',
        text : 'Cancel·lar',
        action : 'cancelar'
    } ],

    items : [
    {
        xtype : 'container',
        layout : 'hbox',
        padding : 10,
        height: 50,

        items : [
        {
            border : 0,
            xtype : 'grouppedComboBox',
            editable : false,
            labelWidth : 60,
            labelAlign : 'left',
            margin : '3 20 0 0',
            fieldLabel : 'Grupo',
            emptyText : 'Trieu...',
            store : 'StoreGrupos',
            displayField : 'nombreCa',
            groupField : "programaId",
            groupDisplayField : "programaNombre",
            valueField : 'id',
            name : 'filtroGrupos',
            width : 500
        },
        {
            xtype : 'button',
            text : 'Netejar Filtres',
            name : 'limpiarFiltros'
        } ]
    },
    {
        xtype : 'campanyas.gridItems',
        flex : 1,
        name : 'gridItems'
    } ]
});