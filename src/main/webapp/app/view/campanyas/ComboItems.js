Ext.define('CRM.view.campanyas.ComboItems',
{
    extend : 'Ext.form.field.ComboBox',
    alias : 'widget.comboItems',

    constructor : function(args)
    {
        var me = this;
        var groupField = args.groupField || "grupoNombre";
        var groupDisplayField = args.groupDisplayField || groupField;
        var displayField = args.displayField || "nombreCa";

        args.tpl = new Ext.XTemplate('<tpl for=".">', '<tpl if="this.' + groupField + ' != values.' + groupField + '">', '<tpl exec="this.' + groupField + ' = values.' + groupField + '"></tpl>',
                '<div class="x-panel-header-default x-panel-header-text-container x-panel-header-text x-panel-header-text-default" title="{' + groupDisplayField + '}">{' + groupDisplayField
                        + '}</div>', '</tpl>', '<div class="x-boundlist-item">{' + displayField + '}</div>', '</tpl>');

        me.callParent(arguments);
    }
});