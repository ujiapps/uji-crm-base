Ext.define('CRM.view.campanyas.VentanaCampanya', {
    extend: 'Ext.Window',
    title: 'Campanyas',
    width: 700,
    alias: 'widget.ventanaCampanya',
    modal: true,
    fbar: [{
        xtype: 'button',
        text: 'Afegir',
        name: 'anyadirCampanya'
    },
        {
            xtype: 'button',
            text: 'Actualitzar',
            name: 'actualizarCampanya',
            hidden: true
        },
        {
            xtype: 'button',
            text: 'Cancel·lar',
            name: 'cancelarCampanya',
            listeners: {
                click: function () {
                    this.up('window').cerrarVentanaCampanya();
                }
            }
        }],

    items: [{
        xtype: 'form',
        name: 'formCampanya',
        modal: true,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        defaults: {
            labelWidth: 150
        },
        items: [{
            name: 'id',
            hidden: true,
            xtype: 'textfield'
        }, {
            fieldLabel: 'Nom',
            name: 'nombre',
            padding: 5,
            xtype: 'textfield',
            allowBlank: false
        }, {
            fieldLabel: 'Descripció',
            allowBlank: true,
            name: 'descripcion',
            padding: 5,
            xtype: 'textfield'
        }, {
            fieldLabel: 'Programa',
            name: 'programaId',
            allowBlank: false,
            padding: 5,
            xtype: 'combobox',
            store: 'StoreProgramas',
            editable: false,
            displayField: 'nombreCa',
            valueField: 'id',
            emptyText: 'Selecciona un Programa'
        }, {
            fieldLabel: 'Campanya',
            name: 'campanyaId',
            xtype: 'combobox',
            allowBlank: true,
            store: 'StoreCampanyas',
            editable: true,
            displayField: 'nombre',
            valueField: 'id',
            queryMode: 'local',
            emptyText: 'Selecciona una campanya',
            // forceSelection: true,
            padding: 5
        }, {
            fieldLabel: 'Vincle assoc.',
            name: 'subVinculo',
            flex: 1,
            xtype: 'combobox',
            store: 'StoreSubVinculos',
            editable: true,
            displayField: 'subVinculoNombre',
            valueField: 'subVinculoId',
            queryMode: 'local',
            allowBlank: true,
            padding: 5
        }, {
            fieldLabel: 'Fideliza',
            name: 'fideliza',
            flex: 1,
            xtype: 'checkbox',
            padding: 5
        }, {
            fieldLabel: 'Llinea de facturació',
            name: 'lineaFacturacionId',
            flex: 1,
            xtype: 'combobox',
            store: 'StoreLineasFacturacionByCampanya',
            editable: false,
            displayField: 'nombre',
            valueField: 'id',
            queryMode: 'local',
            padding: 5
        }, {
            fieldLabel: 'Correu a on avisar dels cobraments efectuats',
            name: 'correoAvisaCobro',
            padding: 5,
            xtype: 'textfield'
        }, {
            fieldLabel: 'Texte LOPD',
            name: 'campanyaTextoId',
            flex: 1,
            xtype: 'combobox',
            store: 'StoreCampanyaTextos',
            editable: false,
            displayField: 'nombre',
            valueField: 'id',
            queryMode: 'local',
            allowBlank: false,
            padding: 5
        }, {
            fieldLabel: 'Data inici activa',
            allowBlank: true,
            name: 'fechaInicioActiva',
            padding: 5,
            xtype: 'datefield',
            format: 'd/m/Y',
            submitFormat: 'd/m/Y'
        }, {
            fieldLabel: 'Data fi activa',
            allowBlank: true,
            name: 'fechaFinActiva',
            padding: 5,
            xtype: 'datefield',
            format: 'd/m/Y',
            submitFormat: 'd/m/Y'
        }]
    }],

    cargaVentanaCampanya: function (record) {
        if (record == null) {
            this.down("button[name=actualizarCampanya]").setVisible(false);
            this.down("button[name=anyadirCampanya]").setVisible(true);
        } else {
            var form = this.down("form[name=formCampanya]");
            Ext.Ajax.request({
                url: '/crm/rest/campanya/' + record.data.id,
                success: function (res) {
                    var c = Ext.decode(res.responseText).data;
                    var campanya = Ext.create('CRM.model.Campanya', {
                        id : c.id,
                        descripcion : c.descripcion,
                        fechaFinActiva : c.fechaFinActiva,
                        fideliza : c.fideliza,
                        subVinculo : c.subVinculo,
                        programaId : c.programaId,
                        campanyaId : c.campanyaId,
                        campanyaTextoId : c.campanyaTextoId,
                        correoAvisaCobro : c.correoAvisaCobro,
                        nombre : c.nombre,
                        fechaInicioActiva : c.fechaInicioActiva,
                        lineaFacturacionId: c.lineaFacturacionId
                    });
                    form.loadRecord(campanya);
                }
            });

            this.down("button[name=actualizarCampanya]").setVisible(true);
            this.down("button[name=anyadirCampanya]").setVisible(false);
        }
    },

    dameDatosCampanya: function () {
        var form = this.down("form[name=formCampanya]").getForm();
        if (form.isValid()) {
            return this.down("form[name=formCampanya]").getValues(false, false, false, true);
        } else {
            Ext.Msg.alert('Campanya', 'Les dades no son correctes.');
        }
    },

    dameCampanyaId: function () {
        return this.down("form[name=formCampanya]").getForm().findField('id').getValue();
    },

    cerrarVentanaCampanya: function () {
        this.destroy();
    },

    renderSolicitudDato: function (val) {
        var data = ['No sol.licitar', 'Opcional', 'Obligatori No modificable', 'Obligatori Modificable'];
        return data[val];
    }
})
;
