Ext.require('Ext.tree.TreePanel');

Ext.define('CRM.view.ApplicationViewport',
    {
        extend: 'Ext.ux.uji.ApplicationViewport',
        alias: 'widget.applicationViewportCrm',

        buildLogoPanel: function () {
            this.add(
                {
                    region: 'north',
                    layout: 'border',
                    height: 70,
                    items: [
                        {
                            region: 'center',
                            style: 'border:0px',
                            html: '<div style="background: url(//e-ujier.uji.es/img/portal2/imagenes/cabecera_1px.png) repeat-x scroll left top transparent; height: 70px;">'
                                + '<img src="//e-ujier.uji.es/img/portal2/imagenes/logo_uji_horizontal.png" style="float: left;margin: 10px 16px;" />' + '<div style="float:left; margin-top:11px;">'
                                + '<span style="color: rgb(255,255, 255); font-family: Helvetica,Arial,sans-serif;font-size:1.2em;">E-UJIER@</span><br/>'
                                + '<span style="color: #CDCCE5; font-family: Helvetica,Arial,sans-serif;">' + this.tituloAplicacion + '</span></div></div>'
                        }]
                });
        },

        buildNavigationTree: function () {
            var navigationTree = new Ext.tree.TreePanel(
                {
                    title: 'Conectat com ' + login + '@',
                    region: 'west',
                    alias: 'widget.navigationtree',
                    lines: false,
                    width: this.treeWidth,
                    split: true,
                    collapsible: true,
                    autoScroll: true,
                    rootVisible: false,
                    bodyStyle: 'padding-bottom:20px;',

                    store: Ext.create('Ext.data.TreeStore',
                        {
                            expanded: true,
                            autoLoad: true,
                            root: {
                                children: [{
                                    "id": "CRM.view.clientes.PanelClientes",
                                    "title": "Clients",
                                    "text": "Clients",
                                    "leaf": true,
                                    "checked": null,
                                    "route": "clientes",
                                    "itemId": 131,
                                    "selectable": null,
                                    "iconCls": null,
                                    "row": []
                                }, {
                                    "id": "CRM.view.seguimientos.PanelSeguimientos",
                                    "title": "Seguiment de Campanyes",
                                    "text": "Seguiment de Campanyes",
                                    "leaf": true,
                                    "checked": null,
                                    "route": "seguimientos",
                                    "itemId": 136,
                                    "selectable": null,
                                    "iconCls": null,
                                    "row": []
                                }, {
                                    "id": "CRM.view.envios.PanelEnvios",
                                    "title": "Envíos",
                                    "text": "Envíos",
                                    "leaf": true,
                                    "checked": null,
                                    "route": "envios",
                                    "itemId": 134,
                                    "selectable": null,
                                    "iconCls": null,
                                    "row": []
                                }, {
                                    "id": "CRM.view.listados.PanelListados",
                                    "title": "Llistats predefinits",
                                    "text": "Llistats predefinits",
                                    "leaf": true,
                                    "checked": null,
                                    "route": "listados",
                                    "itemId": 139,
                                    "selectable": null,
                                    "iconCls": null,
                                    "row": []
                                }, {
                                    "id": "CRM.view.servicios.PanelServicios",
                                    "title": "Gestió Serveis",
                                    "text": "Gestió Serveis",
                                    "leaf": true,
                                    "checked": null,
                                    "route": "servicios",
                                    "itemId": 132,
                                    "selectable": null,
                                    "iconCls": null,
                                    "row": []
                                }, {
                                    "id": "CRM.view.programas.PanelProgramas",
                                    "title": "Gestió de Programes",
                                    "text": "Gestió de Programes",
                                    "leaf": true,
                                    "checked": null,
                                    "route": "programas",
                                    "itemId": 133,
                                    "selectable": null,
                                    "iconCls": null,
                                    "row": []
                                }, {
                                    "id": "CRM.view.campanyas.PanelCampanyas",
                                    "title": "Gestió de Campanyes",
                                    "text": "Gestió de Campanyes",
                                    "leaf": true,
                                    "checked": null,
                                    "route": "campañas",
                                    "itemId": 135,
                                    "selectable": null,
                                    "iconCls": null,
                                    "row": []
                                }, {
                                    "id": "CRM.view.gestionEnvios.PanelGestionEnvios",
                                    "title": "Gestió de enviaments",
                                    "text": "Gestió de enviaments",
                                    "leaf": true,
                                    "checked": null,
                                    "route": "gestion-envios",
                                    "itemId": 405627405,
                                    "selectable": null,
                                    "iconCls": null,
                                    "row": []
                                }, {
                                    "id": "CRM.view.pagos.TabPanelPagos",
                                    "title": "Gestió de Rebuts",
                                    "text": "Gestió de Rebuts",
                                    "leaf": true,
                                    "checked": null,
                                    "route": "recibos",
                                    "itemId": 138,
                                    "selectable": null,
                                    "iconCls": null,
                                    "row": []
                                }, {
                                    "id": "CRM.view.estudios.PanelEstudios",
                                    "title": "Gestió estudis",
                                    "text": "Gestió estudis",
                                    "leaf": true,
                                    "checked": null,
                                    "route": "estudios",
                                    "itemId": 197,
                                    "selectable": null,
                                    "iconCls": null,
                                    "row": []
                                }, {
                                    "id": "CRM.view.grupos.PanelGrupos",
                                    "title": "Gestió de Suscriptors",
                                    "text": "Gestió de Suscriptors",
                                    "leaf": true,
                                    "checked": null,
                                    "route": "suscriptores",
                                    "itemId": 130,
                                    "selectable": null,
                                    "iconCls": null,
                                    "row": []
                                }, {
                                    "id": "CRM.view.auxiliares.PanelAuxiliares",
                                    "title": "Altres gestions",
                                    "text": "Altres gestions",
                                    "leaf": true,
                                    "checked": null,
                                    "route": "otras",
                                    "itemId": 137,
                                    "selectable": null,
                                    "iconCls": null,
                                    "row": []
                                }]
                            }
                            // proxy :
                            // {
                            //     type : 'ajax',
                            //     url : '/crm/rest/navigation/class?codigoAplicacion=CRM',
                            //     noCache : true,
                            //
                            //     reader :
                            //     {
                            //         type : 'json',
                            //         root : 'row'
                            //     }
                            // }
                        })

                });

            navigationTree.getRootNode().expand();
            this.add(navigationTree);
        }
    });