Ext.define('CRM.view.personas.GridPersonas',
{
    extend : 'Ext.grid.Panel',
    store : 'StoreServicioUsuario',
    alias : 'widget.gridPersonas',
    selModel :
    {
        mode : 'SINGLE'
    },
    sortableColumns : true,
    tbar : [
    {
        xtype : 'button',
        name : 'anyadirPersona',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarPersona',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'id',
        hidden : true,
        dataIndex : 'id',
        menuDisabled : true,
        flex : 1
    },
    {
        text : 'Persona',
        dataIndex : 'nombrePersona',
        menuDisabled : true,
        flex : 1
    } ]

});
