Ext.define('CRM.view.pagos.TabPanelPagos',
    {
        extend: 'Ext.tab.Panel',
        title: 'Tarifes',
        requires: ['CRM.view.pagos.tarifas.PanelTarifas', 'CRM.view.pagos.devoluciones.GridFicherosDevoluciones', 'CRM.view.pagos.recibos.PanelRecibos'],
        alias: 'widget.tabPanelPagos',
        layout:'fit',
        closable: true,
        items: [

            {
                xtype: 'panelRecibos',
                title: 'Recibos'
            },
            {
                xtype: 'form',
                name: 'formFicheroDevolucion',
                title: 'Devolucions',
                layout:'fit',
                items: [{
                    xtype: 'gridFicherosDevoluciones',
                    name: 'gridFicherosDevoluciones'
                }]
            },
            {
                xtype: 'panelTarifas',
                title: 'Tarifes'
            }]

    });