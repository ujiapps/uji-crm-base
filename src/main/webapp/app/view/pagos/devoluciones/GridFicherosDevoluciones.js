Ext.define('CRM.view.pagos.devoluciones.GridFicherosDevoluciones',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridFicherosDevoluciones',
        store: 'StoreFicherosDevoluciones',
        sortableColumns: true,
        selModel: {
            mode: 'SINGLE'
        },

        tbar: [

            {
                xtype: 'filefield',
                fieldLabel: 'Fitxer',
                labelWidth: 50,
                name: 'anyadirFicheroDevolucion',
                buttonConfig: {
                    text: 'Sel·lecciona...',
                    disabled: false
                },
                flex: 5
            },
            {
                xtype: 'button',
                name: 'buttonAnyadirFicheroDevolucion',
                iconCls: 'accept',
                disabled: true,
                flex: 0.5

            }],

        bbar: [{
            xtype: 'button',
            text: 'Recarregar',
            action: 'reload',
            reference: 'reload',
            handler: function () {
                this.up('gridFicherosDevoluciones').store.load();
            }

        }],
        columns: [
            {
                text: 'Nom',
                dataIndex: 'nombreFichero',
                flex: 1
            },
            {
                text: 'Data',
                dataIndex: 'fecha',
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                flex: 1
            }]

    });
