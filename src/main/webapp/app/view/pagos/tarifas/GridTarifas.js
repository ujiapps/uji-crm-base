Ext.define('CRM.view.pagos.tarifas.GridTarifas',
{
    extend : 'Ext.grid.Panel',
    //title : 'Tarifes',
    alias : 'widget.gridTarifas',
    store : 'StoreTarifas',
    requires : [ 'Ext.grid.plugin.RowEditing' ],
    forceFit : true,
    sortableColumns : true,

    selModel :
    {
        mode : 'SINGLE'
    },

    plugins : [
    {
        ptype : 'rowediting',
        clicksToEdit : 2,
        pluginId : 'editingTarifa'
    } ],

    tbar : [
    {
        xtype : 'button',
        name : 'anyadirTarifa',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarTarifa',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [

    {
        text : 'Data Inici',
        dataIndex : 'fechaInicio',
        renderer : Ext.util.Format.dateRenderer('d/m/Y'),
        flex : 1,
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            allowBlank : false
        }
    },
    {
        text : 'Data Fí',
        dataIndex : 'fechaFin',
        renderer : Ext.util.Format.dateRenderer('d/m/Y'),
        flex : 1,
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            allowBlank : false
        }
    },
    {
        text : 'Import',
        dataIndex : 'importe',
        flex : 1,
        editor :
        {
            allowBlank : false
        }
    } ]

});
