Ext.define('CRM.view.pagos.tarifas.PanelTarifaEnvios',
    {
        extend: 'Ext.tab.Panel',

        autoScroll: true,
        alias: 'widget.panelTarifaEnvios',
        requires: ['CRM.view.pagos.tarifas.GridTarifasEnviosImagenes'],
        modal: true,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        // defaults: {
        //     padding: 5
        // },

        items: [
            {
                // xtype: 'tabpanel',
                // title : 'Editor',
                // name: 'editorTarifasEnvios',
                // flex: 3,
                // items: [{
                title: 'Editor',
                name: 'editor',
                // xtype: 'panel',
                autoScroll: true,
                // flex: 1,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                fbar: [{
                    xtype: 'textfield',
                    name: 'correoPrueba',

                },
                    {
                        xtype: 'button',
                        text: 'Enviar prueba',
                        action: 'enviar',
                        name: 'enviar',
                        listeners: {
                            click: function () {

                                var cuerpo = this.up("tabpanel[name=panelTarifaEnvios]").down("[name=editor]").down("[name=cuerpo]").getValue(),
                                    correoPrueba = this.up("tabpanel[name=panelTarifaEnvios]").down("textfield[name=correoPrueba]").getValue(),
                                    asunto = this.up("[name=panelTarifas]").down("grid[name=gridTarifasEnvios]").getSelectionModel().getSelection()[0].data.asunto,
                                    responder = this.up("[name=panelTarifas]").down("grid[name=gridTarifasEnvios]").getSelectionModel().getSelection()[0].data.responderA;

                                Ext.Ajax.request({
                                    method: 'POST',
                                    url: '/crm/rest/envio/prueba',
                                    params: {
                                        cuerpo: cuerpo,
                                        correoPrueba: correoPrueba,
                                        asunto: asunto,
                                        responder: responder
                                    },
                                    success: function (response) {
                                        var resp = Ext.JSON.decode(response.responseText);
                                        if (resp.success) {
                                            Ext.Msg.alert('Envio', 'Envio realizado correctamente');
                                        } else {
                                            Ext.Msg.alert('Error envio', 'No s`ha pogut realitzar l\'enviament, comprueba que has puesto un correo donde enviarlo');
                                        }
                                    }
                                })
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Guardar',
                        action: 'guardar',
                        name: 'guardar',
                        listeners: {
                            click: function () {

                                var id = this.up("[name=panelTarifas]").down("grid[name=gridTarifasEnvios]").getSelectionModel().getSelection()[0].data.id,
                                    cuerpo = this.up("tabpanel[name=panelTarifaEnvios]").down("[name=editor]").down("[name=cuerpo]").getValue(),
                                    grid = this.up("[name=panelTarifas]").down("grid[name=gridTarifasEnvios]"),
                                    store = grid.getStore();

                                Ext.Ajax.request({
                                    method: 'PUT',
                                    url: '/crm/rest/tipotarifaenvio/' + id + '/cuerpo',
                                    params: {
                                        cuerpo: cuerpo
                                    },
                                    success: function () {
                                        store.load({
                                            callback: function () {
                                                grid.getSelectionModel().select(store.find('id', id))
                                            }
                                        });

                                    },
                                    failure: function () {
                                    }
                                })
                            }
                        }
                    }
                ]
            }, {
                xtype: 'form',
                name: 'formSubirImagenes',
                title: 'Imagenes',
                items: [{
                    xtype: 'gridTarifasEnviosImagenes',
                    name: 'imagenesEnviosTarifas',
                    // flex: 1,
                    listeners: {
                        select: function () {
                            this.down("button[name=botonBorrarImagen]").setDisabled(false);
                        }
                    }
                }]
                // }]
            }, {
                xtype: 'gridEnviosProgramados',
                title: 'Programació del enviament',
                // flex: 1,
                listeners: {
                    select: function () {
                        this.down("button[name=borrarEnvioProgramado]").setDisabled(false);
                    }
                }
            }]
    });