Ext.define('CRM.view.pagos.tarifas.PanelTarifas',
    {
        extend: 'Ext.panel.Panel',
        title: 'Tarifes',
        name: 'panelTarifas',
        requires: ['CRM.view.pagos.tarifas.GridTiposTarifas', 'CRM.view.pagos.tarifas.GridTarifas', 'CRM.view.pagos.tarifas.GridTarifasEnvios'],
        alias: 'widget.panelTarifas',
        defaults: {
            border: 0
        },

        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        items: [
            {
                xtype: 'gridTiposTarifas',
                name: 'gridTiposTarifas',
                // flex: 1,
                // collapsible : true,
                height : 150,
                resizable: true,
            },
            {
                xtype: 'tabpanel',
                flex: 4,
                disabled: true,
                autoScroll: true,
                items: [
                    {
                        xtype: 'gridTarifas',
                        title: 'Tarifas'
                    }, {
                        xtype: 'panel',
                        layout: 'border',
                        title: 'Enviaments',
                        border : 0,
                        items: [{
                            xtype: 'gridTarifasEnvios',
                            name: 'gridTarifasEnvios',
                            // collapsible : true,
                            height : 150,
                            resizable: true,
                            // flex: 1,
                            padding: 5,
                            region: 'north',
                            listeners: {
                                select: function (grid, elem) {
                                    if (elem.data.id !== null) {
                                        this.up("tabpanel").down("tabpanel[name=panelTarifaEnvios]").setActiveTab(0);
                                        this.down("button[name=borrarTarifaEnvio]").setDisabled(false);
                                        var editor = this.up("tabpanel").down("[name=panelTarifaEnvios]").down("[name=editor]");
                                        var cuerpo = editor.down("[name=cuerpo]");

                                        if (cuerpo != null) {
                                            cuerpo.destroy();
                                        }

                                        var config = {
                                            width: '100%',
                                            height: '100%',
                                            name: 'cuerpo',
                                            autoScroll: true
                                        };

                                        var editorNuevo = new Ext.ux.CKEditor(config);

                                        editorNuevo.setValue(elem.data.cuerpo);
                                        editor.add(editorNuevo);
                                        editorNuevo.setUrl('/envioimagen/visualizar/tarifaenvio/' + elem.data.id);

                                        this.up("tabpanel").down("[name=panelTarifaEnvios]").setDisabled(false);
                                        this.up("tabpanel").down("[name=imagenesEnviosTarifas]").envioId = elem.data.id;

                                        this.up("tabpanel").down("[name=imagenesEnviosTarifas]").getStore().getProxy().url = 'rest/envioimagen/tarifaenvio/' + elem.data.id;
                                        this.up("tabpanel").down("[name=imagenesEnviosTarifas]").getStore().load();
                                        // this.up("tabpanel").down("grid[name=gridTarifasEnvios]").getStore().getProxy().setExtraParam('tipoTarifaEnvioId', elem.data.id);

                                        this.up("tabpanel").down("grid[name=gridEnviosProgramados]").getStore().load({
                                                params: {
                                                    tipoTarifaEnvioId: elem.data.id
                                                }
                                            }
                                        );
                                    }
                                },

                                edit: function () {
                                    var store = this.getStore();

                                    store.sync({
                                        scope: this,
                                        callback: function (obj) {
                                            var resp = Ext.JSON.decode(obj.operations[0].response.responseText);
                                            this.getSelectionModel().deselectAll();
                                            this.getSelectionModel().select(store.find('id', resp.data.id));
                                        }
                                    });
                                }
                            }
                        }, {
                            xtype: 'panelTarifaEnvios',
                            name: 'panelTarifaEnvios',

                            padding: 5,
                            region: 'center',
                            disabled: true,
                            border : 0,
                            resizable: true
                            // flex: 4
                        }]
                    }]
            }]
    });