Ext.define('CRM.view.pagos.tarifas.GridTarifasEnvios',
    {
        extend: 'Ext.grid.Panel',
        //title: 'Enviaments',
        alias: 'widget.gridTarifasEnvios',
        store: 'StoreTarifasEnvios',

        requires: ['Ext.grid.plugin.RowEditing'],
        forceFit: true,
        sortableColumns: true,

        selModel: {
            mode: 'SINGLE'
        },

        plugins: [
            {
                ptype: 'rowediting',
                clicksToEdit: 2,
                pluginId: 'editingTarifaEnvio'
            }],

        tbar: [
            {
                xtype: 'button',
                name: 'anyadirTarifaEnvio',
                text: 'Afegir',
                iconCls: 'application-add',
                listeners: {
                    click: function () {
                        var store = this.up("grid[name=gridTarifasEnvios]").getStore(),
                            tarifaEnvio = store.model.create({
                                tipoTarifaId: this.up("panel[name=panelTarifas]").down("grid[name=gridTiposTarifas]").getSelectionModel().getSelection()[0].data.id
                            }),
                            rowEditor = this.up("grid[name=gridTarifasEnvios]").getPlugin("editingTarifaEnvio");

                        rowEditor.cancelEdit();
                        store.insert(0, tarifaEnvio);
                        rowEditor.startEdit(0, 0);
                    }
                }
            },
            {
                xtype: 'button',
                name: 'borrarTarifaEnvio',
                text: 'Esborrar',
                iconCls: 'application-delete',
                disabled: true,
                listeners: {
                    click: function () {
                        var ref = this;
                        Ext.Msg.confirm('Eliminació d\'enviaments', '<b> Esteu segur/a de voler esborrar l\'enviament sel·leccionat?</b>', function (btn) {
                                if (btn == 'yes') {
                                    var envio = ref.up("grid[name=gridTarifasEnvios]").getSelectionModel().getSelection();
                                    if (envio.length == 1) {
                                        var registro = envio[0];
                                        var store = ref.up("grid[name=gridTarifasEnvios]").getStore();
                                        store.remove(registro);
                                        store.sync({
                                            callback: function () {
                                                ref.up("tabpanel").down("tabpanel[name=editorTarifasEnvios]").setActiveTab(0);
                                                ref.up("tabpanel").down("form[name=panelTarifaEnvios]").setDisabled(true);
                                            }
                                        });
                                    }
                                }
                            }
                        )
                        ;
                    }
                }
            }],

        columns: [

            {
                header: 'Asumpte',
                dataIndex: 'asunto',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            },
            {
                header: 'Desde',
                dataIndex: 'desde',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            },
            {
                header: 'responder a',
                dataIndex: 'responderA',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            },
            {
                header: 'Enviar a',
                xtype: 'combocolumn',
                dataIndex: 'tipoCorreoEnvio',
                combo: {
                    xtype: 'combobox',
                    editable: false,
                    store: Ext.create('CRM.store.StoreTiposCorreo'),
                    displayField: 'nombre',
                    valueField: 'id',
                    name: 'tipoCorreoEnvio',
                    queryMode: 'local',
                    allowBlank: false
                },
                flex: 1
            }]

    });
