Ext.define('CRM.view.pagos.tarifas.GridTarifasEnviosImagenes',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridTarifasEnviosImagenes',
        store: Ext.create('CRM.store.StoreEnviosImagenes', {id: 'StoreTarifasEnviosImagenes'}),
        sortableColumns: true,
        autoScroll: true,
        hideHeaders: true,
        envioId: '',
        border: 0,
        defaults: {
            border: 0
        },
        selModel: {
            mode: 'SINGLE'
        },

        tbar: [

            {
                xtype: 'filefield',
                fieldLabel: 'Imagen',
                labelWidth: 50,
                name: 'anyadirImagen',
                buttonConfig: {
                    text: 'Sel·lecciona...',
                    disabled: false
                },

                listeners: {
                    change: function () {
                        this.up("grid[name=imagenesEnviosTarifas]").down("button[name=botonAnyadirImagen]").setDisabled(false);
                        this.up("grid[name=imagenesEnviosTarifas]").down("button[name=botonBorrarImagen]").setDisabled(true);
                    }
                },
                flex: 5,
                regex: /(.)+((\.png)|(\.jpg)|(\.jpeg)(\w)?)$/i,
                regexText: 'Only PNG and JPEG image formats are accepted'
            },
            {
                xtype: 'button',
                name: 'botonAnyadirImagen',
                iconCls: 'accept',
                disabled: true,
                // flex: 0.5,
                listeners: {
                    click: function () {
                        var envioId = this.up("grid[name=imagenesEnviosTarifas]").envioId,
                            formImagenes = this.up("form[name=formSubirImagenes]");

                        if (formImagenes.getForm().isValid()) {
                            formImagenes.submit(
                                {
                                    url: '/crm/rest/envioimagen/tarifaenvio/' + envioId,
                                    scope: this,
                                    success: function () {
                                        this.up("grid[name=imagenesEnviosTarifas]").down("button[name=botonAnyadirImagen]").setDisabled(true);
                                        this.up("grid[name=imagenesEnviosTarifas]").down("button[name=botonBorrarImagen]").setDisabled(true);
                                        this.up("grid[name=imagenesEnviosTarifas]").getStore().load();
                                    },
                                    failure: function (x, resp) {
                                        Ext.Msg.alert('Insercció de imatges', '<b>' + resp.result.message + '</b>');
                                    }
                                });
                        } else {
                            Ext.Msg.alert('Insercció de imatges', '<b>El fitxer no es una imatge</b>');
                        }
                    }
                }
            },
            {
                xtype: 'button',
                name: 'botonBorrarImagen',
                iconCls: 'application-delete',
                disabled: true,
                listeners: {
                    click: function () {
                        var grid = this.up("grid[name=imagenesEnviosTarifas]"),
                            selection = grid.getSelectionModel().getSelection(),
                            botonBorrar = grid.down("button[name=botonBorrarImagen]");

                        Ext.Msg.confirm('Eliminar', 'El registre s\'eliminarà. Estàs segur de voler continuar?', function (btn) {
                            if (btn == 'yes') {
                                grid.setLoading(true);
                                Ext.Ajax.request({
                                    method: 'DELETE',
                                    url: '/crm/rest/envioimagen/' + selection[0].data.id,
                                    success: function () {
                                        botonBorrar.setDisabled(true);
                                        grid.getStore().load({
                                            callback: function () {
                                                grid.setLoading(false);
                                            }
                                        });

                                    },
                                    failure: function () {
                                        Ext.Msg.alert('Esborrat de fitxers', '<b>El fitxer no ha pogut ser borrat correctament</b>');
                                        grid.setLoading(false);
                                    }
                                })
                            }
                        });
                    }
                }
            }],

        columns: [
            {
                text: 'id',
                dataIndex: 'id',
                hidden: true
            },
            {
                dataIndex: 'nombre',
                flex: 2
            }]

    });
