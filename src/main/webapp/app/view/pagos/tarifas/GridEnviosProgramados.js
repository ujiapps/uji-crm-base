Ext.define('CRM.view.pagos.tarifas.GridEnviosProgramados',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridEnviosProgramados',
        store: 'StoreTarifasEnviosProgramados',
        requires: ['Ext.grid.plugin.RowEditing'],
        forceFit: true,
        sortableColumns: true,
        name: 'gridEnviosProgramados',

        selModel: {
            mode: 'SINGLE'
        },

        plugins: [
            {
                ptype: 'rowediting',
                clicksToEdit: 2,
                pluginId: 'editingEnvioProgramado'
            }],

        tbar: [
            {
                xtype: 'button',
                name: 'anyadirEnvioProgramado',
                text: 'Afegir',
                iconCls: 'application-add'
            },
            {
                xtype: 'button',
                name: 'borrarEnvioProgramado',
                text: 'Esborrar',
                iconCls: 'application-delete',
                disabled: true
            }],

        columns: [

            {
                header: 'Data Referencia',
                xtype: 'combocolumn',
                dataIndex: 'tipoFecha',
                flex: 1,
                combo: {
                    xtype: 'combobox',
                    store: Ext.create('CRM.store.StoreTiposFechaProgramacionEnvio'),
                    displayField: 'nombre',
                    valueField: 'id',
                    editable: false
                }
            },
            {
                text: 'Díes',
                dataIndex: 'dias',
                flex: 1,
                editor: {
                    xtype: 'textfield',
                    allowBlank: false
                }
            },
            {
                text: 'Cuando Realizar el envio',
                dataIndex: 'cuandoRealizarEnvio',
                xtype: 'combocolumn',
                flex: 1,
                combo: {
                    xtype: 'combobox',
                    store: Ext.create('CRM.store.StoreTiposAccionProgramacionEnvio'),
                    displayField: 'nombre',
                    valueField: 'id',
                    editable: false
                }
            }]

    });
