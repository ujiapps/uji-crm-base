Ext.define('CRM.view.pagos.tarifas.GridTiposTarifas', {
    extend: 'Ext.grid.Panel',
    // title: 'Tipus Tarifes',
    alias: 'widget.gridTiposTarifas',
    requires: ['CRM.store.StoreTiposTarifas', 'Ext.grid.plugin.RowEditing'],
    store: Ext.create('CRM.store.StoreTiposTarifas'),
    forceFit: true,
    sortableColumns: true,

    selModel: {
        mode: 'SINGLE'
    },

    plugins: [{
        ptype: 'rowediting',
        clicksToEdit: 2,
        pluginId: 'editingTipoTarifa'
    }],

    tbar: [{
        xtype: 'button',
        name: 'anyadirTipoTarifa',
        text: 'Afegir',
        iconCls: 'application-add',
        disabled: true

    }, {
        xtype: 'button',
        name: 'borrarTipoTarifa',
        text: 'Esborrar',
        iconCls: 'application-delete',
        disabled: true
    }, '|', {
        fieldLabel: 'Seleccionar campanya',
        labelWidth: 150,
        width: '80%',
        xtype: 'combobox',
        store: 'StoreCampanyasTarifa',
        editable: false,
        displayField: 'nombre',
        valueField: 'id',
        name: 'comboCampanyasTarifas',
        allowBlank: false,
        listeners: {
            select: function (elem, selec) {

                    this.up("grid").getStore().load({
                        url: 'rest/tipotarifa/campanya/' + selec[0].data.id
                    });
                    this.up("grid").down("[name=anyadirTipoTarifa]").setDisabled(false);
                    this.up("grid").down("[name=comboLineasFacturacion]").combo.store.load({
                        url: 'rest/lineafacturacion/campanya',
                        params: {
                            campanyaId: selec[0].data.id
                        }
                    });
                    this.up("grid").up("panel").down("[name=panelTarifaEnvios]").setDisabled(true);
            }
        }
    }],

    columns: [{
        text: 'Nom',
        dataIndex: 'nombre',
        flex: 2,
        editor: {
            allowBlank: false
        }
    // }, {
    //     text: 'Dias caducidad',
    //     dataIndex: 'diasCaducidad',
    //     flex: 1,
    //     editor: {
    //         allowBlank: false
    //     }
    // }, {
    //     text: 'Dias Vigencia',
    //     dataIndex: 'diasVigencia',
    //     flex: 1,
    //     editor: {
    //         allowBlank: false
    //     }
    }, {
        text: 'Meses Vigencia',
        dataIndex: 'mesesVigencia',
        flex: 1,
        editor: {
            allowBlank: false
        }
    }, {
        text: 'Meses Caducidad',
        dataIndex: 'mesesCaduca',
        flex: 1,
        editor: {
            allowBlank: false
        }
    },{
        text: 'Campanya',
        dataIndex: 'campanyaNombre',
        name: 'comboCampanyas',
        // xtype : 'combocolumn',
        flex: 1,
        editor: {
            xtype: 'combobox',
            store: 'StoreCampanyas',
            editable: false,
            displayField: 'nombre',
            valueField: 'id',
            allowBlank: false
        }
    }, {
        text: 'Per Defecte',
        dataIndex: 'defecto',
        xtype: 'booleancolumn',
        trueText: 'Sí',
        falseText: 'No',
        flex: 1,
        editor: {
            xtype: 'checkbox'
        }

    }, {
        header: 'Llinea Facturació',
        dataIndex: 'lineaFacturacionId',
        name: 'comboLineasFacturacion',
        xtype: 'combocolumn',
        flex: 1,
        combo: {
            xtype: 'combobox',
            store: Ext.create('Ext.data.Store', {
                model: 'CRM.model.LineaFacturacion',
                autoLoad: false,
                autoSync: false,
                proxy: {
                    type: 'rest',
                    url: '/crm/rest/lineafacturacion',
                    reader: {
                        type: 'json',
                        successProperty: 'success',
                        root: 'data'
                    }
                }

            }),
            editable: false,
            displayField: 'nombre',
            valueField: 'id',
            allowBlank: true,
            queryMode: 'local'
        }
    }]
})
;
