function dame_exportado(value) {
    if (value == 0) return 'No';
    if (value == 1) return 'Si';
    if (value == 2) return 'A medias';
}

Ext.define('CRM.view.pagos.recibos.GridRecibos',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridRecibos',
        store: 'StoreRecibos',
        forceFit: true,
        sortableColumns: true,

        selModel: {
            mode: 'SINGLE'
        },
        dockedItems: [
            {
                xtype: 'pagingtoolbar',
                store: 'StoreRecibos',
                dock: 'bottom',
                displayInfo: true
            }],

        columns: [
            {
                text: 'Num',
                dataIndex: 'id',
                //flex: 0.5,
                hidden: true
            },
            {
                text: 'Id',
                dataIndex: 'codificacion',
                flex: 0.5
            },
            {
                text: 'Data creació',
                dataIndex: 'fechaCreacion',
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                flex: 1
            },
            {
                text: 'Data Pagament',
                dataIndex: 'fechaPago',
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                flex: 1
            },
            {
                text: 'Data Limit Pagament',
                dataIndex: 'fechaPagoLimite',
                renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                flex: 1
            },
            {
                text: 'Import',
                dataIndex: 'importeNeto',
                flex: 1
            },
            {
                text: 'Tipus Cobrament',
                dataIndex: 'tipoCobroNombre',
                flex: 1
            },
            {
                text: 'Descripció',
                dataIndex: 'descripcion',
                flex: 2
            },
            {
                text: 'Observacions',
                dataIndex: 'observaciones',
                flex: 2
            },
            {
                text: 'Exportado',
                dataIndex: 'exportado',
                renderer: dame_exportado,
                flex: 0.5
            }
        ],
        viewConfig: {
            getRowClass: function (row, index) {
                if (row.get('moroso')) {
                    return 'morosoCls';
                }
            }
        }
    }
)
;
