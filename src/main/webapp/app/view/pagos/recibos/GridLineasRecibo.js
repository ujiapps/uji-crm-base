Ext.define('CRM.view.pagos.recibos.GridLineasRecibo',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridLineasRecibo',
        labelAlign: 'top',
        autoScroll: true,
        store: 'StoreLineasRecibo',
        title: 'Línees',

        selModel: {
            mode: 'SINGLE'
        },

        columns: [
            {
                text: 'Concepte',
                dataIndex: 'concepto',
                flex: 2
            },
            {
                text: 'Origen',
                dataIndex: 'origen',
                flex: 0.5
            },
            {
                text: 'Referència 1',
                dataIndex: 'referencia1',
                flex: 1
            },
            {
                text: 'Referència 2',
                dataIndex: 'referencia2',
                flex: 1
            }
            ,
            {
                text: 'Línea finançament',
                dataIndex: 'lineaFinanciacion',
                flex: 0.5
            },
            {
                text: 'Import brut',
                dataIndex: 'importeBruto',
                flex: 1
            },
            {
                text: 'Import net',
                dataIndex: 'importeNeto',
                flex: 1
            },
            {
                text: 'DTO aplicat',
                dataIndex: 'descuento',
                flex: 1
            }
        ]
    });