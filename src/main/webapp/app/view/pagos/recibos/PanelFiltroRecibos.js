Ext.define('CRM.view.pagos.recibos.PanelFiltroRecibos',
    {
        extend: 'Ext.panel.Panel',
        alias: 'widget.filtroRecibos',
        frame: true,
        border: false,
        padding: 5,
        closable: false,
        layout: 'anchor',
        name: 'filtroRecibos',
        arrayFechas: [],
        arrayCampanyas: [],
        arrayEstados: [],
        arrayExportaciones: [],
        requires: ['CRM.model.enums.TiposOperadores', 'CRM.view.busqueda.FiltroFechas'],
        fbar: [
            {
                xtype: 'button',
                text: 'Cercar',
                name: 'botonBusqueda',
                iconCls: 'database-go'
            },
            {
                xtype: 'button',
                name: 'botonLimpiarBusqueda',
                text: 'Netejar',
                listeners: {
                    click: function () {
                        this.up('panel').limpiarCampos();
                    }
                }
            },

            {
                xtype: 'button',
                name: 'botonExportar',
                text: 'Exportar csv'
            }],

        initComponent: function () {
            var me = this;

            me.items = [
                {
                    xtype: 'container',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textarea',
                            name: 'textoFiltros',
                            margin: '5 10 0 0',
                            readOnly: true
                        },
                        {
                            xtype: 'panel',
                            title: 'Filtre Campanyes',
                            collapsible: true,
                            collapsed: true,
                            frame: true,
                            margin: 5,
                            fbar: [
                                {
                                    xtype: 'button',
                                    text: 'netejar filtre',
                                    name: 'limpiarFiltroCampanyaRecibos',
                                    listeners: {
                                        click: function () {
                                            this.up('panel').up('panel').limpiarCamposCampanya();
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'afegir condició',
                                    name: 'anyadirFiltroCampanyaRecibos'
                                }
                            ],
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    editable: false,
                                    emptyText: 'Trieu...',
                                    store: 'StoreOperadoresLogicos',
                                    displayField: 'nombre',
                                    valueField: 'id',
                                    name: 'comboOperadorLogicoCampanyaRecibos',
                                    padding: '0 5 0 0',
                                    flex: 1
                                },
                                {
                                    xtype: 'comboCampanyas',
                                    store: 'StoreCampanyasAccesoUserConTarifas',
                                    fieldLabel: 'Campanyes',
                                    labelWidth: 120,
                                    displayField: 'nombre',
                                    valueField: 'id',
                                    name: 'comboFiltroCampanya',
                                    queryMode: 'local',
                                    emptyText: 'Trieu...',
                                    editable: true,
                                    padding: '0 5 0 0',
                                    flex: 2
                                },
                                {
                                    xtype: 'combobox',
                                    editable: true,
                                    fieldLabel: 'Estat',
                                    labelWidth: 50,
                                    emptyText: 'Trieu...',
                                    store: 'StoreTiposCampanyasClientes',
                                    displayField: 'nombre',
                                    valueField: 'id',
                                    name: 'campanyaClienteTipoId',
                                    flex: 1
                                }]
                        },
                        {

                            xtype: 'panel',
                            title: 'Filtre Data Creació Rebut',
                            collapsible: true,
                            collapsed: true,
                            margin: 5,
                            frame: true,
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            fbar: [
                                {
                                    xtype: 'button',
                                    text: 'netejar filtre',
                                    name: 'limpiarFiltroFechaCreacionRecibos',
                                    listeners: {
                                        click: function () {
                                            this.up('panel').up('panel').limpiarCamposFecha();
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'afegir condició',
                                    name: 'anyadirFiltroFechaCreacionRecibos'
                                }

                            ],
                            items: [

                                {
                                    xtype: 'combobox',
                                    editable: false,
                                    emptyText: 'Trieu...',
                                    store: 'StoreOperadoresLogicos',
                                    displayField: 'nombre',
                                    valueField: 'id',
                                    name: 'comboOperadorLogicoFechaCreacionRecibos',
                                    padding: '0 5 0 0',
                                    flex: 1
                                },
                                {

                                    xtype: 'filtroFechas',
                                    name: 'filtrosFechasCreacionRecibos',
                                    flex: 8
                                }
                            ]
                        }, {
                            xtype: 'panel',
                            frame: true,
                            title: 'Filtre Estats',
                            collapsible: true,
                            collapsed: true,
                            margin: 5,
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            fbar: [
                                {
                                    xtype: 'button',
                                    text: 'netejar filtre',
                                    name: 'limpiarFiltroEstadoRecibos',
                                    listeners: {
                                        click: function () {
                                            this.up('panel').up('panel').limpiarCamposEstado();
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'afegir condició',
                                    name: 'anyadirFiltroEstadoRecibos'
                                }

                            ],
                            items: [
                                {
                                    xtype: 'combobox',
                                    editable: false,
                                    emptyText: 'Trieu...',
                                    store: 'StoreOperadoresLogicos',
                                    displayField: 'nombre',
                                    valueField: 'id',
                                    name: 'comboOperadorLogicoEstadosRecibos',
                                    padding: '0 5 0 0',
                                    flex: 1
                                },
                                {
                                    xtype: 'combobox',
                                    store: 'StoreTiposEstadoRecibo',
                                    fieldLabel: 'Estat',
                                    displayField: 'nombre',
                                    valueField: 'id',
                                    name: 'comboTipoEstadoRecibo',
                                    queryMode: 'local',
                                    emptyText: 'Trieu...',
                                    editable: true,
                                    flex: 3
                                }
                            ]
                        }, {
                            xtype: 'panel',
                            frame: true,
                            title: 'Filtre Exportacions',
                            collapsible: true,
                            collapsed: true,
                            margin: 5,
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            fbar: [
                                {
                                    xtype: 'button',
                                    text: 'netejar filtre',
                                    name: 'limpiarFiltroExportacionRecibos',
                                    listeners: {
                                        click: function () {
                                            this.up('panel').up('panel').limpiarCamposExportacion();
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'afegir condició',
                                    name: 'anyadirFiltroExportacionRecibos'
                                }

                            ],
                            items: [
                                {
                                    xtype: 'combobox',
                                    editable: false,
                                    emptyText: 'Trieu...',
                                    store: 'StoreOperadoresLogicos',
                                    displayField: 'nombre',
                                    valueField: 'id',
                                    name: 'comboOperadorLogicoExportaciones',
                                    flex: 1
                                },
                                {
                                    xtype: 'checkbox',
                                    boxLabel: 'Exportados. Amb data d\'exportació',
                                    name: 'checkboxExportado',
                                    checked: true,
                                    readOnly: true,
                                    padding: '5 5 5 5'
                                },
                                {
                                    xtype: 'filtroFechas',
                                    name: 'filtroFechasExportaciones',
                                    flex: 3
                                }
                            ]
                        }]
                }
            ];

            this.callParent(arguments);
        },

        dameObjetoCampanya: function () {
            return {
                operador: this.down("combobox[name=comboOperadorLogicoCampanyaRecibos]").getValue(),
                operadorNombre: this.down("combobox[name=comboOperadorLogicoCampanyaRecibos]").getRawValue(),
                campanya: this.down("combobox[name=comboFiltroCampanya]").getValue(),
                campanyaNombre: this.down("combobox[name=comboFiltroCampanya]").getRawValue(),
                campanyaTipoEstado: this.down("combobox[name=campanyaClienteTipoId]").getValue(),
                campanyaTipoEstadoNombre: this.down("combobox[name=campanyaClienteTipoId]").getRawValue()
            };
        },

        dameObjetoFecha: function () {
            return {
                operador: this.down("combobox[name=comboOperadorLogicoFechaCreacionRecibos]").getValue(),
                operadorNombre: this.down("combobox[name=comboOperadorLogicoFechaCreacionRecibos]").getRawValue(),
                campoFecha: this.down("container[name=filtrosFechasCreacionRecibos]").dameObjeto()
            };
        },

        dameObjetoEstado: function () {
            return {
                operador: this.down("combobox[name=comboOperadorLogicoEstadosRecibos]").getValue(),
                operadorNombre: this.down("combobox[name=comboOperadorLogicoEstadosRecibos]").getRawValue(),
                estado: this.down("combobox[name=comboTipoEstadoRecibo]").getValue(),
                estadoNombre: this.down("combobox[name=comboTipoEstadoRecibo]").getRawValue()
            };
        },

        dameObjetoExportacion: function () {
            return {
                operador: this.down("combobox[name=comboOperadorLogicoExportaciones]").getValue(),
                operadorNombre: this.down("combobox[name=comboOperadorLogicoExportaciones]").getRawValue(),
                campoFecha: this.down("container[name=filtroFechasExportaciones]").dameObjeto()
            };
        },

        campoValido: function (valor, num_caracteres) {
            if (valor != null)
                return valor.length >= num_caracteres || valor == -1 || valor == -2;
            else
                return false;
        },

        esFiltroCampanyaValido: function (obj) {
            return (obj.campanya != null);

        },

        esFiltroFechaValido: function (obj) {
            return (this.down("container[name=filtrosFechasCreacionRecibos]").esCampoFechaValido(obj.campoFecha));
        },

        esFiltroEstadoValido: function (obj) {
            return (obj.estado != null);
        },

        esFiltroExportacionValido: function (obj) {
            return (this.down("container[name=filtrosFechasCreacionRecibos]").esCampoFechaValido(obj.campoFecha) || this.down("container[name=filtrosFechasCreacionRecibos]").esCampoFechaVacio(obj.campoFecha));

        },

        toStringCampanya: function (objeto) {

            var texto = '';

            if (objeto.operador == null) texto = 'Y ';
            else texto = objeto.operadorNombre + ' ';

            if (objeto.campanya != null) {
                texto = texto + objeto.campanyaNombre;
            }
            if (objeto.campanyaTipoEstado != null) {
                texto = texto + ' ' + objeto.campanyaTipoEstadoNombre;
            }

            return texto;
        },

        toStringFecha: function (objeto) {

            var texto = '';

            if (objeto.operador == null) texto = 'Y ';
            else texto = objeto.operadorNombre + ' ';

            if (objeto.campoFecha != null) {
                texto = texto + this.down("container[name=filtrosFechasCreacionRecibos]").toString(objeto.campoFecha);
            }

            return texto;
        },

        toStringEstado: function (objeto) {

            var texto = '';

            if (objeto.operador == null) texto = 'Y ';
            else texto = objeto.operadorNombre + ' ';

            if (objeto.estado != null) {
                texto = texto + objeto.estadoNombre;
            }

            return texto;
        },

        toStringExportacion: function (objeto) {

            var texto = '';

            if (objeto.operador == null) texto = 'Y ';
            else texto = objeto.operadorNombre + ' ';

            texto = texto + 'Exportados ';

            if (objeto.campoFecha.tipoFecha != null) {
                texto = texto + ' amb data d\'exportació' + this.down("container[name=filtroFechasExportaciones]").toString(objeto.campoFecha);
            }
            else texto = texto + ' a cualsevol data';

            return texto;
        },

        limpiarCamposCampanya: function () {

            this.down("combobox[name=comboOperadorLogicoCampanyaRecibos]").setValue(null);
            this.down("combobox[name=comboFiltroCampanya]").setValue(null);
            this.down("combobox[name=campanyaClienteTipoId]").setValue(null);
        },

        limpiarCamposFecha: function () {

            this.down("combobox[name=comboOperadorLogicoFechaCreacionRecibos]").setValue(null);
            this.down("container[name=filtrosFechasCreacionRecibos]").limpiarCampos();
        },

        limpiarCamposEstado: function () {

            this.down("combobox[name=comboOperadorLogicoEstadosRecibos]").setValue(null);
            this.down("combobox[name=comboTipoEstadoRecibo]").setValue(null);
        },

        limpiarCamposExportacion: function () {

            this.down("combobox[name=comboOperadorLogicoExportaciones]").setValue(null);
            this.down("container[name=filtroFechasExportaciones]").limpiarCampos();

        },

        limpiaArray: function (vector) {
            while (vector.length > 0) {
                vector.pop();
            }
        },

        limpiarCampos: function () {
            this.down('textarea[name=textoFiltros]').setValue(null);

            this.limpiarCamposCampanya();
            this.limpiarCamposFecha();
            this.limpiarCamposEstado();
            this.limpiarCamposExportacion();

            this.limpiaArray(this.arrayFechas);
            this.limpiaArray(this.arrayCampanyas);
            this.limpiaArray(this.arrayEstados);
            this.limpiaArray(this.arrayExportaciones);

            var objeto =
            {
                arrayCampanyas: null,
                arrayFechas: null,
                arrayEstados: null,
                arrayExportaciones: null
            };

            this.up('panel').down('grid').getStore().getProxy().extraParams = objeto;

            this.up('panel').down('grid').getStore().load({
                params: {
                    start: 0,
                    page: 0
                }
            });
        }
    })
;