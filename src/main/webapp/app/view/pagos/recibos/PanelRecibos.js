Ext.define('CRM.view.pagos.recibos.PanelRecibos',
    {
        extend: 'Ext.panel.Panel',
        title: 'Rebuts',
        alias: 'widget.panelRecibos',
        requires: ['CRM.view.pagos.recibos.GridRecibos', 'CRM.view.pagos.recibos.PanelFiltroRecibos'],
        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        items: [{
            xtype: 'filtroRecibos',
            title : 'Filtrar',
            collapsible: true,
            collapsed: false,
            animCollapse: true,
            collapseDirection: 'bottom'
        }, {
            xtype: 'gridRecibos',
            flex: 5
        }]

    });