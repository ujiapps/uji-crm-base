Ext.define('CRM.view.pagos.recibos.GridMovimientosRecibo',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridMovimientosRecibo',
        labelAlign: 'top',
        autoScroll: true,
        store: 'StoreMovimientosRecibo',
        title: 'Moviments',

        selModel: {
            mode: 'SINGLE'
        },

        columns: [
            {
                text: 'Data',
                dataIndex: 'fecha',
                flex: 1,
                renderer: Ext.util.Format.dateRenderer('d/m/Y')
            },
            {
                text: 'Tipo Moviment',
                dataIndex: 'tipoMovimiento',
                flex: 2
            },
            {
                text: 'Descripció',
                dataIndex: 'descripcion',
                flex: 2
            },
            {
                text: 'Remesa exportació',
                dataIndex: 'remesaExportacion',
                flex: 1
            },
            {
                text: 'Data exportació',
                dataIndex: 'fechaExportacion',
                flex: 1,
                renderer: Ext.util.Format.dateRenderer('d/m/Y')
            },
            {
                xtype: 'actioncolumn',
                text: '',
                name: 'quitarMovimientoExportado',
                flex: 0.5,
                menuDisabled: true,
                align: 'center',
                renderer: function (value, metaData, record) {
                    if (!record.data.remesaExportacion) {
                        metaData.style = 'visibility: hidden';
                    }
                },
                items: [
                    {
                        xtype: 'button',
                        name: 'quitarMovimientoExportado',
                        icon: '//static.uji.es/js/extjs/uji-commons-extjs/img/application_edit.png'
                    }]
            }

        ]
    });