Ext.define('CRM.view.pagos.recibos.VentanaRecibo',
    {
        extend: 'Ext.Window',
        title: 'Rebut',
        width: 700,
        maxHeight: 700,
        autoScroll: true,
        alias: 'widget.ventanaRecibo',
        modal: true,
        //closable: false,
        y: 40,

        listeners: {
            afterrender: function () {
                var viewSize = Ext.getBody().getViewSize();
                this.maxHeight = viewSize.height - 80;
            }
        },

        fbar: [
            {
                xtype: 'button',
                name: 'abrirVentanaRecibo',
                text: 'ir a recibo',
                listeners: {
                    click: function () {
                        this.up('window').abrirVentanaReciboPlsql(this.up('window').down("textfield[name=id]").getValue());
                    }
                }
            //},
            //{
            //    xtype: 'button',
            //    name: 'cerrarVentanaDetalleRecibo',
            //    text: 'cerrar'
            }
        ],

        items: [
            {
                xtype: 'form',
                name: 'formRecibo',
                padding: 10,
                enableKeyEvents: true,
                frame: true,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Rebut',
                        name: 'id',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Tipus rebut',
                        name: 'tipoReciboNombre',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Tipo cobro',
                        name: 'tipoCobroNombre',
                        readOnly: true
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Data creació',
                        name: 'fechaCreacion',
                        readOnly: true,
                        renderer: Ext.util.Format.dateRenderer('d/m/Y')
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Data limit',
                        name: 'fechaPagoLimite',
                        renderer: Ext.util.Format.dateRenderer('d/m/Y')
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Data pagament',
                        name: 'fechaPago',
                        renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                        readOnly: true
                    },
                    {
                        xtype: 'combobox',
                        name: 'idioma',
                        store: 'StoreTiposIdioma',
                        displayField: 'nombre',
                        fieldLabel: 'Idioma',
                        valueField: 'id',
                        allowBlank: false,
                        editable: false
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'correu',
                        name: 'correo'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'IBAN domiciliació',
                        name: 'ibanDomiciliacion'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'IBAN d\'abonament',
                        name: 'ibanAbono'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Descripció',
                        name: 'descripcion'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Destinatari del rebut (nom, direcció postal i identifició)',
                        name: 'destinatario'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Peu del rebut',
                        name: 'pie'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Observacions',
                        name: 'observaciones'
                    },
                    {
                        xtype: 'gridLineasRecibo',
                        name: 'gridLineasRecibo'
                    },
                    {
                        xtype: 'gridMovimientosRecibo',
                        name: 'gridMovimientosRecibo',
                        paddingtop: '10'
                    }]
            }],


        abrirVentanaReciboPlsql: function (recibo) {
            var url = 'https://e-ujier.uji.es/pls/www/!ficha_recibo.inicio?p_recibo=' + recibo;
            window.open(url);
        }

    })
;