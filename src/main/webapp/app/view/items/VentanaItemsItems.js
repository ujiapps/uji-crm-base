Ext.define('CRM.view.items.VentanaItemsItems',
{
    extend : 'Ext.Window',
    title : 'Associació de Items i ítems',
    width : 700,
    height : 400,
    alias : 'widget.ventanaItemsItems',
    requires : [ 'CRM.view.commons.GrouppedComboBox', 'CRM.view.items.GridVentanaItems' ],
    modal : true,

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    fbar : [
    {
        xtype : 'button',
        text : 'Afegir',
        action : 'anyadirOrigen'
    },
    {
        xtype : 'button',
        text : 'Afegir',
        action : 'anyadirDestino'
    },
    {
        xtype : 'button',
        text : 'Cancel·lar',
        action : 'cancelar'
    } ],

    items : [
    {
        xtype : 'container',
        layout : 'hbox',
        padding : 10,
        height : 50,

        items : [
        {
            border : 0,
            xtype : 'grouppedComboBox',
            editable : false,
            labelWidth : 60,
            labelAlign : 'left',
            margin : '3 20 0 0',
            fieldLabel : 'Grupo',
            emptyText : 'Trieu...',
            store : 'StoreVentanaGrupos',
            displayField : 'nombreCa',
            groupField : "programaId",
            groupDisplayField : "programaNombre",
            valueField : 'id',
            name : 'filtroGrupos',
            width : 500
        },
        {
            xtype : 'button',
            text : 'Netejar Filtres',
            name : 'limpiarFiltros'
        } ]
    },
    {
        xtype : 'gridVentanaItems',
        flex : 1,
        name : 'gridVentanaItems'
    } ]
});