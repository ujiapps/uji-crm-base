Ext.define('CRM.view.items.GridItemsItemsDestino',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridItemsItemsDestino',
    store : 'StoreItemsItemsDestino',
    forceFit : true,
    sortableColumns : true,

    tbar : [
    {
        xtype : 'button',
        name : 'anyadirItemItemDestino',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarItemItemDestino',
        text : 'Esborrar',
        iconCls : 'application-delete'
    } ],

    columns : [
    {
        text : 'Item',
        dataIndex : 'itemDestino'
    } ]

});
