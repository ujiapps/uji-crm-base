Ext.define('CRM.view.items.GridVentanaItems',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridVentanaItems',
    store : 'StoreVentanaItems',
    forceFit : true,
    sortableColumns : true,
    multiSelect : true,

    columns : [
    {
        text : 'Item',
        dataIndex : 'nombreCa',
        flex : 2
    } ]

});
