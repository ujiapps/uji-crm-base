Ext.define('CRM.view.items.GridItems',
{
    extend : 'Ext.grid.Panel',
    title : 'Elements del suscriptor',
    alias : 'widget.gridItems',
    store : 'StoreItems',
    forceFit : true,
    sortableColumns : true,

    tbar : [
    {
        xtype : 'button',
        name : 'anyadirItem',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarItem',
        text : 'Esborrar',
        iconCls : 'application-delete',
        disabled : true
    } ],

    columns : [
    {
        text : 'Item',
        dataIndex : 'nombreCa',
        flex : 2
    },
    {
        text : 'Ordre',
        dataIndex : 'orden',
        flex : 1
    },
    {
        text : 'Visible',
        dataIndex : 'visible',
        xtype : 'booleancolumn',
        trueText : 'Sí',
        falseText : 'No',
        flex : 1
    },
    {
        text : 'Tipo Access',
        dataIndex : 'tipoAccesoNombre',
        flex : 1
    },
    {
        text : 'Id',
        dataIndex : 'id',
        name : 'id',
        hidden : true
    } ]

});
