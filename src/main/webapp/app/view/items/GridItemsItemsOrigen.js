Ext.define('CRM.view.items.GridItemsItemsOrigen',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridItemsItemsOrigen',
    store : 'StoreItemsItemsOrigen',
    forceFit : true,
    sortableColumns : true,

    tbar : [
    {
        xtype : 'button',
        name : 'anyadirItemItemOrigen',
        text : 'Afegir',
        iconCls : 'application-add'
    },
    {
        xtype : 'button',
        name : 'borrarItemItemOrigen',
        text : 'Esborrar',
        iconCls : 'application-delete'
    } ],

    columns : [
    {
        text : 'Item',
        dataIndex : 'itemOrigen'
    } ]

});
