Ext.define('CRM.view.clientes.PestanyaPagos.PanelClientesPagos', {
    extend: 'Ext.panel.Panel',
    title: 'Pagos',
    alias: 'widget.panelClientesPagos',
    // closable: false,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    items: [{
        xtype: 'grid',
        name: 'gridClientesPagos',
        store: 'StoreRecibosCliente',
        flex: 1,
        dockedItems: [{
            xtype: 'pagingtoolbar',
            store: 'StoreRecibosCliente',
            dock: 'bottom',
            displayInfo: true
        }],
        columns: [{
            text: 'Fecha Creacion',
            dataIndex: 'fechaCreacion',
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            flex: 1
        }, {
            text: 'Fecha Pago Limite',
            dataIndex: 'fechaPagoLimite',
            flex: 1,
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }, {
            text: 'Fecha Pago',
            dataIndex: 'fechaPago',
            flex: 1,
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }, {
            text: 'Importe',
            flex: 1,
            dataIndex: 'importeNeto'
        }, {
            text: 'Tipo Recibo',
            flex: 1,
            dataIndex: 'tipoReciboNombre'
        }, {
            text: 'Observacions',
            flex: 2,
            dataIndex: 'observaciones'
        }],

        rellenaPanelClientesPagos: function (personaId, tabsServiciosCliente) {
            var store = this.getStore();
            if (personaId) {
                store.getProxy().url = 'rest/recibo/' + personaId;
                store.load({
                    url: 'rest/recibo/' + personaId
                });
            }
            else {
                store.loadData([], false);
            }
        }
    }]
});