Ext.define('CRM.view.clientes.GridSeguimientos', {
    extend: 'Ext.grid.Panel',
    title: 'Seguiments',
    alias: 'widget.gridSeguimientos',
    requires: ['Ext.grid.plugin.RowEditing'],
    sortableColumns: true,

    selModel: {
        mode: 'SINGLE'
    },

    plugins: [{
        ptype: 'rowediting',
        clicksToEdit: 2,
        pluginId: 'rowediting'
    }],

    tbar: [{
        xtype: 'button',
        name: 'anyadir',
        text: 'Afegir',
        iconCls: 'application-add'
    }, {
        xtype: 'button',
        name: 'borrar',
        text: 'Esborrar',
        iconCls: 'application-delete',
        disabled: true
    }],

    columns: [{
        header: 'Tipus',
        dataIndex: 'seguimientoTipoId',
        flex: 2,
        xtype : 'combocolumn',
        combo: {
            xtype: 'combobox',
            store: Ext.create('CRM.store.StoreSeguimientoTipos'),
            editable: false,
            displayField: 'nombre',
            valueField: 'id',
            allowBlank: false,
            emptyText: 'Selecciona un tipus...'
        }
    }, {
        header: 'Campanya',
        dataIndex: 'campanyaId',
        flex: 2,
        xtype: 'combocolumn',
        combo:
            {
                xtype: 'combobox',
                store: Ext.create('CRM.store.StoreCampanyas'),
                editable: false,
                displayField: 'nombre',
                valueField: 'id',
                allowBlank: true,
                emptyText: 'Selecciona una campanya...'
            }
    }, {
        text: 'Data',
        dataIndex: 'fecha',
        renderer: Ext.util.Format.dateRenderer('d/m/Y'),
        flex: 1,
        editor: {
            xtype: 'datefield',
            format: 'd/m/Y',
            minValue: '1',
            allowBlank: false
        }
    }, {
        text: 'Nom',
        dataIndex: 'nombre',
        flex: 2,
        editor: {
            allowBlank: false
        }
    }, {
        text: 'Descripció',
        dataIndex: 'descripcion',
        flex: 6,
        editor: {}
    }],
    listeners: {
        activate: function () {
            var storeClienteSeguimientos = this.getStore();
            storeClienteSeguimientos.load();
        }
    }
});