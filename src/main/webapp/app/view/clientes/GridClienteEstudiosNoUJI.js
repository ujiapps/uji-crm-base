Ext.define('CRM.view.clientes.GridClienteEstudiosNoUJI', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridClienteEstudiosNoUJI',
    requires: ['Ext.grid.plugin.RowEditing'],
    plugins: [{
        ptype: 'rowediting',
        clicksToEdit: 2
    }],
    listeners: {
        selectionchange: function () {
            this.setEstadoComponentes();
        },
        itemdblclick: function () {
            var grid = this;
            this.getPlugin().editor.down("combobox[name=clasificacionId]").getStore().filterBy(function (record) {
                return record.data.tipo == grid.getSelectionModel().getSelection()[0].data.tipoEstudio;
            })
        }
    },
    tbar: [{
        xtype: 'button',
        name: 'anyadirClienteEstudioNoUJI',
        text: 'Afegir',
        iconCls: 'application-add'
    }, {
        xtype: 'button',
        name: 'borrarClienteEstudioNoUJI',
        text: 'Esborrar',
        iconCls: 'application-delete',
        disabled: true
    }],

    columns: [{
        dataIndex: 'id',
        hidden: true
    }, {
        header: 'Tipo Estudio',
        dataIndex: 'tipoEstudio',
        xtype: 'combocolumn',
        combo: {
            xtype: 'combobox',
            editable: false,
            store: Ext.create('CRM.store.StoreTiposEstudiosNoUJI'),
            displayField: 'nombre',
            valueField: 'id',
            name: 'tipoEstudio',
            queryMode: 'local',
            listeners: {
                select: function (elem, sel) {
                    this.up().down("combobox[name=clasificacionId]").getStore().filterBy(function (record) {
                        return record.data.tipo == sel[0].data.id;
                    })
                }
            },
        },
        flex: 1
    }, {
        header: 'Clasificació',
        dataIndex: 'clasificacionId',
        xtype: 'combocolumn',
        combo: {
            xtype: 'combobox',
            editable: false,
            store: Ext.create('CRM.store.StoreClasificacionesEstudioNoUJI'),
            displayField: 'nombreCa',
            valueField: 'id',
            name: 'clasificacionId',
            queryMode: 'local'
        },
        flex: 1
    }, {
        text: 'Nom',
        dataIndex: 'nombre',
        editor: {
            allowBlank: true
        },
        flex: 1
    }, {
        header: 'Universidad',
        dataIndex: 'universidadId',
        xtype: 'combocolumn',
        combo: {
            xtype: 'combobox',
            editable: false,
            store: Ext.create('CRM.store.StoreUniversidades'),
            displayField: 'nombreCa',
            valueField: 'id',
            name: 'universidadId',
            queryMode: 'local'
        },
        flex: 1
    }, {
        text: 'Any finalització',
        dataIndex: 'anyoFinalizacion',
        editor: {
            allowBlank: true
        },
        flex: 0.5
    }],

    setEstadoComponentes: function () {

        var tieneSeleccionGrid = this.getSelectionModel().hasSelection();
        this.down("button[name=borrarClienteEstudioNoUJI]").setDisabled(!tieneSeleccionGrid);
    }
});