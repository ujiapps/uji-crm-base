Ext.define('CRM.view.clientes.VentanaSeguimiento',
{
    extend : 'Ext.Window',
    title : 'Seguiment',
    width : 500,
    alias : 'widget.ventanaSeguimiento',
    modal : true,
    fbar : [
    {
        xtype : 'button',
        text : 'Guardar',
        action : 'guardar'
    },
    {
        xtype : 'button',
        text : 'Cancel·lar',
        action : 'cancelar'
    } ],

    items : [
    {
        xtype : 'form',
        name : 'formEdicionSeguimiento',
        frame : true,
        padding : 10,
        enableKeyEvents : true,
        layout :
        {
            type : 'vbox',
            align : 'stretch'
        },

        items : [
        {
            xtype : 'datefield',
            fieldLabel : 'Data',
            displayField : 'fecha',
            name : 'fecha',
            format : 'd/m/Y',
            allowBlank : false
        },
        {
            xtype : 'combobox',
            name : 'seguimientoTipoId',
            store : 'StoreSeguimientoTipos',
            displayField : 'nombre',
            fieldLabel : 'Tipo',
            valueField : 'id',
            allowBlank : false,
            editable : false
        },

        {
            xtype : 'textfield',
            fieldLabel : 'Nom',
            displayField : 'nombre',
            name : 'nombre',
            allowBlank : false
        },
        {
            xtype : 'textareafield',
            fieldLabel : 'Descripció',
            displayField : 'descripcion',
            name : 'descripcion'
        },
        {
            xtype : 'textfield',
            name : 'clienteId',
            hidden : true
        },
        {
            xtype : 'textfield',
            name : 'id',
            hidden : true
        } ]
    } ]

});