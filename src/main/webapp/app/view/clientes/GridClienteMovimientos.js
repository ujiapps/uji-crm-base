function formatDate(value) {
    return value ? Ext.Date.dateFormat(value, 'd/m/Y') : '';
}

Ext.define('CRM.view.clientes.GridClienteMovimientos',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridClienteMovimientos',

        columns: [
            {
                text: 'Data',
                dataIndex: 'fecha',
                format: 'd/m/Y',
                renderer: formatDate,
                flex: 1
            },
            {
                text: 'Descripció',
                dataIndex: 'descripcion',
                flex: 4
            },

            {
                text: 'Tipus',
                dataIndex: 'tipoNombre',
                flex: 2
            },
            {
                text: 'Campanya',
                dataIndex: 'campanyaNombre',
                flex: 1
            }],
        listeners: {
            activate: function () {
                var storeClienteMovimientos = this.getStore();
                storeClienteMovimientos.load();
            }
        }
    }
);