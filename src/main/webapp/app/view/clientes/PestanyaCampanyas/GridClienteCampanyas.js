Ext.define('CRM.view.clientes.PestanyaCampanyas.GridClienteCampanyas',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridClienteCampanyas',
        labelAlign: 'top',
        autoScroll: true,

        selModel: {
            mode: 'SINGLE'
        },

        plugins: [
            {
                ptype: 'rowediting',
                clicksToEdit: 2
            }],

        tbar: [
            {
                xtype: 'button',
                name: 'anyadir',
                text: 'Afegir',
                iconCls: 'application-add'
            },
            {
                xtype: 'button',
                name: 'borrar',
                text: 'Esborrar',
                iconCls: 'application-delete',
                disabled: true
            }],

        columns: [
            {
                name: 'id',
                dataIndex: 'id',
                hidden: true
            },
            {
                header: 'Campanya',
                dataIndex: 'campanyaId',
                flex: 3,
                xtype: 'combocolumn',
                combo: {
                    xtype: 'combobox',
                    editable: false,
                    displayField: 'nombre',
                    valueField: 'id',
                    queryMode: 'local',
                    store: Ext.create('CRM.store.StoreCampanyas')
                }
            },
            {
                header: 'Tipus',
                dataIndex: 'campanyaClienteTipoId',
                name: 'comboCampanyaClienteTipo',
                // xtype: 'combocolumn',
                flex: 1,
                renderer: function (value, metaData, record) {
                    return record.data.campanyaClienteTipoNombre;
                },
                editor: {
                    xtype: 'combobox',
                    store: Ext.create('CRM.store.StoreTiposEstadoCampanya', {storeId: 'StoreTiposEstadoCampanyaCliente'}),
                    displayField: 'nombre',
                    valueField: 'id',
                    queryMode: 'local'
                }
            },
            {
                header: 'Motivo',
                dataIndex: 'campanyaClienteMotivoId',
                // xtype: 'combocolumn',
                name: 'comboCampanyaClienteMotivo',
                flex: 1,
                renderer: function (value, metaData, record) {
                    return record.data.campanyaClienteMotivoNombre;
                },
                editor: {
                    xtype: 'combobox',
                    store: Ext.create('CRM.store.StoreTiposEstadoCampanyaMotivos', {storeId: 'StoreTiposEstadoCampanyaMotivosCliente'}),
                    displayField: 'nombreCa',
                    valueField: 'id',
                    queryMode: 'local'
                }
            }]
    });