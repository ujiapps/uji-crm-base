function formatDate(value)
{
    return value ? Ext.Date.dateFormat(value, 'd/m/Y') : '';
}

Ext.define('CRM.view.clientes.PestanyaCampanyas.GridClienteMovimientosCampanya',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridClienteMovimientosCampanya',
    frame : true,
    labelAlign : 'top',
    autoScroll : true,
    // store : 'StoreMovimientosCampanya',

    columns : [
    {
        text : 'Data',
        dataIndex : 'fecha',
        format : 'd/m/Y',
        renderer : formatDate,
        flex : 1
    },
    {
        text : 'Descripció',
        dataIndex : 'descripcion',
        flex : 4
    },

    {
        text : 'Tipus',
        dataIndex : 'tipoNombre',
        flex : 2
    } ]
});