function formatDate(value) {
    return value ? Ext.Date.dateFormat(value, 'd/m/Y') : '';
}

Ext.define('CRM.view.clientes.PestanyaCampanyas.GridClienteTarifasCampanya', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridClienteTarifasCampanya',
    frame: true,
    labelAlign: 'top',
    autoScroll: true,
    // store: 'StoreTarifasClienteCampanya',
    plugins: {
        ptype: 'rowediting',
        clicksToEdit: 2
    },
    tbar: [{
        xtype: 'button',
        name: 'anyadirTarifaClienteCampanya',
        text: 'Afegir',
        iconCls: 'application-add'
    }, {
        xtype: 'button',
        name: 'eliminarTarifaClienteCampanya',
        text: 'Esborrar',
        iconCls: 'application-delete',
        disabled: true
    }
    ],

    columns: [{
        //xtype: 'combocolumn',
        text: 'Descripció',
        dataIndex: 'tipoTarifaId',
        flex: 4,
        //store: Ext.create('CRM.store.StoreTiposTarifas')
        //editor : false,
        renderer: function (value, metaData, record) {
            if (value) {
                return record.get("_tipoTarifa").nombre;
            }
            return '';
        }
        //combo: {
        //    xtype: 'combobox',
        //    store: Ext.create('CRM.store.StoreTiposTarifas'),
        //    displayField: 'nombre',
        //    valueField: 'id',
        //    editable: false
        //}
    }
        , {
        text: 'Data Inici',
        dataIndex: 'fechaInicioTarifa',
        format: 'd/m/Y',
        renderer: formatDate,
        flex: 1,
        editor: {
            xtype: 'datefield'
        }
    },  {
        text: 'Data Caduca',
        dataIndex: 'fechaCaducidad',
        format: 'd/m/Y',
        renderer: formatDate,
        flex: 1,
        editor: {
            xtype: 'datefield'
        }
    },
        {
            text: 'Data modificacio',
            dataIndex: 'fechaUltimaModificacion',
            format: 'd/m/Y',
            renderer: formatDate,
            flex: 1
        }]
});