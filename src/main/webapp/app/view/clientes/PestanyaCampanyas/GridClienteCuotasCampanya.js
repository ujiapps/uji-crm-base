function formatDate(value) {
    return value ? Ext.Date.dateFormat(value, 'd/m/Y') : '';
}

Ext.define('CRM.view.clientes.PestanyaCampanyas.GridClienteCuotasCampanya',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridClienteCuotasCampanya',
        frame: true,
        labelAlign: 'top',
        autoScroll: true,
        plugins: [
            {
                ptype: 'rowediting',
                clicksToEdit: 2
            }],

        columns: [{
            text: 'Recibo Id',
            dataIndex: 'reciboId',
            flex: 1
        }, {
            text: 'Data Inici',
            dataIndex: 'fechaInicio',
            format: 'd/m/Y',
            renderer: formatDate,
            flex: 1,
            editor: {
                xtype: 'datefield'
            }
        }, {
            text: 'Data Fí',
            dataIndex: 'fechaFin',
            format: 'd/m/Y',
            renderer: formatDate,
            flex: 1,
            editor: {
                xtype: 'datefield'
            }
        }, {
            text: 'Import',
            dataIndex: 'importe'
        }, {
            xtype: 'combocolumn',
            text: 'Estat',
            dataIndex: 'estado',
            combo: {
                xtype: 'combobox',
                editable: false,
                store: Ext.create('CRM.store.StoreTiposEstadoCuota'),
                displayField: 'nombre',
                valueField: 'id',
                name: 'estado',
                queryMode: 'local'
            }
        }]
    });