Ext.define('CRM.view.clientes.PestanyaCampanyas.GridClienteDatosCampanya',
    {
        extend: 'Ext.grid.Panel',
        alias : 'widget.gridClienteDatosCampanya',
        frame: true,
        labelAlign: 'top',
        autoScroll: true,
        // store: Ext.create('CRM.store.StoreClienteDatos'),
        listeners: {
            selectionchange: function () {
                this.setEstadoComponentes();
            }
        },
        tbar: [
            {
                xtype: 'button',
                name: 'anyadirClienteDatoCampanya',
                text: 'Afegir',
                iconCls: 'application-add'
            },
            {
                xtype: 'button',
                name: 'borrarClienteDato',
                text: 'Esborrar',
                iconCls: 'application-delete',
                disabled: true
            }],

        columns: [
            {
                text: 'Nom',
                dataIndex: 'nombreDatoCa',
                flex: 1
            },
            {
                text: 'Valor',
                dataIndex: 'valorCa',
                flex: 1
            }],

        setEstadoComponentes: function () {

            var tieneSeleccionGrid = this.getSelectionModel().hasSelection();
            this.down("button[name=borrarClienteDato]").setDisabled(!tieneSeleccionGrid);
        }
    });