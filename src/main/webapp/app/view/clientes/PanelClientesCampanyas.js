Ext.define('CRM.view.clientes.PanelClientesCampanyas',
    {
        extend: 'Ext.panel.Panel',
        alias: 'widget.panelClientesCampanyas',

        layout: {
            type: 'hbox',
            align: 'stretch'
        },

        initComponent: function () {
            var me = this;

            me.items = [{
                xtype: 'gridClienteCampanyas',
                itemId: 'gridCampanyas-' + me.itemClienteId,
                store: Ext.create('CRM.store.StoreClienteCampanyas', {
                    itemId: 'storeCampanyas-' + me.itemClienteId,
                    listeners: {
                        beforeload: function (store) {
                            store.getProxy().url = '/crm/rest/campanyacliente/cliente/' + me.itemClienteId;
                        }
                    }
                }),
                flex: 1

            }, {
                xtype: 'tabpanel',
                name: 'tabsCampanyaCliente',
                disabled: true,
                itemId: 'tabCampanyaCliente-' + me.itemClienteId,
                flex: 2,
                items: [{
                    xtype: 'gridClienteMovimientosCampanya',
                    title: 'Moviments',
                    itemId: 'gridClienteMovimientosCampanya-' + me.itemClienteId,
                    store: Ext.create('CRM.store.StoreMovimientosCampanya', {itemId: 'storeMovimientosCampanya-' + me.itemClienteId}),
                    tooltip: 'Moviments efectuats sobre la campanya'
                }, {
                    xtype: 'gridClienteDatosCampanya',
                    title: 'Dades Extra',
                    itemId: 'gridClienteDatosCampanya-' + me.itemClienteId,
                    store: Ext.create('CRM.store.StoreClienteDatos', {itemId: 'storeClienteDatosCampanya-' + me.itemClienteId}),
                    tooltip: 'Dades adquirits a la campanya'
                }, {
                    xtype: 'panel',
                    title: 'Tarifas',
                    tooltip: 'Tarifes asignades al client',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [{
                        xtype: 'gridClienteTarifasCampanya',
                        itemId: 'gridClienteTarifaCampanya-' + me.itemClienteId,
                        store: Ext.create('CRM.store.StoreTarifasClienteCampanya', {itemId: 'storeClienteTarifaCampanya-' + me.itemClienteId}),
                        flex: 1

                    }, {
                        xtype: 'gridClienteCuotasCampanya',
                        itemId: 'gridClienteCuotaCampanya-' + me.itemClienteId,
                        store: Ext.create('CRM.store.StoreCuotasClienteCampanya', {itemId: 'storeClienteCuotasCampanya-' + me.itemClienteId}),
                        flex: 1
                    }]
                }]
            }];

            me.on('activate', function () {
                var storeClienteCampanyas = this.down("[itemId=gridCampanyas-" + me.itemClienteId + "]").getStore();
                storeClienteCampanyas.load();
            });

            this.callParent(me);
        }

    });