Ext.define('CRM.view.clientes.VentanaExportacion',
{
    extend : 'Ext.Window',
    title : 'Exportar dades',
    width : 600,
    height : 500,
    alias : 'widget.ventanaExportacion',
    modal : true,
    autoScroll : true,
    items : [
    {
        xtype : 'form',
        name : 'checksExportacion',
        frame : true

    } ],
    fbar : [
    {
        xtype : 'button',
        text : 'Exportar',
        name : 'exportar'
    },
    {
        xtype : 'button',
        text : 'Cancel·lar',
        name : 'cancelarExportacion'
    } ]

});