Ext.define('CRM.view.clientes.VentanaBusquedaCliente', {
    extend: 'Ext.Window',
    requires: ['CRM.view.clientes.GridClientes', 'CRM.view.busqueda.FiltrosBusqueda'],

    title: 'Búsqueda Clients',
    width: '90%',
    height: '70%',
    alias: 'widget.ventanaBusquedaCliente',

    layout: 'border',
    border: 0,
    //
    defaults: {
            autoScroll: true,
        //     modal: true,
        //     flex: 1,
        //     labelAlign: 'top',
        //     closable: false,
        border: 0,
        //     region: 'center',
    },

    items: [{
        xtype: 'filtrosBusqueda',
        name: 'filtrosBusquedaClientes',
        region: 'north',
        // flex : 1,
        // height : 150,
        resizable : true
    }, {
        xtype: 'gridClientes',
        region: 'center',
        flex: 3,
        // collapsible: true,
        // collapsed: false,
        // frame: true,
        // collapseDirection: 'top',
        name: 'gridClientes',
        // border: 0
    }]
});