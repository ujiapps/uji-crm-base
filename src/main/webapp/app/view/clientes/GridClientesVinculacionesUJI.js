function formatDate(value) {
    return value ? Ext.Date.dateFormat(value, 'd/m/Y') : '';
}

Ext.define('CRM.view.clientes.GridClientesVinculacionesUJI',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridClientesVinculacionesUJI',
        // frame: true,
        // labelAlign: 'top',
        // flex : 1,
        // autoScroll: true,
        enableKeyEvents: true,
        selModel: {
            mode: 'SINGLE'
        },
        store: 'StoreVinculacionesUJI',

        columns: [{
            text: 'Vincul',
            dataIndex: 'vinculoNombre',
            flex: 3
        }, {
            text: 'Sub Vincul',
            dataIndex: 'subVinculoNombre',
            flex: 3
        }, {
            text: 'Dada Alta',
            dataIndex: 'fecha_alta',
            format: 'd/m/Y',
            renderer: formatDate,
            flex: 1
        }, {
            text: 'Dada baixa',
            dataIndex: 'fecha_baja',
            format: 'd/m/Y',
            renderer: formatDate,
            flex: 1
        }],

        rellenaGridVinculacionesUJI: function (personaId) {

            var storeVinculacionesUJI = this.getStore();
            if (personaId) {
                storeVinculacionesUJI.load({
                    url: '/crm/rest/vinculouji/' + personaId
                });
            }
            else {
                storeVinculacionesUJI.loadData([], false);
            }
        }
    });