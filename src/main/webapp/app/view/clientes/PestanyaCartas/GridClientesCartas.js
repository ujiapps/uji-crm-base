Ext.define('CRM.view.clientes.PestanyaCartas.GridClientesCartas',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridClientesCartas',
        tbar: [{
            xtype: 'button',
            text: 'Imprimir Carta',
            name: 'imprimirCartaPdf',
            disabled: true
        }],

        columns: [{
            text: 'Fecha Creacion',
            dataIndex: 'fechaCreacion',
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            flex: 1
        }, {
            text: 'Carta',
            flex: 2,
            dataIndex: 'clienteCarta'
        }],

        listeners: {
            activate: function () {
                var storeClienteCartas = this.getStore();
                storeClienteCartas.load();
            },
            select: function () {
                this.down("button[name=imprimirCartaPdf]").setDisabled(false);
            }
        }
    });