Ext.define('CRM.view.clientes.FormClientesDatos', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.formClientes',
    frame: true,
    border: 0,
    // labelAlign: 'top',
    // autoScroll: true,
    fbar: [{
        xtype: 'button',
        name: 'actualizarCliente',
        text: 'Actualitzar',
        width: 100

    }, {
        xtype: 'button',
        name: 'eliminarCliente',
        text: 'Eliminar Ficha',
        width: 100
    }],

    items: [{
        xtype: 'container',
        layout: 'hbox',
        // padding: 5,
        flex: 1,
        items: [{
            xtype: 'combobox',
            fieldLabel: 'Tipus usuari',
            name: 'tipo',
            labelWidth: 80,
            padding: 5,
            flex: 1,
            store: 'StoreTiposCliente',
            displayField: 'nombre',
            valueField: 'id',
            editable: false
        }, {
            xtype: 'combobox',
            name: 'tipoIdentificacion',
            fieldLabel: 'Tipus Identificació',
            // labelWidth: 80,
            padding: 5,
            flex: 1,
            allowBlank: false,
            displayField: 'nombre',
            valueField: 'id',
            queryMode: 'local',
            store: 'StoreTiposIdentificacion',
            listeners: {
                select: function (store, sel) {
                    if (sel[0].data.nombre == 'Pasaporte') {
                        this.up("form[name=formClientes]").down("textfield[name=identificacion]").vtype = '';
                    } else {
                        this.up("form[name=formClientes]").down("textfield[name=identificacion]").vtype = sel[0].data.nombre;
                    }
                }
            }
        }, {
            xtype: 'textfield',
            fieldLabel: 'Identificació',
            name: 'identificacion',
            labelWidth: 80,
            padding: 5,
            flex: 2
        }, {
            xtype: 'checkbox',
            fieldLabel: 'Ficha zona privada',
            name: 'fichaZonaPrivada',
            labelWidth: 80,
            padding: 5,
            flex: 1,
            inputValue: true
        }]
    }, {
        xtype: 'container',
        layout: 'hbox',
        // padding: 5,
        flex: 1,
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Nom',
            labelWidth: 50,
            padding: 5,
            name: 'nombre',
            flex: 1
        }, {
            xtype: 'textfield',
            fieldLabel: 'Cognoms',
            labelWidth: 80,
            padding: 5,
            name: 'apellidos',
            flex: 3
        }]
    }, {
        xtype: 'container',
        layout: 'hbox',
        items: [{
            xtype: 'fieldset',
            title: 'Correus',
            layout: 'hbox',
            flex: 1,
            // padding: '5 5 5 5',
            items: [{
                xtype: 'textfield',
                fieldLabel: 'Oficial',
                labelWidth: 50,
                padding: '0 5 0 5',
                name: 'correoOficial',
                vtype: 'email',
                readOnly : true,
                flex: 2
            }, {
                xtype: 'textfield',
                fieldLabel: 'Personal',
                labelWidth: 50,
                padding: '0 5 0 5',
                name: 'correo',
                vtype: 'email',
                flex: 2
            }, {
                xtype: 'combobox',
                name: 'correoEstadoId',
                store: 'StoreTiposDatosEstadosEnvios',
                labelWidth: 50,
                padding: '0 5 0 5',
                flex: 1,
                editable: false,
                displayField: 'nombre',
                valueField: 'id',
                allowBlank: true,
                emptyText: 'selecciona un estat'
            }]
        }, {
            xtype: 'fieldset',
            title: 'Telefons',
            layout: 'hbox',
            flex: 1,
            margin: '0 0 0 5',
            // padding: 5,
            items: [{
                xtype: 'textfield',
                fieldLabel: 'Prefix',
                labelWidth: 50,
                padding: '0 5 0 5',
                flex: 1,
                name: 'prefijoTelefono'
            }, {
                xtype: 'textfield',
                fieldLabel: 'Movil',
                name: 'movil',
                labelWidth: 50,
                padding: '0 5 0 5',
                // regex: /^[0-9]+$/,
                // regexText: 'Sols pot contenir caracters numerics',
                flex: 2
            }, {
                xtype: 'combobox',
                name: 'movilEstadoId',
                store: 'StoreTiposDatosEstadosEnvios',
                labelWidth: 50,
                padding: '0 5 0 5',
                flex: 1,
                editable: false,
                displayField: 'nombre',
                valueField: 'id',
                allowBlank: true,
                emptyText: 'selecciona un estat'
            }]
        }]
    }, {
        xtype: 'fieldset',
        title: 'Direcció Postal',
        layout: {type: 'vbox', align: 'stretch'},
        items: [{
            xtype: 'container',
            layout: {type: 'hbox'},
            // padding: 5,
            flex: 1,
            items: [{
                xtype: 'combobox',
                fieldLabel: 'Pais',
                name: 'paisPostalId',
                store: 'StorePaises',
                labelWidth: 50,
                padding: '0 0 5 0',
                flex: 1,
                editable: false,
                displayField: 'nombre',
                valueField: 'id',
                queryMode: 'local',
                allowBlank: false
            }, {
                xtype: 'combobox',
                fieldLabel: 'Provincia',
                name: 'provinciaPostalId',
                store: Ext.create('CRM.store.StoreProvincias'),
                labelWidth: 70,
                padding: '0 0 5 5',
                flex: 1,
                editable: false,
                displayField: 'nombre',
                valueField: 'id',
                queryMode: 'local',
                allowBlank: true
            }, {
                xtype: 'textfield',
                fieldLabel: 'Provincia Nom',
                name: 'provinciaPostalNombre',
                // labelWidth: 70,
                padding: '0 0 5 5',
                flex: 1,
                allowBlank: true
            }, {
                xtype: 'combobox',
                name: 'poblacionPostalId',
                fieldLabel: 'Població',
                store: Ext.create('CRM.store.StorePoblaciones'),
                labelWidth: 50,
                padding: '0 0 5 5',
                flex: 1,
                editable: false,
                displayField: 'nombre',
                valueField: 'id',
                queryMode: 'local',
                allowBlank: true
            }, {
                xtype: 'textfield',
                fieldLabel: 'Població Nom',
                name: 'poblacionPostalNombre',
                // labelWidth: 70,
                padding: '0 0 5 5',
                flex: 1,
                allowBlank: true
            }]
        }, {
            xtype: 'container',
            layout: 'hbox',
            // padding: 5,
            flex: 1,
            items: [{
                xtype: 'textfield',
                fieldLabel: 'Direcció (Calle, plaza, etc)',
                name: 'postal',
                labelWidth: 150,
                padding: 0,
                flex: 3
            }, {
                xtype: 'textfield',
                fieldLabel: 'Codi Postal',
                name: 'codigoPostal',
                labelWidth: 70,
                padding: '0 0 0 5',
                flex: 2
            }, {
                xtype: 'combobox',
                name: 'postalEstadoId',
                store: 'StoreTiposDatosEstadosEnvios',
                labelWidth: 50,
                padding: '0 0 0 5',
                flex: 2,
                editable: false,
                displayField: 'nombre',
                valueField: 'id',
                allowBlank: true,
                emptyText: 'selecciona un estat'

            }]
        }]
    }, {
        xtype: 'container',
        layout: 'hbox',
        flex: 1,
        items: [{
            xtype: 'lookupcombobox',
            appPrefix: 'crm',
            bean: 'persona',
            frame: true,
            fieldLabel: 'Usuari UJI',
            editable: false,
            allowBlank: true,
            displayField: 'nombre',
            valueField: 'id',
            store: '',
            name: 'persona',
            padding: 5,
            flex: 1
        }]
    }, {
        xtype: 'fieldset',
        title: 'Datos personales',
        layout: {type: 'vbox', align: 'stretch'},
        items: [{
            xtype: 'container',
            layout: {type: 'hbox', align: 'stretch'},
            items: [{
                xtype: 'combobox',
                fieldLabel: 'Nacionalidad',
                name: 'nacionalidadId',
                store: Ext.create('CRM.store.StorePaises', 'StoreNacionalidades'),
                labelWidth: 70,
                padding: 0,
                flex: 1,
                editable: false,
                displayField: 'nombre',
                valueField: 'id',
                queryMode: 'local',
                allowBlank: true
            }, {
                xtype: 'datefield',
                fieldLabel: 'Data Naixement',
                name: 'fechaNacimiento',
                // labelWidth: 50,
                format: 'd/m/Y',
                submitFormat: 'd/m/Y',
                padding: '0 0 0 5',
                flex: 1
            }, {
                xtype: 'combobox',
                fieldLabel: 'Sexe',
                name: 'sexo',
                store: 'StoreTipoSexo',
                labelWidth: 50,
                padding: '0 0 0 5',
                flex: 1,
                displayField: 'nombre',
                valueField: 'id',
                queryMode: 'local'
            }]
        }]
    }, {
        xtype: 'hidden',
        name: 'servicioId'
    }]
})
;