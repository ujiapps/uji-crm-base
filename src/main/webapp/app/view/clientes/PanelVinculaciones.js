Ext.define('CRM.view.clientes.PanelVinculaciones', {
    extend: 'Ext.panel.Panel',
    title: 'Items',
    alias: 'widget.panelVinculaciones',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    initComponent: function () {
        var me = this;

        me.items = [{

            xtype: 'gridClientesVinculos',
            itemId: 'gridVinculos-' + me.clienteId,
            store: Ext.create('CRM.store.StoreVinculaciones', {
                storeId: 'storeVinculaciones-' + me.clienteId,
                listeners: {
                    beforeload: function (store) {
                        store.getProxy().url = '/crm/rest/vinculacion/cliente/' + me.clienteId;
                    }
                }
            }),
            flex: 1,
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: 'storeVinculaciones-' + me.clienteId,
                dock: 'bottom',
                displayInfo: true
            }],
            listeners: {
                activate: function () {
                    var storeClienteVinculos = this.getStore();
                    storeClienteVinculos.load();
                }
            }
        }, {
            xtype: 'gridClientesVinculosInversos',
            itemId: 'gridVinculosInversos-' + me.clienteId,
            flex: 1,
            store: Ext.create('CRM.store.StoreVinculacionesInversas', {
                storeId: 'storeVinculacionesInversas-' + me.clienteId,
                listeners: {
                    beforeload: function (store) {
                        store.getProxy().url = '/crm/rest/vinculacion/inversa/cliente/' + me.clienteId;
                    }
                }
            }),
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: 'storeVinculacionesInversas-' + me.clienteId,
                dock: 'bottom',
                displayInfo: true
            }]
        }];

        me.on('activate', function () {
            var storeClienteVinculosInversos = this.down("[itemId=gridVinculosInversos-" + me.clienteId+"]").getStore();
            storeClienteVinculosInversos.load();

            var storeClienteVinculos = this.down("[itemId=gridVinculos-" + me.clienteId+"]").getStore();
            storeClienteVinculos.load();

        });

        this.callParent(me);
    }
});