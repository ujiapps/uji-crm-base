Ext.define('CRM.view.clientes.GridClientesVinculos',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridClientesVinculos',
    enableKeyEvents : true,
    selModel :
    {
        mode : 'SINGLE'
    },

    tbar : [
    {
        xtype : 'button',
        name : 'anyadirVinculo',
        text : 'Afegir vincul',
        iconCls : 'application-add',
        padding : '5 5 5 0'
    },
    {
        xtype : 'button',
        name : 'borrarVinculo',
        text : 'Esborrar vincul',
        iconCls : 'application-delete',
        padding : '5 5 5 0',
        disabled : true
    } ],
    columns : [
    {
        text : 'Nom',
        dataIndex : 'cliente2Nombre',
        flex : 3
    },
    {
        text : 'Vincul',
        dataIndex : 'tipoNombre',
        flex : 1
    },
    {
        text : 'Perfil',
        dataIndex : 'cliente2Etiqueta',
        flex : 1
    } ]
});