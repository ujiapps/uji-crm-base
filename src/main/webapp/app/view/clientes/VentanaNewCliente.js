Ext.define('CRM.view.clientes.VentanaNewCliente', {
    extend: 'Ext.Window',
    title: 'Afegir un nou client',
    width: '80%',
    alias: 'widget.ventanaNewCliente',
    modal: true,

    items: [{
        xtype: 'form',
        name: 'formNewCliente',
        frame: true,
        padding: 5,
        enableKeyEvents: true,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        fbar: [{
            xtype: 'button',
            text: 'Guardar',
            action: 'guardar-cliente'
        }, {
            xtype: 'button',
            text: 'Netejar',
            action: 'limpiar'
        }, {
            xtype: 'button',
            text: 'Cancel·lar',
            action: 'cancelar'
        }],
        items: [{
            xtype: 'form',
            padding: 5,
            frame: true,
            name: 'formNuevoCliente',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [{
                xtype: 'textfield',
                name: 'id',
                valueField: 'id',
                hidden: true
            }, {

                xtype: 'container',
                layout: 'hbox',
                items: [{
                    xtype: 'combobox',
                    name: 'servicio',
                    fieldLabel: 'Servei',
                    allowBlank: false,
                    displayField: 'nombre',
                    valueField: 'id',
                    queryMode: 'local',
                    store: 'StoreServiciosByUsuarioId',
                    padding: 5,
                    flex: 2
                }, {
                    xtype: 'combobox',
                    name: 'tipo',
                    fieldLabel: 'Tipus usuari',
                    store: 'StoreTiposCliente',
                    padding: 5,
                    flex: 1,
                    editable: false,
                    displayField: 'nombre',
                    valueField: 'id',
                    queryMode: 'local',
                    allowBlank: false
                }]
            }, {

                xtype: 'container',
                layout: 'hbox',
                flex: 1,
                items: [{
                    xtype: 'combobox',
                    name: 'tipoIdentificacion',
                    fieldLabel: 'Tipus Identificació',
                    allowBlank: false,
                    displayField: 'nombre',
                    valueField: 'id',
                    queryMode: 'local',
                    store: 'StoreTiposIdentificacion',
                    padding: 5,
                    flex: 1,
                    listeners: {
                        select: function (store, sel) {
                            if (sel[0].data.nombre == 'Pasaporte') {
                                this.up("form[name=formNuevoCliente]").down("textfield[name=identificacion]").vtype = '';
                            }
                            else {
                                this.up("form[name=formNuevoCliente]").down("textfield[name=identificacion]").vtype = sel[0].data.nombre;
                            }
                        }
                    }
                }, {
                    xtype: 'textfield',
                    name: 'identificacion',
                    fieldLabel: 'Identificació',
                    editable: true,
                    allowBlank: false,
                    padding: 5,
                    displayField: 'identificacion',
                    valueField: 'identificacion',
                    flex: 1
                }, {
                    xtype: 'combobox',
                    fieldLabel: 'Nacionalidad',
                    name: 'nacionalidadId',
                    store: Ext.create('CRM.store.StorePaises', 'storeNacionalidades'),
                    padding: 5,
                    flex: 1,
                    editable: false,
                    displayField: 'nombre',
                    valueField: 'id',
                    allowBlank: false,
                    queryMode: 'local'
                }]
            },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    flex: 1,
                    // padding: 5,
                    items: [{
                        xtype: 'textfield',
                        name: 'nombre',
                        fieldLabel: 'Nom',
                        padding: 5,
                        editable: true,
                        allowBlank: true,
                        displayField: 'nombre',
                        valueField: 'nombre',
                        // disabled: true,
                        flex: 1
                    }, {
                        xtype: 'textfield',
                        name: 'apellidos',
                        fieldLabel: 'Apellidos',
                        padding: 5,
                        editable: true,
                        allowBlank: true,
                        displayField: 'apellidos',
                        valueField: 'apellidos',
                        // disabled: true,
                        flex: 2
                    }]
                }, {
                    xtype: 'container',
                    layout: 'hbox',
                    flex: 1,
                    items: [{
                        xtype: 'textfield',
                        name: 'correo',
                        fieldLabel: 'e-mail personal',
                        padding: 5,
                        editable: true,
                        allowBlank: true,
                        displayField: 'correo',
                        valueField: 'correo',
                        vtype: 'email',
                        flex: 1
                    }, {
                        xtype: 'textfield',
                        name: 'movil',
                        fieldLabel: 'Movil',
                        padding: 5,
                        editable: true,
                        allowBlank: true,
                        displayField: 'movil',
                        valueField: 'movil',
                        regex: /^[0-9]+$/,
                        regexText: 'Sols pot contenir caracters numerics',
                        maxLength: 9,
                        maxLengthText: 'El movil no pot tindre mes de nou caracteres',
                        flex: 1
                    }]
                }, {
                    xtype: 'fieldset',
                    title: 'Direcció Postal',
                    layout: {type: 'vbox', align: 'stretch'},
                    items: [{
                        xtype: 'container',
                        layout: {type: 'hbox'},
                        flex: 1,
                        items: [{
                            xtype: 'combobox',
                            fieldLabel: 'Pais',
                            name: 'paisPostalId',
                            store: Ext.create('CRM.store.StorePaises'),
                            labelWidth: 50,
                            padding: '0 0 5 0',
                            flex: 1,
                            editable: false,
                            displayField: 'nombre',
                            valueField: 'id',
                            queryMode: 'local',
                            allowBlank: true
                        }, {
                            xtype: 'combobox',
                            fieldLabel: 'Provincia',
                            name: 'provinciaPostalId',
                            store: Ext.create('CRM.store.StoreProvincias'),
                            labelWidth: 70,
                            padding: '0 0 5 5',
                            flex: 1,
                            editable: false,
                            displayField: 'nombre',
                            valueField: 'id',
                            queryMode: 'local',
                            allowBlank: true
                        }, {
                            xtype: 'textfield',
                            fieldLabel: 'Prov. Nom',
                            name: 'provinciaPostalNombre',
                            padding: '0 0 5 5',
                            labelWidth: 60,
                            flex: 1,
                            allowBlank: true
                        }, {
                            xtype: 'combobox',
                            name: 'poblacionPostalId',
                            fieldLabel: 'Població',
                            store: Ext.create('CRM.store.StorePoblaciones'),
                            labelWidth: 50,
                            padding: '0 0 5 5',
                            flex: 1,
                            editable: false,
                            displayField: 'nombre',
                            valueField: 'id',
                            queryMode: 'local',
                            allowBlank: true
                        }, {
                            xtype: 'textfield',
                            fieldLabel: 'Pobl. Nom',
                            labelWidth: 60,
                            name: 'poblacionPostalNombre',
                            padding: '0 0 5 5',
                            flex: 1,
                            allowBlank: true
                        }]
                    }, {
                        xtype: 'container',
                        layout: 'hbox',
                        flex: 1,
                        items: [{
                            xtype: 'textfield',
                            fieldLabel: 'Adreça',
                            name: 'postal',
                            labelWidth: 50,
                            padding: 0,
                            flex: 1,
                            allowBlank: true
                        }, {
                            xtype: 'textfield',
                            fieldLabel: 'Codi Postal',
                            name: 'codigoPostal',
                            labelWidth: 70,
                            padding: '0 0 0 5',
                            flex: 1,
                            allowBlank: true
                        }]
                    }]
                }, {
                    xtype: 'fieldset',
                    title: 'Datos personales',
                    layout: {type: 'vbox', align: 'stretch'},
                    items: [{
                        xtype: 'container',
                        layout: {type: 'hbox', align: 'stretch'},
                        items: [{
                            xtype: 'datefield',
                            fieldLabel: 'Data Naixement',
                            name: 'fechaNacimiento',
                            format: 'd/m/Y',
                            submitFormat: 'd/m/Y',
                            padding: 0,
                            allowBlank: true,
                            flex: 1
                        }, {
                            xtype: 'combobox',
                            fieldLabel: 'Sexe',
                            name: 'sexoId',
                            store: 'StoreTipoSexo',
                            labelWidth: 50,
                            padding: '0 0 0 5',
                            flex: 1,
                            editable: false,
                            displayField: 'nombre',
                            valueField: 'id',
                            allowBlank: true,
                            queryMode: 'local'
                        }]
                    }]
                }]
        }]
    }]
});