Ext.define('CRM.view.clientes.TabClienteDatos',
    {
        extend: 'Ext.tab.Panel',
        alias: 'widget.tabClienteDatos',
        name: 'tabClienteDatos',
        border: 0,
        requires: ['CRM.view.clientes.GridClienteItems'],

        initComponent: function () {
            var me = this;
            me.items = [{
                xtype: 'formClientes',
                title: 'Dades Personals',
                itemId: 'formCliente-' + me.clienteId,
                tooltip: 'Dades principals de la fitxa de client'
            }, {
                xtype: 'panel',
                itemId: 'panelEstudios-' + me.clienteId,
                title: 'Estudis',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                tooltip: 'Estudis UJI y no UJI de l\'usuari',
                items: [{
                    xtype: 'gridClienteEstudiosNoUJI',
                    title: 'Estudis No UJI',
                    flex: 1,
                    itemId: 'gridEstudiosNoUJI-' + me.clienteId,
                    store: Ext.create('CRM.store.StoreClienteEstudiosNoUJI', {
                        storeId: 'storeEstudiosNoUJI-' + me.clienteId,
                        listeners: {
                            beforeload: function (store) {
                                store.getProxy().setExtraParam('clienteId', me.clienteId);
                            }
                        }
                    }),
                    dockedItems: [{
                        xtype: 'pagingtoolbar',
                        store: 'storeEstudiosNoUJI-' + me.clienteId,
                        dock: 'bottom',
                        displayInfo: true
                    }]
                }, {
                    xtype: 'gridClienteEstudiosUJI',
                    title: 'Estudis UJI',
                    flex: 1,
                    itemId: 'gridEstudiosUJI-' + me.clienteId,
                    store: Ext.create('CRM.store.StoreClienteEstudiosUJI', {
                        storeId: 'storeEstudiosUJI-' + me.clienteId,
                        listeners: {
                            beforeload: function (store) {
                                store.getProxy().setExtraParam('clienteId', me.clienteId);
                            }
                        }
                    }),
                    dockedItems: [{
                        xtype: 'pagingtoolbar',
                        store: 'storeEstudiosUJI-' + me.clienteId,
                        dock: 'bottom',
                        displayInfo: true
                    }]
                }]
            }, {
                xtype: 'gridClienteDatos',
                title: 'Altres Dades',
                tooltip: 'Dades de la fitxa de client',
                itemId: 'gridDatos-' + me.clienteId,
                store: Ext.create('CRM.store.StoreClienteDatos', {
                    storeId: 'storeDatos-' + me.clienteId,
                    listeners: {
                        beforeload: function (store) {
                            store.getProxy().setExtraParam('clienteId', me.clienteId);
                        }
                    }
                }),
                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    store: 'storeDatos-' + me.clienteId,
                    dock: 'bottom',
                    displayInfo: true
                }]
            }, {
                xtype: 'gridClienteItems',
                title: 'Suscripcions',
                tooltip: 'Suscripcions de la fitxa de client',
                itemId: 'gridClienteItems-' + me.clienteId,
                store: Ext.create('CRM.store.StoreClienteItems', {
                    storeId: 'storeClienteItems-' + me.clienteId,
                    listeners: {
                        beforeload: function (store) {
                            store.getProxy().url = '/crm/rest/clienteitem/suscripciones/'
                            store.getProxy().setExtraParam('clienteId', me.clienteId);
                        }
                    }
                }),
                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    store: 'storeClienteItems-' + me.clienteId,
                    dock: 'bottom',
                    displayInfo: true
                }]
            }];

            me.on('activate', function () {
                var storeClienteDatos = this.down("[itemId=gridDatos-" + me.clienteId + "]").getStore();
                storeClienteDatos.load();

                var storeClienteItems = this.down("[itemId=gridClienteItems-" + me.clienteId + "]").getStore();
                storeClienteItems.load();

                if (me.servicioId != 366) {
                    this.down("[itemId=panelEstudios-" + me.clienteId + "]").destroy();
                } else {
                    var storeClienteEstudiosNoUJI = this.down("[itemId=gridEstudiosNoUJI-" + me.clienteId + "]").getStore();
                    storeClienteEstudiosNoUJI.load();

                    var storeClienteEstudiosUJI = this.down("[itemId=gridEstudiosUJI-" + me.clienteId + "]").getStore();
                    storeClienteEstudiosUJI.load();
                }


            });

            this.callParent(me);
        }
    })
;