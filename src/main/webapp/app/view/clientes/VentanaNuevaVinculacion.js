Ext.define('CRM.view.clientes.VentanaNuevaVinculacion',
    {
        extend: 'Ext.Window',
        title: 'Afegir un nova vinculació',
        width: 500,
        alias: 'widget.ventanaNuevaVinculacion',
        modal: true,
        fbar: [{
            xtype: 'button',
            text: 'Guardar',
            action: 'guardar-vinculacion'
        }, {
            xtype: 'button',
            text: 'Netejar',
            action: 'limpiar-vinculacion'
        }, {
            xtype: 'button',
            text: 'Cancel·lar',
            action: 'cancelar-vinculacion'
        }],

        items: [{
            xtype: 'form',
            name: 'formNuevaVinculacion',
            frame: true,
            padding: 10,
            enableKeyEvents: true,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },

            items: [{
                xtype: 'lookupcombobox',
                appPrefix: 'crm',
                bean: 'cliente',
                name: 'comboClientesVinculaciones',
                padding: 10,
                fieldLabel: 'Cliente',
                labelWidth: 75,
                editable: false,
                allowBlank: false,
                displayField: 'nombre',
                valueField: 'id',
                anchor: '100%',
                extraFields: ['Servicio']
            }, {
                xtype: 'combobox',
                name: 'comboTipoVinculo',
                store: 'StoreTiposVinculaciones',
                displayField: 'nombre',
                valueField: 'id',
                padding: 10,
                fieldLabel: 'Tipus Vinculació',
                value: 19
            }]
        }]

    });