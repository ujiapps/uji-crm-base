Ext.define('CRM.view.clientes.GridClientes', {
    extend: 'Ext.grid.Panel',
    title: 'Llistat de Clients',
    alias: 'widget.gridClientes',
    store: 'StoreClientesGrid',
    layout: 'fit',
    clienteGeneralSeleccionado: '',
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'StoreClientesGrid',
        dock: 'bottom',
        displayInfo: true
    }],
    sortableColumns:
        true,

    disableSelection:
        true,
    features: [{
        ftype: 'grouping',
        groupHeaderTpl: '<input class="grpCheckbox" type="checkbox" id="check-'+'{[values.rows[0].data["id"]]}' +'"> Cliente: {[values.rows[0].data["identificacion"]]} </input>',
        collapsible: false
    }],

    columns: [{
        text: 'Nom',
        dataIndex: 'nombreCompleto',
        flex: 2
    }, {
        text: 'Correu',
        dataIndex: 'correo',
        flex: 1
    }, {
        text: 'Movil',
        dataIndex: 'movil',
        flex: 1
    }]
})
;
