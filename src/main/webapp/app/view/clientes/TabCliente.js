Ext.define('CRM.view.clientes.TabCliente',
    {
        extend: 'Ext.tab.Panel',
        alias: 'widget.tabCliente',
        name: 'tabCliente',
        border: 0,
        requires: ['CRM.view.clientes.PestanyaEnvios.GridClientesEnvios', 'CRM.view.clientes.PestanyaEnvios.FormEnvioCliente', 'CRM.view.clientes.PestanyaEnvios.GridClienteEnvioImagenes',
            'CRM.view.clientes.PestanyaEnvios.GridClienteEnvioArchivos'],

        initComponent: function () {
            var me = this;

            me.items = [{
                xtype: 'tabClienteDatos',
                title: 'Dades',
                name: 'tabClienteDatos',
                itemId: 'tabDatos-' + me.itemId,
                servicioId: me.servicioId,
                clienteId: me.itemId,
                tooltip: 'Dades principals de la fitxa de client'
            }, {
                xtype: 'panelVinculaciones',
                title: 'Vinculs',
                itemId: 'panelVinculaciones-' + me.itemId,
                clienteId: me.itemId,
                tooltip: 'Relacions del client amb la resta de clients i empreses'
            }, {
                xtype: 'gridSeguimientos',
                title: 'Seguiments',
                tooltip: 'Registra els seguiments (trucades telefòniques, visites, enviaments) que s\'han realitzat amb el client',
                itemId: 'gridSeguimientos-' + me.itemId,
                clienteId: me.itemId,
                store: Ext.create('CRM.store.StoreSeguimientos', {
                    storeId: 'storeSeguimientos-' + me.itemId,
                    listeners: {
                        beforeload: function (store) {
                            store.getProxy().setExtraParam('clienteId', me.itemId);
                        }
                    }
                }),
                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    store: 'storeSeguimientos-' + me.itemId,
                    dock: 'bottom',
                    displayInfo: true
                }]
            }, {
                xtype: 'gridClientesEtiquetas',
                title: 'Etiquetes',
                tooltip: 'Etiquetes que té associat el client per classificar',
                itemId: 'gridEtiquetas-' + me.itemId,
                clienteId: me.itemId,
                store: Ext.create('CRM.store.StoreClienteEtiquetas', {
                    storeId: 'storeEtiquetas-' + me.itemId,
                    listeners: {
                        beforeload: function (store) {
                            store.getProxy().setExtraParam('clienteId', me.itemId);
                        }
                    }
                }),
                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    store: 'storeEtiquetas-' + me.itemId,
                    dock: 'bottom',
                    displayInfo: true
                }]
            }, {
                xtype: 'panelClientesCampanyas',
                title: 'Campanyes Clients',
                itemClienteId: me.itemId,
                tooltip: 'Campanyes en què està apuntat el client i el seu estat'
            }, {
                xtype: 'container',
                title: 'Enviaments',
                name: 'enviaments' + me.itemId,
                tooltip: 'Històric d\'enviaments que s\'han fet al client, es poden realitzar enviaments puntuals',
                layout: 'border',
                items: [{
                    xtype: 'gridClientesEnvios',
                    name: 'gridClientesEnvios-' + me.itemId,
                    region: 'north',
                    flex: 1,
                    itemId: 'gridEnvios-' + me.itemId,
                    clienteId: me.itemId,
                    store: Ext.create('CRM.store.StoreClienteEnvios', {
                        storeId: 'storeEnvios-' + me.itemId,
                        listeners: {
                            beforeload: function (store) {
                                store.getProxy().url = '/crm/rest/enviocliente/' + me.itemId;
                            }
                        }
                    }),
                    dockedItems: [{
                        xtype: 'pagingtoolbar',
                        store: 'storeEnvios-' + me.itemId,
                        dock: 'bottom',
                        displayInfo: true
                    }],
                    listeners: {
                        select: function (grid, correo) {
                            this.up().down("[name=tabpanelEnviocliente" + me.itemId + "]").setDisabled(false);
                            this.up().down("[name=tabpanelEnviocliente" + me.itemId + "]").down("[name=gridClienteEnvioImagenes]").envioId = correo.data.envioId;
                            this.up().down("[name=tabpanelEnviocliente" + me.itemId + "]").down("[name=gridClienteEnvioArchivos]").envioId = correo.data.envioId;

                            var cliente = this.up("[name=tabsServiciosCliente]").getActiveTab().cliente,
                                formulario = this.up("[name=enviaments" + me.itemId + "]").down("[name=formEnvioCliente" + me.itemId + "]"),
                                emails = Ext.create('Ext.data.Store', {
                                    fields: ['correo'],
                                    data: [
                                        {"correo": cliente.data.correo},
                                        {"correo": cliente.data.correoOficial}
                                    ]
                                });
                            formulario.down("combobox[name=comboMail]").store = emails;

                            formulario.down('combobox[name=comboMail]').setValue(correo.data.para);
                            formulario.down('textfield[name=asunto]').setValue(correo.data.asunto);
                            formulario.down('textfield[name=responder]').setValue(correo.data.responder);
                            formulario.down('textfield[name=desde]').setValue(correo.data.desde);

                            var txt = formulario.down('[name=cuerpo]');

                            if (txt != null) {
                                txt.destroy();
                            }

                            var config = {
                                height: '100%',
                                width: '100%',
                                name: 'cuerpo',
                            };
                            var editorNuevo = new Ext.ux.CKEditor(config);
                            editorNuevo.setValue(correo.data.cuerpo);
                            formulario.down('[name=editor]').add(editorNuevo);
                            editorNuevo.setUrl('/envioimagen/visualizar/' + correo.data.envioId);
                            this.up().down("[name=tabpanelEnviocliente" + me.itemId + "]").down("[name=gridClienteEnvioImagenes]").getStore().getProxy().url = '/crm/rest/envioimagen/' + correo.data.envioId;
                            this.up().down("[name=tabpanelEnviocliente" + me.itemId + "]").down("[name=gridClienteEnvioImagenes]").getStore().load();

                            this.up().down("[name=tabpanelEnviocliente" + me.itemId + "]").down("[name=gridClienteEnvioArchivos]").getStore().getProxy().url = '/crm/rest/envio/' + correo.data.envioId + '/adjunto';
                            this.up().down("[name=tabpanelEnviocliente" + me.itemId + "]").down("[name=gridClienteEnvioArchivos]").getStore().load();
                        }
                    }
                }, {
                    xtype: 'tabpanel',
                    name: 'tabpanelEnviocliente' + me.itemId,
                    disabled: true,
                    flex: 2,
                    region: 'center',
                    items: [{
                        xtype: 'formEnvioCliente',
                        title: 'Envio',
                        name: 'formEnvioCliente' + me.itemId
                    }, {
                        xtype: 'form',
                        name: 'formSubirImagenes',
                        title: 'Imagenes',
                        items: [{
                            xtype: 'gridClienteEnvioImagenes',
                            name: 'gridClienteEnvioImagenes',
                            // flex: 1,
                            listeners: {
                                select: function () {
                                    this.down("button[name=botonBorrarImagen]").setDisabled(false);
                                }
                            }
                        }]
                    }, {
                        xtype: 'form',
                        name: 'formSubirArchivos',
                        title: 'Enlace a archivos',
                        items: [{
                            xtype: 'gridClienteEnvioArchivos',
                            name: 'gridClienteEnvioArchivos',
                            // flex: 1,
                            listeners: {
                                select: function () {
                                    this.down("button[name=botonBorrarArchivo]").setDisabled(false);
                                }
                            }
                        }]
                    }]
                }],
                listeners: {
                    activate: function () {
                        this.down("grid[name=gridClientesEnvios-" + me.itemId + "]").getStore().load();
                        this.down("[name=tabpanelEnviocliente" + me.itemId + "]").setDisabled(true);
                    }
                }
            }, {
                xtype: 'gridClientesCartas',
                title: 'Cartes',
                tooltip: 'Històric de cartes que s\'han fet al client',
                itemId: 'gridCartas-' + me.itemId,
                clienteId: me.itemId,
                store: Ext.create('CRM.store.StoreClienteCartas', {
                    storeId: 'storeCartas-' + me.itemId,
                    listeners: {
                        beforeload: function (store) {
                            store.getProxy().url = '/crm/rest/clientecarta/cliente';
                            store.getProxy().setExtraParam('clienteId', me.itemId);
                        }
                    }
                }),
                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    store: 'storeCartas-' + me.itemId,
                    dock: 'bottom',
                    displayInfo: true
                }],
            }, {
                xtype: 'gridClienteMovimientos',
                title: 'Moviments',
                name: 'gridMovimientosCliente',
                tooltip: 'Històric de canvis del client',
                itemId: 'gridMovimientos-' + me.itemId,
                clienteId: me.itemId,
                store: Ext.create('CRM.store.StoreMovimientos', {
                    storeId: 'storeMovimientos-' + me.itemId,
                    listeners: {
                        beforeload: function (store) {
                            store.getProxy().url = '/crm/rest/movimiento/cliente/' + me.itemId;
                        }
                    }
                }),
                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    store: 'storeMovimientos-' + me.itemId,
                    dock: 'bottom',
                    displayInfo: true
                }],
            }];

            this.callParent(me);
        },


        eliminarCliente: function () {
            this.destroy();
        }

    })
;