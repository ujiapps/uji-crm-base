Ext.define('CRM.view.clientes.GridClienteEstudios', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridClienteEstudios',

    store: 'StoreClienteEstudios',

    columns: [{
        text: 'Estudio',
        dataIndex: 'estudio',
        flex: 2
    }, {
        text: 'Curs Inici',
        dataIndex: 'cursoAcaIni',
        align: 'center',
        width: 90
    }, {
        text: 'Curs Fi',
        dataIndex: 'cursoAcaFin',
        align: 'center',
        width: 90
    }, {
        text: 'Fecha Titulo',
        dataIndex: 'fechaTitulo',
        xtype: 'datecolumn',
        width: 90,
        format: 'd/m/Y',
        align: 'center'
    }],

    rellenaGridClienteEstudios: function (personaId) {

        var storeEstudios = this.getStore();
        if (personaId) {
            storeEstudios.load({
                url: '/crm/rest/estudio/persona/' + personaId
            });
        }
        else {
            storeEstudios.loadData([], false);
        }
    }
});