Ext.define('CRM.view.clientes.PanelClientes',
    {
        extend: 'Ext.panel.Panel',
        title: 'Clients',
        requires: ['CRM.view.clientes.TabServicioPanel'],
        alias: 'widget.panelClientes',
        closable: true,
        layout: 'border',

        tbar: [{
            xtype: 'button',
            name: 'anyadirCliente',
            text: 'Afegir',
            iconCls: 'application-add'
        }, {
            xtype: 'button',
            name: 'busquedaCliente',
            text: 'Buscar',
            icon: window.location.protocol + '//static.uji.es/js/extjs/uji-commons-extjs/img/find.png'
        }],
        items: [{
            xtype: 'tabsServiciosCliente',
            name: 'tabsServiciosCliente',
            flex: 1,
            region: 'center',
            disabled: true
        }],


    });