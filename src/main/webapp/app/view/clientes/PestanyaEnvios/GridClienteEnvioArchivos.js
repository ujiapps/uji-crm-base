Ext.define('CRM.view.clientes.PestanyaEnvios.GridClienteEnvioArchivos',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridClienteEnvioArchivos',
        store: Ext.create('CRM.store.StoreEnviosAdjuntos', {id: 'StoreClienteEnviosArchivos'}),
        sortableColumns: true,
        autoScroll: true,
        // hideHeaders: true,
        envioId: '',
        border: 0,
        defaults: {
            border: 0
        },
        viewConfig: {
            enableTextSelection: true
        },
        selModel: {
            mode: 'SINGLE'
        },

        tbar: [{
            xtype: 'filefield',
            fieldLabel: 'Archivo',
            labelWidth: 50,
            name: 'anyadirArchivo',
            buttonConfig: {
                text: 'Sel·lecciona...',
                disabled: false
            },

            listeners: {
                change: function () {
                    this.up("grid[name=gridClienteEnvioArchivos]").down("button[name=botonAnyadirArchivo]").setDisabled(false);
                    this.up("grid[name=gridClienteEnvioArchivos]").down("button[name=botonBorrarArchivo]").setDisabled(true);
                }
            },
            flex: 5
        }, {
            xtype: 'button',
            name: 'botonAnyadirArchivo',
            iconCls: 'accept',
            disabled: true,
            // flex: 0.5,
            listeners: {
                click: function () {
                    var envioId = this.up("grid[name=gridClienteEnvioArchivos]").envioId,
                        formArchivos = this.up("form[name=formSubirArchivos]");

                    if (formArchivos.getForm().isValid()) {
                        formArchivos.submit(
                            {
                                url: '/crm/rest/envio/' + envioId + '/fichero/',
                                scope: this,
                                success: function () {
                                    this.up("grid[name=gridClienteEnvioArchivos]").down("button[name=botonAnyadirArchivo]").setDisabled(true);
                                    this.up("grid[name=gridClienteEnvioArchivos]").down("button[name=botonBorrarArchivo]").setDisabled(true);
                                    this.up("grid[name=gridClienteEnvioArchivos]").getStore().load();
                                },
                                failure: function (x, resp) {
                                    Ext.Msg.alert('Insercció de adjunts', '<b>' + resp.result.message + '</b>');
                                }
                            });
                    } else {
                        Ext.Msg.alert('Insercció de adjunts', '<b>Error desconegut</b>');
                    }
                }
            }
        },
            {
                xtype: 'button',
                name: 'botonBorrarArchivo',
                iconCls: 'application-delete',
                disabled: true,
                listeners: {
                    click: function () {
                        var grid = this.up("grid[name=gridClienteEnvioArchivos]"),
                            selection = grid.getSelectionModel().getSelection(),
                            botonBorrar = grid.down("button[name=botonBorrarArchivo]");

                        Ext.Msg.confirm('Eliminar', 'El registre s\'eliminarà. Estàs segur de voler continuar?', function (btn) {
                            if (btn == 'yes') {
                                grid.setLoading(true);
                                Ext.Ajax.request({
                                    method: 'DELETE',
                                    url: '/crm/rest/envio/adjunto/' + selection[0].data.id,
                                    success: function () {
                                        botonBorrar.setDisabled(true);
                                        grid.getStore().load({
                                            callback: function () {
                                                grid.setLoading(false);
                                            }
                                        });

                                    },
                                    failure: function () {
                                        Ext.Msg.alert('Esborrat de fitxers', '<b>El fitxer no ha pogut ser borrat correctament</b>');
                                        grid.setLoading(false);
                                    }
                                })
                            }
                        });
                    }
                }
            }],

        columns: [
            {
                text: 'id',
                dataIndex: 'id',
                hidden: true
            },
            {
                dataIndex: 'nombreFichero',
                text: 'Nombre archivo',
                flex: 1
            },
            {
                dataIndex: 'referencia',
                text: 'Link',
                flex: 1,
                renderer: function (value) {
                    return 'https://ujiapps.uji.es/ade/rest/storage/' + value;
                }

            }]

    });
