function formatDate(value) {
    return value ? Ext.Date.dateFormat(value, 'd/m/Y') : '';
}

Ext.define('CRM.view.clientes.PestanyaEnvios.GridClientesEnvios',
    {
        extend: 'Ext.grid.Panel',
        title: 'Enviaments',
        alias: 'widget.gridClientesEnvios',
        layout: 'fit',
        // sortableColumns: true,
        collapsible: true,
        selModel: {
            mode: 'SINGLE'
        },
        tbar: [{
            xtype: 'button',
            text: 'Crear correu nou',
            name: 'crearEnvioNuevo',
            iconCls: 'application-add',
            listeners: {
                click: function () {

                    var cliente = this.up("[name=tabsServiciosCliente]").getActiveTab().cliente,
                        formulario = this.up("[name=enviaments" + this.up("grid").clienteId + "]").down("[name=formEnvioCliente" + this.up("grid").clienteId + "]"),
                        emails = Ext.create('Ext.data.Store', {
                            fields: ['correo'],
                            data: [
                                {"correo": cliente.data.correo},
                                {"correo": cliente.data.correoOficial}
                            ]
                        });

                    formulario.down("combobox[name=comboMail]").store = emails;
                    formulario.down('combobox[name=comboMail]').setValue('');
                    formulario.down('textfield[name=asunto]').setValue('');
                    formulario.down('textfield[name=responder]').setValue('');
                    formulario.down('textfield[name=desde]').setValue('');

                    var txt = formulario.down('[name=cuerpo]');

                    if (txt != null) {
                        txt.destroy();
                    }

                    var config = {
                        height: '100%',
                        width: '100%',
                        name: 'cuerpo',
                    };
                    var editorNuevo = new Ext.ux.CKEditor(config);
                    editorNuevo.setValue('');
                    formulario.down('[name=editor]').add(editorNuevo);

                    this.up("[name=enviaments" + this.up("grid").clienteId + "]").down("[name=tabpanelEnviocliente" + this.up("grid").clienteId + "]").setDisabled(false);
                }
            }
        }, {
            xtype: 'button',
            name: 'borrarEnvio',
            text: 'Esborrar',
            iconCls: 'application-delete',
            // disabled: true,
            listeners: {
                click: function () {
                    // var ref = this;
                    var grid = this.up("grid");
                    var store = grid.getStore();
                    var selection = grid.getSelectionModel().getSelection();
                    var envioId = (selection.length > 0) ? selection[0].data.id : null;
                    var envio = store.findRecord('id', envioId);

                    if (envio) {
                        Ext.Msg.confirm('Esborrar Tipus de dada', 'Esteu segur/a de voler esborrar el registre sel·leccionat?', function (btn) {
                            if (btn == 'yes') {
                                Ext.Ajax.request({
                                    method: 'DELETE',
                                    url: '/crm/rest/enviocliente/' + selection[0].data.id,
                                    success: function () {
                                        grid.getStore().load({
                                            callback: function () {
                                            }
                                        });

                                    },
                                    failure: function () {
                                        Ext.Msg.alert('Esborrat de enviaments', '<b>L\'enviament no ha pogut ser borrat correctament</b>');
                                    }
                                })
                            }
                        });
                    }
                }
            }
        }],
        columns: [{
            text: 'Data',
            dataIndex: 'fechaEnvio',
            format: 'd/m/Y',
            renderer: formatDate,
            // sortable : true,
            flex: 0.5
        }, {
            text: 'Direcció de envío',
            dataIndex: 'para',
            // sortable : false,
            flex: 1
        }, {
            text: 'Nom',
            dataIndex: 'nombre',
            // sortable : false,
            flex: 1
        }, {
            text: 'Asumpte',
            dataIndex: 'asunto',
            // sortable : false,
            flex: 1
        }, {
            text: 'Tipus',
            dataIndex: 'envioTipoNombre',
            // sortable : false,
            flex: 1
        }, {
            text: 'Estado',
            dataIndex: 'estado',
            // sortable : false,
            flex: 1
        }],

        listeners: {
            activate: function () {
                var storeClienteEnvios = this.getStore();
                storeClienteEnvios.load();
            }
        }

    });
