Ext.define('CRM.view.clientes.PestanyaEnvios.FormEnvioCliente', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.formEnvioCliente',
    modal: true,
    layout: {
        type: 'border',
    },
    fbar: [{
        xtype: 'button',
        text: 'Enviar',
        name: 'enviarEnvioCliente',
        listeners: {
            click: function () {
                var form = this.up("form"),
                    clienteId = this.up("[name=tabsServiciosCliente]").getActiveTab().cliente.data.id,
                    gridEnvios = form.up("[name=enviaments" + clienteId + "]").down("grid[name=gridClientesEnvios-" + clienteId + "]");

                if (form.getForm().isValid()) {
                    form.submit({
                        url: '/crm/rest/envio/cliente/' + clienteId,
                        params: {
                            cuerpoForm: form.down('[name=cuerpo]').getValue()
                        },
                        success: function (opt, post) {
                            gridEnvios.getStore().load({
                                callback: function () {
                                    var elem = gridEnvios.getStore().findRecord('envioId', Ext.decode(post.response.responseText).data.id);
                                    gridEnvios.getSelectionModel().select(elem);
                                }
                            });

                            // gridEnvios.getSelectionModel().deselectAll();
                        }
                    });
                    // Ext.Ajax.request(
                    //     {
                    //         url: '/crm/rest/envio/cliente/' + clienteId,
                    //         method: 'POST',
                    //         params: {
                    //             nombre: 'Envio Manual',
                    //             asunto: form.down('textfield[name=asunto]').getValue(),
                    //             responder: form.down('textfield[name=responder]').getValue(),
                    //             desde: form.down('textfield[name=desde]').getValue(),
                    //             envioTipoId: CRM.model.enums.TiposEnvio.EMAIL,
                    //             cuerpo: form.down('[name=cuerpo]').getValue(),
                    //             cliente: clienteId,
                    //             comboMail: form.down('combobox[name=comboMail]').getValue()
                    //         },
                    //         scope: this,
                    //         callback: function () {
                    //         }
                    //     });
                }
            }
        }
    }, {
        xtype: 'button',
        text: 'Netejar',
        name: 'limpiarEnvioCliente',
        listeners: {
            click: function () {
                var envio = Ext.create('CRM.model.Envio'),
                    form = this.up("form");
                form.loadRecord(envio);
                form.down('combobox[name=comboMail]').setValue('');
            }
        }
    }],
    // padding: 10,
    border: 0,
    items: [{
        xtype: 'container',
        defaults: {
            padding: '5 5 0 5'
        },
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        region: 'north',
        items: [{
            xtype: 'combobox',
            fieldLabel: 'Correu',
            name: 'comboMail',
            flex: 1,
            queryMode: 'local',
            displayField: 'correo',
            valueField: 'correo',
            store: ''
        }, {
            xtype: 'textfield',
            fieldLabel: 'Asumpte',
            name: 'asunto',
            flex: 1
        }, {
            xtype: 'textfield',
            fieldLabel: 'Responder',
            name: 'responder',
            flex: 1
        }, {
            xtype: 'textfield',
            fieldLabel: 'Desde',
            name: 'desde',
            flex: 1
        }, {
            xtype: 'filefield',
            fieldLabel: 'Adjunto',
            name: 'anyadirArchivo',
            buttonConfig: {
                text: 'Sel·lecciona...',
                disabled: false
            }
        }]
    }, {
        name: 'editor',
        autoScroll: true,
        flex: 1,
        region: 'center'
    }]
})
;