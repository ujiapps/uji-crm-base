Ext.define('CRM.view.clientes.GridClientesEtiquetas', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridClientesEtiquetas',
    selModel: {
        mode: 'SINGLE'
    },

    initComponent: function () {
        var me = this;

        me.tbar = [{
            // tbar: [{
            xtype: 'combobox',
            store: Ext.create('CRM.store.StoreEtiquetas', {storeId: 'storeComboEtiquetas' + me.clienteId}),
            fieldLabel: 'Etiqueta',
            labelWidth: 60,
            emptyText: 'Escriu la etiqueta...',
            displayField: 'nombre',
            valueField: 'id',
            name: 'comboAnyadirEtiqueta' + me.clienteId,
            queryMode: 'local',
            editable: true,
            hideTrigger: true,
            padding: '5 5 5 0',
            enableKeyEvents: true,
            itemId: 'comboEtiquetas' + me.clienteId
        }, {
            xtype: 'button',
            name: 'anyadir',
            text: 'Afegir',
            iconCls: 'accept',
            padding: '5 5 5 0'
        }, {
            xtype: 'button',
            name: 'borrar',
            text: 'Esborrar etiqueta',
            iconCls: 'application-delete',
            padding: '5 5 5 0',
            disabled: true
        }];

        me.columns = [{
            // columns: [{
            text: 'Etiqueta',
            dataIndex: 'etiquetaNombre',
            flex: 1
        }, {
            text: 'Id',
            dataIndex: 'id',
            hidden: true
        }];
        // listeners: {
// me.listeners = [{
        me.on('activate', function () {
            var storeClienteEtiquetas = this.getStore();
            storeClienteEtiquetas.load();
        });

        this.callParent(me);
    }
});