Ext.define('CRM.view.clientes.GridClienteCursos', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridClienteCursos',
    // frame: true,
    // labelAlign: 'top',
    // autoScroll: true,
    store: 'StoreClienteCursos',

    columns: [{
        text: 'Curs',
        dataIndex: 'nombre',
        flex: 4
    }, {
        text: 'Tipus',
        dataIndex: 'tipoNombre',
        flex: 4
    }, {
        text: 'Data Inscripció',
        dataIndex: 'fechaInscripcion',
        renderer: Ext.util.Format.dateRenderer('d/m/Y'),
        flex: 2
    }, {
        text: 'Import',
        dataIndex: 'importe',
        flex: 1
    }, {
        text: 'Qualificació',
        dataIndex: 'calificacion',
        flex: 1
    }, {
        text: 'Aprofitat',
        dataIndex: 'aprovechamiento',
        xtype: 'booleancolumn',
        trueText: 'Sí',
        falseText: 'No',
        flex: 1
    }, {
        text: 'An·nulat',
        dataIndex: 'anulaInscripcion',
        xtype: 'booleancolumn',
        trueText: 'Sí',
        falseText: 'No',
        flex: 1
    }, {
        text: 'Espera',
        dataIndex: 'espera',
        xtype: 'booleancolumn',
        trueText: 'Sí',
        falseText: 'No',
        flex: 1
    }, {
        text: 'Confirmació',
        dataIndex: 'confirmacion',
        xtype: 'booleancolumn',
        trueText: 'Sí',
        falseText: 'No',
        flex: 1
    }, {
        text: 'Asistencia',
        dataIndex: 'asistencia',
        xtype: 'booleancolumn',
        trueText: 'Sí',
        falseText: 'No',
        flex: 1
    }],

    rellenaGridClienteCursos: function (personaId) {

        var storeCursos = this.getStore();
        if (personaId) {
            storeCursos.load({
                url: '/crm/rest/cursoinscripcion/persona/' + personaId
            });
        }
        else {
            storeCursos.loadData([], false);
        }
    }
});