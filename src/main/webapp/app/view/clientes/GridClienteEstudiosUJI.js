Ext.define('CRM.view.clientes.GridClienteEstudiosUJI', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridClienteEstudiosUJI',
    columns: [{
        header: 'Tipo Estudio',
        dataIndex: 'tipoEstudio',
        xtype: 'combocolumn',
        combo: {
            xtype: 'combobox',
            editable: false,
            store: Ext.create('CRM.store.StoreTiposEstudiosUJI'),
            displayField: 'nombre',
            valueField: 'id',
            queryMode: 'local'
        },
        flex: 1
    }, {
        text: 'Nom Estudi',
        dataIndex: 'nombre',
        flex: 3
    }, {
        text: 'Any finalització',
        dataIndex: 'anyoFinalizacion',
        flex: 0.5
    },{
        text: 'Beca',
        dataIndex: 'tipoBeca',
        flex: 1
    }]
});