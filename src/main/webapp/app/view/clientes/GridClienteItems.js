Ext.define('CRM.view.clientes.GridClienteItems', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridClienteItems',
    requires: ['Ext.grid.plugin.RowEditing'],

    selModel: {
        mode: 'SINGLE'
    },

    plugins: [{
        ptype: 'rowediting',
        clicksToEdit: 2,
    }],

    listeners: {
        selectionchange: function () {
            this.setEstadoComponentes();
        },
        edit: function () {
            this.guardaClienteItem();
        }
    },

    viewConfig: {
        getRowClass: function (row) {
            if (row.get('tipo') == 'AUTO') {
                return 'autoCls';
            }
        }
    },

    tbar: [{
        xtype: 'button',
        name: 'anyadirClienteItem',
        text: 'Afegir',
        iconCls: 'application-add'
    }, {
        xtype: 'button',
        name: 'borrarClienteItem',
        text: 'Esborrar',
        iconCls: 'application-delete',
        disabled: true,
        listeners: {
            click: function () {
                var grid = this.up("grid"),
                    store = grid.getStore(),
                    selection = grid.getSelectionModel().getSelection(),
                    clienteItemId = (selection.length > 0) ? selection[0].data.id : null,
                    clienteItem = store.findRecord('id', clienteItemId);

                if (clienteItem) {
                    Ext.Msg.confirm('Esborrar Tipus de dada', 'Esteu segur/a de voler esborrar el registre sel·leccionat?', function (btn) {
                        if (btn == 'yes') {
                            Ext.Ajax.request({
                                url: '/crm/rest/clienteitem/' + clienteItemId,
                                method: 'DELETE',
                                scope: this,
                                success: function () {
                                    grid.setLoading(true);
                                    grid.setEstadoComponentes();
                                    store.load({
                                        callback: function () {
                                            grid.setLoading(false);
                                        }
                                    });
                                },
                                failure: function () {
                                    Ext.Msg.alert('Esborrar', 'No s`ha pogut esborrar');
                                    grid.setEstadoComponentes();
                                }
                            });
                        }
                    });
                }
            }
        }
    }],

    columns: [{
        text: 'Grupo',
        dataIndex: 'grupoNombre',
        flex: 1
    }, {
        text: 'Item',
        dataIndex: 'nombre',
        flex: 1
    }, {
        text: 'Accepta envío correo',
        dataIndex: 'correo',
        xtype: 'booleancolumn',
        trueText: 'Sí',
        falseText: 'No',
        flex: 1,
        editor: {
            xtype: 'checkbox'
        }
    }, {
        text: 'Accepta envío postal',
        dataIndex: 'postal',
        xtype: 'booleancolumn',
        trueText: 'Sí',
        falseText: 'No',
        flex: 1,
        editor: {
            xtype: 'checkbox'
        }
    }],

    setEstadoComponentes: function () {

        var tieneSeleccionGrid = this.getSelectionModel().hasSelection(),
            esSuscripcionManual = false;

        if (tieneSeleccionGrid) {
            esSuscripcionManual = this.getSelectionModel().getSelection()[0].data.tipo == "MANUAL";
        }

        this.down("button[name=borrarClienteItem]").setDisabled(!(tieneSeleccionGrid && esSuscripcionManual));
    },

    guardaClienteItem: function () {
        var store = this.getStore(),
            grid = this,
            data = grid.getSelectionModel().getSelection()[0].data;

        if (data.tipo == 'MANUAL') {
            Ext.Ajax.request({
                url: '/crm/rest/clienteitem/' + data.id,
                method: 'PUT',
                params: {
                    postal: (data.postal) ? 1 : 0,
                    correo: (data.correo) ? 1 : 0
                },
                scope: this,
                success: function () {
                    grid.setLoading(true);
                    grid.setEstadoComponentes();
                    store.load({
                        callback: function () {
                            grid.setLoading(false);
                        }
                    });
                },
                failure: function () {
                    Ext.Msg.alert('Esborrar', 'No s`ha pogut esborrar');
                    grid.setEstadoComponentes();
                }
            });
        }
        else {
            Ext.Ajax.request({
                url: '/crm/rest/clienteitem/',
                method: 'POST',
                params: {
                    itemId : data.itemId,
                    postal: (data.postal) ? 1 : 0,
                    correo: (data.correo) ? 1 : 0,
                    clienteId : data.clienteId
                },
                scope: this,
                success: function () {
                    grid.setLoading(true);
                    grid.setEstadoComponentes();
                    store.load({
                        callback: function () {
                            grid.setLoading(false);
                        }
                    });
                },
                failure: function () {
                    Ext.Msg.alert('Esborrar', 'No s`ha pogut esborrar');
                    grid.setEstadoComponentes();
                }
            });
        }

        // store.sync(
        //     {
        //         success: function () {
        //             this.setEstadoComponentes();
        //         },
        //         failure: function () {
        //             Ext.Msg.alert('Dades', 'S\'ha produït un error en modificar la dada');
        //             store.rejectChanges();
        //             this.setEstadoComponentes();
        //         },
        //         scope: this
        //     });
    }
});