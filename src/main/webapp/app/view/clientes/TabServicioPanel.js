Ext.define('CRM.view.clientes.TabServicioPanel',
    {
        extend: 'Ext.tab.Panel',
        name: 'tabsServiciosCliente',
        alias: 'widget.tabsServiciosCliente',
        store: Ext.create('CRM.store.StoreClientes'),
        clienteGeneralId : '',

        creaPestanyasServicios: function (clientes, clienteGeneral) {
            for (var i in clientes) {

                // var servicio = this.getStoreServiciosStore().findRecord('id', clientes[i].data.servicioId);
                var tabCliente = Ext.create('CRM.view.clientes.TabCliente', {
                    title: clientes[i].data.servicioNombre, //servicio
                    servicioId : clientes[i].data.servicioId,
                    itemId: clientes[i].data.id,
                    cliente: clientes[i]
                });
                this.add(tabCliente);
                this.cargaPestanyaFormDatos(clienteGeneral, tabCliente.cliente);
            }
        },

        cargaPestanyaFormDatos: function (clienteGeneral, cliente) {
            var ref = this;

            if (cliente.data.paisPostalId && cliente.data.paisPostalId != null) {
                ref.down("[itemId=" + cliente.data.id + "]").down("[itemId=tabDatos-" + cliente.data.id + "]").down("[itemId=formCliente-" + cliente.data.id + "]").down("combobox[name=provinciaPostalId]").getStore().reload();
                if (cliente.data.provinciaPostalId != null) {
                    ref.down("[itemId=" + cliente.data.id + "]").down("[itemId=tabDatos-" + cliente.data.id + "]").down("[itemId=formCliente-" + cliente.data.id + "]").down("combobox[name=poblacionPostalId]").getStore().load({
                        url: 'rest/poblacion/' + cliente.data.provinciaPostalId,
                        callback: function () {
                            ref.down("[itemId=" + cliente.data.id + "]").down("[itemId=tabDatos-" + cliente.data.id + "]").down("[itemId=formCliente-" + cliente.data.id + "]").loadRecord(cliente);
                        }
                    });
                } else {
                    ref.down("[itemId=" + cliente.data.id + "]").down("[itemId=tabDatos-" + cliente.data.id + "]").down("[itemId=formCliente-" + cliente.data.id + "]").loadRecord(cliente);
                }
            } else {
                this.down("[itemId=" + cliente.data.id + "]").down("[itemId=tabDatos-" + cliente.data.id + "]").down("[itemId=formCliente-" + cliente.data.id + "]").loadRecord(cliente);
            }
            var persona = Ext.create('CRM.model.TipoDato', {
                id: cliente.data.personaId,
                nombre: cliente.data.personaNombre
            });
            this.down("[itemId=" + cliente.data.id + "]").down("[itemId=tabDatos-" + cliente.data.id + "]").down("[itemId=formCliente-" + cliente.data.id + "]").down("[name=persona]").setValue(persona);
            this.down("[itemId=" + cliente.data.id + "]").down("[itemId=tabDatos-" + cliente.data.id + "]").down("[itemId=formCliente-" + cliente.data.id + "]").down("[name=identificacion]").setValue(clienteGeneral.data.identificacion);
            this.down("[itemId=" + cliente.data.id + "]").down("[itemId=tabDatos-" + cliente.data.id + "]").down("[itemId=formCliente-" + cliente.data.id + "]").down("[name=tipoIdentificacion]").setValue(clienteGeneral.data.tipoIdentificacion);
        },

        creaPestanyasPersona: function (personaId) {
            var tabPagos = Ext.create('CRM.view.clientes.PestanyaPagos.PanelClientesPagos', {
                title: 'Pagos',
                tooltip: 'Rebuts asignats al client',
                name: 'panelClienteRecibos',
                listeners: {
                    activate: function () {
                        this.down("grid").rellenaPanelClientesPagos(personaId);
                    }
                }
            });
            this.add(tabPagos);

            var tabCursos = Ext.create('CRM.view.clientes.GridClienteCursos', {
                title: 'Cursos',
                tooltip: 'Cursos als quals s\'ha apuntat el client',
                name: 'gridClienteCursos'
            });
            this.add(tabCursos);

            var tabVinculacionesUJI = Ext.create('CRM.view.clientes.GridClientesVinculacionesUJI', {
                title: 'Vinculacions UJI',
                tooltip: 'Relacions que té el client amb la UJI, aquesta informació no es pot modificar',
                name: 'gridClienteVinculacionesUJI'
            });
            this.add(tabVinculacionesUJI);

            var tabEstudios = Ext.create('CRM.view.clientes.GridClienteEstudios', {
                title: 'Estudis UJI',
                tooltip: 'Estudis cursats a la UJI',
                name: 'gridClienteEstudios'
            });
            this.add(tabEstudios);

            var tabCuentas = Ext.create('CRM.view.clientes.GridClienteCuentas', {
                title: 'Cuentas',
                tooltip: 'Cuentas corporativas UJI',
                name: 'gridClienteCuentasCorporativas'
            });
            this.add(tabCuentas);

            var tabFotos = Ext.create('CRM.view.clientes.PestanyaFotos.PanelClientesFotos', {
                title: 'Fotos',
                tooltip: 'Fotos',
                name: 'panelClienteFotos'
            });
            this.add(tabFotos);
        }
    });