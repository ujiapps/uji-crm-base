Ext.define('CRM.view.clientes.GridClienteCuentas', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridClienteCuentas',
    // frame: true,
    // labelAlign: 'top',
    // autoScroll: true,
    store: 'StoreClienteCuentas',

    columns: [{
        text: 'Nombre',
        dataIndex: 'nombre',
        flex: 4
    }, {
        text: 'Data Inici',
        dataIndex: 'inicio',
        renderer: Ext.util.Format.dateRenderer('d/m/Y'),
        flex: 2
    }, {
        text: 'Data Fi',
        dataIndex: 'fin',
        renderer: Ext.util.Format.dateRenderer('d/m/Y'),
        flex: 2
    }, {
        text: 'Principal',
        dataIndex: 'principal',
        xtype: 'booleancolumn',
        trueText: 'Si',
        falseText: 'No',
        flex: 1
    }],

    rellenaGridCuentasCorporativas: function (personaId) {
        var storeCuentasCorporativas = this.getStore();
        if (personaId) {
            storeCuentasCorporativas.load({
                url: '/crm/rest/cuentacorporativa/' + personaId
            });
        }
        else {
            storeCuentasCorporativas.loadData([], false);
        }
    }
});