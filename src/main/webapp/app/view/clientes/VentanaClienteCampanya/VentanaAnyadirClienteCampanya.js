var navegar = function (panel, direction) {
    var layout = panel.getLayout();
    layout[direction]();
    Ext.getCmp('move-prev').setDisabled(!layout.getPrev());
    Ext.getCmp('move-next').setDisabled(!layout.getNext());

    Ext.getCmp('finalizar').setVisible(!layout.getNext());
};

Ext.define('CRM.view.clientes.VentanaClienteCampanya.VentanaAnyadirClienteCampanya', {
    extend: 'Ext.Window',
    title: 'Afegir un client',
    width: 600,
    maxHeight: 700,
    autoScroll: true,
    alias: 'widget.ventanaAnyadirClienteCampanya',
    requieres: ['CRM.view.clientes.VentanaClienteCampanya.PanelClienteCampanya', 'CRM.view.clientes.VentanaClienteCampanya.PanelClienteCampanyaTarifa', 'CRM.view.clientes.VentanaClienteCampanya.PanelClienteCampanyaEstadoMotivos'],
    modal: true,
    y: 40,

    listeners: {
        afterrender: function () {
            var viewSize = Ext.getBody().getViewSize();
            this.maxHeight = viewSize.height - 80;
        }
    },

    items: [{
        xtype: 'panel',

        name: 'panelPrincipalCardFormularios',
        border: false,
        layout: 'card',
        items: [],
        buttons: [{
            iconCls: 'arrow-left',
            id: 'move-prev',
            name: 'move-prev',
            text: 'volver',
            handler: function (btn) {
                navegar(btn.up("panel"), "prev");
            },
            disabled: true
        }, {
            iconCls: 'arrow-right',
            text: 'Siguiente',
            id: 'move-next',
            name: 'move-next',
            // handler: function (btn) {
            //     navegar(btn.up("panel"), "next");
            // },
            disabled: true
        }, {
            text: 'Finalitzar',
            hidden: true,
            name: 'finalizar',
            id: 'finalizar'
        }, {
            text: 'Cancel·lar',
            action: 'close'
        }]
    }
    ]

});

