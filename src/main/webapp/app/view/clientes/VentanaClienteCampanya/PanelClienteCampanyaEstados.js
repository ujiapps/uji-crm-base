Ext.define('CRM.view.clientes.VentanaClienteCampanya.PanelClienteCampanyaEstados', {
    extend: 'Ext.panel.Panel',
    autoScroll: true,
    alias: 'widget.panelClienteCampanyaEstados',
    modal: true,
    frame: true,
    padding: 10,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [{
        xtype: 'comboCampanyas',
        labelWidth: 110,
        labelAlign: 'left',
        fieldLabel: 'Campanya',
        emptyText: 'Trieu una campanya...',
        store: 'StoreCampanyasAccesoUser',
        displayField: 'nombre',
        valueField: 'id',
        name: 'comboCampanyas',
        queryMode: 'local',
        editable: false
    }, {
        xtype: 'lookupcombobox',
        labelWidth: 110,
        labelAlign: 'left',
        fieldLabel: 'Cliente',
        emptyText: 'Trieu un client...',
        appPrefix: 'crm',
        bean: 'cliente',
        name: 'comboClientes',
        editable: false,
        displayField: 'nombre',
        valueField: 'id',
        extraFields: ['Servicio']
    }, {
        xtype: 'combobox',
        labelWidth: 110,
        labelAlign: 'left',
        fieldLabel: 'Estat en el que posar al usuari',
        emptyText: 'Trieu un estat...',
        store: 'StoreTiposEstadoCampanya',
        displayField: 'nombre',
        valueField: 'id',
        name: 'estadoCampanya',
        queryMode: 'local',
        editable: false
    }],
    listeners: {
        activate: function () {
            var combo = this.down("[name=comboCampanyas]");
            var store = combo.getStore();
            store.load();
        }
    }
});