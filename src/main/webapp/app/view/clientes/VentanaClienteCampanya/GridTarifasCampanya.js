Ext.define('CRM.view.clientes.VentanaClienteCampanya.GridTarifasCampanya',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridTarifasCampanya',
        labelAlign: 'top',
        autoScroll: true,
        store: 'StoreTiposTarifasCampanya',
        border : 0,
        selModel: {
            mode: 'SINGLE'
        },

        columns: [
            {
                text: 'Nom Tarifa',
                dataIndex: 'nombre',
                flex: 1
            },
            {
                text: 'Dies caducidad',
                dataIndex: 'diasCaducidad',
                flex: 2
            },
            {
                text: 'dies vigencia',
                dataIndex: 'diasVigencia',
                flex: 0.5
            }
        ]
    });