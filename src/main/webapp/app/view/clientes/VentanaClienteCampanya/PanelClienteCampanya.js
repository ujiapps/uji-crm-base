Ext.define('CRM.view.clientes.VentanaClienteCampanya.PanelClienteCampanya',
    {
        extend: 'Ext.panel.Panel',
        autoScroll: true,
        alias: 'widget.ventanaClienteCampanya',
        name: 'ventanaClienteCampanya',
        modal: true,
        closable: false,

        items: [
            {
                xtype: 'form',
                name: 'formClienteCampanya',
                padding: 10,
                enableKeyEvents: true,
                frame: true,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: []
            }]

    });