Ext.define('CRM.view.clientes.VentanaClienteCampanya.PanelClienteCampanyaTarifa',
    {
        extend: 'Ext.panel.Panel',
        autoScroll: true,
        alias: 'widget.panelClienteCampanyaTarifa',
        name : 'panelClienteCampanyaTarifa',
        modal: true,

        items: [{
            xtype: 'gridTarifasCampanya',
            name: 'gridTarifasCampanya'
        }]

    });