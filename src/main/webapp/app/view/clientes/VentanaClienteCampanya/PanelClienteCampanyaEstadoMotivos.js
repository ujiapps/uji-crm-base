Ext.define('CRM.view.clientes.VentanaClienteCampanya.PanelClienteCampanyaEstadoMotivos',
    {
        extend: 'Ext.panel.Panel',
        autoScroll: true,
        alias: 'widget.panelClienteCampanyaEstadoMotivos',
        name: 'panelClienteCampanyaEstadoMotivos',
        modal: true,

        items: [{
            xtype: 'gridEstadoMotivosCampanya',
            name: 'gridEstadoMotivosCampanya'
        }]

    });