Ext.define('CRM.view.clientes.VentanaClienteCampanya.GridEstadoMotivosCampanya',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridEstadoMotivosCampanya',
        labelAlign: 'top',
        autoScroll: true,
        store: 'StoreTiposEstadoCampanyaMotivos',
        border: 0,
        selModel: {
            mode: 'SINGLE'
        },

        columns: [
            {
                text: 'Motiu',
                dataIndex: 'nombreCa',
                flex: 1
            }
        ]
    });