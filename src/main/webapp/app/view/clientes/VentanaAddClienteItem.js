Ext.define('CRM.view.clientes.VentanaAddClienteItem', {
    extend: 'Ext.Window',
    // extend: 'Ext.form.FormPanel',
    alias: 'widget.ventanaAddClienteItem',
    name: 'ventanaAddClienteItem',
    modal: true,
    width: 400,
    title: 'Afegir Items a Clients',
    frame: true,
    border: 0,
    padding: 10,

    fbar: [{
        xtype: 'button',
        text: 'Guardar',
        listeners: {
            click: function () {
                var form = this.up("window").down("form[name=formItem]");
                var clienteId = this.up("window").clienteId;
                if (form.getForm().isValid()) {
                    form.submit({
                        url: '/crm/rest/clienteitem/',
                        params: {
                            clienteId: clienteId
                        },
                        scope: this,
                        success: function () {
                            this.up("window").grid.getStore().load({
                                // url: '/crm/rest/clienteitem/suscripciones/'
                            });
                            this.up("window").destroy();
                        }

                    });
                }
            }
        }
    },],

    listeners: {

        show: function () {
            var form = this.down("form[name=formItem]"),
                comboPrograma = form.down("combobox[name=programaId]"),
                comboGrupo = form.down("combobox[name=grupoId]");

            comboPrograma.getStore().filterBy(function (record) {
                return comboGrupo.getStore().find("programaId", record.data.id) != -1;
            });
        }
    },

    items: [{
        xtype: 'form',
        frame: true,
        name: 'formItem',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'combobox',
            store: Ext.create('CRM.store.StoreProgramas'),
            displayField: 'nombre',
            valueField: 'id',
            name: 'programaId',
            fieldLabel: 'Programa',
            editable: false,
            allowBlank: false,
            queryMode: 'local',
            listeners: {
                select: function (combo, elem) {
                    var form = this.up();
                    form.down("combobox[name=grupoId]").setValue(null);
                    form.down("combobox[name=itemId]").setValue(null);
                    form.down("combobox[name=grupoId]").getStore().filterBy(function (record) {
                        return record.data.programaId == elem[0].data.id;
                    });
                    form.down("combobox[name=itemId]").getStore().loadData([], false);
                },
                change: function () {
                    this.up("window").setEstadoComponentes();
                }
            }
        }, {
            xtype: 'combobox',
            store: Ext.create('CRM.store.StoreGrupos', {}),
            displayField: 'nombre',
            valueField: 'id',
            name: 'grupoId',
            fieldLabel: 'Grup',
            disabled: true,
            editable: false,
            allowBlank: false,
            queryMode: 'local',
            listeners: {
                select: function (combo, elem) {
                    var form = this.up();
                    form.setLoading(true);
                    form.down("combobox[name=itemId]").setValue(null);
                    form.down("combobox[name=itemId]").getStore().load({
                        params: {
                            grupoId: elem[0].data.id
                        },
                        callback: function () {
                            form.setLoading(false);
                        }
                    })
                },
                change: function () {
                    this.up("window").setEstadoComponentes();
                }
            }
        }, {
            xtype: 'combobox',
            store: Ext.create('CRM.store.StoreItems'),
            displayField: 'nombre',
            valueField: 'id',
            editable: false,
            allowBlank: false,
            disabled: true,
            fieldLabel: 'Item',
            queryMode: 'local',
            name: 'itemId',
            listeners: {
                change: function () {
                    this.up("window").setEstadoComponentes();
                }
            }
        }, {
            xtype: 'checkbox',
            fieldLabel: 'Correo',
            name: 'correo',
            checked: true,
            disabled: true,
            uncheckedValue: 0,
            inputValue: 1
        }, {
            xtype: 'checkbox',
            fieldLabel: 'Postal',
            name: 'postal',
            checked: true,
            disabled: true,
            uncheckedValue: 0,
            inputValue: 1
        }]
    }],

    setEstadoComponentes: function () {
        var form = this.down("form[name=formItem]"),
            comboPrograma = form.down("combobox[name=programaId]"),
            comboGrupo = form.down("combobox[name=grupoId]"),
            comboItem = form.down("combobox[name=itemId]");

        comboGrupo.setDisabled(!comboPrograma.isValid());
        comboItem.setDisabled(!comboGrupo.isValid());

        form.down("checkbox[name=correo]").setDisabled(!(comboItem.isValid() && (comboItem.getValue()) ? true : false));
        form.down("checkbox[name=postal]").setDisabled(!(comboItem.isValid() && (comboItem.getValue()) ? true : false));
    }
})
;