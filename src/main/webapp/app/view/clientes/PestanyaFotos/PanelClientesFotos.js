Ext.define('CRM.view.clientes.PestanyaFotos.PanelClientesFotos', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.panelClientesFotos',
    bodyPadding: 10,

    fieldDefaults: {
        msgTarget: 'side'
    },
    layout: {
        type: 'hbox',
        align: 'center',
        pack: 'center'
    },
    items: [{
        xtype: 'fieldset',
        title: 'Foto CRM',
        padding: '10',
        margin: '10',
        items: [{
            xtype: 'image',
            width: 100,
            minWidth: 100,
            maxWidth: 100,
            name: 'fotoCRM',
            cls: 'imgFoto'
        }]
    }, {
        xtype: 'fieldset',
        title: 'Foto Persona',
        padding: '10',
        margin: '10',
        items: [{
            xtype: 'image',
            width: 100,
            minWidth: 100,
            maxWidth: 100,
            name: 'fotoPersona',
            cls: 'imgFoto'
        }]
    }],

    tbar: [{
        xtype: 'form',
        border: false,
        name: 'subirFoto',
        bodyStyle: 'padding: 2px 2px 0 2px;',
        items: [{
            xtype: 'filefield',
            buttonOnly: true,
            name: 'fotoCRM',
            buttonText: 'Putxar foto'
        }]
    }, '|', {
        xtype: 'button',
        name: 'descargar',
        text: 'Descarregar CRM'
    }, {
        xtype: 'button',
        name: 'validar',
        text: 'Validar foto'
    }],

    loadFotoCRM: function (imagen) {
        this.down('image[name=fotoCRM]').setSrc(imagen);
    },

    loadFotoPersona: function (imagen) {
        this.down('image[name=fotoPersona]').setSrc(imagen);
    },

    loadFotos: function () {
        var me = this;
        Ext.Ajax.request({
            url: '/crm/rest/cliente/' + me.clienteId + '/fotos',

            success: function (response, opts) {
                var data = Ext.decode(response.responseText).data;
                if (data.binarioCRM) {
                    me.loadFotoCRM(data.binarioCRM);
                }
                if (data.binarioPersona) {
                    me.loadFotoPersona(data.binarioPersona);
                }
            }
        });
    },

    rellenaPanelClientesFotos: function (clienteId) {
        this.clienteId = clienteId;
        this.loadFotos();
    }
});