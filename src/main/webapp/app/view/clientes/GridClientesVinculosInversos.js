Ext.define('CRM.view.clientes.GridClientesVinculosInversos',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridClientesVinculosInversos',
    title : 'Vinculacions inverses',
    columns : [
    {
        text : 'Vincul',
        dataIndex : 'tipoNombre',
        flex : 1,
        renderer : function(value)
        {
            return value + ' en';
        }
    },
    {
        text : 'Nom',
        dataIndex : 'cliente1Nombre',
        flex : 3
    },
    {
        text : 'Perfil',
        dataIndex : 'cliente1Etiqueta',
        flex : 1
    } ]
});