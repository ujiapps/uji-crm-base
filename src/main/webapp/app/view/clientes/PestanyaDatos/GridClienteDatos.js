Ext.define('CRM.view.clientes.PestanyaDatos.GridClienteDatos', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridClienteDatos',
    listeners: {
        selectionchange: function () {
            this.setEstadoComponentes();
        }
    },
    tbar: [{
        xtype: 'button',
        name: 'anyadirClienteDato',
        text: 'Afegir',
        iconCls: 'application-add'
    }, {
        xtype: 'button',
        name: 'borrarClienteDato',
        text: 'Esborrar',
        iconCls: 'application-delete',
        disabled: true
    }],

    columns: [{
        text: 'Nom',
        dataIndex: 'nombreDatoCa',
        flex: 1
    }, {
        text: 'Valor',
        dataIndex: 'valorCa',
        flex: 1
    }, {
        text: 'Comentaris',
        dataIndex: 'observaciones',
        flex: 1
    }, {
        text: 'Fecha ultima modificación',
        dataIndex: 'fechaUltimaModificacion',
        renderer: Ext.util.Format.dateRenderer('d/m/Y'),
        flex: 1
    }],

    setEstadoComponentes: function () {

        var tieneSeleccionGrid = this.getSelectionModel().hasSelection();
        this.down("button[name=borrarClienteDato]").setDisabled(!tieneSeleccionGrid);
    }
});