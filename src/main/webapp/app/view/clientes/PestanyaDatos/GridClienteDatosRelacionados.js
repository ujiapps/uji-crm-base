Ext.define('CRM.view.clientes.PestanyaDatos.GridClienteDatosRelacionados',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridClienteDatosRelacionados',
        frame: true,
        labelAlign: 'top',
        autoScroll: true,
        // store: Ext.create('CRM.store.StoreClienteDatos'),

        tbar: [
            {
                xtype: 'button',
                name: 'anyadirClienteDato',
                text: 'Afegir',
                iconCls: 'application-add'
            },
            {
                xtype: 'button',
                name: 'borrarClienteDato',
                text: 'Esborrar',
                iconCls: 'application-delete',
                disabled: true
            }],
        listeners: {
            selectionchange: function () {
                this.setEstadoComponentes();
            }
        },
        columns: [

            {
                text: 'Nom',
                dataIndex: 'nombreDatoCa',
                flex: 1
            },
            {
                text: 'Valor',
                dataIndex: 'valorCa',
                flex: 1
            },
            {
                text: 'Comentaris',
                dataIndex: 'observaciones',
                flex: 1
            }],

        setEstadoComponentes: function () {

            var tieneSeleccionGrid = this.getSelectionModel().hasSelection();
            this.down("button[name=borrarClienteDato]").setDisabled(!tieneSeleccionGrid);
        }

    });