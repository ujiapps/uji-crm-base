Ext.define('CRM.view.clientes.PestanyaDatos.VentanaGeneralTipo',
    {
        extend: 'Ext.Window',
        // title: 'Afegir a campanya',
        width: 500,
        y: 50,
        alias: 'widget.ventanaGeneralTipo',
        modal: true,
        padding: 5,
        autoScroll: true,
        name: 'ventanaGeneralTipo',
        // requires: ['clientes.PestanyaDatos.FormTipoItem', 'clientes.PestanyaDatos.FormTipoDato', 'clientes.PestanyaDatos.VentanaElegirTipoDato'],
        fbar: [
            {
                xtype: 'button',
                text: 'Afegir i continuar',
                name: 'afegirTipoDato',
                id: 'afegirTipoDatoYContinuar',
                tipo: 'CONTINUAR'

            },
            {
                xtype: 'button',
                text: 'Afegir i finalitzar',
                name: 'afegirTipoDato',
                id: 'afegirTipoDatoYFinalizar',
                tipo: 'CERRAR'
            },
            {
                xtype: 'button',
                text: 'Modificar i continuar',
                name: 'modificarTipoDato',
                id: 'modificarTipoDatoYContinuar',
                tipo: 'CONTINUAR'
            },
            {
                xtype: 'button',
                text: 'Modificar i finalitzar',
                name: 'modificarTipoDato',
                id: 'modificarTipoDatoYFinalizar',
                tipo: 'CERRAR'
            },
            {
                xtype: 'button',
                text: 'Cancel·lar',
                name: 'cerrarVentanaTipoDato'
            }],

        items: [
            {
                xtype: 'formTipoItem',
                name: 'formTipoItem',
                hidden: true
            },
            {
                xtype: 'formTipoDato',
                name: 'formTipoDato',
                hidden: true
            }
        ],

        initComponent: function () {

            this.callParent(arguments);

            this.down("button[id=afegirTipoDatoYFinalizar]").setVisible(!this.conf);
            this.down("button[id=afegirTipoDatoYContinuar]").setVisible(!this.conf);
            this.down("button[id=modificarTipoDatoYFinalizar]").setVisible(this.conf);
            this.down("button[id=modificarTipoDatoYContinuar]").setVisible(this.conf);

            this.down("[name=formTipoItem]").setVisible(this.tipo == 'ITEM');
            this.down("[name=formTipoDato]").setVisible(this.tipo == 'DATO-EXTRA');

            if (this.conf) {
                if (this.tipo == 'DATO-EXTRA') {

                    if (this.conf.data.itemId) {
                        this.tipoOrigen = 'ITEM';
                        this.origen = this.conf.data.itemId;
                    }

                    if (this.conf.data.clienteDatoId) {
                        this.tipoOrigen = 'DATO-EXTRA';
                        this.origen = this.conf.data.clienteDatoId;
                    }

                    this.rellenaDatoExtra(this.conf.data);

                }
                else {

                    if (this.conf.data.clienteItemRelacionado) {
                        this.tipoOrigen = 'ITEM';
                        this.origen = this.conf.data.clienteItemRelacionado;
                    }

                    if (this.conf.data.clienteDato) {
                        this.tipoOrigen = 'DATO-EXTRA';
                        this.origen = this.conf.data.clienteDato;
                    }

                    this.rellenaDatoItem(this.conf.data);
                }

            }
            else {

                if (this.tipo == 'DATO-EXTRA') {
                    this.down("[name=formTipoDato]").down("[name=ventanaTipoDatoCampoTipoCombobox]").setVisible(true);
                    this.down("[name=formTipoDato]").down("grid[name=gridClienteDatosTipoDatoRelacionados]").getStore().loadData([], false);
                    this.down("[name=formTipoDato]").down("grid[name=gridClienteDatosTipoDatoRelacionados]").setVisible(false);
                }

                else {
                    this.down("[name=formTipoItem]").down("[name=comboVentanaTipoItemProgramas]").setVisible(true);
                    this.down("[name=formTipoItem]").down("[name=comboVentanaTipoItemGrupos]").setVisible(true);
                    this.down("[name=formTipoItem]").down("grid[name=gridClienteDatosTipoItemRelacionados]").getStore().loadData([], false);
                    this.down("[name=formTipoItem]").down("grid[name=gridClienteDatosTipoItemRelacionados]").setVisible(false);
                }
            }
        },

        mostrarTipoDato: function (tipoDato) {

            this.down("[name=formTipoDato]").down("[name=ventanaTipoDatoCampoTexto]").setVisible(tipoDato === CRM.model.enums.TiposDato.TEXTO.nombre);
            this.down("[name=formTipoDato]").down("[name=ventanaTipoDatoCampoTextoArea]").setVisible(tipoDato === CRM.model.enums.TiposDato.TEXTO_LARGO.nombre);
            this.down("[name=formTipoDato]").down("[name=ventanaTipoDatoCampoNumerico]").setVisible(tipoDato.substring(0, 1) === CRM.model.enums.TiposDato.NUMERICO.nombre);
            this.down("[name=formTipoDato]").down("[name=ventanaTipoDatoCampoFichero]").setVisible(tipoDato.substring(0, 1) === CRM.model.enums.TiposDato.BINARIO.nombre);
            this.down("[name=formTipoDato]").down("[name=botonDescargarFichero]").setVisible(tipoDato.substring(0, 1) === CRM.model.enums.TiposDato.BINARIO.nombre);
            this.down("[name=formTipoDato]").down("[name=ventanaTipoDatoCampoFecha]").setVisible(tipoDato.substring(0, 1) === CRM.model.enums.TiposDato.FECHA.nombre);
            this.down("[name=formTipoDato]").down("[name=ventanaTipoDatoCampoSelect]").setVisible(tipoDato.substring(0, 1) === CRM.model.enums.TiposDato.SELECCION.nombre);
        },

        rellenaDatoExtra: function (dato) {

            this.down("[name=formTipoDato]").down("[name=id]").setValue(dato.id);

            var campo;

            this.mostrarTipoDato(dato.tipoDatoTipoNombre);

            if (dato.tipoDatoTipoNombre.substring(0, 2) === CRM.model.enums.TiposDato.TEXTO_LARGO.nombre) {
                campo = this.down("[name=formTipoDato]").down("[name=ventanaTipoDatoCampoTextoArea]");
                campo.setValue(dato.valor);
            }
            else if (dato.tipoDatoTipoNombre.substring(0, 1) === CRM.model.enums.TiposDato.TEXTO.nombre) {
                campo = this.down("[name=formTipoDato]").down("[name=ventanaTipoDatoCampoTexto]");
                campo.setValue(dato.valor);
            }
            else if (dato.tipoDatoTipoNombre.substring(0, 1) === CRM.model.enums.TiposDato.NUMERICO.nombre) {
                if (dato.clienteDatoTipoTipoNombre === CRM.model.enums.TiposDato.BOOLEAN.nombre) {

                    campo = this.down("[name=formTipoDato]").down("[name=ventanaTipoDatoCampoSelect]");

                    var store = Ext.create('Ext.data.Store',
                        {
                            proxy: {
                                type: 'ajax',
                                reader: {
                                    type: 'array'
                                }
                            },
                            fields: [
                                {
                                    name: 'id',
                                    type: 'int'
                                },
                                {
                                    name: 'nombre',
                                    type: 'string'
                                }],
                            data: [
                                {
                                    id: 0,
                                    nombre: 'NO'
                                },
                                {
                                    id: 1,
                                    nombre: 'SI'
                                }],
                            queryMode: 'local'
                        });
                    campo.setStore(store);
                }
                else {
                    campo = this.down("[name=formTipoDato]").down("[name=ventanaTipoDatoCampoNumerico]");
                }
                campo.setValue(dato.valor);
            }
            else if (dato.tipoDatoTipoNombre.substring(0, 1) === CRM.model.enums.TiposDato.BINARIO.nombre) {
                campo = this.down("[name=formTipoDato]").down("[name=ventanaTipoDatoCampoFichero]");
                campo.setValue(dato.valor);
            }
            else if (dato.tipoDatoTipoNombre.substring(0, 1) === CRM.model.enums.TiposDato.FECHA.nombre) {
                campo = this.down("[name=formTipoDato]").down("[name=ventanaTipoDatoCampoFecha]");
                campo.setValue(dato.valor);
            }
            else if (dato.tipoDatoTipoNombre.substring(0, 1) === CRM.model.enums.TiposDato.SELECCION.nombre) {
                campo = this.down("[name=formTipoDato]").down("[name=ventanaTipoDatoCampoSelect]");
                campo.getStore().load({
                    url: '/crm/rest/tipo/opciones/' + dato.tipoDatoId,
                    params: {
                        tipoDatoNombre: dato.tipoDatoTipoNombre.substring(0, 1)
                    },
                    callback: function () {
                        campo.setValue(dato.valorSelect);
                    }
                });
            }
            else {
                campo.setValue(dato.valor);
            }

            campo.setFieldLabel(dato.tipoDatoNombre);

            this.down("[name=formTipoDato]").down("combobox[name=comboTipoAcceso]").setValue(dato.tipoAccesoId);
            this.down("[name=formTipoDato]").down("textfield[name=ventanaTipoDatoCampoComentario]").setValue(dato.observaciones);

            this.down("[name=formTipoDato]").down("grid[name=gridClienteDatosTipoDatoRelacionados]").getStore().load({
                url: '/crm/rest/dato/relacionados/',
                params: {
                    dato: dato.id
                }
            });
        },


        rellenaDatoItem: function (dato) {

            this.down("[name=formTipoItem]").down("[name=id]").setValue(dato.id);

            this.down("[name=formTipoItem]").down("combobox[name=comboVentanaTipoItemItems]").setFieldLabel(dato.grupoNombre);
            this.down("[name=formTipoItem]").down("combobox[name=comboVentanaTipoItemItems]").getStore().load({
                params: {
                    grupoId: dato.grupoId
                },
                scope: this,
                callback: function () {
                    this.down("[name=formTipoItem]").down("combobox[name=comboVentanaTipoItemItems]").setValue(dato.itemId);
                }
            });
            this.down("[name=formTipoItem]").down("checkbox[name=ventanaTipoItemPostal]").setValue(dato.postal);
            this.down("[name=formTipoItem]").down("checkbox[name=ventanaTipoItemCorreo]").setValue(dato.correo);

            this.down("[name=formTipoItem]").down("grid[name=gridClienteDatosTipoItemRelacionados]").getStore().load({
                url: '/crm/rest/dato/relacionados/',
                params: {
                    dato: dato.id
                }
            });


        }
    });