Ext.define('CRM.view.clientes.PestanyaDatos.FormTipoDato',
    {
        extend: 'Ext.form.FormPanel',
        alias: 'widget.formTipoDato',
        modal: true,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        padding: 5,
        // autoScroll: true,
        requires: ['CRM.store.StoreTiposDatosSelect'],
        items: [

            {
                xtype: 'textfield',
                hidden: true,
                name: 'id'
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'combobox',
                        name: 'ventanaTipoDatoCampoTipoCombobox',
                        store: Ext.create('CRM.store.StoreClientesDatosTiposClientes'),
                        queryMode: 'local',
                        displayField: 'nombre',
                        valueField: 'id',
                        fieldLabel: 'Elige tipo de dato',
                        hidden: true,
                        padding: 5,
                        listeners: {
                            select: function (combo, item) {
                                this.up("window").mostrarTipoDato(item[0].data.clienteDatoTipoTipoNombre);
                            }
                        }
                    },
                    {
                        xtype: 'textfield',
                        name: 'ventanaTipoDatoCampoTexto',
                        padding: 5,
                        hidden: true

                    },
                    {
                        xtype: 'textarea',
                        name: 'ventanaTipoDatoCampoTextoArea',
                        padding: 5,
                        hidden: true

                    },
                    {
                        xtype: 'textfield',
                        name: 'ventanaTipoDatoCampoNumerico',
                        regex: /^[0-9]+$/,
                        regexText: 'Sols pot contenir caracters numerics',
                        padding: 5,
                        hidden: true
                    },
                    {
                        xtype: 'combobox',
                        name: 'ventanaTipoDatoCampoSelect',
                        padding: 5,
                        store: Ext.create('CRM.store.StoreTiposDatosSelect'),
                        queryMode: 'local',
                        displayField: 'etiqueta',
                        valueField: 'id',
                        hidden: true
                    },
                    {
                        xtype: 'datefield',
                        name: 'ventanaTipoDatoCampoFecha',
                        padding: 5,
                        format: 'd/m/Y',
                        hidden: true
                    },
                    {
                        xtype: 'textfield',
                        name: 'ventanaTipoDatoCampoFichero',
                        padding: 5,
                        hidden: true
                    },
                    {
                        xtype: 'button',
                        name: 'botonDescargarFichero',
                        padding: 5,
                        iconCls: 'arrow-down',
                        hidden: true
                    }]
            },
            {
                xtype: 'combobox',
                name: 'comboTipoAcceso',
                store: 'StoreTiposDatosAcceso',
                displayField: 'nombre',
                valueField: 'id',
                padding: 5,
                fieldLabel: 'Tipo de acceso usuario',
                allowBlank: false
            },
            {
                xtype: 'textfield',
                name: 'ventanaTipoDatoCampoComentario',
                padding: 5,
                fieldLabel: 'Comentarios'

            },
            {
                xtype: 'gridClienteDatosRelacionados',
                name: 'gridClienteDatosTipoDatoRelacionados',
                store: Ext.create('CRM.store.StoreClienteDatos'),
                title: 'Datos relacionados'

            }
        ]
    })
;