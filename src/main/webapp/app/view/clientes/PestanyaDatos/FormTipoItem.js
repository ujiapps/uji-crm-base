Ext.define('CRM.view.clientes.PestanyaDatos.FormTipoItem',
    {
        extend: 'Ext.form.FormPanel',
        // title: 'Afegir a campanya',
        width: 500,
        alias: 'widget.formTipoItem',
        modal: true,
        padding: 5,
        autoScroll: true,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        items: [
            {
                xtype: 'textfield',
                hidden: true,
                name: 'id'
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'combobox',
                        name: 'comboVentanaTipoItemProgramas',
                        store: Ext.create('CRM.store.StoreProgramasAccesoAdmin'),
                        displayField: 'nombreCa',
                        valueField: 'id',
                        queryMode: 'local',
                        padding: 5,
                        hidden: true,
                        flex: 1,
                        listeners: {
                            select: function (combo, item) {
                                combo.up("window").down("combobox[name=comboVentanaTipoItemGrupos]").getStore().load({
                                    params: {
                                        programaId: item[0].data.id
                                    }
                                });
                            }
                        }
                    },
                    {
                        xtype: 'combobox',
                        name: 'comboVentanaTipoItemGrupos',
                        store: Ext.create('CRM.store.StoreGrupos'),
                        displayField: 'nombreCa',
                        valueField: 'id',
                        queryMode: 'local',
                        padding: 5,
                        hidden: true,
                        flex: 1,
                        listeners: {
                            select: function (combo, item) {
                                combo.up("window").down("combobox[name=comboVentanaTipoItemItems]").getStore().load({
                                    params: {
                                        grupoId: item[0].data.id
                                    }
                                });
                            }
                        }
                    },
                    {
                        xtype: 'combobox',
                        name: 'comboVentanaTipoItemItems',
                        store: Ext.create('CRM.store.StoreItems'),
                        displayField: 'nombreCa',
                        valueField: 'id',
                        queryMode: 'local',
                        flex: 1,
                        padding: 5
                    }
                ]
            },

            {
                fieldLabel: 'Postal',
                name: 'ventanaTipoItemPostal',
                padding: 5,
                inputValue: 1,
                uncheckedValue: 0,
                checked: true,
                xtype: 'checkbox'
            },
            {
                fieldLabel: 'Email',
                name: 'ventanaTipoItemCorreo',
                padding: 5,
                inputValue: 1,
                uncheckedValue: 0,
                checked: true,
                xtype: 'checkbox'
            },
            {
                xtype: 'gridClienteDatosRelacionados',
                name: 'gridClienteDatosTipoItemRelacionados',
                store: Ext.create('CRM.store.StoreClienteDatos'),
                title: 'Datos relacionados'

            }
        ]
    })
;