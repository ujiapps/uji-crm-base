Ext.define('CRM.view.clientes.PestanyaDatos.VentanaElegirTipoDato',
    {
        extend: 'Ext.Window',
        // title: 'Afegir a campanya',
        width: 500,
        alias: 'widget.ventanaElegirTipoDato',
        modal: true,
        padding: 5,
        autoScroll: true,
        name: 'ventanaElegirTipoDato',
        title: 'Elegir tipo dato',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [
            {
                xtype: 'combobox',
                name: 'ventanaElegirTipoDatoCombo',
                padding: 5,
                store: Ext.create('Ext.data.Store',
                    {
                        proxy: {
                            type: 'ajax',
                            reader: {
                                type: 'array'
                            }
                        },
                        fields: [
                            {
                                name: 'id',
                                type: 'string'
                            },
                            {
                                name: 'nombre',
                                type: 'string'
                            }],
                        data: [
                            {
                                id: 'DATO-EXTRA',
                                nombre: 'Dato Extra'
                            },
                            {
                                id: 'ITEM',
                                nombre: 'Item'
                            }],
                        queryMode: 'local'
                    }),
                queryMode: 'local',
                displayField: 'nombre',
                valueField: 'id'
            }

        ]
    })
;