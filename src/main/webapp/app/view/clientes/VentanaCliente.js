Ext.define('CRM.view.clientes.VentanaCliente', {
    // extend: 'Ext.Window',
    extend: 'Ext.panel.Panel',
    title: 'Client',
    width: '90%',
    height: '70%',
    alias: 'widget.ventanaCliente',
    requires: ['CRM.view.clientes.TabServicioPanel'],

    layout: 'border',
    border: 0,

    defaults: {
        autoScroll: true,
        modal: true,
        flex: 1,
        labelAlign: 'top',
        closable: false,
        border: 0,
        region: 'center',
    },

    items: [{
        xtype: 'tabsServiciosCliente',
        name: 'tabsServiciosCliente',
        disabled: true
    }]
});