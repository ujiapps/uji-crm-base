Ext.define('CRM.view.listados.PanelListados',
    {
        extend: 'Ext.tab.Panel',
        title: 'Listados',
        requires: ['CRM.view.listados.enviosNoValidos.PanelEnviosNoValidos'],
        alias: 'widget.panelListados',
        closable: true,

        layout: {
            type: 'border',
            align: 'stretch'
        },
        items: [
            {
                xtype: 'panelEnviosNoValidos',
                title: 'Envios Fallidos'
            }]
    });