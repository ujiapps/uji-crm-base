Ext.define('CRM.view.listados.enviosNoValidos.FiltroEnviosNoValidos', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.filtroEnviosNoValidos',
    frame: true,
    border: false,
    padding: 5,
    closable: false,
    // arrayBusqueda: new Array(),
    // requires: ['CRM.model.enums.TiposOperadores', 'CRM.model.enums.TiposComparaciones', 'CRM.model.enums.TiposFechas'],
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    fbar: [{
        xtype: 'button',
        name: 'botonBusqueda',
        icon: '//static.uji.es/js/extjs/uji-commons-extjs/img/find.png',
        maxWidth: 23,
        listeners: {
            click: function () {
                // var obj = {
                // arrayBusqueda: Ext.encode(this.getFiltroEnviosNoValidos().arrayBusqueda),
                // modificado: this.getFiltroEnviosNoValidos().down("radiogroup[name=radioModificadoGroup]").getValue(),
                // operador: this.getFiltroEnviosNoValidos().down("combobox[name=comboOperadorLogicoModificado]").getValue()
                // };
                var obj = this.up("panel[name=filtroEnviosNoValidos]").busqueda(),
                    grid = this.up("panel[name=panelEnviosNoValidos]").down("grid[name=gridEnviosNoValidos]"),
                    storeEnvios = grid.getStore();

                if (obj != null) {
                    // var envios = this.getStoreEnviosNoValidosStore();

                    storeEnvios.getProxy().extraParams = obj;
                    storeEnvios.load({
                        params: {
                            start: 0,
                            page: 0
                        }
                    });
                    storeEnvios.currentPage = 1;
                }
            }
        }
    }, {
        xtype: 'button',
        name: 'botonLimpiarBusqueda',
        text: 'netejar',
        listeners: {
            click: function () {
                this.up('panel[name=filtroEnviosNoValidos]').limpiarCampos();
            }
        }
    }],

    items: [{
        xtype: 'textfield',
        fieldLabel: 'Correo',
        name: 'textoCorreoFiltro'
    }
    ],

// })

//     initComponent: function () {
//         var me = this;
//
//         me.items = [
//             {
//                 xtype: 'fieldset',
//                 //border: true,
//                 title: 'Filtre Dades',
//                 collapsible: false,
//                 //frame: true,
//                 layout: {
//                     type: 'vbox',
//                     align: 'stretch'
//                 },
//                 items: [
//                     {
//                         xtype: 'textarea',
//                         name: 'textoFiltros',
//                         readOnly: true
//
//                     },
//                     {
//                         xtype: 'container',
//                         layout: {
//                             type: 'hbox',
//                             align: 'stretch'
//                         },
//                         padding: '5 0 0 0',
//
//                         items: [
//                             {
//                                 xtype: 'combobox',
//                                 editable: false,
//                                 emptyText: 'Trieu...',
//                                 store: 'StoreOperadoresLogicos',
//                                 displayField: 'nombre',
//                                 valueField: 'id',
//                                 name: 'comboOperadorLogicoFechas',
//                                 padding: 5
//                             },
//                             {
//                                 xtype: 'radiogroup',
//                                 name: 'radioFechaGroup',
//                                 layout: 'hbox',
//                                 items: [
//                                     {
//                                         boxLabel: 'Té data',
//                                         name: 'radioFecha',
//                                         padding: 5,
//                                         inputValue: '1'
//                                     }, {
//                                         boxLabel: 'Tots',
//                                         name: 'radioFecha',
//                                         padding: 5,
//                                         inputValue: '0',
//                                         checked: true
//                                     }
//                                 ],
//                                 listeners: {
//                                     change: function () {
//                                         this.up('panel').setEstadoCamposFechasDisabled();
//                                     }
//                                 }
//                             },
//
//                             {
//                                 xtype: 'combobox',
//                                 editable: false,
//                                 emptyText: 'Trieu...',
//                                 store: 'StoreTiposFecha',
//                                 displayField: 'nombre',
//                                 valueField: 'id',
//                                 name: 'comboTipoFecha',
//                                 disabled: true,
//                                 padding: 5,
//                                 flex: 1,
//                                 listeners: {
//                                     select: function (combo, selection) {
//                                         this.up('panel').setEstadoCamposFechas(selection[0].data.id);
//                                     }
//                                 }
//                             },
//
//                             {
//                                 xtype: 'combobox',
//                                 editable: false,
//                                 emptyText: 'Trieu...',
//                                 store: 'StoreTiposComparacion',
//                                 displayField: 'nombre',
//                                 disabled: true,
//                                 valueField: 'id',
//                                 name: 'comboComparacion',
//                                 padding: 5,
//                                 flex: 1,
//                                 listeners: {
//                                     select: function (combo, selection) {
//                                         var tipoComparacion = selection[0].data.id == 64;
//
//                                         if (this.up('panel').down("combobox[name=comboTipoFecha]").getValue() == 1) {
//                                             this.up('panel').down("combobox[name=comboMesesMax]").setVisible(tipoComparacion);
//                                         }
//                                         if (this.up('panel').down("combobox[name=comboTipoFecha]").getValue() == 2) {
//                                             this.up('panel').down("combobox[name=comboAnyosMax]").setVisible(tipoComparacion);
//                                         }
//                                         if (this.up('panel').down("combobox[name=comboTipoFecha]").getValue() == 3) {
//                                             this.up('panel').down("datefield[name=fechaValorMax]").setVisible(tipoComparacion);
//                                         }
//                                         if (!tipoComparacion) {
//                                             this.up('panel').down("combobox[name=comboAnyosMax]").setValue(null);
//                                             this.up('panel').down("combobox[name=comboMesesMax]").setValue(null);
//                                             this.up('panel').down("datefield[name=fechaValorMax]").setValue(null);
//                                         }
//
//                                     }
//                                 }
//                             },
//                             {
//                                 xtype: 'combobox',
//                                 editable: false,
//                                 emptyText: 'Trieu...',
//                                 store: 'StoreMeses',
//                                 displayField: 'nombre',
//                                 valueField: 'id',
//                                 name: 'comboMeses',
//                                 padding: 5,
//                                 hidden: true,
//                                 flex: 1
//                             },
//                             {
//                                 xtype: 'combobox',
//                                 editable: false,
//                                 emptyText: 'Trieu...',
//                                 store: 'StoreMeses',
//                                 displayField: 'nombre',
//                                 valueField: 'id',
//                                 name: 'comboMesesMax',
//                                 padding: 5,
//                                 hidden: true,
//                                 flex: 1
//                             },
//                             {
//                                 xtype: 'combobox',
//                                 editable: false,
//                                 emptyText: 'Trieu...',
//                                 store: 'StoreAnyos',
//                                 displayField: 'id',
//                                 valueField: 'id',
//                                 name: 'comboAnyos',
//                                 padding: 5,
//                                 hidden: true,
//                                 flex: 1
//                             },
//                             {
//                                 xtype: 'combobox',
//                                 editable: false,
//                                 emptyText: 'Trieu...',
//                                 store: 'StoreAnyos',
//                                 displayField: 'id',
//                                 valueField: 'id',
//                                 name: 'comboAnyosMax',
//                                 padding: 5,
//                                 hidden: true,
//                                 flex: 1
//                             },
//
//                             {
//                                 xtype: 'datefield',
//                                 fieldLabel: 'Data',
//                                 name: 'fechaValorMin',
//                                 format: 'd/m/Y',
//                                 editable: false,
//                                 padding: 5,
//                                 labelWidth: 50,
//                                 hidden: true,
//                                 flex: 1
//                             },
//                             {
//                                 xtype: 'datefield',
//                                 fieldLabel: 'Data Fí',
//                                 name: 'fechaValorMax',
//                                 format: 'd/m/Y',
//                                 editable: false,
//                                 padding: 5,
//                                 labelWidth: 50,
//                                 hidden: true,
//                                 flex: 1
//                             }, {
//                                 xtype: 'button',
//                                 text: 'afegir condició',
//                                 name: 'anyadirFiltroFecha'
//                             }]
//                     }]
//             },
//             {
//                 xtype: 'container',
//                 layout: {
//                     type: 'hbox',
//                     align: 'stretch'
//                 }
//                 ,
//                 padding: '5 0 0 0',
//
//                 items: [
//                     {
//                         xtype: 'combobox',
//                         editable: false,
//                         emptyText: 'Trieu...',
//                         store: 'StoreOperadoresLogicos',
//                         displayField: 'nombre',
//                         valueField: 'id',
//                         name: 'comboOperadorLogicoModificado',
//                         padding: 5
//                     },
//
//                     {
//                         xtype: 'radiogroup',
//                         name: 'radioModificadoGroup',
//                         layout: 'hbox',
//                         items: [
//
//                             {
//                                 boxLabel: 'Modificat',
//                                 name: 'radioModificado',
//                                 padding: 5,
//                                 inputValue: '1'
//                             }, {
//                                 boxLabel: 'Tots',
//                                 name: 'radioModificado',
//                                 padding: 5,
//                                 inputValue: '0',
//                                 checked: true
//                             }
//                         ]
//                     }]
//             }
//         ]
//         ;
//
//         this.callParent(arguments);
//     },
//
//     setEstadoCamposFechasDisabled: function () {
//
//         if (this.down("[name=radioFecha]").getGroupValue() == 1) {
//             this.down("combobox[name=comboComparacion]").setDisabled(false);
//             this.down("combobox[name=comboTipoFecha]").setDisabled(false);
//         }
//         else {
//             this.down("combobox[name=comboComparacion]").setDisabled(true);
//             this.down("combobox[name=comboTipoFecha]").setDisabled(true);
//         }
//
//         this.setEstadoCamposFechas(0);
//     }
//     ,
//
//     setEstadoCamposFechas: function (tipoFecha) {
//
//         this.down("combobox[name=comboComparacion]").setValue(null);
//         this.down("datefield[name=fechaValorMin]").setValue(null);
//         this.down("datefield[name=fechaValorMax]").setValue(null);
//         this.down("combobox[name=comboAnyos]").setValue(null);
//         this.down("combobox[name=comboAnyosMax]").setValue(null);
//         this.down("combobox[name=comboMeses]").setValue(null);
//         this.down("combobox[name=comboMesesMax]").setValue(null);
//
//         this.down("combobox[name=comboAnyosMax]").setVisible(false);
//         this.down("combobox[name=comboMesesMax]").setVisible(false);
//         this.down("datefield[name=fechaValorMax]").setVisible(false);
//
//         if (tipoFecha == 0) {
//             this.down("datefield[name=fechaValorMin]").setVisible(false);
//             this.down("combobox[name=comboMeses]").setVisible(false);
//             this.down("combobox[name=comboAnyos]").setVisible(false);
//             this.down("combobox[name=comboTipoFecha]").setValue(null);
//             this.down("combobox[name=comboComparacion]").setValue(null);
//         }
//
//         if (tipoFecha == 1) {
//             this.down("datefield[name=fechaValorMin]").setVisible(false);
//             this.down("combobox[name=comboMeses]").setVisible(true);
//             this.down("combobox[name=comboAnyos]").setVisible(false);
//         }
//
//         if (tipoFecha == 2) {
//             this.down("datefield[name=fechaValorMin]").setVisible(false);
//             this.down("combobox[name=comboMeses]").setVisible(false);
//             this.down("combobox[name=comboAnyos]").setVisible(true);
//         }
//
//         if (tipoFecha == 3) {
//             this.down("datefield[name=fechaValorMin]").setVisible(true);
//             this.down("combobox[name=comboMeses]").setVisible(false);
//             this.down("combobox[name=comboAnyos]").setVisible(false);
//         }
//
//     }
//     ,
//
    busqueda: function () {
        var objeto =
            {
                correo: this.down("textfield[name=textoCorreoFiltro]").getValue()
//             operador: this.down("combobox[name=comboOperadorLogicoFechas]").getValue(),
//             fecha: this.down("datefield[name=fechaValorMin]").getValue(),
//             fechaMax: this.down("datefield[name=fechaValorMax]").getValue(),
//             tipoComparacion: this.down("combobox[name=comboComparacion]").getValue(),
//             tipoFecha: this.down("combobox[name=comboTipoFecha]").getValue(),
//             mes: this.down("combobox[name=comboMeses]").getValue(),
//             anyo: this.down("combobox[name=comboAnyos]").getValue(),
//             mesMax: this.down("combobox[name=comboMesesMax]").getValue(),
//             anyoMax: this.down("combobox[name=comboAnyosMax]").getValue(),
//             comportamiento: this.down("radiogroup[name=radioFechaGroup]").getValue()
        };


        return objeto;

    }
//     ,
//
//     toString: function (objeto) {
//
//         var texto = '';
//
//         if (objeto.operador == CRM.model.enums.TiposOperadores.Y) {
//             texto = 'Y';
//         }
//         if (objeto.operador == CRM.model.enums.TiposOperadores.O) {
//             texto = 'O';
//         }
//         if (objeto.operador == CRM.model.enums.TiposOperadores.YNO) {
//             texto = 'Y No';
//         }
//         if (objeto.operador == CRM.model.enums.TiposOperadores.ONO) {
//             texto = 'O No';
//         }
//
//         if (objeto.comportamiento.radioFecha == 1) {
//             if (objeto.tipoFecha != null) {
//                 if (objeto.tipoFecha == CRM.model.enums.TiposFechas.MENSUAL) {
//                     texto = texto + ' Mes ';
//                 }
//                 if (objeto.tipoFecha == CRM.model.enums.TiposFechas.ANUAL) {
//                     texto = texto + ' Any ';
//                 }
//                 if (objeto.tipoFecha == CRM.model.enums.TiposFechas.COMPLETA) {
//                     texto = texto + ' Data completa ';
//                 }
//                 if (objeto.tipoComparacion != null) {
//                     if (objeto.tipoComparacion == CRM.model.enums.TiposComparaciones.MAYORQUE) {
//                         texto = texto + ' mayor que ';
//                     }
//                     if (objeto.tipoComparacion == CRM.model.enums.TiposComparaciones.MENORQUE) {
//                         texto = texto + ' menor que ';
//                     }
//                     if (objeto.tipoComparacion == CRM.model.enums.TiposComparaciones.IGUAL) {
//                         texto = texto + ' igual a ';
//                     }
//                     if (objeto.tipoComparacion == CRM.model.enums.TiposComparaciones.MAYOROIGUAL) {
//                         texto = texto + ' mayor o igual a ';
//                     }
//                     if (objeto.tipoComparacion == CRM.model.enums.TiposComparaciones.MENOROIGUAL) {
//                         texto = texto + ' menor o igual a ';
//                     }
//                     if (objeto.tipoComparacion == CRM.model.enums.TiposComparaciones.ENTRE) {
//                         texto = texto + ' entre ';
//                     }
//
//                 }
//
//                 if (objeto.fecha != null) {
//                     texto = texto + Ext.Date.dateFormat(objeto.fecha, 'd/m/Y').toString();
//                 }
//
//                 if (objeto.mes != null) {
//                     texto = texto + objeto.mes;
//                 }
//
//                 if (objeto.anyo != null) {
//                     texto = texto + objeto.anyo;
//                 }
//
//                 if (objeto.tipoComparacion != null) {
//                     if (objeto.tipoComparacion == CRM.model.enums.TiposComparaciones.ENTRE) {
//                         texto = texto + ' y ';
//
//                         if (objeto.fechaMax != null) {
//                             texto = texto + Ext.Date.dateFormat(objeto.fechaMax, 'd/m/Y').toString();
//                         }
//
//                         if (objeto.mesMax != null) {
//                             texto = texto + objeto.mesMax;
//                         }
//
//                         if (objeto.anyoMax != null) {
//                             texto = texto + objeto.anyoMax;
//                         }
//                     }
//                 }
//             }
//             else {
//                 texto = texto + ' te data. '
//             }
//
//         }
//
//         if (objeto.comportamiento.radioFecha == 0) {
//             texto = texto + ' Totes les dates. ';
//         }
//
//         return texto;
//     }
    ,

    limpiarCampos: function () {

        this.down("textfield[name=textoCorreoFiltro]").setValue(null);
//
//         this.down("combobox[name=comboTipoFecha]").setValue(null);
//         this.down("combobox[name=comboComparacion]").setValue(null);
//         this.down("datefield[name=fechaValorMin]").setValue(null);
//         this.down("datefield[name=fechaValorMax]").setValue(null);
//         this.down("combobox[name=comboAnyos]").setValue(null);
//         this.down("combobox[name=comboAnyosMax]").setValue(null);
//         this.down("combobox[name=comboMeses]").setValue(null);
//         this.down("combobox[name=comboMesesMax]").setValue(null);
//
//         this.down("combobox[name=comboAnyos]").setVisible(false);
//         this.down("combobox[name=comboMeses]").setVisible(false);
//         this.down("datefield[name=fechaValorMin]").setVisible(false);
//
//         this.down("combobox[name=comboAnyosMax]").setVisible(false);
//         this.down("combobox[name=comboMesesMax]").setVisible(false);
//         this.down("datefield[name=fechaValorMax]").setVisible(false);
//
//         this.down("[name=radioFecha]").setValue(0);
//         this.down("[name=radioModificado]").setValue(0);
//
//         this.down("textarea[name=textoFiltros]").setValue(null);
//         this.down("combobox[name=comboOperadorLogicoFechas]").setValue(null);
//         this.down("combobox[name=comboOperadorLogicoModificado]").setValue(null);
//
//         while (this.arrayBusqueda.length > 0) {
//             this.arrayBusqueda.pop();
//         }
//
        var objeto =
            {
                correo: null
//             arrayBusqueda: null,
//             modificado: 0,
//             operador: 0
        };
//
        this.up('panel').down('grid').getStore().getProxy().extraParams = objeto;

        this.up('panel').down('grid').getStore().load({
            params: {
                start: 0,
                page: 0
            }
        });
    }
})
;