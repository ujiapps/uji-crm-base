Ext.define('CRM.view.listados.enviosNoValidos.PanelEnviosNoValidos',
    {
        extend: 'Ext.panel.Panel',
        requires: ['CRM.view.listados.enviosNoValidos.GridEnviosNoValidos', 'CRM.view.listados.enviosNoValidos.FiltroEnviosNoValidos'],
        alias: 'widget.panelEnviosNoValidos',
        closable: true,
        name : 'panelEnviosNoValidos',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        items: [
            {
                xtype: 'filtroEnviosNoValidos',
                name : 'filtroEnviosNoValidos'
                //flex: 1
            },
            {
                xtype: 'gridEnviosNoValidos',
                name: 'gridEnviosNoValidos',
                flex: 6
            }]

    });