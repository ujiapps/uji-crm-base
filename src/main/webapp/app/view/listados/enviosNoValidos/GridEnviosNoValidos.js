function formatDate(value) {
    return value ? Ext.Date.dateFormat(value, 'd/m/Y') : '';
}

Ext.define('CRM.view.listados.enviosNoValidos.GridEnviosNoValidos',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridEnviosNoValidos',
        store: 'StoreEnviosNoValidos',
        // sortableColumns: false,
        requires: ['Ext.grid.plugin.RowEditing'],
        dockedItems: [
            {
                xtype: 'pagingtoolbar',
                store: 'StoreEnviosNoValidos',
                dock: 'bottom',
                displayInfo: true
            }],

        plugins: [
            {
                ptype: 'rowediting',
                clicksToEdit: 2,
                pluginId: 'editingEnviosNoValidos'
            }],

        columns: [
            {
                text: 'enviado a',
                dataIndex: 'nombre',
                sortable : true,
                flex: 4
            },
            {
                text: 'Estat',
                dataIndex: 'estado',
                sortable : false,
                flex: 1
            },
            {
                text: 'Fecha primer intento',
                dataIndex: 'fechaPrimerIntento',
                format: 'd/m/Y',
                sortable : true,
                renderer: formatDate,
                flex: 1
            },{
                text: 'Fecha envío',
                dataIndex: 'fechaEnvio',
                format: 'd/m/Y',
                sortable : true,
                renderer: formatDate,
                flex: 1
            },
            {
                text: 'Num Intentos',
                dataIndex: 'numIntentos',
                sortable: false,
                flex: 1
            },
            {
                text: 'Modificat',
                dataIndex: 'modificado',
                flex: 1,
                sortable: false,
                xtype: 'booleancolumn',
                trueText: 'Sí',
                falseText: 'No',
                editor: {
                    xtype: 'checkbox'
                }
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                sortable : false,
                menuDisabled: true,
                align: 'center',
                name: 'abrirCliente',
                items: [
                    {
                        xtype: 'button',
                        name: 'abrirCliente',
                        icon: '//static.uji.es/js/extjs/uji-commons-extjs/img/application_edit.png'
                    }]
            }]

    });