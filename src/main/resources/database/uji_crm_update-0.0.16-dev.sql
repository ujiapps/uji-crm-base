ALTER TABLE crm_clientes_est_no_uji ADD cliente_dato_id number;
ALTER TABLE crm_clientes_est_no_uji ADD CONSTRAINT crm_clientes_est_no_uji_fk3 FOREIGN KEY (cliente_dato_id) REFERENCES crm_clientes_datos(id);

ALTER TABLE crm_clientes_datos_opciones ADD ETIQUETA_ES varchar(2000);
ALTER TABLE crm_clientes_datos_opciones ADD ETIQUETA_EN varchar(2000);
ALTER TABLE crm_clientes_datos_opciones ADD REFERENCIA varchar(200);
ALTER TABLE crm_clientes_datos_opciones ADD ORDEN number;

