CREATE TABLE CRM_CLIENTES_GENERAL
(
  ID NUMBER NOT NULL
, PERSONA_ID NUMBER
, TIPO_IDENTIFICACION_ID NUMBER
, IDENTIFICACION VARCHAR2(200)
, CONSTRAINT CRM_CLIENTES_GENERAL_PK PRIMARY KEY
  (
    ID
  )
  ENABLE
);

alter table "UJI_CRM"."CRM_CLIENTES_GENERAL" add constraint CRM_CLIENTES_GENERAL_FK1 foreign key("TIPO_IDENTIFICACION_ID") references "CRM_TIPOS"("ID")

insert into
 crm_clientes_general (id, identificacion, tipo_identificacion_id, persona_id)
 select hibernate_sequence.nextval, x.identificacion, x.tipo_identificacion, x.persona_id
   from (select identificacion, tipo_identificacion, persona_id from crm_clientes group by identificacion, tipo_identificacion, persona_id having count(*) = 1) x;

insert into
 crm_clientes_general (id, identificacion, tipo_identificacion_id, persona_id)
 select hibernate_sequence.nextval, x.identificacion, x.tipo_identificacion, x.persona_id
   from (select identificacion, tipo_identificacion, persona_id from crm_clientes group by identificacion, tipo_identificacion, persona_id having count(*) > 1) x;

ALTER TABLE CRM_CLIENTES
  ADD "CLIENTE_GENERAL_ID"  NUMBER;

update crm_clientes c
   set cliente_general_id = (select id from crm_clientes_general cg where c.identificacion = cg.identificacion and nvl(c.persona_id, -1) = nvl(cg.persona_id, -1));


ALTER TABLE CRM_CLIENTES
  ADD "DEFINITIVO"  NUMBER (1);

update crm_clientes set definitivo = 1;


ALTER TABLE CRM_CLIENTES
  DROP COLUMN "ETIQUETA";

ALTER TABLE CRM_CLIENTES
  ADD "SERVICIO_ID"  NUMBER;

alter table "UJI_CRM"."CRM_CLIENTES" add constraint CRM_CLIENTES_FK7 foreign key("SERVICIO_ID") references "CRM_SERVICIOS"("ID");

update crm_clientes set servicio_id = 366;

