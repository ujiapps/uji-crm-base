SET DEFINE OFF;
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (1, 'Pendent', 'clientes.estadosEnvios');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (2, 'Vàlid', 'clientes.estadosEnvios');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (3, 'No Vàlid', 'clientes.estadosEnvios');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (4, 'Escritura', 'clientesDatos.accesos');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (5, 'Lectura', 'clientesDatos.accesos');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (6, 'N', 'clientesDatosTipos.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (7, 'F', 'clientesDatosTipos.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (8, 'T', 'clientesDatosTipos.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (9, 'Intern', 'clientesDatos.accesos');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (10, 'B', 'clientesDatosTipos.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (11, 'TM', 'clientesDatosTipos.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (12, 'NT', 'clientesDatosTipos.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (14, 'No actiu', 'items.recepcionInfo');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (15, 'Modificable', 'items.recepcionInfo');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (16, 'No modificable', 'items.recepcionInfo');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (17, 'Modificable + Defecto', 'items.recepcionInfo');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (18, 'No Modificable + Defecto', 'items.recepcionInfo');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (19, 'Gerente', 'vinculaciones.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (20, 'S', 'clienteDatosTipos.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (21, 'PROPOST', 'campanyasClientes.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (22, 'DEFINITIU', 'campanyasClientes.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (23, 'Secretario', 'vinculaciones.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (24, 'Trabajador', 'vinculaciones.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (26, 'MANUAL-USUARI', 'campanyasClientes.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (27, 'ENVIOMAIL', 'envios.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (28, 'ENVIOSMS', 'envios.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (29, 'ENVIOMOVIL', 'envios.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (30, 'ENVIOPOSTAL', 'envios.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (31, 'TL', 'clientesDatosTipos.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (32, 'Benvinguda', 'campanyasEnviosAuto.tipoDato');
Insert into UJI_CRM.CRM_TIPOS
   (ID, NOMBRE, TABLA_COLUMNA)
 Values
   (33, 'Baixa', 'campanyasEnviosAuto.tipoDato');
COMMIT;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SET DEFINE OFF;
Insert into UJI_CRM.CRM_CLIENTES_DATOS_TIPOS
(ID, NOMBRE, TIPO_ID)
Values
  (8, 'Acepta Envios', 20);
Insert into UJI_CRM.CRM_CLIENTES_DATOS_TIPOS
(ID, NOMBRE, TIPO_ID, TAMANYO)
Values
  (1, 'Otro Contacto', 8, 1000);
Insert into UJI_CRM.CRM_CLIENTES_DATOS_TIPOS
(ID, NOMBRE, TIPO_ID)
Values
  (2, 'Edad', 6);
Insert into UJI_CRM.CRM_CLIENTES_DATOS_TIPOS
(ID, NOMBRE, TIPO_ID)
Values
  (3, 'Fecha Nacimiento', 7);
Insert into UJI_CRM.CRM_CLIENTES_DATOS_TIPOS
(ID, NOMBRE, TIPO_ID)
Values
  (4, 'Fichero', 10);
Insert into UJI_CRM.CRM_CLIENTES_DATOS_TIPOS
(ID, NOMBRE, TIPO_ID, TAMANYO)
Values
  (5, 'Teléfono', 12, 15);
Insert into UJI_CRM.CRM_CLIENTES_DATOS_TIPOS
(ID, NOMBRE, TIPO_ID)
Values
  (6, 'Mail', 11);
Insert into UJI_CRM.CRM_CLIENTES_DATOS_TIPOS
(ID, NOMBRE, TIPO_ID)
Values
  (7, 'Sexo', 20);
COMMIT;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SET DEFINE OFF;
Insert into UJI_CRM.CRM_CLIENTES_DATOS_OPCIONES
(ID, TIPO_ID, VALOR, ETIQUETA)
Values
  (3, 8, 0, 'No');
Insert into UJI_CRM.CRM_CLIENTES_DATOS_OPCIONES
(ID, TIPO_ID, VALOR, ETIQUETA)
Values
  (4, 8, 1, 'Si');
Insert into UJI_CRM.CRM_CLIENTES_DATOS_OPCIONES
(ID, TIPO_ID, VALOR, ETIQUETA)
Values
  (1, 7, 0, 'Home');
Insert into UJI_CRM.CRM_CLIENTES_DATOS_OPCIONES
(ID, TIPO_ID, VALOR, ETIQUETA)
Values
  (2, 7, 1, 'Dona');
COMMIT;
