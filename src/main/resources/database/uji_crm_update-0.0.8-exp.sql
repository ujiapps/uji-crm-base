ALTER TABLE UJI_CRM.CRM_CLIENTES_DATOS_TIPOS ADD (ORDEN NUMBER);

ALTER TABLE UJI_CRM.CRM_CAMPANYAS_ENVIOS_AUTO ADD (
    DESDE  VARCHAR2(200),
    RESPONDER_A VARCHAR2(200)
);

ALTER TABLE UJI_CRM.CRM_ENVIOS ADD (
    DESDE  VARCHAR2(200),
    RESPONDER_A VARCHAR2(200)
);

ALTER TABLE UJI_CRM.CRM_ITEMS ADD (
    DESCRIPCION_CA  VARCHAR2(4000),
    DESCRIPCION_ES  VARCHAR2(4000),
    DESCRIPCION_UK  VARCHAR2(4000)
);

ALTER TABLE UJI_CRM.CRM_GRUPOS ADD (
    DESCRIPCION_CA  VARCHAR2(4000),
    DESCRIPCION_ES  VARCHAR2(4000),
    DESCRIPCION_UK  VARCHAR2(4000)
);

CREATE TABLE UJI_CRM.CRM_ACCIONES
(
  HASH        VARCHAR2(100 CHAR),
  ACCION      VARCHAR2(4000 CHAR),
  REFERENCIA  VARCHAR2(200 CHAR),
  FECHA       DATE                              DEFAULT sysdate
);

ALTER TABLE UJI_CRM.CRM_ETIQUETAS ADD
(
  busqueda AS (UPPER(TRANSLATE(LOWER(nombre),'áéíóúàèìòùäëïöüâêîôû','aeiouaeiouaeiouaeiou'))) virtual
););


CREATE OR REPLACE FORCE VIEW UJI_CRM.CRM_EXT_PERSONAS
(
   ID,
   IDENTIFICACION,
   NOMBRE,
   APELLIDO1,
   APELLIDO2,
   NOMBRE_APELLIDOS,
   APELLIDOS_NOMBRE,
   CORREO,
   MOVIL,
   POSTAL,
   NOMBRE_APELLIDOS_BUSQUEDA,
   APELLIDOS_NOMBRE_BUSQUEDA,
   SEXO,
   FECHA_NACIMIENTO
)
AS
   select   p.id,
            p.identificacion,
            p.nombre,
            p.apellido1,
            p.apellido2,
            p.nombre || ' ' || p.apellido1 || ' ' || p.apellido2,
            p.apellido1 || ' ' || p.apellido2 || ', ' || p.nombre,
            d.email,
            d.movil,
            d.nombre || chr (10) || d.linea2 || chr (10) || d.linea3,
            cmp.limpia (p.nombre || ' ' || p.apellido1 || ' ' || p.apellido2),
            cmp.limpia (
               p.apellido1 || ' ' || p.apellido2 || ', ' || p.nombre
            ),
            d.sexo,
            d.f_nacimiento
     from   gri_per.per_personas p, gri_per.per_vw_domicilios d
    where   p.id = d.per_id;


CREATE OR REPLACE FORCE VIEW UJI_CRM.CRM_VW_ENVIOS_CLIENTES
(
   ID,
   CUENTA,
   CORREO,
   CORREO_ESTADO,
   CORREO_ESTADO_ITEM,
   MOVIL,
   MOVIL_ESTADO,
   SMS_ESTADO_ITEM,
   APP_MOVIL_ESTADO_ITEM,
   POSTAL,
   POSTAL_ESTADO,
   POSTAL_ESTADO_ITEM,
   ENVIO_TIPO,
   ENVIO_ID
)
AS
   select   id,
            cuenta,
            correo,
            correo_estado,
            correo_estado_item,
            movil,
            movil_estado,
            sms_estado_item,
            app_movil_estado_item,
            postal,
            postal_estado,
            postal_estado_item,
            envio_tipo,
            envio_id
     from   (select   c.id,
                      p.cuenta,
                      c.correo,
                      c.correo_estado_id correo_estado,
                      1 correo_estado_item,
                      c.movil,
                      c.movil_estado_id movil_estado,
                      1 sms_estado_item,
                      1 app_movil_estado_item,
                      c.postal,
                      c.postal_estado_id postal_estado,
                      1 postal_estado_item,
                      v.tipo_id envio_tipo,
                      v.id envio_id
               from   crm_envios_criterios ec,
                      crm_envios v,
                      crm_clientes c,
                      crm_ext_personas p
              where       c.persona_id(+) = p.id
                      and ec.referencia1_id = c.id
                      and ec.envio_id = v.id
                      and origen = 'Cliente'
                      and ec.tipo = 1
             union
             select   c.id,
                      p.cuenta,
                      c.correo,
                      c.correo_estado_id correo_estado,
                      ci.correo correo_estado_item,
                      c.movil,
                      c.movil_estado_id movil_estado,
                      ci.sms sms_estado_item,
                      ci.bt app_movil_estado_item,
                      c.postal,
                      c.postal_estado_id postal_estado,
                      ci.postal postal_estado_item,
                      v.tipo_id envio_tipo,
                      v.id envio_id
               from   crm_envios_criterios ec,
                      crm_envios v,
                      crm_items i,
                      crm_clientes_items ci,
                      crm_clientes c,
                      crm_ext_personas p
              where       c.persona_id(+) = p.id
                      and ec.referencia1_id = i.id
                      and i.id = ci.item_id
                      and ci.cliente_id = c.id
                      and ec.envio_id = v.id
                      and origen = 'Grupos'
                      and ec.tipo = 1
             union
             select   c.id,
                      p.cuenta,
                      c.correo,
                      c.correo_estado_id,
                      1,
                      c.movil,
                      c.movil_estado_id,
                      1,
                      1,
                      c.postal,
                      c.postal_estado_id,
                      1,
                      v.tipo_id,
                      v.id
               from   crm_envios_criterios ec,
                      crm_envios v,
                      crm_etiquetas e,
                      crm_clientes_etiquetas ce,
                      crm_clientes c,
                      crm_ext_personas p
              where       c.persona_id(+) = p.id
                      and ec.referencia1_id = e.id
                      and ec.envio_id = v.id
                      and e.id = ce.etiqueta_id
                      and ce.cliente_id = c.id
                      and origen = 'Etiquetes'
                      and ec.tipo = 1
             union
             select   c.id,
                      p.cuenta,
                      c.correo,
                      c.correo_estado_id,
                      1,
                      c.movil,
                      c.movil_estado_id,
                      1,
                      1,
                      c.postal,
                      c.postal_estado_id,
                      1,
                      v.tipo_id,
                      v.id
               from   crm_envios_criterios ec,
                      crm_envios v,
                      crm_campanyas ca,
                      crm_campanyas_clientes cc,
                      crm_clientes c,
                      crm_ext_personas p
              where       c.persona_id(+) = p.id
                      and ec.referencia1_id = ca.id
                      and ec.envio_id = v.id
                      and cc.cliente_id = c.id
                      and cc.campanya_id = ca.id
                      and cc.tipo_id = ec.referencia2_id
                      and origen = 'Campanyes'
                      and ec.tipo = 1) x
    where   (x.id in
                   (select   c.id
                      from   crm_envios_criterios ec,
                             crm_envios v,
                             crm_clientes c
                     where       ec.referencia1_id = c.id
                             and ec.envio_id = v.id
                             and origen = 'Cliente'
                             and ec.tipo = 2
                    union
                    select   c.id
                      from   crm_envios_criterios ec,
                             crm_envios v,
                             crm_items i,
                             crm_clientes_items ci,
                             crm_clientes c
                     where       ec.referencia1_id = i.id
                             and i.id = ci.item_id
                             and ci.cliente_id = c.id
                             and ec.envio_id = v.id
                             and origen = 'Grupos'
                             and ec.tipo = 2
                    union
                    select   c.id
                      from   crm_envios_criterios ec,
                             crm_envios v,
                             crm_etiquetas e,
                             crm_clientes_etiquetas ce,
                             crm_clientes c
                     where       ec.referencia1_id = e.id
                             and ec.envio_id = v.id
                             and e.id = ce.etiqueta_id
                             and ce.cliente_id = c.id
                             and origen = 'Etiquetes'
                             and ec.tipo = 2
                    union
                    select   c.id
                      from   crm_envios_criterios ec,
                             crm_envios v,
                             crm_campanyas ca,
                             crm_campanyas_clientes cc,
                             crm_clientes c
                     where       ec.referencia1_id = ca.id
                             and ec.envio_id = v.id
                             and cc.cliente_id = c.id
                             and cc.campanya_id = ca.id
                             and cc.tipo_id = ec.referencia2_id
                             and origen = 'Campanyes'
                             and ec.tipo = 2)
             or (select   count ( * )
                   from   (select   c.id
                             from   crm_envios_criterios ec,
                                    crm_envios v,
                                    crm_clientes c
                            where       ec.referencia1_id = c.id
                                    and ec.envio_id = v.id
                                    and origen = 'Cliente'
                                    and ec.tipo = 2
                           union
                           select   c.id
                             from   crm_envios_criterios ec,
                                    crm_envios v,
                                    crm_items i,
                                    crm_clientes_items ci,
                                    crm_clientes c
                            where       ec.referencia1_id = i.id
                                    and i.id = ci.item_id
                                    and ci.cliente_id = c.id
                                    and ec.envio_id = v.id
                                    and origen = 'Grupos'
                                    and ec.tipo = 2
                           union
                           select   c.id
                             from   crm_envios_criterios ec,
                                    crm_envios v,
                                    crm_etiquetas e,
                                    crm_clientes_etiquetas ce,
                                    crm_clientes c
                            where       ec.referencia1_id = e.id
                                    and ec.envio_id = v.id
                                    and e.id = ce.etiqueta_id
                                    and ce.cliente_id = c.id
                                    and origen = 'Etiquetes'
                                    and ec.tipo = 2
                           union
                           select   c.id
                             from   crm_envios_criterios ec,
                                    crm_envios v,
                                    crm_campanyas ca,
                                    crm_campanyas_clientes cc,
                                    crm_clientes c
                            where       ec.referencia1_id = ca.id
                                    and ec.envio_id = v.id
                                    and cc.cliente_id = c.id
                                    and cc.campanya_id = ca.id
                                    and cc.tipo_id = ec.referencia2_id
                                    and origen = 'Campanyes'
                                    and ec.tipo = 2)) = 0)
   minus
   select   c.id,
            p.cuenta,
            c.correo,
            c.correo_estado_id correo_estado,
            1 correo_estado_item,
            c.movil,
            c.movil_estado_id movil_estado,
            1 sms_estado_item,
            1 app_movil_estado_item,
            c.postal,
            c.postal_estado_id postal_estado,
            1 postal_estado_item,
            v.tipo_id envio_tipo,
            v.id envio_id
     from   crm_envios_criterios ec,
            crm_envios v,
            crm_clientes c,
            crm_ext_personas p
    where       c.persona_id(+) = p.id
            and ec.referencia1_id = c.id
            and ec.envio_id = v.id
            and origen = 'Cliente'
            and ec.tipo = 3;
                                    
ALTER TABLE UJI_CRM.CRM_CLIENTES ADD (TIPO CHAR(1 BYTE) DEFAULT 'F');

CREATE OR REPLACE FORCE VIEW UJI_CRM.CRM_EXT_CURSOS_INSCRITOS
(
   ID,
   NOMBRE,
   TIPO,
   TIPO_COMPARTE,
   TIPO_NOMBRE,
   PERSONA_ID,
   FECHA_INSCRIPCION,
   ANULA_INSCRIPCION,
   ASISTENCIA,
   APROVECHAMIENTO,
   ESPERA,
   CONFIRMACION,
   CALIFICACION,
   IMPORTE
)
AS
   select   c.id,
            c.nombre,
            c.uest_id,
            decode (c.compartido, 0, null, c.comparte) uest_id_comparte,
            e.nombre,
            lc.per_id,
            lc.f_inscripcion,
            lc.anula_inscripcion,
            lc.asistencia,
            lc.aprovechamiento,
            lc.espera,
            lc.confirmacion,
            lc.calificacion,
            lc.importe
     from         grh_lista_curso lc
               join
                  grh_cursos c
               on lc.ecur_crso_id = c.id
            join
               est_ubic_estructurales e
            on c.uest_id = e.id;

CREATE TABLE UJI_CRM.CRM_PROGRAMAS_CURSOS
(
  ID           NUMBER,
  PROGRAMA_ID  NUMBER                           NOT NULL,
  UEST_ID      NUMBER                           NOT NULL
);

CREATE UNIQUE INDEX UJI_CRM.CRM_PROGRAMAS_CURSOS_PK ON UJI_CRM.CRM_PROGRAMAS_CURSOS (ID);

ALTER TABLE UJI_CRM.CRM_PROGRAMAS_CURSOS ADD (CONSTRAINT CRM_PROGRAMAS_CURSOS_PK PRIMARY KEY (ID));

ALTER TABLE UJI_CRM.CRM_PROGRAMAS_CURSOS ADD (CONSTRAINT CRM_PROGRAMAS_CURSOS_FK1 FOREIGN KEY (PROGRAMA_ID) REFERENCES UJI_CRM.CRM_PROGRAMAS (ID)); 

ALTER TABLE UJI_CRM.CRM_CLIENTES
MODIFY(BUSQUEDA  Generated Always As (UPPER(TRANSLATE(LOWER("NOMBRE_OFICIAL"||' '||"APELLIDOS_OFICIAL"||' '||"NOMBRE_OFICIAL"||' '||"IDENTIFICACION"||' '||"CORREO"||' '||TO_CHAR("MOVIL")),'áéíóúàèìòùäëïöüâêîôû','aeiouaeiouaeiouaeiou'))));

ALTER TABLE UJI_CRM.CRM_CLIENTES
RENAME COLUMN NOMBRE TO NOMBRE_COMERCIAL;

ALTER TABLE UJI_CRM.CRM_CLIENTES
MODIFY(BUSQUEDA  Generated Always As (UPPER(TRANSLATE(LOWER("NOMBRE_OFICIAL"||' '||"APELLIDOS_OFICIAL"||' '||"NOMBRE_OFICIAL"||' '||"NOMBRE_COMERCIAL"||' '||"IDENTIFICACION"||' '||"CORREO"||' '||TO_CHAR("MOVIL")),'áéíóúàèìòùäëïöüâêîôû','aeiouaeiouaeiouaeiou'))));

CREATE OR REPLACE FORCE VIEW UJI_CRM.CRM_V_PER_PROGRAMAS_CURSOS
(
   ID,
   PERSONA_ID,
   uest_id
)
AS
   select   p.id, p.persona_id, c.uest_id
     from   CRM_V_PERSONAS_PROGRAMAS p, crm_programas_cursos c
    where   p.programa_id = c.programa_id;


CREATE OR REPLACE FORCE VIEW UJI_CRM.CRM_VW_ENVIOS_CRITERIOS
(
   ID,
   ENVIO_ID,
   TIPO_ENVIO,
   TIPO,
   ORIGEN,
   REFERENCIA1_ID,
   REFERENCIA2_ID,
   NOMBRE,
   COUNT_CORREO,
   COUNT_SMS,
   COUNT_APP_MOVIL,
   COUNT_POSTAL
)
AS
     select   ec.id,
              ec.envio_id,
              v.tipo_id,
              ec.tipo,
              ec.origen,
              ec.referencia1_id,
              ec.referencia2_id,
              i.nombre_ca,
              sum(decode (c.correo,
                          null, 0,
                          decode (c.correo_estado_id, 3, 0, ci.correo))),
              sum(decode (c.movil,
                          null, 0,
                          decode (c.movil_estado_id, 3, 0, ci.sms))),
              sum(decode (c.movil,
                          null, 0,
                          decode (c.movil_estado_id, 3, 0, ci.bt))),
              sum(decode (c.postal,
                          null, 0,
                          decode (c.postal_estado_id, 3, 0, ci.postal)))
       from   crm_envios_criterios ec,
              crm_envios v,
              crm_items i,
              crm_clientes_items ci,
              crm_clientes c
      where       ec.referencia1_id = i.id
              and i.id = ci.item_id(+)
              and ci.cliente_id = c.id(+)
              and ec.envio_id = v.id
              and origen = 'Grupos'
   group by   ec.id,
              ec.envio_id,
              v.tipo_id,
              ec.tipo,
              ec.origen,
              ec.referencia1_id,
              ec.referencia2_id,
              i.nombre_ca
   union
     select   ec.id,
              ec.envio_id,
              v.tipo_id,
              ec.tipo,
              ec.origen,
              ec.referencia1_id,
              ec.referencia2_id,
              e.nombre,
              sum(decode (c.correo,
                          null, 0,
                          decode (c.correo_estado_id, 3, 0, 1))),
              sum (
                 decode (c.movil, null, 0, decode (c.movil_estado_id, 3, 0, 1))
              ),
              sum (
                 decode (c.movil, null, 0, decode (c.movil_estado_id, 3, 0, 1))
              ),
              sum(decode (c.postal,
                          null, 0,
                          decode (c.postal_estado_id, 3, 0, 1)))
       from   crm_envios_criterios ec,
              crm_envios v,
              crm_etiquetas e,
              crm_clientes_etiquetas ce,
              crm_clientes c
      where       ec.referencia1_id = e.id
              and ec.envio_id = v.id
              and e.id = ce.etiqueta_id(+)
              and ce.cliente_id = c.id(+)
              and origen = 'Etiquetes'
   group by   ec.id,
              ec.envio_id,
              v.tipo_id,
              ec.tipo,
              ec.origen,
              ec.referencia1_id,
              ec.referencia2_id,
              e.nombre
   union
     select   ec.id,
              ec.envio_id,
              v.tipo_id,
              ec.tipo,
              ec.origen,
              ec.referencia1_id,
              ec.referencia2_id,
              c.apellidos_oficial || ', ' || c.nombre_oficial,
              sum(decode (c.correo,
                          null, 0,
                          decode (c.correo_estado_id, 3, 0, 1))),
              sum (
                 decode (c.movil, null, 0, decode (c.movil_estado_id, 3, 0, 1))
              ),
              sum (
                 decode (c.movil, null, 0, decode (c.movil_estado_id, 3, 0, 1))
              ),
              sum(decode (c.postal,
                          null, 0,
                          decode (c.postal_estado_id, 3, 0, 1)))
       from   crm_envios_criterios ec, crm_envios v, crm_clientes c
      where       ec.referencia1_id = c.id
              and ec.envio_id = v.id
              and origen = 'Cliente'
   group by   ec.id,
              ec.envio_id,
              v.tipo_id,
              ec.tipo,
              ec.origen,
              ec.referencia1_id,
              ec.referencia2_id,
              c.apellidos_oficial || ', ' || c.nombre_oficial
   union
   select   ec.id,
            ec.envio_id,
            v.tipo_id,
            ec.tipo,
            ec.origen,
            ec.referencia1_id,
            ec.referencia2_id,
            ca.nombre || ' (' || t.nombre || ')',
            nvl (
               (select   sum(decode (c.correo,
                                     null, 0,
                                     decode (c.correo_estado_id, 3, 0, 1)))
                  from   crm_campanyas_clientes cc, crm_clientes c
                 where       cc.cliente_id = c.id
                         and campanya_id = ec.referencia1_id
                         and ec.referencia2_id = cc.tipo_id),
               0
            ),
            nvl (
               (select   sum(decode (c.movil,
                                     null, 0,
                                     decode (c.movil_estado_id, 3, 0, 1)))
                  from   crm_campanyas_clientes cc, crm_clientes c
                 where       cc.cliente_id = c.id
                         and campanya_id = ec.referencia1_id
                         and ec.referencia2_id = cc.tipo_id),
               0
            ),
            nvl (
               (select   sum(decode (c.movil,
                                     null, 0,
                                     decode (c.movil_estado_id, 3, 0, 1)))
                  from   crm_campanyas_clientes cc, crm_clientes c
                 where       cc.cliente_id = c.id
                         and campanya_id = ec.referencia1_id
                         and ec.referencia2_id = cc.tipo_id),
               0
            ),
            nvl (
               (select   sum(decode (c.postal,
                                     null, 0,
                                     decode (c.postal_estado_id, 3, 0, 1)))
                  from   crm_campanyas_clientes cc, crm_clientes c
                 where       cc.cliente_id = c.id
                         and campanya_id = ec.referencia1_id
                         and ec.referencia2_id = cc.tipo_id),
               0
            )
     from   crm_envios_criterios ec,
            crm_envios v,
            crm_campanyas ca,
            crm_tipos t
    where       ec.referencia1_id = ca.id
            and ec.envio_id = v.id
            and t.id = ec.referencia2_id
            and origen = 'Campanyes';