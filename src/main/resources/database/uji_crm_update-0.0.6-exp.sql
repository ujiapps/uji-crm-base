ALTER TABLE UJI_CRM.CRM_CAMPANYAS_CLIENTES ADD (ESTADO VARCHAR2(400 BYTE));

ALTER TABLE crm_clientes ADD (identificacion varchar2(200));

CREATE TABLE uji_crm.crm_campanyas_envios_auto
(
  id          NUMBER NOT NULL ,
  campanya_id NUMBER NOT NULL ,
  tipo_id     NUMBER NOT NULL ,
  asunto      VARCHAR2 (200) NOT NULL ,
  cuerpo      VARCHAR2 (4000 BYTE) NOT NULL
);
CREATE INDEX uji_crm.crm_campanyas_env_auto_FK1_IX ON uji_crm.crm_campanyas_envios_auto (campanya_id ASC);
CREATE INDEX uji_crm.crm_campanyas_env_auto_FK2_IX ON uji_crm.crm_campanyas_envios_auto (tipo_id ASC);
ALTER TABLE uji_crm.crm_campanyas_envios_auto ADD CONSTRAINT crm_campanyas_envios_auto_PK PRIMARY KEY (id);
ALTER TABLE uji_crm.crm_campanyas_envios_auto ADD CONSTRAINT crm_campanyas_envios_auto_FK1 FOREIGN KEY (campanya_id) REFERENCES uji_crm.crm_campanyas (id);
ALTER TABLE uji_crm.crm_campanyas_envios_auto ADD CONSTRAINT crm_campanyas_envios_auto_FK2 FOREIGN KEY (tipo_id) REFERENCES uji_crm.crm_tipos(id);

ALTER TABLE crm_clientes ADD
(
  nombre_oficial    varchar2(200),
  apellidos_oficial varchar2(200)
);

ALTER TABLE crm_clientes ADD
(
  busqueda AS (UPPER(TRANSLATE(LOWER(nombre_oficial|| ' '||apellidos_oficial||' '||nombre_oficial|| ' '||nombre||' '||identificacion|| ' ' ||correo||' '||movil),'áéíóúàèìòùäëïöüâêîôû','aeiouaeiouaeiouaeiou'))) virtual
);