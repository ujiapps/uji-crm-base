ALTER TABLE UJI_CRM.CRM_CAMPANYAS ADD (SOLICITAR_POSTAL NUMBER);
ALTER TABLE UJI_CRM.CRM_CAMPANYAS ADD (VINCULACION_ID NUMBER);

ALTER TABLE UJI_CRM.CRM_CAMPANYAS_DATOS ADD(REQUERIDO NUMBER(1));
ALTER TABLE UJI_CRM.CRM_CAMPANYAS_DATOS ADD(TIPO_ACCESO_ID NUMBER);

ALTER TABLE UJI_CRM.CRM_CAMPANYAS_DATOS ADD
CONSTRAINT CRM_CAMPANYAS_DATOS_FK5
 FOREIGN KEY (TIPO_ACCESO_ID)
 REFERENCES UJI_CRM.CRM_TIPOS (ID);

ALTER TABLE UJI_CRM.CRM_CLIENTES ADD (CLAVE VARCHAR2(200));

create table crm_envios_criterios
(
 id  number primary key,
 envio_id number,
 tipo number,
 origen varchar2(10),
 referencia1_id number,
 referencia2_id number
)

alter table crm_envios_criterios
add constraint crm_envios_criterios_fk1 foreign key (envio_id) references crm_envios (id);

create index crm_envios_criterios_fk1_i on crm_envios_criterios (envio_id);

ALTER TABLE UJI_CRM.CRM_ENVIOS_CRITERIOS ADD (
  CONSTRAINT CRM_ENVIOS_CRITERIOS_UN1
 UNIQUE (ENVIO_ID, TIPO, REFERENCIA1_ID, REFERENCIA2_ID));


CREATE OR REPLACE FORCE VIEW UJI_CRM.CRM_VW_ENVIOS_CRITERIOS(
  id,
  envio_id,
  tipo_envio,
  tipo,
  origen,
  referencia1_id,
  referencia2_id,
  nombre,
  count_correo,
  count_sms,
  count_app_movil,
  count_postal)
AS
  select ec.id, ec.envio_id, v.tipo_id, ec.tipo, ec.origen, ec.referencia1_id, ec.referencia2_id, i.nombre_ca,
         sum(decode(c.correo, null, 0, decode(c.correo_estado_id, 3, 0, ci.correo))), sum(decode(c.movil, null, 0, decode(c.movil_estado_id, 3, 0, ci.sms))),
         sum(decode(c.movil, null, 0, decode(c.movil_estado_id, 3, 0, ci.bt))), sum(decode(c.postal, null, 0, decode(c.postal_estado_id, 3, 0, ci.postal)))
    from crm_envios_criterios ec, crm_envios v, crm_items i, crm_clientes_items ci,
      crm_clientes c
   where ec.referencia1_id = i.id
     and i.id = ci.item_id (+)
     and ci.cliente_id = c.id (+)
     and ec.envio_id = v.id
     and origen = 'Grupos'
   group by ec.id, ec.envio_id, v.tipo_id, ec.tipo, ec.origen, ec.referencia1_id, ec.referencia2_id, i
      .nombre_ca
  union
  select ec.id, ec.envio_id, v.tipo_id, ec.tipo, ec.origen, ec.referencia1_id, ec.referencia2_id, e.nombre,
         sum(decode(c.correo, null, 0, decode(c.correo_estado_id, 3, 0, 1))), sum(decode(c.movil, null, 0, decode(c.movil_estado_id, 3 , 0, 1))),
         sum(decode(c.movil, null, 0, decode(c.movil_estado_id, 3, 0, 1))), sum(decode(c.postal, null, 0, decode(c.postal_estado_id, 3, 0, 1)))
    from crm_envios_criterios ec, crm_envios v, crm_etiquetas e, crm_clientes_etiquetas ce, crm_clientes c
   where ec.referencia1_id = e.id
     and e.id = ce.etiqueta_id (+)
     and ce.cliente_id = c.id (+)
     and ec.envio_id = v.id
     and origen = 'Etiquetes'
   group by ec.id, ec.envio_id, v.tipo_id, ec.tipo, ec.origen, ec.referencia1_id, ec.referencia2_id, e.nombre
  union
  select ec.id, ec.envio_id, v.tipo_id, ec.tipo, ec.origen, ec.referencia1_id, ec.referencia2_id, ca.nombre ||' ('||t.nombre||')',
              nvl((select sum(decode (c.correo, null, 0, decode (c.correo_estado_id, 3, 0, 1))) from crm_campanyas_clientes cc, crm_clientes c where cc.cliente_id = c.id and campanya_id = ec.referencia1_id and ec.referencia2_id = cc.tipo_id), 0),
              nvl((select sum(decode (c.movil, null, 0, decode (c.movil_estado_id, 3, 0, 1))) from crm_campanyas_clientes cc, crm_clientes c where cc.cliente_id = c.id and campanya_id = ec.referencia1_id and ec.referencia2_id = cc.tipo_id), 0),
              nvl((select sum(decode (c.movil, null, 0, decode (c.movil_estado_id, 3, 0, 1))) from crm_campanyas_clientes cc, crm_clientes c where cc.cliente_id = c.id and campanya_id = ec.referencia1_id and ec.referencia2_id = cc.tipo_id), 0),
              nvl((select sum(decode (c.postal, null, 0, decode (c.postal_estado_id, 3, 0, 1))) from crm_campanyas_clientes cc, crm_clientes c where cc.cliente_id = c.id and campanya_id = ec.referencia1_id and ec.referencia2_id = cc.tipo_id), 0)
    from crm_envios_criterios ec, crm_envios v, crm_campanyas ca, crm_tipos t
   where ec.referencia1_id = ca.id
     and ec.envio_id = v.id
     and t.id = ec.referencia2_id
     and origen = 'Campanyes';


create or replace view crm_vw_envios_clientes
(
   id,
   correo,
   correo_estado,
   correo_estado_item,
   movil,
   movil_estado,
   sms_estado_item,
   app_movil_estado_item,
   postal,
   postal_estado,
   postal_estado_item,
   envio_tipo,
   envio_id
)
as
   select   id,
            correo,
            correo_estado,
            correo_estado_item,
            movil,
            movil_estado,
            sms_estado_item,
            app_movil_estado_item,
            postal,
            postal_estado,
            postal_estado_item,
            envio_tipo,
            envio_id
     from   (select   c.id,
                      c.correo,
                      c.correo_estado_id correo_estado,
                      ci.correo correo_estado_item,
                      c.movil,
                      c.movil_estado_id movil_estado,
                      ci.sms sms_estado_item,
                      ci.bt app_movil_estado_item,
                      c.postal,
                      c.postal_estado_id postal_estado,
                      ci.postal postal_estado_item,
                      v.tipo_id envio_tipo,
                      v.id envio_id
               from   crm_envios_criterios ec,
                      crm_envios v,
                      crm_items i,
                      crm_clientes_items ci,
                      crm_clientes c
              where       ec.referencia1_id = i.id
                      and i.id = ci.item_id
                      and ci.cliente_id = c.id
                      and ec.envio_id = v.id
                      and origen = 'Grupos'
                      and ec.tipo = 1
             union
             select   c.id,
                      c.correo,
                      c.correo_estado_id,
                      1,
                      c.movil,
                      c.movil_estado_id,
                      1,
                      1,
                      c.postal,
                      c.postal_estado_id,
                      1,
                      v.tipo_id,
                      v.id
               from   crm_envios_criterios ec,
                      crm_envios v,
                      crm_etiquetas e,
                      crm_clientes_etiquetas ce,
                      crm_clientes c
              where       ec.referencia1_id = e.id
                      and ec.envio_id = v.id
                      and e.id = ce.etiqueta_id
                      and ce.cliente_id = c.id
                      and origen = 'Etiquetes'
                      and ec.tipo = 1
             union
             select   c.id,
                      c.correo,
                      c.correo_estado_id,
                      1,
                      c.movil,
                      c.movil_estado_id,
                      1,
                      1,
                      c.postal,
                      c.postal_estado_id,
                      1,
                      v.tipo_id,
                      v.id
               from   crm_envios_criterios ec,
                      crm_envios v,
                      crm_campanyas ca,
                      crm_campanyas_clientes cc,
                      crm_clientes c
              where       ec.referencia1_id = ca.id
                      and ec.envio_id = v.id
                      and cc.cliente_id = c.id
                      and cc.campanya_id = ca.id
                      and cc.tipo_id = ec.referencia2_id
                      and origen = 'Campanyes'
                      and ec.tipo = 1) x
    where   (x.id in
                   (select   c.id
                      from   crm_envios_criterios ec,
                             crm_envios v,
                             crm_items i,
                             crm_clientes_items ci,
                             crm_clientes c
                     where       ec.referencia1_id = i.id
                             and i.id = ci.item_id
                             and ci.cliente_id = c.id
                             and ec.envio_id = v.id
                             and origen = 'Grupos'
                             and ec.tipo = 2
                    union
                    select   c.id
                      from   crm_envios_criterios ec,
                             crm_envios v,
                             crm_etiquetas e,
                             crm_clientes_etiquetas ce,
                             crm_clientes c
                     where       ec.referencia1_id = e.id
                             and ec.envio_id = v.id
                             and e.id = ce.etiqueta_id
                             and ce.cliente_id = c.id
                             and origen = 'Etiquetes'
                             and ec.tipo = 2
                    union
                    select   c.id
                      from   crm_envios_criterios ec,
                             crm_envios v,
                             crm_campanyas ca,
                             crm_campanyas_clientes cc,
                             crm_clientes c
                     where       ec.referencia1_id = ca.id
                             and ec.envio_id = v.id
                             and cc.cliente_id = c.id
                             and cc.campanya_id = ca.id
                             and cc.tipo_id = ec.referencia2_id
                             and origen = 'Campanyes'
                             and ec.tipo = 2)
             or (select   count ( * )
                   from   (select   c.id
                             from   crm_envios_criterios ec,
                                    crm_envios v,
                                    crm_items i,
                                    crm_clientes_items ci,
                                    crm_clientes c
                            where       ec.referencia1_id = i.id
                                    and i.id = ci.item_id
                                    and ci.cliente_id = c.id
                                    and ec.envio_id = v.id
                                    and origen = 'Grupos'
                                    and ec.tipo = 2
                           union
                           select   c.id
                             from   crm_envios_criterios ec,
                                    crm_envios v,
                                    crm_etiquetas e,
                                    crm_clientes_etiquetas ce,
                                    crm_clientes c
                            where       ec.referencia1_id = e.id
                                    and ec.envio_id = v.id
                                    and e.id = ce.etiqueta_id
                                    and ce.cliente_id = c.id
                                    and origen = 'Etiquetes'
                                    and ec.tipo = 2
                           union
                           select   c.id
                             from   crm_envios_criterios ec,
                                    crm_envios v,
                                    crm_campanyas ca,
                                    crm_campanyas_clientes cc,
                                    crm_clientes c
                            where       ec.referencia1_id = ca.id
                                    and ec.envio_id = v.id
                                    and cc.cliente_id = c.id
                                    and cc.campanya_id = ca.id
                                    and cc.tipo_id = ec.referencia2_id
                                    and origen = 'Campanyes'
                                    and ec.tipo = 2)) = 0);



CREATE OR REPLACE FORCE VIEW UJI_CRM.CRM_EXT_VINCULACIONES
(
   ID,
   NOMBRE
)
AS
  select id, nombre
    from gri_per.per_subvinculos
   where vin_id = 12;



CREATE TABLE UJI_CRM.CRM_MOVIMIENTOS
(
  ID           NUMBER                           NOT NULL,
  FECHA        DATE                             NOT NULL,
  CLIENTE_ID   NUMBER                           NOT NULL,
  TIPO_ID      NUMBER                           NOT NULL,
  DESCRIPCION  VARCHAR2(4000 CHAR),
  CAMPANYA_ID  NUMBER
);


CREATE UNIQUE INDEX UJI_CRM.CRM_MOVIMIENTOS_PK ON UJI_CRM.CRM_MOVIMIENTOS (ID);
CREATE INDEX UJI_CRM.CRM_MOVIMIENTOS_R01_I ON UJI_CRM.CRM_MOVIMIENTOS(CLIENTE_ID);
CREATE INDEX UJI_CRM.CRM_MOVIMIENTOS_R02_I ON UJI_CRM.CRM_MOVIMIENTOS(TIPO_ID);

CREATE OR REPLACE TRIGGER UJI_CRM.crm_movimientos_id
before insert ON UJI_CRM.CRM_MOVIMIENTOS for each row
begin
  if :new.id is null then
    :new.id := hibernate_sequence.nextval;
  end if;
end;
/

ALTER TABLE UJI_CRM.CRM_MOVIMIENTOS ADD (CONSTRAINT CRM_MOVIMIENTOS_PK PRIMARY KEY (ID));
ALTER TABLE UJI_CRM.CRM_MOVIMIENTOS ADD (CONSTRAINT CRM_MOVIMIENTOS_R01 FOREIGN KEY (CLIENTE_ID) REFERENCES UJI_CRM.CRM_CLIENTES (ID),
                                         CONSTRAINT CRM_MOVIMIENTOS_R02 FOREIGN KEY (TIPO_ID) REFERENCES UJI_CRM.CRM_TIPOS (ID));

CREATE TABLE UJI_CRM.CRM_ITEMS_ITEMS
(
  ID               NUMBER,
  ITEM_ID_ORIGEN   NUMBER                       NOT NULL,
  ITEM_ID_DESTINO  NUMBER                       NOT NULL
);

CREATE INDEX UJI_CRM.CRM_ITEMS_ITEMS_R01_I ON UJI_CRM.CRM_ITEMS_ITEMS (ITEM_ID_ORIGEN);
CREATE INDEX UJI_CRM.CRM_ITEMS_ITEMS_R02_I ON UJI_CRM.CRM_ITEMS_ITEMS(ITEM_ID_DESTINO);
ALTER TABLE UJI_CRM.CRM_ITEMS_ITEMS ADD (PRIMARY KEY (ID));

ALTER TABLE UJI_CRM.CRM_ITEMS_ITEMS ADD (CONSTRAINT CRM_ITEMS_ITEMS_R01 FOREIGN KEY (ITEM_ID_ORIGEN) REFERENCES UJI_CRM.CRM_ITEMS (ID),
                                         CONSTRAINT CRM_ITEMS_ITEMS_R02 FOREIGN KEY (ITEM_ID_DESTINO) REFERENCES UJI_CRM.CRM_ITEMS (ID));


ALTER TABLE UJI_CRM.CRM_CLIENTES_DATOS ADD (ITEM_ID  NUMBER);
ALTER TABLE UJI_CRM.CRM_CLIENTES_DATOS ADD CONSTRAINT CRM_CLIENTES_DATOS_FK4 FOREIGN KEY (ITEM_ID) REFERENCES UJI_CRM.CRM_ITEMS (ID);

ALTER TABLE UJI_CRM.CRM_CAMPANYAS_DATOS ADD(CAMPANYA_DATO_ID NUMBER);
ALTER TABLE UJI_CRM.CRM_CAMPANYAS_DATOS ADD CONSTRAINT CRM_CAMPANYAS_DATOS_FK5 FOREIGN KEY (CAMPANYA_DATO_ID) REFERENCES UJI_CRM.CRM_CAMPANYAS_DATOS (ID);
CREATE INDEX UJI_CRM.CRM_CAMPANYAS_DATOS_FK3X ON UJI_CRM.CRM_CAMPANYAS_DATOS (CAMPANYA_DATO_ID);


ALTER TABLE UJI_CRM.CRM_CAMPANYAS_DATOS RENAME COLUMN TEXTO TO TEXTO_CA;

CREATE TABLE UJI_CRM.CRM_CAMPANYAS_TEXTOS
(
  ID        NUMBER,
  NOMBRE    VARCHAR2(100)                       NOT NULL,
  TEXTO_CA  VARCHAR2(4000),
  TEXTO_ES  VARCHAR2(4000),
  TEXTO_UK  VARCHAR2(4000)
);


ALTER TABLE UJI_CRM.CRM_CAMPANYAS_TEXTOS ADD (CONSTRAINT CRM_CAMPANYAS_TEXTOS_PK PRIMARY KEY (id));

ALTER TABLE UJI_CRM.CRM_CAMPANYAS ADD (CAMPANYA_TEXTO_ID  NUMBER NOT NULL);
ALTER TABLE UJI_CRM.CRM_CAMPANYAS MODIFY(CAMPANYA_TEXTO_ID  DEFAULT 1);

ALTER TABLE UJI_CRM.CRM_CAMPANYAS ADD CONSTRAINT CRM_CAMPANYAS_FK3 FOREIGN KEY (CAMPANYA_TEXTO_ID) REFERENCES UJI_CRM.CRM_CAMPANYAS_TEXTOS (ID);


