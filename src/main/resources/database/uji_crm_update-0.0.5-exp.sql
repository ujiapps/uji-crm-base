CREATE OR REPLACE FORCE VIEW UJI_CRM.CRM_V_PERSONAS_PROGRAMAS (
   ID,
   PERSONA_ID,
   PROGRAMA_ID,
   TIPO_ACCESO
   ) AS
   select p.id * 10000000 + su.persona_id * 10 + 1, su.persona_id, p.id, 'ADMIN'
   from    crm_programas p
   join
      crm_servicios_usuarios su
   on su.servicio_id = p.servicio_id
   UNION
   select pu.programa_id * 10000000 + pu.persona_id * 10 + decode (tipo, 'ADMIN', 1, 0), pu.persona_id, pu.programa_id
   , pu.tipo
   from crm_programas_usuarios pu;