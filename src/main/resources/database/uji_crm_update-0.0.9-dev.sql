CREATE TABLE UJI_CRM.CRM_EXT_UBIC_ESTRUCTURALES
(
  ID      NUMBER,
  NOMBRE  VARCHAR2(200 BYTE)                    NOT NULL
);


create or replace force view crm_vw_items as
select distinct i.*, g.programa_id from crm_items i, crm_grupos g where I.GRUPO_ID = g.id;

CREATE TABLE UJI_CRM.CRM_CAMPANYAS_CLI_ANYADIR_AUTO
(
  CAMPANYA_ID   NUMBER,
  ESTADO_ID     NUMBER,
  ID            NUMBER                          NOT NULL,
  CLIENTES_IDS  CLOB
);


CREATE UNIQUE INDEX UJI_CRM.CRM_CAMPANYAS_CLI_ANYADIR_PK ON UJI_CRM.CRM_CAMPANYAS_CLI_ANYADIR_AUTO
(ID);

CREATE OR REPLACE TRIGGER UJI_CRM.crm_campanyas_cli_anyadir_id
before insert ON UJI_CRM.CRM_CAMPANYAS_CLI_ANYADIR_AUTO for each row
begin
  if :new.id is null then
    :new.id := hibernate_sequence.nextval;
  end if;
end;
/

ALTER TABLE UJI_CRM.CRM_CAMPANYAS_CLI_ANYADIR_AUTO ADD (
  CONSTRAINT CRM_CAMPANYAS_CLI_ANYADIR_PK
 PRIMARY KEY
 (ID));

 ALTER TABLE UJI_CRM.CRM_CAMPANYAS_CLI_ANYADIR_AUTO ADD (
  CONSTRAINT CRM_CAMPANYAS_CLI_ANYADIR_FK1
 FOREIGN KEY (CAMPANYA_ID)
 REFERENCES UJI_CRM.CRM_CAMPANYAS (ID),
  CONSTRAINT CRM_CAMPANYAS_CLI_ANYADIR_FK2
 FOREIGN KEY (ESTADO_ID)
 REFERENCES UJI_CRM.CRM_TIPOS (ID));


create or replace
trigger UJI_CRM.crm_camp_cli_anyadir
after insert ON UJI_CRM.CRM_CAMPANYAS_CLI_ANYADIR_AUTO
for each row
begin
  subvinculos.altaMasimaClientesCampanya(:new.clientes_ids, :new.campanya_id, :new.estado_id);
end;


Paquete Subvinculos :
   procedure altaMasimaClientesCampanya(clientes_ids in clob, p_campanya in number, p_estado in number) is

    ALTAMASIVA      number := 63;

    vClienteId      number;
    vIni            number;
    vFin            number;
    vId             number;

  begin

    vIni := 1;
    vFin := 0;
    while vFin < length(clientes_ids) loop

      vFin := instr(clientes_ids, ',', vIni);
      if vFin != 0 then
        vClienteId := to_number(substr(clientes_ids, vIni, vFin - vIni));
        vIni := vFin + 1;
      else
        if length(clientes_ids) > 0 then
          vClienteId := to_number(substr(clientes_ids, vIni, length(clientes_ids) - (vIni-1)));
        end if;
        vFin := length(clientes_ids) + 1;
      end if;

      begin
        vId := hibernate_sequence.nextval;
        insert into crm_campanyas_clientes (id, cliente_id, campanya_id, tipo_id) values (vId, vClienteId, p_campanya, p_estado);

        altaClienteCampanya(vId);
        insert into crm_movimientos (fecha, cliente_id, tipo_id, campanya_id) values (sysdate, vClienteId, ALTAMASIVA, p_campanya);
        envia_correos.envia_automaticos(vClienteId, p_estado, p_campanya);
      exception when others then null;
      end;
    end loop;

  end;


  Paquete Envia_correos :

  procedure envia_automaticos (p_cliente in number, p_tipo in number, p_campanya in number) is

    vCuerpo                varchar2(32000);
    vCorreo                varchar2(100);
    vHash                  varchar2(100);
    vAsunto                varchar2(200);
    vNombre                varchar2(200);
    vTipoMov               number;
    vEnvioId               number;
    vDesde                 varchar2(200);
    vResponder             varchar2(200);


  begin
--    vHash := lower(dbms_crypto.hash(utl_i18n.string_to_raw (systimestamp||'sdkskljw', 'UTF8'), 3));
    begin
      insert into crm_sesiones (id, fecha, fecha_caducidad, cliente_id, tipo_id)
      values (vHash, sysdate, sysdate+90, p_cliente, 0);
      commit;
    exception
      when others then null;
    end;

    select c.correo, parsear_envio(cuerpo, c.id, e.campanya_id, vHash), asunto, e.nombre, e.desde, e.responder_a
      into vCorreo, vCuerpo, vAsunto, vNombre, vDesde, vResponder
      from crm_clientes c, crm_campanyas_envios_auto e
     where c.id = p_cliente
       and e.campanya_id = p_campanya
       and e.tipo_id = p_tipo;

    vTipoMov := case p_tipo when PROPUESTO then MOVPROPUESTO when DEFINITIVO then MOVDEFINITIVO when BAJA then MOVBAJA end;

    insert into crm_movimientos (fecha, cliente_id, tipo_id, descripcion, campanya_id)
    values (sysdate, p_cliente, vTipoMov, vCorreo, p_campanya);

    if vCorreo is not null then
      insert into crm_envios (nombre, cuerpo, persona_id, asunto, fecha, tipo_id, fecha_envio_destinatario, desde, responder_a)
      values (nvl(vNombre, NOMBRE), vCuerpo, UJI, vAsunto, sysdate, MAIL, sysdate, vDesde, vResponder) returning id into vEnvioId;

      insert into crm_envios_clientes (envio_id, cliente_id, para)
      values (vEnvioId, p_cliente, vCorreo);
    end if;
    commit;

  exception
    when others then null;
  end;

  insert into crm_tipos (id, nombre, tabla_columna) values (64, 'Entre', 'enviosCriterios.tipoComparacion');

    CREATE OR REPLACE FORCE VIEW "UJI_CRM"."CRM_VW_CAMP_CLIENTE_BUSQUEDA" ("CAMPANYA_CLIENTE_ID", "NUM_SEGUIMIENTOS", "CLIENTE_ID", "CAMPANYA_ID", "ESTADO_CAMPANYA_ID", "ESTADO", "APELLIDOS_OFICIAL", "BUSQUEDA", "CORREO", "ETIQUETA", "IDENTIFICACION", "MOVIL", "NOMBRE_OFICIAL", "PERSONA_ID", "POSTAL", "CLIENTE_TIPO", "CAMPANYA_PADRE_ID", "CAMPANYA_DESCRIPCION", "CAMPANYA_NOMBRE", "PROGRAMA_ID", "ETIQUETAS") AS
  select distinct cc.id as campanya_cliente_id, (select count(s.id)
                                                    from crm_seguimientos s
                                                   where s.campanya_id = cc.campanya_id
                                                     and s.cliente_id = cc.cliente_id)
                                                    as num_seguimientos, c.id as cliente_id, ca.id as campanya_id, cc.tipo_id as estado_campanya_id, cc.estado as estado, c.apellidos_oficial, c.busqueda, c.correo, c.etiqueta,
                   c.identificacion, c.movil, c.nombre_oficial, c.persona_id, c.postal, c.tipo as cliente_tipo, ca.campanya_id as campanya_padre_id, ca.descripcion as campanya_descripcion, ca.nombre as campanya_nombre, ca.programa_id,
                   (select wm_concat(e.nombre)
                      from crm_etiquetas e, crm_clientes_etiquetas ce
                     where e.id = ce.etiqueta_id
                       and ce.cliente_id = c.id)
                      etiquetas
     from crm_clientes c, crm_campanyas_clientes cc, crm_campanyas ca
    where c.id = cc.cliente_id(+)
      and cc.campanya_id = ca.id (+) ;


  CREATE TABLE "UJI_CRM"."CRM_CAMPANYAS_ENVIOS_ADMIN"
   (	"ID" NUMBER NOT NULL ENABLE,
	"CAMPANYA_ID" NUMBER NOT NULL ENABLE,
	"TIPO_ID" NUMBER NOT NULL ENABLE,
	"ASUNTO" VARCHAR2(200 BYTE) NOT NULL ENABLE,
	"CUERPO" VARCHAR2(4000 BYTE) NOT NULL ENABLE,
	"EMAILS" VARCHAR2(4000 BYTE) NOT NULL ENABLE,
	 CONSTRAINT "CRM_CAMPANYAS_ENV_ADMIN_FK1" FOREIGN KEY ("CAMPANYA_ID")
	  REFERENCES "UJI_CRM"."CRM_CAMPANYAS" ("ID") ENABLE,
	 CONSTRAINT "CRM_CAMPANYAS_ENV_ADMIN_FK2" FOREIGN KEY ("TIPO_ID")
	  REFERENCES "UJI_CRM"."CRM_TIPOS" ("ID") ENABLE
   );

  CREATE INDEX "UJI_CRM"."CRM_CAMPANYAS_ENV_ADMIN_FK1_IX" ON "UJI_CRM"."CRM_CAMPANYAS_ENVIOS_ADMIN" ("CAMPANYA_ID");
  CREATE INDEX "UJI_CRM"."CRM_CAMPANYAS_ENV_ADMIN_FK2_IX" ON "UJI_CRM"."CRM_CAMPANYAS_ENVIOS_ADMIN" ("TIPO_ID");


CREATE OR REPLACE TRIGGER "UJI_CRM"."CRM_CAMPANYA_CLI_ENVIO_ADMIN"
AFTER insert OR UPDATE ON UJI_CRM.CRM_CAMPANYAS_CLIENTES
for each row
begin
  envia_correos.envia_correo_admin(:new.campanya_id, :new.tipo_id, :new.cliente_id);
end;
/

--PAQUETE ENVIA_CORREO

 procedure envia_correo_admin(p_campanya in number, p_tipo in number, p_cliente in number) is

      vAsunto           varchar2(100);
      vCuerpo           clob;
      vCorreos          varchar2(1000);
      vEnvioId          number;
      vPuntoYComaIni    number;
      vPuntoYComaFin    number;
      vEmail            varchar2(1000);

   begin

      begin
         select asunto, cuerpo, emails
           into vAsunto, vCuerpo, vCorreos
           from crm_campanyas_envios_admin
          where campanya_id = p_campanya
            and tipo_id = p_tipo;

         if vCorreos is not null then

            vCuerpo := parsear_envio_admin (p_texto=> vCuerpo, p_cliente=> p_cliente, p_estado=> p_tipo);

            vEnvioId := uji_mensajeria.envios_crm.dame_id;
            uji_mensajeria.envios_crm.mensaje(vEnvioId, 'MAIL', vAsunto, vCuerpo, null, sysdate, 'noreply@uji.es', 'noreply@uji.es');

            while nullif(length(vCorreos), 0) is not null loop
               vPuntoYComaIni := 0;
               vPuntoYComaFin := instr(vCorreos, ';');
               if vPuntoYComaFin != 0 then
                  vEmail := substr(vCorreos, vPuntoYComaIni, vPuntoYComaFin-1);
                  vCorreos := substr(vCorreos, vPuntoYComaFin+1);
               else
                  vEmail := vCorreos;
                  vCorreos := '';
               end if;

               if vEmail is not null then
                  uji_mensajeria.envios_crm.destinatario(vEnvioId, vEmail);
               end if;
            end loop;
         end if;

      exception when others then null;
      end;

   exception when others then null;
   end;

   -- FUNCIONES ADICIONALES

   create or replace
function         parsear_envio_admin (p_texto in clob, p_cliente in number, p_estado in number) return clob is

  vTexto                    clob;
  vEstado                   varchar2(1000);
  vNombre                   varchar2(1000);

begin

  vTexto := p_texto;

  if instr(vTexto, '$[cliente]') > 0 then
    select nvl(c.nombre_oficial||' '||c.apellidos_oficial, c.nombre_comercial)
      into vNombre
      from crm_clientes c
     where c.id = p_cliente;

    vTexto := replace(vTexto, '$[cliente]', vNombre);
  end if;


  if instr(vTexto, '$[estado]') > 0 then

    select nombre
      into vEstado
      from crm_tipos t
     where t.id = p_estado;

    vTexto := replace(vTexto, '$[estado]', vEstado);
  end if;

  return vTexto;

exception
  when others then return null;
end;

   CREATE TABLE "UJI_CRM"."CRM_CAMPANYAS_CARTAS"
   (	"ID" NUMBER NOT NULL ENABLE,
	"CAMPANYA_ID" NUMBER NOT NULL ENABLE,
	"TIPO_ID" NUMBER NOT NULL ENABLE,
	"CUERPO" VARCHAR2(4000 BYTE) NOT NULL ENABLE);
	 ALTER TABLE CRM_CAMPANYAS_CARTAS ADD(CONSTRAINT "CRM_CAMPANYAS_CARTAS_PK" PRIMARY KEY ("ID"));

	 ALTER TABLE CRM_CAMPANYAS_CARTAS ADD(CONSTRAINT "CRM_CAMPANYAS_CARTAS_FK1" FOREIGN KEY ("CAMPANYA_ID")
	  REFERENCES "UJI_CRM"."CRM_CAMPANYAS" ("ID") );
	 ALTER TABLE CRM_CAMPANYAS_CARTAS ADD(CONSTRAINT "CRM_CAMPANYAS_CARTAS_FK2" FOREIGN KEY ("TIPO_ID")
	  REFERENCES "UJI_CRM"."CRM_TIPOS" ("ID"));

  CREATE INDEX "UJI_CRM"."CRM_CAMPANYAS_CARTAS_FK1_IX" ON "UJI_CRM"."CRM_CAMPANYAS_CARTAS" ("CAMPANYA_ID");
  CREATE INDEX "UJI_CRM"."CRM_CAMPANYAS_CARTAS_FK2_IX" ON "UJI_CRM"."CRM_CAMPANYAS_CARTAS" ("TIPO_ID");


 CREATE TABLE "UJI_CRM"."CRM_CAMPANYAS_CAMPANYAS"
   (	"ID" NUMBER,
	"campanya_id_origen" NUMBER NOT NULL ENABLE,
	"campanya_id_destino" NUMBER NOT NULL ENABLE,
	"estado_id_origen" NUMBER NOT NULL ENABLE,
	"estado_id_destino" NUMBER NOT NULL ENABLE);

	 ALTER TABLE UJI_CRM.CRM_CAMPANYAS_CAMPANYAS ADD ( CONSTRAINT "CRM_CAMPANYAS_CAMPANYAS_PK" PRIMARY KEY ("ID"));
	 ALTER TABLE UJI_CRM.CRM_CAMPANYAS_CAMPANYAS ADD (CONSTRAINT "crm_campanyas_campanyas_FK1" FOREIGN KEY ("campanya_id_origen")
	  REFERENCES "UJI_CRM"."CRM_CAMPANYAS" ("ID"));
	 ALTER TABLE UJI_CRM.CRM_CAMPANYAS_CAMPANYAS ADD (CONSTRAINT "crm_campanyas_campanyas_FK2" FOREIGN KEY ("campanya_id_destino")
	  REFERENCES "UJI_CRM"."CRM_CAMPANYAS" ("ID"));
	 ALTER TABLE UJI_CRM.CRM_CAMPANYAS_CAMPANYAS ADD (CONSTRAINT "crm_campanyas_campanyas_FK3" FOREIGN KEY ("estado_id_destino")
	  REFERENCES "UJI_CRM"."CRM_TIPOS" ("ID"));
	 ALTER TABLE UJI_CRM.CRM_CAMPANYAS_CAMPANYAS ADD (CONSTRAINT "crm_campanyas_campanyas_FK4" FOREIGN KEY ("estado_id_origen")
	  REFERENCES "UJI_CRM"."CRM_TIPOS" ("ID"));