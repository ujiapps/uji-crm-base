 CREATE TABLE "UJI_CRM"."CRM_CORREOS_NO_VALIDOS_CONTROL" 
   (	"ID" NUMBER NOT NULL ENABLE, 
	"CORREO_NO_VALIDO_ID" NUMBER NOT NULL ENABLE, 
	"MODIFICADO" NUMBER(1,0) DEFAULT 0, 
	 CONSTRAINT "CRM_CORREOS_NO_VALIDOS_CO_PK" PRIMARY KEY ("ID"));


  CREATE OR REPLACE FORCE VIEW "UJI_CRM"."CRM_EXT_RECIBOS" ("ID", "FECHA_CREACION", "FECHA_PAGO", "IMPORTE_NETO", "TIPO_RECIBO_ID", "TIPO_RECIBO_NOMBRE", "TIPO_COBRO_ID", "TIPO_COBRO_NOMBRE", "FECHA_PAGO_LIMITE", "PERSONA_ID", "OBSERVACIONES", "DESCRIPCION", "IDIOMA", "TPV_CORREO", "CUENTA_BANCARIA_ABONO", "CTA_CLIENTE", "DESTINATARIO", "PIE", "EMISORA_ID", "MOROSO", "EXPORTADO") AS
  select r."ID",r."FECHA_CREACION",r."FECHA_PAGO",r."IMPORTE_NETO",r."TIPO_RECIBO_ID",r."TIPO_RECIBO_NOMBRE",r."TIPO_COBRO_ID",r."TIPO_COBRO_NOMBRE",r."FECHA_PAGO_LIMITE",r."PERSONA_ID",r."OBSERVACIONES",r."DESCRIPCION",r."IDIOMA",r."TPV_CORREO",r."CUENTA_BANCARIA_ABONO",r."CTA_CLIENTE",r."DESTINATARIO",r."PIE",r."EMISORA_ID",r."MOROSO", decode((select count(*) from crm_ext_movimientos_recibos m, crm_recibos_movimientos_export e where m.recibo_id = r.id and m.id = e.recibo_movimiento_id and e.valido = 1), 0, 0, (select count (*) from crm_ext_movimientos_recibos m where m.recibo_id = r.id), 1, 2 ) exportado
  from crm_ext_recibos_aux r;


   CREATE TABLE "UJI_CRM"."CRM_RECIBOS_REMESAS_EXPORT"
   (	"ID" NUMBER NOT NULL ENABLE,
	"FECHA" DATE DEFAULT sysdate NOT NULL ENABLE,
	 CONSTRAINT "CRM_RECIBOS_REMESAS_EXPOR_PK" PRIMARY KEY ("ID"));

	 	 CREATE TABLE "UJI_CRM"."CRM_RECIBOS_MOVIMIENTOS_EXPORT"
   (	"ID" NUMBER NOT NULL ENABLE,
	"RECIBO_MOVIMIENTO_ID" NUMBER NOT NULL ENABLE,
	"REMESA_ID" NUMBER,
	"VALIDO" NUMBER(1,0) DEFAULT 1 NOT NULL ENABLE,
	 CONSTRAINT "CRM_RECIBOS_MOVIMIENTOS_E_PK" PRIMARY KEY ("ID"));


  CREATE OR REPLACE FORCE VIEW "UJI_CRM"."CRM_EXT_MOVIMIENTOS_RECIBOS" ("ID", "REMESA_ID", "FECHA", "DESCRIPCION", "TIPO_ID", "TIPO_NOMBRE", "RECIBO_ID", "LINEA_ID", "IMPORTE", "SOLICITANTE_NOMBRE", "FIRMANTE_NOMBRE", "CTA_CLIENTE", "CTA_UJI", "TIPO_CONCILIA", "FECHA_CONCILIA", "SIGNO_CONCILIA", "MOVIMIENTO_ID", "FECHA_INS", "EXPEDIENTE", "REMESA", "FECHA_EXPORTACION", "ID_EXPORTADO") AS
   select m."ID",m."REMESA_ID",m."FECHA",m."DESCRIPCION",m."TIPO_ID",m."TIPO_NOMBRE",m."RECIBO_ID",m."LINEA_ID",m."IMPORTE",m."SOLICITANTE_NOMBRE",m."FIRMANTE_NOMBRE",m."CTA_CLIENTE",m."CTA_UJI",m."TIPO_CONCILIA",m."FECHA_CONCILIA",m."SIGNO_CONCILIA",m."MOVIMIENTO_ID",m."FECHA_INS",m."EXPEDIENTE", e.remesa_id remesa, r.fecha fecha_exportacion, e.id
    from crm_ext_movimientos_rec_aux m left join crm_recibos_movimientos_export e on m.id = e.recibo_movimiento_id and nvl(e.valido, 0) = 1
												 left join crm_recibos_remesas_export r on r.id = e.remesa_id;


CREATE TABLE "UJI_CRM"."CRM_LINEAS_FACTURACION"
   (	"ID" NUMBER NOT NULL ENABLE,
	"SERVICIO_ID" NUMBER NOT NULL ENABLE,
	"NOMBRE" VARCHAR2(200 BYTE) NOT NULL ENABLE,
	"DESCRIPCION" VARCHAR2(1000 BYTE),
	"FORMATO_ID" NUMBER,
	 CONSTRAINT "CRM_LINEAS_FACTURACION_PK" PRIMARY KEY ("ID")
	 CONSTRAINT "CRM_LINEAS_FACTURACION_FK1" FOREIGN KEY ("SERVICIO_ID")
	  REFERENCES "UJI_CRM"."CRM_SERVICIOS" ("ID") ENABLE,
	 CONSTRAINT "CRM_LINEAS_FACTURACION_FK2" FOREIGN KEY ("FORMATO_ID")
	  REFERENCES "UJI_CRM"."CRM_RECIBOS_FORMATO" ("ID") ENABLE);


 CREATE TABLE "UJI_CRM"."CRM_RECIBOS_FORMATO"
   (	"ID" NUMBER NOT NULL ENABLE,
	"FORMATO" VARCHAR2(20 BYTE) NOT NULL ENABLE,
	 CONSTRAINT "CRM_RECIBOS_FORMATO_ID_PK" PRIMARY KEY ("ID"));


 CREATE TABLE "UJI_CRM"."CRM_CAMPANYAS_ESTADOS"
   (	"ID" NUMBER NOT NULL ENABLE,
	"NOMBRE" VARCHAR2(200 BYTE) NOT NULL ENABLE,
	"CAMPANYA_ID" NUMBER,
	"ACCION" VARCHAR2(200 BYTE),
	 CONSTRAINT "CRM_CAMPANYA_ESTADOS_PK" PRIMARY KEY ("ID"),
	 CONSTRAINT "CRM_CAMPANYAS_ESTADOS_FK1" FOREIGN KEY ("CAMPANYA_ID")
	  REFERENCES "UJI_CRM"."CRM_CAMPANYAS" ("ID") ENABLE
   ) ;


 CREATE TABLE "UJI_CRM"."CRM_CAMPANYAS_ESTADOS_MOTIVOS"
   (	"ID" NUMBER NOT NULL ENABLE,
	"CAMPANYA_ESTADO_ID" NUMBER NOT NULL ENABLE,
	"NOMBRE" VARCHAR2(200 BYTE) NOT NULL ENABLE,
	 CONSTRAINT "CRM_CAMPANYA_ESTADOS_MOTI_PK" PRIMARY KEY ("ID"),
	 CONSTRAINT "CRM_CAMPANYAS_ESTADOS_MOT_FK1" FOREIGN KEY ("CAMPANYA_ESTADO_ID")
	  REFERENCES "UJI_CRM"."CRM_CAMPANYAS_ESTADOS" ("ID") ENABLE);

 CREATE TABLE "UJI_CRM"."CRM_CAMPANYAS_CLIENTE_EST_MOT"
   (	"ID" NUMBER NOT NULL ENABLE,
	"CAMPANYA_ESTADO_MOTIVO_ID" NUMBER NOT NULL ENABLE,
	"COMENTARIOS" VARCHAR2(1000 BYTE),
	 CONSTRAINT "CRM_CAMPANYA_CLIENTE_EST__PK" PRIMARY KEY ("ID"),
	 CONSTRAINT "CRM_CAMPANYAS_CLIENTE_EST_FK1" FOREIGN KEY ("CAMPANYA_ESTADO_MOTIVO_ID")
	  REFERENCES "UJI_CRM"."CRM_CAMPANYAS_ESTADOS_MOTIVOS" ("ID") ENABLE);