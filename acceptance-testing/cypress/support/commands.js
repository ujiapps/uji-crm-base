// Comando que selecciona el primer elemento de un combo
Cypress.Commands.add("selectFirstItem", (selector) => {
    cy.get(selector).click();
    cy.get(selector).invoke('data', 'componentid')
        .then(value => {
            cy.get(`#${value}-picker-listEl`).children().first().click();
        });
});

// Comando que selecciona el elemento de un combo por valor
Cypress.Commands.add("selectItemValue", (selector, valor) => {
    cy.get(selector).click();
    cy.get(selector).invoke('data', 'componentid')
        .then(value => {
            cy.get(`#${value}-picker-listEl`).children("li:contains('"+valor+"')").first().click();
        });
});

// Comando que devuelve los campos invalidos de un row editor
Cypress.Commands.add("getInvalidFields", () => {
    return cy.get('.x-grid-row-editor').get('.x-form-invalid-field');

});
// Comando que comprueba los campos invalidos de un row editor
Cypress.Commands.add("haveInvalidFields", (num) => {
    if (num >=0) {
        return cy.get('.x-grid-row-editor').get('.x-form-invalid-field').should('have.length', num);
    } else {
        return cy.get('.x-grid-row-editor').get('.x-form-invalid-field');
    }
});

Cypress.Commands.add("getEditor", { prevSubject: true }, (subject) => {
    return cy.get(subject).get('.x-grid-row-editor');
});

Cypress.Commands.add("getButton", { prevSubject: true }, (subject,btnText) => {
    return cy.get(subject).find('.x-btn').contains(btnText);
});

// Comando que devuelve boton acceptar de un row editor
Cypress.Commands.add("editorAcceptBtn", () => {
    return cy.get('a.x-btn.x-row-editor-update-button');
});

// Comando que devuelve el boton de acceptar de un message box
Cypress.Commands.add("messageBoxAcceptBtn", () => {
    return cy.get('.x-message-box').find('.x-btn').contains('Sí');
});

// Comando que devuelve el boton único "Acceptar" de un message box
Cypress.Commands.add("uniqueMessageBoxAcceptBtn", () => {
    return cy.get('.x-message-box').find('.x-btn').contains('Acceptar');
});

Cypress.Commands.add("hasInvalidFieldsForm", (num) => {
    if (num >=0) {
        return cy.get('.x-form-invalid-field').should('have.length', num);
    } else {
        return cy.get('.x-form-invalid-field');
    }
});

//Comando que comprueba si un setLoading se está ejecutando
Cypress.Commands.add("checkLoading", () => {
    cy.get('body')
            .then(body => {
                if (body.find('.x-mask-msg').length) {
                    return '.x-mask-msg';
                }

                return 'Not exists';
            })
            .then(selector => {
                if (selector === '.x-mask-msg')
                    cy.get(selector).should('not.visible');
            });
});

Cypress.Commands.add("waitRequest", (alias, n) => {
    cy.wait(alias).its('response.statusCode').should('eq', n);
    cy.checkLoading();
});