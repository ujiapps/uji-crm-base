class EstudisUJIPage {

    fieldsEstudisUjiGrid = {
        tieneItemsAsociados: 'input[name=tieneItemsAsociados]',
        tipoEstudios: 'input[name=tipoEstudios]',
    }

    fieldsItemWindow = {
        programaId: 'input[name=programaId]',
        grupoId: 'input[name=grupoId]',
        itemId: 'input[name=itemId]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#estudiosUJI';
    }

    getMenuItem() {
        return this.browser.contains("Gestió estudis");
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getPanelEstudios() {
        return this.browser.get('[id^=panelEstudios]');
    }

    getEstudisUjiGrid() {
        return this.browser.get('[id^=estudiosUJIGrid]');
    }

    getEstudiosUJIItemsGrid() {
        return this.browser.get('[id^=estudiosUJIItemsGrid]');
    }

    getEstudiosUJIItemsWindow() {
        return this.browser.get('[id^=estudiosUJIItemsWindow]');
    }

    getBtnAfegirFromItemGrid() {
        return this.getEstudiosUJIItemsGrid().getButton('Afegir');
    }

    getBtnEsborrarFromIemGrid() {
        return this.getEstudiosUJIItemsGrid().getButton('Esborrar');
    }

    getBtnDesarItemWindow() {
        return this.getEstudiosUJIItemsWindow().getButton('Desar');
    }

    getItemTabBar() {
        return this.getPanelEstudios().get('.x-tab-bar').contains('Items');
    }

    selectSenseItemAssociatsCombo(valor) {
        let sel = cy.get(this.fieldsEstudisUjiGrid.tieneItemsAsociados);
        sel.first().click();
        sel.invoke('data', 'componentid')
            .then(value => {
                cy.get(`#${value}-picker-listEl`).children("li:contains('"+valor+"')").first().click();
            });
    }

    selectTipusEstudiCombo(valor) {
        let sel = cy.get(this.fieldsEstudisUjiGrid.tipoEstudios);
        sel.first().click();
        sel.invoke('data', 'componentid')
            .then(value => {
                cy.get(`#${value}-picker-listEl`).children("li:contains('"+valor+"')").first().click();
            });
    }

    selectProgramaCombo() {
        this.getEstudiosUJIItemsWindow().selectItemValue(this.fieldsItemWindow.programaId, 'ACTES GRADUACIÓ');
    }

    selectGrupoCombo() {
        this.getEstudiosUJIItemsWindow().selectItemValue(this.fieldsItemWindow.grupoId, 'Facultat');
    }

    selectItemCombo() {
        this.getEstudiosUJIItemsWindow().selectItemValue(this.fieldsItemWindow.itemId, 'Facultat Ciències de la Salut');
    }

    selectRowEstudisUJI() {
        return this.getEstudisUjiGrid().get('.x-grid-cell').contains('Tècniques Experimentals en Química (Interuniversitari)');
    }

    selectItemGrid() {
        return this.getEstudiosUJIItemsGrid().get('.x-grid-cell').contains('Facultat Ciències de la Salut');
    }
}

export default EstudisUJIPage