import estudiosNoUJI from '../../../fixtures/estudiosNoUJI.json'

class EstudiosNoUJIPage {

    fields = {
        nombreCa: 'input[name=nombreCa]',
        nombreEs: 'input[name=nombreEs]',
        nombreUk: 'input[name=nombreUk]',
        tipo: 'input[name=tipo]',
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#estudiosNoUJI';
    }

    getMenuItem() {
        return this.browser.contains("Gestió estudis");
    }

    getEstudiosNoUJITab() {
        return this.browser.get('.x-tab').contains("Estudis no UJI");
    }

    getEstudiosNoUJIGrid() {
        return this.browser.get('[id^=estudiosNoUJIGrid]');
    }

    getAfegirEstudioNoUJIBtn() {
        return this.getEstudiosNoUJIGrid().getButton('Afegir');
    }

    getEsborrarEstudioNoUJIBtn() {
        return this.getEstudiosNoUJIGrid().getButton('Esborrar');
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    fillEditorFields() {
        this.getEstudiosNoUJIGrid().getEditor().within(() => {
            this.browser.get(this.fields.nombreCa).type(estudiosNoUJI.nombreCa);
            this.browser.get(this.fields.nombreEs).type(estudiosNoUJI.nombreEs);
            this.browser.get(this.fields.nombreUk).type(estudiosNoUJI.nombreUk);
        });
        this.browser.selectFirstItem(this.fields.tipo);
    }

    clearTest() {
        this.browser.get('.x-grid-cell').contains(estudiosNoUJI.nombreCa).first().click();
        this.getEsborrarEstudioNoUJIBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

}

export default EstudiosNoUJIPage