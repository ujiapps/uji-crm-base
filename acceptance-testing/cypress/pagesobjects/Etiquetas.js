class EtiquetasPage {

    fields = {
        nombre: 'input[name=nombre]'
    }

    constructor(browser){
        this.browser = browser;
        this.hashtag = '#etiquetas';
    }

    getMenuItem(){
        let upper = this.browser.contains("Altres gestions");
        upper.click();
        return this.browser.contains("Etiquetes");
    }

    getEtiquetasGrid(){
        return this.browser.get('[id^=etiquetasGrid]');
    }

    getAfegirEtiquetaBtn(){
        return this.getEtiquetasGrid().getButton('Afegir');
    }

    getEsborrarEtiquetaBtn(){
        return this.getEtiquetasGrid().getButton('Esborrar');
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    fillEditorFields(texto) {
        this.browser.get('[id^=etiquetasGrid]').getEditor().within(() => {
           this.browser.get(this.fields.nombre).type(texto);
        });
    }

    clearTest(texto){
        this.browser.get('.x-grid-cell').contains(texto).first().click();
        this.getEsborrarEtiquetaBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

}

export default EtiquetasPage