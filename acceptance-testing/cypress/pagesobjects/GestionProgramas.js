import programa from '../fixtures/programa.json'

class GestionProgramasPage {

    fields = {
        nombreCa: 'input[name=nombreCa]',
        nombreEs: 'input[name=nombreEs]',
        nombreUk: 'input[name=nombreUk]',
        plantilla: 'input[name=plantilla]',
        orden: 'input[name=orden]',
        visible: 'input[name=visible]',
        servicio: '[name=servicioNombre]',
        comboAccesos: '[name=comboAccesos]'

    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#programas';
    }


    getMenuItem() {
        return this.browser.contains("Gestió de Programes");
    }
    getProgramasGrid() {
        return this.browser.get('[id^=programasGrid]')
    }

    getAfegirProgramaBtn() {
        return this.getProgramasGrid().getButton('Afegir');
    }

    getEsBorrarProgramaBtn() {
        return this.getProgramasGrid().getButton('Esborrar');
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }
    

    fillEditorFields() {
        this.browser.get('[id^=programasGrid]').getEditor().within(() => {
            this.browser.get(this.fields.nombreCa).type(programa.nombreCa);
            this.browser.get(this.fields.nombreEs).type(programa.nombreEs);
            this.browser.get(this.fields.nombreUk).type(programa.nombreUk);
            this.browser.get(this.fields.plantilla).type(programa.plantilla);
            this.browser.get(this.fields.orden).clear().type(programa.orden);
            this.browser.get(this.fields.visible).check();
        });
        this.browser.selectFirstItem(this.fields.servicio);
        this.browser.selectFirstItem(this.fields.comboAccesos);
    }

    clearTest(){
        this.browser.get('.x-grid-cell').contains(programa.nombreCa).first().click();
        this.getEsBorrarProgramaBtn().click();
        this.browser.messageBoxAcceptBtn().click();
        
    }
}

export default GestionProgramasPage