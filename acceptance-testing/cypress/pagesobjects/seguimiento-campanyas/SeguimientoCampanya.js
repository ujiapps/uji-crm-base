class SeguimientoCampanyaPage {

    fields = {
        // tab clients
        busqueda: 'input[name=busqueda]',
        comboEtiqueta: 'input[name=comboEtiqueta]',
        comboTipo: 'input[name=campanyaClienteTipoId]',
        comboItems: 'input[name=comboItems]',
        comboSeguimientos: 'input[name=busquedaSeguimientoTipo]',
        // tab seguiments
        campanyaFases: 'input[name=campanyaFases]',
        fecha: 'input[name=fecha]',
        nombre: 'input[name=nombre]',
        ujicombocolumn: 'input[name=ujicombocolumn]',
        // tab actes
        comboActos: 'input[name=comboActos]',
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#seguimientos';
    }

    visitarSeccion() {
        this.browser.visit("/");
        this.browser.contains("Seguiment de Campanyes").click();
    }

    getPanelSeguimientos() {
        return this.browser.get('[id^=panelSeguimientos]');
    }

    selectTab(nombrePanel) {
        return this.browser.get('[id^=seguimientosTab]').get('[id^=tabbar]').contains(nombrePanel);
    }

    selectSubtab(nombrePanel) {
        return this.browser.get('[id^=actosCampanyaTab]').get('[id^=tabbar]').contains(nombrePanel);
    }

    getClientesCampanyaPanel() {
        return this.browser.get('[id^=clientesCampanyaPanel]');
    }

    getClientesCampanyaGrid() {
        return this.browser.get('[id^=clientesCampanyaGrid]');
    }

    getFilaClientesCampanyaGrid(dniCliente) {
        this.selectTab('Clients Campanya').click();
        return this.getClientesCampanyaGrid().get('.x-grid-cell').contains(dniCliente).first();
    }

    getFilaSeguimientosGrid() {
        return this.getSeguimientosGrid().get('.x-grid-cell').contains('09/09/2021').first();
    }

    getFiltroAvanzadoClientesCampanya() {
        return this.browser.get('[id^=filtroAvanzadoClientesCampanyaPanel]');
    }

    getNuevoSeguimientoWindow() {
        return this.browser.get('[id^=nuevoSeguimientoWindow]');
    }

    getSeguimientosClienteGrid() {
        return this.getNuevoSeguimientoWindow().get('[id^=seguimientosClienteGrid]');
    }

    getSeguimientosGrid() {
        return this.browser.get('[id^=seguimientosGrid]');
    }

    selectFiltroFases() {
        return this.browser.get('[id^=filtroFasesCampanyaPanel]').selectFirstItem(this.fields.campanyaFases);
    }

    selectFiltroTipoActo() {
        return this.browser.get('[id^=actosCampanyaPanel]').selectFirstItem(this.fields.comboActos);
    }

    seleccionarCampanya(nombreCampanya) {
        this.getPanelSeguimientos().get('[id^=treepanel]').get('.x-grid-cell').contains(nombreCampanya).first().click();
    }

    usarFiltroBusqueda(textoBusqueda) {
        this.getClientesCampanyaPanel().get(this.fields.busqueda).type(textoBusqueda);
        this.comprobarFiltro();
    }

    usarFiltroEtiqueta(textoEtiqueta) {
        this.getClientesCampanyaPanel().get(this.fields.comboEtiqueta).type(textoEtiqueta);
        this.comprobarFiltro();
    }

    usarFiltroTipo(textoTipo) {
        this.getClientesCampanyaPanel().selectItemValue(this.fields.comboTipo, textoTipo);
        this.comprobarFiltro();
    }

    usarFiltroItems(textoItem) {
        this.getFiltroAvanzadoClientesCampanya().get('.x-tool-tool-el').click();
        this.getFiltroAvanzadoClientesCampanya().get(this.fields.comboItems).get('.x-form-arrow-trigger').should('be.visible').then((combos) => {
            let comboItems = combos[2];
            comboItems.click();
            this.getFiltroAvanzadoClientesCampanya().get(this.fields.comboItems).get('.x-boundlist-item').contains(textoItem).click();
        });

        this.comprobarFiltro();
    }

    usarFiltroSeguimientos(textoItem) {
        this.getFiltroAvanzadoClientesCampanya().get('.x-tool-tool-el').click();
        this.getFiltroAvanzadoClientesCampanya().selectItemValue(this.fields.comboSeguimientos, textoItem);
        this.comprobarFiltro();
    }

    comprobarFiltro() {
        this.getClientesCampanyaPanel().getButton("Cercar").click();
        this.getClientesCampanyaGrid().should('be.visible');
        this.getClientesCampanyaGrid().find('tr').should('have.length', 1);
        this.getClientesCampanyaPanel().getButton("Netejar").click()
    }

    comprobarNumeroSeguimientosCliente(dniCliente) {

        // capturar la columna número de seguimientos de la fila cuyo dni es dniCliente en clientesCampanyaGrid
        var numSeguimientos;

        this.getFilaClientesCampanyaGrid(dniCliente).click();
        this.getClientesCampanyaGrid().find("tr").then((tr) => {
            for (const property in tr[0]) {
                if (property == 'innerText') {
                    let fila = tr[0][property];
                    let tmp = fila.split('\n\t\n');
                    numSeguimientos = tmp[5].replace('\t', '');
                    // y comprobar que coincide con el número de filas de seguimientosClienteGrid
                    this.getClientesCampanyaGrid().getButton('Seguiments').click();
                    this.getNuevoSeguimientoWindow().should('be.visible');
                    this.getSeguimientosClienteGrid().find('tr').should('have.length', numSeguimientos);
                }
            }
        });
    }

    irTabSeguimientos() {
        this.getPanelSeguimientos().get('[id^=treepanel]').get('.x-grid-cell').contains('AlumniBasic').first().dblclick();
        cy.waitRequest('@loadCampanyas', 200);
        this.getPanelSeguimientos().get('[id^=treepanel]').get('.x-tree-node-text').contains('Alumni Bàsic').first().click();
        this.selectTab('Seguiments Campanya').click();
        cy.waitRequest('@loadClientes', 200);
        this.selectFiltroFases();
        cy.waitRequest('@campanyaseguimientoget', 200);
    }

    irTabActos() {
        this.getPanelSeguimientos().get('[id^=treepanel]').get('.x-grid-cell').contains('Actes Graduació').first().dblclick();
        this.getPanelSeguimientos().get('[id^=treepanel]').contains('Curs 14/15').first().dblclick();
        this.getPanelSeguimientos().get('[id^=treepanel]').contains('Escola Superior de Tecnologia i Ciències Experimentals 15').first().click();
        this.selectTab('Actes Campanya').click();
    }

    rellenarFilaSeguimientosGrid() {
        this.irTabSeguimientos();
        this.getSeguimientosGrid().getButton('Afegir').click();
        this.getSeguimientosGrid().getEditor().get(this.fields.fecha).type('09/09/2021');
        this.getSeguimientosGrid().getEditor().get(this.fields.nombre).type('nom prova cypress');
        this.getSeguimientosGrid().selectFirstItem(this.fields.ujicombocolumn);
        this.getSeguimientosGrid().editorAcceptBtn().click();
    }

    borrarFilaSeguimientosGrid() {
        this.irTabSeguimientos();
        this.getSeguimientosGrid().get('.x-grid-cell').contains('10/09/2021').first().click();
        this.getSeguimientosGrid().getButton('Esborrar').click();
        this.getSeguimientosGrid().messageBoxAcceptBtn().click();
    }

    testActosDeCampanya() {
        this.irTabActos();
        this.selectFiltroTipoActo();
        this.selectSubtab('Info CRM');
    }
}

export default SeguimientoCampanyaPage