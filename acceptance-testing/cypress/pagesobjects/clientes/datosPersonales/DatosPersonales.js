class DatosPersonalesPage {

    fields = {
        apellidos: 'input[name=apellidos]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#datosPersonales';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem() {
        return this.browser.contains("Clients");
    }

    getPanelAltresDades() {
        return this.browser.get('[id^=clienteDatosPersonalesForm]')
    }

    getActualitzarBtn() {
        return this.getPanelAltresDades().getButton('Actualitzar');
    }

    actualizarDatosPersonales(s) {
        this.browser.get(this.fields.apellidos).focus().clear().type(s);
        this.getActualitzarBtn().click();
    }
}

export default DatosPersonalesPage