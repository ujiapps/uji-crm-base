import tiposTarifa from "../../../fixtures/tiposTarifa.json";

class SuscripcionesClientePage {

    campos = {
        movil: 'input[name=movil]',
        programa: 'input[name=programaId]',
        grupo: 'input[name=grupoId]',
        item: 'input[name=itemId]',
        postal: 'input[name=postal]',
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#clientes';
    }

    buscarCliente() {
        this.browser.visit("/");
        this.browser.contains("Clients").click();
        this.getCercarBtnPanelClientes().click();
        this.getBuscadorClientesWindow().get(this.campos.movil).last().type('444444444');
        this.getCercarBtnBuscadorClientesWindow().click();
    }

    getPanelClientes() {
        return this.browser.get('[id^=panelClientes]');
    }

    getAfegirBtn() {
        return this.getClienteSuscripcionesGrid().getButton('Afegir');
    }

    getDesarBtn() {
        return this.getSuscripcionClienteWindow().getButton('Desar');
    }

    getBuscadorClientesWindow() {
        return this.browser.get('[id^=buscadorClientesWindow]');
    }

    getSuscripcionClienteWindow() {
        return this.browser.get('[id^=nuevaSuscripcionWindow]');
    }

    rellenarSuscripcionCliente() {
        this.getSuscripcionClienteWindow().selectItemValue(this.campos.programa, 'ACTES GRADUACIÓ');
        this.getSuscripcionClienteWindow().selectItemValue(this.campos.grupo, 'Facultat');
        this.getSuscripcionClienteWindow().selectItemValue(this.campos.item, 'asdasdasd');
    }

    editarSuscripcionCliente() {
        this.getClienteSuscripcionesGrid().getEditor().within(() => {
            this.browser.get(this.campos.postal).uncheck();
        })
    }

    borrarSuscripcionCliente() {
        this.getClienteSuscripcionesGrid().contains('td', 'Facultat').click();

        this.getBorrarSuscripcionClienteBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

    getBorrarSuscripcionClienteBtn() {
        return this.getClienteSuscripcionesGrid().getButton('Esborrar');
    }

    getClientesGrid() {
        return this.getBuscadorClientesWindow().get('[id^=clientesGrid]');
    }

    getClienteSuscripcionesGrid() {
        return this.getPanelClientes().get('[id^=clienteSuscripcionesGrid]');
    }

    getCercarBtnPanelClientes() {
        return this.getPanelClientes().find('.x-btn').contains('Cercar');
    }

    getCercarBtnBuscadorClientesWindow() {
        return this.getBuscadorClientesWindow().find('.x-btn').contains('Cercar');
    }

    seleccionarFilaSuscripcionesCliente() {
        this.getClienteSuscripcionesGrid().contains('td', 'Facultat').dblclick();
    }

    seleccionarFilaClientesGrid() {
        this.getClientesGrid().find('.x-grid-cell').contains('Lozano, Iratxe').dblclick();
    }

    irPanelSuscripciones() {
        this.getPanelClientes().find('.x-tab-inner').contains('Suscripcions').click();
    }
}

export default SuscripcionesClientePage