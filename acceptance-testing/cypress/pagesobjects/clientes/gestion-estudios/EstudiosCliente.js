class EstudiosClientePage {

    fields = {
        busqueda: 'input[name=busqueda]'
    }

    fieldsEstudis = {
        tipoEstudio: 'input[name=tipoEstudio]'
    }

    /*** Client Search ***/

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#clientes';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem() {
        return this.browser.contains("Clients");
    }

    getPanelClientes() {
        return this.browser.get('[id^=panelClientes]');
    }

    getBuscadorClientesWindow() {
        return this.browser.get('[id^=buscadorClientesWindow]');
    }

    getClientesGrid() {
        return this.getBuscadorClientesWindow().get('[id^=clientesGrid]');
    }

    getCercarBtnPanelClientes() {
        return this.getPanelClientes().find('.x-btn').contains('Cercar');
    }

    buscarNombreCypress() {
        this.getBuscadorClientesWindow().get(this.fields.busqueda).type('cypress');
    }

    getCercarBtnBuscadorClientesWindow() {
        return this.getBuscadorClientesWindow().find('.x-btn').contains('Cercar');
    }

    seleccionarFilaClientesGrid() {
        this.getClientesGrid().find('.x-grid-cell').contains('cypress apellidos').dblclick();
    }

    /*** Unique Methods ***/

    getEstudisPanel() {
        this.getPanelClientes().find('.x-tab-bar').contains('Estudis').click();
    }

    getClienteEstudiosNoUJIGrid() {
        return this.getPanelClientes().get('[id^=clienteEstudiosNoUJIGrid]');
    }

    getAfegirBtn() {
        return this.getClienteEstudiosNoUJIGrid().getButton('Afegir');
    }

    getEsborrarBtn() {
        return this.getClienteEstudiosNoUJIGrid().getButton('Esborrar');
    }

    selectRowEstudisUjiGrid() {
        this.getClienteEstudiosNoUJIGrid().get('.x-grid-cell').contains('Grau').click();
    }

    fillEditorFields() {
        this.browser.selectItemValue(this.fieldsEstudis.tipoEstudio, 'Grau');
    }
}

export default EstudiosClientePage