class BuscarPage {

    fields = {
        busqueda: 'input[name=busqueda]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#clientes';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem() {
        return this.browser.contains("Clients");
    }

    getPanelClientes() {
        return this.browser.get('[id^=panelClientes]');
    }

    getClientesGrid() {
        return this.getBuscadorClientesWindow().get('[id^=clientesGrid]');
    }

    getBuscadorClientesWindow() {
        return this.browser.get('[id^=buscadorClientesWindow]');
    }

    getCercarBtnPanelClientes() {
        return this.getPanelClientes().find('.x-btn').contains('Cercar');
    }

    getCercarBtnBuscadorClientesWindow() {
        return this.getBuscadorClientesWindow().find('.x-btn').contains('Cercar');
    }

    buscarNombreCypress() {
        this.getBuscadorClientesWindow().get(this.fields.busqueda).type('cypress');
    }

    seleccionarFilaClientesGrid() {
        this.getClientesGrid().find('.x-grid-cell').contains('cypress apellidos, Estefania').dblclick();
    }
}

export default BuscarPage