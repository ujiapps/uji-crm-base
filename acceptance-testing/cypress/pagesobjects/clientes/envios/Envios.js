class EnviosPage {

    fields = {
        busqueda: 'input[name=busqueda]'
    }

    fieldsForm = {
        correo: 'input[name=comboMail]',
        responder: 'input[name=responder]',
        asunto: 'input[name=asunto]'
    }

    /*** Client Search ***/

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#clientes';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem() {
        return this.browser.contains("Clients");
    }

    getPanelClientes() {
        return this.browser.get('[id^=panelClientes]');
    }

    getBuscadorClientesWindow() {
        return this.browser.get('[id^=buscadorClientesWindow]');
    }

    getClientesGrid() {
        return this.getBuscadorClientesWindow().get('[id^=clientesGrid]');
    }

    getCercarBtnPanelClientes() {
        return this.getPanelClientes().find('.x-btn').contains('Cercar');
    }

    buscarNombreCypress() {
        this.getBuscadorClientesWindow().get(this.fields.busqueda).eq(0).type('cypress');
    }

    getCercarBtnBuscadorClientesWindow() {
        return this.getBuscadorClientesWindow().find('.x-btn').contains('Cercar');
    }

    seleccionarFilaClientesGrid() {
        this.getClientesGrid().find('.x-grid-cell').contains('cypress apellidos, Estefania').dblclick();
    }

    /*** Unique Methods ***/

    getEnviosTab() {
        this.getPanelClientes().find('.x-tab-bar').contains('Enviament').click();
    }

    getClienteEnvioGrid() {
        return this.browser.get('[id^=enviosClienteGrid]');
    }

    getClienteEnvioForm() {
        return this.browser.get('[id^=envioClienteForm]');
    }

    getAfegirBtn() {
        this.getClienteEnvioGrid().find('.x-btn').contains('Crear').click();
    }

    typeDatos() {
        this.getClienteEnvioForm().selectFirstItem(this.fieldsForm.correo);
        this.getClienteEnvioForm().get(this.fieldsForm.responder).clear().type('Cypress@Test.com');
        this.getClienteEnvioForm().get(this.fieldsForm.asunto).clear().type('Cypress Test Prueba');
    }

    getDesarBtn() {
        this.getClienteEnvioForm().getButton('Desar').click();
    }

    getEnviarBtn() {
        this.getClienteEnvioForm().getButton('En').click();
    }

    getDuplicarBtn() {
        this.getClienteEnvioGrid().getButton('Duplicar').click();
    }

    getEsborrarBtn() {
        this.getClienteEnvioGrid().getButton('Es').click();
    }

    selectRowEnvioGrid() {
        this.getClienteEnvioGrid().get('.x-grid-cell').contains('Cypress Test Prueba').click();
    }

    selectRowEnvioGridDupli() {
        this.getClienteEnvioGrid().get('.x-grid-cell').contains('Cypress Test Prueba Duplicada').click();
    }

    typeDupli() {
        this.getClienteEnvioForm().get(this.fieldsForm.asunto).clear().type('Cypress Test Prueba Duplicada');
    }

    writeOnBody() {
        let cy = this.getClienteEnvioForm();
        cy.get('iframe').then(elem => {
            const body = elem.contents().find('body');

            let stripe = cy.wrap(body);
            stripe.eq(0).type("Texto CypressTest Generado");
        });
    }

    /*** envioClienteConfirmWindow ***/

    getEnvioClienteConfirmWindow() {
        return this.browser.get('[id^=envioClienteConfirmWindow]');
    }

    getEnviarFromWindow() {
        this.getEnvioClienteConfirmWindow().getButton('En').click();
    }
}

export default EnviosPage