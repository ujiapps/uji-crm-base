class SeguimientosClientePage {

    campos = {
        movil: 'input[name=movil]',
        tipoSeguimiento: 'input[name=seguimientoTipoId]',
        campanya: 'input[name=campanyaId]',
        nombre: 'input[name=nombre]',
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#clientes';
    }

    buscarCliente() {
        this.browser.visit("/");
        this.browser.contains("Clients").click();
        this.getCercarBtnPanelClientes().click();
        this.getBuscadorClientesWindow().get(this.campos.movil).last().type('444444444');
        this.getCercarBtnBuscadorClientesWindow().click();
    }

    getPanelClientes() {
        return this.browser.get('[id^=panelClientes]');
    }

    getAfegirBtn() {
        return this.getClienteSeguimientosGrid().getButton('Afegir');
    }

    getDesarBtn() {
        return this.getSeguimientoClienteWindow().getButton('Desar');
    }

    getBuscadorClientesWindow() {
        return this.browser.get('[id^=buscadorClientesWindow]');
    }

    getSeguimientoClienteWindow() {
        return this.browser.get('[id^=clienteSeguimientosWindow]');
    }

    rellenarSeguimientoCliente() {
        this.getSeguimientoClienteWindow().selectItemValue(this.campos.tipoSeguimiento, 'Presencial');
        this.getSeguimientoClienteWindow().selectItemValue(this.campos.campanya, 'aaaa');
        this.getSeguimientoClienteWindow().find(this.campos.nombre).type('nombre seguimiento cypress');
    }

    editarSeguimientoCliente() {
        this.getSeguimientoClienteWindow().find(this.campos.nombre).clear().type('nombre modificado seguimiento cypress');
    }

    borrarSeguimientoCliente() {
        this.browser.get('.x-grid-cell').contains('Presencial').click();
        this.getBorrarSeguimientoClienteBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

    getBorrarSeguimientoClienteBtn() {
        return this.getClienteSeguimientosGrid().getButton('Esborrar');
    }

    getClientesGrid() {
        return this.getBuscadorClientesWindow().get('[id^=clientesGrid]');
    }

    getClienteSeguimientosGrid() {
        return this.getPanelClientes().get('[id^=clienteSeguimiento]');
    }

    getCercarBtnPanelClientes() {
        return this.getPanelClientes().find('.x-btn').contains('Cercar');
    }

    getCercarBtnBuscadorClientesWindow() {
        return this.getBuscadorClientesWindow().find('.x-btn').contains('Cercar');
    }

    seleccionarFilaSeguimientosCliente() {
        return this.browser.get('.x-grid-cell').contains('Presencial');
    }

    seleccionarFilaClientesGrid() {
        this.getClientesGrid().find('.x-grid-cell').contains('Lozano, Iratxe').dblclick();
    }

    irPanelSeguimientos() {
        this.getPanelClientes().find('.x-tab-inner').contains('Seguiments').click();
    }
}

export default SeguimientosClientePage