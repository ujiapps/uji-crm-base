class CampanyaClientePage {

    fields = {
        comboCampanyas: 'input[name=comboCampanyas]',
        comboEstados: 'input[name=comboEstados]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#datosPersonales';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem() {
        return this.browser.contains("Clients");
    }

    getPanelClientes() {
        return this.browser.get('[id^=panelClientes]');
    }

    selectCampanyasTab() {
        this.getPanelClientes().find('.x-tab-bar').contains('Campanyes').click();
    }

    getCampanyasGrid() {
        return this.browser.get('[id^=campanyasClienteGrid]');
    }

    getAfegirBtn() {
        return this.getCampanyasGrid().getButton('Afegir');
    }

    getEsborrarBtn() {
        return this.getCampanyasGrid().getButton('Esborrar');
    }

    selectCampanyaRow() {
        this.getCampanyasGrid().find('.x-grid-cell').contains('Admès').click();
    }

    //*********** Anyadir Campanya Window
    getAnyadirCampanyaWindow() {
        return this.browser.get('[id^=nuevaCampanyaClienteWindow]');
    }

    getFinalitzarBtn() {
        return this.getAnyadirCampanyaWindow().getButton('Finalitzar');
    }

    selectCombos() {
        this.browser.wait(500);
        this.getAnyadirCampanyaWindow().find('.x-form-arrow-trigger').first().click();
        this.getAnyadirCampanyaWindow().get('.x-boundlist-item').contains("Anàlisi de dades amb gràfics dinàmics en Excel 2016 18-19").click();
        this.browser.selectFirstItem(this.fields.comboEstados);
        this.getFinalitzarBtn().should('be.visible').click();
    }

    borrarCampanya() {
        this.selectCampanyaRow();
        this.getEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

}

export default CampanyaClientePage