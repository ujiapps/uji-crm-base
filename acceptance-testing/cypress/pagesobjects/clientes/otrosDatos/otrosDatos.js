class OtrosDatosPage {

    datoExtraFields = {
        tipoDatoId: 'input[name=tipoDatoId]',
        campoTexto: 'input[name=campoTexto]',
        tipoAccesoId: 'input[name=tipoAccesoId]',
    }

    itemFields = {
        programaId: 'input[name=programaId]',
        grupoId:'input[name=grupoId]',
        itemId:'input[name=itemId]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#otrosDatosClientes';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem() {
        return this.browser.contains("Clients");
    }

    getPanelClientes() {
        return this.browser.get('[id^=panelClientes]');
    }

    selectOtrosDatosTab() {
        this.getPanelClientes().find('.x-tab-bar').contains('Altres Dades').click();
    }

    getOtrosDatosGrid() {
        return this.browser.get('[id^=clienteOtrosDatosGrid]');
    }

    getAfegirBtn() {
        return this.getOtrosDatosGrid().getButton('Afegir');
    }

    getEsborrarBtn() {
        return this.getOtrosDatosGrid().getButton('Esborrar');
    }

    selectDatoExtraRow() {
        this.getOtrosDatosGrid().find('.x-grid-cell').contains('Codi Postal').click();
    }

    eliminarDatoExtra() {
        this.selectDatoExtraRow();
        this.getEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

    selectItemRow() {
        this.getOtrosDatosGrid().find('.x-grid-cell').contains('Facultat').click();
    }

    eliminarItem() {
        this.selectItemRow();
        this.getEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

    //*********** Tipus de dada Window
    getTipusDadaWindow() {
        return this.browser.get('[id^=nuevoOtroDatoWindow]');
    }

    elegirDatoExtra() {
        this.getTipusDadaWindow().find('.x-form-arrow-trigger').first().click();
        this.getTipusDadaWindow().get('.x-boundlist-item').contains("Dato Extra").click();
    }

    elegirItem() {
        this.getTipusDadaWindow().find('.x-form-arrow-trigger').first().click();
        this.getTipusDadaWindow().get('.x-boundlist-item').contains("Item").click();
    }

    fillDatoExtraForm() {
        //this.browser.selectFirstItem(this.datoExtraFields.tipoDatoId);
        this.browser.selectItemValue(this.datoExtraFields.tipoDatoId, "Codi Postal");
        this.browser.selectFirstItem(this.datoExtraFields.tipoAccesoId);
        this.browser.get(this.datoExtraFields.campoTexto).type(12001);
    }

    fillItemForm() {
        this.browser.selectItemValue(this.itemFields.programaId, "ACTES GRADUACIÓ");
        cy.waitRequest('@grupo', 200)
        this.browser.selectFirstItem(this.itemFields.grupoId);
        cy.waitRequest('@item', 200)
        this.browser.selectFirstItem(this.itemFields.itemId);
    }

    getAfegirIContinuarBtn() {
        return this.getTipusDadaWindow().getButton('Afegir i Continuar');
    }

    getAfegirIFinalitzarBtn() {
        return this.getTipusDadaWindow().getButton('Afegir i Finalitzar');
    }

    //Datos Relacionados
    selectItemParaModificar() {
        this.getOtrosDatosGrid().find('.x-grid-cell').contains('Facultat').dblclick();
    }

    getModificarItemWindow() {
        return this.browser.get('[id^=modificarOtroItemWindow]');
    }

    getDatosRelacionadosGrid() {
        return this.getModificarItemWindow().get('[id^=clienteDatosRelacionadosGrid]');
    }

    getAfegirDatoRelacionadoBtn() {
        return this.getDatosRelacionadosGrid().getButton('Afegir');
    }

    getEsborrarDatoRelacionadoBtn() {
        return this.getDatosRelacionadosGrid().getButton('Esborrar');
    }

    getModificarIFinalitzarBtn() {
        return this.getModificarItemWindow().getButton('Modificar i Finalitzar');
    }

    anyadirDatoRelacionado() {
        this.getAfegirDatoRelacionadoBtn().click();
        this.elegirDatoExtra();
        cy.waitRequest('@acceso', 200)
        cy.waitRequest('@datotipo', 200)
        this.fillDatoExtraForm();
        this.getAfegirIFinalitzarBtn().click();
    }

    selectDatoRelacionadoRow(){
        this.getDatosRelacionadosGrid().find('.x-grid-cell').contains('Codi Postal').click();
    }

    eliminarDatoRelacionado() {
        this.selectDatoRelacionadoRow();
        this.getEsborrarDatoRelacionadoBtn().click();
    }


}

export default OtrosDatosPage