class VinculosPage {

    fields = {
        busqueda: 'input[name=busqueda]'
    }

    fieldsVinculos = {
        comboClientesVinculaciones: 'input[name=comboClientesVinculaciones]',
        comboTipoVinculo: 'input[name=comboTipoVinculo]'
    }

    fieldsUjiLookUpCombo = {
        query: 'input[name=query]'
    }

    /*** Client Search ***/

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#clientes';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem() {
        return this.browser.contains("Clients");
    }

    getPanelClientes() {
        return this.browser.get('[id^=panelClientes]');
    }

    getBuscadorClientesWindow() {
        return this.browser.get('[id^=buscadorClientesWindow]');
    }

    getClientesGrid() {
        return this.getBuscadorClientesWindow().get('[id^=clientesGrid]');
    }

    getCercarBtnPanelClientes() {
        return this.getPanelClientes().find('.x-btn').contains('Cercar');
    }

    buscarNombreCypress() {
        this.getBuscadorClientesWindow().get(this.fields.busqueda).type('cypress');
    }

    getCercarBtnBuscadorClientesWindow() {
        return this.getBuscadorClientesWindow().find('.x-btn').contains('Cercar');
    }

    seleccionarFilaClientesGrid() {
        this.getClientesGrid().find('.x-grid-cell').contains('cypress apellidos').dblclick();
    }

    /*** Unique Methods ***/

    getClienteVinculosGrid() {
        return this.browser.get('[id^=clienteVinculosGrid]');
    }

    getClienteNuevoVinculoWindow() {
        return this.browser.get('[id^=clienteNuevoVinculoWindow]');
    }

    getVinculosTab() {
        this.getPanelClientes().find('.x-tab-bar').contains('Vincles').click()
    }

    getAfegirBtn() {
        return this.getClienteVinculosGrid().getButton('Afegir');
    }

    getEsborrarBtn() {
        return this.getClienteVinculosGrid().getButton('Esborrar');
    }

    getClientComboBox() {
        return this.getClienteNuevoVinculoWindow().get(this.fieldsVinculos.comboClientesVinculaciones);
    }

    getTipusVinculacioComboBox() {
        return this.getClienteNuevoVinculoWindow().get(this.fieldsVinculos.comboTipoVinculo);
    }

    getDesarBtnFromAfegirVinculacioWin() {
        return this.getClienteNuevoVinculoWindow().getButton('Desar');
    }

    selectVinculacion() {
        this.getTipusVinculacioComboBox().selectItemValue(this.fieldsVinculos.comboTipoVinculo, 'Secretario');
    }

    selectRowFromVinculosGrid(elem) {
        return this.getClienteVinculosGrid().find('.x-grid-cell').contains(elem);
    }

    /** Cercar Registres **/
    getUjiLookUpCombo() {
        return this.browser.get('[id^=ujilookupWindow]');
    }

    getQueryFromUjiLookUpCombo() {
        return this.getUjiLookUpCombo().get(this.fieldsUjiLookUpCombo.query);
    }

    getBtnCercarUjiLoopUpCombo() {
        return this.getUjiLookUpCombo().getButton('Cerca');
    }

    selectRowUjiLookUpComboVinculos(elem) {
        return this.getUjiLookUpCombo().find('.x-grid-cell').contains(elem);
    }

    getBtnSeleccionarUjiLoopUpCombo() {
        return this.getUjiLookUpCombo().getButton('Seleccionar');
    }
}

export default VinculosPage