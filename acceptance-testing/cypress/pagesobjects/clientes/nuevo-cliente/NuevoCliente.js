class NuevoClientePage {

    campos = {
        servicio: 'input[name=servicio]',
        tipo: 'input[name=tipo]',
        nombre: 'input[name=nombre]',
        apellidos: 'input[name=apellidos]',
        correo: 'input[name=correo]',
        tipoIdentificacion: 'input[name=tipoIdentificacion]',
        identificacion: 'input[name=identificacion]',
        nacionalidadId: 'input[name=nacionalidadId]',
        paisPostalId: 'input[name=paisPostalId]',
        fechaNacimiento: 'input[name=fechaNacimiento]',
        sexoId: 'input[name=sexoId]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#clientes';
    }

    visitarSeccion() {
        this.browser.visit("/");
        this.browser.contains("Clients").click();
    }

    getPanelClientes() {
        return this.browser.get('[id^=panelClientes]');
    }

    getAfegirBtn() {
        return this.getPanelClientes().find('.x-btn').contains('Afegir');
    }

    getNuevoClienteWindow() {
        return this.browser.get('[id^=nuevoClienteWindow]');
    }

    rellenarNuevoClienteForm() {
        this.getNuevoClienteWindow().selectItemValue(this.campos.servicio, 'Servei de Comunicacions i Publicacions');

        this.getNuevoClienteWindow().get(this.campos.tipo).last().click();
        this.getNuevoClienteWindow().get(this.campos.tipo).get('.x-boundlist-item').contains('Persona física').click();

        this.getNuevoClienteWindow().get(this.campos.correo).last().type('e2e@cypress.io');

        this.getNuevoClienteWindow().get(this.campos.tipoIdentificacion).last().click();
        this.getNuevoClienteWindow().get(this.campos.tipoIdentificacion).get('.x-boundlist-item').contains('DNI').click();

        this.getNuevoClienteWindow().get(this.campos.identificacion).last().click();
        this.getNuevoClienteWindow().get(this.campos.identificacion).last().type('44276049Z');

        this.getNuevoClienteWindow().get(this.campos.nombre).last().type('cypress nombre');
        this.getNuevoClienteWindow().get(this.campos.apellidos).last().type('cypress apellidos');

        this.getNuevoClienteWindow().get(this.campos.nacionalidadId).should('be.visible').then((combos) => {
            this.getNuevoClienteWindow().get(combos[1]).type('Afganistan').click();
        });

        this.getNuevoClienteWindow().get(this.campos.paisPostalId).should('be.visible').then((combos) => {
            this.getNuevoClienteWindow().get(combos[1]).type('Afganistan').click();
        });

        this.getNuevoClienteWindow().selectFirstItem(this.campos.sexoId);

        this.getNuevoClienteWindow().get(this.campos.fechaNacimiento).should('be.visible').then((items) => {
            this.browser.get(items[1]).type('01/01/1970', {force: true});
        });

        this.getNuevoClienteWindow().getButton('Desar').click();
    }
}

export default NuevoClientePage