class FotosClientePage {

    campos = {
        correo: 'input[name=correo]',
        validar: 'input[name=validar]',
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#clientes';
    }

    buscarCliente() {
        this.browser.visit("/");
        this.browser.contains("Clients").click();
        this.getCercarBtnPanelClientes().click();
        this.getBuscadorClientesWindow().get(this.campos.correo).last().type('e2e@cypress.io');
        this.getCercarBtnBuscadorClientesWindow().click();
    }

    getPanelClientes() {
        return this.browser.get('[id^=panelClientes]');
    }

    getBuscadorClientesWindow() {
        return this.browser.get('[id^=buscadorClientesWindow]');
    }

    getClientesGrid() {
        return this.getBuscadorClientesWindow().get('[id^=clientesGrid]');
    }

    getCercarBtnPanelClientes() {
        return this.getPanelClientes().find('.x-btn').contains('Cercar');
    }

    getCercarBtnBuscadorClientesWindow() {
        return this.getBuscadorClientesWindow().find('.x-btn').contains('Cercar');
    }

    seleccionarFilaClientesGrid() {
        this.getClientesGrid().find('.x-grid-cell').contains('cypress apellidos').dblclick();
    }

    irPanelFotos() {
        this.getPanelClientes().find('.x-tab-inner').contains('Fotos').click();
    }

    validarFotoCRM() {
        this.getPanelClientes().find('.x-btn').contains('Validar').click();
        this.browser.messageBoxAcceptBtn().click();
        this.browser.uniqueMessageBoxAcceptBtn().click();
    }
}

export default FotosClientePage