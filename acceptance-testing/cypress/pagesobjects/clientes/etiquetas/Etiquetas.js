class EtiquetasPage {

    fields = {
        busqueda: 'input[name=busqueda]'
    }

    fieldsEtiquetasTab = {
        etiqueta: 'input[name=etiqueta]'
    }

    /*** Client Search ***/

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#clientes';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem() {
        return this.browser.contains("Clients");
    }

    getPanelClientes() {
        return this.browser.get('[id^=panelClientes]');
    }

    getBuscadorClientesWindow() {
        return this.browser.get('[id^=buscadorClientesWindow]');
    }

    getClientesGrid() {
        return this.getBuscadorClientesWindow().get('[id^=clientesGrid]');
    }

    getCercarBtnPanelClientes() {
        return this.getPanelClientes().find('.x-btn').contains('Cercar');
    }

    buscarNombreCypress() {
        this.getBuscadorClientesWindow().get(this.fields.busqueda).eq(0).type('cypress');
    }

    getCercarBtnBuscadorClientesWindow() {
        return this.getBuscadorClientesWindow().find('.x-btn').contains('Cercar');
    }

    seleccionarFilaClientesGrid() {
        this.getClientesGrid().find('.x-grid-cell').contains('cypress apellidos').dblclick();
    }

    /*** Unique Methods ***/

    getEtiquetasTab() {
        this.getPanelClientes().find('.x-tab-bar').contains('Etiquetes').click()
    }

    getClienteEtiquetasGrid() {
        return this.browser.get('[id^=clienteEtiquetasGrid]');
    }

    getEtiquetasUjiCombo() {
        return this.getClienteEtiquetasGrid().get(this.fieldsEtiquetasTab.etiqueta);
    }

    selectItemFromUjiComboWithValue(selector, valor) {
        cy.get(selector).first().click();
        cy.get(selector).invoke('data', 'componentid')
            .then(value => {
                cy.get(`#${value}-picker-listEl`).children("li:contains('"+valor+"')").first().click();
            });
    }

    typeEtiqueta(valor) {
        this.getClienteEtiquetasGrid().get(this.fieldsEtiquetasTab.etiqueta).first().type(valor);
        this.selectItemFromUjiComboWithValue(this.fieldsEtiquetasTab.etiqueta, 'Erasmus');
    }

    typeUnexpectedEtiqueta(valor) {
        this.getClienteEtiquetasGrid().get(this.fieldsEtiquetasTab.etiqueta).first().type(valor);
    }

    getAfegirButton() {
        return this.getClienteEtiquetasGrid().getButton('Afegir');
    }

    getEsborrarButton() {
        return this.getClienteEtiquetasGrid().getButton('Esborrar');
    }

    selectRowFromClienteEtiquetasGrid(valor) {
        this.getClienteEtiquetasGrid().find('.x-grid-cell').contains(valor).click();
    }
}

export default EtiquetasPage