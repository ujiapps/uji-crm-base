class EnvioCampanyaPage {

    fields = {
        tipoEnvioAuto: 'input[name=tipoEnvioAuto]',
        tipoCorreoEnvio:'input[name=tipoCorreoEnvio]',
        asunto: 'input[name=asunto]',
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#enviosCampanya';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem(){
        return this.browser.contains("Gestió de Campanyes");
    }

    selectTab() {
        return this.browser.get('[id^=tabCampanyas]').get('[id^=tabbar]').contains('Enviaments');
    }

    getGrid(){
        return this.browser.get('[id^=campanyaEnviosGrid]');
    }

    getAfegirBtn() {
        return this.getGrid().find('.x-btn').contains('Afegir');
    }

    getEsborrarBtn() {
        return this.getGrid().find('.x-btn').contains('Esborrar');
    }

    selectRow() {
        this.getGrid().find('.x-grid-cell').contains("Asunto cypress").click();
    }

    //************* Enviaments Window
    getWindow() {
        return this.browser.get('[id^=campanyaEnvioWindow]')
    }

    fillFormFields() {
       this.browser.selectFirstItem(this.fields.tipoEnvioAuto);
       this.browser.selectFirstItem(this.fields.tipoCorreoEnvio);
       this.browser.get(this.fields.asunto).type("Asunto cypress");
       this.getDesarBtn().click();
    }

    getDesarBtn() {
        return this.getWindow().getButton('Desar');
    }

    borrarEnvioCampanya() {
        this.selectRow();
        this.getEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

    //************* Programar envios
    getProgramarEnviosBtn() {
        return this.getGrid().find('.x-btn').contains('Programar enviament');
    }

    getProgramacionEnviosWindow() {
        return this.browser.get('[id^=programacionEnvioWindow]');
    }

    getProgramacionEnviosGrid() {
        return this.browser.get('[id^=enviosCampanyaProgramadosGrid]');
    }

    getAfegirProgramacionBtn() {
        return this.getProgramacionEnviosGrid().getButton('Afegir');
    }

    getEsborrarProgramacionBtn() {
        return this.getProgramacionEnviosGrid().getButton('Esborrar');
    }

    fillEditorFields() {
        this.getProgramacionEnviosGrid().getEditor().within(() => {
            this.browser.get('input[name=dias]').type(7);
        });
        this.getProgramacionEnviosGrid().find('a.x-btn.x-row-editor-update-button').click();
    }

    borrarProgramacionEnvio() {
        this.getEsborrarProgramacionBtn().click();
        this.browser.messageBoxAcceptBtn().click();
        this.getProgramacionEnviosWindow().find('.x-tool-close').click();
    }
}

export default EnvioCampanyaPage