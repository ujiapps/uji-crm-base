class DestinoOrigenPage {

    fields = {
        estadoOrigen: 'input[name=estadoOrigen]',
        campanyaDestino: 'input[name=campanyaDestino]',
        estadoDestino: 'input[name=estadoDestino]',
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#tipos-datos';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem(){
        return this.browser.contains("Gestió de Campanyes");
    }

    selectTab() {
        return this.browser.get('[id^=tabCampanyas]').get('[id^=tabbar]').contains('Campanyes destí-origen');
    }

    getGrid(){
        return this.browser.get('[id^=campanyasCampanyadestinoGrid]');
    }

    getAfegirBtn() {
        return this.getGrid().find('.x-btn').contains('Afegir');
    }

    getEsborrarBtn() {
        return this.getGrid().find('.x-btn').contains('Esborrar');
    }

    selectRow() {
        this.getGrid().find('.x-grid-cell').contains("Estado cypress").click();
    }


    //**************** window

    getWindow() {
        return this.browser.get('[id^=campanyasCampanyadestinoWindow]')
    }

    fillCombos() {

        this.browser.selectFirstItem(this.fields.estadoOrigen);
        this.browser.selectFirstItem(this.fields.campanyaDestino);
        this.browser.selectFirstItem(this.fields.estadoDestino);

        this.getWindow().getButton('Desar').click();
    }

    borrarDestinoOrigen() {
        this.selectRow();
        this.getEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }
}

export default DestinoOrigenPage