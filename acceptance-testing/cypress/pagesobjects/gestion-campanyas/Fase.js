import EstadoPage from "./Estado";

class FasePage {

    fields = {
        fechaInicio: 'input[name=fechaIni]',
        fechaFin: 'input[name=fechaFin]',
        faseNombre: 'input[name=faseNombre]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#gestion-campanyas';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem() {
        return this.browser.contains("Gestió de Campanyes");
    }

    selectTab() {
        return this.browser.get('[id^=tabCampanyas]').get('[id^=tabbar]').contains('Fases');
    }

    getAfegirBtn() {
        return this.getGrid().find('.x-btn').contains('Afegir');
    }

    getEsborrarBtn() {
        return this.getGrid().find('.x-btn').contains('Esborrar');
    }

    getGrid(){
        return this.browser.get('[id^=gridCampanyaFases]');
    }

    getEditorFase() {
        return this.getGrid().find('.x-grid-row-editor-wrap')
    }

    getDate(){
        let today = new Date();
        return today.getDate().toString().padStart(2, "0") + '/' + (today.getMonth()+1).toString().padStart(2, "0") + '/' + today.getFullYear();
    }

    fillEditorFasesGrid(){
        let editor = this.getEditorFase();
        editor.within(() => {
            this.browser.get(this.fields.fechaInicio).type(this.getDate());
            this.browser.get(this.fields.fechaFin).type(this.getDate());
        });
        this.browser.selectFirstItem(this.fields.faseNombre);
        this.browser.editorAcceptBtn().click();
    }

    selectRow(){
        this.getGrid().find('.x-grid-cell').contains(this.getDate()).then((cp) => {
            cp.click();
        });
    }

    clearTestFasesGrid(){
        this.getEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

}

export default FasePage