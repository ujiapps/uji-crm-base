class EstadoPage {

    fields = {
        nombre: 'input[name=nombre]',
        accion: 'input[name=accion]'
    }

    fieldsMotivo = {
        nombre: 'input[name=nombreEs]',
        nom: 'input[name=nombreCa]',
        name: 'input[name=nombreUk]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#gestion-campanyas';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem() {
        return this.browser.contains("Gestió de Campanyes");
    }

    selectTab() {
        return this.browser.get('[id^=tabCampanyas]').get('[id^=tabbar]').contains('Estats');
    }

    getAfegirBtn() {
        return this.getGrid().find('.x-btn').contains('Afegir');
    }

    getEsborrarBtn() {
        return this.getGrid().find('.x-btn').contains('Esborrar');
    }

    getOpcionesBtn() {
        return this.getGrid().find('.x-btn').contains('Gestió de motius');
    }

    getGrid(){
        return this.browser.get('[id^=gridCampanyaEstados]');
    }

    getEditorEstado() {
        return this.getGrid().find('.x-grid-row-editor-wrap')
    }

    fillEditorEstadosGrid(){
        let editor = this.getEditorEstado();
        editor.within(() => {
            this.browser.get(this.fields.nombre).type("Estado cypress");
            this.browser.get(this.fields.accion).type("an action");
        });
        this.browser.editorAcceptBtn().click();
    }

    selectRow(){
        this.getGrid().find('.x-grid-cell').contains("Estado cypress").then((cp) => {
            cp.click();
        });
    }

    clearTestEstadosGrid(){
        this.getGrid().get('td.x-grid-cell').contains('Estado cypress').first().click();
        this.getEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }


    // ***************************** MOTIVOS

    addMotivoBtn(){
        this.getOpcionesBtn().click();
    }

    getWindowMotivo(){
        return this.browser.get('[data-componentid^=estadosWindow]');
    }

    getMotivoGrid(){
        return this.getWindowMotivo().get('[id^=gridCampanyaEstadoMotivos]');
    }

    getMotivoAfegirBtn() {
        return this.getMotivoGrid().find('.x-btn').contains('Afegir');
    }

    getMotivoEsborrarrBtn() {
        return this.getMotivoGrid().find('.x-btn').contains('Esborrar');
    }

    getEditorMotivo() {
        return this.getMotivoGrid().find('.x-grid-row-editor-wrap');
    }

    fillEditorMotivosGrid(){
        let editor = this.getEditorMotivo();
        editor.within(() => {
            this.browser.get(this.fieldsMotivo.nombre).type("Motivo cypress");
            this.browser.get(this.fieldsMotivo.nom).type("Motiu cypress");
            this.browser.get(this.fieldsMotivo.name).type("Motive cypress");
        });
        this.getMotivoGrid().find('a.x-btn.x-row-editor-update-button').click();
    }

    clearTestMotivoGrid(){
        this.getMotivoGrid().find('.x-grid-cell').contains("Motivo cypress").then((cp) => {
            cp.click();
        });
        this.getMotivoEsborrarrBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

    closeMotivoWindow(){
        this.getWindowMotivo().find('.x-tool-close').then((cp) => {
            cp.click();
        });
    }
}

export default EstadoPage