class FormulariosCampanyaPage {

    fields = {
        titulo: 'input[name=titulo]',
        solicitarIdentificacion:'input[name=solicitarIdentificacion]',
        solicitarNombre:'input[name=solicitarNombre]',
        solicitarCorreo:'input[name=solicitarCorreo]',
        solicitarMovil:'input[name=solicitarMovil]',
        solicitarPostal:'input[name=solicitarPostal]',
        radiofieldEstadoDestino: 'input[name=radiofieldEstadoDestino]',
    }

    datosFormularioFields = {
        clienteDatoTipo:'input[name=clienteDatoTipo]',
        tipoAcceso:'input[name=tipoAcceso]',
        orden:'input[name=orden]',
        textoCa: 'input[name=textoCa]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#formulariosCampanya';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem(){
        return this.browser.contains("Gestió de Campanyes");
    }

    selectTab() {
        return this.browser.get('[id^=tabCampanyas]').get('[id^=tabbar]').contains('Formularis');
    }

    getGrid(){
        return this.browser.get('[id^=campanyaFormulariosGrid]');
    }

    getAfegirBtn() {
        return this.getGrid().find('.x-btn').contains('Afegir');
    }

    getEsborrarBtn() {
        return this.getGrid().find('.x-btn').contains('Esborrar');
    }

    selectRow() {
        this.getGrid().find('.x-grid-cell').contains("Titulo Cypress").click();
    }

    //**************** window

    getWindow() {
        return this.browser.get('[id^=formularioWindow]');
    }

    fillFormFields() {
        this.browser.get(this.fields.titulo).type("Titulo Cypress").click();
        this.browser.selectFirstItem(this.fields.solicitarIdentificacion);
        this.browser.selectFirstItem(this.fields.solicitarNombre);
        this.browser.selectFirstItem(this.fields.solicitarCorreo);
        this.browser.selectFirstItem(this.fields.solicitarMovil);
        this.browser.selectFirstItem(this.fields.solicitarPostal);

        this.browser.get('.x-form-item-body').contains('Estado cypress').get('.x-form-cb-input').check();
        this.getWindow().getButton('Desar').click();
        //this.getWindow().get('.x-toolbar-item').contains('Tancar').click();
    }

    borrarFormularioCampanya() {
        this.selectRow();
        this.getEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

    //**************** Datos del formulario
    getDatosFormularioGrid() {
        return this.browser.get('[id^=campanyaDatosGrid]');
    }

    getAfegirDatoFormularioBtn() {
       return this.getDatosFormularioGrid().getButton('Afegir');
       cy.waitRequest('@clientedatotipo', 200)
        cy.waitRequest('@grupoall', 200)
        cy.waitRequest('@campanyaitem', 200)
        cy.waitRequest('@loadDatoFormulario', 200)
        cy.waitRequest('@accesotipo', 200)
        cy.waitRequest('@tipovalidacion', 200)
    }

    getEsborrarDatoFormularioBtn() {
        return this.getDatosFormularioGrid().getButton('Esborrar');
    }

    getDatosFormularioWindow() {
        return this.browser.get('[id^=campanyaDatosWindow]');
    }

    fillDatosFormularioForm() {
        //Dada, tipus d'acces, ordre
        this.browser.selectFirstItem(this.datosFormularioFields.clienteDatoTipo);
        this.browser.get(this.datosFormularioFields.textoCa).type('Dada Extra Cypress');
        this.browser.selectFirstItem(this.datosFormularioFields.tipoAcceso);
        this.browser.get(this.datosFormularioFields.orden).type(10).click();
        this.getDatosFormularioWindow().getButton('Desar').click();
    }

    anyadirDatoFormulario() {
        this.getAfegirDatoFormularioBtn().click();
        this.fillDatosFormularioForm();
    }

    selectDatoRow(){
        this.getDatosFormularioGrid().find('.x-grid-cell').contains('Dada Extra Cypress').click();
        cy.wait('@loadDatoFormulario');
    }

    borrarDatoFormulario() {
        this.selectDatoRow();
        this.getEsborrarDatoFormularioBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

}

export default FormulariosCampanyaPage