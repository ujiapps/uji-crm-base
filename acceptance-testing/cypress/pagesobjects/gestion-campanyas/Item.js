class ItemPage {

    fieldsItem = {
        programa: 'input[name=etiqueta]',
        grupo: 'input[name=valor]',
        item: 'input[name=item]'
    };

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#tipos-datos';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem() {
        return this.browser.contains("Gestió de Campanyes");
    }

    selectTab() {
        return this.browser.get('[id^=tabCampanyas]').get('[id^=tabbar]').contains('Items');
    }

    getGrid(){
        return this.browser.get('[id^=gridCampanyaItems]');
    }

    getAfegirBtn() {
        return this.getGrid().find('.x-btn').contains('Afegir');
    }

    getEsborrarBtn() {
        return this.getGrid().find('.x-btn').contains('Esborrar');
    }

    selectRow(){
        this.getGrid().find('.x-grid-cell').contains("Espanya").click();
    }

    borrarItem() {
        this.selectRow();
        this.getEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

    // ******************** window

    getWindow() {
        return this.browser.get('[id^=campanyaItemWindow]');
    }

    getItemCampanyaGrid() {
        return this.getWindow().get('[id^=itemsCampanyaGrid]');
    }

    getDesarBtn() {
        return this.getWindow().get('.x-btn').contains('Desar');
    }

    getFiltroGruposCombo() {
        return this.getWindow().get('input[name=filtroGrupos]').should('be.visible');
    }

    selectGrupo(){
        this.getFiltroGruposCombo().then((cp) => {
            this.getWindow().get('.x-form-arrow-trigger').click();
            this.getWindow().get('.x-boundlist-item').contains('Països de naixement').click();
            this.getItemCampanyaGrid().find('.x-grid-cell').contains("Espanya").first().click().then(()=>{
                this.getWindow().find('.x-btn').contains('Desar').click();
            })
        });
    }


}

export default ItemPage