
class CampanyaPage {

    fields = {
        nombre: 'input[name=nombre]',
        descripcion: 'input[name=descripcion]',
        programa: 'input[name=programaId]',
        campanya: 'input[name=campanyaId]',
        vinculo: 'input[name=subVinculo]',
        fideliza: 'input[name=fideliza]',
        lineafacturacion: 'input[name=lineaFacturacionId]',
        correo: 'input[name=correoAvisaCobro]',
        textoLOPD: 'input[name=campanyaTextoId]',
        fechaIni: 'input[name=fechaInicioActiva]',
        fechaFin: 'input[name=fechaFinActiva]',
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#gestion-campanyas';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem() {
        return this.browser.contains("Gestió de Campanyes");
    }


    // **************** panel campanya

    getPanelCampanyas() {
        return this.browser.get('[id^=panelCampanyas]');
    }

    getAfegirBtn() {
        return this.getPanelCampanyas().find('.x-btn').contains('Afegir');
    }

    getEsborrarBtn() {
        return this.getPanelCampanyas().find('.x-btn').contains('Esborrar');
    }

    clearCampanyaGrid(){
        this.getPanelCampanyas().get('[id^=treepanel]').get('.x-grid-cell').contains("Prueba Cypress").first().click();
        this.getEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

    selectCampanya() {
        this.getPanelCampanyas().get('[id^=treepanel]').get('.x-grid-cell').contains("Prueba Cypress").first().click();
    }


    // **************** ventana campanya

    getVentana() {
        return this.browser.get('[data-componentId^=anyadirCampanyaWindow]');
    }
    getDesarBtn(){
        return this.getVentana().scrollIntoView().find('a.x-btn').contains('Desar');
    }

    getMessageBox() {
        return this.browser.get('.x-message-box').contains('Acceptar');
    }

    fillCampanyaForm(){
        this.getVentana().get(this.fields.nombre).type('Prueba Cypress campanya');
        this.browser.selectItemValue(this.fields.programa, "213");
        cy.waitRequest('@lineafacturacion', 200)
        this.browser.selectItemValue(this.fields.textoLOPD, "Alumni Premium");
    }

    createCampanya() {
        this.getAfegirBtn().click();
        cy.waitRequest('@subvinculo', 200)
        cy.waitRequest('@campanyatexto', 200)
        cy.waitRequest('@campanya', 200)
        cy.waitRequest('@programa', 200)
        this.getDesarBtn().click();
        cy.hasInvalidFieldsForm(3);
        this.getMessageBox().click();
        this.fillCampanyaForm();
        cy.hasInvalidFieldsForm(0);
        this.getDesarBtn().click();
    }

}

export default CampanyaPage