class EnvioAdministracionPage {

    fields = {
        campanyaEnvioTipoId: 'input[name=campanyaEnvioTipoId]',
        asunto: 'input[name=asunto]',
        emails: 'input[name=emails]',
        cuerpoEnvioAuto: 'input[name=cuerpoEnvioAuto]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#enviosAdmin';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem(){
        return this.browser.contains("Gestió de Campanyes");
    }

    selectTab() {
        return this.browser.get('[id^=tabCampanyas]').get('[id^=tabbar]').contains('Enviaments Administració');
    }

    getGrid() {
        return this.browser.get('[id^=campanyaEnvioAdminGrid]');
    }

    getAfegirBtn() {
        return this.getGrid().getButton('Afegir');
    }

    getEsborrarBtn() {
        return this.getGrid().getButton('Esborrar');
    }

    selectRow() {
        this.getGrid().find('.x-grid-cell').contains("Asunto cypress").click();
    }

    //**************** Add window
    getWindow() {
        return this.browser.get('[id^=campanyaEnvioAdminWindow]');
    }

    fillFormFields() {
        this.browser.selectFirstItem(this.fields.campanyaEnvioTipoId);
        this.getWindow().get(this.fields.asunto).type('Asunto cypress');
        this.getWindow().get(this.fields.emails).type('a@a.es');
        this.getWindow().get('iframe').then(elem => {
            const body = elem.contents().find('body');

            let stripe = this.browser.wrap(body);
            stripe.eq(0).clear().type("Prueba de Cypress.");
        });

        this.getWindow().getButton('Desar').click();
    }

    borrarEnvioAdmin() {
        this.selectRow();
        this.getEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }
}

export default EnvioAdministracionPage