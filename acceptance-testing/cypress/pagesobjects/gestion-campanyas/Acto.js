class ActoCampanyaPage {

    fields = {
        titulo: 'input[name=titulo]',
        duracion: 'input[name=duracion]',
        contenido: 'input[name=contenido]',
        fecha: 'input[name=fecha]',
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#actosCampanya';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem(){
        return this.browser.contains("Gestió de Campanyes");
    }

    selectTab() {
        return this.browser.get('[id^=tabCampanyas]').get('[id^=tabbar]').contains('Actes');
    }

    getGrid(){
        return this.browser.get('[id^=campanyaActosGrid]');
    }

    getAfegirBtn() {
        return this.getGrid().find('.x-btn').contains('Afegir');
    }

    getEsborrarBtn() {
        return this.getGrid().find('.x-btn').contains('Esborrar');
    }

    selectRow() {
        this.getGrid().find('.x-grid-cell').contains("Titulo cypress").click();
    }

    //**************** window

    getWindow() {
        return this.browser.get('[id^=campanyaActoWindow]');
    }

    fillFormFields() {
        this.browser.get(this.fields.titulo).type('Titulo cypress');
        this.browser.get(this.fields.duracion).type(10);
        this.browser.get(this.fields.contenido).type('Contenido Cypress');
        this.browser.get(this.fields.fecha).type('15/09/2021');

        this.getWindow().getButton('Desar').click();
    }

    borrarActoCampanya() {
        this.selectRow();
        this.getEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }
}

export default ActoCampanyaPage