class GestionEnviosPage {
    fields= {
        nombre: 'input[name=nombre]',
        contenido: 'input[name=contenido]',
        ujifilefield: 'input[name=file]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#gestion-envios';
    }

    getMenuItem() {
        return this.browser.contains("Gestió de enviaments");
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getGestionEnviosGrid() {
        return this.browser.get('[id^=gestionEnviosGrid]');
    }

    getEditorPlantillaWindow() {
        return this.browser.get('[id^=editorPlantillaWindow]');
    }

    getUjiWindowUploader() {
        return this.browser.get('[id^=ujiwindowuploader]');
    }

    getAfegirGestionEnviosBtn() {
        return this.getGestionEnviosGrid().getButton('Afegir');
    }

    getDeleteBtn() {
        return this.getGestionEnviosGrid().getButton('Esborrar')
    }

    getEditarBtn() {
        return this.getGestionEnviosGrid().getButton('Editar');
    }

    selectRowEnvio(elem) {
        return this.browser.get('.x-grid-cell').contains(elem);
    }

    writeInContenido() {
        let cy = this.getEditorPlantillaWindow();
        cy.get('.x-panel-body').get('iframe').then(elem => {
            const  body = elem.contents().find('body');
            let stripe = cy.wrap(body);
            stripe.type("Cypress Test Text Example");
        });
    }

    editContenido() {
        let cy = this.getEditorPlantillaWindow();
        cy.get('.x-panel-body').get('iframe').then(elem => {
            const  body = elem.contents().find('body');
            let stripe = cy.wrap(body);
            stripe.clear().type("EDIT Cypress Test Text Example");
        });
    }

    abrirServidorDeImagenes() {
        return this.getEditorPlantillaWindow().get('.cke_dialog_ui_button').contains('Veure');
    }

    getNomTextField() {
        return this.getEditorPlantillaWindow().find(this.fields.nombre);
    }

    getDesarBtn() {
        return this.getEditorPlantillaWindow().getButton('Desar');
    }

    getCancelarBtn() {
        return this.getEditorPlantillaWindow().get('.x-autocontainer-innerCt').contains('Cancel·lar');
    }

    getMensageDeAlerta() {
        return this.browser.messageBoxAcceptBtn();
    }

    getAcceptarBtnMessageBox() {
        return this.browser.uniqueMessageBoxAcceptBtn();
    }

    getNavegaBtn() {
        return this.getUjiWindowUploader().get(this.fields.ujifilefield);
    }
}

export default GestionEnviosPage