class ElementosSuscriptoresPage {

    fields = {
        tipoAccesoId: 'input[name=tipoAccesoId]',
        nombreCa: 'input[name=nombreCa]',
        nombreEs: 'input[name=nombreEs]',
        nombreUk: 'input[name=nombreUk]',
        filtroGrupos: 'input[name=filtroGrupos]',
    }

    testStrings = {
        nom: "Test Item",
        nombre: "Test Item",
        name: "Test Item",
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#elementosSuscriptores';
    }

    getElementosGrid(){
        return this.browser.get('[id^=itemsGrid]');
    }

    getAfegirItemWindow() {
        return this.browser.get('[data-componentId^=itemsWindow]');
    }

    getAfegirElementoBtn() {
        return this.getElementosGrid().getButton('Afegir');
    }

    getEsborrarElementoBtn() {
        return this.getElementosGrid().getButton('Esborrar');
    }

    fillFormFields() {
        this.browser.selectFirstItem(this.fields.tipoAccesoId);
        this.getAfegirItemWindow().within( () => {
            this.browser.get(this.fields.nombreCa).type(this.testStrings.nom);
            this.browser.get(this.fields.nombreEs).type(this.testStrings.nombre);
            this.browser.get(this.fields.nombreUk).type(this.testStrings.name);
        });
    }

    getActualizarWindowBtn() {
        return this.getAfegirItemWindow().getButton('Actualitzar');
    }

    eliminarItem() {
        this.getGridRow();
        this.getEsborrarElementoBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

    getGridRow() {
        this.getElementosGrid().get('.x-grid-cell').contains(this.testStrings.nom).first().click();
    }

    //Items Origen - Items Destino
    getItemsOrigenBtn() {
        return this.getElementosGrid().getButton('Items origen');
    }

    getAddItemsOrigenWindow() {
        return this.browser.get('[data-componentId^=itemsItemsAddWindow]');
    }

    getItemsOrigenGrid() {
        return this.browser.get('[id^=itemsOrigenGrid]');
    }

    getAfegirItemOrigenBtn() {
        return this.getItemsOrigenGrid().getButton('Afegir');
    }

    getEsborrarItemOrigenBtn() {
        return this.getItemsOrigenGrid().getButton('Esborrar');
    }

    seleccionarComboItemOrigen() {
        this.browser.get('input[name=filtroGrupos]').click();
        this.browser.get('.x-boundlist-item').contains('Països de naixement').click();
    }

    anyadirItemOrigen(){
        this.browser.get('.x-grid-cell').contains('Espanya').first().click();
        this.getAddItemsOrigenWindow().getButton('Afegir').click();
    }

    borrarItemOrigen() {
        this.getItemOrigenRow();
        this.getEsborrarItemOrigenBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

    getItemOrigenRow() {
        this.getItemsOrigenGrid().get('.x-grid-cell-inner').contains('Espanya').first().click();
    }

}

export default ElementosSuscriptoresPage