import suscripciones from '../../fixtures/suscripciones.json'

class SuscripcionesPage {

     fields = {
          tipoAccesoId: 'input[name=tipoAccesoId]',
          orden: 'input[name=orden]',
          desde: 'input[name=desde]',
          asunto: 'input[name=asunto]',
          responderA: 'input[name=responderA]',
          nombreCa: 'input[name=nombreCa]',
          nombreEs: 'input[name=nombreEs]',
          nombreUk: 'input[name=nombreUk]',
          programa: 'input[name=programa]'
     }

     constructor(browser) {
          this.browser = browser;
          this.hastag = '#suscripciones';
     }

     getMenuItem() {
          return this.browser.contains("Gestió de Suscriptors");
     }

     getSuscripcionesGrid() {
          return this.browser.get('[id^=gruposGrid]');
     }

     getSuscriptoresWindow() {
          return this.browser.get('[data-componentId^=gruposWindow]');
     }

     getAfegirGrupoBtn() {
          return this.getSuscripcionesGrid().getButton('Afegir');
     }

     getEsborrarGrupoBtn() {
          return this.getSuscripcionesGrid().getButton('Esborrar');
     }

     visitSection() {
          this.browser.visit("/");
          this.getMenuItem().click();
          cy.waitRequest('@admin', 200);
          this.browser.selectItemValue(this.fields.programa, suscripciones.grupo);
     }

     visitSectionSuscr() {
          this.browser.visit("/");
          this.getMenuItem().click();
          cy.waitRequest('@programadmin', 200);
          this.browser.selectItemValue(this.fields.programa, suscripciones.grupo);
     }

     fillFormFields() {
          this.browser.selectFirstItem(this.fields.tipoAccesoId);
          this.getSuscriptoresWindow().within( () => {
               this.browser.get(this.fields.orden).type(suscripciones.orden);
               this.browser.get(this.fields.desde).type(suscripciones.desde);
               this.browser.get(this.fields.asunto).type(suscripciones.asunto);
               this.browser.get(this.fields.responderA).type(suscripciones.responderA);
               this.browser.get(this.fields.nombreCa).type(suscripciones.nombreCa);
               this.browser.get(this.fields.nombreEs).type(suscripciones.nombreEs);
               this.browser.get(this.fields.nombreUk).type(suscripciones.nombreUk);
          })
     }

     getActualizarWindowBtn() {
          return this.getSuscriptoresWindow().getButton('Actualitzar');
     }

     clearTest() {
          this.getGridRow();
          this.getEsborrarGrupoBtn().click();
          this.browser.messageBoxAcceptBtn().click();
     }

     getGridRow() {
          this.browser.get('.x-grid-cell').contains(suscripciones.nombreCa).first().click();
     }
}

export default SuscripcionesPage