class TiposSeguimientosPage {

    fields = {
        nombre: 'input[name=nombre]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#tiposSeguimientos';
    }

    getMenuItem() {
        let upper = this.browser.contains("Altres gestions");
        upper.click();
        return this.browser.contains("Tipus seguiments");
    }

    getTiposSeguimientosGrid() {
        return this.browser.get('[id^=tiposSeguimientosGrid]');
    }

    getAfegirSeguimientoBtn() {
        return this.getTiposSeguimientosGrid().getButton('Afegir');
    }

    getEsborrarSeguimientoBtn() {
        return this.getTiposSeguimientosGrid().getButton('Esborrar');
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    fillEditorFields() {
        this.browser.get('[id^=tiposSeguimientosGrid]').getEditor().within(() => {
            this.browser.get(this.fields.nombre).type("Seguimiento Cypress Test");
        });
    }

    clearTest() {
        this.browser.get('.x-grid-cell').contains("Seguimiento Cypress Test").first().click();
        this.getEsborrarSeguimientoBtn().click();
        this.browser.messageBoxAcceptBtn().click();

    }
}

export default TiposSeguimientosPage