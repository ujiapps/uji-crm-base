class ServiciosPage {

    fieldsServicio = {
        nombre: 'input[name=nombre]',
    }

    fieldsLineaFacturacion = {
        nombre: 'input[name=nombre]',
        descripcion: 'input[name=descripcion]',
        comboEmisora: 'input[name=comboEmisora]',
        comboFormato: 'input[name=comboFormato]',
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#servicios';
    }

    visitarSeccion() {
        this.browser.visit('/');
        this.browser.contains('Gestió Serveis').click();
    }

    getServiciosGrid() {
        return this.browser.get('[id^=serviciosGrid]');
    }

    getFilaServiciosGrid() {
        return this.browser.get('.x-grid-cell').contains('servicio cypress').first();
    }

    getPersonasGrid() {
        return this.browser.get('[id^=personasServiciosGrid]');
    }

    getLineasFacturacionGrid() {
        return this.browser.get('[id^=lineasFacturacionGrid]');
    }

    getLookupWindow() {
        return this.browser.get('[data-componentId^=ujilookupWindow]');
    }

    getLookupGrid() {
        return this.getLookupWindow().get('[id^=gridpanel]');
    }

    getLookupInput() {
        return this.getLookupWindow().get('input[name=query]');
    }

    getLookupButton() {
        return this.getLookupWindow().getButton('Cerca');
    }

    rellenarServicio() {
        this.getServiciosGrid().getButton('Afegir').click();
        this.getServiciosGrid().getEditor().get(this.fieldsServicio.nombre).type('servicio cypress');
        this.getServiciosGrid().editorAcceptBtn().click();
    }

    rellenarPersona() {
        this.getFilaServiciosGrid().click();
        cy.waitRequest('@serviciousuario', 200);
        cy.waitRequest('@lineafacturacion', 200);
        cy.waitRequest('@emisora', 200);
        cy.waitRequest('@reciboformato', 200);

        this.getPersonasGrid().getButton('Afegir').click();
        this.getPersonasGrid().getEditor().click();

        this.getLookupInput().should('be.visible');

        this.getLookupInput().type('helena');
        this.getLookupButton().click();
        cy.waitRequest('@lookup', 200);

        this.getLookupWindow().contains('td', 'Helena').dblclick();
        this.getPersonasGrid().editorAcceptBtn().click();
    }

    rellenarLineaFacturacion() {
        this.getFilaServiciosGrid().click();
        this.selectTab('Línies Facturació').click();
        this.getLineasFacturacionGrid().getButton('Afegir').click();

        this.getLineasFacturacionGrid().getEditor().get(this.fieldsLineaFacturacion.nombre).type('linea facturacion cypress');
        this.getLineasFacturacionGrid().getEditor().get(this.fieldsLineaFacturacion.descripcion).type('linea facturacion cypress');

        this.getLineasFacturacionGrid().selectItemValue(this.fieldsLineaFacturacion.comboEmisora, "Servei de Gestió de la Docència i Estudiants");
        this.getLineasFacturacionGrid().selectItemValue(this.fieldsLineaFacturacion.comboFormato, "NUM_UJI");
        this.getLineasFacturacionGrid().editorAcceptBtn().click();
    }

    selectTab(nombrePanel) {
        return this.browser.get('[id^=tabpanel]').get('[id^=tabbar]').contains(nombrePanel);
    }

    borrarServicio() {
        this.browser.get('.x-grid-cell').contains('servicio cypress').first().click();
        this.getServiciosGrid().getButton('Esborrar').click();
        this.browser.messageBoxAcceptBtn().click();
    }

    borrarLineaFacturacion() {
        this.getFilaServiciosGrid().click();
        this.selectTab('Línies Facturació').click();
        this.browser.get('.x-grid-cell').contains('linea facturacion cypress').first().click();
        this.getLineasFacturacionGrid().getButton('Esborrar').click();
        this.browser.messageBoxAcceptBtn().click();
    }

    borrarPersona() {
        this.getFilaServiciosGrid().click();
        this.selectTab('Persones').click();
        this.getPersonasGrid().get('td.x-grid-cell').get('div').contains('Helena').first().click();
        this.getPersonasGrid().getButton('Esborrar').click();
        this.browser.messageBoxAcceptBtn().click();
    }
}

export default ServiciosPage