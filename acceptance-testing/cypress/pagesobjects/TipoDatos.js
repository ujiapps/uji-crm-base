
class TipoDatosPage {

    fieldsTipoDato = {
        nombre: 'input[name=nombre]',
        tipo: 'input[name=tipoCombo]',
        tamanyo: 'input[name=tamanyo]',
        orden: 'input[name=orden]',
        duplicar: 'input[name=duplicar]',
        modificar: 'input[name=modificar]'
    };

    fieldsItemDato = {
        etiqueta: 'input[name=etiqueta]',
        valor: 'input[name=valor]',
    };

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#tipos-datos';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem(){
        let upper = this.browser.contains("Altres gestions");
        upper.click();
        return this.browser.contains("Tipus de dades");
    }

    // *************** TIPO DATOS
    getTipoDatosGrid() {
        return this.browser.get('[id^=tiposDatosGrid]');
    }

    getAfegirTipoDatoBtn() {
        return this.getTipoDatosGrid().find('.x-btn').contains('Afegir');
    }

    getEsborrarBtn() {
        return this.getTipoDatosGrid().find('.x-btn').contains('Esborrar');
    }

    getAfegirItemsBtn() {
        return this.getTipoDatosGrid().find('.x-btn').contains('Items');
    }

    getEditorTipoDatos() {
        return this.getTipoDatosGrid().find('.x-grid-row-editor-wrap')
    }

    fillEditorTipoDatoGridFields() {
        let editor = this.getEditorTipoDatos();
        editor.within(() => {
            this.browser.get(this.fieldsTipoDato.nombre).type("Tipo dato test cypress");
            //this.browser.get(this.fieldsTipoDato.tipo).click();
            this.browser.get(this.fieldsTipoDato.tamanyo).type("400");
            //this.browser.get(this.fieldsTipoDato.orden).type("100");
            this.browser.get(this.fieldsTipoDato.duplicar).check();
            this.browser.get(this.fieldsTipoDato.modificar).check();
        });
        this.browser.selectItemValue(this.fieldsTipoDato.tipo, "Select");
        this.browser.editorAcceptBtn().click();
    }

    clearTestTipoDatoGrid(){
        this.browser.get('.x-grid-cell').contains("Tipo dato test cypress").first().click();
        this.getEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

    // ******************** DETALLES

    getWindow(){
        return this.browser.get('[id^=detalleTiposDatosWindow]');
    }

    getItemGrid(){
        return this.browser.get('[id^=detalleTiposDatosGrid]');
    }

    getEditorItems() {
        return this.getItemGrid().find('.x-grid-row-editor-wrap')
    }

    getColumnHeader(text){
        return this.getItemGrid().find('.x-column-header').contains(text)
    }

    getItemAddBtn(){
        return this.getItemGrid().find('.x-btn').contains('Afegir');
    }

    getItemEsborrarBtn(){
        return this.getItemGrid().find('.x-btn').contains('Esborrar');
    }

    getItemCancelarBtn(){
        return this.getWindow().find('.boton-cancelar');
    }

    fillEditorItemGridFields(){
        let editor = this.getEditorItems();
        editor.within(() => {
            this.browser.get(this.fieldsItemDato.etiqueta).type("A cypress");
            this.browser.get(this.fieldsItemDato.valor).type("400");
        });
        this.browser.editorAcceptBtn().click();
    }

    selectRow(){
        this.browser.get('.x-grid-cell').contains("Select").first().click();
    }

    clearTestItemGrid(){
        this.getColumnHeader('Etiqueta').click();
        this.getColumnHeader('Etiqueta').click();
        this.getItemGrid().get('td.x-grid-cell').get('div').contains('A cypress').first().click();
        this.getItemEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

}

export default TipoDatosPage