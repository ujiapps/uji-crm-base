class EnvioTipoTarifaPage {

    fields = {
        asunto: 'input[name=asunto]',
        desde:'input[name=desde]',
        responderA:'input[name=responderA]',
        tipoCorreoEnvio:'input[name=tipoCorreoEnvio]'
    }

    fieldsProg = {
        dias: 'input[name=dias]',
        cuandoRealizarEnvio: 'input[name=cuandoRealizarEnvio]',
        tipoFecha: 'input[name=tipoFecha]',
    }

    ejemplos = {
        asunto: 'Asunto Test',
        desde:'desde@test.es',
        responderA:'para@test.es',
        dias: 10
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#enviosTipoTarifa';
    }

    getEnviosTab() {
        return this.browser.get('.x-tab').contains("Enviaments");
    }

    getEnviosGrid() {
        return this.browser.get('[id^=envioTarifaGrid]');
    }

    getAfegirEnvioBtn() {
        return this.getEnviosGrid().getButton('Afegir');
    }

    getEsborrarEnvioBtn() {
        return this.getEnviosGrid().getButton('Esborrar');
    }

    fillEditorFields() {
        this.getEnviosGrid().getEditor().within(() => {
            this.browser.get(this.fields.asunto).type(this.ejemplos.asunto);
            this.browser.get(this.fields.desde).type(this.ejemplos.desde);
            this.browser.get(this.fields.responderA).type(this.ejemplos.responderA);
        });
        this.browser.selectFirstItem(this.fields.tipoCorreoEnvio);
    }

    clearTest() {
        this.getEnvioGridRow();
        this.getEsborrarEnvioBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

    getEnvioGridRow() {
        this.browser.get('.x-grid-cell').contains(this.ejemplos.asunto).first().click();
    }

    //Programación de envios
    //=====================================================
    getProgramacionEnviosBtn() {
        return this.getEnviosGrid().getButton('Programació de l\'enviament');
    }

    getProgramacionEnviosGrid() {
         return this.browser.get('[id^=programacionGrid]');
    }

    getAfegirProgramacionEnviosBtn() {
        return this.getProgramacionEnviosGrid().getButton('Afegir');
    }

    getEsborrarProgramacionEnviosBtn() {
        return this.getProgramacionEnviosGrid().getButton('Esborrar');
    }

    fillProgramacionEnviosFields() {
        this.getProgramacionEnviosGrid().getEditor().within(() => {
            this.browser.get(this.fieldsProg.dias).type(this.ejemplos.dias);
        });

        this.browser.selectFirstItem(this.fieldsProg.cuandoRealizarEnvio);
        this.seleccionarTipoFecha();
    }

    seleccionarTipoFecha() {
        this.getProgramacionEnviosGrid().find('.x-form-arrow-trigger').first().click();
        this.getProgramacionEnviosGrid().get('.x-boundlist-item').contains('Data fi rebut').click();
    }

    borrarProgramacionEnvio() {
        this.getProgramacionEnviosGrid().find('.x-grid-cell').contains(this.ejemplos.dias).first().click();
        this.getEsborrarProgramacionEnviosBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }
}

export default EnvioTipoTarifaPage