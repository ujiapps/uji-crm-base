class TarifasPage {

    fields = {
        fechaInicio:'input[name=fechaInicio]',
        fechaFin: 'input[name=fechaFin]',
        importe:'input[name=importe]',
    }

    ejemplos = {
        ini: "10/10/2021",
        fin: "11/11/2021",
        importe: 40
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#tarifas';
    }

    getTarifasTab() {
        return this.browser.get('.x-tab').contains("Tarifes");
    }

    getTarifasGrid() {
        return this.browser.get('[id^=tarifasGrid]');
    }

    getAfegirTarifaBtn() {
        return this.getTarifasGrid().getButton('Afegir');
    }

    getEsborrarTarifaBtn() {
        return this.getTarifasGrid().getButton('Esborrar');
    }

    fillEditorFields() {
        this.getTarifasGrid().getEditor().within(() => {
            this.browser.get(this.fields.fechaInicio).type(this.ejemplos.ini);
            this.browser.get(this.fields.fechaFin).type(this.ejemplos.fin);
            this.browser.get(this.fields.importe).type(this.ejemplos.importe);
        })
    }

    clearTest() {
        this.getTarifasGrid().get('.x-grid-cell').contains(this.ejemplos.ini).first().click();
        this.getEsborrarTarifaBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }
}

export default TarifasPage