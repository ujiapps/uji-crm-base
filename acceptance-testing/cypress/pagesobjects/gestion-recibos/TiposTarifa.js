import tiposTarifa from '../../fixtures/tiposTarifa.json'

class TiposTarifaPage {

    fields = {
        nombre: 'input[name=nombre]',
        mesesVigencia: 'input[name=mesesVigencia]',
        mesesCaduca: 'input[name=mesesCaduca]',
        defecto: 'input[name=defecto]',
        comboLineasFacturacion: 'input[name=comboLineasFacturacion]',
        comboCampanyasTarifas: 'input[name=comboCampanyasTarifas]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#tiposTarifas';
    }

    getMenuItem() {
        return this.browser.contains("Gestió de Rebuts");
    }

    getTiposTarifasTab() {
        return this.browser.get('.x-tab').contains("Tipus tarifes");
    }

    getTiposTarifasGrid() {
        return this.browser.get('[id^=tiposTarifasGrid]');
    }

    getAfegirTipoTarifaBtn() {
        return this.getTiposTarifasGrid().getButton('Afegir');
    }

    getEsborrarBtn() {
        return this.getTiposTarifasGrid().getButton('Esborrar');
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
        this.getTiposTarifasTab().click();
        cy.wait(500);
        this.browser.selectFirstItem(this.fields.comboCampanyasTarifas);
    }

    fillEditorFields() {
        this.browser.get('[id^=tiposTarifasGrid]').getEditor().within( () => {
            this.browser.get(this.fields.nombre).type(tiposTarifa.nombre);
            this.browser.get(this.fields.mesesVigencia).type(tiposTarifa.mesesVigencia);
            this.browser.get(this.fields.mesesCaduca).type(tiposTarifa.mesesCaduca);
            this.browser.get(this.fields.defecto).check();
        });
        this.browser.selectFirstItem(this.fields.comboLineasFacturacion);
    }

    clearTest(){
        this.browser.get('.x-grid-cell').contains(tiposTarifa.nombre).first().click();
        this.getEsborrarBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }

    getGridRow(){
        this.browser.get('.x-grid-cell').contains(tiposTarifa.nombre).first().click();
    }

    getBottomTab(){
        return this.browser.get('.x-tab').contains("Enviaments");
    }
}

export default TiposTarifaPage