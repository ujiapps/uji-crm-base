class EnviosPage {

    fieldsMailEnvioForm = {
        nombre: 'input[name=nombre]',
        asunto: 'input[name=asunto]',
        tipoCorreoEnvio: 'input[name=tipoCorreoEnvio]'
    }

    fieldsBuscadorEnvioForm = {
        busqueda: 'input[name=busqueda]'
    }

    fieldsComboPersonas = {
        comboPersonas: 'input[name=comboPersonas]'
    }

    fieldsUjiLookUpCombo = {
        query: 'input[name=query]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hastag = '#envios';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem() {
        return this.browser.contains("Enviaments");
    }

    getPanelEnvios() {
        return this.browser.get('[id^=panelEnvios]');
    }

    getMailEnvioForm() {
        return this.browser.get('[id^=mailEnvioForm]');
    }

    getBuscadorEnvioForm() {
        return this.browser.get('[id^=buscadorEnvioForm]');
    }

    getMailClientesGrid() {
        return this.browser.get('[id^=mailClientesGrid]');
    }

    getNuevoMail() {
        return this.getPanelEnvios().getButton('Nou mail');
    }

    getBtnCercarEnviament() {
        return this.getPanelEnvios().getButton('Cercar enviaments');
    }

    getNombre() {
        return this.getMailEnvioForm().get(this.fieldsMailEnvioForm.nombre);
    }

    getAsunto() {
        return this.getMailEnvioForm().get(this.fieldsMailEnvioForm.asunto);
    }

    selectComboTipoEnvio() {
        this.getMailEnvioForm().selectFirstItem(this.fieldsMailEnvioForm.tipoCorreoEnvio);
    }

    getDesarBtn() {
        return this.getMailEnvioForm().getButton('Desar');
    }

    getDesarYEnviarBtn() {
        return this.getMailEnvioForm().getButton('Desar i enviar');
    }

    getEsborrarBtn() {
        return this.getMailEnvioForm().getButton('Esborrar');
    }

    getDuplicatBtn() {
        return this.getMailEnvioForm().getButton('Duplicar');
    }

    getBusqueda() {
        return this.getBuscadorEnvioForm().get(this.fieldsBuscadorEnvioForm.busqueda);
    }

    getBtnCercar() {
        return this.getBuscadorEnvioForm().getButton('Cercar');
    }

    selectRowBuscadorEnvio(elem) {
        return this.getBuscadorEnvioForm().get('.x-grid-cell').contains(elem);
    }

    selectRowMailClientesGrid(elem) {
        return this.getMailClientesGrid().get('.x-grid-cell').contains(elem);
    }

    getEliminarBtnFromTabClients() {
        return this.getMailClientesGrid().getButton('Eliminar de l\'enviament');
    }

    getTabClientes() {
        return this.getPanelEnvios().get('.x-tab-bar').contains('Clients');
    }

    getTabEnviaments() {
        return this.getPanelEnvios().get('.x-tab-bar').contains('Enviament');
    }

    getMailClientesGrid() {
        return this.browser.get('[id^=mailClientesGrid]');
    }

    getAfegirClientBtn() {
        return this.getMailClientesGrid().getButton('Afegir');
    }

    getAddClienteWindow() {
        return this.browser.get('[id^=addClienteWindow]');
    }

    selectComboAddClienteWindow() {
        return this.getAddClienteWindow().get(this.fieldsComboPersonas.comboPersonas);
    }

    getBtnAfegirFromUjiLookUpCombo() {
        return this.getAddClienteWindow().getButton('Afegir');
    }

    getUjiLookUpCombo() {
        return this.browser.get('[id^=ujilookupWindow]');
    }

    selectRowUjiLookUpCombo(elem) {
        return this.getUjiLookUpCombo().find('.x-grid-cell').contains(elem);
    }

    getQueryFromUjiLookUpCombo() {
        return this.getUjiLookUpCombo().get(this.fieldsUjiLookUpCombo.query);
    }

    getBtnCercarUjiLoopUpCombo() {
        return this.getUjiLookUpCombo().getButton('Cerca');
    }

    getBtnSeleccionarUjiLoopUpCombo() {
        return this.getUjiLookUpCombo().getButton('Seleccionar');
    }

    writeOnBody() {
        let cy = this.getMailEnvioForm();
        cy.get('iframe').then(elem => {
            const body = elem.contents().find('body');

            let stripe = cy.wrap(body);
            stripe.eq(0).type("Texto CypressTest Generado");
        });
    }
}

export default EnviosPage