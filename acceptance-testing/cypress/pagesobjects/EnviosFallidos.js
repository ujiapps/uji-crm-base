class EnviosFallidosPage {
    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#envios-fallidos';
    }

    getTopToolbar() {
        return this.getListadoGrid().find('.x-toolbar');
    }
    getListadoGrid() {
        return this.browser.get('[id^=enviosFallidosGrid]')
    }

    getCercarBtn() {
        return this.getListadoGrid().find('.x-btn').contains('Cercar');
    }

    getNetejarBtn() {
        return this.getListadoGrid().find('.x-btn').contains('Netejar');
    }

    getMenuItem() {
        let upper = this.browser.contains("Llistats predefinits");
        upper.click();
        return this.browser.contains("Enviaments fallits");
    }

    getCercarInput() {
        return this.getTopToolbar().find('[id$=inputEl]').first();
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }
}

export default EnviosFallidosPage