class VinculacionesPage {

    fields = {
        nombre: 'input[name=nombre]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#tipos-vinculaciones';
    }

    getMenuItem() {
        this.browser.contains("Altres gestions").click();
        return this.browser.contains("Tipus vinculacions");
    }

    getTiposVinculacionesGrid() {
        return this.browser.get('[id^=tiposVinculacionesGrid]');
    }

    getAfegirVinculacionBtn() {
        return this.getTiposVinculacionesGrid().getButton('Afegir');
    }

    getEsborrarVinculacionBtn() {
        return this.getTiposVinculacionesGrid().getButton('Esborrar');
    }

    getFilaTiposVinculacionesGrid() {
        return this.browser.get('.x-grid-cell').contains("vinculacion nueva").first();
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    fillEditorFields() {
        this.getTiposVinculacionesGrid().getEditor().within(() => {
            this.browser.get(this.fields.nombre).type("vinculacion nueva");
        });
    }

    modifyEditorFields() {
        this.getTiposVinculacionesGrid().getEditor().within(() => {
            this.browser.get(this.fields.nombre).clear().type("vinculacion modificada");
        });
    }

    borrarRegistro() {
        this.browser.get('.x-grid-cell').contains("vinculacion modificada").first().click();
        this.getEsborrarVinculacionBtn().click();
        this.browser.messageBoxAcceptBtn().click();
    }
}

export default VinculacionesPage