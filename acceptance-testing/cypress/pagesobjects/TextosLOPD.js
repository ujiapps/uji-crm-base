class TextosLOPDPage {

    fields = {
        nombre: 'input[name=nombre]',
        textoCa: 'input[name=textoCa]',

        codigo: 'input[name=codigo]',
        textoEs: 'input[name=textoEs]',
        textoUk: 'input[name=textoUk]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#textos-LOPD';
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getMenuItem() {
        let upper = this.browser.contains("Altres gestions");
        upper.click();
        return this.browser.contains("Textes LOPD");
    }

    getTextosLOPDGrid() {
        return this.browser.get('[id^=textosLOPDGrid]');
    }

    getTextosLOPDWindow() {
        return this.browser.get('[id^=textosLOPDWindow]');
    }

    getAfegirTextosLOPDBtn() {
        return this.getTextosLOPDGrid().getButton('Afegir');
    }

    getEsborrarTextosLOPDBtn() {
        return this.getTextosLOPDGrid().getButton('Esborrar');
    }

    getNombreTextField() {
        this.getTextosLOPDWindow().find(this.fields.nombre).type("Textos LOPD");
    }

    getCodigoTextField() {
        this.getTextosLOPDWindow().find(this.fields.codigo).type("3");
    }

    getTextosIdiomasArea() {
        let cy = this.getTextosLOPDWindow();

        cy.get('.x-tab-bar').contains("Text Valencià").as('tabVal');
        cy.get('@tabVal').get('iframe').then(elem => {
            const body = elem.contents().find('body');

            let stripe = cy.wrap(body);
            stripe.eq(0).type("Text CypressTest en Valencià");
        });

        cy.get('.x-tab-bar').contains("Text Castellà").as('tabEsp');
        cy.get('@tabEsp').click();
        cy.get('@tabEsp').get('iframe').then(elem => {
            const body = elem.contents().find('body');

            let stripeEs = cy.wrap(body);
            stripeEs.eq(1).type("Texto CypressTest en Castellano");
        });

        cy.get('.x-tab-bar').contains("Text Anglès").as('tabUk');
        cy.get('@tabUk').click();
        cy.get('iframe').then(elem => {
            const body = elem.contents().find('body');

            let stripe = cy.wrap(body);
            stripe.eq(2).type("English text on a CypressTest");
        });
    }

    editTextosLOPDTextAreas() {
        this.getTextosLOPDWindow().find(this.fields.nombre).clear().type("EDIT Textos LOPD");
        let cy = this.getTextosLOPDWindow();

        cy.get('.x-tab-bar').contains("Text Valencià").as('tabVal');
        cy.get('@tabVal').get('iframe').then(elem => {
            const body = elem.contents().find('body');

            let stripe = cy.wrap(body);
            stripe.eq(0).clear().type("EDIT Text CypressTest en Valencià");
        });

        cy.get('.x-tab-bar').contains("Text Castellà").as('tabEsp');
        cy.get('@tabEsp').click();
        cy.get('@tabEsp').get('iframe').then(elem => {
            const body = elem.contents().find('body');

            let stripeEs = cy.wrap(body);
            stripeEs.eq(1).clear().type("EDIT Texto CypressTest en Castellano");
        });

        cy.get('.x-tab-bar').contains("Text Anglès").as('tabUk');
        cy.get('@tabUk').click();
        cy.get('iframe').then(elem => {
            const body = elem.contents().find('body');

            let stripe = cy.wrap(body);
            stripe.eq(2).clear().type("EDIT English text on a CypressTest");
        });

    }

    desarButton() {
        return this.getTextosLOPDWindow().getButton('Desar');
    }

    deleteTextosLOPD() {
        this.browser.get('.x-grid-cell').contains("EDIT Text CypressTest en Valencià").click();
        this.getEsborrarTextosLOPDBtn().click();
        return this.browser.messageBoxAcceptBtn();
    }

    selectRowTextosLOPD() {
        return this.browser.get('.x-grid-cell').contains("Text CypressTest en Valencià");
    }
}

export default TextosLOPDPage