import TiposTarifaPage from "../../pagesobjects/gestion-recibos/TiposTarifa";
import EnvioTipoTarifaPage from "../../pagesobjects/gestion-recibos/EnvioTipoTarifa";

describe('Testing envíos de tipos de tarifa', () => {

    let tipoTarifaPage;
    let enviosPage;

    beforeEach(() => {
        tipoTarifaPage = new TiposTarifaPage(cy);
        enviosPage = new EnvioTipoTarifaPage(cy);

        cy.intercept('GET','/crm/rest/tipotarifa/campanya/*').as('loadTipoTarifas');
        cy.intercept('POST', '/crm/rest/tipotarifa/*').as('addTipoTarifa');

        cy.intercept('GET', 'crm/rest/tipotarifaenvio/*').as('loadEnvios');
        cy.intercept('POST', 'crm/rest/tipotarifaenvio/*').as('addEnvio');

        cy.intercept('GET','crm/rest/tipotarifaenvioprogramado/*').as('loadProgramacionesEnvios');
        cy.intercept('POST', 'crm/rest/tipotarifaenvioprogramado/*').as('addProgramacionEnvio');

        tipoTarifaPage.visitSection();
        cy.wait('@loadTipoTarifas');
    });

    it('Definir un envío', () =>  {
        //Seleccionar campanya y anyadir un tipo de tarifa
        tipoTarifaPage.getAfegirTipoTarifaBtn().click();
        cy.haveInvalidFields(1);
        tipoTarifaPage.fillEditorFields();
        cy.haveInvalidFields(0);
        cy.editorAcceptBtn().click();
        cy.wait('@addTipoTarifa').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });
        //=================================================
        enviosPage.getEnviosTab().click();
        enviosPage.getAfegirEnvioBtn().click();
        cy.haveInvalidFields(4);
        enviosPage.fillEditorFields();
        cy.haveInvalidFields(0);
        enviosPage.getEnviosGrid().find('a.x-btn.x-row-editor-update-button').click();
        cy.wait('@addEnvio').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });
    });

    it('Definir programación de un envío', () => {
        tipoTarifaPage.getGridRow();
        enviosPage.getEnviosTab().click();
        enviosPage.getEnvioGridRow();
        enviosPage.getProgramacionEnviosBtn().click();
        enviosPage.getAfegirProgramacionEnviosBtn().click();
        cy.wait('@loadProgramacionesEnvios');

        cy.haveInvalidFields(3);
        enviosPage.fillProgramacionEnviosFields();
        cy.haveInvalidFields(0);
        enviosPage.getProgramacionEnviosGrid().find('a.x-btn.x-row-editor-update-button').click();
        cy.wait('@addProgramacionEnvio').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });

    });

    it('Borrar programación de un envío', () => {
        tipoTarifaPage.getGridRow();
        enviosPage.getEnviosTab().click();
        enviosPage.getEnvioGridRow();
        enviosPage.getProgramacionEnviosBtn().click();
        cy.wait('@loadProgramacionesEnvios');

        enviosPage.borrarProgramacionEnvio();
        cy.wait('@loadProgramacionesEnvios')
    });

    it('Borrar un envío', () => {
        tipoTarifaPage.getGridRow();
        enviosPage.getEnviosTab().click();
        enviosPage.clearTest();
        cy.wait('@loadEnvios');
        //=================================================
        //Borrar el tipo de tarifa
        tipoTarifaPage.clearTest();
    });
})