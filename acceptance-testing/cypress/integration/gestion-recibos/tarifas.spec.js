import TarifasPage from "../../pagesobjects/gestion-recibos/Tarifas";
import TiposTarifaPage from "../../pagesobjects/gestion-recibos/TiposTarifa";

describe('Testing Tarifas de campanya', () => {

    let tipoTarifaPage;
    let tarifaPage;

    beforeEach(() => {
        tipoTarifaPage = new TiposTarifaPage(cy);
        tarifaPage = new TarifasPage(cy);

        cy.intercept('GET','/crm/rest/tipotarifa/campanya/*').as('loadTipoTarifas');
        cy.intercept('POST', '/crm/rest/tipotarifa/*').as('addTipoTarifa');

        cy.intercept('GET', 'crm/rest/tarifa/*').as('loadTarifas');
        cy.intercept('POST', 'crm/rest/tarifa/*').as('addTarifa');

        tipoTarifaPage.visitSection();
        cy.wait('@loadTipoTarifas');
    });

    it('Definir una tarifa', () => {
        //Seleccionar campanya y anyadir un tipo de tarifa
        tipoTarifaPage.getAfegirTipoTarifaBtn().click();
        cy.haveInvalidFields(1);
        tipoTarifaPage.fillEditorFields();
        cy.haveInvalidFields(0);
        cy.editorAcceptBtn().click();
        cy.wait('@addTipoTarifa').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });
        //=================================================
        tarifaPage.getAfegirTarifaBtn().click();
        cy.haveInvalidFields(2);
        tarifaPage.fillEditorFields();
        cy.haveInvalidFields(0);
        tarifaPage.getTarifasGrid().find('a.x-btn.x-row-editor-update-button').click();
        //cy.editorAcceptBtn().click();
        cy.wait('@addTarifa').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });
    });

    it('Borrar una tarifa', () => {
        tipoTarifaPage.getGridRow();
        cy.wait('@loadTarifas');
        tarifaPage.clearTest();

        //=================================================
        //Borrar el tipo de tarifa
        tipoTarifaPage.clearTest();
    })
})