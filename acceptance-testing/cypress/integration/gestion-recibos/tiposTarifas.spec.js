import TiposTarifaPage from "../../pagesobjects/gestion-recibos/TiposTarifa";

describe('Testing gestion de tipos tarifa', () => {

    let page

    beforeEach(() => {
        page = new TiposTarifaPage(cy);

        cy.intercept('GET','/crm/rest/tipotarifa/campanya/*').as('loadTipoTarifas');
        cy.intercept('POST', '/crm/rest/tipotarifa/*').as('addTipoTarifa');
        cy.intercept('GET', 'crm/rest/tarifa/*').as('loadTarifas');
        cy.intercept('GET', 'crm/rest/tipotarifaenvio/*').as('loadEnvios');

        page.visitSection();
        cy.wait('@loadTipoTarifas');
    })

    it('Definir un tipo de tarifa', () => {
        page.getAfegirTipoTarifaBtn().click();
        cy.haveInvalidFields(1);
        page.fillEditorFields();
        cy.haveInvalidFields(0);
        cy.editorAcceptBtn().click();
        cy.wait('@addTipoTarifa').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });
    });

    it('Comprobar tabs de tarifas y envíos', () => {
        page.getGridRow();
        cy.wait('@loadTarifas').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });
        cy.wait('@loadEnvios').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });
    });

    it('Borrar un tipo de tarifa', () => {
        page.clearTest();
    });


})