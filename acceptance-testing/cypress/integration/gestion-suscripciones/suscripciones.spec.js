import SuscripcionesPage from "../../pagesobjects/gestion-suscripciones/Suscripciones";

describe('Testing gestión de suscripciones', () => {

    let page

    beforeEach(() => {
        page = new SuscripcionesPage(cy);

        cy.intercept('GET', '/crm/rest/grupo/*').as('loadSuscriptores');
        cy.intercept('POST', '/crm/rest/grupo').as('addSuscriptor');
        cy.intercept('GET', '/crm/rest/item/*').as('loadElementos');
        cy.intercept('GET', '/crm/rest/programa/admin/*').as('programadmin');
        cy.intercept('GET', '/crm/rest/tipoacceso/*').as('tipoaccesso');
        cy.intercept('DELETE', '/crm/rest/grupo/*').as('deletegrupo');

        page.visitSectionSuscr();
        cy.waitRequest('@loadSuscriptores', 200);
    })

    it('Definir un suscriptor', () => {
        page.getAfegirGrupoBtn().click();
        cy.waitRequest('@tipoaccesso', 200);
        cy.waitRequest('@programadmin', 200);
        page.fillFormFields();
        page.getActualizarWindowBtn().click();
        cy.waitRequest('@addSuscriptor', 200);
        cy.waitRequest('@loadSuscriptores', 200);
    });

    it('Borrar un suscriptor', () => {
        page.clearTest();
        cy.waitRequest('@deletegrupo', 204);
        cy.waitRequest('@loadSuscriptores', 200);
    });
})