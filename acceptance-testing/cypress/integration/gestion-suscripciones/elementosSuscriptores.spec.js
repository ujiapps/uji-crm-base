import SuscripcionesPage from "../../pagesobjects/gestion-suscripciones/Suscripciones";
import ElementosSuscriptoresPage from "../../pagesobjects/gestion-suscripciones/ElementosSuscriptores";

describe('Testing de elementos de suscriptores', () => {
    let grupo;
    let elemento;

    beforeEach(() => {
        grupo = new SuscripcionesPage(cy);
        elemento = new ElementosSuscriptoresPage(cy);

        cy.intercept('GET', '/crm/rest/grupo/*').as('loadSuscriptores');

        cy.intercept('GET', '/crm/rest/item/*').as('loadElementos');
        cy.intercept('POST', '/crm/rest/item').as('addElemento');

        cy.intercept('GET', '/crm/rest/itemitem/item/origen/*').as('loadItemsOrigen');
        cy.intercept('POST', '/crm/rest/itemitem').as('addItemOrigen');

        cy.intercept('GET', '/crm/rest/tipoacceso/*').as('tipoacceso');
        cy.intercept('GET', '/crm/rest/programa/admin/*').as('admin');
        cy.intercept('GET', '/crm/rest/item/*').as('item');

        cy.intercept('DELETE', '/crm/rest/itemitem/*').as('deleteitem');
        cy.intercept('DELETE', '/crm/rest/item/*').as('deleteitemsus');
        cy.intercept('DELETE', '/crm/rest/grupo/*').as('deletegrupo');
        grupo.visitSection();
    });

    it('Definir un elemento del suscriptor', () => {
        //Crear suscriptor
        cy.waitRequest('@loadSuscriptores', 200);
        grupo.getAfegirGrupoBtn().click();
        cy.waitRequest('@tipoacceso', 200);
        cy.waitRequest('@admin', 200);
        grupo.fillFormFields();
        grupo.getActualizarWindowBtn().click();
        //=====================================
        cy.waitRequest('@loadSuscriptores', 200);
        grupo.getGridRow();
        cy.waitRequest('@loadElementos', 200);
        elemento.getAfegirElementoBtn().click();
        cy.waitRequest('@loadSuscriptores', 200);
        elemento.fillFormFields();
        elemento.getActualizarWindowBtn().click();
        cy.waitRequest('@addElemento', 200);
        cy.waitRequest('@item', 200);
    });

    it('Definir un item origen', () => {
        cy.waitRequest('@loadSuscriptores', 200);
        grupo.getGridRow();
        cy.waitRequest('@loadElementos', 200);
        elemento.getGridRow();
        elemento.getItemsOrigenBtn().click();
        cy.waitRequest('@loadItemsOrigen', 200);

        elemento.getAfegirItemOrigenBtn().click();
        cy.waitRequest('@loadSuscriptores', 200);
        elemento.seleccionarComboItemOrigen();
        cy.waitRequest('@loadElementos', 200);
        elemento.anyadirItemOrigen();
        cy.waitRequest('@addItemOrigen', 200);
        cy.waitRequest('@loadItemsOrigen', 200);
    });

    it('Borrar un item origen', () => {
        cy.waitRequest('@loadSuscriptores', 200);
        grupo.getGridRow();
        cy.waitRequest('@loadElementos', 200);

        elemento.getGridRow();
        elemento.getItemsOrigenBtn().click();
        cy.waitRequest('@loadItemsOrigen', 200);

        elemento.borrarItemOrigen();
        cy.waitRequest('@deleteitem', 204);
        cy.waitRequest('@loadItemsOrigen', 200);
    })

    it('Borrar un elemento del suscriptor', () => {
        cy.waitRequest('@loadSuscriptores', 200);
        grupo.getGridRow();
        cy.waitRequest('@loadElementos', 200);
        elemento.eliminarItem();
        cy.waitRequest('@deleteitemsus', 204);
        //=====================================
        //Eliminar suscriptor
        grupo.clearTest();
        cy.waitRequest('@deletegrupo', 204);
        cy.waitRequest('@loadSuscriptores', 200);
    })
})