import EnviosPage from "../../pagesobjects/Envios";

describe.skip('Testing Enviaments', () => {

    let page;

    beforeEach(() => {
        page = new EnviosPage(cy);

        cy.intercept('GET', '/crm/app.json*').as('json');
        cy.intercept('GET', '/crm/rest/navigation/*').as('nav');
        cy.intercept('GET', '/crm/rest/userinfo*').as('user');
        cy.intercept('GET', '/crm/rest/programa/*').as('programa');
        cy.intercept('GET', '/crm/rest/enviocliente/envio/*').as('loadClientes');
        cy.intercept('GET', '/crm/rest/tipo/envio/*').as('loadEnvio');
        cy.intercept('GET', '/crm/rest/envio/*').as('loadFiltrosEnvio');
        cy.intercept('GET', '/crm/rest/envio/*/adjunto*').as('adjunto');
        cy.intercept('GET', '/crm/rest/envioimagen/*').as('envioimagen');

        cy.intercept('PUT', '/crm/rest/envio/*/enviar/0').as('addEnvio');
        cy.intercept('PUT', '/crm/rest/envio/*/enviar/1').as('addAndSendEnvio');

        cy.intercept('POST', '/crm/rest/enviocriterio/envio/cliente/').as('addCliente');
        cy.intercept('POST', '/crm/rest/envio/*/duplicar').as('postEnvio');
        cy.intercept('POST', '/crm/rest/envio/').as('postMail');

        cy.intercept('DELETE', '/crm/rest/enviocriterio/envio/cliente/').as('deleteCliente');
        cy.intercept('DELETE', '/crm/rest/envio/*').as('deleteEnvio');

        page.visitSection();
        cy.waitRequest('@json', 200);
        cy.waitRequest('@nav', 200);
        cy.waitRequest('@user', 200);
        cy.waitRequest('@programa', 200);
        cy.waitRequest('@loadEnvio', 200);
        cy.waitRequest('@programa', 200);
    });

    it('Nuevo envio', () => {
        page.getNuevoMail().click();
       /* Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        });*/

        cy.waitRequest('@postMail', 200);
        cy.waitRequest('@adjunto', 200);
        cy.waitRequest('@envioimagen', 200);

        page.getNombre().clear().type('Cypress Test Nombre Envío');
        page.getAsunto().type('Cypress Test AsuntoEnvío');
        page.selectComboTipoEnvio();
        page.getDesarBtn().click();
        cy.uniqueMessageBoxAcceptBtn().click();
        page.writeOnBody();
        page.getDesarBtn().click();
        cy.uniqueMessageBoxAcceptBtn().click();

        cy.waitRequest('@addEnvio', 200);

        page.getTabClientes().click();
        page.getAfegirClientBtn().click();
        page.selectComboAddClienteWindow().click();
        page.getQueryFromUjiLookUpCombo().type('Iratxe Lozano');
        page.getBtnCercarUjiLoopUpCombo().click();
        page.selectRowUjiLookUpCombo('Iratxe Lozano').click();
        page.getBtnSeleccionarUjiLoopUpCombo().click();
        page.getBtnAfegirFromUjiLookUpCombo().click();

        cy.waitRequest('@addCliente', 204);
        cy.waitRequest('@loadClientes', 200);

        page.getTabEnviaments().click()
        page.getDesarYEnviarBtn().click();

        cy.waitRequest('@addAndSendEnvio', 200);

        cy.uniqueMessageBoxAcceptBtn().click();
    });

    it('Borrar un cliente de un Envio', () => {
        page.getBtnCercarEnviament().click();

        cy.waitRequest('@loadEnvio', 200);

        page.getBusqueda().type('Cypress Test Nombre Envío');
        page.getBtnCercar().click();

        cy.waitRequest('@loadFiltrosEnvio', 200);

        page.selectRowBuscadorEnvio('Cypress Test Nombre Envío').dblclick();
        page.getTabClientes().click();

        cy.waitRequest('@loadClientes', 200);

        page.selectRowMailClientesGrid('Iratxe').click();
        page.getEliminarBtnFromTabClients().click();
        cy.messageBoxAcceptBtn().click();

        cy.waitRequest('@deleteCliente', 204);
        cy.waitRequest('@loadClientes', 200);
    });

    it('Duplicar envio', () => {
        page.getBtnCercarEnviament().click();

        cy.waitRequest('@loadEnvio', 200);

        page.getBusqueda().type('Cypress Test Nombre Envío');
        page.getBtnCercar().click();

        cy.waitRequest('@loadFiltrosEnvio', 200);

        page.selectRowBuscadorEnvio('Cypress Test Nombre Envío').dblclick();
        page.getDuplicatBtn().click();

        cy.waitRequest('@postEnvio', 200);

        cy.uniqueMessageBoxAcceptBtn().click();
    })

    it('Borrar Envio y duplicado', () => {
        page.getBtnCercarEnviament().click();

        cy.waitRequest('@loadEnvio', 200);

        page.getBusqueda().type('Cypress Test Nombre Envío - DUPLICAT');
        page.getBtnCercar().click();

        cy.waitRequest('@loadFiltrosEnvio', 200);

        page.selectRowBuscadorEnvio('Cypress Test Nombre Envío - DUPLICAT').dblclick();
        page.getEsborrarBtn().click();
        cy.messageBoxAcceptBtn().click();

        cy.waitRequest('@deleteEnvio', 204);

        page.getBtnCercarEnviament().click();

        cy.waitRequest('@loadEnvio', 200);

        page.getBusqueda().type('Cypress Test Nombre Envío');
        page.getBtnCercar().click();

        cy.waitRequest('@loadFiltrosEnvio', 200);

        page.selectRowBuscadorEnvio('Cypress Test Nombre Envío').dblclick();
        page.getEsborrarBtn().click();
        cy.messageBoxAcceptBtn().click();

        cy.waitRequest('@deleteEnvio', 204);
    });
})
