import TipoDatosPage from '../../pagesobjects/TipoDatos';

describe('Testing altres dades: tipo datos', () => {

    let page;

    beforeEach(() => {
        page = new TipoDatosPage(cy);

        cy.intercept('GET', '/crm/rest/clientedatotipo/*').as('loadDatos');
        cy.intercept('POST', '/crm/rest/clientedatotipo/*').as('addDato');
        cy.intercept('GET', '/crm/rest/tipo/tipodatos/*').as('loadTipos');
        cy.intercept('GET', '/crm/rest/clientedatoopcion/*').as('loadopciones');
        cy.intercept('POST', '/crm/rest/clientedatoopcion/*').as('addOpciones');
        page.visitSection();
        cy.wait('@loadDatos');
    });

    it('Definir un tipo de dato', () => {


        page.getAfegirTipoDatoBtn().click();
        cy.haveInvalidFields(2);
        page.fillEditorTipoDatoGridFields();
        cy.haveInvalidFields(0);
        cy.waitRequest('@addDato', 200);
    });

    it('Insertar items', () => {
        page.selectRow();
        page.getAfegirItemsBtn().click();
        cy.waitRequest('@loadopciones', 200);

        page.getItemAddBtn().click();
        cy.haveInvalidFields(1);
        page.fillEditorItemGridFields();
        cy.waitRequest('@loadopciones', 200);

        page.clearTestItemGrid();
        cy.waitRequest('@loadopciones', 200);
        page.getItemCancelarBtn().click();
    });


    it('Borrar dato', () => {
        page.clearTestTipoDatoGrid();
        cy.wait('@loadDatos');
    });
});