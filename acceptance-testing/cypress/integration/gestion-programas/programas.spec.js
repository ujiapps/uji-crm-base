import GestionProgramasPage from '../../pagesobjects/GestionProgramas';

describe('Testing gestion de programas', () => {

    let page

    beforeEach(() => {
        page = new GestionProgramasPage(cy);
        cy.intercept('GET', '/crm/rest/programa/*').as('loadProgramas');
        cy.intercept('POST', '/crm/rest/programa/*').as('addPrograma');
        page.visitSection();
        cy.wait('@loadProgramas');
    })

    it('Definir un programa', () => {
        page.getAfegirProgramaBtn().click();
        cy.haveInvalidFields(3);
        page.fillEditorFields();
        cy.haveInvalidFields(0);
        cy.editorAcceptBtn().click();
        cy.wait('@addPrograma').should(({ response }) => {
            expect(response.statusCode).to.eq(200);
        });

    });

    it('Borrar programas', () => {
        page.clearTest();
        cy.wait('@loadProgramas');
    });

});