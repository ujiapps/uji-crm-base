import EtiquetasPage from "../../../pagesobjects/clientes/etiquetas/Etiquetas";

describe('Testing gestion de etiquetas ( Cliente )', () => {

    let page;

    beforeEach(() => {
        page = new EtiquetasPage(cy);

        cy.intercept('GET', '/crm/rest/clientegrid/*').as('loadClienteGrid');
        cy.intercept('GET', '/crm/rest/cliente/*').as('loadCliente');
        cy.intercept('GET', '/crm/rest/clienteetiqueta/*').as('loadEtiqueta');
        page.visitSection();

        page.getCercarBtnPanelClientes().click();
        page.buscarNombreCypress();
        page.getCercarBtnBuscadorClientesWindow().click();

        cy.wait('@loadClienteGrid').its('response.statusCode').should('eq',200);
        cy.wait(600);
        page.seleccionarFilaClientesGrid();

        cy.wait('@loadCliente').its('response.statusCode').should('eq',200);
        cy.wait(600);

        page.getEtiquetasTab();
    });

    it('Añadir etiquetas existente a un cliente', () => {
        cy.intercept('POST', '/crm/rest/clienteetiqueta/*').as('addEtiqueta');

        page.typeEtiqueta('Erasmus');
        page.getAfegirButton().click();
        cy.wait('@addEtiqueta').its('response.statusCode').should('eq',200);
        cy.wait('@loadEtiqueta').its('response.statusCode').should('eq',200);
    });

    it('Añadir etiquetas inexistentes  un cliente', () => {
        cy.intercept('POST', '/crm/rest/clienteetiqueta/*').as('addEtiqueta');

        page.typeUnexpectedEtiqueta('Etiqueta Cypress Test');
        page.getAfegirButton().click();
        cy.wait('@addEtiqueta').its('response.statusCode').should('eq',200);
        cy.wait('@loadEtiqueta').its('response.statusCode').should('eq',200);
    });

    it('Eliminar etiquetas de un cliente', () => {
        cy.intercept('DELETE', '/crm/rest/clienteetiqueta/*').as('deleteEtiqueta');

        page.selectRowFromClienteEtiquetasGrid('Erasmus');
        page.getEsborrarButton().click();
        cy.messageBoxAcceptBtn().click();
        cy.wait('@deleteEtiqueta').its('response.statusCode').should('eq',204);

        cy.wait(600);

        page.selectRowFromClienteEtiquetasGrid('Etiqueta Cypress Test');
        page.getEsborrarButton().click();
        cy.messageBoxAcceptBtn().click();
        cy.wait('@deleteEtiqueta').its('response.statusCode').should('eq',204);
    });
});