import BuscarPage from "../../../pagesobjects/clientes/buscador/Buscar";
import CampanyaClientePage from "../../../pagesobjects/clientes/campanyas/CampanyaCliente";

describe('Testing Campanyas Cliente', () => {

    let buscar;
    let campanya;

    beforeEach(() => {
        buscar = new BuscarPage(cy);
        campanya = new CampanyaClientePage(cy);
        cy.intercept('GET', '/crm/rest/clientegrid/*').as('loadClienteGrid');
        cy.intercept('GET', '/crm/rest/cliente/*').as('loadCliente');

        cy.intercept('GET', '/crm/rest/campanyacliente/cliente/').as('loadCampanyas');
        cy.intercept('POST', '/crm/rest/campanyacliente/').as('addCampanya');
        cy.intercept('GET', '/crm/rest/campanya/user/*').as('loadComboCampanya');

        campanya.visitSection();
    });

    it('Añadir y eliminar una campanya', () => {
        //Buscar cliente
        buscar.getCercarBtnPanelClientes().click();
        buscar.buscarNombreCypress();
        buscar.getCercarBtnBuscadorClientesWindow().click();
        cy.wait('@loadClienteGrid').its('response.statusCode').should('eq',200);
        buscar.seleccionarFilaClientesGrid();
        cy.wait('@loadCliente');

        //Crear campanya
        campanya.selectCampanyasTab();
        campanya.getAfegirBtn().click();
        cy.wait('@loadComboCampanya').its('response.statusCode').should('eq',200);
        campanya.selectCombos();
        cy.wait('@addCampanya').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });

        //Borrar campanya
        cy.wait(500);
        campanya.borrarCampanya();
    })
})