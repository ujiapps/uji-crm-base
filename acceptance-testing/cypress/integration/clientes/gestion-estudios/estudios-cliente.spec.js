import EstudiosClientePage from "../../../pagesobjects/clientes/gestion-estudios/EstudiosCliente";

describe('Testing Gestion Estudios (Cliente)', () => {

    let page;

     beforeEach(() => {
         page = new EstudiosClientePage(cy);
         page.visitSection();

         cy.intercept('GET', '/crm/rest/clientegrid/*').as('loadClienteGrid');
         cy.intercept('GET', '/crm/rest/cliente/*').as('loadCliente');
         cy.intercept('GET', '/crm/rest/estudionouji/*').as('loadEstudiUJI');
     });

     it('Añadir estudios no UJI a cliente', () => {
         cy.intercept('POST', '/crm/rest/estudionouji/*').as('addEstudiUJI');

         page.getCercarBtnPanelClientes().click();
         page.buscarNombreCypress();
         page.getCercarBtnBuscadorClientesWindow().click();

         cy.wait('@loadClienteGrid').its('response.statusCode').should('eq',200);
         cy.wait(600);
         page.seleccionarFilaClientesGrid();

         cy.wait('@loadCliente').its('response.statusCode').should('eq',200);
         cy.wait(600);
         page.getEstudisPanel();
         cy.wait(600);
         page.getAfegirBtn().click();
         page.fillEditorFields();

         cy.editorAcceptBtn().click();
         cy.wait('@addEstudiUJI').its('response.statusCode').should('eq',200);
         cy.wait('@loadEstudiUJI').its('response.statusCode').should('eq',200);
     });

     it('Borrar estudios de un cliente', () => {
         cy.intercept('DELETE', '/crm/rest/estudionouji/*').as('deleteEstudiUJI');

         page.getCercarBtnPanelClientes().click();
         page.buscarNombreCypress();
         page.getCercarBtnBuscadorClientesWindow().click();

         cy.wait('@loadClienteGrid').its('response.statusCode').should('eq',200);
         cy.wait(600);
         page.seleccionarFilaClientesGrid();

         cy.wait('@loadCliente').its('response.statusCode').should('eq',200);
         cy.wait(600);
         page.getEstudisPanel();
         cy.wait(600);

         page.selectRowEstudisUjiGrid()
         page.getEsborrarBtn().click()
         cy.messageBoxAcceptBtn().click();

         cy.wait('@deleteEstudiUJI').its('response.statusCode').should('eq',204);
         cy.wait('@loadEstudiUJI').its('response.statusCode').should('eq',200);
     });
})