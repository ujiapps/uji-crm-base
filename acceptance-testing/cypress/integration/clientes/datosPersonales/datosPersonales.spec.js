import DatosPersonalesPage from "../../../pagesobjects/clientes/datosPersonales/DatosPersonales";
import BuscarPage from "../../../pagesobjects/clientes/buscador/Buscar";

describe('Testing Datos Personales de Cliente', () => {

    let buscar;
    let datosPersonales;

    beforeEach(() => {
        buscar = new BuscarPage(cy);
        datosPersonales = new DatosPersonalesPage(cy);

        cy.intercept('GET', '/crm/rest/clientegrid/*').as('loadClienteGrid');
        cy.intercept('GET', '/crm/rest/cliente/*').as('loadCliente');
        cy.intercept('PUT', 'crm/rest/cliente/').as('actualizarCliente');
        datosPersonales.visitSection();
    });

    it('Actualizar datos personales de cliente', () => {
        //Buscar cliente
        buscar.getCercarBtnPanelClientes().click();
        buscar.buscarNombreCypress();
        buscar.getCercarBtnBuscadorClientesWindow().click();
        cy.wait('@loadClienteGrid').its('response.statusCode').should('eq',200);
        buscar.seleccionarFilaClientesGrid();
        cy.wait('@loadCliente');

        //Actualizar datos personales
        cy.wait(1000);
        datosPersonales.actualizarDatosPersonales(' ApellidoNuevo');
        cy.wait('@actualizarCliente').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });

        datosPersonales.actualizarDatosPersonales(' cypress apellidos');
        cy.wait('@actualizarCliente').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });

    });

})