import BuscarPage from "../../../pagesobjects/clientes/buscador/Buscar";
import OtrosDatosPage from "../../../pagesobjects/clientes/otrosDatos/otrosDatos";

describe('Testing Otros Datos Cliente', () => {

    let buscar;
    let otroDato;

    beforeEach(() => {
        buscar = new BuscarPage(cy);
        otroDato = new OtrosDatosPage(cy);
        cy.intercept('GET', '/crm/rest/clientegrid/*').as('loadClienteGrid');
        cy.intercept('GET', '/crm/rest/cliente/*').as('loadCliente');

        cy.intercept('GET', '/crm/rest/dato/*').as('loadDatoExtra');
        cy.intercept('POST', '/crm/rest/clientedato/').as('addDatoExtra');
        cy.intercept('DELETE', '/crm/rest/clientedato/*').as('deleteDatoExtra');

        cy.intercept('POST', '/crm/rest/clienteitem/').as('addItem');
        cy.intercept('DELETE', '/crm/rest/clienteitem/*').as('deleteItem');

        cy.intercept('GET', '/crm/rest/cliente/general/*').as('clienteGeneral');
        cy.intercept('GET', '/crm/rest/cliente/*/fotos*').as('clienteFotos');

        cy.intercept('GET', '/crm/rest/tipo/acceso/*').as('acceso');
        cy.intercept('GET', '/crm/rest/clientedatotipo/clientes/*').as('datotipo');

        cy.intercept('GET', '/crm/rest/programa/*').as('programa');

        cy.intercept('GET', '/crm/rest/grupo/*').as('grupo');
        cy.intercept('GET', '/crm/rest/item/*').as('item');
        cy.intercept('GET', '/crm/rest/clienteitem/*').as('clienteitem');
        cy.intercept('GET', '/crm/rest/dato/relacionados/*').as('relacionados');

        cy.intercept('PUT', '/crm/rest/clienteitem/*').as('clienteitemput');

        otroDato.visitSection();
    });

    it('Añadir y eliminar dato extra', () => {
        //Buscar cliente
        buscar.getCercarBtnPanelClientes().click();
        buscar.buscarNombreCypress();
        buscar.getCercarBtnBuscadorClientesWindow().click();
        cy.waitRequest('@loadClienteGrid', 200);
        buscar.seleccionarFilaClientesGrid();
        cy.waitRequest('@loadCliente', 200);

        //Crear dato extra
        otroDato.selectOtrosDatosTab();

        cy.waitRequest('@clienteGeneral', 200);
        cy.waitRequest('@clienteFotos', 200);
        cy.waitRequest('@loadDatoExtra', 200);

        otroDato.getAfegirBtn().click();
        otroDato.elegirDatoExtra();

        cy.waitRequest('@acceso', 200);
        cy.waitRequest('@datotipo', 200);

        otroDato.fillDatoExtraForm();
        otroDato.getAfegirIFinalitzarBtn().click();
        cy.waitRequest('@addDatoExtra', 200);
        cy.waitRequest('@loadDatoExtra', 200);

        //Borrar dato extra
        otroDato.eliminarDatoExtra();
        cy.uniqueMessageBoxAcceptBtn().click();
        cy.waitRequest('@deleteDatoExtra', 204);

        //Crear Item
        otroDato.getAfegirBtn().click();
        otroDato.elegirItem();

        cy.waitRequest('@programa', 200)

        otroDato.fillItemForm();
        otroDato.getAfegirIFinalitzarBtn().click();
        cy.waitRequest('@addItem', 200);
        cy.waitRequest('@loadDatoExtra', 200);

        //Anyadir Dato Relacionado a Item
        otroDato.selectItemParaModificar();
        cy.waitRequest('@clienteitem', 200);
        otroDato.anyadirDatoRelacionado();
        cy.waitRequest('@addDatoExtra', 200);
        cy.waitRequest('@relacionados', 200);

        //Borrar Dato Relacionado a Item
        otroDato.eliminarDatoRelacionado();
        cy.waitRequest('@deleteDatoExtra', 204);
        cy.uniqueMessageBoxAcceptBtn().click();
        cy.waitRequest('@relacionados', 200);
        otroDato.getModificarIFinalitzarBtn().click();
        cy.waitRequest('@clienteitemput', 200);

        //Borrar item
        otroDato.eliminarItem();
        cy.uniqueMessageBoxAcceptBtn().click();
        cy.waitRequest('@deleteItem', 204);
        cy.waitRequest('@loadDatoExtra', 200);
    })
})