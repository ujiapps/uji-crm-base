import VinculosPage from "../../../pagesobjects/clientes/gestion-vinculos/Vinculos";

describe('Testing Gestion de Vinculos (Cliente)', () => {

    let page;

    beforeEach(() => {
        page = new VinculosPage(cy);
        page.visitSection();

        cy.intercept('GET', '/crm/rest/clientegrid/*').as('loadClienteGrid');
        cy.intercept('GET', '/crm/rest/cliente/*').as('loadCliente');
        cy.intercept('GET', '/crm/rest/vinculacion/cliente/*').as('loadClienteVinc');

        page.getCercarBtnPanelClientes().click();
        page.buscarNombreCypress();
        page.getCercarBtnBuscadorClientesWindow().click();

        cy.wait('@loadClienteGrid').its('response.statusCode').should('eq',200);
        cy.wait(600);
        page.seleccionarFilaClientesGrid();

        cy.wait('@loadCliente').its('response.statusCode').should('eq',200);
        cy.wait(600);

        page.getVinculosTab();
    });

    it('Vincular clientes', () => {
        cy.intercept('POST', '/crm/rest/vinculacion/cliente/*').as('addClienteVinc');

        page.getAfegirBtn().click();

        page.getClientComboBox().click();

        cy.wait(600);

        page.getQueryFromUjiLookUpCombo().type('Iratxe Lozano');
        page.getBtnCercarUjiLoopUpCombo().click();
        page.selectRowUjiLookUpComboVinculos('Iratxe Lozano').click();
        page.getBtnSeleccionarUjiLoopUpCombo().click();

        page.selectVinculacion();
        page.getDesarBtnFromAfegirVinculacioWin().click()

        cy.wait('@addClienteVinc').its('response.statusCode').should('eq',200);
        cy.wait('@loadClienteVinc').its('response.statusCode').should('eq',200);
    });

    it('Eliminar clientes vinculados', () => {
        cy.intercept('DELETE', 'crm/rest/vinculacion/*').as('deleteClienteVinc');

        page.selectRowFromVinculosGrid('Iratxe Lozano').click();
        page.getEsborrarBtn().click();
        cy.messageBoxAcceptBtn().click();

        cy.wait('@deleteClienteVinc').its('response.statusCode').should('eq',204);
        cy.wait('@loadClienteVinc').its('response.statusCode').should('eq',200);
    });
})