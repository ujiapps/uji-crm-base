import SeguimientosClientePage from '../../../pagesobjects/clientes/seguimientos/SeguimientosCliente';

describe('Test seguimientos de cliente', () => {
    let page;

    beforeEach(() => {
        page = new SeguimientosClientePage(cy);
        cy.intercept('GET', '/crm/rest/seguimiento/*').as('loadClienteSeguimientoGrid');
        cy.intercept('GET', '/crm/rest/seguimientotipo/*').as('loadSeguimientoTipo');
        cy.intercept('GET', '/crm/rest/campanya/*').as('loadCampanya');

        cy.intercept('POST', '/crm/rest/seguimiento/').as('addSeguimientoCliente');
        cy.intercept('PUT', '/crm/rest/seguimiento/*').as('editSeguimientoCliente');
        cy.intercept('DELETE', '/crm/rest/seguimiento/*').as('deleteSeguimientoCliente');

        page.buscarCliente();
        page.seleccionarFilaClientesGrid();
        page.irPanelSeguimientos();

        cy.waitRequest('@loadSeguimientoTipo', 200);
        cy.waitRequest('@loadCampanya', 200);
        cy.waitRequest('@loadClienteSeguimientoGrid', 200);
    });

    it.skip('Buscar cliente existente', () => {
        page.buscarCliente();
        page.seleccionarFilaClientesGrid();
        page.irPanelSeguimientos();
    });

    it('POST seguimiento de cliente', () => {
        page.getAfegirBtn().click();

        cy.waitRequest('@loadSeguimientoTipo', 200);
        cy.waitRequest('@loadCampanya', 200);

        page.rellenarSeguimientoCliente();
        page.getDesarBtn().click();
        cy.waitRequest('@addSeguimientoCliente', 200);
    });

    it('PUT seguimiento de cliente', () => {
        page.seleccionarFilaSeguimientosCliente().dblclick();

        cy.waitRequest('@loadSeguimientoTipo', 200);
        cy.waitRequest('@loadCampanya', 200);

        page.editarSeguimientoCliente();
        page.getDesarBtn().click();
        cy.waitRequest('@editSeguimientoCliente', 200);
    });

    it('DELETE seguimiento de cliente', () => {
        page.buscarCliente();
        page.seleccionarFilaClientesGrid();
        page.irPanelSeguimientos();

        cy.waitRequest('@loadSeguimientoTipo', 200);
        cy.waitRequest('@loadCampanya', 200);
        cy.waitRequest('@loadClienteSeguimientoGrid', 200);

        page.borrarSeguimientoCliente();
        cy.waitRequest('@deleteSeguimientoCliente', 204);
    });
});