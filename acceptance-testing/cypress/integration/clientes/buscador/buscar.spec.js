import BuscarPage from "../../../pagesobjects/clientes/buscador/Buscar";

describe('Testing Buscar Cliente', () => {

    let page;

    beforeEach(() => {
        page = new BuscarPage(cy);
        page.visitSection();
    });

    it('Buscar cliente', () => {
        cy.intercept('GET', '/crm/rest/clientegrid/*').as('loadClienteGrid');
        cy.intercept('GET', '/crm/rest/cliente/*').as('loadCliente');

        page.getCercarBtnPanelClientes().click();
        page.buscarNombreCypress();
        page.getCercarBtnBuscadorClientesWindow().click();

        cy.wait('@loadClienteGrid').its('response.statusCode').should('eq',200);

        page.seleccionarFilaClientesGrid();

        cy.wait('@loadCliente').its('response.statusCode').should('eq',200);
    });
})