import FotosClientePage from '../../../pagesobjects/clientes/fotos/FotosCliente'

describe('Test fotos cliente', () => {
    let page;

    beforeEach(() => {
        page = new FotosClientePage(cy);
        cy.intercept('GET', '/crm/rest/clientegrid/*').as('cargaClientes')
    });

    it('Buscar cliente existente', () => {
        page.buscarCliente();

        cy.wait('@cargaClientes').its('response.statusCode').should('eq',200);

        page.seleccionarFilaClientesGrid();

        page.irPanelFotos();
        page.validarFotoCRM();
    });
});