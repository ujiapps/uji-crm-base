import SuscripcionesClientePage from '../../../pagesobjects/clientes/suscripciones/SuscripcionesCliente';

describe('Test suscripciones de cliente', () => {
    let page;

    beforeEach(() => {
        page = new SuscripcionesClientePage(cy);
        cy.intercept('POST', '/crm/rest/clienteitem/').as('addSuscripcionCliente');
        cy.intercept('PUT', '/crm/rest/clienteitem/*').as('editSuscripcionCliente');
        cy.intercept('DELETE', '/crm/rest/clienteitem/*').as('deleteSuscripcionCliente');
        cy.intercept('GET', '/crm/rest/clienteitem/suscripciones/*').as('loadSuscripcionesCliente');
        cy.intercept('GET', '/crm/rest/programa/*').as('loadComboPrograma');

    });

    it('Buscar cliente existente', () => {
        page.buscarCliente();
        page.seleccionarFilaClientesGrid();
        page.irPanelSuscripciones();
    });

    it('POST suscripción de cliente', () => {
        page.getAfegirBtn().click();
        cy.waitRequest('@loadComboPrograma', 200);
        page.rellenarSuscripcionCliente();
        page.getDesarBtn().click();
        cy.waitRequest('@addSuscripcionCliente', 200);
        cy.waitRequest('@loadSuscripcionesCliente', 200);
    });

    it('PUT suscripción de cliente', () => {
        page.seleccionarFilaSuscripcionesCliente();
        page.editarSuscripcionCliente();
        cy.editorAcceptBtn().click();
        cy.waitRequest('@editSuscripcionCliente', 200);
        cy.waitRequest('@loadSuscripcionesCliente', 200);
    });

    it('DELETE suscripción de cliente', () => {
        page.borrarSuscripcionCliente();
        cy.waitRequest('@deleteSuscripcionCliente', 204);
    });
});