import EnviosPage from "../../../pagesobjects/clientes/envios/Envios";

describe('Testing Gestion de Envios ( Cliente )', () => {

    let page;

    beforeEach(() => {
        page = new EnviosPage(cy);
        page.visitSection();

        cy.intercept('GET', '/crm/rest/clientegrid/*').as('loadClienteGrid');
        cy.intercept('GET', '/crm/rest/cliente/*').as('loadCliente');
        cy.intercept('POST', '/crm/rest/envio/cliente/*').as('addEnvio');
        cy.intercept('GET', '/crm/rest/enviocliente/*').as('loadEnvio');
        cy.intercept('PUT', '/crm/rest/envio/*/cliente').as('editEnvio');
        cy.intercept('DELETE', '/crm/rest/enviocliente/*').as('deleteEnvio');
        cy.intercept('GET', '/crm/rest/cliente/general/*').as('clientegeneral');

        page.getCercarBtnPanelClientes().click();
        page.buscarNombreCypress();
        page.getCercarBtnBuscadorClientesWindow().click();

        cy.waitRequest('@loadClienteGrid' ,200);
        page.seleccionarFilaClientesGrid();

        cy.waitRequest('@loadCliente' ,200);

        page.getEnviosTab();
        cy.waitRequest('@clientegeneral' ,200);
    });

    it('Añadir un nuevo envio', () => {
        page.getAfegirBtn();
        page.typeDatos();
        page.writeOnBody();
        page.getDesarBtn();

        cy.waitRequest('@addEnvio', 200);
        cy.waitRequest('@loadEnvio', 200);
    });

    it('Editar un envio', () => {
        cy.waitRequest('@loadEnvio', 200);

        page.selectRowEnvioGrid();
        page.typeDatos();
        page.writeOnBody();
        page.getDesarBtn();

        cy.waitRequest('@editEnvio', 200);
        cy.waitRequest('@loadEnvio', 200)
    });

    it('Duplicar un envio', () => {
        cy.waitRequest('@loadEnvio', 200);

        page.selectRowEnvioGrid();
        page.getDuplicarBtn();
        page.typeDupli();
        page.getDesarBtn();

        cy.waitRequest('@addEnvio', 200)
    });

    it('Eliminar un envio duplicado', () => {
        cy.waitRequest('@loadEnvio', 200);

        page.selectRowEnvioGridDupli();
        page.getEsborrarBtn();
        cy.messageBoxAcceptBtn().click();
        cy.waitRequest('@deleteEnvio', 204);
        cy.waitRequest('@loadEnvio' ,200);
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        });
    });

    it('Eliminar un envio', () => {
        cy.waitRequest('@loadEnvio', 200);

        page.selectRowEnvioGrid();
        page.getEsborrarBtn();
        cy.messageBoxAcceptBtn().click();
        cy.waitRequest('@deleteEnvio', 204);
        cy.waitRequest('@loadEnvio' ,200);
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        });
    });
})