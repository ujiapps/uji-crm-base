import NuevoClientePage from '../../../pagesobjects/clientes/nuevo-cliente/NuevoCliente';

describe('Test clientes', () => {
    let page;

    beforeEach(() => {
        page = new NuevoClientePage(cy);
        page.visitarSeccion();
    });

    it('Anyadir un cliente', () => {
        page.getAfegirBtn().click();
        page.rellenarNuevoClienteForm();
        cy.uniqueMessageBoxAcceptBtn().click();
    });
});