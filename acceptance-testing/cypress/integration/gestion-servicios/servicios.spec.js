import ServiciosPage from "../../pagesobjects/gestion-servicios/Servicios";

describe('Testing gestión de servicios', () => {

    let page, urlServicios, urlPersonas, urlLineasFacturacion;

    beforeEach(() => {
        page = new ServiciosPage(cy);
        urlServicios = '/crm/rest/servicio/*';
        urlPersonas = '/crm/rest/serviciousuario/*';
        urlLineasFacturacion = '/crm/rest/lineafacturacion/*';
        cy.intercept('GET', urlServicios).as('loadServicios');
        cy.intercept('POST', urlServicios).as('addServicio');
        cy.intercept('POST', urlPersonas).as('addPersona');
        cy.intercept('POST', urlLineasFacturacion).as('addLineaFacturacion');
        cy.intercept('GET', '/crm/rest/lookup*').as('lookup');

        cy.intercept('GET', '/crm/rest/serviciousuario/*').as('serviciousuario');
        cy.intercept('GET', '/crm/rest/lineafacturacion/*').as('lineafacturacion');
        cy.intercept('GET', '/crm/rest/emisora/*').as('emisora');
        cy.intercept('GET', '/crm/rest/reciboformato/*').as('reciboformato');

        page.visitarSeccion();
        cy.waitRequest('@loadServicios', 200);
    })

    it('POST servicio', () => {
        page.rellenarServicio();

        cy.waitRequest('@addServicio', 200);
    });

    it('POST persona', () => {
        page.rellenarPersona();

        cy.waitRequest('@addPersona', 200);
    });

    it('POST linea facturación', () => {
        page.rellenarLineaFacturacion();

        cy.waitRequest('@addLineaFacturacion', 200);
    });

    it('DELETE linea facturación', () => {
        page.borrarLineaFacturacion();
    });

    it('DELETE persona', () => {
        page.borrarPersona();
    });

    it('DELETE servicio', () => {
        page.borrarServicio();
    });
});