import GestionEnviosPage from "../../pagesobjects/gestion-envios/GestionEnvios";

describe('Testing gestion de envios', () => {
    let page;

    beforeEach(() => {
        page = new GestionEnviosPage(cy);

        cy.intercept('GET', '/crm/app.json*').as('loadJson');
        cy.intercept('GET', '/crm/rest/navigation/*').as('loadNavigation');
        cy.intercept('GET', '/crm/rest/userinfo*').as('loadUserInfo');

        cy.intercept('GET', '/crm/rest/envioplantilla/*').as('loadGestionEnvios');
        cy.intercept('POST', '/crm/rest/envioplantilla').as('addGestionEnvios');
        cy.intercept('PUT', '/crm/rest/envioplantilla/*').as('editGestionEnvios');
        cy.intercept('DELETE', '/crm/rest/envioplantilla/*').as('deleteGestionEnvios');

        page.visitSection();
        cy.waitRequest('@loadJson', 200);
        cy.waitRequest('@loadNavigation', 200);
        cy.waitRequest('@loadUserInfo', 200);

        cy.waitRequest('@loadGestionEnvios', 200);
    });

    it('Definir un envio', () => {
        page.getAfegirGestionEnviosBtn().click();
        cy.checkLoading();
        page.getNomTextField().type("Cypress Test Text - Gestion de Envios");

        page.writeInContenido();
        page.getDesarBtn().click();

        cy.waitRequest('@addGestionEnvios',200);

        page.getAcceptarBtnMessageBox().click();
        page.getCancelarBtn().click();

        cy.waitRequest('@loadGestionEnvios', 200);
    });

    it('Editar un envio', () => {
        page.selectRowEnvio("Cypress Test Text - Gestion de Envios").click();
        page.getEditarBtn().click();
        page.getNomTextField().clear().type("EDIT Cypress Test Text - Gestion de Envios");
        cy.checkLoading();
        page.editContenido();
        page.getDesarBtn().click();

        cy.waitRequest('@editGestionEnvios', 200);

        page.getAcceptarBtnMessageBox().click();
        page.getCancelarBtn().click();

        cy.waitRequest('@loadGestionEnvios', 200);
    });

    it('Borrar un envio', () => {
        page.selectRowEnvio("EDIT Cypress Test Text - Gestion de Envios").click();
        page.getDeleteBtn().click();
        page.getMensageDeAlerta().click();
        cy.waitRequest('@deleteGestionEnvios', 204);
    });
});
