import EnviosFallidosPage from '../../../pagesobjects/EnviosFallidos';

describe('Testing Envíos fallidos', () => {
    let page;

    before(() => {
        page = new EnviosFallidosPage(cy);
        cy.intercept({
            method: 'GET',
            url: /correonovalido\/.*correo=&/,
        }).as('loadFallidos');
        cy.intercept({
            method: 'GET',
            url: /correonovalido\/.*correo=[^&]+&/,
        }).as('searchFallidos');
        page.visitSection();
        cy.wait('@loadFallidos').should(({ response }) => {
            expect(response.statusCode).to.eq(200);
        });

    })

    it('Buscar envios por correo', () => {
        page.getCercarInput().type("agusti");
        page.getCercarBtn().click();
        cy.wait('@searchFallidos').should(({ response }) => {
            expect(response.body.data.length).to.eq(response.body.data.filter(el => el.nombre.indexOf("agusti") >= 0).length);
        });
    })

    it('Limpiar búsqueda', () => {
        page.getNetejarBtn().click();
        page.getCercarInput().should('be.empty');
    })
})
