import EtiquetasPage from '../../../pagesobjects/Etiquetas';

describe('Testing gestión de etiquetas', () => {

    let page;

    beforeEach(() => {
        page = new EtiquetasPage(cy);

        cy.intercept('GET', '/crm/rest/etiqueta/*').as('loadEtiquetas');
        cy.intercept('POST', '/crm/rest/etiqueta/*').as('addEtiqueta');

        page.visitSection();
        cy.wait('@loadEtiquetas');
    })

    it('Definir una etiqueta', () => {
        page.getAfegirEtiquetaBtn().click();
        cy.haveInvalidFields(1);
        page.fillEditorFields("Prueba etiqueta");
        cy.haveInvalidFields(0);
        cy.editorAcceptBtn().click();
        cy.wait('@addEtiqueta').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });
    });

    it('Borrar etiqueta', () => {
        page.clearTest("Prueba etiqueta");
        cy.wait('@loadEtiquetas')
    })
})