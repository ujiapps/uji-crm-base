import TextosLOPDPage from '../../../pagesobjects/TextosLOPD';

describe('Testing textos LOPD', () => {
    let page;

    beforeEach(() => {
        page = new TextosLOPDPage(cy);
        cy.intercept('GET', '/crm/rest/campanyatexto/*').as('loadTextosLOPD');
        page.visitSection();
        cy.wait('@loadTextosLOPD');
    });

    it('Definir un texto LOPD', () => {
        cy.intercept('POST', '/crm/rest/campanyatexto/').as('addTextosLOPD');
        page.getAfegirTextosLOPDBtn().click();
        page.getNombreTextField();
        page.getCodigoTextField();
        page.getTextosIdiomasArea();
        page.desarButton().click();
        cy.wait('@addTextosLOPD').its('response.statusCode').should('eq',200);
        cy.wait('@loadTextosLOPD');
    });

    it('Actualizar un texto LOPD', () => {
        cy.intercept('PUT', '/crm/rest/campanyatexto/*').as('editTextosLOPD');
        page.selectRowTextosLOPD().dblclick();
        page.editTextosLOPDTextAreas();
        page.desarButton().click();
        cy.wait('@editTextosLOPD').its('response.statusCode').should('eq',200);
        cy.wait('@loadTextosLOPD');
    });

    it('Borrar un texto LOPD', () => {
        cy.intercept('DELETE', '/crm/rest/campanyatexto/*').as('deleteTextosLOPD');
        page.deleteTextosLOPD().click();
        cy.wait('@deleteTextosLOPD').its('response.statusCode').should('eq',204);
        cy.wait('@loadTextosLOPD');
    });
});