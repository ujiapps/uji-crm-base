import VinculacionesPage from '../../../pagesobjects/Vinculaciones';

describe('Testing tipos de vinculaciones', () => {

    let page;

    beforeEach(() => {
        page = new VinculacionesPage(cy);
        page.visitSection();
        cy.intercept('GET', '/crm/rest/tipo/vinculaciones/*').as('loadVinculaciones');
        cy.intercept('POST', '/crm/rest/tipo/vinculaciones/*').as('addVinculacion');
        cy.intercept('PUT', '/crm/rest/tipo/vinculaciones/*').as('modifyVinculacion');
        //cy.wait('@loadVinculaciones');
    })

    it('POST', () => {
        page.getAfegirVinculacionBtn().click();
        cy.haveInvalidFields(1);
        page.fillEditorFields();
        cy.haveInvalidFields(0);
        cy.editorAcceptBtn().click();
        cy.wait('@addVinculacion').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });
    });

    it('PUT', () => {
        page.getFilaTiposVinculacionesGrid().dblclick();
        page.modifyEditorFields();
        cy.editorAcceptBtn().click();
        cy.wait('@modifyVinculacion').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });
    });

    it('DELETE', () => {
        page.borrarRegistro();
        //cy.wait('@loadVinculaciones')
    })

    /*
    it('GET para comprobar que recibimos datos', () => {
        cy.request({
            method: 'GET',
            url: 'http://localhost:9005/crm/rest/tipo/vinculaciones'
        }).then(function(response) {
            expect(response.body).have.property('success', true);
        });
    });

    let getTiposDeVinculacion = () => cy.request('http://localhost:9005/crm/rest/tipo/vinculaciones').its('body.data');

    it('GET para comprobar que recibimos las propiedades .json esperadas', () => {
        getTiposDeVinculacion().each(value => expect(value).to.have.all.keys('tablaColumna', 'id', 'nombre'))
    });

    let tipoVinculacionId;

    it('POST', () => {
        cy.request({
            method: 'POST',
            url: 'http://localhost:9005/crm/rest/tipo/vinculaciones',
            body: {
                'nombre': 'prueba inserción tipo de vinculación'
            },
            headers: {
                'content-type': 'application/json',
            }
        }).then(function(response) {
            tipoVinculacionId = response.body.data.id;
            expect(response.body).have.property('data');
            expect(response.body.data).to.deep.contains({
                nombre: "prueba inserción tipo de vinculación"
            });
        });
    });

    it('PUT', () => {
        cy.request({
            method: 'PUT',
            url: 'http://localhost:9005/crm/rest/tipo/vinculaciones/' + tipoVinculacionId,
            body: {
                id: tipoVinculacionId,
                nombre: 'prueba modificación tipo de vinculación',
                tablaColumna: 'vinculaciones.tipoDato'
            },
            headers: {
                'content-type': 'application/json',
            }
        }).then(function(response)  {
            expect(response.status).equal(200)
            expect(response.body.data.nombre).equal('prueba modificación tipo de vinculación');
        });
    });

    it('DELETE', () => {
        cy.request({
            method: 'DELETE',
            url: 'http://localhost:9005/crm/rest/tipo/vinculaciones/' + tipoVinculacionId,
            body: {
                id: tipoVinculacionId,
                nombre: 'prueba modificación tipo de vinculación',
                tablaColumna: 'vinculaciones.tipoDato'
            },
            headers: {
                'content-type': 'application/json',
            }
        }).then(function(response) {
            expect(response.status).equal(204);
        });
    });
     */
});