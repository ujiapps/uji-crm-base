import TiposSeguimientosPage from "../../../pagesobjects/TiposSeguimientos";

describe('Testing tipos de seguimiento', () => {

    let page;

    beforeEach(() => {
        page = new TiposSeguimientosPage(cy);

        cy.intercept('GET', '/crm/rest/seguimientotipo/*').as('loadTiposSeguimientos');
        cy.intercept('POST', '/crm/rest/seguimientotipo/*').as('addTipoSeguimiento');

        page.visitSection();
        cy.wait('@loadTiposSeguimientos');
    })

    it('Definir un tipus de seguiment', () => {
        page.getAfegirSeguimientoBtn().click();
        cy.haveInvalidFields(1);
        page.fillEditorFields();
        cy.haveInvalidFields(0);
        cy.editorAcceptBtn().click();
        cy.wait('@addTipoSeguimiento').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });
    });

    it('Borrar tipus de seguiment', () => {
        page.clearTest();
        cy.wait('@loadTiposSeguimientos')
    })

})