import EstudiosNoUJIPage from "../../../pagesobjects/gestion-estudios/estudiosNoUJI/EstudiosNoUJI";

describe('Testing gestión de estudios No UJI', () => {

    let page;

    beforeEach(() => {
        page = new EstudiosNoUJIPage(cy);

        cy.intercept('GET','/crm/rest/clasificacionestudionouji/*').as('loadEstudiosNoUJI');
        cy.intercept('POST', '/crm/rest/clasificacionestudionouji/*').as('addEstudioNoUJI');

        page.visitSection();
        cy.wait('@loadEstudiosNoUJI');
        page.getEstudiosNoUJITab().click();
    });

    it('Definir un estudio no UJI', () => {
        page.getAfegirEstudioNoUJIBtn().click();
        cy.haveInvalidFields(4);
        page.fillEditorFields();
        cy.haveInvalidFields(0);
        cy.editorAcceptBtn().click();
        cy.wait('@addEstudioNoUJI').should(({response}) => {
            expect(response.statusCode).to.eq(200);
        });
    });

    it('Borrar un estudio no UJI', () => {
        page.clearTest();
        cy.wait('@loadEstudiosNoUJI');
    })
})