import EstudisUJIPage from "../../../pagesobjects/gestion-estudios/estudisUJI/EstudisUJI";

describe('Testing gestión de estudios UJI', () => {

    let page;

    beforeEach(() => {
        page = new EstudisUJIPage(cy);

        cy.intercept('GET','/crm/rest/grupo/*').as('loadGrupoUJI');
        cy.intercept('GET', '/crm/rest/clasificacionestudio/*').as('loadClasificacionesEstudioUJI');

        page.visitSection();

        cy.wait('@loadGrupoUJI');
        cy.wait('@loadClasificacionesEstudioUJI');
    });

    it('Añadir item a Estudio UJI', () => {
        cy.intercept('POST','/crm/rest/itemclasificacion/clasificacion/*').as('addItem');
        cy.intercept('GET','/crm/rest/itemclasificacion/clasificacion/*').as('loadItem');

        page.selectSenseItemAssociatsCombo('Sense items associats');
        page.selectTipusEstudiCombo('Doctorat');
        page.selectRowEstudisUJI().click();
        page.getItemTabBar().click();
        page.getBtnAfegirFromItemGrid().click();

        page.selectProgramaCombo();
        page.selectGrupoCombo();
        page.selectItemCombo();
        page.getBtnDesarItemWindow().click();

        cy.wait('@addItem').its('response.statusCode').should('eq',200);
        cy.wait('@loadItem');
    });

    it('Eliminar item de Estudio UJI', () => {
        cy.intercept('DELETE','/crm/rest/itemclasificacion/*').as('deleteItem');

        page.selectSenseItemAssociatsCombo('Amb items associats');
        page.selectTipusEstudiCombo('Doctorat');
        page.selectRowEstudisUJI().click();
        page.getItemTabBar().click();
        page.selectItemGrid().click();
        page.getBtnEsborrarFromIemGrid().click();
        cy.messageBoxAcceptBtn().click();

        cy.wait('@deleteItem').its('response.statusCode').should('eq',204);
    });
})