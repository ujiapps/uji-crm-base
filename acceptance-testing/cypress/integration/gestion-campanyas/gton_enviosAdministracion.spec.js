import CampanyaPage from "../../pagesobjects/gestion-campanyas/Campanya";
import EstadoPage from "../../pagesobjects/gestion-campanyas/Estado";
import EnvioAdministracionPage from "../../pagesobjects/gestion-campanyas/EnvioAdministracion";

describe('Testing gestión de campanyas: envios Administración', () => {

    let campanya;
    let estado;
    let enviosAdmin;

    beforeEach(() => {
        campanya = new CampanyaPage(cy);
        estado = new EstadoPage(cy);
        enviosAdmin = new EnvioAdministracionPage(cy);
        cy.intercept('GET', '/crm/rest/campanya/user/tree/root/admin/*').as('loadCampanyas');
        cy.intercept('GET', '/crm/rest/tipoestadocampanya/campanya/*').as('loadEstadosCampanya');
        cy.intercept('GET', '/crm/rest/campanyaenvioadmin/*').as('loadEnviosAdmin');

        cy.intercept('GET', '/crm/rest/subvinculo/*').as('subvinculo');
        cy.intercept('GET', '/crm/rest/campanyatexto/*').as('campanyatexto');
        cy.intercept('GET', '/crm/rest/campanya/*').as('campanya');
        cy.intercept('GET', '/crm/rest/programa/*').as('programa');
        cy.intercept('GET', '/crm/rest/lineafacturacion/*').as('lineafacturacion');

        enviosAdmin.visitSection();
        cy.wait('@loadCampanyas');
    });

    it('Crear y borrar envio Administración', () => {
        // Crea campaña
        campanya.createCampanya();
        cy.waitRequest('@loadCampanyas', 200);

        //Crea un estado para poder anyadirlo después a destino-origen
        campanya.selectCampanya();
        estado.selectTab().click();
        cy.waitRequest('@loadEstadosCampanya', 200);
        estado.getAfegirBtn().click();
        estado.fillEditorEstadosGrid();
        cy.waitRequest('@loadEstadosCampanya', 200);

        //Crear envio Administración
        enviosAdmin.selectTab().click();
        cy.waitRequest('@loadEnviosAdmin', 200);
        enviosAdmin.getAfegirBtn().click();
        cy.waitRequest('@loadEstadosCampanya', 200);
        enviosAdmin.fillFormFields();
        cy.waitRequest('@loadEnviosAdmin', 200);

        //Borrar envio Administración
        enviosAdmin.borrarEnvioAdmin();
        cy.waitRequest('@loadEnviosAdmin', 200);

        // Borrar estado y campaña
        estado.selectTab().click();
        cy.waitRequest('@loadEstadosCampanya', 200);
        estado.clearTestEstadosGrid();
        campanya.clearCampanyaGrid();
        cy.waitRequest('@loadCampanyas', 200);
    })
})