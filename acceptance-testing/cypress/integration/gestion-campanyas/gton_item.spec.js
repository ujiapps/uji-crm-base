import CampanyaPage from "../../pagesobjects/gestion-campanyas/Campanya";
import ItemPage from "../../pagesobjects/gestion-campanyas/Item";

describe('Testing gestion de campanyas: item', () => {

    let campanya;
    let item;

    beforeEach(() => {
        campanya = new CampanyaPage(cy);
        item = new ItemPage(cy);
        cy.intercept('GET', '/crm/rest/campanya/user/tree/root/admin/*').as('loadCampanyas');
        cy.intercept('GET', '/crm/rest/campanyaitem/*').as('loadItems');
        cy.intercept('GET', '/crm/rest/grupo/*').as('loadGrupo');

        cy.intercept('GET', '/crm/rest/subvinculo/*').as('subvinculo');
        cy.intercept('GET', '/crm/rest/campanyatexto/*').as('campanyatexto');
        cy.intercept('GET', '/crm/rest/campanya/*').as('campanya');
        cy.intercept('GET', '/crm/rest/programa/*').as('programa');
        cy.intercept('GET', '/crm/rest/lineafacturacion/*').as('lineafacturacion');

        item.visitSection();
        cy.waitRequest('@loadCampanyas', 200);
    });

    it('Crear ítem', () => {

        // Crea campaña
        campanya.createCampanya();
        cy.waitRequest('@loadCampanyas', 200);

        // Crea y borra item a partir de la campaña
        campanya.selectCampanya();
        item.selectTab().click();
        cy.waitRequest('@loadItems', 200);
        item.getAfegirBtn().click();
        cy.waitRequest('@loadGrupo', 200);
        item.selectGrupo();
        item.borrarItem();
        cy.waitRequest('@loadItems', 200);
        campanya.clearCampanyaGrid();
    });
});