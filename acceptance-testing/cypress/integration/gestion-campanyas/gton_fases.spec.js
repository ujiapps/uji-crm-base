import CampanyaPage from "../../pagesobjects/gestion-campanyas/Campanya";
import FasePage from "../../pagesobjects/gestion-campanyas/Fase";

describe('Testing gestion de campanyas: fases', () => {

    let campanya;
    let fase;

    beforeEach(() => {
        campanya = new CampanyaPage(cy);
        fase = new FasePage(cy);
        cy.intercept('GET', '/crm/rest/campanya/user/tree/root/admin/*').as('loadCampanyas');
        cy.intercept('GET', '/crm/rest/campanyafase/*').as('loadFase');

        cy.intercept('GET', '/crm/rest/subvinculo/*').as('subvinculo');
        cy.intercept('GET', '/crm/rest/campanyatexto/*').as('campanyatexto');
        cy.intercept('GET', '/crm/rest/campanya/*').as('campanya');
        cy.intercept('GET', '/crm/rest/programa/*').as('programa');
        cy.intercept('GET', '/crm/rest/lineafacturacion/*').as('lineafacturacion');

        fase.visitSection();
        cy.wait('@loadCampanyas');
    });

    it('Crear fase', () => {

        // Crea campaña
        campanya.createCampanya();
        cy.wait('@loadCampanyas');

        // Crea fase a partir de la campaña
        campanya.selectCampanya();
        fase.selectTab().click();
        cy.wait('@loadFase');
        fase.getAfegirBtn().click();
        fase.fillEditorFasesGrid();
        cy.wait('@loadFase');

        // Borrar estado y campaña
        fase.selectRow();
        fase.clearTestFasesGrid();
        campanya.clearCampanyaGrid();

    });

});