import CampanyaPage from "../../pagesobjects/gestion-campanyas/Campanya";
import EstadoPage from "../../pagesobjects/gestion-campanyas/Estado";
import EnvioCampanyaPage from "../../pagesobjects/gestion-campanyas/Envio";

describe('Testing gestión de campanyas: Envios', () => {
    let campanya;
    let estado;
    let envio;

    beforeEach(() => {
        campanya = new CampanyaPage(cy);
        estado = new EstadoPage(cy);
        envio = new EnvioCampanyaPage(cy);
        cy.intercept('GET', '/crm/rest/campanya/user/tree/root/admin/*').as('loadCampanyas');
        cy.intercept('GET', '/crm/rest/tipoestadocampanya/campanya/*').as('loadEstadosCampanya');
        cy.intercept('GET', 'crm/rest/campanyaenvioauto/*').as('loadEnviosCampanya');
        cy.intercept('GET', 'crm/rest/campanyaenvioprogramado/*').as('loadProgramacionEnvios');

        cy.intercept('GET', '/crm/rest/subvinculo/*').as('subvinculo');
        cy.intercept('GET', '/crm/rest/campanyatexto/*').as('campanyatexto');
        cy.intercept('GET', '/crm/rest/campanya/*').as('campanya');
        cy.intercept('GET', '/crm/rest/programa/*').as('programa');
        cy.intercept('GET', '/crm/rest/lineafacturacion/*').as('lineafacturacion');
        envio.visitSection();
        cy.waitRequest('@loadCampanyas', 200);
    });

    it('Crear envío de campanya', () => {

        // Crea campaña
        campanya.createCampanya();
        cy.waitRequest('@loadCampanyas', 200);

        //Crea un estado para poder anyadirlo después al envio
        campanya.selectCampanya();
        estado.selectTab().click();
        cy.waitRequest('@loadEstadosCampanya', 200);
        estado.getAfegirBtn().click();
        estado.fillEditorEstadosGrid();
        cy.waitRequest('@loadEstadosCampanya', 200);

        //Crear envio
        envio.selectTab().click();
        cy.waitRequest('@loadEnviosCampanya', 200);
        envio.getAfegirBtn().click();
        envio.fillFormFields();
        cy.waitRequest('@loadEnviosCampanya', 200);

        //Crear programacion de envio
        envio.selectRow();
        envio.getProgramarEnviosBtn().click();
        cy.waitRequest('@loadProgramacionEnvios', 200);
        envio.getAfegirProgramacionBtn().click();
        envio.fillEditorFields();

        //Borrar programacion de envio
        envio.borrarProgramacionEnvio();
        cy.waitRequest('@loadProgramacionEnvios', 200);

        //Borrar envio
        envio.borrarEnvioCampanya();
        cy.waitRequest('@loadEnviosCampanya', 200);

        // Borrar estado y campaña
        estado.selectTab().click();
        cy.waitRequest('@loadEstadosCampanya', 200);
        estado.clearTestEstadosGrid();
        campanya.clearCampanyaGrid();
        cy.waitRequest('@loadCampanyas', 200);
    })
})