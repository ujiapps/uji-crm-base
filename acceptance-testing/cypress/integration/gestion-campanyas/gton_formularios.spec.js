import CampanyaPage from "../../pagesobjects/gestion-campanyas/Campanya";
import EstadoPage from "../../pagesobjects/gestion-campanyas/Estado";
import FormulariosCampanyaPage from "../../pagesobjects/gestion-campanyas/Formulario";

describe('Testing gestión de campanyas: Formularios', () => {
    let campanya;
    let estado;
    let formulario;

    beforeEach(() => {
        campanya = new CampanyaPage(cy);
        estado = new EstadoPage(cy);
        formulario = new FormulariosCampanyaPage(cy);
        cy.intercept('GET', '/crm/rest/campanya/user/tree/root/admin/*').as('loadCampanyas');
        cy.intercept('GET', '/crm/rest/tipoestadocampanya/campanya/*').as('loadEstadosCampanya');
        cy.intercept('GET', '/crm/rest/campanyaformulario/*').as('loadFormularios');
        cy.intercept('GET', '/crm/rest/campanyadato/*').as('loadDatoFormulario');

        cy.intercept('GET', '/crm/rest/subvinculo/*').as('subvinculo');
        cy.intercept('GET', '/crm/rest/campanyatexto/*').as('campanyatexto');
        cy.intercept('GET', '/crm/rest/campanya/*').as('campanya');
        cy.intercept('GET', '/crm/rest/programa/*').as('programa');

        cy.intercept('GET', '/crm/rest/lineafacturacion/*').as('lineafacturacion');
        cy.intercept('POST', '/crm/rest/campanya').as('campanyapost');

        cy.intercept('GET', '/crm/rest/campanyaacto/*').as('campanyaacto');
        cy.intercept('GET', '/crm/rest/tiposolicitudcampo/*').as('tiposolicitudcampo');

        cy.intercept('GET', '/crm/rest/clientedatotipo/*').as('clientedatotipo');
        cy.intercept('GET', '/crm/rest/campanya/*/item/*').as('grupoall');
        cy.intercept('GET', '/crm/rest/campanyadato/*').as('campanyaitem');
        cy.intercept('GET', '/crm/rest/tipo/acceso/*').as('accesotipo');
        cy.intercept('GET', '/crm/rest/tipovalidacion/*').as('tipovalidacion');

        cy.intercept('POST', '/crm/rest/campanyadato').as('campanyadatopost');
        cy.intercept('DELETE', '/crm/rest/campanyadato/*').as('campanyadatodelete');
        cy.intercept('DELETE', '/crm/rest/campanyaformulario/*').as('campanyaformulariodelete');
        cy.intercept('DELETE', '/crm/rest/tipoestadocampanya/*').as('tipoestadocampanyadelete');
        cy.intercept('DELETE', '/crm/rest/campanya/*').as('deletecampanya');

        formulario.visitSection();
        cy.waitRequest('@loadCampanyas', 200);
    });

    it('Definir formulario de campanya', () => {

        // Crea campaña
        campanya.createCampanya();
        cy.waitRequest('@campanyapost', 200)
        cy.waitRequest('@campanya', 200)
        cy.waitRequest('@loadCampanyas', 200);

        //Crea un estado para poder anyadirlo después al envio
        campanya.selectCampanya();
        // estado.selectTab().click();
        cy.waitRequest('@loadEstadosCampanya', 200);
        estado.getAfegirBtn().click();
        estado.fillEditorEstadosGrid();
        cy.waitRequest('@loadEstadosCampanya', 200);

        //Crear formulario
        formulario.selectTab().click();
        cy.waitRequest('@loadFormularios', 200);
        formulario.getAfegirBtn().click();

        cy.waitRequest('@campanyaacto', 200)
        cy.waitRequest('@loadEstadosCampanya', 200)
        cy.waitRequest('@tiposolicitudcampo', 200)

        formulario.fillFormFields();

        //Rellenar Datos Formulario
        formulario.anyadirDatoFormulario();
        cy.waitRequest('@campanyadatopost', 200);

        //Borrar datos formulario
        formulario.borrarDatoFormulario();
        cy.waitRequest('@campanyadatodelete', 204);

        cy.waitRequest('@loadDatoFormulario', 200);
        formulario.getWindow().get('.x-toolbar-item').contains('Tancar').click();
        cy.waitRequest('@loadFormularios', 200);

        //Borrar formulario
        formulario.borrarFormularioCampanya();
        cy.waitRequest('@campanyaformulariodelete', 204);

        // Borrar estado y campaña
        estado.selectTab().click();
        cy.waitRequest('@loadFormularios', 200);
        cy.waitRequest('@loadEstadosCampanya', 200);

        estado.clearTestEstadosGrid();
        cy.waitRequest('@tipoestadocampanyadelete', 204);
        cy.waitRequest('@loadEstadosCampanya', 200);

        campanya.clearCampanyaGrid();
        cy.waitRequest('@deletecampanya', 204);
        cy.waitRequest('@loadCampanyas', 200);
    });
})