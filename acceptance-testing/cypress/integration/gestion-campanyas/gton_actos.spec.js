import CampanyaPage from "../../pagesobjects/gestion-campanyas/Campanya";
import EstadoPage from "../../pagesobjects/gestion-campanyas/Estado";
import ActoCampanyaPage from "../../pagesobjects/gestion-campanyas/Acto";

describe('Testing gestión de campanyas: Actos', () => {

    let campanya;
    let estado;
    let acto;

    beforeEach(() => {
        campanya = new CampanyaPage(cy);
        estado = new EstadoPage(cy);
        acto = new ActoCampanyaPage(cy);
        cy.intercept('GET', '/crm/rest/campanya/user/tree/root/admin/*').as('loadCampanyas');
        cy.intercept('GET', '/crm/rest/tipoestadocampanya/campanya/*').as('loadEstadosCampanya');
        cy.intercept('GET', 'crm/rest/campanyaacto/*').as('loadActos');

        cy.intercept('GET', '/crm/rest/subvinculo/*').as('subvinculo');
        cy.intercept('GET', '/crm/rest/campanyatexto/*').as('campanyatexto');
        cy.intercept('GET', '/crm/rest/campanya/*').as('campanya');
        cy.intercept('GET', '/crm/rest/programa/*').as('programa');
        cy.intercept('GET', '/crm/rest/lineafacturacion/*').as('lineafacturacion');

        acto.visitSection();
        cy.wait('@loadCampanyas');
    });

    it('Crear y borrar acto', () => {
        // Crea campaña
        campanya.createCampanya();
        cy.wait('@loadCampanyas');

        //Crea un estado para poder anyadirlo después a destino-origen
        campanya.selectCampanya();
        estado.selectTab().click();
        cy.wait('@loadEstadosCampanya');
        estado.getAfegirBtn().click();
        estado.fillEditorEstadosGrid();
        cy.wait('@loadEstadosCampanya');

        //Crear acto
        acto.selectTab().click();
        cy.wait('@loadActos');
        acto.getAfegirBtn().click();
        acto.fillFormFields();
        cy.wait('@loadActos');

        //Borrar acto
        acto.borrarActoCampanya();
        cy.wait('@loadActos');

        // Borrar estado y campaña
        estado.selectTab().click();
        cy.wait('@loadEstadosCampanya');
        estado.clearTestEstadosGrid();
        campanya.clearCampanyaGrid();
        cy.wait('@loadCampanyas');
    })
})