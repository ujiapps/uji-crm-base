import CampanyaPage from '../../pagesobjects/gestion-campanyas/Campanya';


describe('Testing gestion de campanyas', () => {

    let campanya;

    beforeEach(() => {
        campanya = new CampanyaPage(cy);
        cy.intercept('GET', '/crm/rest/campanya/user/tree/root/admin/*').as('loadCampanyas');
        cy.intercept('POST', '/crm/rest/programa/*').as('addPrograma');

        cy.intercept('GET', '/crm/rest/subvinculo/*').as('subvinculo');
        cy.intercept('GET', '/crm/rest/campanyatexto/*').as('campanyatexto');
        cy.intercept('GET', '/crm/rest/campanya/*').as('campanya');
        cy.intercept('GET', '/crm/rest/programa/*').as('programa');
        cy.intercept('GET', '/crm/rest/lineafacturacion/*').as('lineafacturacion');

        campanya.visitSection();
        cy.wait('@loadCampanyas');
    });


    it('Definir una campanya', () => {

        campanya.getAfegirBtn().click();
        campanya.getDesarBtn().click();
        cy.hasInvalidFieldsForm(3);
        campanya.getMessageBox().click();
        campanya.fillCampanyaForm();
        cy.hasInvalidFieldsForm(0);
        campanya.getDesarBtn().click();
        cy.wait('@loadCampanyas');
    });

    it('Eliminar campanya', () => {
        campanya.clearCampanyaGrid();
    });




});