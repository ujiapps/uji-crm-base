import CampanyaPage from "../../pagesobjects/gestion-campanyas/Campanya";
import DestinoOrigenPage from "../../pagesobjects/gestion-campanyas/DestinoOrigen";
import EstadoPage from "../../pagesobjects/gestion-campanyas/Estado";

describe('Testing gestión de campanyas: destino-origen', () =>{

    let campanya;
    let destino_origen;
    let estado;

    beforeEach(() => {
        campanya = new CampanyaPage(cy);
        destino_origen = new DestinoOrigenPage(cy);
        estado = new EstadoPage(cy);
        cy.intercept('GET', '/crm/rest/campanya/user/tree/root/admin/*').as('loadCampanyas');
        cy.intercept('GET', '/crm/rest/campanyacampanya/campanya/destino*').as('loadDestinos');
        cy.intercept('GET', '/crm/rest/campanya/*').as('loadCampanyadest');
        cy.intercept('GET', '/crm/rest/tipoestadocampanya/campanya/*').as('loadEstadosCampanya');

        cy.intercept('GET', '/crm/rest/subvinculo/*').as('subvinculo');
        cy.intercept('GET', '/crm/rest/campanyatexto/*').as('campanyatexto');
        cy.intercept('GET', '/crm/rest/campanya/*').as('campanya');
        cy.intercept('GET', '/crm/rest/programa/*').as('programa');
        cy.intercept('GET', '/crm/rest/lineafacturacion/*').as('lineafacturacion');
        
        destino_origen.visitSection();
        cy.wait('@loadCampanyas');
    });

    it('Crear y borrar destino-origen', () => {

        // Crea campaña
        campanya.createCampanya();
        cy.wait('@loadCampanyas');

        //Crea un estado para poder anyadirlo después a destino-origen
        campanya.selectCampanya();
        estado.selectTab().click();
        cy.wait('@loadEstadosCampanya');
        estado.getAfegirBtn().click();
        estado.fillEditorEstadosGrid();
        cy.wait('@loadEstadosCampanya');

        // Crear destino-origen a partir del estado
        destino_origen.selectTab().click();
        cy.wait('@loadDestinos');
        destino_origen.getAfegirBtn().click();
        cy.wait('@loadCampanyadest');
        cy.wait('@loadEstadosCampanya');
        destino_origen.fillCombos();
        cy.wait('@loadDestinos');

        // Borrar destino-origen
        destino_origen.borrarDestinoOrigen();
        cy.wait('@loadDestinos');

        // Borrar estado y campaña
        estado.selectTab().click();
        cy.wait('@loadEstadosCampanya');
        estado.clearTestEstadosGrid();
        campanya.clearCampanyaGrid();
        cy.wait('@loadCampanyas');
    });

});