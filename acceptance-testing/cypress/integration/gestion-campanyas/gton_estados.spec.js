import EstadoPage from "../../pagesobjects/gestion-campanyas/Estado";
import CampanyaPage from "../../pagesobjects/gestion-campanyas/Campanya";

describe('Testing gestion de campanyas: estados', () => {

    let campanya;
    let estado;

    beforeEach(() => {
        campanya = new CampanyaPage(cy);
        estado = new EstadoPage(cy);
        cy.intercept('GET', '/crm/rest/campanya/user/tree/root/admin/*').as('loadCampanyas');
        cy.intercept('GET', '/crm/rest/tipoestadocampanya/campanya/*').as('loadTipoEstado');
        cy.intercept('GET', '/crm/rest/tipoestadocampanyamotivo/*').as('loadMotivo');

        cy.intercept('GET', '/crm/rest/subvinculo/*').as('subvinculo');
        cy.intercept('GET', '/crm/rest/campanyatexto/*').as('campanyatexto');
        cy.intercept('GET', '/crm/rest/campanya/*').as('campanya');
        cy.intercept('GET', '/crm/rest/programa/*').as('programa');
        cy.intercept('GET', '/crm/rest/lineafacturacion/*').as('lineafacturacion');

        estado.visitSection();
        cy.waitRequest('@loadCampanyas', 200);
    });

    it('Crear estado', () => {

    // Crea campaña
        campanya.createCampanya();
        cy.waitRequest('@loadCampanyas', 200);

    // Crea estado a partir de la campaña
        campanya.selectCampanya();
        estado.selectTab().click();
        cy.waitRequest('@loadTipoEstado', 200);
        estado.getAfegirBtn().click();
        estado.fillEditorEstadosGrid();
        cy.waitRequest('@loadTipoEstado', 200);
        estado.selectRow();

    // Crea y borrar motivo a partir del estado
        estado.addMotivoBtn();
        estado.getMotivoAfegirBtn().click();
        estado.fillEditorMotivosGrid();
        cy.waitRequest('@loadMotivo', 200);
        estado.clearTestMotivoGrid();
        cy.waitRequest('@loadMotivo', 200);
        estado.closeMotivoWindow();

    // Borrar estado y campaña
        estado.clearTestEstadosGrid();
        campanya.clearCampanyaGrid();
    })
});