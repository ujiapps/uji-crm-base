import SeguimientoCampanyaPage from '../../pagesobjects/seguimiento-campanyas/SeguimientoCampanya';

describe('Testing seguimiento de campanyas', () => {

    let page;

    beforeEach(() => {
        page = new SeguimientoCampanyaPage(cy);
        cy.intercept('GET', '/crm/rest/campanya/user/tree/root/*').as('loadCampanyas');
        cy.intercept('GET', '/crm/rest/entradas/crm/*').as('loadActoCRMCampanya');
        cy.intercept('GET', '/crm/rest/campanyacliente/*').as('loadClientes');
        cy.intercept('GET', '/crm/rest/campanyaseguimiento/*').as('campanyaseguimientoget');
        cy.intercept('DELETE', '/crm/rest/campanyaseguimiento/*').as('deletecampanyaseguimiento');
        cy.intercept('POST', '/crm/rest/campanyaseguimiento/*').as('postcampanyaseguimiento');
        cy.intercept('PUT', '/crm/rest/campanyaseguimiento/*').as('putcampanyaseguimiento');
        page.visitarSeccion();
        cy.wait('@loadCampanyas');
    });

    it('tab clientes de campanya', () => {
        page.seleccionarCampanya('AAABBBJoaquin');
        page.usarFiltroBusqueda('Gimeno');
        page.usarFiltroEtiqueta('Treballador SAUJI');
        page.usarFiltroTipo('Hola que tal');
        page.usarFiltroSeguimientos('Presencial');
        // page.usarFiltroItems('Afganistan');
    });

    it.skip('Comprobar número de seguimientos de un cliente de una campanya', () => {
        page.seleccionarCampanya('AAABBBJoaquin');
        cy.waitRequest('@loadClientes', 200);
        page.comprobarNumeroSeguimientosCliente('73395993A');
    });

    it('tab seguimientos de campanya POST', () => {
        page.rellenarFilaSeguimientosGrid();
        cy.waitRequest('@postcampanyaseguimiento', 200);
        cy.waitRequest('@campanyaseguimientoget', 200);
    });

    it('tab seguimientos de campanya PUT', () => {
        page.irTabSeguimientos();
        page.getFilaSeguimientosGrid().dblclick();
        page.getSeguimientosGrid().getEditor().get(page.fields.fecha).clear().type('10/09/2021');
        page.getSeguimientosGrid().editorAcceptBtn().click();
        cy.waitRequest('@putcampanyaseguimiento', 200);
        cy.waitRequest('@campanyaseguimientoget', 200);
    });

    it('tab seguimientos de campanya DELETE', () => {
        page.borrarFilaSeguimientosGrid();
        cy.waitRequest('@deletecampanyaseguimiento', 204);
        cy.waitRequest('@campanyaseguimientoget', 200);
    });

    it('tab actos de campanya', () => {
        page.testActosDeCampanya();
        cy.waitRequest('@loadActoCRMCampanya', 200);
    });
});